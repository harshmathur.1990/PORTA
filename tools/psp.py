# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# psp.py                                                             #
#                                                                    #
# Tanausú del Pino Alemán                                            #
#   Instituto de Astrofísica de Canarias                           #
#                                                                    #
# A large part of the psp file handling is a modification from the   #
# PSPfile class from:                                                #
#                                                                    #
# Jiří Štěpán                                                        #
#   Astronomical Institute ASCR                                      #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Class for psp file handling                                        #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  24/05/2018 - V1.0.0 - First version. (TdPA)                       #
#                                                                    #
######################################################################
######################################################################
######################################################################

# ANSI codes
_psp__cursor_up_one = '\x1b[1A'
_psp__erase_line = '\x1b[2K'
_psp__tnormal = '\033[0m'
_psp__tnormalgreen = '\033[92m'
_psp__tnormalbold = '\033[1m'
_psp__tnormalblue = '\033[94m'
_psp__tnormalred = '\033[91m'
_psp__tnormalseparator = ''
_psp__twarning = _psp__tnormalred + '##Warning## ' + \
                 _psp__tnormal
_psp__terror = _psp__tnormalred + \
               _psp__tnormalbold + '##Error## ' + \
               _psp__tnormal

import sys,os,copy,time,struct
try:
    import numpy as np
except ImportError:
    print(_psp_class__terror + 'Missing numpy')
    raise
except:
    print(_psp_class__terror + \
          'Unexpected error while loading numpy:', \
          sys.exc_info()[0])
    raise
try:
    from scipy import fftpack
    from scipy import signal
    from scipy import interpolate
    from scipy.ndimage import gaussian_filter
    _psp__scipy = True
except ImportError:
    _psp__scipy = False
except:
    msg = _psp__terror + 'Unexpected error while loading scipy:'
    print(msg)
    print('  '+str(sys.exc_info()[0]))
    print('  '+str(sys.exc_info()[1]))
    _psp__scipy = False

if _psp__scipy:
    try:
        from degrade import *
        _psp__degrade = True
    except ImportError:
        _psp__degrade = False
    except:
        msg = _psp__twarning + \
              'Unexpected error while loading degrade.py:'
        print(msg)
        print('  '+str(sys.exc_info()[0]))
        print('  '+str(sys.exc_info()[1]))
        _psp__degrade = False
else:
    msg = _psp__twarning + \
          'Cannot load degrade.py without scipy:'
    print(msg)
    _psp__degrade = False

######################################################################
######################################################################
######################################################################
######################################################################

class psp():
    ''' Class for Porta SPectrum files (PSP). The class allows to
        access the PSP data, and to degrade it
    '''
######################################################################
######################################################################

    def __init__(self, filename=None):
        ''' Class initialization
                fName: Path to psp file
        '''

        # Argument handling
        if filename is None:
            self.__error('filename is a required argument')

        #
        # Set self variables

        # Size of header
        self.__header_size = 0

        # Read header
        self.__filename = filename
        self.__f = open(self.__filename, 'rb+')
        self.__header_size = self.__readheader()

        if self.__header_size < 1:
            self.__close()

        # Useful constants
        self.__cm2s = 1.3787950676165365e-8 
        self.__fwhm2sig = 4.24660900144009534e-1
        self.__sqrt2pi = 2.50662827463100024e0
        self.__deg2ra = 1.74532925199432955e-2
        self.__ra2deg = 5.72957795130823229e1

######################################################################
######################################################################

    def __error(self, msg=None, error=None):
        ''' Error message
        '''

        # Argument handling
        if msg is None:
            msg = ''

        # Set message
        msg = __terror + msg

        # Print error
        print(msg)
        if error is not None:
            for err in error:
                print('  '+str(err))

######################################################################
######################################################################

    def __warning(self, msg=None, error=None):
        ''' Warning message
        '''

        # Argument handling
        if msg is None:
            msg = ''

        # Set message
        msg = __twarning + msg

        # Print error
        print(msg)
        if error is not None:
            for err in error:
                print('  '+str(err))

######################################################################
######################################################################

    def cm2Mm(self, x):

        return x*1e-8

######################################################################
######################################################################

    def Mm2cm(self, x):

        return x*1e8

######################################################################
######################################################################

    def cm2sec(self, x):

        return x*self.__cm2s

######################################################################
######################################################################

    def m2sec(self, x):

        return x*self.__cm2s*1e2

######################################################################
######################################################################

    def km2sec(self, x):

        return x*self.__cm2s*1e5

######################################################################
######################################################################

    def Mm2sec(self, x):

        return x*self.__cm2s*1e8

######################################################################
######################################################################

    def sec2Mm(self, x):

        return x*1e-8/self.__cm2s

######################################################################
######################################################################

    def fwhm2sig(self, x):

        return x*self.__fwhm2sig

######################################################################
######################################################################

    def deg2ra(self, x):

        return x*self.__deg2ra

######################################################################
######################################################################

    def sec2ra(self, x):

        return x*self.__deg2ra/3600.

######################################################################
######################################################################

    def ra2deg(self, x):

        return x*self.__ra2deg

######################################################################
######################################################################

    def ra2sec(self, x):

        return x*self.__ra2deg*3600.

######################################################################
######################################################################

    def nm2m(self, x):

        return x*1e-9

######################################################################
######################################################################

    def Ang2m(self, x):

        return x*1e-10

######################################################################
######################################################################

    def Ang2mAng(self, x):

        return x*1e3

######################################################################
######################################################################

    def m2Ang(self, x):

        return x*1e10

######################################################################
######################################################################

    def mAng2m(self, x):

        return x*1e-13

######################################################################
######################################################################

    def __close(self):
        ''' Close the source file
        '''

        self.__f.close()

######################################################################
######################################################################

    def __readheader(self):
        ''' Read header of the PSP file'''

        # Initialize size of the header
        hsize = 0

        try:

            # Read magic string
            magic = self.__f.read(3);
            hsize += 3

            # Check magic string
            if magic != 'psp':
                self.__error('Invalid PSP file, failed to ' + \
                             'identify the magic string')
                return -1

            # Read psp version
            byte = self.__f.read(4)
            hsize += 4
            self.__psp_ver = struct.unpack('<i', byte)[0]

            # Read comments
            self.__comment = self.__f.read(1023)
            hsize += 1023

            # Read LOS
            byte = self.__f.read(8)
            hsize += 8
            self.__theta = struct.unpack('<d', byte)[0]
            byte = self.__f.read(8)
            hsize += 8
            self.__chi = struct.unpack('<d', byte)[0]

            # Read wavelengths
            byte = self.__f.read(4)
            hsize += 4
            self.__nl = struct.unpack('<i', byte)[0]
            self.__nl0 = self.__nl

            byte = self.__f.read(8*self.__nl)
            hsize += 8*self.__nl
            self.__l = np.array(struct.unpack('<' + 'd'*self.__nl, \
                                              byte))
            self.__l0 = np.copy(self.__l)

            # Surface ID
            byte = self.__f.read(4)
            hsize += 4
            self.__surface = struct.unpack('<i', byte)[0]

            # Domain
            byte = self.__f.read(8)
            hsize += 8
            self.__dx = self.cm2Mm(struct.unpack('<d', byte)[0])
            byte = self.__f.read(8)
            hsize += 8
            self.__dy = self.cm2Mm(struct.unpack('<d', byte)[0])

            # Period
            self.__period = []
            byte = self.__f.read(1)
            hsize += 1
            self.__period.append(struct.unpack('<b', byte)[0])
            byte = self.__f.read(1)
            hsize += 1
            self.__period.append(struct.unpack('<b', byte)[0])

            # Spatial axis
            byte = self.__f.read(4)
            hsize += 4
            self.__nx = struct.unpack('<i', byte)[0]
            self.__nx0 = self.__nx
            byte = self.__f.read(4)
            hsize += 4
            self.__ny = struct.unpack('<i', byte)[0]
            self.__ny0 = self.__nx

            byte = self.__f.read(8*self.__nx)
            hsize += 8*self.__nx
            self.__x = np.array(struct.unpack('<' + 'd'*self.__nx, \
                                              byte))
            self.__x0 = np.copy(self.__x)

            byte = self.__f.read(8*self.__ny)
            hsize += 8*self.__ny
            self.__y = np.array(struct.unpack('<' + 'd'*self.__ny, \
                                              byte))
            self.__y0 = np.copy(self.__y)

        except:

            self.__error('Unexpected error while loading scipy:', \
                         error=sys.exc_info()[:2])
            return -1

        return hsize

######################################################################
######################################################################

    def __writeheader(self, filename=None):
        ''' Write header of the PSP file'''

        # Check filename

        if filename is None:
            self.__error('filename is a required argument')
            return -1

        if filename == self.__fileName: 
            self.__error('Cannot write into the original file')
            return -1

        # Try opening file
        try:
            self.__fout = open(name=filename, mode='wb')
        except:
            self.__error('Cannot open file for writing: ' + \
                         filename, error=sys.exc_info()[:2])
            return -1

        # Write header:

        # Magic
        self.__fout.write(bytearray('psp'))

        # psp version
        self.__fout.write(struct.pack('<i', self.__psp_ver))

        # Comments
        self.__fout.write(bytearray(self.__comment))

        # LOS
        self.__fout.write(struct.pack('<d', self.__theta))
        self.__fout.write(struct.pack('<d', self.__chi))

        # Wavelength
        self.__fout.write(struct.pack('<i', self.__nl))
        for ii in range(self.__nwl):
            self.__fout.write(struct.pack('<d', self.__l[ii]))

        # Surface ID
        self.__fout.write(struct.pack('<i', self.__id_surface))

        # Domain
        self.__fout.write(struct.pack('<d', self.Mm2cm(self.__dx)))
        self.__fout.write(struct.pack('<d', self.Mm2cm(self.__dy)))

        # Period
        self.__fout.write(struct.pack('<b', self.__period[0]))
        self.__fout.write(struct.pack('<b', self.__period[1]))

        # Axis
        self.__fout.write(struct.pack('<i', self.__nx))
        self.__fout.write(struct.pack('<i', self.__ny))
        for ii in range(self.__nx):
            self.__fout.write(struct.pack('<d', self.Mm2cm(self.__x[ii])))
        for ii in range(self.__ny):
            self.__fout.write(struct.pack('<d', self.Mm2cm(self.__y[ii])))

        return 0

######################################################################
######################################################################

    def __writedata(self, data=None):
        ''' Write data of the PSP file and close the file'''

        if data is None:

            self.__error('Data is a required argument')
            return

        else:

            try:

                if len(data.shape) != 4:
                    self.__error('data must be 4D array')
                    return

                if 'float' not in str(data.dtype):
                    self.__error('data must be float type')
                    return

            except:

                self.__error('Unexpected error while checking ' + \
                             'the data', error= sys.exc_info()[:2])
                return -1

        try:

            if self.__fout.closed:
                self.__error('There is not an opened output ' + \
                             'unit. Use writeheader before')
                return

        except NameError:

            self.__error('There is not an opened output ' + \
                         'unit. Use writeheader before')
            return

        except:

            self.__error('Error while checking output unit numpy:', \
                         error=sys.exc_info()[:2])
            return

        self.__fout.write(struct.pack('%sf' % (self.__nx*self.__ny* \
                                               self.__nl*4), \
                                      *data.flatten()))
        self.__fout.close()

######################################################################
######################################################################

    def writepsp(self, filename=None, data=None):
        ''' Write a psp file with the new stokes parameters
        '''

        # Check filename

        if filename is None:
            self.__error('filename is a required argument')
            return

        if filename == self.__fileName: 
            self.__error('Cannot write into the original file')
            return 

        if data is None:

            self.__error('data is a required argument')
            return

        else:

            try:

                if len(data.shape) != 4:
                    self.__error('data must be 4D array')
                    return

                if 'float' not in str(data.dtype):
                    self.__error('data must be float type')
                    return

            except:

                self.__error('Unexpected error while checking ' + \
                             'the data', error=sys.exc_info()[:2])
                return -1

        nl = self.__l.shape[0]
        nx, ny = self.__x.shape[0], self.__y.shape[0]

        if nl != self.__nl:
            self.__error('Wrong size of wavelength vector')
            return

        if nx != self.__nx:
            self.__error('Wrong size of x axis')
            return

        if ny != self.__ny:
            self.__error('Wrong size of y axis')
            return


        nl, ny, nx, nstk = data.shape

        if nl != self.__nl:
            self.__error('Wrong size of data wavelength dimension')
            return

        if nx != self.__nx:
            self.__error('Wrong size of data x dimension')
            return

        if ny != self.__ny:
            self.__error('Wrong size of data y dimension')
            return

        if nstk != 4:
            self.__error('Wrong size of data stokes dimension')
            return

        check = self.__writeheader(filename)

        if check == 0:
            self.__writedata(data)

######################################################################
######################################################################

    def getnxy(self):
        ''' Get nx and ny, number of points per axis x and y
        '''

        return self.__nx, self.__ny

######################################################################
######################################################################

    def getnxy0(self):
        ''' Get nx and ny, number of points per axis x and y in the
            file
        '''

        return self.__nx0, self.__ny0

######################################################################
######################################################################

    def getnx(self):
        ''' Get nx number of points per axis x 
        '''

        return self.__nx

######################################################################
######################################################################

    def getnx0(self):
        ''' Get nx number of points per axis x in the file
        '''

        return self.__nx0

######################################################################
######################################################################

    def getx(self):
        ''' x coordinates (in Mm)
        '''

        return self.__x

######################################################################
######################################################################

    def getx0(self):
        ''' x coordinates in the file (in Mm)
        '''

        return self.__x0

######################################################################
######################################################################

    def getny(self):
        ''' Get ny, number of points per axis y
        '''

        return self.__ny

######################################################################
######################################################################

    def getny0(self):
        ''' Get ny number of points per axis y in the file
        '''

        return self.__ny

######################################################################
######################################################################

    def gety(self):
        ''' y coordinates (in Mm)
        '''

        return self.__y

######################################################################
######################################################################

    def gety0(self):
        ''' y coordinates in the file (in Mm)
        '''

        return self.__y0

######################################################################
######################################################################

    def gettheta(self):
        ''' Return theta angle
        '''

        return self.__theta

######################################################################
######################################################################

    def getchi(self):
        ''' Return chi angle
        '''

        return self.__chi

######################################################################
######################################################################

    def getnl(self):
        ''' Number of wavelengths
        '''

        return self.__nl

######################################################################
######################################################################

    def getnl0(self):
        ''' Number of wavelengths in the file
        '''

        return self.__nl0

######################################################################
######################################################################

    def getwavelength(self):
        ''' Wavelengths
        '''

        return self.__l

######################################################################
######################################################################

    def getwavelength0(self):
        ''' Wavelengths in the file
        '''

        return self.__l0

######################################################################
######################################################################

    def getmap(self, il=None):
        ''' Array of Stokes XY maps at the wavelength index il
        '''

        if il is None:
            il = 0

        # Compute jump for this wavelength
        skip = self.__header_size + 8*il*self.__nx0*self.__ny0*4

        # Jump to plane in file
        self.__f.seek(skip, 0)

        bytes = self.__f.read(4*4*self.__nx0*self.__ny0)
        stokes = np.array(struct.unpack('<'+'f'*(self.__nx0* \
                                                 self.__ny0*4*4), \
                                        bytes)).reshape(self.__ny0, \
                                                        self.__nx0,4)

        return stokes

######################################################################
######################################################################

    def getspectrumxy(self, ix=None, iy=None):
        ''' Get Stokes spectrum at a particular point [ix,iy] of the
            surface
        '''

        if ix is None:
            ix = 0
        if iy is None:
            iy = 0

        if ix < 0:
            self.__error('ix must be larger or equal than 0')
            return -1
        if ix > self.__nx0 - 1:
            self.__error('ix must be smaller than x axis size')
            return -1

        if iy < 0:
            self.__error('iy must be larger or equal than 0')
            return -1
        if iy > self.__ny0 - 1:
            self.__error('iy must be smaller than y axis size')
            return -1

        xyshift = 4*(self.__nx0*iy + ix)*4 + self.__header_size

        stokes = np.zeros(self.__nl0,4)

        for il in range(self.__nl0):
            skip = self.__header_size
            self.__f.seek(xyshift + 4*il*self.__ny0*self.__nx0*4, 0)
            byte = self.__f.read(4*4)
            stokes[il,:] = np.array(struct.unpack('<'+'f'*4, byte))

        return stokes

######################################################################
######################################################################

    def getaveragespectrum(self, il0=None, il1=None):
        ''' Spatially averaged spectrum between wavelength indices
            il0, il1
        '''

        if il0 is None:
            il0 = 0
        if il1 is None:
            il1 = self.__nl0 - 1

        if il0 >= il1:
            self.__error('il0 must be smaller than il1')
            return -1
        if il0 < 0:
            self.__error('il0 must be larger or equal than 0')
            return -1
        if il1 > self.__nl0 - 1:
            self.__error('il1 must be smaller than wavelength size')
            return -1

        nl = il2 - il1 + 1

        stokes = np.zeros(nl,4)

        for il in range(il0,il1+1):
            stkxy = self.getmap(il)
            stokes[il,:] = np.mean(stkxy,axis=(0,1))

        return stokes

######################################################################
######################################################################

    def getminiwavelength(self, il0=0, il1=0):
        ''' Returns 2D array of wavelengths with minimum intensity
            in between the two wavelength indices
        '''

        if il0 is None:
            il0 = 0
        if il1 is None:
            il1 = self.__nl0 - 1

        if il0 >= il1:
            self.__error('il0 must be smaller than il1')
            return -1
        if il0 < 0:
            self.__error('il0 must be larger or equal than 0')
            return -1
        if il1 > self.__nl - 1:
            self.__error('il1 must be smaller than wavelength size')
            return -1

        inds = np.zeros(self.__ny0, self.__nx0)

        for ix,iy in [(ix,iy) for ix in self.__nx0 \
                              for iy in self.__ny0]:

            stk = self.__getspectrumxy(ix,iy)[il0:il1+1,0]
            inds[ix,iy] = il0 + numpy.argmin(stk)

        return inds

######################################################################
######################################################################

    def getmaxpwavelength(self, il0=0, il1=0):
        ''' Returns 2D array of wavelengths with maximum linear
            polarization degree in between the two wavelength indices
        '''

        if il0 is None:
            il0 = 0
        if il1 is None:
            il1 = self.__nl0 - 1

        if il0 >= il1:
            self.__error('il0 must be smaller than il1')
            return -1
        if il0 < 0:
            self.__error('il0 must be larger or equal than 0')
            return -1
        if il1 > self.__nl0 - 1:
            self.__error('il1 must be smaller than wavelength size')
            return -1

        inds = np.zeros(self.__ny0, self.__nx0)

        for ix,iy in [(ix,iy) for ix in self.__nx0 \
                              for iy in self.__ny0]:

            stk = self.__getspectrumxy(ix,iy)[il0:il1+1,:]
            stk = np.sqrt(stk[:,1]*stk[:,1] + stk[:,2]*stk[:,2])
            inds[ix,iy] = il0 + numpy.argmax(stk)

        return inds

######################################################################
######################################################################

    def getcube(self):
        ''' Returns 4D cube of Stokes parameters
        '''

        self.__f.seek(self.__header_size, 0)

        # Read Stokes
        byte = self.__f.read(8*4*self.__nx0*self.__ny0*self.__nwl0)
        stokes = np.array(struct.unpack('<'+'f'*(self.__nx0* \
                                                 self.__ny0* \
                                                 self.__nl0*4), \
                                        byte)).reshape(self.__nl0, \
                                                       self.__ny0, \
                                                       self.__nx0,4)
        return stokes

######################################################################
######################################################################

    def degrade_primary(self, data=None, x=None, y=None, lamb=None, \
                        kernel=None, l0=None, D=None, r0=None, \
                        fwhm=None):
        ''' 2D convolution for the specified wavelengths and stokes
            parameters
        '''

        if not __degrade:
            self.__error('modules for degradation not loaded')
            return -1

        deg = degrade_class()

        t0 = time.clock()

        if data is None:

            self.__warning('data no specified, taking ' + \
                           'original Stokes parameters')
            data = self.getcube()

        try:

            if len(data.shape) != 4:
                self.__error('data must be 4D array, even if ' + \
                             'some dimensions are one')
                return -1

            if 'float' not in str(data.dtype):
                self.__error('data must be float type')
                return -1

        except:

            self.__error('Unexpected error while checking ' + \
                         'the data', error=sys.exc_info()[:2])
            return -1

        if x is None:
            x = self.__x

        try:

            if len(x.shape) != 1:
                self.__error('x must be 1D array')
                return -1

            if 'float' not in str(x.dtype):
                self.__error('x must be float type')
                return -1

        except:

            self.__error('Unexpected error while checking ' + \
                         'x axis', error=sys.exc_info()[:2])
            return -1

        if y is None:
            y = self.__y

        try:

            if len(y.shape) != 1:
                self.__error('y must be 1D array')
                return -1

            if 'float' not in str(x.dtype):
                self.__error('y must be float type')
                return -1

        except:

            self.__error('Unexpected error while checking ' + \
                         'y axis', error=sys.exc_info()[:2])
            return -1

        if lamb is None:
            lamb = self.__l

        try:

            if len(lamb.shape) != 1:
                self.__error('lamb must be 1D array')
                return -1

            if 'float' not in str(lamb.dtype):
                self.__error('lamb must be float type')
                return -1

        except:

            self.__error('Unexpected error while checking ' + \
                         'lamb axis', error=sys.exc_info()[:2])
            return -1

        nl, ny, nx, nstk = data.shape
        nxa = x.shape[0]
        nya = y.shape[0]
        nla = lamb.shape[0]

        if nx != nxa:
            self.__error('x dimension of data an axis differs')
            return -1

        if ny != nya:
            self.__error('y dimension of data an axis differs')
            return -1

        if nl != nla and l0 is None:
            self.__error('lambda dimension of data an axis ' + \
                         'differs and l0 is not specified')
            return -1

        if kernel is None:
            self.__error('kernel is a required argument')
            return -1

        # Prepare axis, part I
        lamb = self.Ang2m(lamb)

        # PDF gaussian
        if kernel.lower() in 'gauss':

            if fwhm is None:

                if D is None:
                    self.__error('You must specify fwhm ' + \
                                 'or D for the gauss kernel')
                    return -1

                else:
                    lD = D

                # Definition
                if l0 is None:
                    f = .99/lD
                    par = np.copy(lamb)*0.
                    for il in range(NWL):
                        par[il] = self.fwhm2sig(a.ra2sec(f*lamb[il]))

            else:

                if D is not None:
                    self.__warning('You specified both fwhm ' + \
                                   'and D. The default is using ' + \
                                   'sigma')
                lD = None
                par = np.copy(lamb)*0.

                # Definition
                if l0 is None:
                    f = .99/prim[1]
                    par = np.copy(lamb)*0.
                    for il in range(NWL):
                        par[il] = self.fwhm2sig(fwhm)
                else:
                    par = np.copy(lamb)*0.

            lkernel = 'gauss'
            lr0 = None

        # If difraction degradation
        elif kernel.lower() in 'airy':

            if D is None:
                self.__error('You must specify D ' + \
                             'for the Airy kernel')
                return -1

            else:
                lD = D

            par = np.copy(lamb)*0.

            lkernel = 'airy'
            lr0 = None

        # If difraction + seeing (Fried 1966 long exposure)
        # degradation
        elif kernel.lower() == 'seeing':

            if r0 is None:
                self.__error('You must specify r0 ' + \
                             'for the seeing kernel')
                return -1

            else:
                lr0 = r0

            if D is None:
                self.__error('You must specify D ' + \
                             'for the seeing kernel')
                return -1

            else:
                lD = D

            par = np.copy(lamb)*0.

            lkernel = 'seeing'

        # Wrong degradation
        else:

            self.__error('kernel type not recognized ' + kernel)
            return -1

        # Fix wavelength if specified
        if l0 is not None:
            ll = l0

        #
        # Prepare axis, part II

        xx = self.Mm2sec(x)*np.cos(self.deg2ra(psp.gettheta()))
        yy = self.Mm2sec(y)

        # Allocate output
        datao = np.copy(data)

        # Check if arrays need reversal

        # X
        if xx[1] < xx[0]:
            xreverse = True
            xx = xx[::-1]
            datao = datao[:,:,::-1,:]
        else:
            xreverse = False

        # Y
        if yy[1] < yy[0]:
            yreverse = True
            yy = yy[::-1]
            datao = datao[:,::-1,:,:]
        else:
            yreverse = False

        # Check periodicity
        xperiod = period[0] == 1
        xextend = not xperiod
        yperiod = period[1] == 1
        yextend = not yperiod

        # For each wavelength and stokes parameter
        for il,istk in [(il,istk) for il in range(nl) \
                                  for istk in range(nstk)]:

            # If not defined, get current wavelength
            if l0 is None:
                ll = lamb[il]

            # 2D convolution
            datao[il,:,:,istk] = deg.convol2D(datao[il,:,:,istk], \
                                              xaxis=yy, \
                                              yaxis=xx, \
                                              xpar=par[il], \
                                              ypar=par[il], \
                                              xperiod=xperiod, \
                                              yperiod=yperiod, \
                                              xextend=xextend, \
                                              yextend=yextend, \
                                              xdir=None, \
                                              ydir=None, \
                                              kernel=lkernel, \
                                              r0=lr0, D=lD, l0=lll)

        # Reverse back
        if xreverse:
            datao = datao[:,:,::-1,:]
        if yreverse:
            datao = datao[:,::-1,:,:]

        t1 = time.clock()

        return datao

######################################################################
######################################################################

    def degrade_spectrum(self, data=None, lamb=None, kernel=None, \
                         fwhm=None, dl=None):
        ''' 1D spectral convolution for the specified spatial points
            and Stokes parameters
        '''

        if not __degrade:
            self.__error('modules for degradation not loaded')
            return -1

        deg = degrade_class()

        t0 = time.clock()

        if data is None:

            self.__warning('data no specified, taking ' + \
                           'original Stokes parameters')
            data = self.getcube()


        try:

            if len(data.shape) != 4:
                self.__error('data must be 4D array, even if ' + \
                             'some dimensions are one')
                return -1

            if 'float' not in str(data.dtype):
                self.__error('data must be float type')
                return -1

        except:

            self.__error('Unexpected error while checking ' + \
                         'the data', error=sys.exc_info()[:2])
            return -1


        if lamb is None:
            lamb = self.__l

        try:

            if len(lamb.shape) != 1:
                self.__error('lamb must be 1D array')
                return -1

            if 'float' not in str(lamb.dtype):
                self.__error('lamb must be float type')
                return -1

        except:

            self.__error('Unexpected error while checking ' + \
                         'lamb axis', error=sys.exc_info()[:2])
            return -1


        lamb = self.Ang2m(lamb)

        nl, ny, nx, nstk = data.shape
        nla = lamb.size

        if nl != nla:
            self.__error('lambda dimension of data an axis ' + \
                         'differs')
            return -1

        if fwhm is None:
            self.__error('fwhm is a required argument')
            return -1

        if kernel is None:
            self.__error('kernel is a required argument')
            return -1

        # Allocate output
        datao = np.copy(data)

        #
        # Check if arrays need reversal

        # lambda
        if lamb[1] < lamb[0]:
            lreverse = True
            lamb = lamb[::-1]
            datao = datao[::-1,:,:,:]
        else:
            lreverse = False

        # If gaussian degradation
        if kernel.lower() in 'gauss':

            # If refining the wavelength grid previous to the
            # convolution
            if dl is not None:

                inter = True
                ldl = self.mAng2m(dl)
                l1 = np.amax(lamb)
                l0 = np.amin(lamb)
                nl1 = int((l1 - l0)/ldl + 1)
                lamb1 = np.linspace(0,nl1-1,num=nl1, \
                                    dtype=lamb.dtype)
                lamb1 *= ldl
                lamb1 += l0
                equi = True

            # Not refining
            else:

                inter = False
                nl1 = nl
                lamb1 = lamb

                ds = lamb1[1:] - lamb1[0:-1]
                equi = True
                for ii in range(0,len(ds)-1):
                    for jj in range(ii+1,len(ds)):
                        if (ds[ii] - ds[jj])/(ds[ii] + ds[jj]) > \
                           1e-8:
                            equi = False
                            break
                    if not equi:
                        break

            lkernel = 'gauss'
            par = a.fwhm2sig(self.mAng2m(fwhm))

        # Wrong degradation
        else:

            self.__error('kernel type not recognized ' + kernel)
            return -1

        # For each spatial node and stokes parameter
        for iy,ix,istk in [(iy,ix,istk) for iy in range(ny) \
                                        for ix in range(nx) \
                                        for istk in range(nstk)]:

            # Get the vector to degrade
            stkin = np.copy(datao[:,iy,ix,istk])

            # Refine if necessary
            if inter:
                stkin = np.interp(lamb1,lamb,stkin)

            # Convolution
            stkin = deg.convol1D(stkin, axis=lamb1, \
                                 par=par, extend=True, \
                                 adir=5., kernel=lkernel, \
                                 equi=equi)

            # Coarse if necessary
            if inter:
                stkin = np.interp(lamb,lamb1,stkin) 

            # Store in array
            datao[:,iy,ix,istk] = stkin

        t1 = time.clock()

        return datao

######################################################################
######################################################################

    def degrade_spatialaxis(self, data=None, x=None, y=None, \
                            kernel=None, axis=None, fwhm=None, \
                            box=None, ds=None):
        ''' 1D spatial convolution for the specified spatial points
            and Stokes parameters in the specified axis
        '''

        if not __degrade:
            self.__error('modules for degradation not loaded')
            return -1

        deg = degrade_class()

        t0 = time.clock()

        if data is None:

            self.__warning('data no specified, taking ' + \
                           'original Stokes parameters')
            data = self.getcube()

        try:

            if len(data.shape) != 4:
                self.__error('data must be 4D array, even if ' + \
                             'some dimensions are one')
                return -1

            if 'float' not in str(data.dtype):
                self.__error('data must be float type')
                return -1

        except:

            self.__error('Unexpected error while checking ' + \
                         'the data', error=sys.exc_info()[:2])
            return -1

        if x is None:
            x = self.__x

        try:

            if len(x.shape) != 1:
                self.__error('x must be 1D array')
                return -1

            if 'float' not in str(x.dtype):
                self.__error('x must be float type')
                return -1

        except:

            self.__error('Unexpected error while checking ' + \
                         'x axis', error=sys.exc_info()[:2])
            return -1

        if y is None:
            y = self.__y

        try:

            if len(y.shape) != 1:
                self.__error('y must be 1D array')
                return -1

            if 'float' not in str(x.dtype):
                self.__error('y must be float type')
                return -1

        except:

            self.__error('Unexpected error while checking ' + \
                         'y axis', error=sys.exc_info()[:2])
            return -1

        nl, ny, nx, nstk = data.shape
        nxa = x.shape[0]
        nya = y.shape[0]

        if nx != nxa:
            self.__error('x dimension of data an axis differs')
            return -1

        if ny != nya:
            self.__error('y dimension of data an axis differs')
            return -1

        if kernel is None:
            self.__error('kernel is a required argument')
            return -1

        if axis is None:
            self.__error('axis is a required argument')
            return -1

        if axis.lower() not in ['x','y']:
            self.__error('axis can only be x or y')
            return -1


        # PDF gaussian
        if kernel.lower() in 'gauss':

            if fwhm is None:
                self.__error('You must specify fwhm ' + \
                             'for the gauss kernel')
                return -1

            lkernel = 'gauss'
            par = self.fwhm2sig(fwhm)

        # If square degradation
        elif kernel.lower() in 'square':

            lkernel = 'square'
            par = box


        # Wrong degradation
        else:

            self.__error('kernel type not recognized ' + kernel)
            return -1


        # Check the direction to convolve
        if axis.lower() is 'x':
            laxis = 1
            na = nx
            aa = self.Mm2sec(x)* \
                 np.cos(self.deg2ra(psp.gettheta()))
        elif axis.lower() is 'y':
            laxis = 2
            na = ny
            aa = self.Mn2sec(y)

        period = self.__period[laxis-1] == 1
        extend = not period


        # If refining the grid previous to the convolution
        if ds is not None:

            inter = True
            lds = ds
            a0 = np.amin(aa)
            a1 = np.amax(aa)
            na1 = int((a1 - a0)/lds + 1)
            aa1 = np.linspace(0,na1-1,num=na1, \
                              dtype=aa.dtype)
            aa1 *= ds
            aa1 += a0
            equi = True

        # Not refining
        else:

            inter = False
            na1 = NA
            aa1 = aa

            equi = True
            lds = aa[1:] - a[0:-1]
            for ii in range(0,len(lds)-1):
                for jj in range(ii+1,len(ds)):
                    if (lds[ii] - lds[jj])/(lds[ii] + lds[jj]) > \
                       1e-8:
                        equi = False
                        break
                if not equi:
                    break

        # Allocate output
        datao = np.copy(data)

        # Check for reverse
        if aa[1] < aa[0]:
            areverse = True
            aa = aa[::-1]
            if laxis == 1:
                datao = datao[:,:,::-1,:]
            else:
                datao = datao[:,::-1,:,:]
        else:
            areverse = False

        # If convolving along x
        if axis == 1:

            # For each wavelength, y and stokes parameter
            for il,iy,istk in [(il,iy,istk) for il in range(nl) \
                                            for iy in range(ny) \
                                            for istk in range(nstk)]:

                # Get the vector to degrade
                stkin = np.copy(datao[il,iy,:,istk])

                # Refine if necessary
                if inter:
                    stkin = np.interp(aa1,aa,stkin)

                # Convolution
                stkin = a.convol1D(stkin, axis=aa1, \
                                   par=par, period=period, \
                                   extend=extend, adir=None, \
                                   kernel=lkernel, equi=equi)

                # Coarse if necessary
                if inter:
                    stkin = np.interp(aa,aa1,stkin) 

                # Store in array
                datao[il,iy,:,istk] = stkin

        # If convolving along y
        elif axis == 2:

            # For each wavelength, x and stokes parameter
            for il,ix,istk in [(il,ix,istk) for il in range(nl) \
                                            for ix in range(nx) \
                                            for istk in range(nstk)]:

                # Get the vector to degrade
                stkin = np.copy(datao[il,:,ix,istk])

                # Refine if necessary
                if inter:
                    stkin = np.interp(aa1,aa,stkin)

                # Convolution
                stkin = a.convol1D(stkin, axis=aa1, \
                                   par=par, period=period, \
                                   extend=extend, adir=None, \
                                   kernel=lkernel, equi=equi)

                # Coarse if necessary
                if inter:
                    stkin = np.interp(aa,aa1,stkin) 

                # Store in array
                datao[il,:,ix,istk] = stkin

        t1 = time.clock()

        return datao

######################################################################
######################################################################

    def degrade_pixelize(self, data=None, x=None, y=None, lamb=None, \
                         box=None, ds=None, update=None):
        ''' 1D spatial convolution with a square and pixelization of
            the result. This routine changes the axis
        '''

        if not __degrade:
            self.__error('modules for degradation not loaded')
            return -1

        deg = degrade_class()

        t0 = time.clock()

        if data is None:

            self.__warning('data no specified, taking ' + \
                           'original Stokes parameters')
            data = self.getcube()

        try:

            if len(data.shape) != 4:
                self.__error('data must be 4D array, even if ' + \
                             'some dimensions are one')
                return -1

            if 'float' not in str(data.dtype):
                self.__error('data must be float type')
                return -1

        except:

            self.__error('Unexpected error while checking ' + \
                         'the data', error=sys.exc_info()[:2])
            return -1

        if x is None:
            x = self.__x

        try:

            if len(x.shape) != 1:
                self.__error('x must be 1D array')
                return -1

            if 'float' not in str(x.dtype):
                self.__error('x must be float type')
                return -1

        except:

            self.__error('Unexpected error while checking ' + \
                         'x axis', error=sys.exc_info()[:2])
            return -1

        if y is None:
            y = self.__y

        try:

            if len(y.shape) != 1:
                self.__error('y must be 1D array')
                return -1

            if 'float' not in str(x.dtype):
                self.__error('y must be float type')
                return -1

        except:

            self.__error('Unexpected error while checking ' + \
                         'y axis', error=sys.exc_info()[:2])
            return -1

        if lamb is None:
            lamb = self.__l

        try:

            if len(lamb.shape) != 1:
                self.__error('lamb must be 1D array')
                return -1

            if 'float' not in str(lamb.dtype):
                self.__error('lamb must be float type')
                return -1

        except:

            self.__error('Unexpected error while checking ' + \
                         'lamb axis', error=sys.exc_info()[:2])
            return -1

        nl, ny, nx, nstk = data.shape
        nxa = x.shape[0]
        nya = y.shape[0]
        nla = lamb.shape[0]

        if nx != nxa:
            self.__error('x dimension of data an axis differs')
            return -1

        if ny != nya:
            self.__error('y dimension of data an axis differs')
            return -1

        if nl != nla and l0 is None:
            self.__error('lambda dimension of data an axis ' + \
                         'differs and l0 is not specified')
            return -1

        if box is None:
            self.__error('You must specify the box parameter')
            return -1

        if update is None:
            lupdate = False
        else:
            lupdate = update
            if not isinstance(lupdate, bool):
                self.__error('update must be bool, using false')
                lupdate = False

        lds = {'x':None, 'y':None, 'l':None}
        if ds is not None:
            try:
                ldsk = lds.keys()
                for key in ds.keys():
                    try:
                        if key in ldsk:
                            lds[key] = ds[key]
                    except KeyError:
                        pass
                    except:
                        raise

            except:
                self.__warning('ds must be a dictionary, ignoring')

        # Kernel
        lkernel = 'square'

        # Prepare axis
        lamb = self.Ang2m(lamb)
        xx = self.Mm2sec(x)*np.cos(self.deg2ra(psp.gettheta()))
        yy = self.Mm2sec(y)

        # Allocate output
        datao = np.copy(data)

        # Check if arrays need reversal

        # X
        if xx[1] < xx[0]:
            xreverse = True
            xx = xx[::-1]
            datao = datao[:,:,::-1,:]
        else:
            xreverse = False

        # Y
        if yy[1] < yy[0]:
            yreverse = True
            yy = yy[::-1]
            datao = datao[:,::-1,:,:]
        else:
            yreverse = False

        # lamb
        if lamb[1] < lamb[0]:
            lreverse = True
            lamb = lamb[::-1]
            datao = datao[::-1,:,:,:]
        else:
            lreverse = False

        # Check periodicity
        xperiod = period[0] == 1
        xextend = not xperiod
        yperiod = period[1] == 1
        yextend = not yperiod
        lperiod = False
        lextend = True
        equi = True

        # Define operation list and allowed list
        try:
            oplst = box.keys()
        except:
            self.__error('box must be a dictionary')
            return -1

        opnum = len(oplst)
        oplst0 = ['x','y','l']

        # Check that operation is valid and unique
        for op,ii in zip(oplst,range(opnum)):
            if op not in oplst0:
                self.__warning('Pixel dimension not recognized ' + op)
                continue
            for jj in range(ii+1,opnum):
                if op == oplst[jj]:
                    self.__warning('Pixel dimension duplicated' + op)
                    continue

        # For each operation
        for op,ii in zip(oplst,range(opnum)):

            # If spectral pixelization
            if op == 'l':

                # Get the minimum distance between wavelengths,
                # and the limits
                mindl = np.amin(lamb[1:] - lamb[0:-1])
                l1 = np.amax(lamb)
                l0 = np.amin(lamb)

                # Get the fine array to convolve
                dl0 = a.mAng2m(box[op])

                # Fine grid
                if lds[op] is None:

                    ldl = dl0

                    # Get the real fine dl, smaller than the original
                    ldl *= .5
                    while ldl > .5*mindl:
                        ldl *= .5

                else:

                    ldl = lds[op]

                # Build fine grid
                nl1 = int((l1 - l0)/ldl + 1)
                lamb1 = np.linspace(0,nl1-1,num=nl1, \
                                    dtype=lamb.dtype)
                lamb1 *= ldl
                lamb1 += l0

                # Get final wavelength array
                nlf = int((l1 - l0)/dl0 + 1)
                lambf = np.linspace(0,NWLf-1,num=NWLf, \
                                    dtype=lamb.dtype)
                lambf *= dl0
                lambf += l0

                # Create output array
                datao = np.zeros((nlf,ny,nx,4))

                # Parameters of convolution
                par = dl0
                extend = True
                period = False

                # For each spatial node and stokes parameter
                for iy,ix,istk in [(iy,ix,istk) \
                                            for iy in range(ny) \
                                            for ix in range(nx) \
                                            for istk in range(nstk)]:

                    # Get the vector to degrade
                    stkin = np.copy(data[:,iy,ix,istk])

                    # Refine
                    stkin = np.interp(lamb1,lamb,stkin)

                    # Convolution
                    stkin = a.convol1D(stkin, axis=lamb1, \
                                       par=par, period=period, \
                                       extend=extend, adir=None, \
                                       kernel=lkernel, equi=equi)

                    # Interpolate to final array
                    stkin = np.interp(lambf,lamb1,stkin) 

                    # Store in array
                    datao[:,iy,ix,istk] = stkin


                # Update arrays
                nl = nlf
                lamb = np.copy(lambf)
                lambf = 0.
                data = np.copy(datao)

            # If X axis pixelization
            if op() == 'x':

                # Get the minimum distance between points
                # and the limits
                mindx = np.amin(xx[1:] - xx[0:-1])
                x1 = np.amax(xx)
                x0 = np.amin(xx)

                # Get the fine array to convolve
                dx0 = box[op]

                # Fine grid
                if lds[op] is None:

                    ldx = dx0

                    # Get the real fine dx, smaller than the original
                    ldx *= .5
                    while ldx > .5*mindx:
                        ldx *= .5

                else:

                    ldx = lds[op]

                # Build fine grid
                nx1 = int((x1 - x0)/ldx + 1)
                x1 = np.linspace(0,nx1-1,num=nx1,dtype=xx.dtype)
                x1 *= ds
                x1 += s0

                # Get final wavelength array
                nxf = int((x1 - x0)/ldx0 + 1)
                xf = np.linspace(0,nxf-1,num=nxf,dtype=xx.dtype)
                xf *= dx0
                xf += x0

                # Create output array
                datao = np.zeros((nl,ny,nxf,4))

                # Parameters of convolution
                par = dx0
                extend = False
                period = True

                # For each spatial node and stokes parameter
                for il,iy,istk in [(il,iy,istk) \
                                            for il in range(nl) \
                                            for iy in range(ny) \
                                            for istk in range(nstk)]:

                    # Get the vector to degrade
                    stkin = np.copy(data[il,iy,:,istk])

                    # Refine
                    stkin = np.interp(x1,x,stkin)

                    # Convolution
                    stkin = a.convol1D(stkin, axis=x1, \
                                       par=par, period=period, \
                                       extend=extend, adir=None, \
                                       kernel=lkernel, equi=equi)

                    # Interpolate to final array
                    stkin = np.interp(xf,x1,stkin) 

                    # Store in array
                    datao[il,iy,:,istk] = stkin

                # Update arrays for good
                nx = nxf
                x = np.copy(xf)
                xf = 0.
                data = np.copy(datao)

            # If Y axis pixelization
            if op.upper() == 'y':

                # Get the minimum distance between points
                # and the limits
                mindy = np.amin(yy[1:] - yy[0:-1])
                y1 = np.amax(yy)
                y0 = np.amin(yy)

                # Get the fine array to convolve
                dy0 = box[op]

                # Fine grid
                if lds[op] is None:

                    ldy = dy0

                    # Get the real fine dx, smaller than the original
                    ldy *= .5
                    while ldy > .5*mindy:
                        ldy *= .5

                else:

                    ldy = lds[op]

                # Build fine grid
                ny1 = int((y1 - y0)/dy + 1)
                y1 = np.linspace(0,ny1-1,num=ny1,dtype=yy.dtype)
                y1 *= dy
                y1 += y0

                # Get final wavelength array
                nyf = int((y1 - y0)/dy0 + 1)
                yf = np.linspace(0,nyf-1,num=nyf,dtype=y.dtype)
                yf *= dy0
                yf += y0

                # Create output array
                datao = np.zeros((nl,nyf,nx,4))

                # Parameters of convolution
                par = dy0
                extend = False
                period = True

                # For each spatial node and stokes parameter
                for il,ix,istk in [(il,ix,istk) \
                                            for il in range(nl) \
                                            for ix in range(nx) \
                                            for istk in range(nstk)]:

                    # Get the vector to degrade
                    stkin = np.copy(data[il,:,ix,istk])

                    # Refine
                    stkin = np.interp(y1,y,stkin)

                    # Convolution
                    stkin = a.convol1D(stkin, axis=y1, \
                                       par=par, period=period, \
                                       extend=extend, adir=None, \
                                       kernel=lkernel, equi=equi)

                    # Interpolate to final array
                    stkin = np.interp(yf,y1,stkin) 

                    # Store in array
                    datao[il,:,ix,istk] = stkin

                # Update arrays for good
                ny = nyf
                y = np.copy(yf)
                data = np.copy(datao)

        if xreverse:
            x = x[::-1]
            data = data[:,:,::-1,:]
        if yreverse:
            y = y[::-1]
            data = data[:,::-1,:,:]
        if lreverse:
            l = l[::-1]
            data = data[::-1,:,:,:]

        if update:
            self.update(x=x,y=y,lamb=lamb)

        return x,y,lamb,data


######################################################################
######################################################################

    def update(self, x=None, y=None, lamb=None):
        ''' Updates the header with new axis and dimensions
        '''

        if x is not None:
            self.__x = np.copy(x)
            self.__nx = x.size

        if y is not None:
            self.__y = np.copy(y)
            self.__ny = y.size

        if x is not None:
            self.__l = np.copy(lamb)
            self.__nl = lamb.size

