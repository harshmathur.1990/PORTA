# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# psp_plotprofile.py                                                 #
#                                                                    #
# Tanausú del Pino Alemán                                            #
# Ángel de Vicente                                                   #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Module for plotting psp profile data                               #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  02/09/2020 - V1.0.9 - Bugfix: The axis formatter using pltxformat #
#                        and pltyformat was not working properly     #
#                        when having more than one plot. Removed     #
#                        those and implemented the formatter with    #
#                        lambda functions (AdV)                      #
#                                                                    #
#  18/05/2020 - V1.0.8 - Bugfix: The size of the sliders were not    #
#                        being updated when changing the plot or the #
#                        plotted file (TdPA)                         #
#                                                                    #
#  10/03/2020 - V1.0.7 - Added colors to indicate focus (TdPA)       #
#                      - Solved focus problems (AdV)                 #
#                                                                    #
#  20/08/2019 - V1.0.6 - Compatibility with old libraries regarding  #
#                        NaviationToolbar (TdPA)                     #
#                                                                    #
#  20/05/2019 - V1.0.5 - NavigationToolbar2TkAgg has been deprecated #
#                        since v 2.2 of Matplotlib. Moving back to   #
#                        NavigationToolbar2Tk (AdV)                  #
#                                                                    #
#  22/04/2019 - V1.0.4 - NavigationToolbar2Tk has been deprecated in #
#                        favor of NavigationToolbar2TkAgg. (TdPA)    #
#                                                                    #
#  22/05/2018 - V1.0.3 - Bugfix: When the X axis were different      #
#                        between several curves, the final plot used #
#                        the axis of the first loaded file. Now it   #
#                        interpolates to the common axis before      #
#                        plotting. (TdPA)                            #
#                                                                    #
#  24/04/2018 - V1.0.2 - Compatibility with python 3+. (TdPA)        #
#                                                                    #
#  22/03/2018 - V1.0.1 - Bugfix: Make sure figures are destroyed to  #
#                        avoid memory overflow when using draw many  #
#                        times. (TdPA)                               #
#                      - Bugfix: It was necessary to do deepcopies   #
#                        when appending during the addition of new   #
#                        entries, at least for the dictionaries. Did #
#                        for all of them anyways. (TdPA)             #
#                      - Bugfix: Wrong key in method for fit button  #
#                        for the plotting magnitude limits. (TdPA)   #
#                                                                    #
#  21/03/2018 - V1.0.0 - First Version. (TdPA)                       #
#                                                                    #
######################################################################
######################################################################
######################################################################

import source.general
from source.loader import *
from source.tooltip import *
from source.colors import *

######################################################################
######################################################################


class PSP_plotprof_class(Toplevel):
    ''' Class that defines the support window for the plot
        configuration
    '''

######################################################################
######################################################################

    def __init__(self, parent, title=None):
        ''' Initialization of the PSP_plot_class class.
        '''

        global CONF
        CONF = source.general.CONF

        # Initialized a support window.
        Toplevel.__init__(self, parent)
        self.transient(parent)

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Set the title if any
        if title:
            self.title(title)

        # Active border
        self.configure(background=CONF['col_act'])

        self.parent = parent
        self.result = None
        self.plt_action = None
        head = Frame(self)
        self.head = head
        body = Frame(head)
        self.up = Frame(body)
        self.do = Frame(body)
        self.head = head
        self.box = body
        self.initial_focus = self.body(body)
        head.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        body.pack(fill=BOTH,expand=1)

        self.grab_set()
        self.lift()
        
        if not self.initial_focus:
            self.initial_focus = self

        # Configuration widgets
        self.optionbox(self.up)

        # Action buttons
        self.buttons(self.do)

        self.up.pack(fill=BOTH,expand=1,side=TOP)
        self.do.pack(fill=BOTH,expand=1,side=BOTTOM)
        body.pack(fill=BOTH,expand=1)
        head.pack(fill=BOTH,expand=1)

        self.protocol("WM_DELETE_WINDOW", self.close)
        self.bind("<Escape>", self.close)
       #self.bind("<Return>", self.update_entry)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)

######################################################################
######################################################################

    def body(self, master):
        ''' Creates the dialog body and set initial focus.
        '''

        return master

######################################################################
######################################################################

    def close(self, event=None):
        ''' Method that handles the window closing.
        '''

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

######################################################################
######################################################################
######################################################################
######################################################################

    def optionbox(self, box):
        ''' Defines all the control widgets of PSP_plotprof_class
            class.
        '''

        global PSP
        PSP = source.general.PSP
        global PLOT
        PLOT = source.general.PLOT
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global VARS
        VARS = source.general.VARS

        # Sanity check
        if PLOTPROF['iplt'] > PLOTPROF['nplt']:
            PLOTPROF['iplt'] = PLOTPROF['nplt']
        iplt = PLOTPROF['iplt'] - 1

        if PLOTPROF['ifile'] > PLOTPROF['nfile']:
            PLOTPROF['ifile'] = PLOTPROF['nfile']
        ifile = PLOTPROF['ifile'][iplt] - 1

        self.XX = None
        adata = []
        ii = 0
        # Available data
        for head,data in zip(PSP['head'],PSP['data']):
            if head['loaded']:
                if head['manywav']:
                    ii += 1
                    adata.append(head['nameshort'])
                   #self.XX.append(data['l'])
                    if self.XX is None:
                        self.XX = data['l']
                    else:
                        np.append(self.XX, data['l'])

        self.XX = np.sort(self.XX)
        self.NX = len(self.XX)

        idata = PLOTPROF['idata'][iplt][ifile] - 1
        while not PSP['head'][idata]['loaded']:
            idata += 1

        # Initialize actual plot limits
        if PLOTPROF['min'] is None:
            PLOTPROF['min'] = [[{}]]
            PLOTPROF['max'] = [[{}]]
            PLOTPROF['loc'] = [[{}]]
            for key in (list(VARS['axis_dic'].keys()) + \
                        VARS['SP_list']):
                PLOTPROF['min'][0][0][key] = str(PLOT['min0'][key])
                PLOTPROF['max'][0][0][key] = str(PLOT['max0'][key])
                PLOTPROF['loc'][0][0][key] = str(PLOT['min0'][key])

        # Initialize spatial indexes
        if PLOTPROF['Mind'][0][0]['x'] is None:
            for key in VARS['axis_dic'].keys():
                num = float(PLOTPROF['max'][0][0][key])
                ind = (np.abs(PSP['data'][idata][key]-num)).argmin()
                PLOTPROF['Mind'][0][0][key] = ind

        # Sanity check indexes
        if PLOTPROF['Mind'][iplt][ifile]['l'] >= self.NX:
            PLOTPROF['Mind'][iplt][ifile]['l'] = self.NX - 1
            PLOTPROF['max'][iplt][ifile]['l'] = self.XX[self.NX - 1]
        if PLOTPROF['Mind'][iplt][ifile]['x'] >= \
           PSP['head'][idata]['nodes'][0]:
            PLOTPROF['Mind'][iplt][ifile]['x'] = \
                                    PSP['head'][idata]['nodes'][0] - 1
            PLOTPROF['max'][iplt][ifile]['x'] = \
                                   PSP['data'][idata]['x'][ \
                                   PLOTPROF['Mind'][iplt][ifile]['x']]
        if PLOTPROF['Mind'][iplt][ifile]['y'] >= \
           PSP['head'][idata]['nodes'][1]:
            PLOTPROF['Mind'][iplt][ifile]['y'] = \
                                    PSP['head'][idata]['nodes'][1] - 1
            PLOTPROF['max'][iplt][ifile]['y'] = \
                                   PSP['data'][idata]['y'][ \
                                   PLOTPROF['Mind'][iplt][ifile]['y']]
        if PLOTPROF['mind'][iplt][ifile]['l'] >= self.NX:
            PLOTPROF['mind'][iplt][ifile]['l'] = self.NX - 1
            PLOTPROF['min'][iplt][ifile]['l'] = self.XX[self.NX - 1]
        if PLOTPROF['mind'][iplt][ifile]['x'] >= \
           PSP['head'][idata]['nodes'][0]:
            PLOTPROF['mind'][iplt][ifile]['x'] = \
                                    PSP['head'][idata]['nodes'][0] - 1
            PLOTPROF['min'][iplt][ifile]['x'] = \
                                   PSP['data'][idata]['x'][ \
                                   PLOTPROF['mind'][iplt][ifile]['x']]
        if PLOTPROF['mind'][iplt][ifile]['y'] >= \
           PSP['head'][idata]['nodes'][1]:
            PLOTPROF['mind'][iplt][ifile]['y'] = \
                                    PSP['head'][idata]['nodes'][1] - 1
            PLOTPROF['min'][iplt][ifile]['y'] = \
                                   PSP['data'][idata]['y'][ \
                                   PLOTPROF['mind'][iplt][ifile]['y']]
        if PLOTPROF['sind'][iplt][ifile]['x'] >= \
           PSP['head'][idata]['nodes'][0]:
            PLOTPROF['sind'][iplt][ifile]['x'] = \
                                    PSP['head'][idata]['nodes'][0] - 1
            PLOTPROF['loc'][iplt][ifile]['x'] = \
                                   PSP['data'][idata]['x'][ \
                                   PLOTPROF['sind'][iplt][ifile]['x']]
        if PLOTPROF['sind'][iplt][ifile]['y'] >= \
           PSP['head'][idata]['nodes'][1]:
            PLOTPROF['sind'][iplt][ifile]['y'] = \
                                    PSP['head'][idata]['nodes'][1] - 1
            PLOTPROF['loc'][iplt][ifile]['y'] = \
                                   PSP['data'][idata]['y'][ \
                                   PLOTPROF['sind'][iplt][ifile]['y']]


        # Widget padding
        padx = 5
        pady = padx
        ipadx = 3
        ipady = ipadx


        # Number of plots
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l0 = Label(box0, text="Plot Selection",justify=CENTER, \
                     font=self.cFont, anchor=CENTER)
        b0l0.grid(row=row,column=col,columnspan=5,sticky=NSEW)
        # Label
        row += 1
        col = 0
        self.nplt0_val = IntVar()
        self.nplt0_val.set(PLOTPROF['nplt'])
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l1 = Label(box0, text="Number of plots:",justify=RIGHT, \
                     font=self.cFont, anchor=E)
        b0l1.grid(row=row,column=col,sticky=NSEW)
        col += 1
        # Number display
        Grid.columnconfigure(box0,col,weight=1)
        self.nplt0_label = Label(box0, \
                                 text="{0}".format(PLOTPROF['nplt']),\
                                 font=self.cFont)
        createToolTip(self.nplt0_label,TOOLTIPS['12'])
        self.nplt0_label.grid(row=row,column=col,sticky=NSEW)
        # Minus button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.nplt0_bmin = Button(box0, text="-", font=self.cFont, \
                                 command=self.min0)
        createToolTip(self.nplt0_bmin,TOOLTIPS['-'])
        self.nplt0_bmin.grid(row=row,column=col,sticky=NSEW)
        # Plus button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.nplt0_bplu = Button(box0, text="+", font=self.cFont, \
                                 command=self.plu0)
        createToolTip(self.nplt0_bplu,TOOLTIPS['+'])
        self.nplt0_bplu.grid(row=row,column=col,sticky=NSEW)
        # Plot to edit
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.wplt0_ind = StringVar()
        self.wplt0_ind.set("Plot {0}".format(PLOTPROF['iplt']))
        lst = []
        for ii in range(1,PLOTPROF['nplt']+1):
            lst.append("Plot {0}".format(ii))
        self.wplt0_optm = OptionMenu(box0, self.wplt0_ind, \
                                     *lst, command=self.set0)
        createToolTip(self.wplt0_optm,TOOLTIPS['13'])
        self.wplt0_optm.config(font=self.cFont)
        self.wplt0_optm.nametowidget(self.wplt0_optm.menuname). \
                                          config(font=self.cFont) 
        self.wplt0_optm.grid(row=row,column=col,sticky=NSEW)
        # Pack box0
        box0.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)



        # Files
        box1 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box1,row,weight=1)
        Grid.columnconfigure(box1,col,weight=1)
        b1l0 = Label(box1, text="File Selection",justify=CENTER, \
                     font=self.cFont, anchor=CENTER)
        b1l0.grid(row=row,column=col,columnspan=5,sticky=NSEW)
        # Label
        row += 1
        col = 0
        self.nfil1_val = IntVar()
        self.nfil1_val.set(PLOTPROF['nfile'][iplt])
        Grid.rowconfigure(box1,row,weight=1)
        Grid.columnconfigure(box1,col,weight=1)
        b1l1 = Label(box1, text="Number of curves:",justify=RIGHT, \
                     font=self.cFont, anchor=E)
        b1l1.grid(row=row,column=col,sticky=NSEW)
        col += 1
        # Number display
        Grid.columnconfigure(box1,col,weight=1)
        self.nfil1_label = Label(box1, \
                         text="{0}".format(PLOTPROF['nfile'][iplt]), \
                                 font=self.cFont)
        createToolTip(self.nfil1_label,TOOLTIPS['35'])
        self.nfil1_label.grid(row=row,column=col,sticky=NSEW)
        # Minus button
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.nfil1_bmin = Button(box1, text="-", font=self.cFont, \
                                 command=self.min1)
        createToolTip(self.nfil1_bmin,TOOLTIPS['-'])
        self.nfil1_bmin.grid(row=row,column=col,sticky=NSEW)
        # Plus button
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.nfil1_bplu = Button(box1, text="+", font=self.cFont, \
                                 command=self.plu1)
        createToolTip(self.nfil1_bplu,TOOLTIPS['+'])
        self.nfil1_bplu.grid(row=row,column=col,sticky=NSEW)
        # Curve to edit
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.wfil1_ind = StringVar()
        self.wfil1_ind.set("Cruve {0}".format( \
                           PLOTPROF['ifile'][iplt]))
        lst = []
        for ii in range(1,PLOTPROF['nfile'][iplt]+1):
            lst.append("Curve {0}".format(ii))
        self.wfil1_optm = OptionMenu(box1, self.wfil1_ind, \
                                     *lst, command=self.set10)
        createToolTip(self.wfil1_optm,TOOLTIPS['36'])
        self.wfil1_optm.config(font=self.cFont)
        self.wfil1_optm.nametowidget(self.wfil1_optm.menuname). \
                                          config(font=self.cFont) 
        self.wfil1_optm.grid(row=row,column=col,sticky=NSEW)
        # File selection
        row += 1
        col = 0
        Grid.rowconfigure(box1,row,weight=1)
        Grid.columnconfigure(box1,col,weight=1)
        self.wdat1_ind = StringVar()
        self.wdat1_ind.set(adata[idata])
        lst = []
        for ii in range(1,len(adata)+1):
            lst.append('{0}: {1}'.format(ii,adata[ii-1]))
        self.wdat1_optm = OptionMenu(box1, self.wdat1_ind, \
                                     *lst, command=self.set11)
        createToolTip(self.wdat1_optm,TOOLTIPS['17'])
        self.wdat1_optm.config(font=self.cFont)
        self.wdat1_optm.nametowidget(self.wdat1_optm.menuname). \
                                          config(font=self.cFont) 
        self.wdat1_optm.grid(row=row,column=col, \
                             columnspan=5,sticky=NSEW)
        # Pack box1
        box1.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)



        # Variable to plot
        box2 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box2,row,weight=1)
        Grid.columnconfigure(box2,col,weight=1)
        b2l = Label(box2, text="Variable Selection",justify=CENTER, \
                     font=self.cFont, anchor=CENTER)
        b2l.grid(row=row,column=col,columnspan=6,sticky=NSEW)
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box2,row,weight=1)
        Grid.columnconfigure(box2,col,weight=1)
        b2l0 = Label(box2, text="Variable to plot:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        b2l0.grid(row=row,column=col,sticky=NSEW)
        # Variable selection
        col += 1
        axis = PLOTPROF['axis'][iplt]
        Grid.columnconfigure(box2,col,weight=1)
        self.wvar2_ind = StringVar()
        self.wvar2_ind.set(axis)
        lst = VARS['SP_list']
        self.wvar2_optm = OptionMenu(box2, self.wvar2_ind, \
                                     *lst, command=self.set2)
        createToolTip(self.wvar2_optm,TOOLTIPS['22'])
        self.wvar2_optm.config(font=self.cFont)
        self.wvar2_optm.nametowidget(self.wvar2_optm.menuname). \
                                          config(font=self.cFont) 
        self.wvar2_optm.grid(row=row,column=col,sticky=NSEW)
        # Minimum
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box2,row,weight=1)
        Grid.columnconfigure(box2,col,weight=1)
        self.b2l1 = Label(box2, text="Minimum:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b2l1.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box2,col,weight=1)
        self.min2_name = StringVar()
        self.min2_name.set(PLOTPROF['min'][iplt][0][axis])
        self.min2_name.trace('w',self.min2_val)
        self.min2_entry = Entry(box2, font=self.cFont, \
                                textvariable=self.min2_name)
        self.min2_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box2,col,weight=1)
        self.auni21_ind = StringVar()
        self.auni21_ind.set("{0}".format(VARS['aunit_dic'][axis]))
        self.auni21_label = Label(box2, \
                                  text=self.auni21_ind.get(), \
                                  font=self.cFont)
        self.auni21_label.grid(row=row,column=col,sticky=NSEW)
        # Maximum
        # Label
        col += 1
        Grid.columnconfigure(box2,col,weight=1)
        self.b2l2 = Label(box2, text="Maximum:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b2l2.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box2,col,weight=1)
        self.max2_name = StringVar()
        self.max2_name.set(PLOTPROF['max'][iplt][0][axis])
        self.max2_name.trace('w',self.max2_val)
        self.max2_entry = Entry(box2, font=self.cFont, \
                                textvariable=self.max2_name)
        self.max2_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box2,col,weight=1)
        self.auni22_ind = StringVar()
        self.auni22_ind.set("{0}".format(VARS['aunit_dic'][axis]))
        self.auni22_label = Label(box2, \
                                  text=self.auni22_ind.get(), \
                                  font=self.cFont)
        self.auni22_label.grid(row=row,column=col,sticky=NSEW)
        # Reset button
        row += 1
        col = 0
        Grid.columnconfigure(box2,col,weight=1)
        self.lim2_bres = Button(box2, text="Reset", font=self.cFont, \
                                 command=self.res2)
        createToolTip(self.lim2_bres,TOOLTIPS['31'])
        self.lim2_bres.grid(row=row,column=col,columnspan=3, \
                            sticky=NSEW)
        col += 2
        # Fit button
        col += 1
        Grid.columnconfigure(box2,col,weight=1)
        self.lim2_bfit = Button(box2, text="Fit", font=self.cFont, \
                                 command=self.fit2)
        createToolTip(self.lim2_bfit,TOOLTIPS['32'])
        self.lim2_bfit.grid(row=row,column=col,columnspan=3, \
                            sticky=NSEW)
        # Pack box2
        box2.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)


        # Wavelength
        box3 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box3,row,weight=1)
        Grid.columnconfigure(box3,col,weight=1)
        b3l = Label(box3, text="Wavelength limits", \
                    justify=CENTER,font=self.cFont, anchor=CENTER)
        b3l.grid(row=row,column=col,columnspan=8,sticky=NSEW)
        # Minimum lambda
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box3,row,weight=1)
        Grid.columnconfigure(box3,col,weight=1)
        self.xmil3_name = StringVar()
        self.xmil3_name.set(u'Minimum \u03BB:')
        self.b3l0 = Label(box3, text=self.xmil3_name.get(), \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b3l0.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.xmin3_name = StringVar()
        self.xmin3_name.set(PLOTPROF['min'][iplt][0]['l'])
        self.xmin3_name.trace('w',self.xmin3_val)
        self.xmin3_entry = Entry(box3, font=self.cFont, \
                                textvariable=self.xmin3_name)
        self.xmin3_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.xmiu3_label = Label(box3,text='nm', \
                                  font=self.cFont, anchor=W)
        self.xmiu3_label.grid(row=row,column=col,sticky=NSEW)
        # Index label
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.aind31_name = StringVar()
        self.aind31_name.set('Index {0}'.format( \
                             PLOTPROF['mind'][iplt][0]['l']+1))
        self.aind31_label = Label(box3,text=self.aind31_name.get(), \
                                  font=self.cFont)
        self.aind31_label.grid(row=row,column=col,sticky=NSEW)
        # Maximum x
        # Label
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.xmal3_name = StringVar()
        self.xmal3_name.set(u'Maximum \u03BB:')
        self.b3l1 = Label(box3, text=self.xmal3_name.get(), \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b3l1.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.xmax3_name = StringVar()
        self.xmax3_name.set(PLOTPROF['max'][iplt][0]['l'])
        self.xmax3_name.trace('w',self.xmax3_val)
        self.xmax3_entry = Entry(box3, font=self.cFont, \
                                textvariable=self.xmax3_name)
        self.xmax3_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.xmau3_label = Label(box3,text='nm', \
                                 font=self.cFont, anchor=W)
        self.xmau3_label.grid(row=row,column=col,sticky=NSEW)
        # Index label
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.aind32_name = StringVar()
        self.aind32_name.set('Index {0}'.format( \
                             PLOTPROF['Mind'][iplt][0]['l']+1))
        self.aind32_label = Label(box3,text=self.aind32_name.get(), \
                                  font=self.cFont)
        self.aind32_label.grid(row=row,column=col,sticky=NSEW)
        # Slider minimum
        row += 1
        col = 1
        Grid.rowconfigure(box3,row,weight=1)
        Grid.columnconfigure(box3,col,weight=1)
        self.xmi3_ind = IntVar()
        self.xmi3_ind.set(PLOTPROF['mind'][iplt][0]['l'])
        self.xmi3_slide = Scale(box3, from_=0, \
                            to=self.NX - 1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.xmi3_ind, \
                            font=self.cFont, \
                            command=self.set31)
        self.xmi3_slide.grid(row=row,column=col,sticky=NSEW)
        # Slider maximum
        col += 4
        Grid.columnconfigure(box3,col,weight=1)
        self.xma3_ind = IntVar()
        self.xma3_ind.set(PLOTPROF['Mind'][iplt][0]['l'])
        self.xma3_slide = Scale(box3, from_=0, \
                            to=self.NX - 1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.xma3_ind, \
                            font=self.cFont, \
                            command=self.set32)
        self.xma3_slide.grid(row=row,column=col,sticky=NSEW)
        # Pack box3
        box3.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)


        # Space
        box4 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box4,row,weight=1)
        Grid.columnconfigure(box4,col,weight=1)
        b4l = Label(box4, text="Spatial Range Selection", \
                    justify=CENTER,font=self.cFont, anchor=CENTER)
        b4l.grid(row=row,column=col,columnspan=8,sticky=NSEW)
        # Get single var
        single = PLOTPROF['single'][iplt][ifile]
        # Minimum x
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box4,row,weight=1)
        Grid.columnconfigure(box4,col,weight=1)
        self.xmil4_name = StringVar()
        if single:
            self.xmil4_name.set("Position {0}:".format( \
                                PSP['head'][idata]['axis_1_name']))
        else:
            self.xmil4_name.set("Minimum {0}:".format( \
                                PSP['head'][idata]['axis_1_name']))
        self.b4l0 = Label(box4, text=self.xmil4_name.get(), \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b4l0.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.xmin4_name = StringVar()
        if single:
            self.xmin4_name.set(PLOTPROF['loc'][iplt][ifile]['x'])
        else:
            self.xmin4_name.set(PLOTPROF['min'][iplt][ifile]['x'])
        self.xmin4_name.trace('w',self.xmin4_val)
        self.xmin4_entry = Entry(box4, font=self.cFont, \
                                textvariable=self.xmin4_name)
        self.xmin4_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.xmiu4_label = Label(box4,text='Mm', \
                                  font=self.cFont, anchor=W)
        self.xmiu4_label.grid(row=row,column=col,sticky=NSEW)
        # Index label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.aind41_name = StringVar()
        if single:
            self.aind41_name.set('Index {0}'.format( \
                               PLOTPROF['sind'][iplt][ifile]['x']+1))
        else:
            self.aind41_name.set('Index {0}'.format( \
                               PLOTPROF['mind'][iplt][ifile]['x']+1))
        self.aind41_label = Label(box4,text=self.aind41_name.get(), \
                                  font=self.cFont)
        self.aind41_label.grid(row=row,column=col,sticky=NSEW)
        # Maximum x
        # Label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.xmal4_name = StringVar()
        self.xmal4_name.set("Maximum {0}:".format( \
                            PSP['head'][idata]['axis_1_name']))
        self.b4l1 = Label(box4, text=self.xmal4_name.get(), \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b4l1.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.xmax4_name = StringVar()
        self.xmax4_name.set(PLOTPROF['max'][iplt][ifile]['x'])
        self.xmax4_name.trace('w',self.xmax4_val)
        self.xmax4_entry = Entry(box4, font=self.cFont, \
                                textvariable=self.xmax4_name)
        self.xmax4_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.xmau4_label = Label(box4,text='Mm', \
                                 font=self.cFont, anchor=W)
        self.xmau4_label.grid(row=row,column=col,sticky=NSEW)
        # Index label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.aind42_name = StringVar()
        self.aind42_name.set('Index {0}'.format( \
                             PLOTPROF['Mind'][iplt][ifile]['x']+1))
        self.aind42_label = Label(box4,text=self.aind42_name.get(), \
                                  font=self.cFont)
        self.aind42_label.grid(row=row,column=col,sticky=NSEW)
        # Slider minimum
        row += 1
        col = 1
        Grid.rowconfigure(box4,row,weight=1)
        Grid.columnconfigure(box4,col,weight=1)
        self.xmi4_ind = IntVar()
        if single:
            self.xmi4_ind.set(PLOTPROF['sind'][iplt][ifile]['x'])
        else:
            self.xmi4_ind.set(PLOTPROF['mind'][iplt][ifile]['x'])
        self.xmi4_slide = Scale(box4, from_=0, \
                            to=PSP['head'][idata]['nodes'][0]-1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.xmi4_ind, \
                            font=self.cFont, \
                            command=self.set41)
        self.xmi4_slide.grid(row=row,column=col,sticky=NSEW)
        # Slider maximum
        col += 4
        Grid.columnconfigure(box4,col,weight=1)
        self.xma4_ind = IntVar()
        self.xma4_ind.set(PLOTPROF['Mind'][iplt][ifile]['x'])
        self.xma4_slide = Scale(box4, from_=0, \
                            to=PSP['head'][idata]['nodes'][0]-1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.xma4_ind, \
                            font=self.cFont, \
                            command=self.set42)
        self.xma4_slide.grid(row=row,column=col,sticky=NSEW)
        # Minimum y
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box4,row,weight=1)
        Grid.columnconfigure(box4,col,weight=1)
        self.ymil4_name = StringVar()
        if single:
            self.ymil4_name.set("Position {0}:".format( \
                            PSP['head'][idata]['axis_2_name']))
        else:
            self.ymil4_name.set("Minimum {0}:".format( \
                            PSP['head'][idata]['axis_2_name']))
        self.b4l3 = Label(box4, text=self.ymil4_name.get(), \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b4l3.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.ymin4_name = StringVar()
        if single:
            self.ymin4_name.set(PLOTPROF['loc'][iplt][ifile]['y'])
        else:
            self.ymin4_name.set(PLOTPROF['min'][iplt][ifile]['y'])
        self.ymin4_name.trace('w',self.ymin4_val)
        self.ymin4_entry = Entry(box4, font=self.cFont, \
                                textvariable=self.ymin4_name)
        self.ymin4_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.ymiu4_label = Label(box4,text='Mm', \
                                  font=self.cFont, anchor=W)
        self.ymiu4_label.grid(row=row,column=col,sticky=NSEW)
        # Index label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.aind43_name = StringVar()
        if single:
            self.aind43_name.set('Index {0}'.format( \
                             PLOTPROF['sind'][iplt][ifile]['y']+1))
        else:
            self.aind43_name.set('Index {0}'.format( \
                             PLOTPROF['mind'][iplt][ifile]['y']+1))
        self.aind43_label = Label(box4,text=self.aind43_name.get(), \
                                  font=self.cFont)
        self.aind43_label.grid(row=row,column=col,sticky=NSEW)
        # Maximum x
        # Label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.ymal4_name = StringVar()
        self.ymal4_name.set("Maximum {0}:".format( \
                            PSP['head'][idata]['axis_2_name']))
        self.b4l4 = Label(box4, text=self.ymal4_name.get(), \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b4l4.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.ymax4_name = StringVar()
        self.ymax4_name.set(PLOTPROF['max'][iplt][ifile]['y'])
        self.ymax4_name.trace('w',self.ymax4_val)
        self.ymax4_entry = Entry(box4, font=self.cFont, \
                                textvariable=self.ymax4_name)
        self.ymax4_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.ymau4_label = Label(box4,text='Mm', \
                                  font=self.cFont, anchor=W)
        self.ymau4_label.grid(row=row,column=col,sticky=NSEW)
        # Index label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.aind44_name = StringVar()
        self.aind44_name.set('Index {0}'.format( \
                             PLOTPROF['Mind'][iplt][ifile]['y']+1))
        self.aind44_label = Label(box4,text=self.aind44_name.get(), \
                                  font=self.cFont)
        self.aind44_label.grid(row=row,column=col,sticky=NSEW)
        # Slider minimum
        row += 1
        col = 1
        Grid.rowconfigure(box4,row,weight=1)
        Grid.columnconfigure(box4,col,weight=1)
        self.ymi4_ind = IntVar()
        if single:
            self.ymi4_ind.set(PLOTPROF['sind'][iplt][ifile]['y'])
        else:
            self.ymi4_ind.set(PLOTPROF['mind'][iplt][ifile]['y'])
        self.ymi4_slide = Scale(box4, from_=0, \
                            to=PSP['head'][idata]['nodes'][1]-1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.ymi4_ind, \
                            font=self.cFont, \
                            command=self.set43)
        self.ymi4_slide.grid(row=row,column=col,sticky=NSEW)
        # Slider maximum
        col += 4
        Grid.columnconfigure(box4,col,weight=1)
        self.yma4_ind = IntVar()
        self.yma4_ind.set(PLOTPROF['Mind'][iplt][ifile]['y'])
        self.yma4_slide = Scale(box4, from_=0, \
                            to=PSP['head'][idata]['nodes'][1]-1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.yma4_ind, \
                            font=self.cFont, \
                            command=self.set44)
        self.yma4_slide.grid(row=row,column=col,sticky=NSEW)
        # Check for single point
        row += 1
        col = 0
        Grid.rowconfigure(box4,row,weight=1)
        Grid.columnconfigure(box4,col,weight=1)
        self.sng4_stat = IntVar()
        if single:
            self.sng4_stat.set(1)
        else:
            self.sng4_stat.set(0)
        self.sng4_check = Checkbutton(box4, text="Single", \
                                      font=self.cFont, \
                                      variable=self.sng4_stat, \
                                      command=self.check4)
        createToolTip(self.sng4_check,text=TOOLTIPS['34'])
        self.sng4_check.grid(row=row,column=col,columnspan=8, \
                             sticky=NSEW)
        # Disable widgets
        if single:
            self.b4l1.config(state='disabled')
            self.xmax4_entry.config(state='disabled')
            self.xmau4_label.config(state='disabled')
            self.xma4_slide.config(state='disabled')
            self.b4l4.config(state='disabled')
            self.ymax4_entry.config(state='disabled')
            self.ymau4_label.config(state='disabled')
            self.yma4_slide.config(state='disabled')
        else:
            self.b4l1.config(state='normal')
            self.xmax4_entry.config(state='normal')
            self.xmau4_label.config(state='normal')
            self.xma4_slide.config(state='normal')
            self.b4l4.config(state='normal')
            self.ymax4_entry.config(state='normal')
            self.ymau4_label.config(state='normal')
            self.yma4_slide.config(state='normal')
        # Pack box4
        box4.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)


        # Colormap and linestyle
        box5 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box5,row,weight=1)
        Grid.columnconfigure(box5,col,weight=1)
        b5l = Label(box5, text="Color and Style Selection", \
                    justify=CENTER,font=self.cFont, anchor=CENTER)
        b5l.grid(row=row,column=col,columnspan=2,sticky=NSEW)
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box5,row,weight=1)
        Grid.columnconfigure(box5,col,weight=1)
        b5l0 = Label(box5, text="Colormap:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        b5l0.grid(row=row,column=col,sticky=NSEW)
        # Name
        col += 1
        Grid.columnconfigure(box5,col,weight=1)
        self.wcol5_name = StringVar()
        self.wcol5_name.set( \
                  VARS['lcolor_name'][PLOTPROF['color'][iplt][ifile]])
        self.wcol5_lab = Label(box5, text=self.wcol5_name.get(), \
                              justify=RIGHT,font=self.cFont, anchor=W)
        self.wcol5_lab.grid(row=row,column=col,sticky=NSEW)
        # Variable selection slider
        row += 1
        col = 0
        Grid.rowconfigure(box5,row,weight=1)
        Grid.columnconfigure(box5,col,weight=1)
        self.wcol5_ind = IntVar()
        self.wcol5_ind.set(PLOTPROF['color'][iplt][ifile])
        self.wcol5_slide = Scale(box5, from_=0, \
                            to=len(VARS['lcolor_name'])-1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.wcol5_ind, \
                            command=self.set51)
        self.wcol5_slide.grid(row=row,column=col,columnspan=2, \
                              sticky=NSEW)
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box5,row,weight=1)
        Grid.columnconfigure(box5,col,weight=1)
        b5l1 = Label(box5, text="Line style:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        b5l1.grid(row=row,column=col,sticky=NSEW)
        # Name
        col += 1
        Grid.columnconfigure(box5,col,weight=1)
        self.wlin5_name = StringVar()
        self.wlin5_name.set( \
                   VARS['llsty_name'][PLOTPROF['line'][iplt][ifile]])
        self.wlin5_lab = Label(box5, text=self.wlin5_name.get(), \
                              justify=RIGHT,font=self.cFont, anchor=W)
        self.wlin5_lab.grid(row=row,column=col,sticky=NSEW)
        # Variable selection slider
        row += 1
        col = 0
        Grid.rowconfigure(box5,row,weight=1)
        Grid.columnconfigure(box5,col,weight=1)
        self.wlin5_ind = IntVar()
        self.wlin5_ind.set(PLOTPROF['line'][iplt][ifile])
        self.wlin5_slide = Scale(box5, from_=0, \
                            to=len(VARS['llsty_name'])-1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.wlin5_ind, \
                            command=self.set52)
        self.wlin5_slide.grid(row=row,column=col,columnspan=2, \
                              sticky=NSEW)
        # Pack box5
        box5.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)


        # Axis Format and font size
        box6 = LabelFrame(box)
        row = 0
        col = 0
        # Label title
        Grid.rowconfigure(box6,row,weight=1)
        Grid.columnconfigure(box6,col,weight=1)
        b6l0 = Label(box6, text="Format Axis Selection", \
                          justify=CENTER,font=self.cFont, \
                          anchor=CENTER)
        b6l0.grid(row=row,column=col,columnspan=4,sticky=NSEW)
        # X axis format
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box6,row,weight=1)
        Grid.columnconfigure(box6,col,weight=1)
        b6l2 = Label(box6, text="Horizontal axis format:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        b6l2.grid(row=row,column=col,columnspan=2,sticky=NSEW)
        col += 1
        # Entry
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.xfor6_name = StringVar()
        if PLOTPROF['format'][iplt]['x'] is None:
            self.xfor6_name.set('None')
        else:
            self.xfor6_name.set(PLOTPROF['format'][iplt]['x'])
        self.xfor6_name.trace('w',self.xfor6_val)
        self.xfor6_entry = Entry(box6, font=self.cFont, \
                                 textvariable=self.xfor6_name)
        self.xfor6_entry.grid(row=row,column=col,columnspan=2, \
                              sticky=NSEW)
        # X axis font size
        # Check
        row += 1
        col = 0
        Grid.rowconfigure(box6,row,weight=1)
        Grid.columnconfigure(box6,col,weight=1)
        self.xfs6_stat = IntVar()
        if PLOTPROF['cfont'][iplt]['x']:
            self.xfs6_stat.set(1)
        else:
            self.xfs6_stat.set(0)
        self.xfs6_check = Checkbutton(box6, \
                                      text="Change horizontal " + \
                                           "axis font size:", \
                                      anchor=W, \
                                      font=self.cFont, \
                                      variable=self.xfs6_stat, \
                                      command=self.check61)
        createToolTip(self.xfs6_check,text=TOOLTIPS['27'])
        self.xfs6_check.grid(row=row,column=col,sticky=NSEW)
        # Label
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.xfs6_lab = Label(box6, \
                              text=str(PLOTPROF['sfont'][iplt]['x']),\
                              justify=RIGHT,font=self.cFont, anchor=E)
        self.xfs6_lab.grid(row=row,column=col,sticky=NSEW)
        # Minus button
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.xfs6_bmin = Button(box6, text="-", font=self.cFont, \
                                command=self.min61)
        createToolTip(self.xfs6_bmin,TOOLTIPS['-'])
        self.xfs6_bmin.grid(row=row,column=col,sticky=NSEW)
        # Plus button
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.xfs6_bplu = Button(box6, text="+", font=self.cFont, \
                                command=self.plu61)
        createToolTip(self.xfs6_bplu,TOOLTIPS['+'])
        self.xfs6_bplu.grid(row=row,column=col,sticky=NSEW)
        # Disable change of font
        if PLOTPROF['cfont'][iplt]['x']:
            self.xfs6_lab.config(state='normal')
            self.xfs6_bmin.config(state='normal')
            self.xfs6_bplu.config(state='normal')
        else:
            self.xfs6_lab.config(state='disabled')
            self.xfs6_bmin.config(state='disabled')
            self.xfs6_bplu.config(state='disabled')
        # Y axis format
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box6,row,weight=1)
        Grid.columnconfigure(box6,col,weight=1)
        b6l3 = Label(box6, text="Vertical axis format:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        b6l3.grid(row=row,column=col,columnspan=2,sticky=NSEW)
        col += 1
        # Entry
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.yfor6_name = StringVar()
        if PLOTPROF['format'][iplt]['y'] is None:
            self.yfor6_name.set('None')
        else:
            self.yfor6_name.set(PLOTPROF['format'][iplt]['y'])
        self.yfor6_name.trace('w',self.yfor6_val)
        self.yfor6_entry = Entry(box6, font=self.cFont, \
                                 textvariable=self.yfor6_name)
        self.yfor6_entry.grid(row=row,column=col,columnspan=2, \
                              sticky=NSEW)
        # X axis font size
        # Check
        row += 1
        col = 0
        Grid.rowconfigure(box6,row,weight=1)
        Grid.columnconfigure(box6,col,weight=1)
        self.yfs6_stat = IntVar()
        if PLOTPROF['cfont'][iplt]['y']:
            self.yfs6_stat.set(1)
        else:
            self.yfs6_stat.set(0)
        self.yfs6_check = Checkbutton(box6, \
                                      text="Change vertical " + \
                                           "axis font size:", \
                                      anchor=W, \
                                      font=self.cFont, \
                                      variable=self.yfs6_stat, \
                                      command=self.check62)
        createToolTip(self.yfs6_check,text=TOOLTIPS['27'])
        self.yfs6_check.grid(row=row,column=col,sticky=NSEW)
        # Label
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.yfs6_lab = Label(box6, \
                              text=str(PLOTPROF['sfont'][iplt]['y']),\
                              justify=RIGHT,font=self.cFont, anchor=E)
        self.yfs6_lab.grid(row=row,column=col,sticky=NSEW)
        # Minus button
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.yfs6_bmin = Button(box6, text="-", font=self.cFont, \
                                command=self.min62)
        createToolTip(self.yfs6_bmin,TOOLTIPS['-'])
        self.yfs6_bmin.grid(row=row,column=col,sticky=NSEW)
        # Plus button
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.yfs6_bplu = Button(box6, text="+", font=self.cFont, \
                                command=self.plu62)
        createToolTip(self.yfs6_bplu,TOOLTIPS['+'])
        self.yfs6_bplu.grid(row=row,column=col,sticky=NSEW)
        # Disable change of font
        if PLOTPROF['cfont'][iplt]['y']:
            self.yfs6_lab.config(state='normal')
            self.yfs6_bmin.config(state='normal')
            self.yfs6_bplu.config(state='normal')
        else:
            self.yfs6_lab.config(state='disabled')
            self.yfs6_bmin.config(state='disabled')
            self.yfs6_bplu.config(state='disabled')
        # Pack box6
        box6.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)


######################################################################
######################################################################

    def buttons(self, box):
        ''' Defines the control buttons of the widget
        '''

        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # Save configuration button
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt_draw = Button(box0, text="Save configuration", \
                                font=self.cFont, \
                                command=self.save)
        createToolTip(self.butt_draw,TOOLTIPS['28'])
        self.butt_draw.grid(row=row,column=col,sticky=NSEW)
        # Restore configuration button
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt_draw = Button(box0, \
                                text="Load configuration", \
                                font=self.cFont, \
                                command=self.load)
        createToolTip(self.butt_draw,TOOLTIPS['29'])
        self.butt_draw.grid(row=row,column=col,sticky=NSEW)
        # Reset configuration button
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt_draw = Button(box0, \
                                text="Reset configuration", \
                                font=self.cFont, \
                                command=self.reset)
        createToolTip(self.butt_draw,TOOLTIPS['30'])
        self.butt_draw.grid(row=row,column=col,sticky=NSEW)
        # Plot button
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt_draw = Button(box0, text="Draw", font=self.cFont, \
                                 command=self.draw)
        createToolTip(self.butt_draw,TOOLTIPS['18'])
        self.butt_draw.grid(row=row,column=col,sticky=NSEW)
        # Close button
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt_close = Button(box0, text="Close", \
                                  font=self.cFont, command=self.close)
        createToolTip(self.butt_close,TOOLTIPS['11'])
        self.butt_close.grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)


######################################################################
######################################################################
######################################################################
######################################################################

    def min0(self):
        ''' Reduce in 1 the number of plots
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        if PLOTPROF['nplt'] <= 1:
            return
        PLOTPROF['nplt'] -= 1
        if PLOTPROF['iplt'] > PLOTPROF['nplt']:
            PLOTPROF['iplt'] = PLOTPROF['nplt']
        self.set_top()

        return

######################################################################
######################################################################

    def plu0(self):
        ''' Increse in 1 the number of plots
        '''

        global PLOT
        PLOT = source.general.PLOT
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PLOTPROF_def
        PLOTPROF_def = source.general.PLOTPROF_def

        if PLOTPROF['nplt'] >= 9:
            return

        PLOTPROF['nplt'] += 1

        while len(PLOTPROF['idata']) < PLOTPROF['nplt']:
            PLOTPROF['idata'].append(copy.deepcopy( \
                              PLOTPROF_def['idata'][0]))
            PLOTPROF['nfile'].append(copy.deepcopy( \
                              PLOTPROF_def['nfile'][0]))
            PLOTPROF['ifile'].append(copy.deepcopy( \
                              PLOTPROF_def['ifile'][0]))
            PLOTPROF['axis'].append(copy.deepcopy( \
                              PLOTPROF_def['axis'][0]))
            PLOTPROF['single'].append(copy.deepcopy( \
                              PLOTPROF_def['single'][0]))
            PLOTPROF['color'].append(copy.deepcopy( \
                              PLOTPROF_def['color'][0]))
            PLOTPROF['line'].append(copy.deepcopy( \
                              PLOTPROF_def['color'][0]))
            PLOTPROF['format'].append(copy.deepcopy( \
                              PLOTPROF_def['format'][0]))
            PLOTPROF['cfont'].append(copy.deepcopy( \
                              PLOTPROF_def['cfont'][0]))
            PLOTPROF['sfont'].append(copy.deepcopy( \
                              PLOTPROF_def['sfont'][0]))
            PLOTPROF['sind'].append(copy.deepcopy( \
                              PLOTPROF_def['sind'][0]))
            PLOTPROF['mind'].append(copy.deepcopy( \
                              PLOTPROF_def['mind'][0]))
            PLOTPROF['Mind'].append(copy.deepcopy( \
                              PLOTPROF['Mind'][-1]))
            PLOTPROF['min'].append(copy.deepcopy( \
                              [PLOT['min0']]))
            PLOTPROF['max'].append(copy.deepcopy( \
                              [PLOT['max0']]))
            PLOTPROF['loc'].append(copy.deepcopy( \
                              [PLOT['min0']]))
        self.set_top()

        return

######################################################################
######################################################################

    def set0(self, val):
        ''' Set the index of the plot to be changed
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        self.update_entry()
        PLOTPROF['iplt'] = int(val.split(" ")[1])
        self.set_top()

        return

######################################################################
######################################################################

    def min1(self):
        ''' Reduce in 1 the number of plots
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1

        if PLOTPROF['nfile'][iplt] <= 1:
            return
        PLOTPROF['nfile'][iplt] -= 1
        if PLOTPROF['ifile'][iplt] > PLOTPROF['nfile'][iplt]:
            PLOTPROF['ifile'][iplt] = PLOTPROF['nfile'][iplt]
        self.set_top()

        return

######################################################################
######################################################################

    def plu1(self):
        ''' Increse in 1 the number of curves
        '''

        global PLOT
        PLOT = source.general.PLOT
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PLOTPROF_def
        PLOTPROF_def = source.general.PLOTPROF_def

        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1

        PLOTPROF['nfile'][iplt] += 1

        while len(PLOTPROF['idata'][iplt]) < PLOTPROF['nfile'][iplt]:
            PLOTPROF['idata'][iplt].append(copy.deepcopy( \
                                    PLOTPROF_def['idata'][0][0]))
            PLOTPROF['single'][iplt].append(copy.deepcopy( \
                                    PLOTPROF_def['single'][0][0]))
            PLOTPROF['color'][iplt].append(copy.deepcopy( \
                                    PLOTPROF_def['color'][0][0]))
            PLOTPROF['line'][iplt].append(copy.deepcopy( \
                                    PLOTPROF_def['line'][0][0]))
            PLOTPROF['format'].append(copy.deepcopy( \
                              PLOTPROF_def['format'][0]))
            PLOTPROF['cfont'].append(copy.deepcopy( \
                              PLOTPROF_def['cfont'][0]))
            PLOTPROF['sfont'].append(copy.deepcopy( \
                              PLOTPROF_def['sfont'][0]))
            PLOTPROF['sind'][iplt].append(copy.deepcopy( \
                                   PLOTPROF_def['sind'][0][0]))
            PLOTPROF['mind'][iplt].append(copy.deepcopy( \
                              PLOTPROF_def['mind'][0][0]))
            PLOTPROF['Mind'][iplt].append(copy.deepcopy( \
                              PLOTPROF['Mind'][iplt][-1]))
            PLOTPROF['min'][iplt].append(copy.deepcopy( \
                              PLOT['min0']))
            PLOTPROF['max'][iplt].append(copy.deepcopy( \
                              PLOT['max0']))
            PLOTPROF['loc'][iplt].append(copy.deepcopy( \
                              PLOT['min0']))
        self.set_top()

        return

######################################################################
######################################################################

    def set10(self, val):
        ''' Set the curve to edit
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1

        self.update_entry()
        PLOTPROF['ifile'][iplt] = int(val.split(" ")[1])
        self.set_top()

        return

######################################################################
######################################################################

    def set11(self, val):
        ''' Set the file to plot
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1

        self.update_entry()
        PLOTPROF['idata'][iplt][ifile] = int(val.split(":")[0])
        self.set_top()

        return

######################################################################
######################################################################

    def set2(self, val):
        ''' Set the variable to plot
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1

        self.update_entry()
        PLOTPROF['axis'][iplt] = val
        self.set_top()

        return

######################################################################
######################################################################

    def res2(self):
        ''' Resets the current limits to the default
        '''

        global PLOT
        PLOT = source.general.PLOT
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1
        key = PLOTPROF['axis'][iplt]

        PLOTPROF['min'][iplt][0][key] = PLOT['min0'][key]
        PLOTPROF['max'][iplt][0][key] = PLOT['max0'][key]
        self.set_top()

        return

######################################################################
######################################################################

    def fit2(self):
        ''' Finds and set the limits for the current file and plot
            variable
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PSP
        PSP = source.general.PSP

        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1
        key = PLOTPROF['axis'][iplt]

        miny = 1e99
        maxy = -1e99

        for ifile in range(PLOTPROF['nfile'][iplt]):

            idata = PLOTPROF['idata'][iplt][ifile] - 1

            miny = min(np.amin(PSP['data'][idata][key]),miny)
            maxy = max(np.amax(PSP['data'][idata][key]),maxy)

        PLOTPROF['min'][iplt][0][key] = miny
        PLOTPROF['max'][iplt][0][key] = maxy
        self.set_top()

        return

######################################################################
######################################################################

    def set31(self, val):
        ''' Set the minimum x wavelength index
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PSP
        PSP = source.general.PSP


        iplt = PLOTPROF['iplt'] - 1

        PLOTPROF['mind'][iplt][0]['l'] = int(val)
        PLOTPROF['min'][iplt][0]['l'] = str(self.XX[int(val)])
        self.xmin3_name.set(PLOTPROF['min'][iplt][0]['l'])
        self.update_entry()
        self.set_top()

        return

######################################################################
######################################################################

    def set32(self, val):
        ''' Set the maximum x wavelength index
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PSP
        PSP = source.general.PSP


        iplt = PLOTPROF['iplt'] - 1

        PLOTPROF['Mind'][iplt][0]['l'] = int(val)
        PLOTPROF['max'][iplt][0]['l'] = str(self.XX[int(val)])
        self.xmax3_name.set(PLOTPROF['max'][iplt][0]['l'])
        self.update_entry()
        self.set_top()

        return

######################################################################
######################################################################

    def set41(self, val):
        ''' Set the minimum x index
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PSP
        PSP = source.general.PSP


        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1
        idata = PLOTPROF['idata'][iplt][ifile] - 1

        if PLOTPROF['single'][iplt][ifile]:

            PLOTPROF['sind'][iplt][ifile]['x'] = int(val)
            PLOTPROF['loc'][iplt][ifile]['x'] = \
                                str(PSP['data'][idata]['x'][int(val)])
            self.xmin4_name.set(PLOTPROF['loc'][iplt][ifile]['x'])

        else:

            PLOTPROF['mind'][iplt][ifile]['x'] = int(val)
            PLOTPROF['min'][iplt][ifile]['x'] = \
                                str(PSP['data'][idata]['x'][int(val)])
            self.xmin4_name.set(PLOTPROF['min'][iplt][ifile]['x'])


        self.update_entry()
        self.set_top()

        return

######################################################################
######################################################################

    def set42(self, val):
        ''' Set the maximum x index
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PSP
        PSP = source.general.PSP


        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1
        idata = PLOTPROF['idata'][iplt][ifile] - 1

        PLOTPROF['Mind'][iplt][ifile]['x'] = int(val)
        PLOTPROF['max'][iplt][ifile]['x'] = \
                                str(PSP['data'][idata]['x'][int(val)])
        self.xmax4_name.set(PLOTPROF['max'][iplt][ifile]['x'])

        self.update_entry()
        self.set_top()

        return

######################################################################
######################################################################

    def set43(self, val):
        ''' Set the minimum y index
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PSP
        PSP = source.general.PSP


        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1
        idata = PLOTPROF['idata'][iplt][ifile] - 1

        if PLOTPROF['single'][iplt][ifile]:

            PLOTPROF['sind'][iplt][ifile]['y'] = int(val)
            PLOTPROF['loc'][iplt][ifile]['y'] = \
                                str(PSP['data'][idata]['y'][int(val)])
            self.ymin4_name.set(PLOTPROF['loc'][iplt][ifile]['y'])

        else:

            PLOTPROF['mind'][iplt][ifile]['y'] = int(val)
            PLOTPROF['min'][iplt][ifile]['y'] = \
                                str(PSP['data'][idata]['y'][int(val)])
            self.ymin4_name.set(PLOTPROF['min'][iplt][ifile]['y'])

        self.update_entry()
        self.set_top()

        return

######################################################################
######################################################################

    def set44(self, val):
        ''' Set the maximum y index
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PSP
        PSP = source.general.PSP

        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1
        idata = PLOTPROF['idata'][iplt][ifile] - 1

        PLOTPROF['Mind'][iplt][ifile]['y'] = int(val)
        PLOTPROF['max'][iplt][ifile]['y'] = \
                                str(PSP['data'][idata]['y'][int(val)])
        self.ymax4_name.set(PLOTPROF['max'][iplt][ifile]['y'])
        self.update_entry()
        self.set_top()

        return

######################################################################
######################################################################

    def check4(self):
        ''' Check to get a single location
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1

        PLOTPROF['single'][iplt][ifile] = self.sng4_stat.get() == 1
        self.set_top()

        return

######################################################################
######################################################################

    def set51(self, val):
        ''' Set the color map to plot
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1

        self.update_entry()
        PLOTPROF['color'][iplt][ifile] = int(val)
        self.set_top()

        return

######################################################################
######################################################################

    def set52(self, val):
        ''' Set the linestyle to plot
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1

        self.update_entry()
        PLOTPROF['line'][iplt][ifile] = int(val)
        self.set_top()

        return

######################################################################
######################################################################

    def min61(self):
        ''' Reduce in 1 the horizontal axis font size
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1

        if PLOTPROF['sfont'][iplt]['x'] <= 1:
            return
        PLOTPROF['sfont'][iplt]['x'] -= 1
        self.set_top()

        return

######################################################################
######################################################################

    def plu61(self):
        ''' increase in 1 the horizontal axis font size
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1

        if PLOTPROF['sfont'][iplt]['x'] >= 99:
            return
        PLOTPROF['sfont'][iplt]['x'] += 1
        self.set_top()

        return

######################################################################
######################################################################

    def check61(self):
        ''' Check to change font size of horizontal axis
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1

        PLOTPROF['cfont'][iplt]['x'] = self.xfs6_stat.get() == 1
        self.set_top()

        return

######################################################################
######################################################################

    def min62(self):
        ''' Reduce in 1 the vertical axis font size
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1

        if PLOTPROF['sfont'][iplt]['y'] <= 1:
            return
        PLOTPROF['sfont'][iplt]['y'] -= 1
        self.set_top()

        return

######################################################################
######################################################################

    def plu62(self):
        ''' increase in 1 the vertical axis font size
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1

        if PLOTPROF['sfont'][iplt]['y'] >= 99:
            return
        PLOTPROF['sfont'][iplt]['y'] += 1
        self.set_top()

        return

######################################################################
######################################################################

    def check62(self):
        ''' Check to change font size of vertical axis
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1

        PLOTPROF['cfont'][iplt]['y'] = self.yfs6_stat.get() == 1
        self.set_top()

        return

######################################################################
######################################################################

    def load(self):
        ''' Loads a previously stored GUI configuration
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PATH
        PATH = source.general.PATH

        # Open the dialog pick file
        filein = tkFileDialog.askopenfilename( \
                                         initialdir=PATH['psp_dir'], \
                                         parent=self)

        # Check if you selected something
        if filein:
            PLOTPROF_in = {}
            try:
                with open(filein, "rb") as cache:
                    CACHE = pickle.load(cache)
                    for key in PLOTPROF.keys():
                        PLOTPROF_in[key] = CACHE['PLOTPROF'][key]
                for key in PLOTPROF.keys():
                    PLOTPROF[key] = PLOTPROF_in[key]
                self.redraw()

            except:
                raise
                tkMessageBox.showwarning("Load", \
                                         "Could not load that file", \
                                         parent=self)
                pass

######################################################################
######################################################################

    def save(self):
        ''' Saves the configuration of the GUI
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PATH
        PATH = source.general.PATH

        # Update the entries first
        self.update_entry()
        # Open the dialog pick file
        filein = tkFileDialog.asksaveasfilename( \
                                       initialdir=PATH['psp_dir'], \
                                       parent=self)
        if filein:
            try:
                with open(filein, "wb") as cache:
                   pass
                   CACHE = {'PLOTPROF' : PLOTPROF}
                   pickle.dump(CACHE, cache)
            except IOError:
                pass
            except EOFError:
                pass
            except:
                raise

######################################################################
######################################################################

    def reset(self):
        ''' Reset the GUI
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PLOTPROF_def
        PLOTPROF_def = source.general.PLOTPROF_def

        check = tkMessageBox.askokcancel("Reset", "Are you sure?", \
                                         parent=self)

        if check:
            try:
                for key in PLOTPROF.keys():
                    PLOTPROF[key] = PLOTPROF_def[key]
                self.redraw()
            except:
                raise

######################################################################
######################################################################

    def draw(self):
        ''' Method that handles the button Draw. It calls a support
            window of the PSP_canvas_class class.
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global CONF
        CONF = source.general.CONF

        # Update entries
        self.update_entry()

        # Check if you loaded the data previously
        for nfile,mind,Mind,single,ii in \
            zip(PLOTPROF['nfile'], \
                PLOTPROF['mind'],PLOTPROF['Mind'], \
                PLOTPROF['single'],range(PLOTPROF['nplt'])):
            for jj in range(nfile):
                if not single[jj]:
                    if Mind[jj]['x'] <= mind[jj]['x']:
                        tkMessageBox.showwarning("Plot Profile", \
                               "In plot {0}, ".format(ii+1) + \
                               "and curve {0}, ".format(jj+1) + \
                               "minimum X is larger or " + \
                               "equal than maximum X",parent=self)
                        return
                    if Mind[jj]['y'] <= mind[jj]['y']:
                        tkMessageBox.showwarning("Plot Profile", \
                               "In plot {0}, ".format(ii+1) + \
                               "and curve {0}, ".format(jj+1) + \
                               "minimum Y is larger or " + \
                               "equal than maximum Y",parent=self)
                        return
            if Mind[0]['l'] <= mind[0]['l']:
                tkMessageBox.showwarning("Plot Profile", \
                             "In plot {0}, ".format(ii+1) + \
                             "minimum wavelength is larger or " + \
                             "equal than maximum wavelgnth", \
                             parent=self)
                return

        PLOTPROF['l'] = self.XX

        # Make current passive
        self.config(background=CONF['col_pas'])

        # Calls the support window
        draw = PLOTPROF_canvas_class(self.master, title='Plot')
        self.grab_set()
        self.lift()
        
        PLOTPROF.pop('l')

        # Make current active
        self.configure(background=CONF['col_act'])


######################################################################
######################################################################
######################################################################
######################################################################

    def redraw(self):
        ''' Redraws the whole window
        '''

        self.box.destroy()
        self.box = Frame(self.head)
        self.up = Frame(self.box)
        self.do = Frame(self.box)
        self.optionbox(self.up)
        self.buttons(self.do)
        self.up.pack(fill=BOTH,expand=1,side=TOP)
        self.do.pack(fill=BOTH,expand=1,side=BOTTOM)
        self.box.pack(fill=BOTH,expand=1)

######################################################################
######################################################################

    def min2_val(self, *dumm):
        ''' Validates min2 entry
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1
        axis = PLOTPROF['axis'][iplt]

        val = self.min2_name.get()
        try:
            num = float(val)
            PLOTPROF['min'][iplt][0][axis] = num
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                        self.min2_name.set( \
                                PLOTPROF['min'][iplt][0][axis])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            self.min2_name.set( \
                                    PLOTPROF['min'][iplt][0][axis])
        except:
            raise

######################################################################
######################################################################

    def max2_val(self, *dumm):
        ''' Validates max2 entry
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1
        axis = PLOTPROF['axis'][iplt]

        val = self.max2_name.get()
        try:
            num = float(val)
            PLOTPROF['max'][iplt][0][axis] = num
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                        self.max2_name.set( \
                                PLOTPROF['max'][iplt][0][axis])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            self.max2_name.set( \
                                    PLOTPROF['max'][iplt][0][axis])
        except:
            raise

######################################################################
######################################################################

    def xmin3_val(self, *dumm):
        ''' Validates xmin3 entry
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1

        val = self.xmin3_name.get()
        try:
            num = float(val)
            PLOTPROF['min'][iplt][0]['l'] = val
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                       self.xmin3_name.set( \
                               PLOTPROF['min'][iplt][0]['l'])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            self.xmin3_name.set( \
                                     PLOTPROF['min'][iplt][0]['l'])
        except:
            raise

######################################################################
######################################################################

    def xmax3_val(self, *dumm):
        ''' Validates xmax3 entry
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1

        val = self.xmax3_name.get()
        try:
            num = float(val)
            PLOTPROF['max'][iplt][0]['l'] = val
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                       self.xmax3_name.set( \
                                PLOTPROF['max'][iplt][0]['l'])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            self.xmax3_name.set( \
                                     PLOTPROF['max'][iplt][0]['l'])
        except:
            raise

######################################################################
######################################################################

    def xmin4_val(self, *dumm):
        ''' Validates xmin4 entry
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1
        idata = PLOTPROF['idata'][iplt][ifile] - 1
        single = PLOTPROF['single'][iplt][ifile]

        val = self.xmin4_name.get()
        try:
            num = float(val)
            if single:
                PLOTPROF['loc'][iplt][ifile]['x'] = val
            else:
                PLOTPROF['min'][iplt][ifile]['x'] = val
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                        if single:
                            self.xmin4_name.set( \
                                 PLOTPROF['loc'][iplt][ifile]['x'])
                        else:
                            self.xmin4_name.set( \
                                 PLOTPROF['min'][iplt][ifile]['x'])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            if single:
                                self.xmin4_name.set( \
                                 PLOTPROF['loc'][iplt][ifile]['x'])
                            else:
                                self.xmin4_name.set( \
                                 PLOTPROF['min'][iplt][ifile]['x'])
        except:
            raise

######################################################################
######################################################################

    def xmax4_val(self, *dumm):
        ''' Validates xmax4 entry
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1
        idata = PLOTPROF['idata'][iplt][ifile] - 1

        val = self.xmax4_name.get()
        try:
            num = float(val)
            PLOTPROF['max'][iplt][ifile]['x'] = val
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                        self.xmax4_name.set( \
                             PLOTPROF['max'][iplt][ifile]['x'])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            self.xmax4_name.set( \
                              PLOTPROF['max'][iplt][ifile]['x'])
        except:
            raise

######################################################################
######################################################################

    def ymin4_val(self, *dumm):
        ''' Validates ymin4 entry
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1
        idata = PLOTPROF['idata'][iplt][ifile] - 1
        single = PLOTPROF['single'][iplt][ifile]

        val = self.ymin4_name.get()
        try:
            num = float(val)
            if single:
                PLOTPROF['loc'][iplt][ifile]['y'] = val
            else:
                PLOTPROF['min'][iplt][ifile]['y'] = val
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                        if single:
                            self.ymin4_name.set( \
                                 PLOTPROF['loc'][iplt][ifile]['y'])
                        else:
                            self.ymin4_name.set( \
                                 PLOTPROF['min'][iplt][ifile]['y'])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            if single:
                                self.ymin4_name.set( \
                                 PLOTPROF['loc'][iplt][ifile]['y'])
                            else:
                                self.ymin4_name.set( \
                                 PLOTPROF['min'][iplt][ifile]['y'])
        except:
            raise

######################################################################
######################################################################

    def ymax4_val(self, *dumm):
        ''' Validates ymax4 entry
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1
        idata = PLOTPROF['idata'][iplt][ifile] - 1

        val = self.ymax4_name.get()
        try:
            num = float(val)
            PLOTPROF['max'][iplt][ifile]['y'] = val
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                        self.ymax4_name.set( \
                             PLOTPROF['max'][iplt][ifile]['y'])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            self.ymax4_name.set( \
                             PLOTPROF['max'][iplt][ifile]['y'])
        except:
            raise

######################################################################
######################################################################

    def xfor6_val(self, *dumm):
        ''' Validates horizontal axis format entry
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1

        val = self.xfor6_name.get()
        # Apply the colormap label format
        if val.lower() in 'none':
            PLOTPROF['format'][iplt]['x'] = 'None'
            if val.lower() == 'none':
                self.xfor6_name.set('None')
        else:
            try:
                test = self.xfor6_name.get() % 0.
                PLOTPROF['format'][iplt]['x'] = val
            except:
                if len(val) == 0:
                    return
                for v in val:
                    if v not in '0123456789.%dfe':
                        self.xfor6_name.set( \
                                  PLOTPROF['format'][iplt]['x'])
                        return
                if val[0] != '%':
                    self.xfor6_name.set( \
                                  PLOTPROF['format'][iplt]['x'])

######################################################################
######################################################################

    def yfor6_val(self, *dumm):
        ''' Validates vertical axis format entry
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        iplt = PLOTPROF['iplt'] - 1

        val = self.yfor6_name.get()
        # Apply the colormap label format
        if val.lower() in 'none':
            PLOTPROF['format'][iplt]['y'] = 'None'
            if val.lower() == 'none':
                self.yfor6_name.set('None')
        else:
            try:
                test = self.yfor6_name.get() % 0.
                PLOTPROF['format'][iplt]['y'] = val
            except:
                if len(val) == 0:
                    return
                for v in val:
                    if v not in '0123456789.%dfe':
                        self.yfor6_name.set( \
                                  PLOTPROF['format'][iplt]['y'])
                        return
                if val[0] != '%':
                    self.yfor6_name.set( \
                                  PLOTPROF['format'][iplt]['y'])

######################################################################
######################################################################

    def update_entry(self, event=None):
        ''' Really updates entries
        '''

        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PSP
        PSP = source.general.PSP

        iplt = PLOTPROF['iplt'] - 1
        axis = PLOTPROF['axis'][iplt]
        ifile = PLOTPROF['ifile'][iplt] - 1
        idata = PLOTPROF['idata'][iplt][ifile] - 1
        single = PLOTPROF['single'][iplt][ifile]

        # Min plot variable
        val = self.min2_name.get()
        try:
            num = float(val)
            PLOTPROF['min'][iplt][0][axis] = val
        except ValueError:
            pass
        except:
            raise
        # Max plot variable
        val = self.max2_name.get()
        try:
            num = float(val)
            PLOTPROF['max'][iplt][0][axis] = val
        except ValueError:
            pass
        except:
            raise
        # Min Wavelength variable
        val = self.xmin3_name.get()
        try:
            num = float(val)
            ind = (np.abs(self.XX-num)).argmin()
            num = self.XX[ind]
            PLOTPROF['min'][iplt][0]['l'] = str(num)
            PLOTPROF['mind'][iplt][0]['l'] = ind
        except ValueError:
            pass
        except:
            raise
        # Max Wavelength variable
        val = self.xmax3_name.get()
        try:
            num = float(val)
            ind = (np.abs(self.XX-num)).argmin()
            num = self.XX[ind]
            PLOTPROF['max'][iplt][0]['l'] = str(num)
            PLOTPROF['Mind'][iplt][0]['l'] = ind
        except ValueError:
            pass
        except:
            raise
        # Min x variable
        val = self.xmin4_name.get()
        try:
            num = float(val)
            ind = (np.abs(PSP['data'][idata]['x']-num)).argmin()
            num = PSP['data'][idata]['x'][ind]
            if single:
                PLOTPROF['loc'][iplt][ifile]['x'] = str(num)
                PLOTPROF['sind'][iplt][ifile]['x'] = ind
            else:
                PLOTPROF['min'][iplt][ifile]['x'] = str(num)
                PLOTPROF['mind'][iplt][ifile]['x'] = ind
        except ValueError:
            pass
        except:
            raise
        # Max x variable
        val = self.xmax4_name.get()
        try:
            num = float(val)
            ind = (np.abs(PSP['data'][idata]['x']-num)).argmin()
            num = PSP['data'][idata]['x'][ind]
            PLOTPROF['max'][iplt][ifile]['x'] = str(num)
            PLOTPROF['Mind'][iplt][ifile]['x'] = ind
        except ValueError:
            pass
        except:
            raise
        # Min y variable
        val = self.ymin4_name.get()
        try:
            num = float(val)
            ind = (np.abs(PSP['data'][idata]['y']-num)).argmin()
            num = PSP['data'][idata]['y'][ind]
            if single:
                PLOTPROF['loc'][iplt][ifile]['y'] = str(num)
                PLOTPROF['sind'][iplt][ifile]['y'] = ind
            else:
                PLOTPROF['min'][iplt][ifile]['y'] = str(num)
                PLOTPROF['mind'][iplt][ifile]['y'] = ind
        except ValueError:
            pass
        except:
            raise
        # Max y variable
        val = self.ymax4_name.get()
        try:
            num = float(val)
            ind = (np.abs(PSP['data'][idata]['y']-num)).argmin()
            num = PSP['data'][idata]['y'][ind]
            PLOTPROF['max'][iplt][ifile]['y'] = str(num)
            PLOTPROF['Mind'][iplt][ifile]['y'] = ind
        except ValueError:
            pass
        except:
            raise
        # Horizontal axis format
        val = self.xfor6_name.get()
        if val.lower() in 'none':
            PLOTPROF['format'][iplt]['x'] = 'None'
        else:
            try:
                test = self.xfor6_name.get() % 0.
                PLOTPROF['format'][iplt]['x'] = val
            except:
                pass
        # Vertical axis format
        val = self.yfor6_name.get()
        if val.lower() in 'none':
            PLOTPROF['format'][iplt]['y'] = 'None'
        else:
            try:
                test = self.yfor6_name.get() % 0.
                PLOTPROF['format'][iplt]['y'] = val
            except:
                pass

        return

######################################################################
######################################################################

    def set_top(self):
        ''' Updates the top part of the widget window
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF
        global PSP
        PSP = source.general.PSP

        iplt = PLOTPROF['iplt'] - 1
        ifile = PLOTPROF['ifile'][iplt] - 1
        idata = PLOTPROF['idata'][iplt][ifile] - 1
        single = PLOTPROF['single'][iplt][ifile]
        adata = []
        ii = 0
        for head in PSP['head']:
            if head['loaded']:
                if head['manywav']:
                    ii += 1
                    adata.append(head['nameshort'])

        # Sanity check indexes
        if PLOTPROF['Mind'][iplt][ifile]['l'] >= self.NX:
            PLOTPROF['Mind'][iplt][ifile]['l'] = self.NX - 1
            PLOTPROF['max'][iplt][ifile]['l'] = self.XX[self.NX - 1]
        if PLOTPROF['Mind'][iplt][ifile]['x'] >= \
           PSP['head'][idata]['nodes'][0]:
            PLOTPROF['Mind'][iplt][ifile]['x'] = \
                                    PSP['head'][idata]['nodes'][0] - 1
            PLOTPROF['max'][iplt][ifile]['x'] = \
                                   PSP['data'][idata]['x'][ \
                                   PLOTPROF['Mind'][iplt][ifile]['x']]
        if PLOTPROF['Mind'][iplt][ifile]['y'] >= \
           PSP['head'][idata]['nodes'][1]:
            PLOTPROF['Mind'][iplt][ifile]['y'] = \
                                    PSP['head'][idata]['nodes'][1] - 1
            PLOTPROF['max'][iplt][ifile]['y'] = \
                                   PSP['data'][idata]['y'][ \
                                   PLOTPROF['Mind'][iplt][ifile]['y']]
        if PLOTPROF['mind'][iplt][ifile]['l'] >= self.NX:
            PLOTPROF['mind'][iplt][ifile]['l'] = self.NX - 1
            PLOTPROF['min'][iplt][ifile]['l'] = self.XX[self.NX - 1]
        if PLOTPROF['mind'][iplt][ifile]['x'] >= \
           PSP['head'][idata]['nodes'][0]:
            PLOTPROF['mind'][iplt][ifile]['x'] = \
                                    PSP['head'][idata]['nodes'][0] - 1
            PLOTPROF['min'][iplt][ifile]['x'] = \
                                   PSP['data'][idata]['x'][ \
                                   PLOTPROF['mind'][iplt][ifile]['x']]
        if PLOTPROF['mind'][iplt][ifile]['y'] >= \
           PSP['head'][idata]['nodes'][1]:
            PLOTPROF['mind'][iplt][ifile]['y'] = \
                                    PSP['head'][idata]['nodes'][1] - 1
            PLOTPROF['min'][iplt][ifile]['y'] = \
                                   PSP['data'][idata]['y'][ \
                                   PLOTPROF['mind'][iplt][ifile]['y']]
        if PLOTPROF['sind'][iplt][ifile]['x'] >= \
           PSP['head'][idata]['nodes'][0]:
            PLOTPROF['sind'][iplt][ifile]['x'] = \
                                    PSP['head'][idata]['nodes'][0] - 1
            PLOTPROF['loc'][iplt][ifile]['x'] = \
                                   PSP['data'][idata]['x'][ \
                                   PLOTPROF['sind'][iplt][ifile]['x']]
        if PLOTPROF['sind'][iplt][ifile]['y'] >= \
           PSP['head'][idata]['nodes'][1]:
            PLOTPROF['sind'][iplt][ifile]['y'] = \
                                    PSP['head'][idata]['nodes'][1] - 1
            PLOTPROF['loc'][iplt][ifile]['y'] = \
                                   PSP['data'][idata]['y'][ \
                                   PLOTPROF['sind'][iplt][ifile]['y']]

        # Number of plots
        self.nplt0_val.set(PLOTPROF['nplt'])
        self.nplt0_label.configure(text="{0}".format( \
                                                    PLOTPROF['nplt']))
        # Plot to edit
        self.wplt0_optm['menu'].delete(0,'end')
        self.wplt0_ind.set("Plot {0}".format(PLOTPROF['iplt']))
        for jj in range(1,PLOTPROF['nplt']+1):
            self.wplt0_optm['menu'].add_command( \
                 label = "Plot {0}".format(jj), \
                 command=lambda ii= "Plot {0}".format(jj): \
                 self.set0(ii))
        # Number of files
        self.nfil1_val.set(PLOTPROF['nfile'][iplt])
        self.nfil1_label.configure(text="{0}".format( \
                                   PLOTPROF['nfile'][iplt]))
        # Curve to edit
        self.wfil1_optm['menu'].delete(0,'end')
        self.wfil1_ind.set("Curve {0}".format( \
                           PLOTPROF['ifile'][iplt]))
        for jj in range(1,PLOTPROF['nfile'][iplt]+1):
            self.wfil1_optm['menu'].add_command( \
                 label = "Curve {0}".format(jj), \
                 command=lambda ii= "Curve {0}".format(jj): \
                 self.set10(ii))
        # File to plot
        self.wdat1_ind.set(adata[idata])

        # Variable to plot
        axis = PLOTPROF['axis'][iplt]
        self.wvar2_ind.set(axis)
        self.min2_name.set(PLOTPROF['min'][iplt][0][axis])
        self.max2_name.set(PLOTPROF['max'][iplt][0][axis])

        # Wavelength
        self.xmi3_slide.configure(to=self.NX - 1)
        self.xma3_slide.configure(to=self.NX - 1)
        self.aind31_name.set('Index {0}'.format( \
                         PLOTPROF['mind'][iplt][0]['l']+1))
        self.aind31_label.config(text=self.aind31_name.get())
        self.aind32_name.set('Index {0}'.format( \
                         PLOTPROF['Mind'][iplt][0]['l']+1))
        self.aind32_label.config(text=self.aind32_name.get())
        self.xmin3_name.set(PLOTPROF['min'][iplt][0]['l'])
        self.xmax3_name.set(PLOTPROF['max'][iplt][0]['l'])
        self.xmi3_ind.set(PLOTPROF['mind'][iplt][0]['l'])
        self.xma3_ind.set(PLOTPROF['Mind'][iplt][0]['l'])

        # Space
        self.xmi4_slide.configure(to=PSP['head'][idata]['nodes'][0]-1)
        self.xma4_slide.configure(to=PSP['head'][idata]['nodes'][0]-1)
        self.ymi4_slide.configure(to=PSP['head'][idata]['nodes'][1]-1)
        self.yma4_slide.configure(to=PSP['head'][idata]['nodes'][1]-1)
        self.auni21_ind.set("{0}".format(VARS['aunit_dic'][axis]))
        self.auni21_label.config(text=self.auni21_ind.get())
        self.auni22_ind.set("{0}".format(VARS['aunit_dic'][axis]))
        self.auni22_label.config(text=self.auni22_ind.get())
        if single:
            self.xmil4_name.set("Position {0}:".format( \
                            PSP['head'][idata]['axis_1_name']))
        else:
            self.xmil4_name.set("Minimum {0}:".format( \
                            PSP['head'][idata]['axis_1_name']))
        self.b4l0.config(text=self.xmil4_name.get())
        self.xmal4_name.set("Maximum {0}:".format( \
                            PSP['head'][idata]['axis_1_name']))
        self.b4l1.config(text=self.xmal4_name.get())
        if single:
            self.ymil4_name.set("Position {0}:".format( \
                            PSP['head'][idata]['axis_2_name']))
        else:
            self.ymil4_name.set("Minimum {0}:".format( \
                            PSP['head'][idata]['axis_2_name']))
        self.b4l3.config(text=self.ymil4_name.get())
        self.ymal4_name.set("Maximum {0}:".format( \
                            PSP['head'][idata]['axis_2_name']))
        self.b4l4.config(text=self.ymal4_name.get())
        if single:
            self.aind41_name.set('Index {0}'.format( \
                     PLOTPROF['sind'][iplt][ifile]['x']+1))
        else:
            self.aind41_name.set('Index {0}'.format( \
                     PLOTPROF['mind'][iplt][ifile]['x']+1))
        self.aind41_label.config(text=self.aind41_name.get())
        self.aind42_name.set('Index {0}'.format( \
                    PLOTPROF['Mind'][iplt][ifile]['x']+1))
        self.aind42_label.config(text=self.aind42_name.get())
        if single:
            self.aind43_name.set('Index {0}'.format( \
                        PLOTPROF['sind'][iplt][ifile]['y']+1))
        else:
            self.aind43_name.set('Index {0}'.format( \
                        PLOTPROF['mind'][iplt][ifile]['y']+1))
        self.aind43_label.config(text=self.aind43_name.get())
        self.aind44_name.set('Index {0}'.format( \
                    PLOTPROF['Mind'][iplt][ifile]['y']+1))
        self.aind44_label.config(text=self.aind44_name.get())
        if single:
            self.xmin4_name.set(PLOTPROF['loc'][iplt][ifile]['x'])
            self.ymin4_name.set(PLOTPROF['loc'][iplt][ifile]['y'])
        else:
            self.xmin4_name.set(PLOTPROF['min'][iplt][ifile]['x'])
            self.ymin4_name.set(PLOTPROF['min'][iplt][ifile]['y'])
        self.xmax4_name.set(PLOTPROF['max'][iplt][ifile]['x'])
        self.ymax4_name.set(PLOTPROF['max'][iplt][ifile]['y'])
        if single:
            self.xmi4_ind.set(PLOTPROF['sind'][iplt][ifile]['x'])
            self.ymi4_ind.set(PLOTPROF['sind'][iplt][ifile]['y'])
        else:
            self.xmi4_ind.set(PLOTPROF['mind'][iplt][ifile]['x'])
            self.ymi4_ind.set(PLOTPROF['mind'][iplt][ifile]['y'])
        self.xma4_ind.set(PLOTPROF['Mind'][iplt][ifile]['x'])
        self.yma4_ind.set(PLOTPROF['Mind'][iplt][ifile]['y'])
        # Disable widgets
        if single:
            self.sng4_stat.set(1)
            self.b4l1.config(state='disabled')
            self.xmax4_entry.config(state='disabled')
            self.xmau4_label.config(state='disabled')
            self.xma4_slide.config(state='disabled')
            self.b4l4.config(state='disabled')
            self.ymax4_entry.config(state='disabled')
            self.ymau4_label.config(state='disabled')
            self.yma4_slide.config(state='disabled')
        else:
            self.sng4_stat.set(0)
            self.b4l1.config(state='normal')
            self.xmax4_entry.config(state='normal')
            self.xmau4_label.config(state='normal')
            self.xma4_slide.config(state='normal')
            self.b4l4.config(state='normal')
            self.ymax4_entry.config(state='normal')
            self.ymau4_label.config(state='normal')
            self.yma4_slide.config(state='normal')

        # Color and linestyle
        self.wcol5_name.set(VARS['lcolor_name'] \
                                [PLOTPROF['color'][iplt][ifile]])
        self.wcol5_lab.config(text=self.wcol5_name.get())
        self.wcol5_ind.set(PLOTPROF['color'][iplt][ifile])
        self.wlin5_name.set(VARS['llsty_name'] \
                                [PLOTPROF['line'][iplt][ifile]])
        self.wlin5_lab.config(text=self.wlin5_name.get())
        self.wlin5_ind.set(PLOTPROF['line'][iplt][ifile])


        # Horizontal axis format and font
        # Entry
        if PLOTPROF['format'][iplt]['x'] is None:
            self.xfor6_name.set('None')
        else:
            self.xfor6_name.set(PLOTPROF['format'][iplt]['x'])
        # Check
        if PLOTPROF['cfont'][iplt]['x']:
            self.xfs6_stat.set(1)
        else:
            self.xfs6_stat.set(0)
        # Label
        self.xfs6_lab.config(text=str(PLOTPROF['sfont'][iplt]['x']))
        # Disable change of font
        if PLOTPROF['cfont'][iplt]['x']:
            self.xfs6_lab.config(state='normal')
            self.xfs6_bmin.config(state='normal')
            self.xfs6_bplu.config(state='normal')
        else:
            self.xfs6_lab.config(state='disabled')
            self.xfs6_bmin.config(state='disabled')
            self.xfs6_bplu.config(state='disabled')

        # Vertical axis format and font
        # Entry
        if PLOTPROF['format'][iplt]['y'] is None:
            self.yfor6_name.set('None')
        else:
            self.yfor6_name.set(PLOTPROF['format'][iplt]['y'])
        # Check
        if PLOTPROF['cfont'][iplt]['y']:
            self.yfs6_stat.set(1)
        else:
            self.yfs6_stat.set(0)
        # Label
        self.yfs6_lab.config(text=str(PLOTPROF['sfont'][iplt]['y']))
        # Disable change of font
        if PLOTPROF['cfont'][iplt]['y']:
            self.yfs6_lab.config(state='normal')
            self.yfs6_bmin.config(state='normal')
            self.yfs6_bplu.config(state='normal')
        else:
            self.yfs6_lab.config(state='disabled')
            self.yfs6_bmin.config(state='disabled')
            self.yfs6_bplu.config(state='disabled')

######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
######################################################################


class PLOTPROF_canvas_class(Toplevel):
    ''' Class that defines the support window to plot the 2D plots of
        the GUI.
    '''

    def __init__(self, parent, title = None):
        ''' Initialization of the PLOTPROF_canvas_class class.
        '''

        global CONF
        CONF = source.general.CONF

        # Initialized a support window.
        Toplevel.__init__(self, parent)
        self.transient(parent)

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Set the title if any
        if title:
            self.title(title)

        self.parent = parent
        self.result = None

        body = Frame(self)
        self.initial_focus = self.body(body)
        body.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        
        # Action buttons
        self.buttonbox(body)
        
        # Figure
        self.canvas(body)
        
        body.pack()

        self.grab_set()
        self.lift()
        
        if not self.initial_focus:
            self.initial_focus = self

        self.protocol("WM_DELETE_WINDOW", self.close)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)


    def body(self, master):
        ''' Creates the dialog body and set initial focus.
        '''

        return master

######################################################################
######################################################################

    def buttonbox(self, box):
        ''' Add the Draw and Close buttons to the canvas.
        '''

        global CONF
        CONF = source.general.CONF

        ibox = Frame(box)
        w = Button(ibox, text="Close", width=10, font=self.cFont, \
                   command=self.close)
        w.pack(side=LEFT, padx=CONF['b_pad'], pady=CONF['b_pad'])
        ibox.pack()
        ibox.configure(background=CONF['col_act'])

        self.bind("<Escape>", self.close)

######################################################################
######################################################################

    def close(self, event=None):
        ''' Method that handles the window closing.
        '''

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

######################################################################
######################################################################

    def canvas(self, body, event=None):
        ''' Method for draw. Plots given the widget variables.
        '''

        global VARS
        VARS = source.general.VARS
        global PSP
        PSP = source.general.PSP
        global PLOTPROF
        PLOTPROF = source.general.PLOTPROF

        # Choose the color maps
        cmapv = PLOTPROF['color']


        # Build subplots given the number of figures
        cent = [1, 2, 2, 2, 3, 3, 3, 3, 3]
        dece = [1, 1, 2, 2, 2, 2, 3, 3, 3]
        fig = plt.figure(figsize=VARS['figsize'][PLOTPROF['nplt']-1])
        fig.clf()

        # For each plot
        for ii in range(9):

            # Check if we are done
            if (ii + 1) > PLOTPROF['nplt']:
                break

            # Introduce subfigure
            ax = fig.add_subplot(100*cent[PLOTPROF['nplt'] - 1] + \
                                  10*dece[PLOTPROF['nplt'] - 1] + \
                                  ii + 1)

            # Get plot quantities
            axis = PLOTPROF['axis'][ii]

            # Get horizontal axis
            l0 = float(PLOTPROF['min'][ii][0]['l'])
            l1 = float(PLOTPROF['max'][ii][0]['l'])
            il0 = int(PLOTPROF['mind'][ii][0]['l'])
            il1 = int(PLOTPROF['Mind'][ii][0]['l'])
            XX = np.array(PLOTPROF['l'][il0:il1+1])

            # Get vertical axis
            y0 = float(PLOTPROF['min'][ii][0][axis])
            y1 = float(PLOTPROF['max'][ii][0][axis])

            # Set axis
            plt.axis([l0,l1,y0,y1])

            # Set titles
            if PLOTPROF['cfont'][ii]['x']:
                ax.set_xlabel('$\lambda$ [nm]', \
                             fontsize=PLOTPROF['sfont'][ii]['x'])
                for tick in ax.xaxis.get_major_ticks():
                     tick.label.set_fontsize( \
                                    int(PLOTPROF['sfont'][ii]['x']))
            else:
                ax.set_xlabel('$\lambda$ [nm]')
            if PLOTPROF['cfont'][ii]['y']:
                ax.set_ylabel(VARS['SPL_dic'][axis], \
                             fontsize=PLOTPROF['sfont'][ii]['y'])
                for tick in ax.yaxis.get_major_ticks():
                     tick.label.set_fontsize( \
                                    int(PLOTPROF['sfont'][ii]['y']))
            else:
                ax.set_ylabel(VARS['SPL_dic'][axis])

            # Formatter
            # Horizontal axis
            if PLOTPROF['format'][ii]['x'] != 'None' and PLOTPROF['format'][ii]['x'] != None:
                pltxformat = "lambda y,_ : \"" + PLOTPROF['format'][ii]['x'] + "\" % (y)"
                xformatter = FuncFormatter(eval(pltxformat))
                ax.xaxis.set_major_formatter(xformatter)
            # Vertical axis
            if PLOTPROF['format'][ii]['y'] != 'None' and PLOTPROF['format'][ii]['y'] != None:
                pltyformat = "lambda y,_ : \"" + PLOTPROF['format'][ii]['y'] + "\" % (y)"
                yformatter = FuncFormatter(eval(pltyformat))
                ax.yaxis.set_major_formatter(yformatter)

            # For each file
            for jj in range(PLOTPROF['nfile'][ii]):

                # Get file
                idata = PLOTPROF['idata'][ii][jj] - 1
                single = PLOTPROF['single'][ii][jj]
                col = PLOTPROF['color'][ii][jj]
                lin = PLOTPROF['line'][ii][jj]
                head = PSP['head'][idata]
                data = PSP['data'][idata]


                # Get vertical axis data
                if single:
                    ix = int(PLOTPROF['sind'][ii][jj]['x'])
                    iy = int(PLOTPROF['sind'][ii][jj]['y'])
                    YY = data[axis][:,iy,ix]
                    YY = np.interp(XX, data['l'], YY)
                else:
                    ix0 = int(PLOTPROF['mind'][ii][jj]['x'])
                    ix1 = int(PLOTPROF['Mind'][ii][jj]['x'])
                    iy0 = int(PLOTPROF['mind'][ii][jj]['y'])
                    iy1 = int(PLOTPROF['Mind'][ii][jj]['y'])
                    YY = np.mean(data[axis][:,iy0:iy1+1,ix0:ix1+1], \
                                 axis = tuple((1,2)))
                    YY = np.interp(XX, data['l'], YY)

                plt.plot(XX,YY,VARS['lcolor_dict'] \
                                   [VARS['lcolor_name'][col]] + \
                               VARS['llsty_dict'] \
                                   [VARS['llsty_name'][lin]])

        # Run matplotlib
        plt.tight_layout(pad=1.0, w_pad=2.0, h_pad=2.0)
        canvas = FigureCanvasTkAgg(fig, self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=BOTTOM, fill=BOTH, \
                                    expand=True)
        toolbar = NavigationToolbar(canvas, self)
        toolbar.update()
        canvas._tkcanvas.pack(side=TOP, fill=BOTH, expand=True)
        plt.close('all')

######################################################################
######################################################################




