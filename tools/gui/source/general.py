# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# general.py                                                         #
#                                                                    #
# Tanausú del Pino Alemán                                            #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Module with global variables                                       #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  10/03/2020 - V1.0.4 - Added color options to CONF, they can be    #
#                        read from the cache (TdPA)                  #
#                                                                    #
#  26/02/2020 - V1.0.3 - Bugfix: It seems Stokes were shown in cgs,  #
#                        not in mks (TdPA)                           #
#                                                                    #
#  23/01/2020 - V1.0.2 - Bugfix: stereoradiands should be sr and not #
#                        str. (TdPA)                                 #
#                                                                    #
#  09/06/2018 - V1.0.1 - Added tooltip. (TdPA)                       #
#                                                                    #
#  12/03/2018 - V1.0.0 - First version. (TdPA)                       #
#                                                                    #
#  07/03/2018 - V0.0.0 - Start Code. (TdPA)                          #
#                                                                    #
######################################################################
######################################################################
######################################################################

from source.loader import *


cgs = r'[erg Hz${}^{-1}$ cm${}^{-2}$ s${}^{-1}$ ' + \
      'sr${}^{-1}$]'
mks = r'[J Hz${}^{-1}$ m${}^{-2}$ s${}^{-1}$ ' + \
      'sr${}^{-1}$]'
mksc = r'[J Hz${}^{-1}$ m${}^{-2}$ s${}^{-1}$ ' + \
       'sr${}^{-1}$ km${}^{-1}$]'

TOOLTIPS = {'0': "", \
            '-': "Reduce number by 1", \
            '+': "Increase number by 1", \
            '1': "Section of the GUI to manage the loading of data", \
            '2': "Open a dialog pickfile to select the file \n" + \
                 "and read its header", \
            '3': "Select the file to manage", \
            '4': "Nothing loaded", \
            '5': "File is loaded", \
            '6': "File is not loaded", \
            '7': "Load the chosen file", \
            '8': "Drops the loaded data of the chosen file", \
            '9': "Removes the file from the list", \
            '10': "Opens window to configure the plots", \
            '11': "Close current widget window", \
            '12': "Number of plots to display", \
            '13': "Select the plot to change", \
            '14': "Select the type of plot", \
            '15': "Number of files to get data from for the plot", \
            '16': "Select the file to change", \
            '17': "Select the file to get data from for the plot", \
            '18': "Creates a plot with the specified properties", \
            '19': "Select the horizontal axis of the plot", \
            '20': "Open the limit selection for this axis", \
            '21': "Select the vertical or cut 1 axis of the plot", \
            '22': "Select the variable to plot", \
            '23': "Select the cut or cut 2 axis of the plot", \
            '24': "Check to plot in frequencies where P is " + \
                  "maximum", \
            '25': "Check to shrink the horizontal axis given " + \
                  "the LOS angle", \
            '26': "Select the colormap for the plot", \
            '27': "Check to change font size of this axis", \
            '28': "Stores the widget configuration", \
            '29': "Loads a widget configuration file", \
            '30': "Resets the widget configuration", \
            '31': "Resets the limits to the defaults", \
            '32': "Finds the limits for this file and variable", \
            '33': "Select the cut axis", \
            '34': "Choose a single point to plot a profile", \
            '35': "Number of curves in the plot", \
            '36': "Select the curve to edit", \
            '37': "Change GUI font size", \
            '38': "No file selected", \
            '39': "Check to interpolate data while reading", \
            '40': "Interpolate read data", \
            '41': "Open the widget for the pmd module", \
            '42': "Plot the loaded data", \
            '43': "Plot data without loading extracting with C", \
            '44': "Loads all chosen files", \
            '45': "Drops the loaded data of all chosen files", \
            '46': "Checks all variables to plot", \
            '47': "Unchecks all variables to plot", \
            '48': "Toggles all variables to plot", \
            '49': "Save the selected vtk files in desired folder", \
            'delete': "delete" \
           }

VARS = {'figsize' : [(5,4),(5,7),(9,7),(9,7),(8,9),(8,9), \
                     (12,9),(12,9),(12,9)], \
        'axis_dic': {'x' : 'axis_1_name', \
                     'y' : 'axis_2_name', \
                     'l' : 'axis_3_name'}, \
        'axis_ind': {'x' : 0, \
                     'y' : 1}, \
        'axis_idic': {'axis_1_name' : 'x', \
                      'axis_2_name' : 'y', \
                      'axis_3_name' : 'l'}, \
        'taxis2_dic' : {1: '    Vertical axis:', \
                        2: '       Cut axis 1:'} , \
        'taxis3_dic' : {1: '         Cut axis:', \
                        2: '       Cut axis 2:'} , \
        'ptype_list': ['2D','Lines'], \
        'ptype_dic': {1 : '2D', 2 : 'Lines'}, \
        'ptype_idic': {'2D' : 1, 'Lines' : 2}, \
        'SP_list' : ['I','Q/I','U/I','V/I','P','Log I'], \
        'SPL_dic' : {'I' : r'I '+cgs,\
                     'Q/I' : 'Q/I [%]',\
                     'U/I' : 'U/I [%]', \
                     'V/I' : 'V/I [%]', \
                     'P' : 'P [%]', \
                     'Log I' : r'Log I '+cgs, \
                     'Q' : r'Q '+cgs, \
                     'U' : r'U '+cgs, \
                     'V' : r'V '+cgs, \
                     'P*I' : r'P '+cgs}, \
        'aunit_dic' : {'x' : 'Mm', 'y' : 'Mm', 'l' : 'nm', \
                       'I' : 'CGS', \
                       'Q/I' : '%',\
                       'U/I' : '%', \
                       'V/I' : '%', \
                       'P' : '%', \
                       'Log I' : 'CGS' \
                      }, \
        'format_list' : [], \
        'head_size' : {2: 201844}, \
        'endian' : {'Big' : '>', 'Little' : '<'}, \
        'palet_name' : ['RedYellowBlue','SignAdjustable', \
                        'Greys', 'inferno', 'plasma', \
                        'magma', 'Blues', 'BuGn', \
                        'BuPu', 'GnBu', 'Greens', \
                        'viridis', 'Oranges', 'OrRd', \
                        'PuBu', 'PuBuGn', 'PuRd', \
                        'Purples', 'RdPu', 'Reds', \
                        'YlGn', 'YlGnBu', 'YlOrBr', \
                        'YlOrRd', 'afmhot', 'autumn', \
                        'bone', 'cool', 'copper', \
                        'gist_heat', 'gray', 'hot', \
                        'pink', 'spring', 'summer', \
                        'winter', 'BrBG', 'bwr', \
                        'coolwarm', 'PiYG', 'PRGn', \
                        'PuOr', 'RdBu', 'RdGy', \
                        'RdYlBu', 'RdYlGn', \
                        'Spectral', 'seismic', \
                        'Accent', 'Dark2', 'Paired', \
                        'Pastel1', 'Pastel2', 'Set1', \
                        'Set2', 'Set3', 'gist_earth', \
                        'terrain', 'ocean', \
                        'gist_stern', 'brg', 'CMRmap', \
                        'cubehelix', 'gnuplot', \
                        'gnuplot2', 'gist_ncar', \
                        'nipy_spectral', 'jet', \
                        'rainbow', 'gist_rainbow', \
                        'hsv', 'flag', 'prism'], \
         'lcolor_name' : ['black','blue','green','red', \
                          'cyan','magenta','yellow', \
                          'white'], \
         'lcolor_dict' : {'black' : 'k', 'blue' : 'b', \
                          'green' : 'g','red' : 'r', \
                          'cyan' : 'c','magenta' : 'm', \
                          'yellow' : 'y','white' : 'w'}, \
         'llsty_name' : ['solid','dashed','dotted'], \
         'llsty_dict' : {'solid' : '', 'dashed' : '--', \
                        'dotted' : ":"}, \
         'delete' : 'delete' \
         }

MODS = {'twolevel' : 'source.twolevel', \
        'multilevel' : 'source.multilevel', \
        'contpol' : 'source.contpol'}

CONF_def = {'wfont_size' : 12, \
            'cfont_size' : 8, \
            'endian' : VARS['endian']['Little'], \
            'format' : 'None', \
            'interpol': False, \
            'nodes': {'x': 100, 'y': 100, 'z': 100}, \
            'col_act': '#008000', \
            'col_pas': '#D3D3D3', \
            'b_pad': 7, \
            'delete' : 'delete' \
           }

CONF_read = ['wfont_size', 'cfont_size', \
             'col_act', 'col_pas', 'b_pad']

PATH = {'psp_dir': '', \
        'pmd_dir': '', \
        'delete' : 'delete' \
       }

PSP_def = {'nf' : 0, \
           'idata' : 1, \
           'data' : [], \
           'head' : [], \
           'delete' : 'delete' \
          }

PMD_def = {'data' : None, \
           'head' : None, \
           'delete' : 'delete' \
          }

PLOT_def = {'nplt' : 1, \
            'iplt' : 1, \
            'tplt' : [1], \
            'nf' : [1], \
            'idata': [[1]], \
            'ifile': [1], \
            'iaxis1': ['x'], \
            'iaxis2': ['y'], \
            'iaxis3': ['l'], \
            'iaxis4': ['I'], \
            'min0' : None, \
            'max0' : None, \
            'min' : None, \
            'max' : None, \
            'delete' : 'delete' \
           }

PLOTXY_def = {'nplt' : 1, \
              'iplt' : 1, \
              'idata': [1], \
              'min' : None, \
              'max' : None, \
              'wav' : None, \
              'iwav' : [0], \
              'mind' : [{'x':0,'y':0}], \
              'Mind' : [{'x':None,'y':None}], \
              'axis': ['I'], \
              'maxp' : [False] , \
              'shrink': [False], \
              'color': [0], \
              'format' : [{'x' : None, \
                           'y' : None, \
                           'z' : None}], \
              'cfont' : [{'x' : False, \
                          'y' : False, \
                          'z' : False}], \
              'sfont' : [{'x' : 12, 'y' : 12, 'z' : 12}], \
              'delete' : 'delete' \
             }


PLOTPROF_def = {'nplt' : 1, \
                'iplt' : 1, \
                'nfile' : [1], \
                'ifile': [1], \
                'idata': [[1]], \
                'single' : [[False]], \
                'min' : None, \
                'max' : None, \
                'loc' : None, \
                'sind' : [[{'x':0,'y':0,'l':0}]], \
                'mind' : [[{'x':0,'y':0,'l':0}]], \
                'Mind' : [[{'x':None,'y':None,'l':None}]], \
                'axis': ['I'], \
                'color': [[0]], \
                'line': [[0]], \
                'format' : [{'x' : None, \
                             'y' : None, \
                             'z' : None}], \
                'cfont' : [{'x' : False, \
                            'y' : False, \
                            'z' : False}], \
                'sfont' : [{'x' : 12, 'y' : 12, 'z' : 12}], \
                'delete' : 'delete' \
                }

PLOTSLIT_def = {'nplt' : 1, \
                'iplt' : 1, \
                'idata': [1], \
                'min' : None, \
                'max' : None, \
                'vcaxis': None, \
                'icaxis': [{'x':0,'y':0}], \
                'mind' : [{'x':0,'y':0,'l':0}], \
                'Mind' : [{'x':None,'y':None,'l':None}], \
                'axis': ['I'], \
                'caxis': None, \
                'vaxis': None, \
                'shrink': [False], \
                'color': [0], \
                'format' : [{'x' : None, \
                             'y' : None, \
                             'l' : None, \
                             'z' : None}], \
                'cfont' : [{'x' : False, \
                            'y' : False, \
                            'l' : False, \
                            'z' : False}], \
                        'sfont' : [{'x' : 12, 'y' : 12, \
                                    'l' : 12, 'z' : 12}], \
                'delete' : 'delete' \
               }


        # Whether axis gridlines and ticks are below the axes elements
aaaPARS = {u'axes.axisbelow': False, \
        # Axes edge color
        u'axes.edgecolor': u'k', \
        # Axes background color
        u'axes.facecolor': u'w', \
        # Use mathtext for scientific notation
        u'axes.formatter.use_mathtext': False, \
        # Display grid or not
        u'axes.grid': False, \
        # Color of the labels
        u'axes.labelcolor': u'k', \
        # Space between axis and label
        u'axes.labelpad': 5.0, \
        # Fontsize of axes label
        u'axes.labelsize': u'medium', \
        # Weight of the X and Y labels
        u'axes.labelweight': u'normal', \
        # Axes linewidth
        u'axes.linewidth': 1.0, \
        # Fontsize of axes title
        u'axes.titlesize': u'large', \
        # Weight of the title
        u'axes.titleweight': u'normal', \
        # xmargin
        u'axes.xmargin': 0.0, \
        # ymargin
        u'axes.ymargin': 0.0, \
        u'figure.autolayout': False, \
        # Dots per inch
        u'figure.dpi': 80.0, \
        u'figure.edgecolor': u'w', \
        u'figure.facecolor': u'0.75', \
        u'figure.frameon': True, \
        u'figure.max_open_warning': 20, \
        u'figure.subplot.bottom': 0.1, \
        u'figure.subplot.hspace': 0.2, \
        u'figure.subplot.left': 0.125, \
        u'figure.subplot.right': 0.9, \
        u'figure.subplot.top': 0.9, \
        u'figure.subplot.wspace': 0.2, \
        u'font.cursive': [u'Apple Chancery', u'Textile', \
                          u'Zapf Chancery', u'Sand', \
                          u'Script MT', u'Felipa', \
                          u'cursive'], \
        u'font.family': [u'sans-serif'], \
        u'font.fantasy': [u'Comic Sans MS', u'Chicago', \
                          u'Charcoal', u'ImpactWestern', \
                          u'Humor Sans', u'fantasy'], \
        u'font.monospace': [u'Bitstream Vera Sans Mono', \
                            u'DejaVu Sans Mono', \
                            u'Andale Mono', u'Nimbus Mono L', \
                            u'Courier New', u'Courier', \
                            u'Fixed', u'Terminal', \
                            u'monospace'], \
        u'font.sans-serif': [u'Bitstream Vera Sans', \
                             u'DejaVu Sans', u'Lucida Grande', \
                             u'Verdana', u'Geneva', \
                             u'Lucid', u'Arial', \
                             u'Helvetica', u'Avant Garde', \
                             u'sans-serif'], \
        u'font.serif': [u'Bitstream Vera Serif', \
                        u'DejaVu Serif', \
                        u'New Century Schoolbook', \
                        u'Century Schoolbook L', \
                        u'Utopia', u'ITC Bookman', \
                        u'Bookman', u'Nimbus Roman No9 L', \
                        u'Times New Roman', u'Times', \
                        u'Palatino', u'Charter', u'serif'], \
        u'font.size': 12.0, \
        u'font.stretch': u'normal', \
        u'font.style': u'normal', \
        u'font.variant': u'normal', \
        u'font.weight': u'normal', \
        u'grid.alpha': 1.0, \
        u'grid.color': u'k', \
        u'grid.linestyle': u':', \
        u'grid.linewidth': 0.5, \
        u'image.aspect': u'equal', \
        u'image.cmap': u'jet', \
        u'image.composite_image': True, \
        u'image.interpolation': u'bilinear', \
        u'image.lut': 256, \
        u'image.origin': u'upper', \
        u'image.resample': False, \
        u'interactive': False, \
        u'keymap.all_axes': [u'a'], \
        u'keymap.back': [u'left', u'c', u'backspace'], \
        u'keymap.forward': [u'right', u'v'], \
        u'keymap.fullscreen': [u'f', u'ctrl+f'], \
        u'keymap.grid': [u'g'], \
        u'keymap.home': [u'h', u'r', u'home'], \
        u'keymap.pan': [u'p'], \
        u'keymap.quit': [u'ctrl+w', u'cmd+w'], \
        u'keymap.save': [u's', u'ctrl+s'], \
        u'keymap.xscale': [u'k', u'L'], \
        u'keymap.yscale': [u'l'], \
        u'keymap.zoom': [u'o'], \
        u'legend.borderaxespad': 0.5, \
        u'legend.borderpad': 0.4, \
        u'legend.columnspacing': 2.0, \
        u'legend.edgecolor': u'inherit', \
        u'legend.facecolor': u'inherit', \
        u'legend.fancybox': False, \
        u'legend.fontsize': u'large', \
        u'legend.framealpha': None, \
        u'legend.frameon': True, \
        u'legend.handleheight': 0.7, \
        u'legend.handlelength': 2.0, \
        u'legend.handletextpad': 0.8, \
        u'legend.isaxes': True, \
        u'legend.labelspacing': 0.5, \
        u'legend.loc': u'upper right', \
        u'legend.markerscale': 1.0, \
        u'legend.numpoints': 2, \
        u'legend.scatterpoints': 3, \
        u'legend.shadow': False, \
        u'lines.antialiased': True, \
        u'lines.color': u'b', \
        u'lines.dash_capstyle': u'butt', \
        u'lines.dash_joinstyle': u'round', \
        u'lines.linestyle': u'-', \
        u'lines.linewidth': 1.0, \
        u'lines.marker': u'None', \
        u'lines.markeredgewidth': 0.5, \
        u'lines.markersize': 6.0, \
        u'lines.solid_capstyle': u'projecting', \
        u'lines.solid_joinstyle': u'round', \
        u'markers.fillstyle': u'full', \
        u'mathtext.bf': u'serif:bold', \
        u'mathtext.cal': u'cursive', \
        u'mathtext.default': u'it', \
        u'mathtext.fallback_to_cm': True, \
        u'mathtext.fontset': u'cm', \
        u'mathtext.it': u'serif:italic', \
        u'mathtext.rm': u'serif', \
        u'mathtext.sf': u'sans\\-serif', \
        u'mathtext.tt': u'monospace', \
        u'nbagg.transparent': True, \
        u'patch.antialiased': True, \
        u'patch.edgecolor': u'k', \
        u'patch.facecolor': u'b', \
        u'patch.linewidth': 1.0, \
        u'path.effects': [], \
        u'path.simplify': True, \
        u'path.simplify_threshold': 0.1111111111111111, \
        u'path.sketch': None, \
        u'path.snap': True, \
        u'pdf.compression': 6, \
        u'pdf.fonttype': 3, \
        u'pdf.inheritcolor': False, \
        u'pdf.use14corefonts': False, \
        u'pgf.debug': False, \
        u'pgf.preamble': [], \
        u'pgf.rcfonts': True, \
        u'pgf.texsystem': u'xelatex', \
        u'plugins.directory': u'.matplotlib_plugins', \
        u'ps.distiller.res': 6000, \
        u'ps.fonttype': 3, \
        u'ps.papersize': u'letter', \
        u'ps.useafm': False, \
        u'ps.usedistiller': False, \
        u'savefig.bbox': None, \
        u'savefig.directory': u'~', \
        u'savefig.dpi': 100.0, \
        u'savefig.edgecolor': u'w', \
        u'savefig.facecolor': u'w', \
        u'savefig.format': u'png', \
        u'savefig.frameon': True, \
        u'savefig.jpeg_quality': 95, \
        u'savefig.orientation': u'portrait', \
        u'savefig.pad_inches': 0.1, \
        u'savefig.transparent': False, \
        u'svg.fonttype': u'path', \
        u'svg.image_inline': True, \
        u'svg.image_noscale': False, \
        u'text.antialiased': True, \
        u'text.color': u'k', \
        u'text.dvipnghack': None, \
        u'text.hinting': u'auto', \
        u'text.hinting_factor': 8, \
        u'text.latex.preamble': [], \
        u'text.latex.preview': False, \
        u'text.latex.unicode': False, \
        u'text.usetex': False, \
        u'timezone': u'UTC', \
        u'tk.window_focus': False, \
        u'toolbar': u'toolbar2', \
        u'verbose.fileo': u'sys.stdout', \
        u'verbose.level': u'silent', \
        u'webagg.open_in_browser': True, \
        u'webagg.port': 8988, \
        u'webagg.port_retries': 50, \
        u'xtick.color': u'k', \
        u'xtick.direction': u'in', \
        u'xtick.labelsize': u'medium', \
        u'xtick.major.pad': 4.0, \
        u'xtick.major.size': 4.0, \
        u'xtick.major.width': 0.5, \
        u'xtick.minor.pad': 4.0, \
        u'xtick.minor.size': 2.0, \
        u'xtick.minor.visible': False, \
        u'xtick.minor.width': 0.5, \
        u'ytick.color': u'k', \
        u'ytick.direction': u'in', \
        u'ytick.labelsize': u'medium', \
        u'ytick.major.pad': 4.0, \
        u'ytick.major.size': 4.0, \
        u'ytick.major.width': 0.5, \
        u'ytick.minor.pad': 4.0, \
        u'ytick.minor.size': 2.0, \
        u'ytick.minor.visible': False, \
        u'ytick.minor.width': 0.5 \
       }

CONF = copy.deepcopy(CONF_def)
PSP = copy.deepcopy(PSP_def)
PMD = copy.deepcopy(PMD_def)
PLOT = copy.deepcopy(PLOT_def)
PLOTXY = copy.deepcopy(PLOTXY_def)
PLOTSLIT = copy.deepcopy(PLOTSLIT_def)
PLOTPROF = copy.deepcopy(PLOTPROF_def)

