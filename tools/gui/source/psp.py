# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# psp.py                                                             #
#                                                                    #
# Tanausú del Pino Alemán                                            #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# GUI for the psp files visualization                                #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  09/04/2020 - V1.0.5 - Added some warning messages at read (TdPA)  #
#                                                                    #
#  10/03/2020 - V1.0.4 - Added colors to indicate focus (TdPA)       #
#                      - Solved focus problems (AdV)                 #
#                                                                    #
#  04/10/2019 - V1.0.3 - Now the GUI recognizes itself if the file   #
#                        is binary or hdf5, so just one button to    #
#                        choose (TdPA)                               #
#                                                                    #
#  27/09/2019 - V1.0.2 - Added buttons to load all chosen files that #
#                        are not loaded, to unload every loaded      #
#                        file, and remove every unload file (TdPA)   #
#                                                                    #
#  24/04/2018 - V1.0.1 - Compatibility with python 3+. (TdPA)        #
#                                                                    #
#  27/03/2018 - V1.0.4 - Added loading indicator. (TdPA)             #
#                                                                    #
#  26/03/2018 - V1.0.3 - Added comment handling. (TdPA)              #
#                                                                    #
#  21/03/2018 - V1.0.2 - Added Plot Profile button. (TdPA)           #
#                      - Added dimensional checks over loaded files  #
#                        before opening the widget for xy, slit and  #
#                        profile plots. (TdPA)                       #
#                                                                    #
#  20/03/2018 - V1.0.1 - Added Plot Slit button. (TdPA)              #
#                                                                    #
#  12/03/2018 - V1.0.0 - First Version. (TdPA)                       #
#                                                                    #
#  07/03/2018 - V0.0.0 - Start Code. (TdPA)                          #
#                                                                    #
######################################################################
######################################################################
######################################################################

import source.general
from source.loader import *
from source.tooltip import *
from source.psp_plotxy import *
from source.psp_plotslit import *
from source.psp_plotprofile import *
import h5py

######################################################################
######################################################################

class PSP_class(Toplevel):
    ''' Class that defines the main body of the psp visualizer
    '''

######################################################################
######################################################################

    def __init__(self, parent, title = None):
        ''' Initializes the widget single tab
        '''

        global CONF
        CONF = source.general.CONF

        # Initialized a support window.
        Toplevel.__init__(self, parent)
        self.transient(parent)

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Set the title if any
        if title:
            self.title(title)

        # Active border
        self.configure(background=CONF['col_act'])

        self.parent = parent
        self.result = None
        self.plt_action = None

        head = Frame(self)
        body = Frame(head)
        self.up = Frame(body)
        self.mid = Frame(body)
        self.do = Frame(body)
        self.head = head
        self.box = body
        self.initial_focus = self.body(body)
        head.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        body.pack(fill=BOTH,expand=1)

        self.grab_set()
        self.lift()
        
        if not self.initial_focus:
            self.initial_focus = self

        # Configuration file manager
        self.optionbox(self.up)

        # Configuration display
        self.display(self.mid)

        # Configuration buttons
        self.buttons(self.do)

        self.up.pack(fill=BOTH,expand=1,side=TOP)
        self.mid.pack(fill=BOTH,expand=1,side=TOP)
        self.do.pack(fill=BOTH,expand=1,side=BOTTOM)
        body.pack(fill=BOTH,expand=1)
        head.pack(fill=BOTH,expand=1)

        self.protocol("WM_DELETE_WINDOW", self.close)
        self.bind("<Escape>", self.close)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)


######################################################################
######################################################################

    def body(self, master):
        ''' Creates the dialog body and set initial focus.
        '''

        return master

######################################################################
######################################################################

    def close(self, event=None):
        ''' Method that handles the window closing.
        '''

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

######################################################################
######################################################################
######################################################################
######################################################################

    def optionbox(self, box):
        ''' Defines all the control widgets of PSP_class class.
        '''

        global PSP
        PSP = source.general.PSP
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # General label
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l = Label(box0, text="Manage files", font=self.cFont, \
                    justify=CENTER)
        createToolTip(b0l,TOOLTIPS['1'])
        b0l.grid(row=row,column=col,columnspan=6,sticky=NSEW)
        # Choose button
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        self.butt0_choose = Button(box0, text="Choose new", \
                                   font=self.cFont, \
                                   command=self.choose0)
        createToolTip(self.butt0_choose,TOOLTIPS['2'])
        self.butt0_choose.grid(row=row,column=col,columnspan=6, \
                               sticky=NSEW)
        '''
        # Choose button (binary)
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        self.butt0_choose = Button(box0, text="Choose new", \
                                   font=self.cFont, \
                                   command=self.choose0_binary)
        createToolTip(self.butt0_choose,TOOLTIPS['2'])
        self.butt0_choose.grid(row=row,column=col,columnspan=6, \
                               sticky=NSEW)
        # Choose button (HDF5)
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        self.butt0_choose_h5 = Button(box0, \
                                      text="Choose new (HDF5)", \
                                      font=self.cFont, \
                                      command=self.choose0_h5)
        createToolTip(self.butt0_choose_h5,TOOLTIPS['2'])
        self.butt0_choose_h5.grid(row=row,column=col,columnspan=6, \
                               sticky=NSEW)
        '''
        # Change all buttons
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        # Load all button
        col = 0
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_loada = Button(box0,text="Load all", \
                                  font=self.cFont, \
                                  command=self.loada0)
        createToolTip(self.butt0_loada,TOOLTIPS['44'])
        self.butt0_loada.grid(row=row,column=col,columnspan=2, \
                              sticky=NSEW)
        # Unload all button
        col += 2
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_unloada = Button(box0,text="Unload all", \
                                    font=self.cFont, \
                                    command=self.unloada0)
        createToolTip(self.butt0_unloada,TOOLTIPS['45'])
        self.butt0_unloada.grid(row=row,column=col,columnspan=2, \
                                sticky=NSEW)
        # Remove all button
        col += 2
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_rmva = Button(box0,text="Remove all", \
                                 font=self.cFont, \
                                 command=self.rmva0)
        createToolTip(self.butt0_rmva,TOOLTIPS['9'])
        self.butt0_rmva.grid(row=row,column=col,columnspan=2, \
                             sticky=NSEW)
        # Loaded files
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        # OptionMenu
        col = 0
        Grid.columnconfigure(box0,col,weight=1)
        self.data0_ind = StringVar()
        if PSP['nf'] == 0:
            self.data0_ind.set(TOOLTIPS['4'])
            lst = ['']
        else:
            if PSP['idata'] > PSP['nf']:
                PSP['idata'] = PSP['nf']
            self.data0_ind.set("Datum {0}".format(PSP['idata']))
            lst = []
            for ii in range(PSP['nf']):
                lst.append("{0}: {1}".format(ii+1,\
                                  PSP['head'][ii]['nameshort']))
        self.data0_optm = OptionMenu(box0, self.data0_ind, \
                                     *lst, command=self.set0)
        self.data0_optm.config(font=self.cFont)
        self.data0_optm.nametowidget(self.data0_optm.menuname). \
                                          config(font=self.cFont)
        createToolTip(self.data0_optm, TOOLTIPS['3'])
        self.data0_optm.grid(row=row,column=col,columnspan=2, \
                             sticky=NSEW)
        # Status
        col += 2
        Grid.columnconfigure(box0,col,weight=1)
        box00 = LabelFrame(box0)
        Grid.rowconfigure(box00,0,weight=1)
        Grid.columnconfigure(box00,0,weight=1)
        if PSP['nf'] == 0:
            self.data0_name = Label(box00, text=TOOLTIPS['4'], \
                                    anchor=CENTER, font=self.cFont)
            createToolTip(self.data0_name,TOOLTIPS['4'])
        else:
            if PSP['head'][PSP['idata']-1]['loaded']:
                self.data0_name = Label(box00, text='Loaded', \
                                        anchor=CENTER, \
                                        font=self.cFont)
                createToolTip(self.data0_name, TOOLTIPS['5'])
            else:
                self.data0_name = Label(box00, text='Not loaded', \
                                        anchor=CENTER, \
                                        font=self.cFont)
                createToolTip(self.data0_name, TOOLTIPS['6'])
        self.data0_name.grid(row=0,column=0,sticky=NSEW)
        box00.grid(row=row,column=col,sticky=NSEW)
        # Load button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_load = Button(box0,text="Load",font=self.cFont, \
                                 command=self.load0)
        createToolTip(self.butt0_load,TOOLTIPS['7'])
        self.butt0_load.grid(row=row,column=col,sticky=NSEW)
        # Unload button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_unload = Button(box0,text="Unload", \
                                   font=self.cFont, \
                                   command=self.unload0)
        createToolTip(self.butt0_unload,TOOLTIPS['8'])
        self.butt0_unload.grid(row=row,column=col,sticky=NSEW)
        # Remove button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_rmv = Button(box0,text="Remove",font=self.cFont, \
                                command=self.rmv0)
        createToolTip(self.butt0_rmv,TOOLTIPS['9'])
        self.butt0_rmv.grid(row=row,column=col,sticky=NSEW)
        # Disable widgets if no data loaded
        if PSP['nf'] == 0:
            self.data0_optm.config(state='disabled')
            self.data0_name.config(state='disabled')
            self.butt0_loada.config(state='disabled')
            self.butt0_unloada.config(state='disabled')
            self.butt0_rmva.config(state='disabled')
            self.butt0_load.config(state='disabled')
            self.butt0_unload.config(state='disabled')
            self.butt0_rmv.config(state='disabled')
        else:
            self.data0_optm.config(state='normal')
            self.data0_name.config(state='normal')
            if PSP['head'][PSP['idata']-1]['loaded']:
                self.butt0_load.config(state='disabled')
                self.butt0_unload.config(state='normal')
            else:
                self.butt0_load.config(state='normal')
                self.butt0_unload.config(state='disabled')
            self.butt0_rmv.config(state='normal')
            # Initialize change all buttons
            active = False
            for ip in range(PSP['nf']):
                if not PSP['head'][ip]['loaded']:
                    active = True
                    break
            if active:
                self.butt0_loada.config(state='normal')
                self.butt0_rmva.config(state='normal')
            else:
                self.butt0_loada.config(state='disabled')
                self.butt0_rmva.config(state='disabled')
            active = False
            for ip in range(PSP['nf']):
                if PSP['head'][ip]['loaded']:
                    active = True
                    break
            if active:
                self.butt0_unloada.config(state='normal')
            else:
                self.butt0_unloada.config(state='disabled')
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def display(self, box):
        ''' Initializes the display of the header
        '''

        global PSP
        PSP = source.general.PSP
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Create information label frame
        box0 = LabelFrame(box)
        Grid.rowconfigure(box0,0,weight=1)
        Grid.columnconfigure(box0,0,weight=1)
        if PSP['nf'] == 0:
            text = '\n\nChoose a psp file\n\n'
            self.head = Label(box0, text=text, anchor=CENTER, \
                              justify=CENTER, relief=FLAT, \
                              bd=15, padx=3, pady=3, \
                              font=self.cFont)
        else:
            head = PSP['head'][PSP['idata']-1]
            text = '\n'
            text += ' File: {0}\n'.format(head['nameshort'])
            text += ' Psp version: {0}\n'.format(head['psp_ver'])
            text += ' Comments: {0}\n'.format(head['comments'])
            text += ' Angles: {0} {1}\n'.format(*head['angles'])
            form = ' Dimensions: {0} wavelengths, {1}x{2} ' + \
                   'spatial nodes\n'
            text += form.format(head['nwl'],*head['nodes'])
            form = ' Domain size: {0:8.3e} x {1:8.3e} cm^2\n'
            text += form.format(*head['domain'])
            form = ' Periodicity: [{0},{1}]\n'
            text += form.format(*head['period'])
            form = ' Surface: {0}\n'
            text += form.format(head['surface'])
            text += '\n'
            self.head = Label(box0, text=text, anchor=CENTER, \
                              justify=LEFT, relief=FLAT, \
                              bd=15, padx=3, pady=3, \
                              font=self.cFont)
        self.head.grid(row=0,column=0,sticky=NSEW)
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def buttons(self, box):
        ''' Defines the control buttons of the widget
        '''

        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Data load (box0)
        box0 = LabelFrame(box)
        col = 0
        row = 0
        # Plot XY button
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_plotxy = Button(box0, text="Plot XY", \
                                   font=self.cFont, \
                                   command=self.plotxy1)
        createToolTip(self.butt1_plotxy,TOOLTIPS['10'])
        self.butt1_plotxy.grid(row=row,column=col,sticky=NSEW)
        # Plot slit button
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_plotslit = Button(box0, text="Plot Slit", \
                                     font=self.cFont, \
                                     command=self.plotslit1)
        createToolTip(self.butt1_plotslit,TOOLTIPS['10'])
        self.butt1_plotslit.grid(row=row,column=col,sticky=NSEW)
        # Plot profile button
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_plotprof = Button(box0, text="Plot Profile", \
                                     font=self.cFont, \
                                     command=self.plotprof1)
        createToolTip(self.butt1_plotprof,TOOLTIPS['10'])
        self.butt1_plotprof.grid(row=row,column=col,sticky=NSEW)
        # Close button
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_close = Button(box0, text="Close", \
                                  font=self.cFont, command=self.close)
        createToolTip(self.butt1_close,TOOLTIPS['11'])
        self.butt1_close.grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)


######################################################################
######################################################################
######################################################################
######################################################################

    def choose0(self):
        ''' Method that handles the Choose button. Opens a dialog to
            pick an input file
        '''

        global PATH
        PATH = source.general.PATH
        global PSP
        PSP = source.general.PSP

        # Open the dialog pick file
        filein = tkFileDialog.askopenfilename( \
                                         initialdir=PATH['psp_dir'], \
                                         parent=self)

        # If file selected
        if filein:

            # Reset output
            inpt = {}

            # Try HDF5
            check = self.get_header_h5(filein,inpt)
            if check:
                PSP['head'].append(inpt)
                PSP['data'].append({})
                PSP['nf'] += 1
                self.set_top()
                self.set_mid()
            else:
                inpt = {}
                # Try binary
                check = self.get_header_binary(filein,inpt)
                if check:
                    PSP['head'].append(inpt)
                    PSP['data'].append({})
                    PSP['nf'] += 1
                    self.set_top()
                    self.set_mid()

        return

######################################################################
######################################################################

    def choose0_binary(self):
        ''' Method that handles the Choose button. Opens a dialog to
            pick an input file. Only binary.
        '''

        global PATH
        PATH = source.general.PATH
        global PSP
        PSP = source.general.PSP

        # Open the dialog pick file
        filein = tkFileDialog.askopenfilename( \
                                         initialdir=PATH['psp_dir'], \
                                         parent=self)

        # Check if you selected something
        if filein:
            inpt = {}
            check = self.get_header_binary(filein,inpt)
            if check:
                PSP['head'].append(inpt)
                PSP['data'].append({})
                PSP['nf'] += 1
                self.set_top()
                self.set_mid()
        return

######################################################################
######################################################################

    def choose0_h5(self):
        ''' Method that handles the Choose button. Opens a dialog to
            pick an input file. Only hdf5.
        '''

        global PATH
        PATH = source.general.PATH
        global PSP
        PSP = source.general.PSP

        # Open the dialog pick file
        filein = tkFileDialog.askopenfilename( \
                                         initialdir=PATH['psp_dir'], \
                                         parent=self)

        # Check if you selected something
        if filein:
            inpt = {}
            check = self.get_header_h5(filein,inpt)
            if check:
                PSP['head'].append(inpt)
                PSP['data'].append({})
                PSP['nf'] += 1
                self.set_top()
                self.set_mid()
        return
    
######################################################################
######################################################################

    def set0(self,val):
        ''' Method that handles the dropmenu to select the file to
            manage in the widget
        '''

        global PSP
        PSP = source.general.PSP

        # Set index
        PSP['idata'] = int(val.split(":")[0])
        self.set_top()
        self.set_mid()

        return

######################################################################
######################################################################

    def loada0(self):
        ''' Method that handles the Load all button. Load the axis and
            Stokes parameters for all unloaded psp files
        '''

        global PSP
        PSP = source.general.PSP

        # Indicate we are loading
        self.data0_name.configure(text='Loading',font=self.cFont)
        self.data0_name.update_idletasks()

        # Store current index
        ID = PSP['idata']

        # Go through all files
        for ip in range(PSP['nf']):

            # If loaded, skip
            if PSP['head'][ip]['loaded']:
                continue

            # Change idata
            PSP['idata'] = ip + 1

            # If not loaded, load it
            res = self.get_data()
            self.update_lims()

        # Restore current ID
        PSP['idata'] = ID
        self.set_top()

######################################################################
######################################################################

    def load0(self):
        ''' Method that handles the Load button. Load the axis and
            Stokes parameters from currently selected psp file
        '''

        global PSP
        PSP = source.general.PSP

        # Indicate we are loading
        self.data0_name.configure(text='Loading',font=self.cFont)
        self.data0_name.update_idletasks()

        # Try to read the data
        res = self.get_data()
        self.set_top()
        self.update_lims()

######################################################################
######################################################################

    def unloada0(self):
        ''' Method that handles the Unload all button. Unload the axis
            and Stokes parameters already loaded.
        '''

        global PSP
        PSP = source.general.PSP

        # Store current index
        ID = PSP['idata']

        # Go through all files
        for ip in range(PSP['nf']):

            # If not loaded, skip
            if not PSP['head'][ip]['loaded']:
                continue

            # Change idata
            PSP['idata'] = ip + 1

            # Try to delete the data
            res = self.drop_data()

        # Restore current ID
        PSP['idata'] = ID
        self.set_top()


######################################################################
######################################################################

    def unload0(self):
        ''' Method that handles the Unload button. Unload the axis 
            and Stokes parameters already loaded.
        '''

        global PSP
        PSP = source.general.PSP

        # Try to delete the data
        res = self.drop_data()
        self.set_top()

######################################################################
######################################################################

    def rmva0(self):
        ''' Method that removes all unloaded files from the GUI widget
        '''

        global PSP
        PSP = source.general.PSP

        # Store current index
        ID = PSP['idata']

        # Initialize ip index
        ip = 0

        # Go through all files
        while True:

            # Break condition
            if ip >= PSP['nf']:
                break

            # If loaded, skip
            if PSP['head'][ip]['loaded']:
                ip += 1
                continue

            # Change idata
            PSP['idata'] = ip + 1

            # Try to delete the data
            res = self.drop_data()
            if res:
                PSP['nf'] -= 1
                # Pop the data
                PSP['data'].pop(PSP['idata']-1)
                PSP['head'].pop(PSP['idata']-1)
                # Check if empty
                if PSP['nf'] > 0:
                    # Avoid index overflow
                    if PSP['idata'] > PSP['nf']:
                        PSP['idata'] = PSP['nf']
            else:
                ip += 1
            self.update_lims()

        # Restore current ID
        PSP['idata'] = ID
        self.set_mid()
        self.set_top()

######################################################################
######################################################################

    def rmv0(self):
        ''' Method that removes the currently selected file from the
            GUI widget
        '''

        global PSP
        PSP = source.general.PSP

        # Try to delete the data
        res = self.drop_data()
        if res:
            PSP['nf'] -= 1
            # Pop the data
            PSP['data'].pop(PSP['idata']-1)
            PSP['head'].pop(PSP['idata']-1)
            # Check if empty
            if PSP['nf'] > 0:
                # Avoid index overflow
                if PSP['idata'] > PSP['nf']:
                    PSP['idata'] = PSP['nf']
        self.set_mid()
        self.set_top()
        self.update_lims()

######################################################################
######################################################################

    def plot1(self):
        ''' Method that handles the button Plot. It calls a support
            window of the PSP_plot_class class.
        '''

        global PSP
        PSP = source.general.PSP
        global CONF
        CONF = source.general.CONF

        load = False
        for head in PSP['head']:
            if head['loaded']:
                load = True
                break

        # Check if you loaded the data previously
        if not load:
            tkMessageBox.showwarning("Slit", \
                                     "You have to load at least " + \
                                     "one file.",parent=self)
            return

        # Make current passive
        self.config(background=CONF['col_pas'])

        # Calls the support window
        plot = PSP_plot_class(self.master, title='Plot Window')
        self.grab_set()
        self.lift()

        # Make current active
        self.configure(background=CONF['col_act'])
        
######################################################################
######################################################################

    def plotxy1(self):
        ''' Method that handles the button Plot XY. It calls a support
            window of the PSP_plotxy_class class.
        '''

        global PSP
        PSP = source.general.PSP
        global CONF
        CONF = source.general.CONF

        load = False
        inval = False
        for head in PSP['head']:
            if head['loaded']:
                if head['manyax1'] and head['manyax2']:
                    load = True
                    break
                else:
                    inval = True

        # Check if you loaded the data previously
        if not load:
            if inval:
                tkMessageBox.showwarning("XY", \
                                     "You have to load a valid " + \
                                     "file for XY representation.", \
                                     parent=self)
                return
            else:
                tkMessageBox.showwarning("XY", \
                                     "You have to load at least " + \
                                     "one file.",parent=self)
                return

        # Make current passive
        self.config(background=CONF['col_pas'])

        # Calls the support window
        plotxy = PSP_plotxy_class(self.master, title='Plot Window')
        self.grab_set()
        self.lift()

        # Make current active
        self.configure(background=CONF['col_act'])
        
######################################################################
######################################################################

    def plotslit1(self):
        ''' Method that handles the button Plot Slit. It calls a
            support window of the PSP_plotslit_class class.
        '''

        global PSP
        PSP = source.general.PSP
        global CONF
        CONF = source.general.CONF

        load = False
        inval = False
        for head in PSP['head']:
            if head['loaded']:
                if head['manywav'] and \
                   (head['manyax1'] or head['manyax2']):
                    load = True
                    break
                else:
                    inval = True

        # Check if you loaded the data previously
        if not load:
            if inval:
                tkMessageBox.showwarning("Slit", \
                                     "You have to load a valid " + \
                                     "file for slit " + \
                                     "representation.",parent=self)
                return
            else:
                tkMessageBox.showwarning("Slit", \
                                     "You have to load at least " + \
                                     "one file.",parent=self)
                return

        # Make current passive
        self.config(background=CONF['col_pas'])

        # Calls the support window
        plotslit = PSP_plotslit_class(self.master, \
                                      title='Plot Window')
        self.grab_set()
        self.lift()

        # Make current active
        self.configure(background=CONF['col_act'])
        
######################################################################
######################################################################

    def plotprof1(self):
        ''' Method that handles the button Plot Profile. It calls a
            support window of the PSP_plotprof_class class.
        '''

        global PSP
        PSP = source.general.PSP
        global CONF
        CONF = source.general.CONF

        load = False
        inval = False
        for head in PSP['head']:
            if head['loaded']:
                if head['manywav']:
                    load = True
                    break
                else:
                    inval = True

        # Check if you loaded the data previously
        if not load:
            if inval:
                tkMessageBox.showwarning("Profile", \
                                 "You have to load a valid " + \
                                 "file for profile representation.", \
                                 parent=self)
                return
            else:
                tkMessageBox.showwarning("Profile", \
                                     "You have to load at least " + \
                                     "one file.",parent=self)
                return

        # Make current passive
        self.config(background=CONF['col_pas'])

        # Calls the support window
        plotprof = PSP_plotprof_class(self.master, \
                                      title='Plot Window')
        self.grab_set()
        self.lift()

        # Make current active
        self.configure(background=CONF['col_act'])

######################################################################
######################################################################
######################################################################
######################################################################

    def set_top(self):
        ''' Reconfigures the top of the widget window
        '''

        global PSP
        PSP = source.general.PSP
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        self.data0_optm['menu'].delete(0,'end')
        # OptionMenu
        if PSP['nf'] == 0:
            self.data0_ind.set(TOOLTIPS['4'])
        else:
            self.data0_ind.set("Datum {0}".format(PSP['idata']))
            lst = []
            for jj in range(PSP['nf']):
                self.data0_optm['menu'].add_command( \
                     label = "{0}: {1}".format(jj+1,\
                     PSP['head'][jj]['nameshort']), \
                     command=lambda ii= \
                     "{0}: {1}".format(jj+1,\
                     PSP['head'][jj]['nameshort']): \
                     self.set0(ii))
        # Status
        if PSP['nf'] == 0:
            self.data0_name.config(text=TOOLTIPS['4'], \
                                   font=self.cFont)
            createToolTip(self.data0_name,TOOLTIPS['4'])
        else:
            if PSP['head'][PSP['idata']-1]['loaded']:
                self.data0_name.config(text='Loaded', \
                                       font=self.cFont)
                createToolTip(self.data0_name, TOOLTIPS['5'])
            else:
                self.data0_name.config(text='Not loaded', \
                                       font=self.cFont)
                createToolTip(self.data0_name, TOOLTIPS['6'])
        # Disable widgets if no data loaded
        if PSP['nf'] == 0:
            self.data0_optm.config(state='disabled')
            self.data0_name.config(state='disabled')
            self.butt0_loada.config(state='disabled')
            self.butt0_unloada.config(state='disabled')
            self.butt0_rmva.config(state='disabled')
            self.butt0_load.config(state='disabled')
            self.butt0_unload.config(state='disabled')
            self.butt0_rmv.config(state='disabled')
        else:
            self.data0_optm.config(state='normal')
            self.data0_name.config(state='normal')
            if PSP['head'][PSP['idata']-1]['loaded']:
                self.butt0_load.config(state='disabled')
                self.butt0_unload.config(state='normal')
            else:
                self.butt0_load.config(state='normal')
                self.butt0_unload.config(state='disabled')
            self.butt0_rmv.config(state='normal')
            # Initialize change all buttons
            active = False
            for ip in range(PSP['nf']):
                if not PSP['head'][ip]['loaded']:
                    active = True
                    break
            if active:
                self.butt0_loada.config(state='normal')
                self.butt0_rmva.config(state='normal')
            else:
                self.butt0_loada.config(state='disabled')
                self.butt0_rmva.config(state='disabled')
            active = False
            for ip in range(PSP['nf']):
                if PSP['head'][ip]['loaded']:
                    active = True
                    break
            if active:
                self.butt0_unloada.config(state='normal')
            else:
                self.butt0_unloada.config(state='disabled')

        return

######################################################################
######################################################################

    def set_mid(self):
        ''' Reconfigures the display of the widget window
        '''

        global PSP
        PSP = source.general.PSP
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Update information label frame
        if PSP['nf'] == 0:
            text = '\n\nChoose a psp file\n\n'
            self.head.config(justify=CENTER)
        else:
            head = PSP['head'][PSP['idata']-1]
            text = '\n'
           #text += ' File: {0}\n'.format(head['nameshort'])
            text += ' Psp version: {0}\n'.format(head['psp_ver'])
            text += ' Comments: {0}\n'.format(head['comments'])
            text += ' Angles: {0} {1}\n'.format(*head['angles'])
            form = ' Dimensions: {0} wavelengths, {1}x{2} ' + \
                   'spatial nodes\n'
            text += form.format(head['nwl'],*head['nodes'])
            form = ' Domain size: {0:8.3e} x {1:8.3e} cm^2\n'
            text += form.format(*head['domain'])
            form = ' Periodicity: [{0},{1}]\n'
            text += form.format(*head['period'])
            form = ' Surface: {0}\n'
            text += form.format(head['surface'])
            text += '\n'
            self.head.config(justify=LEFT)
        self.head.config(text=text)

        return


######################################################################
######################################################################
######################################################################
######################################################################

    def get_header_h5(self, filein, inpt):
        ''' Method that reads the header of the selected file. Returns
            false if not a valid psp file and true otherwise.
        '''

        global PSP
        PSP = source.general.PSP
        global CONF
        CONF = source.general.CONF

        # Check if it is a file at all
        if not os.path.isfile(filein):
            return False

        # Check if hdf5
        try:
            with h5py.File(filein, 'r') as f:
                pass
        except IOError:
            return False

        # And try to read the header
        try:
            
            with h5py.File(filein,'r') as f:
                inpt['io'] = "hdf5"
                
                if sys.version_info[0] < 3:
                    inpt['magic'] = f.attrs['IO_PSP_MAGIC']
                else:
                    inpt['magic'] = f.attrs['IO_PSP_MAGIC']. \
                                      decode('utf-8')
                if inpt['magic'] != 'psp':
                    msg = "Magic string {0} not recognized"
                    msg = msg.format(inpt['magic'])
                    tkMessageBox.showwarning("Load",msg,parent=self)
                    return False

                inpt['psp_ver'] = f.attrs['VERSION'][0]
                #  bytes = f.read(1023)
                #  inpt['size'] += 1023
                if sys.version_info[0] < 3:
                    inpt['comments'] = f.attrs['COMMENT']
                else:
                    inpt['comments'] = f.attrs['COMMENT']. \
                                         decode('utf-8')
                # Check if everything in comment is a space
                if inpt['comments'] == \
                   ' '*len(list(inpt['comments'])):
                    inpt['comments'] = ' '
                # Introduce changes of lines
                else:
                    siz = len(list(inpt['comments']))
                    col = 400
                    while col < siz:
                        inpt['comments'] = \
                            inpt['comments'][:col+1] + '\n' + \
                            inpt['comments'][col+1:]
                        col += 402
                        siz += 2
                        
                inpt['angles'] = f.attrs['LOS_THETA'][0], \
                                 f.attrs['LOS_CHI'][0]
                inpt['nwl'] = f.attrs['NUMBER_WAVELENGTHS'][0]
                if inpt['nwl'] == 1:
                    inpt['manywav'] = False
                else:
                    inpt['manywav'] = True

                inpt['lambda'] = np.asarray(f.attrs['WAVELENGTHS'])

                inpt['surface'] = f.attrs['ORIENTATION_' + \
                                          'ATMOSPHERIC_SURFACE'][0]
                if inpt['surface'] == 1:
                    inpt['axis_1_name'] = 'X'
                    inpt['axis_2_name'] = 'Y'
                else:  #DEFAULT
                    inpt['axis_1_name'] = 'X'
                    inpt['axis_2_name'] = 'Y'
                    #inpt['axis_3_name'] = u'\u03BB'
                    inpt['axis_3_name'] = 'Wavelength'

                inpt['domain'] = f.attrs['DOMAIN_SIZE']
                inpt['period'] = f.attrs['PERIODICITY']

                inpt['nodes'] = f.attrs['GRID_DIMENSIONS']
                if inpt['nodes'][0] == 1:
                    inpt['manyax1'] = False
                else:
                    inpt['manyax1'] = True
                    
                if inpt['nodes'][1] == 1:
                    inpt['manyax2'] = False
                else:
                    inpt['manyax2'] = True
                if inpt['manyax1'] or inpt['manyax2']:
                    inpt['manyax'] = True
                else:
                    inpt['manyax'] = False

                inpt['loaded'] = False
                path, filename = os.path.split(filein)
                inpt['name'] = filein
                inpt['nameshort'] = "{0} ".format(filename)
                PATH['psp_dir'] = path
                return True
            
        except IOError:
            raise
            tkMessageBox.showwarning("Load","Could not open file", \
                                     parent=self)
            return False

        except:
            raise
            f.close()
            tkMessageBox.showwarning("Load","Could not open file", \
                                     parent=self)
            return False

######################################################################
######################################################################

    def get_header_binary(self, filein, inpt):
        ''' Method that reads the header of the selected file. Returns
            false if not a valid psp file and true otherwise.
        '''

        global PSP
        PSP = source.general.PSP
        global CONF
        CONF = source.general.CONF

        # Check if it is a file at all
        if not os.path.isfile(filein):
            return False

        # And try to read the header
        try:

            with open(filein,'rb') as f:
                inpt['io'] = "binary"
                
                inpt['size'] = 0
                bytes = f.read(3)
                inpt['size'] += 3
                if sys.version_info[0] < 3:
                    inpt['magic'] = ''.join(struct.unpack( \
                                       CONF['endian'] + 'ccc', bytes))
                else:
                    inpt['magic'] = ''
                    for ii in range(3):
                        byte = bytes[ii:ii+1].decode('utf-8')
                        inpt['magic'] += byte
                if inpt['magic'] != 'psp':
                    msg = "Magic string {0} not recognized"
                    msg = msg.format(inpt['magic'])
                    tkMessageBox.showwarning("Load",msg,parent=self)
                    return False

                bytes = f.read(4)
                inpt['size'] += 4
                inpt['psp_ver'] = int(struct.unpack(CONF['endian'] + \
                                                    'I', bytes)[0])
                bytes = f.read(1023)
                inpt['size'] += 1023
                if sys.version_info[0] < 3:
                    comments = struct.unpack(CONF['endian'] + \
                                             'c'*1023, bytes)
                    inpt['comments'] = ['']
                    for i in range(1023):
                        if comments[i] == '\x00':
                            break
                        else:
                            inpt['comments'].append(comments[i])
                    inpt['comments'] = ''.join(inpt['comments'])
                else:
                    inpt['comments'] = ''
                    for i in range(1023):
                        byte = bytes[i:i+1].decode('utf-8')
                        if byte == '\x00':
                            break
                        else:
                            inpt['comments'] += byte
                # Check if everything in comment is a space
                if inpt['comments'] == \
                   ' '*len(list(inpt['comments'])):
                    inpt['comments'] = ' '
                # Introduce changes of lines
                else:
                    siz = len(list(inpt['comments']))
                    col = 400
                    while col < siz:
                        inpt['comments'] = \
                         inpt['comments'][:col+1] + '\n' + \
                         inpt['comments'][col+1:]
                        col += 402
                        siz += 2
                bytes = f.read(16)
                inpt['size'] += 16
                inpt['angles'] = struct.unpack(CONF['endian'] + \
                                               'dd', bytes)
                bytes = f.read(4)
                inpt['size'] += 4
                inpt['nwl'] = struct.unpack(CONF['endian'] + \
                                            'I', bytes)[0]
                if inpt['nwl'] == 1:
                    inpt['manywav'] = False
                else:
                    inpt['manywav'] = True
                bytes = f.read(8*inpt['nwl'])
                inpt['size'] += 8*inpt['nwl']
                inpt['lambda'] = np.array(struct.unpack( \
                                          CONF['endian'] + 'd'* \
                                          inpt['nwl'], bytes))
                bytes = f.read(4)
                inpt['size'] += 4
                inpt['surface'] = struct.unpack(CONF['endian'] + \
                                         'I', bytes)[0]
                if inpt['surface'] == 1:
                    inpt['axis_1_name'] = 'X'
                    inpt['axis_2_name'] = 'Y'
                else:  #DEFAULT
                    inpt['axis_1_name'] = 'X'
                    inpt['axis_2_name'] = 'Y'
               #inpt['axis_3_name'] = u'\u03BB'
                inpt['axis_3_name'] = 'Wavelength'
                bytes = f.read(16)
                inpt['size'] += 16
                inpt['domain'] = struct.unpack(CONF['endian'] + \
                                      'dd', bytes)
                bytes = f.read(2)
                inpt['size'] += 2
                inpt['period'] = struct.unpack(CONF['endian'] + \
                                      'bb', bytes)
                bytes = f.read(8)
                inpt['size'] += 8
                inpt['nodes'] = struct.unpack(CONF['endian'] + \
                                      'II', bytes)
                if inpt['nodes'][0] == 1:
                    inpt['manyax1'] = False
                else:
                    inpt['manyax1'] = True
                if inpt['nodes'][1] == 1:
                    inpt['manyax2'] = False
                else:
                    inpt['manyax2'] = True
                if inpt['manyax1'] or inpt['manyax2']:
                    inpt['manyax'] = True
                else:
                    inpt['manyax'] = False
                inpt['loaded'] = False
                path, filename = os.path.split(filein)
                inpt['name'] = filein
                inpt['nameshort'] = "{0} ".format(filename)
                PATH['psp_dir'] = path
                return True

        except IOError:
            raise
            tkMessageBox.showwarning("Load","Could not open file", \
                                     parent=self)
            return False

        except:
            raise
            f.close()
            tkMessageBox.showwarning("Load","Could not open file", \
                                     parent=self)
            return False

######################################################################
######################################################################

    def get_data(self):
        ''' Reads the axis and Stokes parameters from the psp file.
            Returns true if the reading went well and false otherwise.
        '''

        global PSP
        PSP = source.general.PSP

        head = PSP['head'][PSP['idata']-1]
        data = PSP['data'][PSP['idata']-1]

        # Check if there is a file at all
        if not os.path.isfile(head['name']):
            return False

        # Reads it as a psp file
        try:
            if head['io'] == "hdf5":
                with h5py.File(head['name'],'r') as f:
                
                    # Read axis
                    data['x'] = np.asarray(f.attrs['X_AXIS'])
                    data['x'] *= 1e-8
                    
                    data['y'] = np.asarray(f.attrs['Y_AXIS'])
                    data['y'] *= 1e-8
                    
                    # Read Stokes
                    Stokes = np.zeros((head['nwl'],head['nodes'][1], \
                                       head['nodes'][0],4))
                    Stokes[:,:,:,0] = f['stokes_I']
                    Stokes[:,:,:,1] = f['stokes_Q']
                    Stokes[:,:,:,2] = f['stokes_U']
                    Stokes[:,:,:,3] = f['stokes_V']
            else:
                with open(head['name'],'rb') as f:

                    # Skip the header
                    f.seek(head['size'])
                    
                    # Read axis
                    bytes = f.read(8*head['nodes'][0])
                    data['x'] = np.array(struct.unpack( \
                                             CONF['endian'] + 'd'* \
                                             head['nodes'][0], bytes))
                    data['x'] *= 1e-8
                    bytes = f.read(8*head['nodes'][1])
                    data['y'] = np.array(struct.unpack( \
                                             CONF['endian'] + 'd'* \
                                             head['nodes'][1], bytes))
                    data['y'] *= 1e-8
                    
                    # Read Stokes
                    bytes = f.read(4*head['nodes'][0]* \
                                     head['nodes'][1]* \
                                     head['nwl']*4)
                    Stokes = np.array(struct.unpack( \
                                          CONF['endian'] + 'f'* \
                                          head['nodes'][0]* \
                                          head['nodes'][1]* \
                                          head['nwl']*4, bytes)). \
                                           reshape(head['nwl'] , \
                                                   head['nodes'][1], \
                                                   head['nodes'][0],4)

            # Check the order of the wavelength vector
            if head['manywav']:
                if head['lambda'][1] < head['lambda'][0]:
                    head['lambda'] = head['lambda'][::-1]
                    Stokes = Stokes[::-1,:,:,:]

            # Convert wavelength to nanometers and store in data
            data['l'] = head['lambda']*1e-1

            # Calculate relative Stokes polarization parameters
            Stokes[:,:,:,1] *= 1e2/Stokes[:,:,:,0]
            Stokes[:,:,:,2] *= 1e2/Stokes[:,:,:,0]
            Stokes[:,:,:,3] *= 1e2/Stokes[:,:,:,0]
            
            # Calculate linear polarization degree
            data['P'] = np.sqrt(Stokes[:,:,:,1]* \
                                Stokes[:,:,:,1] + \
                                Stokes[:,:,:,2]* \
                                Stokes[:,:,:,2])
            
            # Translate into easier words
            data['I'] = Stokes[:,:,:,0]
            data['Log I'] = np.log10(data['I'])
            data['Q/I'] = Stokes[:,:,:,1]
            data['U/I'] = Stokes[:,:,:,2]
            data['V/I'] = Stokes[:,:,:,3]
            Stokes = 0
            
            # Calculate the frequency index where the linear
            # polarization degree is maximum at each spatial
            # point
            data['il_MaxP'] = np.zeros(head['nodes'][0]* \
                                       head['nodes'][1] \
                                ).reshape(head['nodes'][0], \
                                          head['nodes'][1])
            for ix in range(head['nodes'][0]):
                for iy in range(head['nodes'][1]):
                    loc = data['P'][:,iy,ix]
                    data['il_MaxP'][ix,iy] = np.argmax(loc)
                    
            head['loaded'] = True
                    
            return True

        except:

            raise
            return False

######################################################################
######################################################################

    def drop_data(self):
        ''' Drop the load data.
            Returns true if the reading went well and false otherwise.
        '''

        global PSP
        PSP = source.general.PSP

        # Delete the data
        try:
            PSP['data'][PSP['idata']-1] = {}
            PSP['head'][PSP['idata']-1]['loaded'] = False
            return True
        except:
            raise
            return False

######################################################################
######################################################################

    def update_lims(self):
        ''' Updates the primordial limits for the PLOT variable
        '''

        global VARS
        VARS = source.general.VARS
        global PSP
        PSP = source.general.PSP
        global PLOT
        PLOT = source.general.PLOT

        # Check axis limits existence
        if PLOT['min0'] is None:
            PLOT['min0'] = {}
            PLOT['max0'] = {}

        # Compute limits
        for head,data in zip(PSP['head'],PSP['data']):
            if not head['loaded']:
                continue
            for key in (list(VARS['axis_dic'].keys()) + \
                        VARS['SP_list']):
                PLOT['min0'][key] = 1e99
                PLOT['max0'][key] = -1e99
                PLOT['min0'][key] = min([PLOT['min0'][key], \
                                      np.amin(data[key])])
                PLOT['max0'][key] = max([PLOT['max0'][key], \
                                      np.amax(data[key])])


######################################################################
######################################################################
######################################################################
######################################################################
