# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# psp_plotslit.py                                                    #
#                                                                    #
# Tanausú del Pino Alemán                                            #
# Ángel de Vicente                                                   #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Module for plotting psp slit data                                  #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  02/09/2020 - V1.0.9 - Bugfix: The axis formatter using pltxformat #
#                        and pltyformat was not working properly     #
#                        when having more than one plot. Removed     #
#                        those and implemented the formatter with    #
#                        lambda functions (AdV)                      #
#                                                                    #
#  18/05/2020 - V1.0.8 - Bugfix: The size of the sliders were not    #
#                        being updated when changing the plot or the #
#                        plotted file (TdPA)                         #
#                      - Re-introduced a bugfix from @AdV, missing   #
#                        index in a check of PLOTSLIT['shrink']      #
#                                                                    #
#  10/03/2020 - V1.0.7 - Added colors to indicate focus (TdPA)       #
#                      - Solved focus problems (AdV)                 #
#                                                                    #
#  20/08/2019 - V1.0.6 - Compatibility with old libraries regarding  #
#                        NaviationToolbar (TdPA)                     #
#                                                                    #
#  20/05/2019 - V1.0.5 - NavigationToolbar2TkAgg has been deprecated #
#                        since v 2.2 of Matplotlib. Moving back to   #
#                        NavigationToolbar2Tk (AdV)                  #
#                                                                    #
#  22/04/2019 - V1.0.4 - NavigationToolbar2Tk has been deprecated in #
#                        favor of NavigationToolbar2TkAgg. (TdPA)    #
#                                                                    #
#  24/04/2018 - V1.0.3 - Compatibility with python 3+. (TdPA)        #
#                                                                    #
#  22/03/2018 - V1.0.2 - Added sanity check for 'mind'. (TdPA)       #
#                      - Bugfix: Make sure figures are destroyed to  #
#                        avoid memory overflow when using draw many  #
#                        times. (TdPA)                               #
#                      - Bugfix: It was necessary to do deepcopies   #
#                        when appending during the addition of new   #
#                        entries, at least for the dictionaries. Did #
#                        for all of them anyways. (TdPA)             #
#                                                                    #
#  21/03/2018 - V1.0.1 - Only files that can be plotted as slit      #
#                        are added to the file list. (TdPA)          #
#                      - Bugfix: The indexes for wavelength were     #
#                        not being updated, they were trying to edit #
#                        the vertical axis. (TdPA)                   #
#                      - Bugfix: The checks of indexes where the     #
#                        ones for the XY plot. (TdPA)                #
#                                                                    #
#  20/03/2018 - V1.0.0 - First Version. (TdPA)                       #
#                                                                    #
######################################################################
######################################################################
######################################################################

import source.general
from source.loader import *
from source.tooltip import *
from source.colors import *

######################################################################
######################################################################


class PSP_plotslit_class(Toplevel):
    ''' Class that defines the support window for the plot
        configuration
    '''

######################################################################
######################################################################

    def __init__(self, parent, title=None):
        ''' Initialization of the PSP_plot_class class.
        '''

        global CONF
        CONF = source.general.CONF

        # Initialized a support window.
        Toplevel.__init__(self, parent)
        self.transient(parent)

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Set the title if any
        if title:
            self.title(title)

        # Active border
        self.configure(background=CONF['col_act'])

        self.parent = parent
        self.result = None
        self.plt_action = None
        head = Frame(self)
        self.head = head
        body = Frame(head)
        self.up = Frame(body)
        self.do = Frame(body)
        self.head = head
        self.box = body
        self.initial_focus = self.body(body)
        head.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        body.pack(fill=BOTH,expand=1)

        self.grab_set()
        self.lift()
        
        if not self.initial_focus:
            self.initial_focus = self

        # Configuration widgets
        self.optionbox(self.up)

        # Action buttons
        self.buttons(self.do)

        self.up.pack(fill=BOTH,expand=1,side=TOP)
        self.do.pack(fill=BOTH,expand=1,side=BOTTOM)
        body.pack(fill=BOTH,expand=1)
        head.pack(fill=BOTH,expand=1)

        self.protocol("WM_DELETE_WINDOW", self.close)
        self.bind("<Escape>", self.close)
       #self.bind("<Return>", self.update_entry)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)

######################################################################
######################################################################

    def body(self, master):
        ''' Creates the dialog body and set initial focus.
        '''

        return master

######################################################################
######################################################################

    def close(self, event=None):
        ''' Method that handles the window closing.
        '''

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

######################################################################
######################################################################
######################################################################
######################################################################

    def optionbox(self, box):
        ''' Defines all the control widgets of PSP_plotslit_class
            class.
        '''

        global PSP
        PSP = source.general.PSP
        global PLOT
        PLOT = source.general.PLOT
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global VARS
        VARS = source.general.VARS

        # Sanity check
        if PLOTSLIT['iplt'] > PLOTSLIT['nplt']:
            PLOTSLIT['iplt'] = PLOTSLIT['nplt']
        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1
        while not PSP['head'][idata]['loaded'] and \
              not PSP['head'][idata]['manywav']:
            idata += 1
        adata = []
        ii = 0
        for head in PSP['head']:
            if head['loaded']:
                if head['manywav'] and \
                   (head['manyax1'] or head['manyax2']):
                    ii += 1
                    adata.append(head['nameshort'])

        # Initialize cut axis
        if PLOTSLIT['caxis'] is None:
            if PSP['head'][idata]['manyax1']:
                PLOTSLIT['caxis'] = ['y']
                PLOTSLIT['vaxis'] = ['x']
                PLOTSLIT['vcaxis'] = \
                        [{'x': str(PSP['data'][idata]['x'][0]), \
                          'y': str(PSP['data'][idata]['y'][0])}]
            else:
                PLOTSLIT['caxis'] = ['y']
                PLOTSLIT['vaxis'] = ['x']
                PLOTSLIT['vcaxis'] = \
                        [{'x': str(PSP['data'][idata]['x'][0]), \
                          'y': str(PSP['data'][idata]['y'][0])}]

        # Initialize actual plot limits
        if PLOTSLIT['min'] is None:
            PLOTSLIT['min'] = [{}]
            PLOTSLIT['max'] = [{}]
            for key in (list(VARS['axis_dic'].keys()) + \
                        VARS['SP_list']):
                PLOTSLIT['min'][0][key] = str(PLOT['min0'][key])
                PLOTSLIT['max'][0][key] = str(PLOT['max0'][key])

        # Initialize 2D indexes
        if PLOTSLIT['Mind'][0]['x'] is None:
            for key in VARS['axis_dic'].keys():
                num = float(PLOTSLIT['max'][0][key])
                ind = (np.abs(PSP['data'][idata][key]-num)).argmin()
                PLOTSLIT['Mind'][0][key] = ind

        # Sanity check indexes
        if PLOTSLIT['Mind'][iplt]['x'] >= \
           PSP['head'][idata]['nodes'][0]:
            PLOTSLIT['Mind'][iplt]['x'] = \
                                    PSP['head'][idata]['nodes'][0] - 1
            PLOTSLIT['max'][iplt]['x'] = PSP['data'][idata]['x'][ \
                                          PLOTSLIT['Mind'][iplt]['x']]
        if PLOTSLIT['Mind'][iplt]['y'] >= \
           PSP['head'][idata]['nodes'][1]:
            PLOTSLIT['Mind'][iplt]['y'] = \
                                    PSP['head'][idata]['nodes'][1] - 1
            PLOTSLIT['max'][iplt]['y'] = PSP['data'][idata]['y'][ \
                                          PLOTSLIT['Mind'][iplt]['y']]
        if PLOTSLIT['Mind'][iplt]['l'] >= PSP['head'][idata]['nwl']:
            PLOTSLIT['Mind'][iplt]['l'] = \
                                    PSP['head'][idata]['nwl'] - 1
            PLOTSLIT['max'][iplt]['l'] = PSP['data'][idata]['l'][ \
                                          PLOTSLIT['Mind'][iplt]['l']]
        if PLOTSLIT['mind'][iplt]['x'] >= \
           PSP['head'][idata]['nodes'][0]:
            PLOTSLIT['mind'][iplt]['x'] = \
                                    PSP['head'][idata]['nodes'][0] - 1
            PLOTSLIT['min'][iplt]['x'] = PSP['data'][idata]['x'][ \
                                          PLOTSLIT['mind'][iplt]['x']]
        if PLOTSLIT['mind'][iplt]['y'] >= \
           PSP['head'][idata]['nodes'][1]:
            PLOTSLIT['mind'][iplt]['y'] = \
                                    PSP['head'][idata]['nodes'][1] - 1
            PLOTSLIT['min'][iplt]['y'] = PSP['data'][idata]['y'][ \
                                          PLOTSLIT['mind'][iplt]['y']]
        if PLOTSLIT['mind'][iplt]['l'] >= PSP['head'][idata]['nwl']:
            PLOTSLIT['mind'][iplt]['l'] = \
                                    PSP['head'][idata]['nwl'] - 1
            PLOTSLIT['min'][iplt]['l'] = PSP['data'][idata]['l'][ \
                                          PLOTSLIT['mind'][iplt]['l']]

        # Possible axis to choose from
        pcaxis = []
        if PSP['head'][idata]['manyax2']:
            pcaxis.append(PSP['head'][idata]['axis_1_name'])
        if PSP['head'][idata]['manyax1']:
            pcaxis.append(PSP['head'][idata]['axis_2_name'])
        if PSP['head'][idata] \
              [VARS['axis_dic'][PLOTSLIT['caxis'][iplt]]] \
           not in pcaxis:
            if PSP['head'][idata]['axis_1_name'] in pcaxis:
                PLOTSLIT['caxis'][iplt] = 'x'
            else:
                PLOTSLIT['caxis'][iplt] = 'y'

        if PLOTSLIT['icaxis'][iplt]['x'] >= \
           PSP['head'][idata]['nodes'][0]:
            PLOTSLIT['icaxis'][iplt]['x'] = \
                                    PSP['head'][idata]['nodes'][0] - 1
            PLOTSLIT['vcaxis'][iplt]['x'] = PSP['data'][idata]['x'][ \
                                        PLOTSLIT['icaxis'][iplt]['x']]
        if PLOTSLIT['icaxis'][iplt]['y'] >= \
           PSP['head'][idata]['nodes'][1]:
            PLOTSLIT['icaxis'][iplt]['y'] = \
                                    PSP['head'][idata]['nodes'][1] - 1
            PLOTSLIT['vcaxis'][iplt]['y'] = PSP['data'][idata]['y'][ \
                                        PLOTSLIT['icaxis'][iplt]['y']]

        # Widget padding
        padx = 5
        pady = padx
        ipadx = 3
        ipady = ipadx


        # Number of plots
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l0 = Label(box0, text="Plot Selection",justify=CENTER, \
                     font=self.cFont, anchor=CENTER)
        b0l0.grid(row=row,column=col,columnspan=5,sticky=NSEW)
        # Label
        row += 1
        col = 0
        self.nplt0_val = IntVar()
        self.nplt0_val.set(PLOTSLIT['nplt'])
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l1 = Label(box0, text="Number of plots:",justify=RIGHT, \
                     font=self.cFont, anchor=E)
        b0l1.grid(row=row,column=col,sticky=NSEW)
        col += 1
        # Number display
        Grid.columnconfigure(box0,col,weight=1)
        self.nplt0_label = Label(box0, \
                                text="{0}".format(PLOTSLIT['nplt']), \
                                 font=self.cFont)
        createToolTip(self.nplt0_label,TOOLTIPS['12'])
        self.nplt0_label.grid(row=row,column=col,sticky=NSEW)
        # Minus button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.nplt0_bmin = Button(box0, text="-", font=self.cFont, \
                                 command=self.min0)
        createToolTip(self.nplt0_bmin,TOOLTIPS['-'])
        self.nplt0_bmin.grid(row=row,column=col,sticky=NSEW)
        # Plus button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.nplt0_bplu = Button(box0, text="+", font=self.cFont, \
                                 command=self.plu0)
        createToolTip(self.nplt0_bplu,TOOLTIPS['+'])
        self.nplt0_bplu.grid(row=row,column=col,sticky=NSEW)
        # Plot to edit
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.wplt0_ind = StringVar()
        self.wplt0_ind.set("Plot {0}".format(PLOTSLIT['iplt']))
        lst = []
        for ii in range(1,PLOTSLIT['nplt']+1):
            lst.append("Plot {0}".format(ii))
        self.wplt0_optm = OptionMenu(box0, self.wplt0_ind, \
                                     *lst, command=self.set0)
        createToolTip(self.wplt0_optm,TOOLTIPS['13'])
        self.wplt0_optm.config(font=self.cFont)
        self.wplt0_optm.nametowidget(self.wplt0_optm.menuname). \
                                          config(font=self.cFont) 
        self.wplt0_optm.grid(row=row,column=col,sticky=NSEW)
        # Pack box0
        box0.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)



        # Files
        box1 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box1,row,weight=1)
        Grid.columnconfigure(box1,col,weight=1)
        b1l0 = Label(box1, text="File Selection",justify=CENTER, \
                     font=self.cFont, anchor=CENTER)
        b1l0.grid(row=row,column=col,sticky=NSEW)
        # File selection
        row += 1
        col = 0
        Grid.rowconfigure(box1,row,weight=1)
        Grid.columnconfigure(box1,col,weight=1)
        self.wdat1_ind = StringVar()
        self.wdat1_ind.set(adata[idata])
        lst = []
        for ii in range(1,len(adata)+1):
            lst.append('{0}: {1}'.format(ii,adata[ii-1]))
        self.wdat1_optm = OptionMenu(box1, self.wdat1_ind, \
                                     *lst, command=self.set1)
        createToolTip(self.wdat1_optm,TOOLTIPS['17'])
        self.wdat1_optm.config(font=self.cFont)
        self.wdat1_optm.nametowidget(self.wdat1_optm.menuname). \
                                          config(font=self.cFont) 
        self.wdat1_optm.grid(row=row,column=col,sticky=NSEW)
        # Pack box1
        box1.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)



        # Variable to plot
        box2 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box2,row,weight=1)
        Grid.columnconfigure(box2,col,weight=1)
        b2l = Label(box2, text="Variable Selection",justify=CENTER, \
                     font=self.cFont, anchor=CENTER)
        b2l.grid(row=row,column=col,columnspan=6,sticky=NSEW)
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box2,row,weight=1)
        Grid.columnconfigure(box2,col,weight=1)
        b2l0 = Label(box2, text="Variable to plot:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        b2l0.grid(row=row,column=col,sticky=NSEW)
        # Variable selection
        col += 1
        axis = PLOTSLIT['axis'][iplt]
        Grid.columnconfigure(box2,col,weight=1)
        self.wvar2_ind = StringVar()
        self.wvar2_ind.set(axis)
        lst = VARS['SP_list']
        self.wvar2_optm = OptionMenu(box2, self.wvar2_ind, \
                                     *lst, command=self.set2)
        createToolTip(self.wvar2_optm,TOOLTIPS['22'])
        self.wvar2_optm.config(font=self.cFont)
        self.wvar2_optm.nametowidget(self.wvar2_optm.menuname). \
                                          config(font=self.cFont) 
        self.wvar2_optm.grid(row=row,column=col,sticky=NSEW)
        # Minimum
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box2,row,weight=1)
        Grid.columnconfigure(box2,col,weight=1)
        self.b2l1 = Label(box2, text="Minimum:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b2l1.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box2,col,weight=1)
        self.min2_name = StringVar()
        self.min2_name.set(PLOTSLIT['min'][iplt][axis])
        self.min2_name.trace('w',self.min2_val)
        self.min2_entry = Entry(box2, font=self.cFont, \
                                textvariable=self.min2_name)
        self.min2_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box2,col,weight=1)
        self.auni21_ind = StringVar()
        self.auni21_ind.set("{0}".format(VARS['aunit_dic'][axis]))
        self.auni21_label = Label(box2, \
                                  text=self.auni21_ind.get(), \
                                  font=self.cFont)
        self.auni21_label.grid(row=row,column=col,sticky=NSEW)
        # Maximum
        # Label
        col += 1
        Grid.columnconfigure(box2,col,weight=1)
        self.b2l2 = Label(box2, text="Maximum:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b2l2.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box2,col,weight=1)
        self.max2_name = StringVar()
        self.max2_name.set(PLOTSLIT['max'][iplt][axis])
        self.max2_name.trace('w',self.max2_val)
        self.max2_entry = Entry(box2, font=self.cFont, \
                                textvariable=self.max2_name)
        self.max2_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box2,col,weight=1)
        self.auni22_ind = StringVar()
        self.auni22_ind.set("{0}".format(VARS['aunit_dic'][axis]))
        self.auni22_label = Label(box2, \
                                  text=self.auni22_ind.get(), \
                                  font=self.cFont)
        self.auni22_label.grid(row=row,column=col,sticky=NSEW)
        # Reset button
        row += 1
        col = 0
        Grid.columnconfigure(box2,col,weight=1)
        self.lim2_bres = Button(box2, text="Reset", font=self.cFont, \
                                 command=self.res2)
        createToolTip(self.lim2_bres,TOOLTIPS['31'])
        self.lim2_bres.grid(row=row,column=col,columnspan=3, \
                            sticky=NSEW)
        col += 2
        # Fit button
        col += 1
        Grid.columnconfigure(box2,col,weight=1)
        self.lim2_bfit = Button(box2, text="Fit", font=self.cFont, \
                                 command=self.fit2)
        createToolTip(self.lim2_bfit,TOOLTIPS['32'])
        self.lim2_bfit.grid(row=row,column=col,columnspan=3, \
                            sticky=NSEW)
        # Pack box2
        box2.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)



        # Cut Axis
        box3 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box3,row,weight=1)
        Grid.columnconfigure(box3,col,weight=1)
        b3l = Label(box3, text="Cut Axis", \
                    justify=CENTER,font=self.cFont, anchor=CENTER)
        b3l.grid(row=row,column=col,columnspan=4,sticky=NSEW)
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box3,row,weight=1)
        Grid.columnconfigure(box3,col,weight=1)
        b3l0 = Label(box3, text="Cut axis:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        b3l0.grid(row=row,column=col,sticky=NSEW)
        # Axis selection
        col += 1
        caxis = PLOTSLIT['caxis'][iplt]
        Grid.columnconfigure(box2,col,weight=1)
        self.caxis3_ind = StringVar()
        self.caxis3_ind.set(PSP['head'][idata] \
                               [VARS['axis_dic'][caxis]])
        self.caxis3_optm = OptionMenu(box3, self.caxis3_ind, \
                                      *pcaxis, command=self.set31)
        createToolTip(self.caxis3_optm,TOOLTIPS['33'])
        self.caxis3_optm.config(font=self.cFont)
        self.caxis3_optm.nametowidget(self.caxis3_optm.menuname). \
                                          config(font=self.cFont) 
        self.caxis3_optm.grid(row=row,column=col,sticky=NSEW)
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box3,row,weight=1)
        Grid.columnconfigure(box3,col,weight=1)
        b3l1 = Label(box3, text="Cut point:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        b3l1.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.caxis3_name = StringVar()
        self.caxis3_name.set(PLOTSLIT['vcaxis'][iplt][caxis])
        self.caxis3_name.trace('w',self.caxis3_val)
        self.caxis3_entry = Entry(box3, font=self.cFont, \
                                textvariable=self.caxis3_name)
        self.caxis3_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.auni31_label = Label(box3,text='Mm', \
                                  font=self.cFont,anchor=W)
        self.auni31_label.grid(row=row,column=col,sticky=NSEW)
        # Index label
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.aind31_name = StringVar()
        self.aind31_name.set('Index {0}'.format( \
                             PLOTSLIT['icaxis'][iplt][caxis]+1))
        self.aind31_label = Label(box3,text=self.aind31_name.get(), \
                                  font=self.cFont)
        self.aind31_label.grid(row=row,column=col,sticky=NSEW)
        # Slider
        row += 1
        col = 1
        Grid.rowconfigure(box3,row,weight=1)
        Grid.columnconfigure(box3,col,weight=1)
        self.caxis3_sind = IntVar()
        self.caxis3_sind.set(PLOTSLIT['icaxis'][iplt])
        self.caxis3_slide = Scale(box3, from_=0, \
                            to=PSP['head'][idata]['nodes'] \
                                  [VARS['axis_ind'][caxis]]-1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.caxis3_sind, \
                            font=self.cFont, \
                            command=self.set32)
        self.caxis3_slide.grid(row=row,column=col,sticky=NSEW)
        # Pack box3
        box3.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)


        # Space
        box4 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box4,row,weight=1)
        Grid.columnconfigure(box4,col,weight=1)
        b4l = Label(box4, text="2D Range Selection", \
                    justify=CENTER,font=self.cFont, anchor=CENTER)
        b4l.grid(row=row,column=col,columnspan=8,sticky=NSEW)
        # Minimum lambda
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box4,row,weight=1)
        Grid.columnconfigure(box4,col,weight=1)
        self.xmil4_name = StringVar()
        self.xmil4_name.set(u'Minimum \u03BB:')
        self.b4l0 = Label(box4, text=self.xmil4_name.get(), \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b4l0.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.xmin4_name = StringVar()
        self.xmin4_name.set(PLOTSLIT['min'][iplt]['l'])
        self.xmin4_name.trace('w',self.xmin4_val)
        self.xmin4_entry = Entry(box4, font=self.cFont, \
                                textvariable=self.xmin4_name)
        self.xmin4_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.xmiu4_label = Label(box4,text='nm', \
                                  font=self.cFont, anchor=W)
        self.xmiu4_label.grid(row=row,column=col,sticky=NSEW)
        # Index label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.aind41_name = StringVar()
        self.aind41_name.set('Index {0}'.format( \
                             PLOTSLIT['mind'][iplt]['l']+1))
        self.aind41_label = Label(box4,text=self.aind41_name.get(), \
                                  font=self.cFont)
        self.aind41_label.grid(row=row,column=col,sticky=NSEW)
        # Maximum x
        # Label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.xmal4_name = StringVar()
        self.xmal4_name.set(u'Maximum \u03BB:')
        self.b4l1 = Label(box4, text=self.xmal4_name.get(), \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b4l1.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.xmax4_name = StringVar()
        self.xmax4_name.set(PLOTSLIT['max'][iplt]['l'])
        self.xmax4_name.trace('w',self.xmax4_val)
        self.xmax4_entry = Entry(box4, font=self.cFont, \
                                textvariable=self.xmax4_name)
        self.xmax4_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.xmau4_label = Label(box4,text='nm', \
                                 font=self.cFont, anchor=W)
        self.xmau4_label.grid(row=row,column=col,sticky=NSEW)
        # Index label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.aind42_name = StringVar()
        self.aind42_name.set('Index {0}'.format( \
                             PLOTSLIT['Mind'][iplt]['l']+1))
        self.aind42_label = Label(box4,text=self.aind42_name.get(), \
                                  font=self.cFont)
        self.aind42_label.grid(row=row,column=col,sticky=NSEW)
        # Slider minimum
        row += 1
        col = 1
        Grid.rowconfigure(box4,row,weight=1)
        Grid.columnconfigure(box4,col,weight=1)
        self.xmi4_ind = IntVar()
        self.xmi4_ind.set(PLOTSLIT['mind'][iplt]['l'])
        self.xmi4_slide = Scale(box4, from_=0, \
                            to=PSP['head'][idata]['nwl']-1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.xmi4_ind, \
                            font=self.cFont, \
                            command=self.set41)
        self.xmi4_slide.grid(row=row,column=col,sticky=NSEW)
        # Slider maximum
        col += 4
        Grid.columnconfigure(box4,col,weight=1)
        self.xma4_ind = IntVar()
        self.xma4_ind.set(PLOTSLIT['Mind'][iplt]['l'])
        self.xma4_slide = Scale(box4, from_=0, \
                            to=PSP['head'][idata]['nwl']-1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.xma4_ind, \
                            font=self.cFont, \
                            command=self.set42)
        self.xma4_slide.grid(row=row,column=col,sticky=NSEW)
        # Get Vertical axis
        vaxis = PLOTSLIT['vaxis'][iplt]
        # Minimum y
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box4,row,weight=1)
        Grid.columnconfigure(box4,col,weight=1)
        self.ymil4_name = StringVar()
        self.ymil4_name.set("Minimum {0}:".format( \
                         PSP['head'][idata][VARS['axis_dic'][vaxis]]))
        self.b4l3 = Label(box4, text=self.ymil4_name.get(), \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b4l3.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.ymin4_name = StringVar()
        self.ymin4_name.set(PLOTSLIT['min'][iplt][vaxis])
        self.ymin4_name.trace('w',self.ymin4_val)
        self.ymin4_entry = Entry(box4, font=self.cFont, \
                                textvariable=self.ymin4_name)
        self.ymin4_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.ymiu4_label = Label(box4,text='Mm', \
                                  font=self.cFont, anchor=W)
        self.ymiu4_label.grid(row=row,column=col,sticky=NSEW)
        # Index label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.aind43_name = StringVar()
        self.aind43_name.set('Index {0}'.format( \
                             PLOTSLIT['mind'][iplt][vaxis]+1))
        self.aind43_label = Label(box4,text=self.aind43_name.get(), \
                                  font=self.cFont)
        self.aind43_label.grid(row=row,column=col,sticky=NSEW)
        # Maximum x
        # Label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.ymal4_name = StringVar()
        self.ymal4_name.set("Maximum {0}:".format( \
                         PSP['head'][idata][VARS['axis_dic'][vaxis]]))
        self.b4l4 = Label(box4, text=self.ymal4_name.get(), \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b4l4.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.ymax4_name = StringVar()
        self.ymax4_name.set(PLOTSLIT['max'][iplt][vaxis])
        self.ymax4_name.trace('w',self.ymax4_val)
        self.ymax4_entry = Entry(box4, font=self.cFont, \
                                textvariable=self.ymax4_name)
        self.ymax4_entry.grid(row=row,column=col,sticky=NSEW)
        # Unit label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.ymau4_label = Label(box4,text='Mm', \
                                  font=self.cFont, anchor=W)
        self.ymau4_label.grid(row=row,column=col,sticky=NSEW)
        # Index label
        col += 1
        Grid.columnconfigure(box4,col,weight=1)
        self.aind44_name = StringVar()
        self.aind44_name.set('Index {0}'.format( \
                             PLOTSLIT['Mind'][iplt][vaxis]+1))
        self.aind44_label = Label(box4,text=self.aind44_name.get(), \
                                  font=self.cFont)
        self.aind44_label.grid(row=row,column=col,sticky=NSEW)
        # Slider minimum
        row += 1
        col = 1
        Grid.rowconfigure(box4,row,weight=1)
        Grid.columnconfigure(box4,col,weight=1)
        nodes = PSP['head'][idata]['nodes'][VARS['axis_ind'][vaxis]]
        self.ymi4_ind = IntVar()
        self.ymi4_ind.set(PLOTSLIT['mind'][iplt][vaxis])
        self.ymi4_slide = Scale(box4, from_=0, to=nodes-1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.ymi4_ind, \
                            font=self.cFont, \
                            command=self.set43)
        self.ymi4_slide.grid(row=row,column=col,sticky=NSEW)
        # Slider maximum
        col += 4
        Grid.columnconfigure(box4,col,weight=1)
        self.yma4_ind = IntVar()
        self.yma4_ind.set(PLOTSLIT['Mind'][iplt][vaxis])
        self.yma4_slide = Scale(box4, from_=0, to=nodes-1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.yma4_ind, \
                            font=self.cFont, \
                            command=self.set44)
        self.yma4_slide.grid(row=row,column=col,sticky=NSEW)
        # Check for shrinking
        row += 1
        col = 0
        Grid.rowconfigure(box4,row,weight=1)
        Grid.columnconfigure(box4,col,weight=1)
        self.shr4_stat = IntVar()
        if PLOTSLIT['shrink'][iplt]:
            self.shr4_stat.set(1)
        else:
            self.shr4_stat.set(0)
        self.shr4_check = Checkbutton(box4, text="Shrink", \
                                      font=self.cFont, \
                                      variable=self.shr4_stat, \
                                      command=self.check4)
        createToolTip(self.shr4_check,text=TOOLTIPS['25'])
        self.shr4_check.grid(row=row,column=col,columnspan=8, \
                             sticky=NSEW)
        # Pack box4
        box4.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)


        # Colormap
        box5 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box5,row,weight=1)
        Grid.columnconfigure(box5,col,weight=1)
        b5l = Label(box5, text="Colormap Selection", \
                    justify=CENTER,font=self.cFont, anchor=CENTER)
        b5l.grid(row=row,column=col,columnspan=2,sticky=NSEW)
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box5,row,weight=1)
        Grid.columnconfigure(box5,col,weight=1)
        b5l0 = Label(box5, text="Colormap:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        b5l0.grid(row=row,column=col,sticky=NSEW)
        # Name
        col += 1
        Grid.columnconfigure(box5,col,weight=1)
        self.wcol5_name = StringVar()
        self.wcol5_name.set(VARS['palet_name'] \
                                [PLOTSLIT['color'][iplt]])
        self.wcol5_lab = Label(box5, text=self.wcol5_name.get(), \
                              justify=RIGHT,font=self.cFont, anchor=W)
        self.wcol5_lab.grid(row=row,column=col,sticky=NSEW)
        # Variable selection slider
        row += 1
        col = 0
        Grid.rowconfigure(box5,row,weight=1)
        Grid.columnconfigure(box5,col,weight=1)
        self.wcol5_ind = IntVar()
        self.wcol5_ind.set(PLOTSLIT['color'][iplt])
        self.wcol5_slide = Scale(box5, from_=0, \
                            to=len(VARS['palet_name'])-1, \
                            orient=HORIZONTAL, showvalue=0, \
                            variable=self.wcol5_ind, \
                            command=self.set5)
        self.wcol5_slide.grid(row=row,column=col,columnspan=2, \
                              sticky=NSEW)
        # Pack box5
        box5.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)


        # Axis Format and font size
        box6 = LabelFrame(box)
        row = 0
        col = 0
        # Label title
        Grid.rowconfigure(box6,row,weight=1)
        Grid.columnconfigure(box6,col,weight=1)
        b6l0 = Label(box6, text="Format Axis Selection", \
                          justify=CENTER,font=self.cFont, \
                          anchor=CENTER)
        b6l0.grid(row=row,column=col,columnspan=4,sticky=NSEW)
        # Colorbar format
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box6,row,weight=1)
        Grid.columnconfigure(box6,col,weight=1)
        b6l1 = Label(box6, text="Color bar format:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        b6l1.grid(row=row,column=col,columnspan=2,sticky=NSEW)
        col += 1
        # Entry
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.zfor6_name = StringVar()
        if PLOTSLIT['format'][iplt]['z'] is None:
            self.zfor6_name.set('None')
        else:
            self.zfor6_name.set(PLOTSLIT['format'][iplt]['z'])
        self.zfor6_name.trace('w',self.zfor6_val)
        self.zfor6_entry = Entry(box6, font=self.cFont, \
                                 textvariable=self.zfor6_name)
        self.zfor6_entry.grid(row=row,column=col,columnspan=2, \
                              sticky=NSEW)
        # Colorbar font size
        # Check
        row += 1
        col = 0
        Grid.rowconfigure(box6,row,weight=1)
        Grid.columnconfigure(box6,col,weight=1)
        self.zfs6_stat = IntVar()
        if PLOTSLIT['cfont'][iplt]['z']:
            self.zfs6_stat.set(1)
        else:
            self.zfs6_stat.set(0)
        self.zfs6_check = Checkbutton(box6, \
                                      text="Change color bar " + \
                                           "font size:", \
                                      anchor=W, \
                                      font=self.cFont, \
                                      variable=self.zfs6_stat, \
                                      command=self.check60)
        createToolTip(self.zfs6_check,text=TOOLTIPS['27'])
        self.zfs6_check.grid(row=row,column=col,columnspan=1, \
                             sticky=NSEW)
        # Label
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.zfs6_lab = Label(box6, \
                          text=str(PLOTSLIT['sfont'][iplt]['z']), \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.zfs6_lab.grid(row=row,column=col,sticky=NSEW)
        # Minus button
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.zfs6_bmin = Button(box6, text="-", font=self.cFont, \
                                command=self.min60)
        createToolTip(self.zfs6_bmin,TOOLTIPS['-'])
        self.zfs6_bmin.grid(row=row,column=col,sticky=NSEW)
        # Plus button
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.zfs6_bplu = Button(box6, text="+", font=self.cFont, \
                                command=self.plu60)
        createToolTip(self.zfs6_bplu,TOOLTIPS['+'])
        self.zfs6_bplu.grid(row=row,column=col,sticky=NSEW)
        # Disable change of font
        if PLOTSLIT['cfont'][iplt]['z']:
            self.zfs6_lab.config(state='normal')
            self.zfs6_bmin.config(state='normal')
            self.zfs6_bplu.config(state='normal')
        else:
            self.zfs6_lab.config(state='disabled')
            self.zfs6_bmin.config(state='disabled')
            self.zfs6_bplu.config(state='disabled')
        # X axis format
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box6,row,weight=1)
        Grid.columnconfigure(box6,col,weight=1)
        b6l2 = Label(box6, text="Horizontal axis format:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        b6l2.grid(row=row,column=col,columnspan=2,sticky=NSEW)
        col += 1
        # Entry
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.xfor6_name = StringVar()
        if PLOTSLIT['format'][iplt]['x'] is None:
            self.xfor6_name.set('None')
        else:
            self.xfor6_name.set(PLOTSLIT['format'][iplt]['x'])
        self.xfor6_name.trace('w',self.xfor6_val)
        self.xfor6_entry = Entry(box6, font=self.cFont, \
                                 textvariable=self.xfor6_name)
        self.xfor6_entry.grid(row=row,column=col,columnspan=2, \
                              sticky=NSEW)
        # X axis font size
        # Check
        row += 1
        col = 0
        Grid.rowconfigure(box6,row,weight=1)
        Grid.columnconfigure(box6,col,weight=1)
        self.xfs6_stat = IntVar()
        if PLOTSLIT['cfont'][iplt]['x']:
            self.xfs6_stat.set(1)
        else:
            self.xfs6_stat.set(0)
        self.xfs6_check = Checkbutton(box6, \
                                      text="Change horizontal " + \
                                           "axis font size:", \
                                      anchor=W, \
                                      font=self.cFont, \
                                      variable=self.xfs6_stat, \
                                      command=self.check61)
        createToolTip(self.xfs6_check,text=TOOLTIPS['27'])
        self.xfs6_check.grid(row=row,column=col,sticky=NSEW)
        # Label
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.xfs6_lab = Label(box6, \
                          text=str(PLOTSLIT['sfont'][iplt]['x']), \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.xfs6_lab.grid(row=row,column=col,sticky=NSEW)
        # Minus button
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.xfs6_bmin = Button(box6, text="-", font=self.cFont, \
                                command=self.min61)
        createToolTip(self.xfs6_bmin,TOOLTIPS['-'])
        self.xfs6_bmin.grid(row=row,column=col,sticky=NSEW)
        # Plus button
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.xfs6_bplu = Button(box6, text="+", font=self.cFont, \
                                command=self.plu61)
        createToolTip(self.xfs6_bplu,TOOLTIPS['+'])
        self.xfs6_bplu.grid(row=row,column=col,sticky=NSEW)
        # Disable change of font
        if PLOTSLIT['cfont'][iplt]['x']:
            self.xfs6_lab.config(state='normal')
            self.xfs6_bmin.config(state='normal')
            self.xfs6_bplu.config(state='normal')
        else:
            self.xfs6_lab.config(state='disabled')
            self.xfs6_bmin.config(state='disabled')
            self.xfs6_bplu.config(state='disabled')
        # Y axis format
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box6,row,weight=1)
        Grid.columnconfigure(box6,col,weight=1)
        b6l3 = Label(box6, text="Vertical axis format:", \
                          justify=RIGHT,font=self.cFont, anchor=E)
        b6l3.grid(row=row,column=col,columnspan=2,sticky=NSEW)
        col += 1
        # Entry
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.yfor6_name = StringVar()
        if PLOTSLIT['format'][iplt]['y'] is None:
            self.yfor6_name.set('None')
        else:
            self.yfor6_name.set(PLOTSLIT['format'][iplt]['y'])
        self.yfor6_name.trace('w',self.yfor6_val)
        self.yfor6_entry = Entry(box6, font=self.cFont, \
                                 textvariable=self.yfor6_name)
        self.yfor6_entry.grid(row=row,column=col,columnspan=2, \
                              sticky=NSEW)
        # X axis font size
        # Check
        row += 1
        col = 0
        Grid.rowconfigure(box6,row,weight=1)
        Grid.columnconfigure(box6,col,weight=1)
        self.yfs6_stat = IntVar()
        if PLOTSLIT['cfont'][iplt]['y']:
            self.yfs6_stat.set(1)
        else:
            self.yfs6_stat.set(0)
        self.yfs6_check = Checkbutton(box6, \
                                      text="Change vertical " + \
                                           "axis font size:", \
                                      anchor=W, \
                                      font=self.cFont, \
                                      variable=self.yfs6_stat, \
                                      command=self.check62)
        createToolTip(self.yfs6_check,text=TOOLTIPS['27'])
        self.yfs6_check.grid(row=row,column=col,sticky=NSEW)
        # Label
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.yfs6_lab = Label(box6, \
                          text=str(PLOTSLIT['sfont'][iplt]['y']), \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.yfs6_lab.grid(row=row,column=col,sticky=NSEW)
        # Minus button
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.yfs6_bmin = Button(box6, text="-", font=self.cFont, \
                                command=self.min62)
        createToolTip(self.yfs6_bmin,TOOLTIPS['-'])
        self.yfs6_bmin.grid(row=row,column=col,sticky=NSEW)
        # Plus button
        col += 1
        Grid.columnconfigure(box6,col,weight=1)
        self.yfs6_bplu = Button(box6, text="+", font=self.cFont, \
                                command=self.plu62)
        createToolTip(self.yfs6_bplu,TOOLTIPS['+'])
        self.yfs6_bplu.grid(row=row,column=col,sticky=NSEW)
        # Disable change of font
        if PLOTSLIT['cfont'][iplt]['y']:
            self.yfs6_lab.config(state='normal')
            self.yfs6_bmin.config(state='normal')
            self.yfs6_bplu.config(state='normal')
        else:
            self.yfs6_lab.config(state='disabled')
            self.yfs6_bmin.config(state='disabled')
            self.yfs6_bplu.config(state='disabled')
        # Pack box6
        box6.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)


######################################################################
######################################################################

    def buttons(self, box):
        ''' Defines the control buttons of the widget
        '''

        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # Save configuration button
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt_draw = Button(box0, text="Save configuration", \
                                font=self.cFont, \
                                command=self.save)
        createToolTip(self.butt_draw,TOOLTIPS['28'])
        self.butt_draw.grid(row=row,column=col,sticky=NSEW)
        # Restore configuration button
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt_draw = Button(box0, \
                                text="Load configuration", \
                                font=self.cFont, \
                                command=self.load)
        createToolTip(self.butt_draw,TOOLTIPS['29'])
        self.butt_draw.grid(row=row,column=col,sticky=NSEW)
        # Reset configuration button
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt_draw = Button(box0, \
                                text="Reset configuration", \
                                font=self.cFont, \
                                command=self.reset)
        createToolTip(self.butt_draw,TOOLTIPS['30'])
        self.butt_draw.grid(row=row,column=col,sticky=NSEW)
        # Plot button
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt_draw = Button(box0, text="Draw", font=self.cFont, \
                                 command=self.draw)
        createToolTip(self.butt_draw,TOOLTIPS['18'])
        self.butt_draw.grid(row=row,column=col,sticky=NSEW)
        # Close button
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt_close = Button(box0, text="Close", \
                                  font=self.cFont, command=self.close)
        createToolTip(self.butt_close,TOOLTIPS['11'])
        self.butt_close.grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)


######################################################################
######################################################################
######################################################################
######################################################################

    def min0(self):
        ''' Reduce in 1 the number of plots
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        if PLOTSLIT['nplt'] <= 1:
            return
        PLOTSLIT['nplt'] -= 1
        if PLOTSLIT['iplt'] > PLOTSLIT['nplt']:
            PLOTSLIT['iplt'] = PLOTSLIT['nplt']
        self.set_top()

        return

######################################################################
######################################################################

    def plu0(self):
        ''' Increse in 1 the number of plots
        '''

        global PLOT
        PLOT = source.general.PLOT
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global PLOTSLIT_def
        PLOTSLIT_def = source.general.PLOTSLIT_def

        if PLOTSLIT['nplt'] >= 9:
            return

        PLOTSLIT['nplt'] += 1

        while len(PLOTSLIT['idata']) < PLOTSLIT['nplt']:
            PLOTSLIT['idata'].append(copy.deepcopy( \
                             PLOTSLIT_def['idata'][0]))
            PLOTSLIT['axis'].append(copy.deepcopy( \
                             PLOTSLIT_def['axis'][0]))
            PLOTSLIT['shrink'].append(copy.deepcopy( \
                             PLOTSLIT['shrink'][0]))
            PLOTSLIT['color'].append(copy.deepcopy( \
                             PLOTSLIT_def['color'][0]))
            PLOTSLIT['format'].append(copy.deepcopy( \
                             PLOTSLIT_def['format'][0]))
            PLOTSLIT['cfont'].append(copy.deepcopy( \
                             PLOTSLIT_def['cfont'][0]))
            PLOTSLIT['sfont'].append(copy.deepcopy( \
                             PLOTSLIT_def['sfont'][0]))
            PLOTSLIT['mind'].append(copy.deepcopy( \
                             PLOTSLIT_def['mind'][0]))
            PLOTSLIT['Mind'].append(copy.deepcopy( \
                             PLOTSLIT['Mind'][-1]))
            PLOTSLIT['caxis'].append(copy.deepcopy( \
                             PLOTSLIT['caxis'][-1]))
            PLOTSLIT['vaxis'].append(copy.deepcopy( \
                             PLOTSLIT['vaxis'][-1]))
            PLOTSLIT['vcaxis'].append(copy.deepcopy( \
                             PLOTSLIT['vcaxis'][-1]))
            PLOTSLIT['icaxis'].append(copy.deepcopy( \
                             PLOTSLIT_def['icaxis'][0]))
            PLOTSLIT['min'].append(copy.deepcopy( \
                             PLOT['min0']))
            PLOTSLIT['max'].append(copy.deepcopy( \
                             PLOT['max0']))
        self.set_top()

        return

######################################################################
######################################################################

    def set0(self, val):
        ''' Set the index of the plot to be changed
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        self.update_entry()
        PLOTSLIT['iplt'] = int(val.split(" ")[1])
        self.set_top()

        return

######################################################################
######################################################################

    def set1(self, val):
        ''' Set the file to plot
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        self.update_entry()
        PLOTSLIT['idata'][iplt] = int(val.split(":")[0])
        self.set_top()

        return

######################################################################
######################################################################

    def set2(self, val):
        ''' Set the variable to plot
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        self.update_entry()
        PLOTSLIT['axis'][iplt] = val
        self.set_top()

        return

######################################################################
######################################################################

    def res2(self):
        ''' Resets the current limits to the default
        '''

        global PLOT
        PLOT = source.general.PLOT
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1
        key = PLOTSLIT['axis'][iplt]

        PLOTSLIT['min'][iplt][key] = PLOT['min0'][key]
        PLOTSLIT['max'][iplt][key] = PLOT['max0'][key]
        self.set_top()

        return

######################################################################
######################################################################

    def fit2(self):
        ''' Finds and set the limits for the current file and plot
            variable
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global PSP
        PSP = source.general.PSP

        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1
        key = PLOTSLIT['axis'][iplt]

        PLOTSLIT['min'][iplt][key] = np.amin(PSP['data'][idata][key])
        PLOTSLIT['max'][iplt][key] = np.amax(PSP['data'][idata][key])
        self.set_top()

        return

######################################################################
######################################################################

    def set31(self, val):
        ''' Set the cut axis
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global PSP
        PSP = source.general.PSP


        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1

        if val.strip() == PSP['head'][idata]['axis_1_name']:
            PLOTSLIT['caxis'][iplt] = 'x'
            PLOTSLIT['vaxis'][iplt] = 'y'
        else:
            PLOTSLIT['caxis'][iplt] = 'y'
            PLOTSLIT['vaxis'][iplt] = 'x'

        self.set_top()

        return

######################################################################
######################################################################

    def set32(self, val):
        ''' Set the cut axis index
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global PSP
        PSP = source.general.PSP


        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1
        caxis = PLOTSLIT['caxis'][iplt]

        PLOTSLIT['icaxis'][iplt][caxis] = int(val)
        PLOTSLIT['vcaxis'][iplt][caxis] = \
                              str(PSP['data'][idata][caxis][int(val)])
        self.caxis3_name.set(PLOTSLIT['vcaxis'][iplt][caxis])
        self.update_entry()
        self.set_top()

        return

######################################################################
######################################################################

    def set41(self, val):
        ''' Set the minimum x index
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global PSP
        PSP = source.general.PSP


        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1

        PLOTSLIT['mind'][iplt]['l'] = int(val)
        PLOTSLIT['min'][iplt]['l'] = \
                                str(PSP['data'][idata]['l'][int(val)])
        self.xmin4_name.set(PLOTSLIT['min'][iplt]['l'])
        self.update_entry()
        self.set_top()

        return

######################################################################
######################################################################

    def set42(self, val):
        ''' Set the maximum x index
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global PSP
        PSP = source.general.PSP


        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1

        PLOTSLIT['Mind'][iplt]['l'] = int(val)
        PLOTSLIT['max'][iplt]['l'] = \
                                str(PSP['data'][idata]['l'][int(val)])
        self.xmax4_name.set(PLOTSLIT['max'][iplt]['l'])
        self.update_entry()
        self.set_top()

        return

######################################################################
######################################################################

    def set43(self, val):
        ''' Set the minimum y index
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global PSP
        PSP = source.general.PSP


        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1
        vaxis = PLOTSLIT['vaxis'][iplt]

        PLOTSLIT['mind'][iplt][vaxis] = int(val)
        PLOTSLIT['min'][iplt][vaxis] = \
                              str(PSP['data'][idata][vaxis][int(val)])
        self.ymin4_name.set(PLOTSLIT['min'][iplt][vaxis])
        self.update_entry()
        self.set_top()

        return

######################################################################
######################################################################

    def set44(self, val):
        ''' Set the maximum y index
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global PSP
        PSP = source.general.PSP

        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1
        vaxis = PLOTSLIT['vaxis'][iplt]

        PLOTSLIT['Mind'][iplt][vaxis] = int(val)
        PLOTSLIT['max'][iplt][vaxis] = \
                              str(PSP['data'][idata][vaxis][int(val)])
        self.ymax4_name.set(PLOTSLIT['max'][iplt][vaxis])
        self.update_entry()
        self.set_top()

        return

######################################################################
######################################################################

    def check4(self):
        ''' Check to shrink vertical axis
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        PLOTSLIT['shrink'][iplt] = self.shr4_stat.get() == 1
        self.set_top()

        return

######################################################################
######################################################################

    def set5(self, val):
        ''' Set the color map to plot
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        self.update_entry()
        PLOTSLIT['color'][iplt] = int(val)
        self.set_top()

        return

######################################################################
######################################################################

    def min60(self):
        ''' Reduce in 1 the colormap font size
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        if PLOTSLIT['sfont'][iplt]['z'] <= 1:
            return
        PLOTSLIT['sfont'][iplt]['z'] -= 1
        self.set_top()

        return

######################################################################
######################################################################

    def plu60(self):
        ''' increase in 1 the colormap font size
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        if PLOTSLIT['sfont'][iplt]['z'] >= 99:
            return
        PLOTSLIT['sfont'][iplt]['z'] += 1
        self.set_top()

        return

######################################################################
######################################################################

    def check60(self):
        ''' Check to change font size of colorbar
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        PLOTSLIT['cfont'][iplt]['z'] = self.zfs6_stat.get() == 1
        self.set_top()

        return

######################################################################
######################################################################

    def min61(self):
        ''' Reduce in 1 the horizontal axis font size
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        if PLOTSLIT['sfont'][iplt]['x'] <= 1:
            return
        PLOTSLIT['sfont'][iplt]['x'] -= 1
        self.set_top()

        return

######################################################################
######################################################################

    def plu61(self):
        ''' increase in 1 the horizontal axis font size
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        if PLOTSLIT['sfont'][iplt]['x'] >= 99:
            return
        PLOTSLIT['sfont'][iplt]['x'] += 1
        self.set_top()

        return

######################################################################
######################################################################

    def check61(self):
        ''' Check to change font size of horizontal axis
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        PLOTSLIT['cfont'][iplt]['x'] = self.xfs6_stat.get() == 1
        self.set_top()

        return

######################################################################
######################################################################

    def min62(self):
        ''' Reduce in 1 the vertical axis font size
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        if PLOTSLIT['sfont'][iplt]['y'] <= 1:
            return
        PLOTSLIT['sfont'][iplt]['y'] -= 1
        self.set_top()

        return

######################################################################
######################################################################

    def plu62(self):
        ''' increase in 1 the vertical axis font size
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        if PLOTSLIT['sfont'][iplt]['y'] >= 99:
            return
        PLOTSLIT['sfont'][iplt]['y'] += 1
        self.set_top()

        return

######################################################################
######################################################################

    def check62(self):
        ''' Check to change font size of vertical axis
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        PLOTSLIT['cfont'][iplt]['y'] = self.yfs6_stat.get() == 1
        self.set_top()

        return

######################################################################
######################################################################

    def load(self):
        ''' Loads a previously stored GUI configuration
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global PATH
        PATH = source.general.PATH

        # Open the dialog pick file
        filein = tkFileDialog.askopenfilename( \
                                         initialdir=PATH['psp_dir'], \
                                         parent=self)

        # Check if you selected something
        if filein:
            PLOTSLIT_in = {}
            try:
                with open(filein, "rb") as cache:
                    CACHE = pickle.load(cache)
                    for key in PLOTSLIT.keys():
                        PLOTSLIT_in[key] = CACHE['PLOTSLIT'][key]
                for key in PLOTSLIT.keys():
                    PLOTSLIT[key] = PLOTSLIT_in[key]
                self.redraw()

            except:
                raise
                tkMessageBox.showwarning("Load", \
                                         "Could not load that file", \
                                         parent=self)
                pass

######################################################################
######################################################################

    def save(self):
        ''' Saves the configuration of the GUI
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global PATH
        PATH = source.general.PATH

        # Update the entries first
        self.update_entry()
        # Open the dialog pick file
        filein = tkFileDialog.asksaveasfilename( \
                                       initialdir=PATH['psp_dir'], \
                                       parent=self)
        if filein:
            try:
                with open(filein, "wb") as cache:
                   pass
                   CACHE = {'PLOTSLIT' : PLOTSLIT}
                   pickle.dump(CACHE, cache)
            except IOError:
                pass
            except EOFError:
                pass
            except:
                raise

######################################################################
######################################################################

    def reset(self):
        ''' Reset the GUI
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global PLOTSLIT_def
        PLOTSLIT_def = source.general.PLOTSLIT_def

        check = tkMessageBox.askokcancel("Reset", "Are you sure?", \
                                         parent=self)

        if check:
            try:
                for key in PLOTSLIT.keys():
                    PLOTSLIT[key] = PLOTSLIT_def[key]
                self.redraw()
            except:
                raise

######################################################################
######################################################################

    def draw(self):
        ''' Method that handles the button Draw. It calls a support
            window of the PSP_canvas_class class.
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global CONF
        CONF = source.general.CONF

        # Update entries
        self.update_entry()

        # Check if you loaded the data previously
        for mind,Mind,vaxis,ii in \
            zip(PLOTSLIT['mind'],PLOTSLIT['Mind'], \
                PLOTSLIT['vaxis'],range(PLOTSLIT['nplt'])):
            if Mind[vaxis] <= mind[vaxis]:
                tkMessageBox.showwarning("Plot Slit", \
                                     "In plot {0}, ".format(ii+1) + \
                                     "minimum Y is larger or " + \
                                     "equal than maximum Y", \
                                     parent=self)
                return
            if Mind['l'] <= mind['l']:
                tkMessageBox.showwarning("Plot Slit", \
                                     "In plot {0}, ".format(ii+1) + \
                                     "minimum wavelength is " + \
                                     "larger or equal than " + \
                                     "maximum wavelength",parent=self)
                return


        # Make current passive
        self.config(background=CONF['col_pas'])

        # Calls the support window
        draw = PLOTSLIT_canvas_class(self.master, title='Plot')
        self.grab_set()
        self.lift()

        # Make current active
        self.configure(background=CONF['col_act'])

######################################################################
######################################################################
######################################################################
######################################################################

    def redraw(self):
        ''' Redraws the whole window
        '''

        self.box.destroy()
        self.box = Frame(self.head)
        self.up = Frame(self.box)
        self.do = Frame(self.box)
        self.optionbox(self.up)
        self.buttons(self.do)
        self.up.pack(fill=BOTH,expand=1,side=TOP)
        self.do.pack(fill=BOTH,expand=1,side=BOTTOM)
        self.box.pack(fill=BOTH,expand=1)

######################################################################
######################################################################

    def min2_val(self, *dumm):
        ''' Validates min2 entry
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1
        axis = PLOTSLIT['axis'][iplt]

        val = self.min2_name.get()
        try:
            num = float(val)
            PLOTSLIT['min'][iplt][axis] = num
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                        self.min2_name.set( \
                                  PLOTSLIT['min'][iplt][axis])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            self.min2_name.set( \
                                      PLOTSLIT['min'][iplt][axis])
        except:
            raise

######################################################################
######################################################################

    def max2_val(self, *dumm):
        ''' Validates max2 entry
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1
        axis = PLOTSLIT['axis'][iplt]

        val = self.max2_name.get()
        try:
            num = float(val)
            PLOTSLIT['max'][iplt][axis] = num
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                        self.max2_name.set( \
                                  PLOTSLIT['max'][iplt][axis])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            self.max2_name.set( \
                                      PLOTSLIT['max'][iplt][axis])
        except:
            raise

######################################################################
######################################################################

    def caxis3_val(self, *dumm):
        ''' Validates wav3 entry
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1
        caxis = PLOTSLIT['caxis'][iplt]

        val = self.caxis3_name.get()
        try:
            num = float(val)
            PLOTSLIT['vcaxis'][iplt][caxis] = val
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                        self.caxis3_name.set( \
                             PLOTSLIT['vcaxis'][iplt][caxis])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            self.caxis3_name.set( \
                                 PLOTSLIT['vcaxis'][iplt][caxis])
        except:
            raise

######################################################################
######################################################################

    def xmin4_val(self, *dumm):
        ''' Validates xmin4 entry
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1

        val = self.xmin4_name.get()
        try:
            num = float(val)
            PLOTSLIT['min'][iplt]['l'] = val
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                       self.xmin4_name.set(PLOTSLIT['min'][iplt]['l'])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            self.xmin4_name.set( \
                                           PLOTSLIT['min'][iplt]['l'])
        except:
            raise

######################################################################
######################################################################

    def xmax4_val(self, *dumm):
        ''' Validates xmax4 entry
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1

        val = self.xmax4_name.get()
        try:
            num = float(val)
            PLOTSLIT['max'][iplt]['l'] = val
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                       self.xmax4_name.set(PLOTSLIT['max'][iplt]['l'])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            self.xmax4_name.set( \
                                           PLOTSLIT['max'][iplt]['l'])
        except:
            raise

######################################################################
######################################################################

    def ymin4_val(self, *dumm):
        ''' Validates ymin4 entry
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1
        vaxis = PLOTSLIT['vaxis'][iplt]

        val = self.ymin4_name.get()
        try:
            num = float(val)
            PLOTSLIT['min'][iplt][vaxis] = val
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                     self.ymin4_name.set(PLOTSLIT['min'][iplt][vaxis])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            self.ymin4_name.set( \
                                         PLOTSLIT['min'][iplt][vaxis])
        except:
            raise

######################################################################
######################################################################

    def ymax4_val(self, *dumm):
        ''' Validates ymax4 entry
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1
        vaxis = PLOTSLIT['vaxis'][iplt]

        val = self.ymax4_name.get()
        try:
            num = float(val)
            PLOTSLIT['max'][iplt][vaxis] = val
        except ValueError:
            if len(val) > 0:
                if len(val) == 1:
                    if val[0] != '+' and val[0] != '-':
                     self.ymax4_name.set(PLOTSLIT['max'][iplt][vaxis])
                else:
                    if val[-1] != 'e':
                        if (val[-2] != 'e' and val[-1] != '+') and \
                           (val[-2] != 'e' and val[-1] != '-'):
                            self.ymax4_name.set( \
                                         PLOTSLIT['max'][iplt][vaxis])
        except:
            raise

######################################################################
######################################################################

    def zfor6_val(self, *dumm):
        ''' Validates colormap format entry
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        val = self.zfor6_name.get()
        # Apply the colormap label format
        if val.lower() in 'none':
            PLOTSLIT['format'][iplt]['z'] = 'None'
            if val.lower() == 'none':
                self.zfor6_name.set('None')
        else:
            try:
                test = self.zfor6_name.get() % 0.
                PLOTSLIT['format'][iplt]['z'] = val
            except:
                if len(val) == 0:
                    return
                for v in val:
                    if v not in '0123456789.%dfe':
                        self.zfor6_name.set( \
                                  PLOTSLIT['format'][iplt]['z'])
                        return
                if val[0] != '%':
                    self.zfor6_name.set( \
                                  PLOTSLIT['format'][iplt]['z'])

######################################################################
######################################################################

    def xfor6_val(self, *dumm):
        ''' Validates horizontal axis format entry
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        val = self.xfor6_name.get()
        # Apply the colormap label format
        if val.lower() in 'none':
            PLOTSLIT['format'][iplt]['x'] = 'None'
            if val.lower() == 'none':
                self.xfor6_name.set('None')
        else:
            try:
                test = self.xfor6_name.get() % 0.
                PLOTSLIT['format'][iplt]['x'] = val
            except:
                if len(val) == 0:
                    return
                for v in val:
                    if v not in '0123456789.%dfe':
                        self.xfor6_name.set( \
                                  PLOTSLIT['format'][iplt]['x'])
                        return
                if val[0] != '%':
                    self.xfor6_name.set( \
                                  PLOTSLIT['format'][iplt]['x'])

######################################################################
######################################################################

    def yfor6_val(self, *dumm):
        ''' Validates vertical axis format entry
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        iplt = PLOTSLIT['iplt'] - 1

        val = self.yfor6_name.get()
        # Apply the colormap label format
        if val.lower() in 'none':
            PLOTSLIT['format'][iplt]['y'] = 'None'
            if val.lower() == 'none':
                self.yfor6_name.set('None')
        else:
            try:
                test = self.yfor6_name.get() % 0.
                PLOTSLIT['format'][iplt]['y'] = val
            except:
                if len(val) == 0:
                    return
                for v in val:
                    if v not in '0123456789.%dfe':
                        self.yfor6_name.set( \
                                  PLOTSLIT['format'][iplt]['y'])
                        return
                if val[0] != '%':
                    self.yfor6_name.set( \
                                  PLOTSLIT['format'][iplt]['y'])

######################################################################
######################################################################

    def update_entry(self, event=None):
        ''' Really updates entries
        '''

        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global PSP
        PSP = source.general.PSP

        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1
        axis = PLOTSLIT['axis'][iplt]
        caxis = PLOTSLIT['caxis'][iplt]

        # Min plot variable
        val = self.min2_name.get()
        try:
            num = float(val)
            PLOTSLIT['min'][iplt][axis] = val
        except ValueError:
            pass
        except:
            raise
        # Max plot variable
        val = self.max2_name.get()
        try:
            num = float(val)
            PLOTSLIT['max'][iplt][axis] = val
        except ValueError:
            pass
        except:
            raise
        # Cut axis variable
        val = self.caxis3_name.get()
        try:
            num = float(val)
            ind = (np.abs(PSP['data'][idata][caxis]-num)).argmin()
            num = PSP['data'][idata][caxis][ind]
            PLOTSLIT['vcaxis'][iplt][caxis] = str(num)
            PLOTSLIT['icaxis'][iplt][caxis] = ind
        except ValueError:
            pass
        except:
            raise
        # Min x variable
        val = self.xmin4_name.get()
        try:
            num = float(val)
            ind = (np.abs(PSP['data'][idata]['l']-num)).argmin()
            num = PSP['data'][idata]['l'][ind]
            PLOTSLIT['min'][iplt]['l'] = str(num)
            PLOTSLIT['mind'][iplt]['l'] = ind
        except ValueError:
            pass
        except:
            raise
        # Max x variable
        val = self.xmax4_name.get()
        try:
            num = float(val)
            ind = (np.abs(PSP['data'][idata]['l']-num)).argmin()
            num = PSP['data'][idata]['l'][ind]
            PLOTSLIT['max'][iplt]['l'] = str(num)
            PLOTSLIT['Mind'][iplt]['l'] = ind
        except ValueError:
            pass
        except:
            raise
        # Vertical var
        vaxis = PLOTSLIT['vaxis'][iplt]
        # Min y variable
        val = self.ymin4_name.get()
        try:
            num = float(val)
            ind = (np.abs(PSP['data'][idata][vaxis]-num)).argmin()
            num = PSP['data'][idata][vaxis][ind]
            PLOTSLIT['min'][iplt][vaxis] = str(num)
            PLOTSLIT['mind'][iplt][vaxis] = ind
        except ValueError:
            pass
        except:
            raise
        # Max y variable
        val = self.ymax4_name.get()
        try:
            num = float(val)
            ind = (np.abs(PSP['data'][idata][vaxis]-num)).argmin()
            num = PSP['data'][idata][vaxis][ind]
            PLOTSLIT['max'][iplt][vaxis] = str(num)
            PLOTSLIT['Mind'][iplt][vaxis] = ind
        except ValueError:
            pass
        except:
            raise
        # Colorbar format
        val = self.zfor6_name.get()
        if val.lower() in 'none':
            PLOTSLIT['format'][iplt]['z'] = 'None'
        else:
            try:
                test = self.zfor6_name.get() % 0.
                PLOTSLIT['format'][iplt]['z'] = val
            except:
                pass
        # Horizontal axis format
        val = self.xfor6_name.get()
        if val.lower() in 'none':
            PLOTSLIT['format'][iplt]['x'] = 'None'
        else:
            try:
                test = self.xfor6_name.get() % 0.
                PLOTSLIT['format'][iplt]['x'] = val
            except:
                pass
        # Vertical axis format
        val = self.yfor6_name.get()
        if val.lower() in 'none':
            PLOTSLIT['format'][iplt]['y'] = 'None'
        else:
            try:
                test = self.yfor6_name.get() % 0.
                PLOTSLIT['format'][iplt]['y'] = val
            except:
                pass

        return

######################################################################
######################################################################

    def set_top(self):
        ''' Updates the top part of the widget window
        '''

        global VARS
        VARS = source.general.VARS
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT
        global PSP
        PSP = source.general.PSP

        iplt = PLOTSLIT['iplt'] - 1
        idata = PLOTSLIT['idata'][iplt] - 1
        adata = []
        ii = 0
        for head in PSP['head']:
            if head['loaded']:
                if head['manywav'] and \
                   (head['manyax1'] or head['manyax2']):
                    ii += 1
                    adata.append(head['nameshort'])

        # Sanity check indexes
        if PLOTSLIT['Mind'][iplt]['x'] >= \
           PSP['head'][idata]['nodes'][0]:
            PLOTSLIT['Mind'][iplt]['x'] = \
                                    PSP['head'][idata]['nodes'][0] - 1
            PLOTSLIT['max'][iplt]['x'] = PSP['data'][idata]['x'][ \
                                         PLOTSLIT['Mind'][iplt]['x']]
        if PLOTSLIT['Mind'][iplt]['y'] >= \
           PSP['head'][idata]['nodes'][1]:
            PLOTSLIT['Mind'][iplt]['y'] = \
                                    PSP['head'][idata]['nodes'][1] - 1
            PLOTSLIT['max'][iplt]['y'] = PSP['data'][idata]['y'][ \
                                         PLOTSLIT['Mind'][iplt]['y']]
        if PLOTSLIT['Mind'][iplt]['l'] >= PSP['head'][idata]['nwl']:
            PLOTSLIT['Mind'][iplt]['l'] = \
                                    PSP['head'][idata]['nwl'] - 1
            PLOTSLIT['max'][iplt]['l'] = PSP['data'][idata]['l'][ \
                                          PLOTSLIT['Mind'][iplt]['l']]
        if PLOTSLIT['mind'][iplt]['x'] >= \
           PSP['head'][idata]['nodes'][0]:
            PLOTSLIT['mind'][iplt]['x'] = \
                                    PSP['head'][idata]['nodes'][0] - 1
            PLOTSLIT['min'][iplt]['x'] = PSP['data'][idata]['x'][ \
                                          PLOTSLIT['mind'][iplt]['x']]
        if PLOTSLIT['mind'][iplt]['y'] >= \
           PSP['head'][idata]['nodes'][1]:
            PLOTSLIT['mind'][iplt]['y'] = \
                                    PSP['head'][idata]['nodes'][1] - 1
            PLOTSLIT['min'][iplt]['y'] = PSP['data'][idata]['y'][ \
                                          PLOTSLIT['mind'][iplt]['y']]
        if PLOTSLIT['mind'][iplt]['l'] >= PSP['head'][idata]['nwl']:
            PLOTSLIT['mind'][iplt]['l'] = \
                                    PSP['head'][idata]['nwl'] - 1
            PLOTSLIT['min'][iplt]['l'] = PSP['data'][idata]['l'][ \
                                          PLOTSLIT['mind'][iplt]['l']]

        # Possible axis to choose from
        pcaxis = []
        if PSP['head'][idata]['manyax2']:
            pcaxis.append(PSP['head'][idata]['axis_1_name'])
        if PSP['head'][idata]['manyax1']:
            pcaxis.append(PSP['head'][idata]['axis_2_name'])
        if PSP['head'][idata] \
              [VARS['axis_dic'][PLOTSLIT['caxis'][iplt]]] \
           not in pcaxis:
            if PSP['head'][idata]['axis_1_name'] in pcaxis:
                PLOTSLIT['caxis'][iplt] = 'x'
            else:
                PLOTSLIT['caxis'][iplt] = 'y'

        if PLOTSLIT['icaxis'][iplt]['x'] >= \
           PSP['head'][idata]['nodes'][0]:
            PLOTSLIT['icaxis'][iplt]['x'] = \
                                    PSP['head'][idata]['nodes'][0] - 1
            PLOTSLIT['vcaxis'][iplt]['x'] = PSP['data'][idata]['x'][ \
                                        PLOTSLIT['icaxis'][iplt]['x']]
        if PLOTSLIT['icaxis'][iplt]['y'] >= \
           PSP['head'][idata]['nodes'][1]:
            PLOTSLIT['icaxis'][iplt]['y'] = \
                                    PSP['head'][idata]['nodes'][1] - 1
            PLOTSLIT['vcaxis'][iplt]['y'] = PSP['data'][idata]['y'][ \
                                        PLOTSLIT['icaxis'][iplt]['y']]

        # Number of plots
        self.nplt0_val.set(PLOTSLIT['nplt'])
        self.nplt0_label.configure( \
                                 text="{0}".format(PLOTSLIT['nplt']))
        # Plot to edit
        self.wplt0_optm['menu'].delete(0,'end')
        self.wplt0_ind.set("Plot {0}".format(PLOTSLIT['iplt']))
        for jj in range(1,PLOTSLIT['nplt']+1):
            self.wplt0_optm['menu'].add_command( \
                 label = "Plot {0}".format(jj), \
                 command=lambda ii= "Plot {0}".format(jj): \
                 self.set0(ii))
        # File to plot
        self.wdat1_ind.set(adata[idata])

        # Variable to plot
        axis = PLOTSLIT['axis'][iplt]
        self.wvar2_ind.set(axis)
        self.min2_name.set(PLOTSLIT['min'][iplt][axis])
        self.max2_name.set(PLOTSLIT['max'][iplt][axis])
        self.auni21_ind.set("{0}".format(VARS['aunit_dic'][axis]))
        self.auni21_label.config(text=self.auni21_ind.get())
        self.auni22_ind.set("{0}".format(VARS['aunit_dic'][axis]))
        self.auni22_label.config(text=self.auni22_ind.get())

        # Cut Axis
        caxis = PLOTSLIT['caxis'][iplt]
        self.caxis3_slide.configure(to=PSP['head'][idata]['nodes'] \
                                       [VARS['axis_ind'][caxis]]-1)
        self.caxis3_ind.set(PSP['head'][idata] \
                               [VARS['axis_dic'][caxis]])
        self.caxis3_optm['menu'].delete(0,'end')
        self.caxis3_ind.set(PSP['head'][idata] \
                               [VARS['axis_dic'][caxis]])
        for jj in range(len(pcaxis)):
            self.caxis3_optm['menu'].add_command( \
                 label = pcaxis[jj], \
                 command=lambda ii= pcaxis[jj]: self.set31(ii))
        self.caxis3_name.set(PLOTSLIT['vcaxis'][iplt][caxis])
        self.caxis3_sind.set(PLOTSLIT['icaxis'][iplt][caxis])
        self.aind31_name.set('Index {0}'.format( \
                             PLOTSLIT['icaxis'][iplt][caxis]+1))
        self.aind31_label.config(text=self.aind31_name.get())

        # 2D
        # wavelength
        self.xmi4_slide.configure(to=PSP['head'][idata]['nwl']-1)
        self.xma4_slide.configure(to=PSP['head'][idata]['nwl']-1)
        self.aind41_name.set('Index {0}'.format( \
                             PLOTSLIT['mind'][iplt]['l']+1))
        self.aind41_label.config(text=self.aind41_name.get())
        self.aind42_name.set('Index {0}'.format( \
                             PLOTSLIT['Mind'][iplt]['l']+1))
        self.aind42_label.config(text=self.aind42_name.get())
        self.xmin4_name.set(PLOTSLIT['min'][iplt]['l'])
        self.xmax4_name.set(PLOTSLIT['max'][iplt]['l'])
        self.xmi4_ind.set(PLOTSLIT['mind'][iplt]['l'])
        self.xma4_ind.set(PLOTSLIT['Mind'][iplt]['l'])
        # space
        vaxis = PLOTSLIT['vaxis'][iplt]
        nodes = PSP['head'][idata]['nodes'][VARS['axis_ind'][vaxis]]
        self.ymi4_slide.configure(to=nodes-1)
        self.yma4_slide.configure(to=nodes-1)
        self.ymil4_name.set("Minimum {0}:".format( \
                         PSP['head'][idata][VARS['axis_dic'][vaxis]]))
        self.b4l3.config(text=self.ymil4_name.get())
        self.ymal4_name.set("Maximum {0}:".format( \
                         PSP['head'][idata][VARS['axis_dic'][vaxis]]))
        self.b4l4.config(text=self.ymal4_name.get())
        self.aind43_name.set('Index {0}'.format( \
                             PLOTSLIT['mind'][iplt][vaxis]+1))
        self.aind43_label.config(text=self.aind43_name.get())
        self.aind44_name.set('Index {0}'.format( \
                             PLOTSLIT['Mind'][iplt][vaxis]+1))
        self.aind44_label.config(text=self.aind44_name.get())
        self.ymin4_name.set(PLOTSLIT['min'][iplt][vaxis])
        self.ymax4_name.set(PLOTSLIT['max'][iplt][vaxis])
        self.ymi4_ind.set(PLOTSLIT['mind'][iplt][vaxis])
        self.yma4_ind.set(PLOTSLIT['Mind'][iplt][vaxis])
        if PLOTSLIT['shrink'][iplt]:
            self.shr4_stat.set(1)
        else:
            self.shr4_stat.set(0)

        # Color
        self.wcol5_name.set(VARS['palet_name'] \
                                [PLOTSLIT['color'][iplt]])
        self.wcol5_lab.config(text=self.wcol5_name.get())
        self.wcol5_ind.set(PLOTSLIT['color'][iplt])

        # Colorbar format and font
        # Entry
        if PLOTSLIT['format'][iplt]['z'] is None:
            self.zfor6_name.set('None')
        else:
            self.zfor6_name.set(PLOTSLIT['format'][iplt]['z'])
        # Check
        if PLOTSLIT['cfont'][iplt]['z']:
            self.zfs6_stat.set(1)
        else:
            self.zfs6_stat.set(0)
        # Label
        self.zfs6_lab.config(text=str(PLOTSLIT['sfont'][iplt]['z']))
        # Disable change of font
        if PLOTSLIT['cfont'][iplt]['z']:
            self.zfs6_lab.config(state='normal')
            self.zfs6_bmin.config(state='normal')
            self.zfs6_bplu.config(state='normal')
        else:
            self.zfs6_lab.config(state='disabled')
            self.zfs6_bmin.config(state='disabled')
            self.zfs6_bplu.config(state='disabled')

        # Horizontal axis format and font
        # Entry
        if PLOTSLIT['format'][iplt]['x'] is None:
            self.xfor6_name.set('None')
        else:
            self.xfor6_name.set(PLOTSLIT['format'][iplt]['x'])
        # Check
        if PLOTSLIT['cfont'][iplt]['x']:
            self.xfs6_stat.set(1)
        else:
            self.xfs6_stat.set(0)
        # Label
        self.xfs6_lab.config(text=str(PLOTSLIT['sfont'][iplt]['x']))
        # Disable change of font
        if PLOTSLIT['cfont'][iplt]['x']:
            self.xfs6_lab.config(state='normal')
            self.xfs6_bmin.config(state='normal')
            self.xfs6_bplu.config(state='normal')
        else:
            self.xfs6_lab.config(state='disabled')
            self.xfs6_bmin.config(state='disabled')
            self.xfs6_bplu.config(state='disabled')

        # Vertical axis format and font
        # Entry
        if PLOTSLIT['format'][iplt]['y'] is None:
            self.yfor6_name.set('None')
        else:
            self.yfor6_name.set(PLOTSLIT['format'][iplt]['y'])
        # Check
        if PLOTSLIT['cfont'][iplt]['y']:
            self.yfs6_stat.set(1)
        else:
            self.yfs6_stat.set(0)
        # Label
        self.yfs6_lab.config(text=str(PLOTSLIT['sfont'][iplt]['y']))
        # Disable change of font
        if PLOTSLIT['cfont'][iplt]['y']:
            self.yfs6_lab.config(state='normal')
            self.yfs6_bmin.config(state='normal')
            self.yfs6_bplu.config(state='normal')
        else:
            self.yfs6_lab.config(state='disabled')
            self.yfs6_bmin.config(state='disabled')
            self.yfs6_bplu.config(state='disabled')

######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
######################################################################
######################################################################


class PLOTSLIT_canvas_class(Toplevel):
    ''' Class that defines the support window to plot the 2D plots of
        the GUI.
    '''

    def __init__(self, parent, title = None):
        ''' Initialization of the PLOTSLIT_canvas_class class.
        '''

        global CONF
        CONF = source.general.CONF

        # Initialized a support window.
        Toplevel.__init__(self, parent)
        self.transient(parent)

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Set the title if any
        if title:
            self.title(title)

        self.parent = parent
        self.result = None

        body = Frame(self)
        self.initial_focus = self.body(body)
        body.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        
        # Action buttons
        self.buttonbox(body)
        
        # Figure
        self.canvas(body)
        
        body.pack()

        self.grab_set()
        self.lift()
        
        if not self.initial_focus:
            self.initial_focus = self

        self.protocol("WM_DELETE_WINDOW", self.close)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)


    def body(self, master):
        ''' Creates the dialog body and set initial focus.
        '''

        return master

######################################################################
######################################################################

    def buttonbox(self, box):
        ''' Add the Draw and Close buttons to the canvas.
        '''

        global CONF
        CONF = source.general.CONF

        ibox = Frame(box)
        w = Button(ibox, text="Close", width=10, font=self.cFont, \
                   command=self.close)
        w.pack(side=LEFT, padx=CONF['b_pad'], pady=CONF['b_pad'])
        ibox.pack()
        ibox.configure(background=CONF['col_act'])

        self.bind("<Escape>", self.close)

######################################################################
######################################################################

    def close(self, event=None):
        ''' Method that handles the window closing.
        '''

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

######################################################################
######################################################################

    def canvas(self, body, event=None):
        ''' Method for draw. Plots given the widget variables.
        '''

        global VARS
        VARS = source.general.VARS
        global PSP
        PSP = source.general.PSP
        global PLOTSLIT
        PLOTSLIT = source.general.PLOTSLIT

        # Choose the color maps
        cmapv = PLOTSLIT['color']


        # Build subplots given the number of figures
        cent = [1, 2, 2, 2, 3, 3, 3, 3, 3]
        dece = [1, 1, 2, 2, 2, 2, 3, 3, 3]
        fig = plt.figure(figsize=VARS['figsize'][PLOTSLIT['nplt']-1])
        fig.clf()

        # For each plot
        for ii in range(9):

            # Check if we are done
            if (ii + 1) > PLOTSLIT['nplt']:
                break

            # Get file
            idata = PLOTSLIT['idata'][ii] - 1
            head = PSP['head'][idata]
            data = PSP['data'][idata]
            axis = PLOTSLIT['axis'][ii]
            caxis = PLOTSLIT['caxis'][ii]
            vaxis = PLOTSLIT['vaxis'][ii]

            # Introduce subfigure
            ax = fig.add_subplot(100*cent[PLOTSLIT['nplt'] - 1] + \
                                  10*dece[PLOTSLIT['nplt'] - 1] + \
                                  ii + 1)

            # Get limits in float
            x0 = float(PLOTSLIT['min'][ii]['l'])
            x1 = float(PLOTSLIT['max'][ii]['l'])
            y0 = float(PLOTSLIT['min'][ii][vaxis])
            y1 = float(PLOTSLIT['max'][ii][vaxis])

            # Get spatial indexes
            ix0 = int(PLOTSLIT['mind'][ii]['l'])
            ix1 = int(PLOTSLIT['Mind'][ii]['l'])
            iy0 = int(PLOTSLIT['mind'][ii][vaxis])
            iy1 = int(PLOTSLIT['Mind'][ii][vaxis])

            # Shrink vertical axis
            if PLOTSLIT['shrink'][ii]:
                YY = data[vaxis]*np.cos(3.141592653589793238*\
                                       head['angles'][0]/180.)
            else:
                YY = data[vaxis]

            extent = (data['l'][ix0],data['l'][ix1],YY[iy0],YY[iy1])


            # Set titles
            if PLOTSLIT['cfont'][ii]['z']:
                ax.set_title(VARS['SPL_dic'][axis], \
                             fontsize=PLOTSLIT['sfont'][ii]['z'])
            else:
                ax.set_title(VARS['SPL_dic'][axis])
            if PLOTSLIT['cfont'][ii]['x']:
                ax.set_xlabel('$\lambda$ [nm]', \
                             fontsize=PLOTSLIT['sfont'][ii]['x'])
            else:
                ax.set_xlabel('$\lambda$ [nm]')
            if PLOTSLIT['cfont'][ii]['y']:
                ax.set_ylabel(head[VARS['axis_dic'][vaxis]] + \
                         ' [Mm]', fontsize=PLOTSLIT['sfont'][ii]['y'])
            else:
                ax.set_ylabel(head[VARS['axis_dic'][vaxis]] + \
                              ' [Mm]')


            # fit the bar
            divider = make_axes_locatable(ax)
            cax = divider.new_horizontal(size="5%", pad=0.05)
            fig = ax.get_figure()
            fig.add_axes(cax)

            # Font size horizontal axis
            if PLOTSLIT['cfont'][ii]['x']:
                for tick in ax.xaxis.get_major_ticks():
                     tick.label.set_fontsize( \
                                    int(PLOTSLIT['sfont'][ii]['x']))

            # Font size vertical axis
            if PLOTSLIT['cfont'][ii]['y']:
                for tick in ax.yaxis.get_major_ticks():
                     tick.label.set_fontsize( \
                                    int(PLOTSLIT['sfont'][ii]['y']))

            # Formatter
            # Horizontal axis
            if PLOTSLIT['format'][ii]['x'] != 'None' and PLOTSLIT['format'][ii]['x'] != None:
                pltxformat = "lambda y,_ : \"" + PLOTSLIT['format'][ii]['x'] + "\" % (y)"
                xformatter = FuncFormatter(eval(pltxformat))
                ax.xaxis.set_major_formatter(xformatter)
            # Vertical axis
            if PLOTSLIT['format'][ii]['y'] != 'None' and PLOTSLIT['format'][ii]['y'] != None:
                pltyformat = "lambda y,_ : \"" + PLOTSLIT['format'][ii]['y'] + "\" % (y)"
                yformatter = FuncFormatter(eval(pltyformat))
                ax.yaxis.set_major_formatter(yformatter)

                
            # Get quantity to draw
            c0 = float(PLOTSLIT['vcaxis'][ii][caxis])
            ic = (np.abs(data[caxis] - c0)).argmin()
            if caxis == 'x':
                Stokes_local = data[axis][ix0:ix1+1,iy0:iy1+1,ic]
            else:
                Stokes_local = data[axis][ix0:ix1+1,ic,iy0:iy1+1]

            # Get limits
            vmin = float(PLOTSLIT['min'][ii][axis])
            vmax = float(PLOTSLIT['max'][ii][axis])

            # Manage color
            if VARS['palet_name'][PLOTSLIT['color'][ii]] == \
                                                     'SignAdjustable':
                if vmin is None:
                    vmin = np.amin(Stokes_local)
                if vmax is None:
                    vmax = np.amax(Stokes_local)
                if vmin*vmax == 0 or abs(vmin - vmax) == 0:
                    PLOTSLIT['color'][ii] = 2
                elif vmin*vmax >= 0:
                    tkMessageBox.showwarning("Plot Slit", \
                                          "The Colormap " + \
                                          "SignAdjustable is " + \
                                          "designed to have " + \
                                          "a sign change.", \
                                          parent=self)
                    PLOTSLIT['color'][ii] = 2
                else:
                    pos1 = [1., 127./128., 0.5, 3./128., 0.]
                    pos2 = [3./128., 0.5, 1.]
                    plt.register_cmap(cmap = color_def([ \
                                            (  0,   0,   0), \
                                            (  0, 255,   0), \
                                            (  0,   0, 255), \
                                            (255, 255, 255), \
                                            (255, 255, 255), \
                                            (255, 255, 255), \
                                            (255, 200,   0), \
                                            (255,   0,   0)], \
                                            'SignAdjustable', \
                                  preleve = [vmin*x for x in pos1] + \
                                            [vmax*x for x in pos2], \
                                            minv = vmin, \
                                            maxv = vmax))


            # Put the plot into the figure
            im = ax.imshow(np.transpose(Stokes_local), \
                           cmap=VARS['palet_name']\
                                [PLOTSLIT['color'][ii]], \
                           vmin=vmin, vmax=vmax, \
                           extent = extent, \
                           origin='lower', \
                           aspect='auto')

            # Draw color bar
            if PLOTSLIT['format'][ii]['z'] == 'None':
                cbar = plt.colorbar(im, orientation='vertical', \
                                    cax=cax)
            else:
                cbar = plt.colorbar(im, orientation='vertical', \
                             format=PLOTSLIT['format'][ii]['z'], \
                             cax=cax)

            # Colorbar font size
            if PLOTSLIT['cfont'][ii]['z']:
                ticklabs = cbar.ax.get_yticklabels()
                cbar.ax.set_yticklabels(ticklabs, \
                            fontsize=PLOTSLIT['sfont'][ii]['z'])

        # Run matplotlib
        plt.tight_layout(pad=1.0, w_pad=2.0, h_pad=2.0)
        canvas = FigureCanvasTkAgg(fig, self)
        canvas.draw()
        canvas.get_tk_widget().pack(side=BOTTOM, fill=BOTH, \
                                    expand=True)
        toolbar = NavigationToolbar(canvas, self)
        toolbar.update()
        canvas._tkcanvas.pack(side=TOP, fill=BOTH, expand=True)
        plt.close('all')



