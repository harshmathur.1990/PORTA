# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# pmd.py                                                             #
#                                                                    #
# Tanausú del Pino Alemán                                            #
# Ángel de Vicente                                                   #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# GUI for the pmd files visualization                                #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  09/04/2020 - V1.0.5 - Added some warning messages at read (TdPA)  #
#                                                                    #
#  10/03/2020 - V1.0.4 - Added colors to indicate focus (TdPA)       #
#                      - Solved focus problems (AdV)                 #
#                                                                    #
#  04/10/2019 - V1.0.3 - Now the GUI recognizes itself if the file   #
#                        is binary or hdf5, so just one button to    #
#                        choose (TdPA)                               #
#                      - Improved comment handling for the display   #
#                        of the header comment (TdPA)                #
#                                                                    #
#  25/06/2019 - V1.0.2 - Bugfix: Module button is locked until a     #
#                        valid file is chosen. (TdPA)                #
#                                                                    #
#  24/04/2018 - V1.0.1 - Compatibility with python 3+. (TdPA)        #
#                                                                    #
#  26/03/2018 - V1.0.0 - First version. (TdPA)                       #
#                                                                    #
#  23/03/2018 - V0.0.0 - Start Code. (TdPA)                          #
#                                                                    #
######################################################################
######################################################################
######################################################################

import source.general
from source.loader import *
from source.tooltip import *
import h5py

######################################################################
######################################################################

class PMD_class(Toplevel):
    ''' Class that defines the main body of the pmd visualizer
    '''

######################################################################
######################################################################

    def __init__(self, parent, title = None):
        ''' Initializes the widget single tab
        '''

        global CONF
        CONF = source.general.CONF

        # Initialized a support window.
        Toplevel.__init__(self, parent)
        self.transient(parent)

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Set the title if any
        if title:
            self.title(title)

        # Active border
        self.configure(background=CONF['col_act'])

        self.parent = parent
        self.result = None
        self.plt_action = None

        head = Frame(self)
        self.head = head

        body = Frame(head)

        self.up = Frame(body)
        self.mid = Frame(body)
        self.do = Frame(body)
        self.box = body
        self.initial_focus = self.body(body)
        head.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        body.pack(fill=BOTH,expand=1)

        self.grab_set()
        self.lift()
        
        if not self.initial_focus:
            self.initial_focus = self

        # Configuration file manager
        self.optionbox(self.up)

        # Configuration display
        self.display(self.mid)

        # Configuration buttons
        self.buttons(self.do)

        self.up.pack(fill=BOTH,expand=1,side=TOP)
        self.mid.pack(fill=BOTH,expand=1,side=TOP)
        self.do.pack(fill=BOTH,expand=1,side=BOTTOM)
        body.pack(fill=BOTH,expand=1)
        head.pack(fill=BOTH,expand=1)

        self.protocol("WM_DELETE_WINDOW", self.close)
        self.bind("<Escape>", self.close)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)


######################################################################
######################################################################

    def body(self, master):
        ''' Creates the dialog body and set initial focus.
        '''

        return master

######################################################################
######################################################################

    def close(self, event=None):
        ''' Method that handles the window closing.
        '''

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

######################################################################
######################################################################
######################################################################
######################################################################

    def optionbox(self, box):
        ''' Defines all the control widgets of PMD_class class.
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # General label
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l = Label(box0, text="Manage file", font=self.cFont, \
                    justify=CENTER)
        createToolTip(b0l,TOOLTIPS['1'])
        b0l.grid(row=row,column=col,columnspan=2,sticky=NSEW)
        # Choose button
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_choose = Button(box0, text="Choose file", \
                                   font=self.cFont, \
                                   command=self.choose0)
        createToolTip(self.butt0_choose,TOOLTIPS['2'])
        self.butt0_choose.grid(row=row,column=col,sticky=NSEW)
        '''
        # Choose button (binary)
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_choose = Button(box0, text="Choose file", \
                                   font=self.cFont, \
                                   command=self.choose0_binary)
        createToolTip(self.butt0_choose,TOOLTIPS['2'])
        self.butt0_choose.grid(row=row,column=col,sticky=NSEW)
        # Choose button (HDF5)
        col += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_choose_h5 = Button(box0, \
                                      text="Choose file (HDF5)", \
                                      font=self.cFont, \
                                      command=self.choose0_h5)
        createToolTip(self.butt0_choose_h5,TOOLTIPS['2'])
        self.butt0_choose_h5.grid(row=row,column=col,sticky=NSEW)
        '''
        # Remove button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_rmv = Button(box0,text="Remove",font=self.cFont, \
                                command=self.rmv0)
        createToolTip(self.butt0_rmv,TOOLTIPS['9'])
        self.butt0_rmv.grid(row=row,column=col,sticky=NSEW)
        # Disable widgets if no data loaded
        if PMD['head'] is None:
            self.butt0_rmv.config(state='disabled')
        else:
            self.butt0_rmv.config(state='normal')
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def display(self, box):
        ''' Initializes the display of the header
        '''

        global PMD
        PMD = source.general.PMD

        # Create information label frame
        box0 = LabelFrame(box)
        Grid.rowconfigure(box0,0,weight=1)
        Grid.columnconfigure(box0,0,weight=1)
        if PMD['head'] is None:
            text = '\n\nChoose a pmd file\n\n'
            self.head_label = Label(box0, text=text, anchor=CENTER, \
                                    justify=CENTER, relief=FLAT, \
                                    bd=15, padx=3, pady=3, \
                                    font=self.cFont)
        else:
            head = PMD['head']
            text = '\n'
            text += ' File: {0}\n'.format(head['nameshort'])
            text += ' Pmd version: {0}\n'.format(head['pmd_ver'])
            text += ' Comments: {0}\n'.format(head['comments'])
            text += ' Angles: {0}x{1}\n'.format(*head['angles'])
            form = ' Original dimensions: {0}x{1}x{2} nodes\n'
            text += form.format(*head['nodes0'])
            form = ' Current dimensions: {0}x{1}x{2} nodes\n'
            text += form.format(*head['nodes'])
            form = ' Domain size: {0:8.3e}x{1:8.3e}x{2:8.3e} cm^3\n'
            text += form.format(*head['domain'])
            form = ' Periodicity: [{0},{1}]\n'
            text += form.format(*head['period'])
            form = ' Module: {0}\n'
            text += form.format(head['module'])
            text += '\n'
            self.head_label = Label(box0, text=text, anchor=CENTER, \
                                    justify=LEFT, relief=FLAT, \
                                    bd=15, padx=3, pady=3, \
                                    font=self.cFont)
        self.head_label.grid(row=0,column=0,sticky=NSEW)
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def buttons(self, box):
        ''' Defines the control buttons of the widget
        '''

        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS
        global PMD
        PMD = source.general.PMD

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # Module button
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_mod = Button(box0, text="Module",  \
                                 font=self.cFont, \
                                 command=self.module)
        createToolTip(self.butt1_mod,TOOLTIPS['41'])
        self.butt1_mod.grid(row=row,column=col,sticky=NSEW)
        # Disable widgets if no data loaded
        if PMD['head'] is None:
            self.butt1_mod.config(state='disabled')
        else:
            self.butt1_mod.config(state='normal')
        # Close button
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_close = Button(box0, text="Close", \
                                  font=self.cFont, command=self.close)
        createToolTip(self.butt1_close,TOOLTIPS['11'])
        self.butt1_close.grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)


######################################################################
######################################################################
######################################################################
######################################################################

    def choose0(self):
        ''' Method that handles the Choose button. Opens a dialog to
            pick an input file
        '''

        global PATH
        PATH = source.general.PATH
        global PMD
        PMD = source.general.PMD

        # Open the dialog pick file
        filein = tkFileDialog.askopenfilename( \
                                         initialdir=PATH['pmd_dir'], \
                                         parent=self)

        # If file selected
        if filein:

            # Reset output
            inpt = {}

            # Try HDF5
            check = self.get_header_h5(filein,inpt)
            if check:
                PMD['head'] = copy.deepcopy(inpt)
                self.set_top()
                self.set_mid()
                return
            else:
                inpt = {}
                # Try binary
                check = self.get_header_binary(filein,inpt)
                if check:
                    PMD['head'] = copy.deepcopy(inpt)
                    self.set_top()
                    self.set_mid()
                    return

        return

######################################################################
######################################################################

    def choose0_binary(self):
        ''' Method that handles the Choose button. Opens a dialog to
            pick an input file. Only binary.
        '''

        global PATH
        PATH = source.general.PATH
        global PMD
        PMD = source.general.PMD

        # Open the dialog pick file
        filein = tkFileDialog.askopenfilename( \
                                         initialdir=PATH['pmd_dir'], \
                                         parent=self)

        # Check if you selected something
        if filein:
            inpt = {}
            check = self.get_header_binary(filein,inpt)
            if check:
                PMD['head'] = copy.deepcopy(inpt)
                self.set_top()
                self.set_mid()

        return

######################################################################
######################################################################

    def choose0_h5(self):
        ''' Method that handles the Choose button. Opens a dialog to
            pick an input file. Only HDF5.
        '''

        global PATH
        PATH = source.general.PATH
        global PMD
        PMD = source.general.PMD

        # Open the dialog pick file
        filein = tkFileDialog.askopenfilename( \
                                         initialdir=PATH['pmd_dir'], \
                                         parent=self)

        # Check if you selected something
        if filein:
            inpt = {}
            check = self.get_header_h5(filein,inpt)
            if check:
                PMD['head'] = copy.deepcopy(inpt)
                self.set_top()
                self.set_mid()

        return

    
######################################################################
######################################################################

    def rmv0(self):
        ''' Method that removes the currently selected file from the
            GUI widget
        '''

        global PMD
        PMD = source.general.PMD

        # Remove
        PMD['head'] = None
        PMD['data'] = None
        self.set_mid()
        self.set_top()

######################################################################
######################################################################

    def module(self):
        ''' Method that handles the button module. It calls a support
            window of the adecuate pmd module
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF
        global MODS
        MODS = source.general.MODS

        # Load the module
        try:

            # Make current passive
            self.config(background=CONF['col_pas'])

            self.mod_class = getattr(__import__( \
                                     MODS[PMD['head']['module']], \
                                     fromlist=['MODULE_class']), \
                                     'MODULE_class')
            self.modc = self.mod_class(self.master, title='Loading')
            self.grab_set()
            self.lift()
            # Make current active
            self.configure(background=CONF['col_act'])
            
        except ImportError:
            tkMessageBox.showwarning("Load", \
                                     "Could not find class for " + \
                                     "module "+ \
                                     PMD['head']['module']+".", \
                                     parent=self)
            # Make current active
            self.configure(background=CONF['col_act'])
            return
        except:
            raise



######################################################################
######################################################################
######################################################################
######################################################################

    def set_top(self):
        ''' Reconfigures the top of the widget window
        '''

        global PMD
        PMD = source.general.PMD

        # Disable widgets if no data loaded
        if PMD['head'] is None:
            self.butt0_rmv.config(state='disabled')
        else:
            self.butt0_rmv.config(state='normal')

        return

######################################################################
######################################################################

    def set_mid(self):
        ''' Reconfigures the display of the widget window
        '''

        global PMD
        PMD = source.general.PMD

        # Update information label frame
        if PMD['head'] is None:
            text = '\n\nChoose a pmd file\n\n'
            self.head_label.config(justify=CENTER)
            self.butt1_mod.config(state='disabled')
        else:
            head = PMD['head']
            text = '\n'
            text += ' File: {0}\n'.format(head['nameshort'])
            text += ' Pmd version: {0}\n'.format(head['pmd_ver'])
            text += ' Comments: {0}\n'.format(head['comments'])
            text += ' Angles: {0}x{1}\n'.format(*head['angles'])
            form = ' Original dimensions: {0}x{1}x{2} nodes\n'
            text += form.format(*head['nodes0'])
            form = ' Current dimensions: {0}x{1}x{2} nodes\n'
            text += form.format(*head['nodes'])
            form = ' Domain size: {0:8.3e}x{1:8.3e}x{2:8.3e} cm^3\n'
            text += form.format(*head['domain'])
            form = ' Periodicity: [{0},{1}]\n'
            text += form.format(*head['period'])
            form = ' Module: {0}\n'
            text += form.format(head['module'])
            text += '\n'
            self.head_label.config(justify=LEFT)
            self.butt1_mod.config(state='normal')
        self.head_label.config(text=text)

        return


######################################################################
######################################################################
######################################################################
######################################################################

    def get_header_binary(self, filein, inpt):
        ''' Method that reads the header of the selected file. Returns
            false if not a valid pmd file and true otherwise.
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF
        global VARS
        VARS = source.general.VARS

        # Check if it is a file at all
        if not os.path.isfile(filein):
            return False

        # And try to read the header
        try:

            with open(filein,'rb') as f:
                inpt['io'] = "binary"
                
                inpt['size'] = 0
                bytes = f.read(8)
                inpt['size'] += 8
                if sys.version_info[0] < 3:
                    inpt['magic'] = ''.join(struct.unpack( \
                                       CONF['endian'] + 'c'*8, bytes))
                else:
                    inpt['magic'] = ''
                    for ii in range(8):
                        byte = bytes[ii:ii+1].decode('utf-8')
                        inpt['magic'] += byte
                if inpt['magic'] != 'portapmd':
                    msg = "Magic string {0} not recognized"
                    msg = msg.format(inpt['magic'])
                    tkMessageBox.showwarning("Load",msg,parent=self)
                    return False

                bytes = f.read(1)
                inpt['size'] += 1
                endian = struct.unpack(CONF['endian'] + \
                                       'b', bytes)[0]
                if endian == 1:
                    inpt['endian'] = VARS['endian']['Big']
                elif endian == 0:
                    inpt['endian'] = VARS['endian']['Little']
                else:
                    msg = "Endian {0} not recognized"
                    msg = msg.format(endian)
                    tkMessageBox.showwarning("Load",msg,parent=self)
                    return False
                bytes = f.read(1)
                inpt['size'] += 1
                inpt['isize'] = struct.unpack(inpt['endian'] + \
                                              'b', bytes)[0]
                bytes = f.read(1)
                inpt['size'] += 1
                inpt['dsize'] = struct.unpack(inpt['endian'] + \
                                              'b', bytes)[0]
                bytes = f.read(4)
                inpt['size'] += 4
                inpt['pmd_ver'] = struct.unpack(inpt['endian'] + \
                                                'I', bytes)[0]
                bytes = f.read(24)
                inpt['size'] += 24
                inpt['date'] = struct.unpack(inpt['endian'] + \
                                             'I'*6, bytes)
                bytes = f.read(2)
                inpt['size'] += 2
                inpt['period'] = struct.unpack(inpt['endian'] + \
                                               'bb', bytes)
                bytes = f.read(24)
                inpt['size'] += 24
                inpt['domain'] = struct.unpack(inpt['endian'] + \
                                               'ddd', bytes)
                bytes = f.read(24)
                inpt['size'] += 24
                inpt['origin'] = struct.unpack(inpt['endian'] + \
                                               'ddd', bytes)
                bytes = f.read(12)
                inpt['size'] += 12
                inpt['nodes'] = struct.unpack(inpt['endian'] + \
                                              'III', bytes)
                inpt['nodes0'] = inpt['nodes']
                bytes = f.read(8192*8)
                inpt['size'] += 8192*8
                inpt['x'] = struct.unpack(inpt['endian'] + \
                                          'd'*8192, bytes)[ \
                                          0:inpt['nodes'][0]]
                bytes = f.read(8192*8)
                inpt['size'] += 8192*8
                inpt['y'] = struct.unpack(inpt['endian'] + \
                                          'd'*8192, bytes)[ \
                                          0:inpt['nodes'][1]]
                bytes = f.read(8192*8)
                inpt['size'] += 8192*8
                inpt['z'] = struct.unpack(inpt['endian'] + \
                                          'd'*8192, bytes)[ \
                                          0:inpt['nodes'][2]]
                inpt['angles'] = []
                bytes = f.read(4)
                inpt['size'] += 4
                inpt['angles'].append(struct.unpack(inpt['endian'] + \
                                            'I', bytes)[0])
                bytes = f.read(4)
                inpt['size'] += 4
                inpt['angles'].append(struct.unpack(inpt['endian'] + \
                                            'I', bytes)[0])
                bytes = f.read(1023)
                inpt['size'] += 1023
                if sys.version_info[0] < 3:
                    module = struct.unpack(inpt['endian'] + \
                                           'c'*1023, bytes)
                    inpt['module'] = ['']
                    for i in range(1023):
                        if module[i] == '\x00' or module[i] == '\n':
                            break
                        else:
                            inpt['module'].append(module[i])
                    inpt['module'] = ''.join(inpt['module'])
                else:
                    inpt['module'] = ''
                    for i in range(1023):
                        byte = bytes[i:i+1].decode('utf-8')
                        if byte == '\x00' or byte == '\n':
                            break
                        else:
                            inpt['module'] += byte
                bytes = f.read(4096)
                inpt['size'] += 4096
                if sys.version_info[0] < 3:
                    comments = struct.unpack(inpt['endian'] + \
                                             'c'*4096, bytes)
                    inpt['comments'] = ['']
                    for i in range(4096):
                        if comments[i] == '\x00':
                            break
                        else:
                            inpt['comments'].append(comments[i])
                    inpt['comments'] = ''.join(inpt['comments'])
                else:
                    inpt['comments'] = ''
                    for i in range(4096):
                        byte = bytes[i:i+1].decode('utf-8')
                        if byte == '\x00':
                            break
                        else:
                            inpt['comments'] += byte
                # Strip ends of line
                inpt['comments'] = inpt['comments'].strip()
                # Check if everything in comment is a space
                if inpt['comments'] == \
                   ' '*len(list(inpt['comments'])):
                    inpt['comments'] = ' '
                # Introduce changes of lines
                else:
                    # Spaces to add
                    ns = 20
                    # First, split in jumps
                    Tmp = inpt['comments'].split('\n')
                    # For each section
                    for ii in range(len(Tmp)):
                        # Add 10 spaces at each section after the
                        # first
                        if ii > 0:
                            Tmp[ii] = ' '*ns + Tmp[ii]
                        siz = len(list(Tmp[ii]))
                        col = 400
                        # If big size of comment
                        while col < siz:
                            Tmp[ii] = Tmp[ii][:col+1] + '\n' + \
                                      ' '*ns + Tmp[ii][col+1:]
                            col += 402 + ns
                            siz += 2 + ns
                    # Mount back the comments
                    inpt['comments'] = '\n'.join(Tmp)
                bytes = f.read(4)
                inpt['size'] += 4
                inpt['msize'] = struct.unpack(inpt['endian'] + \
                                              'I', bytes)[0]
                bytes = f.read(4)
                inpt['size'] += 4
                inpt['gsize'] = struct.unpack(inpt['endian'] + \
                                              'I', bytes)[0]
                inpt['loaded'] = False
                path, filename = os.path.split(filein)
                inpt['name'] = filein
                inpt['nameshort'] = "{0} ".format(filename)
                # Check file size knowing module
                check = self.check_size(inpt)
                if not check:
                    return False
                PATH['pmd_dir'] = path
                return True

        except IOError:
            tkMessageBox.showwarning("Load","Could not open file", \
                                     parent=self)
            return False

        except:
            f.close()
            tkMessageBox.showwarning("Load","Could not open file", \
                                     parent=self)
            return False

######################################################################
######################################################################

    def get_header_h5(self, filein, inpt):
        ''' Method that reads the header of the selected file. Returns
            false if not a valid pmd file and true otherwise.
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF
        global VARS
        VARS = source.general.VARS

        # Check if it is a file at all
        if not os.path.isfile(filein):
            return False

        # Check if hdf5
        try:
            with h5py.File(filein, 'r') as f:
                pass
        except IOError:
            return False

        # And try to read the header
        try:

            with h5py.File(filein,'r') as f:
                inpt['io'] = "hdf5"
                if sys.version_info[0] < 3:
                    inpt['magic'] = f.attrs['PMD_MAGIC']
                else:
                    inpt['magic'] = \
                                  f.attrs['PMD_MAGIC'].decode('utf-8')
                if inpt['magic'] != 'portapmd':
                    msg = "Magic string {0} not recognized"
                    msg = msg.format(inpt['magic'])
                    tkMessageBox.showwarning("Load",msg,parent=self)
                    return False

                inpt['pmd_ver'] = f.attrs['PMD_VERSION']
                inpt['date'] = f.attrs['CREATION_DATE']
                inpt['period'] = f.attrs['PERIODICITY']
                inpt['domain'] = f.attrs['DOMAIN_SIZE']
                inpt['origin'] = f.attrs['DOMAIN_ORIGIN']
                inpt['nodes'] = f.attrs['GRID_DIMENSIONS']
                inpt['nodes0'] = inpt['nodes']

                inpt['x'] = f.attrs['X_AXIS']
                inpt['y'] = f.attrs['Y_AXIS']
                inpt['z'] = f.attrs['Z_AXIS']

                inpt['angles'] = f.attrs['POLAR_NODES'][0], \
                                 f.attrs['AZIMUTH_NODES'][0]
                
                if sys.version_info[0] < 3:
                    inpt['module'] = f.attrs['MODULE_NAME']
                    inpt['comments'] = f.attrs['MODULE_COMMENT']
                else:
                    inpt['module'] = \
                                f.attrs['MODULE_NAME'].decode('utf-8')
                    inpt['comments'] = \
                             f.attrs['MODULE_COMMENT'].decode('utf-8')
                # Strip ends of line
                inpt['comments'] = inpt['comments'].strip()
                # Check if everything in comment is a space
                if inpt['comments'] == \
                   ' '*len(list(inpt['comments'])):
                    inpt['comments'] = ' '
                # Introduce changes of lines
                else:
                    # Spaces to add
                    ns = 20
                    # First, split in jumps
                    Tmp = inpt['comments'].split('\n')
                    # For each section
                    for ii in range(len(Tmp)):
                        # Add 10 spaces at each section after the
                        # first
                        if ii > 0:
                            Tmp[ii] = ' '*ns + Tmp[ii]
                        siz = len(list(Tmp[ii]))
                        col = 400
                        # If big size of comment
                        while col < siz:
                            Tmp[ii] = Tmp[ii][:col+1] + '\n' + \
                                      ' '*ns + Tmp[ii][col+1:]
                            col += 402 + ns
                            siz += 2 + ns
                    # Mount back the comments
                    inpt['comments'] = '\n'.join(Tmp)
                # With HDF5 we don't need PMD_header
                inpt['msize'] = -1
                # Or node sizes
                inpt['gsize'] = -1

                inpt['loaded'] = False
                path, filename = os.path.split(filein)
                inpt['name'] = filein
                inpt['nameshort'] = "{0} ".format(filename)

                PATH['pmd_dir'] = path
                return True

        except IOError:
            tkMessageBox.showwarning("Load","Could not open file", \
                                     parent=self)
            return False

        except:
            f.close()
            tkMessageBox.showwarning("Load","Could not open file", \
                                     parent=self)
            return False
        
######################################################################
######################################################################

    def check_size(self, head):
        ''' Checks the size of the pmd file given the header data
        '''

        global VARS
        VARS = source.general.VARS

        # Check header
        expected = VARS['head_size'][head['pmd_ver']]
        real = head['size']
        if expected != real:
            return False

        expected += head['nodes'][0]* \
                    head['nodes'][1]* \
                    head['nodes'][2]* \
                    head['gsize'] + \
                    head['msize']
        real = os.stat(head['name']).st_size

        if expected == real:
            return True
        else:
            msg = "Expected size {0}. Got {1}"
            msg = msg.format(expected,real)
            tkMessageBox.showwarning("Load",msg,parent=self)
            return False

######################################################################
######################################################################
######################################################################
######################################################################
