# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# twolevel.py                                                        #
#                                                                    #
# Tanausú del Pino Alemán                                            #
# Ángel de Vicente                                                   #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Manager for the twolevel module                                    #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  To do:                                                            #
#    Much of the code in get_data and get_data_h5 are the same.      #
#    We should collapse both versions.                               #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  10/03/2020 - V1.0.7 - Added colors to indicate focus (TdPA)       #
#                      - Solved focus problems (AdV)                 #
#                                                                    #
#  23/01/2020 - V1.0.6 - Added option to store vtk files (TdPA)      #
#                                                                    #
#  02/12/2019 - V1.0.5 - Changed the way to do interpolation with    #
#                        hdf5. Now mimics the way for PMD (binary)   #
#                        so no issues with zoom (as with previous    #
#                        version (AdV)                               #
#                                                                    #
#  04/10/2019 - V1.0.4 - Added buttons to check everything, uncheck  #
#                        everything, and flip everything. (TdPA)     #
#                                                                    #
#  09/06/2019 - V1.0.3 - Added the option to read and interpolate    #
#                        using an external code in C. Does not load  #
#                        the data in python. (TdPA)                  #
#                      - Added a button to use that function. (TdPA) #
#                      - Adjusted formatting. (TdPA)                 #
#                      - Added compatibility for the old pmd files   #
#                        that are different, but somehow still       #
#                        version 2. (TdPA)                           #
#                      - Bugfix: Wrong weights for interpolation     #
#                        in the vertical axis. (TdPA)                #
#                      - Bugfix: Fixed one filename. (TdPA)          #
#                                                                    #
#  25/06/2019 - V1.0.2 - Bugfix: wrong angular momentums in pmd      #
#                        display. (TdPA)                             #
#                      - Bugfix: looks like the arguments in the     #
#                        function resulting from interp2d have to be #
#                        switched with respect to the call to get    #
#                        the mentione function. (TdPA)               #
#                      - Bugfix: Bug in the interpolation that can   #
#                        be run after reading. (TdPA)                #
#                      - Bugfix: Now checks dimensions to            #
#                        interpolate are smaller or equal than the   #
#                        originals. (TdPA)                           #
#                      - Bugfix: Proper check for equal arrays in    #
#                        interpolation. (TdPA)                       #
#                      - Added known bug. (TdPA)                     #
#                      - Zoom needed floats to reduce dimension in   #
#                        the hdf5 reading+interpolation. (TdPA)      #
#                      - The axis needs the same treatment when hdf5 #
#                        reads and interpolates. (TdPA)              #
#                                                                    #
#  24/04/2018 - V1.0.1 - Compatibility with python 3+. (TdPA)        #
#                                                                    #
#  27/03/2018 - V1.0.0 - First version. (TdPA)                       #
#                                                                    #
######################################################################
######################################################################
######################################################################

import source.general
from source.loader import *
from source.tooltip import *
import h5py
from scipy.ndimage import zoom

######################################################################
######################################################################

class MODULE_class(Toplevel):
    ''' Class that defines the main body of the module class
    '''

######################################################################
######################################################################

    def __init__(self, parent, title = None):
        ''' Initializes the widget single tab
        '''

        global CONF
        CONF = source.general.CONF

        # Initialized a support window.
        Toplevel.__init__(self, parent)
        self.transient(parent)

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Set the title if any
        if title:
            self.title(title)

        # Active border
        self.configure(background=CONF['col_act'])

        self.parent = parent
        self.result = None
        self.plt_action = None

        head = Frame(self)
        body = Frame(head)
        self.up = Frame(body)
        self.mid = Frame(body)
        self.do = Frame(body)
        self.bot = Frame(body)
        self.head = head
        self.box = body
        self.initial_focus = self.body(body)
        head.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        body.pack(fill=BOTH,expand=1)

        self.grab_set()
        self.lift()
        
        if not self.initial_focus:
            self.initial_focus = self

        self.buttons(self.do)

        self.display(self.mid)

        self.optionbox(self.up)

        self.progressbar(self.bot)

        self.up.pack(fill=BOTH,expand=1,side=TOP)
        self.mid.pack(fill=BOTH,expand=1,side=TOP)
        self.do.pack(fill=BOTH,expand=1,side=BOTTOM)
        self.bot.pack(fill=BOTH,expand=1,side=BOTTOM)
        body.pack(fill=BOTH,expand=1)
        head.pack(fill=BOTH,expand=1)

        self.protocol("WM_DELETE_WINDOW", self.close)
        self.bind("<Escape>", self.close)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)


######################################################################
######################################################################

    def body(self, master):
        ''' Creates the dialog body and set initial focus.
        '''

        return master

######################################################################
######################################################################

    def close(self, event=None):
        ''' Method that handles the window closing.
        '''

        global PMD
        PMD = source.general.PMD

        if PMD['head']['loaded']:
            tkMessageBox.showwarning("Load", \
                                     "You must unload the data " + \
                                     "before closing!",parent=self)
            return

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()


######################################################################
######################################################################
######################################################################
######################################################################

    def optionbox(self, box):
        ''' Defines all the control widgets of PMD_class class.
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # List the variables for this module
        PMD['vars'] = {0: u'\u03B5', \
                       1: 'Temperature', \
                       2: 'Density', \
                       3: 'B', \
                       4: 'V', \
                       5: 'Lower level', \
                       6: 'Upper level', \
                       7: 'Radiation', \
                       8: 'Damping', \
                       9: 'Depolarizing coeff.', \
                       10: u'\u03B7 cont.', \
                       11: u'\u03B5 cont.'}
        PMD['file'] = {0: 'epsilon', \
                       1: 'Temperature', \
                       2: 'Density', \
                       3: 'B', \
                       4: 'V', \
                       5: 'Lower level density matrix', \
                       6: 'Upper level density matrix', \
                       7: 'Radiation field', \
                       8: 'Damping parameter', \
                       9: 'Depolarizing coefficient', \
                       10: 'Absorptivity continuum', \
                       11: 'Emissivity continuum'}
        PMD['read'] = [0,0,0,0,0,0,0,0,0,0,0,0]

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # General label
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l = Label(box0, text="Load Manager", font=self.cFont, \
                    justify=CENTER)
        createToolTip(b0l,TOOLTIPS['1'])
        b0l.grid(row=row,column=col,columnspan=3,sticky=NSEW)
        # Loaded file
        # Status
        box00 = LabelFrame(box0)
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        Grid.rowconfigure(box00,0,weight=1)
        Grid.columnconfigure(box00,0,weight=1)
        if PMD['head']['loaded']:
            self.data0_name = Label(box00, text='Loaded', \
                                    anchor=CENTER, \
                                    font=self.cFont)
            createToolTip(self.data0_name, TOOLTIPS['5'])
        else:
            self.data0_name = Label(box00, text='Not loaded', \
                                    anchor=CENTER, \
                                    font=self.cFont)
            createToolTip(self.data0_name, TOOLTIPS['6'])
        self.data0_name.grid(row=0,column=0,sticky=NSEW)
        box00.grid(row=row,column=col,sticky=NSEW)
        # Load button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_load = Button(box0,text="Load",font=self.cFont, \
                                 command=self.load0)
        createToolTip(self.butt0_load,TOOLTIPS['7'])
        self.butt0_load.grid(row=row,column=col,sticky=NSEW)
        # Unload button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_unload = Button(box0,text="Unload", \
                                   font=self.cFont, \
                                   command=self.unload0)
        createToolTip(self.butt0_unload,TOOLTIPS['8'])
        self.butt0_unload.grid(row=row,column=col,sticky=NSEW)
        # Disable widgets if no data loaded
        if PMD['head']['loaded']:
            self.butt0_load.config(state='disabled')
            self.butt1_close.config(state='disabled')
            self.butt1_plot.config(state='normal')
            self.butt1_save.config(state='normal')
            self.butt0_unload.config(state='normal')
        else:
            self.butt0_load.config(state='normal')
            self.butt1_close.config(state='normal')
            self.butt1_plot.config(state='disabled')
            self.butt1_save.config(state='disabled')
            self.butt0_unload.config(state='disabled')
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)


        # Interpolation (box1)
        box1 = LabelFrame(box)
        row = 0
        col = 0
        # General label
        Grid.rowconfigure(box1,row,weight=1)
        Grid.columnconfigure(box1,col,weight=1)
        b1l = Label(box1, text="Interpolation parameters", \
                    font=self.cFont, \
                    justify=CENTER)
        b1l.grid(row=row,column=col,columnspan=8,sticky=NSEW)
        # Check for interpolation
        row += 1
        col = 0
        Grid.columnconfigure(box1,col,weight=1)
        self.int1_stat = IntVar()
        if CONF['interpol']:
            self.int1_stat.set(1)
        else:
            self.int1_stat.set(0)
        self.int1_check = Checkbutton(box1, \
                                      text="Interpolate data " + \
                                           "while reading     ", \
                                      anchor=CENTER, \
                                      font=self.cFont, \
                                      variable=self.int1_stat, \
                                      command=self.check1)
        createToolTip(self.int1_check,text=TOOLTIPS['27'])
        self.int1_check.grid(row=row,column=col,sticky=NSEW)
        #
        # Entries
        #
        # X
        # Label
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.b1l1 = Label(box1, text='    NX:', \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b1l1.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        CONF['nodes']['x']=PMD['head']['nodes0'][0]
        self.nx1_name = StringVar()
        self.nx1_name.set(CONF['nodes']['x'])
        self.nx1_name.trace('w',self.nx1_val)
        self.nx1_entry = Entry(box1, font=self.cFont, \
                               textvariable=self.nx1_name)
        self.nx1_entry.grid(row=row,column=col,sticky=NSEW)
        # Y
        # Label
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.b1l2 = Label(box1, text='    NY:', \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b1l2.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        CONF['nodes']['y']=PMD['head']['nodes0'][1]
        self.ny1_name = StringVar()
        self.ny1_name.set(CONF['nodes']['y'])
        self.ny1_name.trace('w',self.ny1_val)
        self.ny1_entry = Entry(box1, font=self.cFont, \
                               textvariable=self.ny1_name)
        self.ny1_entry.grid(row=row,column=col,sticky=NSEW)
        # Z
        # Label
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.b1l3 = Label(box1, text='    NZ:', \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b1l3.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        CONF['nodes']['z']=PMD['head']['nodes0'][2]
        self.nz1_name = StringVar()
        self.nz1_name.set(CONF['nodes']['z'])
        self.nz1_name.trace('w',self.nz1_val)
        self.nz1_entry = Entry(box1, font=self.cFont, \
                               textvariable=self.nz1_name)
        self.nz1_entry.grid(row=row,column=col,sticky=NSEW)
        # Interpolate button
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.butt1_int = Button(box1, text="Interpolate", \
                                font=self.cFont, command=self.int1)
        createToolTip(self.butt1_int,TOOLTIPS['40'])
        self.butt1_int.grid(row=row,column=col,sticky=NSEW)
        if PMD['head']['loaded']:
            self.butt1_int.config(state='normal')
            self.int1_check.config(state='disabled')
        else:
            self.butt1_int.config(state='disabled')
            self.int1_check.config(state='normal')
        # Pack the box
        box1.pack(fill=BOTH, expand=1, side = TOP)



        # Variables to read (box1)
        box2 = LabelFrame(box)
        row = 0
        col = 0
        jump = 4
        # General label
        Grid.rowconfigure(box2,row,weight=1)
        Grid.columnconfigure(box2,col,weight=1)
        b2l = Label(box2, text="Variables to read", \
                    font=self.cFont, \
                    justify=CENTER)
        b2l.grid(row=row,column=col,columnspan=jump,sticky=NSEW)
        # Check for interpolation
        row += 1
        col = -1
        self.var2_stat = []
        self.var2_check = []
        for ii in range(len(PMD['read'])):
            col += 1
            if col >= jump:
                row += 1
                col = 0
            self.var2_stat.append(IntVar())
            Grid.rowconfigure(box2,col,weight=1)
            Grid.columnconfigure(box2,col,weight=1)
            if PMD['read'][ii]:
                self.var2_stat[ii].set(1)
            else:
                self.var2_stat[ii].set(0)
            self.var2_check.append(Checkbutton(box2, \
                                   text=PMD['vars'][ii], \
                                   anchor=CENTER, \
                                   font=self.cFont, \
                                   variable=self.var2_stat[ii], \
                                   command=lambda jj=ii: \
                                           self.check2(jj)))
            createToolTip(self.var2_check[ii],text=TOOLTIPS['27'])
            self.var2_check[ii].grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box2.pack(fill=BOTH, expand=1, side = TOP)



        # General checks
        box3 = LabelFrame(box)
        row = 0
        col = 0
        # Check all
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.butt3_checka = Button(box3,text="Check all", \
                                   font=self.cFont, \
                                   command=self.checka3)
        createToolTip(self.butt0_unload,TOOLTIPS['46'])
        self.butt3_checka.grid(row=row,column=col,sticky=NSEW)
        # Uncheck all
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.butt3_uchecka = Button(box3,text="Uncheck all", \
                                   font=self.cFont, \
                                   command=self.uchecka3)
        createToolTip(self.butt0_unload,TOOLTIPS['47'])
        self.butt3_uchecka.grid(row=row,column=col,sticky=NSEW)
        # Toggle all
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.butt3_togglea = Button(box3,text="Toggle all", \
                                    font=self.cFont, \
                                    command=self.togglea3)
        createToolTip(self.butt0_unload,TOOLTIPS['48'])
        self.butt3_togglea.grid(row=row,column=col,sticky=NSEW)
        if PMD['head']['loaded']:
            self.butt3_checka.config(state='disabled')
            self.butt3_uchecka.config(state='disabled')
            self.butt3_togglea.config(state='disabled')
        else:
            self.butt3_checka.config(state='normal')
            self.butt3_uchecka.config(state='normal')
            self.butt3_togglea.config(state='normal')
        # Pack the box
        box3.pack(fill=BOTH, expand=1, side = TOP)


######################################################################
######################################################################

    def display(self, box):
        ''' Initializes the display of the header
        '''

        global PMD
        PMD = source.general.PMD
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Create information label frame
        box0 = LabelFrame(box)
        Grid.rowconfigure(box0,0,weight=1)
        Grid.columnconfigure(box0,0,weight=1)
        if not PMD['head']['loaded']:
            text = '\n\nLoad the pmd file\n\n'
            self.data_label = Label(box0, text=text, anchor=CENTER, \
                                    justify=CENTER, relief=FLAT, \
                                    bd=15, padx=3, pady=3, \
                                    font=self.cFont)
        else:
            data = PMD['data']
            module = PMD['head']['module']
            text = '\n'
            text += ' Module {0} version {1}\n'.format(module, \
                                               PMD['head']['pmd_ver'])
            text += ' Atomic mass: {0}\n'.format(data['amass'])
            text += ' Einstein coefficient: {0}\n'.format(data['Aul'])
            text += ' Transition energy: {0}\n'.format(data['Eul'])
            if int(data['Jl2']) % 2 == 0:
                form = ' Angular momentum lower level: {0}\n'
                text += form.format(int(data['Jl2'])/2)
            else:
                form = ' Angular momentum lower level: {0}/2\n'
                text += form.format(int(data['Jl2']))
            if int(data['Ju2']) % 2 == 0:
                form = ' Angular momentum upper level: {0}\n'
                text += form.format(int(data['Ju2'])/2)
            else:
                form = ' Angular momentum upper level: {0}/2\n'
                text += form.format(int(data['Ju2']))
            form = ' Lande factor lower level: {0}\n'
            text += form.format(data['gl'])
            form = ' Lande factor upper level: {0}\n'
            text += form.format(data['gu'])
            form = ' Temperature of reference: {0}\n'
            text += form.format(data['Tref'])
            form = ' Original dimensions: {0}x{1}x{2} nodes\n'
            text += form.format(*data['nodes0'])
            form = ' Current dimensions: {0}x{1}x{2} nodes\n'
            text += form.format(*data['nodes'])
            text += '\n'
            self.data_label = Label(box0, text=text, anchor=CENTER, \
                              justify=LEFT, relief=FLAT, \
                              bd=15, padx=3, pady=3, \
                              font=self.cFont)
        self.data_label.grid(row=0,column=0,sticky=NSEW)
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def buttons(self, box):
        ''' Defines the control buttons of the widget
        '''

        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # Plot button
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_plot = Button(box0, text="Plot",  \
                                 font=self.cFont, \
                                 command=self.plot)
        createToolTip(self.butt1_plot,TOOLTIPS['42'])
        self.butt1_plot.grid(row=row,column=col,sticky=NSEW)
        # Plot with C
        '''
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_plotC = Button(box0, text="Plot with C",  \
                                 font=self.cFont, \
                                 command=self.plotC)
        createToolTip(self.butt1_plotC,TOOLTIPS['43'])
        self.butt1_plotC.grid(row=row,column=col,sticky=NSEW)
        # Check that the executable is there
        if os.path.isfile('extractor'):
            self.butt1_plotC.config(state='normal')
        else:
            self.butt1_plotC.config(state='disabled')
        '''
        # Save vtk
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_save = Button(box0, text="Save vtk",  \
                                 font=self.cFont, \
                                 command=self.save)
        createToolTip(self.butt1_save,TOOLTIPS['49'])
        self.butt1_save.grid(row=row,column=col,sticky=NSEW)
        # Close button
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_close = Button(box0, text="Close", \
                                  font=self.cFont, command=self.close)
        createToolTip(self.butt1_close,TOOLTIPS['11'])
        self.butt1_close.grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def progressbar(self, box):
        ''' Initializes the progress bar
        '''

        self.pgf = Frame(box)
        Grid.rowconfigure(self.pgf,0,weight=1)
        Grid.columnconfigure(self.pgf,0,weight=1)
        self.pg = Progressbar(self.pgf)
        self.pg.grid(row=0,column=0,sticky=NSEW)
        self.pgf.pack(fill=BOTH, expand=1, side = TOP)


######################################################################
######################################################################
######################################################################
######################################################################

    def load0(self):
        ''' Method that handles the Load button. Load the pmd file
            content from currently selected pmd file
        '''

        global PMD
        PMD = source.general.PMD

        # Check that you selected something
        if sum(PMD['read']) < 1:
            tkMessageBox.showwarning("Load", \
                                     "You must select at least " + \
                                     "one variable to read.", \
                                     parent=self)
            return

        # Signal in the GUI that we are loading
        self.data0_name.configure(text='Loading',font=self.cFont)
        self.data0_name.update_idletasks()

        # Try to read the data
        if PMD['head']['io'] == "hdf5":
            check = self.get_data_h5()
        else:
            check = self.get_data()
            
        if check:
            PMD['head']['loaded'] = True
        else:
            tkMessageBox.showwarning("Load", \
                                     "Could not read pmd file.", \
                                     parent=self)
            return

        self.set_top()
        self.set_mid()
        self.pg.step(-100)
        self.pg.update_idletasks()

        return

######################################################################
######################################################################

    def unload0(self):
        ''' Method that handles the Unload button. Unload the pmd
            data already loaded.
        '''

        global PMD
        PMD = source.general.PMD

        PMD['data'] = None
        PMD['head']['loaded'] = False
        self.set_top()
        self.set_mid()

        return

######################################################################
######################################################################

    def check1(self):
        ''' Check to interpolate while reading
        '''

        global CONF
        CONF = source.general.CONF

        CONF['interpol'] = self.int1_stat.get() == 1
        self.set_top()

        return

######################################################################
######################################################################

    def int1(self):
        ''' Button to interpolate read data
        '''

        # Try to interpolate the data
        check = self.interpol_data()
        if not check:
            tkMessageBox.showwarning("Load", \
                                     "Could not interpolate " + \
                                     "pmd data.",parent=self)
            return

        self.set_top()
        self.set_mid()
        self.pg.step(-100)
        self.pg.update_idletasks()

        return

######################################################################
######################################################################

    def check2(self, val):
        ''' Check to read a variable
        '''

        global PMD
        PMD = source.general.PMD

        PMD['read'][val] = self.var2_stat[val].get() == 1
        self.set_top()

        return

######################################################################
######################################################################

    def checka3(self):
        ''' Check to read all variables
        '''

        global PMD
        PMD = source.general.PMD

        for ii in range(len(PMD['read'])):
            PMD['read'][ii] = True
            self.var2_stat[ii].set(1)
        self.set_top()

        return

######################################################################
######################################################################

    def uchecka3(self):
        ''' Uncheck to read all variables
        '''

        global PMD
        PMD = source.general.PMD

        for ii in range(len(PMD['read'])):
            PMD['read'][ii] = False
            self.var2_stat[ii].set(0)
        self.set_top()

        return

######################################################################
######################################################################

    def togglea3(self):
        ''' Toggles to read all variables
        '''

        global PMD
        PMD = source.general.PMD

        for ii in range(len(PMD['read'])):
            PMD['read'][ii] = not PMD['read'][ii]
            if PMD['read'][ii]:
                self.var2_stat[ii].set(1)
            else:
                self.var2_stat[ii].set(0)
        self.set_top()

        return

######################################################################
######################################################################

    def plot(self):
        ''' Plot button
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF

        # Identify scalars and vectors
        scal = [0,1,2,8,9,10,11]
        vec = [3,4]

        # Data pointer
        data = PMD['data']

        # Initialize list of load files
        d = []

        # Open script file
        try:
            f = open('cache/dump.py','w')
            folder = 'cache/'
        except IOError:
            tkMessageBox.showwarning("Plot", \
                                     "I could not write in " + \
                                     "cache folder. I am going " + \
                                     "to write in current " + \
                                     "directory, but this is not " + \
                                     "normal behavior.",parent=self)
            f = open('dump.py','w')
            folder = ''
        except:
            raise

        # Script header
        f.write('# -*- coding: utf-8 -*-\n\n')
        f.write('from numpy import array\n')
        f.write('try:\n')
        f.write('    engine = mayavi.engine\n')
        f.write('except NameError:\n')
        f.write('    from mayavi.api import Engine\n')
        f.write('    engine = Engine()\n')
        f.write('    engine.start()\n')
        f.write('if len(engine.scenes) == 0:\n')
        f.write('    engine.new_scene()\n')
        f.write('scene = engine.scenes[0]\n')

        # Initialize file names list
        files = []

        # Create KQ names
        if PMD['read'][5] or PMD['read'][6]:
            KQv = []
            for K in range(np.amax([data['Jl2'],data['Ju2']])+1):
                for Q in range(-K,K+1):
                    for ii,R in zip(range(2),['  Real','  Imag']):
                        KQv.append(' K='+str(K)+' Q='+str(Q)+R)

        # Create data files

        # Create grid once
        grid = vtk.RectilinearGrid(data['x'],data['y'],data['z'])

        # Create scalars
        for ii in scal:
            if not PMD['read'][ii]:
                continue
            pdata = vtk.PointData(vtk.Scalars(data[ii].flatten(), \
                                  name=PMD['file'][ii]))
            vtkdata = vtk.VtkData(grid, pdata)
            files.append(folder+PMD['file'][ii]+'.vtk')
            vtkdata.tofile(files[-1],'binary')
            f.write("vtk_file_reader = engine.open('" + \
                    files[-1] + "', scene)\n")

        # Create vectors
        for ii in vec:
            if not PMD['read'][ii]:
                continue
            pdata = vtk.PointData(vtk.Vectors(data[ii].flatten(), \
                                  name=PMD['file'][ii]))
            vtkdata = vtk.VtkData(grid, pdata)
            files.append(folder+PMD['file'][ii]+'.vtk')
            vtkdata.tofile(files[-1],'binary')
            f.write("vtk_file_reader = engine.open('" + \
                    files[-1] + "', scene)\n")

        # Create rhol
        ii = 5
        if PMD['read'][ii]:
            jj = -1
            for kk in range(data['NRL']):
                for ll in range(2):
                    jj += 1
                    KQ = KQv[jj]
                    if 'Q=0' in KQ and 'Imag' in KQ:
                        continue
                    pdata = vtk.PointData(vtk.Scalars(\
                                  data[ii][:,:,:,kk,ll].flatten(), \
                                  name=PMD['file'][ii]+KQ))
                    vtkdata = vtk.VtkData(grid, pdata)
                    files.append(folder+PMD['file'][ii]+KQ+'.vtk')
                    vtkdata.tofile(files[-1],'binary')
                    f.write("vtk_file_reader = engine.open('" + \
                            files[-1] + "', scene)\n")

        # Create rhou
        ii = 6
        if PMD['read'][ii]:
            jj = -1
            for kk in range(data['NRU']):
                for ll in range(2):
                    jj += 1
                    KQ = KQv[jj]
                    if 'Q=0' in KQ and 'Imag' in KQ:
                        continue
                    pdata = vtk.PointData(vtk.Scalars(\
                                  data[ii][:,:,:,kk,ll].flatten(), \
                                  name=PMD['file'][ii]+KQ))
                    vtkdata = vtk.VtkData(grid, pdata)
                    files.append(folder+PMD['file'][ii]+KQ+'.vtk')
                    vtkdata.tofile(files[-1],'binary')
                    f.write("vtk_file_reader = engine.open('" + \
                            files[-1] + "', scene)\n")

        # Create J
        ii = 7
        if PMD['read'][ii]:
            KQv = []
            for K in range(3):
                for Q in range(0,K+1):
                    for jj,R in zip(range(2),['  Real','  Imag']):
                        if Q == 0 and 'Imag' in R:
                            continue
                        KQv.append(' K='+str(K)+' Q='+str(Q)+R)
            for jj in range(9):
                KQ = KQv[jj]
                pdata = vtk.PointData(vtk.Scalars(\
                                 data[ii][:,:,:,jj].flatten(), \
                                 name=PMD['file'][ii]+KQ))
                vtkdata = vtk.VtkData(grid, pdata)
                files.append(folder+PMD['file'][ii]+KQ+'.vtk')
                vtkdata.tofile(files[-1],'binary')
                f.write("vtk_file_reader = engine.open('" + \
                        files[-1] + "', scene)\n")



        # Close script
        f.close()

        # Make current passive
        self.config(background=CONF['col_pas'])

        # Call mayavi2
        try:
            subprocess.call(["mayavi2","cache/dump.py"])
        except OSError:
            tkMessageBox.showwarning("Plot", \
                                     "I could not open Mayavi2. " + \
                                     "Ensure that Mayavi2 is " + \
                                     "correctly installed and " + \
                                     "that the command mayavi2 " + \
                                     "opens it.",parent=self)
            return
        except:
            raise

        # Make current active
        self.configure(background=CONF['col_act'])

        # Remove script
        try:
            os.remove(folder+'dump.py')
        except OSError:
            tkMessageBox.showwarning("Plot", \
                                     "I could not find my python " + \
                                     "script to delete it. If " + \
                                     "you moved it, I will not " + \
                                     "touch it.",parent=self)
        except:
            raise

        # Delete vtk files
        for fil in files:
            try:
                os.remove(fil)
            except OSError:
                pass
            except:
                raise

        return

######################################################################
######################################################################

    def save(self):
        ''' Save button
        '''

        global PMD
        PMD = source.general.PMD

        # Identify scalars and vectors
        scal = [0,1,2,8,9,10,11]
        vec = [3,4]

        # Data pointer
        data = PMD['data']

        # Initialize list of load files
        d = []

        # Ask for folder
        folder = tkFileDialog.askdirectory(initialdir= \
                                            PATH['pmd_dir'], \
                                           parent=self)

        # If no folder, return
        if not isinstance(folder,str):
            return
        if not os.path.isdir(folder):
            return
        if list(folder)[-1] != '/':
            folder += '/'

        # Initialize file names list
        files = []

        # Create KQ names
        if PMD['read'][5] or PMD['read'][6]:
            KQv = []
            for K in range(np.amax([data['Jl2'],data['Ju2']])+1):
                for Q in range(-K,K+1):
                    for ii,R in zip(range(2),['  Real','  Imag']):
                        KQv.append(' K='+str(K)+' Q='+str(Q)+R)

        # Create data files

        # Create grid once
        grid = vtk.RectilinearGrid(data['x'],data['y'],data['z'])

        # Create scalars
        for ii in scal:
            if not PMD['read'][ii]:
                continue
            pdata = vtk.PointData(vtk.Scalars(data[ii].flatten(), \
                                  name=PMD['file'][ii]))
            vtkdata = vtk.VtkData(grid, pdata)
            files.append(folder+PMD['file'][ii]+'.vtk')
            vtkdata.tofile(files[-1],'binary')

        # Create vectors
        for ii in vec:
            if not PMD['read'][ii]:
                continue
            pdata = vtk.PointData(vtk.Vectors(data[ii].flatten(), \
                                  name=PMD['file'][ii]))
            vtkdata = vtk.VtkData(grid, pdata)
            files.append(folder+PMD['file'][ii]+'.vtk')
            vtkdata.tofile(files[-1],'binary')

        # Create rhol
        ii = 5
        if PMD['read'][ii]:
            jj = -1
            for kk in range(data['NRL']):
                for ll in range(2):
                    jj += 1
                    KQ = KQv[jj]
                    if 'Q=0' in KQ and 'Imag' in KQ:
                        continue
                    pdata = vtk.PointData(vtk.Scalars(\
                                  data[ii][:,:,:,kk,ll].flatten(), \
                                  name=PMD['file'][ii]+KQ))
                    vtkdata = vtk.VtkData(grid, pdata)
                    files.append(folder+PMD['file'][ii]+KQ+'.vtk')
                    vtkdata.tofile(files[-1],'binary')

        # Create rhou
        ii = 6
        if PMD['read'][ii]:
            jj = -1
            for kk in range(data['NRU']):
                for ll in range(2):
                    jj += 1
                    KQ = KQv[jj]
                    if 'Q=0' in KQ and 'Imag' in KQ:
                        continue
                    pdata = vtk.PointData(vtk.Scalars(\
                                  data[ii][:,:,:,kk,ll].flatten(), \
                                  name=PMD['file'][ii]+KQ))
                    vtkdata = vtk.VtkData(grid, pdata)
                    files.append(folder+PMD['file'][ii]+KQ+'.vtk')
                    vtkdata.tofile(files[-1],'binary')

        # Create J
        ii = 7
        if PMD['read'][ii]:
            KQv = []
            for K in range(3):
                for Q in range(0,K+1):
                    for jj,R in zip(range(2),['  Real','  Imag']):
                        if Q == 0 and 'Imag' in R:
                            continue
                        KQv.append(' K='+str(K)+' Q='+str(Q)+R)
            for jj in range(9):
                KQ = KQv[jj]
                pdata = vtk.PointData(vtk.Scalars(\
                                 data[ii][:,:,:,jj].flatten(), \
                                 name=PMD['file'][ii]+KQ))
                vtkdata = vtk.VtkData(grid, pdata)
                files.append(folder+PMD['file'][ii]+KQ+'.vtk')
                vtkdata.tofile(files[-1],'binary')

        return

######################################################################
######################################################################

    def plotC(self):
        ''' Plot button to call C code
        '''

        global PMD
        PMD = source.general.PMD

        # HDF5 not implemented in C
        if PMD['head']['io'] == "hdf5":
            tkMessageBox.showwarning("PlotC", \
                                     "Sorry, but the C method " + \
                                     "is only for the binary " + \
                                     "format.",parent=self)
            return

        # Identify scalars and vectors
        scal = [0,1,2,8,9,10,11]
        vec = [3,4]

        # Construct running string
        # Executable
        srun = ["./extractor"]
        # File name
        srun.append(PMD['head']['name'])
        # No verbose
        srun.append("0")
        # New grid nodes
        if CONF['interpol']:
            srun.append("{0}".format(CONF['nodes']['x']))
            srun.append("{0}".format(CONF['nodes']['y']))
            srun.append("{0}".format(CONF['nodes']['z']))
        else:
            srun.append("{0}".format(PMD['head']['nodes0'][0]))
            srun.append("{0}".format(PMD['head']['nodes0'][1]))
            srun.append("{0}".format(PMD['head']['nodes0'][2]))
        lref = len(srun)

        # Variables to plot
        for i in range(12):
            if PMD['read'][i]:
                srun.append("{0}".format(i))

        # If nothing to read, stop
        if len(srun) <= lref:
            tkMessageBox.showwarning("PlotC", \
                                     "Choose something to read", \
                                     parent=self)
            return

        # Call C code
        subprocess.call(srun)

        # Get size of rho
        with open(PMD['head']['name'],'rb') as f:

            f.seek(PMD['head']['size']+28)
            bytes = f.read(4)
            Jl2 = int(struct.unpack( \
                      CONF['endian'] + 'I', bytes)[0])
            NRL = (Jl2 + 1)*(Jl2 + 1)
            bytes = f.read(4)
            Ju2 = int(struct.unpack( \
                      CONF['endian'] + 'I', bytes)[0])
            NRU = (Ju2 + 1)*(Ju2 + 1)

        # Open script file
        try:
            f = open('cache/dump.py','w')
            folder = 'cache/'
        except IOError:
            tkMessageBox.showwarning("PlotC", \
                                     "I could not write in " + \
                                     "cache folder. I am going " + \
                                     "to write in current " + \
                                     "directory, but this is not " + \
                                     "normal behavior.",parent=self)
            f = open('dump.py','w')
            folder = ''
        except:
            raise

        # Script header
        f.write('# -*- coding: utf-8 -*-\n\n')
        f.write('from numpy import array\n')
        f.write('try:\n')
        f.write('    engine = mayavi.engine\n')
        f.write('except NameError:\n')
        f.write('    from mayavi.api import Engine\n')
        f.write('    engine = Engine()\n')
        f.write('    engine.start()\n')
        f.write('if len(engine.scenes) == 0:\n')
        f.write('    engine.new_scene()\n')
        f.write('scene = engine.scenes[0]\n')

        # Initialize file names list
        files = []

        # Create KQ names
        if PMD['read'][5] or PMD['read'][6]:
            KQv = []
            for K in range(np.amax([Jl2,Ju2])+1):
                for Q in range(-K,K+1):
                    for ii,R in zip(range(2),['  Real','  Imag']):
                        KQv.append(' K='+str(K)+' Q='+str(Q)+R)

        # Scalars
        for ii in scal:
            if not PMD['read'][ii]:
                continue
            files.append(PMD['file'][ii]+'.vtk')
            f.write("vtk_file_reader = engine.open('" + \
                    files[-1] + "', scene)\n")

        # Vectors
        for ii in vec:
            if not PMD['read'][ii]:
                continue
            files.append(PMD['file'][ii]+'.vtk')
            f.write("vtk_file_reader = engine.open('" + \
                    files[-1] + "', scene)\n")

        # Rhol
        ii = 5
        if PMD['read'][ii]:
            jj = -1
            for kk in range(NRL):
                for ll in range(2):
                    jj += 1
                    KQ = KQv[jj]
                    files.append(PMD['file'][ii]+KQ+'.vtk')
                    if 'Q=0' in KQ and 'Imag' in KQ:
                        continue
                    f.write("vtk_file_reader = engine.open('" + \
                            files[-1] + "', scene)\n")

        # Rhou
        ii = 6
        if PMD['read'][ii]:
            jj = -1
            for kk in range(NRU):
                for ll in range(2):
                    jj += 1
                    KQ = KQv[jj]
                    files.append(PMD['file'][ii]+KQ+'.vtk')
                    if 'Q=0' in KQ and 'Imag' in KQ:
                        continue
                    f.write("vtk_file_reader = engine.open('" + \
                            files[-1] + "', scene)\n")

        # J
        ii = 7
        if PMD['read'][ii]:
            KQv = []
            for K in range(3):
                for Q in range(0,K+1):
                    for jj,R in zip(range(2),['  Real','  Imag']):
                        if Q == 0 and 'Imag' in R:
                            continue
                        KQv.append(' K='+str(K)+' Q='+str(Q)+R)
            for jj in range(9):
                KQ = KQv[jj]
                files.append(PMD['file'][ii]+KQ+'.vtk')
                f.write("vtk_file_reader = engine.open('" + \
                        files[-1] + "', scene)\n")

        # Close script
        f.close()

        # Check files are there
        check = True
        for fil in files:
            if not os.path.isfile(fil):
                check = False
                break

        # Call mayavi2
        if check:
            try:
                subprocess.call(["mayavi2","cache/dump.py"])
            except OSError:
                tkMessageBox.showwarning("PlotC", \
                                      "I could not open Mayavi2. " + \
                                      "Ensure that Mayavi2 is " + \
                                      "correctly installed and " + \
                                      "that the command mayavi2 " + \
                                      "opens it.",parent=self)
                return
            except:
                raise

        else:
            tkMessageBox.showwarning("PlotC", \
                                     "There was some problem " + \
                                     "running the C extractor.", \
                                     parent=self)

        # Remove script
        try:
            os.remove(folder+'dump.py')
        except OSError:
            tkMessageBox.showwarning("PlotC", \
                                     "I could not find my python " + \
                                     "script to delete it. If " + \
                                     "you moved it, I will not " + \
                                     "touch it.",parent=self)
        except:
            raise

        # Delete vtk files
        for fil in files:
            try:
                os.remove(fil)
            except OSError:
                pass
            except:
                raise

        return

######################################################################
######################################################################
######################################################################
######################################################################

    def nx1_val(self, *dumm):
        ''' Validates nx entry
        '''

        global CONF
        CONF = source.general.CONF

        val = self.nx1_name.get()
        try:
            num = int(val)
            if num < 0:
                raise ValueError
            CONF['nodes']['x'] = val
        except ValueError:
            if val!="":
                self.nx1_name.set(CONF['nodes']['x'])
            else:
                CONF['nodes']['x'] = 0            
        except:
            raise

######################################################################
######################################################################

    def ny1_val(self, *dumm):
        ''' Validates ny entry
        '''

        global CONF
        CONF = source.general.CONF

        val = self.ny1_name.get()
        try:
            num = int(val)
            if num < 0:
                raise ValueError
            CONF['nodes']['y'] = val
        except ValueError:
            if val!="":
                self.ny1_name.set(CONF['nodes']['y'])
            else:
                CONF['nodes']['y'] = 0            
        except:
            raise

######################################################################
######################################################################

    def nz1_val(self, *dumm):
        ''' Validates nz entry
        '''

        global CONF
        CONF = source.general.CONF

        val = self.nz1_name.get()
        try:
            num = int(val)
            if num < 0:
                raise ValueError
            CONF['nodes']['z'] = val
        except ValueError:
            if val!="":
                self.nz1_name.set(CONF['nodes']['z'])
            else:
                CONF['nodes']['z'] = 0            
        except:
            raise

######################################################################
######################################################################

    def update_entry(self, event=None):
        ''' Really updates entries
        '''

        global CONF
        CONF = source.general.CONF

        # nx variable
        val = self.nx1_name.get()
        try:
            num = int(val)
            if num > 0:
                CONF['nodes']['x'] = val
        except ValueError:
            pass
        except:
            raise
        # ny variable
        val = self.ny1_name.get()
        try:
            num = int(val)
            if num > 0:
                CONF['nodes']['y'] = val
        except ValueError:
            pass
        except:
            raise
        # nz variable
        val = self.nz1_name.get()
        try:
            num = int(val)
            if num > 0:
                CONF['nodes']['z'] = val
        except ValueError:
            pass
        except:
            raise

######################################################################
######################################################################

    def set_top(self):
        ''' Reconfigures the top of the widget window
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Status
        if PMD['head']['loaded']:
            self.data0_name.config(text='Loaded', \
                                   font=self.cFont)
            createToolTip(self.data0_name, TOOLTIPS['5'])
            self.butt0_load.config(state='disabled')
            self.int1_check.config(state='disabled')
            self.butt1_int.config(state='normal')
            self.butt1_close.config(state='disabled')
            self.butt1_plot.config(state='normal')
            self.butt1_save.config(state='normal')
            self.butt0_unload.config(state='normal')
            for ii in range(len(PMD['read'])):
                self.var2_check[ii].config(state='disabled')
        else:
            self.data0_name.config(text='Not loaded', \
                                   font=self.cFont)
            createToolTip(self.data0_name, TOOLTIPS['6'])
            self.butt0_load.config(state='normal')
            self.int1_check.config(state='normal')
            self.butt1_int.config(state='disabled')
            self.butt1_close.config(state='normal')
            self.butt1_plot.config(state='disabled')
            self.butt1_save.config(state='disabled')
            self.butt0_unload.config(state='disabled')
            for ii in range(len(PMD['read'])):
                self.var2_check[ii].config(state='normal')

        # Check if you compiled extractor
        '''
        if os.path.isfile('extractor') :
            self.butt1_plotC.config(state='normal')
        else:
            self.butt1_plotC.config(state='disabled')
        '''

        # Interpolation
        if CONF['interpol']:
            self.int1_stat.set(1)
        else:
            self.int1_stat.set(0)

        # Variables to read
        for ii in range(len(PMD['read'])):
            if PMD['read'][ii]:
                self.var2_stat[ii].set(1)
            else:
                self.var2_stat[ii].set(0)

        # Check buttons status
        if PMD['head']['loaded']:
            self.butt3_checka.config(state='disabled')
            self.butt3_uchecka.config(state='disabled')
            self.butt3_togglea.config(state='disabled')
        else:
            self.butt3_checka.config(state='normal')
            self.butt3_uchecka.config(state='normal')
            self.butt3_togglea.config(state='normal')

        return

######################################################################
######################################################################

    def set_mid(self):
        ''' Reconfigures the display of the widget window
        '''

        global PMD
        PMD = source.general.PMD

        # Update information label frame
        if not PMD['head']['loaded']:
            text = '\n\nLoad the pmd file\n\n'
            self.data_label.config(justify=CENTER)
        else:
            data = PMD['data']
            module = PMD['head']['module']
            text = '\n'
            text += ' Module {0} version {1}\n'.format(module, \
                                               PMD['head']['pmd_ver'])
            text += ' Atomic mass: {0}\n'.format(data['amass'])
            text += ' Einstein coefficient: {0}\n'.format(data['Aul'])
            text += ' Transition energy: {0}\n'.format(data['Eul'])
            if int(data['Jl2']) % 2 == 0:
                form = ' Angular momentum lower level: {0}\n'
            else:
                form = ' Angular momentum lower level: {0}/2\n'
            text += form.format(int(data['Jl2'])/2)
            if int(data['Ju2']) % 2 == 0:
                form = ' Angular momentum upper level: {0}\n'
            else:
                form = ' Angular momentum upper level: {0}/2\n'
            text += form.format(int(data['Jl2'])/2)
            form = ' Lande factor lower level: {0}\n'
            text += form.format(data['gl'])
            form = ' Lande factor upper level: {0}\n'
            text += form.format(data['gu'])
            form = ' Temperature of reference: {0}\n'
            text += form.format(data['Tref'])
            form = ' Original dimensions: {0}x{1}x{2} nodes\n'
            text += form.format(*data['nodes0'])
            form = ' Current dimensions: {0}x{1}x{2} nodes\n'
            text += form.format(*data['nodes'])
            text += '\n'
            self.data_label.config(justify=LEFT)
        self.data_label.config(text=text)

        # Check if you compiled extractor
        '''
        if os.path.isfile('extractor'):
            self.butt1_plotC.config(state='normal')
        else:
            self.butt1_plotC.config(state='disabled')
        '''

        return

######################################################################
######################################################################
######################################################################
######################################################################


    def get_data(self):
        ''' Reads the pmd for the twolevel module given the header
            Returns true if the reading went well and false otherwise.
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Initialize progress bar
        self.pgc = 0
        self.pgl = 0

        # Check if there is a file at all
        if not os.path.isfile(PMD['head']['name']):
            return False

        # Initialize output
        inpt = {}

        # Read it as a pmd file
        try:

            self.pg.update_idletasks()

            with open(PMD['head']['name'],'rb') as f:

                f.seek(PMD['head']['size'])
                bytes = f.read(4)
                inpt['mod_ver'] = struct.unpack( \
                                  CONF['endian'] + 'I', bytes)[0]
                bytes = f.read(8)
                inpt['amass'] = struct.unpack( \
                                CONF['endian'] + 'd', bytes)[0]
                bytes = f.read(8)
                inpt['Aul'] = struct.unpack( \
                              CONF['endian'] + 'd', bytes)[0]
                bytes = f.read(8)
                inpt['Eul'] = struct.unpack( \
                              CONF['endian'] + 'd', bytes)[0]
                bytes = f.read(4)
                inpt['Jl2'] = struct.unpack( \
                              CONF['endian'] + 'I', bytes)[0]
                inpt['NRL'] = (inpt['Jl2'] + 1)*(inpt['Jl2'] + 1)
                self.NRL = inpt['NRL']
                bytes = f.read(4)
                inpt['Ju2'] = struct.unpack( \
                              CONF['endian'] + 'I', bytes)[0]
                inpt['NRU'] = (inpt['Ju2'] + 1)*(inpt['Ju2'] + 1)
                self.NRU = inpt['NRU']
                bytes = f.read(8)
                inpt['gl'] = struct.unpack( \
                             CONF['endian'] + 'd', bytes)[0]
                bytes = f.read(8)
                inpt['gu'] = struct.unpack( \
                             CONF['endian'] + 'd', bytes)[0]
                bytes = f.read(8)
                inpt['Tref'] = struct.unpack( \
                               CONF['endian'] + 'd', bytes)[0]
                inpt['nodes'] = []
                inpt['nodes'] = (PMD['head']['nodes0'][0], \
                                 PMD['head']['nodes0'][1], \
                                 PMD['head']['nodes0'][2])

                inpt['x'] = PMD['head']['x'] \
                                       [0:PMD['head']['nodes0'][0]]
                inpt['y'] = PMD['head']['y'] \
                                       [0:PMD['head']['nodes0'][1]]
                inpt['z'] = PMD['head']['z'] \
                                       [0:PMD['head']['nodes0'][2]]

                #
                # Compatibility with pmd files that were different
                # before 2019, but the version did not change because
                # the code was not public yet
                #

                # Read four bytes, get an integer from them, and roll
                # back the same number of bytes
                bytes = f.read(4)
                f.seek(-4,1)
                test_nx = int(struct.unpack( \
                              CONF['endian'] + 'I', bytes)[0])

                # If this integer is, in fact, nx, we jump forward
                if (test_nx == PMD['head']['nodes0'][0]):
                    f.seek(8*(inpt['nodes'][0]+inpt['nodes'][1]+1),1)

                #
                # End of compatibility patch
                #

                bytes = f.read(8*inpt['nodes'][0]*
                                 inpt['nodes'][1])
                inpt['tempo'] = np.array(struct.unpack( \
                                CONF['endian'] + 'd'* \
                                inpt['nodes'][0]* \
                                inpt['nodes'][1], bytes)). \
                                reshape(inpt['nodes'][1], \
                                        inpt['nodes'][0])
                inpt['nodes0'] = inpt['nodes']

                planesize = PMD['head']['nodes0'][0]* \
                            PMD['head']['nodes0'][1]* \
                            8*(22 + 2*(self.NRL + self.NRU))

                # Create geometry axis
                if CONF['interpol']:

                    # Change number of nodes
                    inpt['nodes'] = [int(CONF['nodes']['x']), \
                                     int(CONF['nodes']['y']), \
                                     int(CONF['nodes']['z'])]

                    # Check sizes
                    for ii in range(len(inpt['nodes'])):
                        if inpt['nodes'][ii] > inpt['nodes0'][ii] or \
                           inpt['nodes'][ii] <= 1:
                            tkMessageBox.showwarning("Load", \
                                         "The dimension of the " + \
                                         "interpolated data " + \
                                         "cannot be larger than " + \
                                         "the original or " + \
                                         "smaller than 2.", \
                                         parent=self)
                            return False

                    # Original grid in Mm
                    temp = {}
                    temp['x'] = np.array(inpt['x'])*1e-8
                    temp['y'] = np.array(inpt['y'])*1e-8
                    temp['z'] = np.array(inpt['z'])*1e-8

                    # Final grid
                    inpt['x'] = np.linspace(min(temp['x']), \
                                            max(temp['x']), \
                                            inpt['nodes'][0])
                    inpt['y'] = np.linspace(min(temp['y']), \
                                            max(temp['y']), \
                                            inpt['nodes'][1])
                    inpt['z'] = np.linspace(min(temp['z']), \
                                            max(temp['z']), \
                                            inpt['nodes'][2])

                    # Check what planes do we really need to read
                    Flags = np.zeros(inpt['nodes0'][2], dtype=bool)
                    liz1 = 0
                    for iz in range(inpt['nodes0'][2]-1):

                        for iz1 in range(liz1,inpt['nodes'][2]):

                            if inpt['z'][iz1] > temp['z'][iz+1]:
                                break

                            liz1 = iz1

                            if inpt['z'][iz1] >= temp['z'][iz]:
                                Flags[iz] = Flags[iz] or True
                                Flags[iz+1] = Flags[iz+1] or True

                else:

                    # Flag all planes for read
                    Flags = np.ones(inpt['nodes0'][2], dtype=bool)

                    # Transform into Mm
                    inpt['x'] = np.array(inpt['x'])*1e-8
                    inpt['y'] = np.array(inpt['y'])*1e-8
                    inpt['z'] = np.array(inpt['z'])*1e-8

                # Identify scalars and vectors
                scal = [0,1,2,8,9,10,11]
                vec = [3,4]

                # Create space to read data
                for ii,read in \
                    zip(range(len(PMD['read'])),PMD['read']):

                    if read:

                        if ii in scal:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0]])
                        elif ii in vec:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0], \
                                                 3])
                        elif ii == 5:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0], \
                                                 self.NRL, 2])
                        elif ii == 6:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0], \
                                                 self.NRU, 2])
                        elif ii == 7:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0], \
                                                 9])

                    else:
                        inpt[ii] = -1

                # Interpolating
                if CONF['interpol']:

                    # Auxiliar initialization
                    izps = 1
                    NX = inpt['nodes'][0]
                    NY = inpt['nodes'][1]
                    P0 = {}
                    P1 = {}

                    for iz in range(inpt['nodes0'][2] - 1):

                        # If the next two planes are not needed,
                        # skip the current plane if have not been
                        # read
                        if not Flags[iz] or not Flags[iz+1]:
                            try:
                                check = P1['done']
                                P1 = {}
                            except KeyError:
                                P0 = self.skip_plane(f, planesize)
                            except :
                                raise
                            continue

                        # Get P0
                        try:
                            check = P0['done']
                        except KeyError:
                            P0 = self.get_plane(f)
                        except:
                            raise

                        # Get P1
                        P1 = self.get_plane(f)

                        zz0 = temp['z'][iz]
                        zz1 = temp['z'][iz+1]

                        for izp in range(izps-1,inpt['nodes'][2]):

                            izps = izp + 1
                            zz = inpt['z'][izp]

                            if zz > zz1:
                                break

                            if zz0 <= zz and zz1 >= zz:

                                # Interpolate in lower plane
                                if not P0['done']:
                                    for var in scal:
                                        if not PMD['read'][var]:
                                            continue
                                        r = interpolate.interp2d( \
                                             temp['y'],temp['x'], \
                                             P0[var])
                                        P0[var] = r(inpt['x'], \
                                                    inpt['y'])
                                    for var in vec:
                                        if not PMD['read'][var]:
                                            continue
                                        for ii in range(3):
                                            r = interpolate. \
                                                 interp2d( \
                                                    temp['y'], \
                                                    temp['x'], \
                                                    P0[var][:,:,ii])
                                            P0[var][0:NY,0:NX,ii] = \
                                                    r(inpt['x'], \
                                                      inpt['y'])
                                        P0[var] = P0[var][0:NY,0:NX,:]
                                    var = 5
                                    if PMD['read'][var]:
                                        for ii in range(self.NRL):
                                            for jj in range(2):
                                                r = interpolate.\
                                                     interp2d( \
                                                     temp['y'], \
                                                     temp['x'], \
                                                P0[var][:,:,ii,jj])
                                                P0[var] \
                                                  [0:NY,0:NX,ii,jj] \
                                                  = r(inpt['x'], \
                                                      inpt['y'])
                                        P0[var] = P0[var] \
                                                    [0:NY,0:NX,:,:]
                                    var = 6
                                    if PMD['read'][var]:
                                        for ii in range(self.NRU):
                                            for jj in range(2):
                                                r = interpolate. \
                                                     interp2d( \
                                                     temp['y'], \
                                                     temp['x'], \
                                                P0[var][:,:,ii,jj])
                                                P0[var] \
                                                  [0:NY,0:NX,ii,jj] \
                                                  = r(inpt['x'], \
                                                      inpt['y'])
                                        P0[var] = P0[var] \
                                                    [0:NY,0:NX,:,:]
                                    var = 7
                                    if PMD['read'][var]:
                                        for ii in range(9):
                                            r = interpolate. \
                                                 interp2d(temp['y'], \
                                                          temp['x'], \
                                                   P0[var][:,:,ii])
                                            P0[var][0:NY,0:NX,ii] = \
                                                       r(inpt['x'], \
                                                         inpt['y'])
                                        P0[var] = P0[var][0:NY,0:NX,:]
                                    P0['done'] = True

                                # Interpolate in upper plane
                                if not P1['done']:
                                    for var in scal:
                                        if not PMD['read'][var]:
                                            continue
                                        r = interpolate.interp2d( \
                                             temp['y'],temp['x'], \
                                             P1[var])
                                        P1[var] = r(inpt['x'], \
                                                    inpt['y'])
                                    for var in vec:
                                        if not PMD['read'][var]:
                                            continue
                                        for ii in range(3):
                                            r = interpolate. \
                                                 interp2d( \
                                                    temp['y'], \
                                                    temp['x'], \
                                                    P1[var][:,:,ii])
                                            P1[var][0:NY,0:NX,ii] = \
                                                    r(inpt['x'], \
                                                      inpt['y'])
                                        P1[var] = P1[var][0:NY,0:NX,:]
                                    var = 5
                                    if PMD['read'][var]:
                                        for ii in range(self.NRL):
                                            for jj in range(2):
                                                r = interpolate.\
                                                     interp2d( \
                                                     temp['y'], \
                                                     temp['x'], \
                                                P1[var][:,:,ii,jj])
                                                P1[var] \
                                                  [0:NY,0:NX,ii,jj] \
                                                  = r(inpt['x'], \
                                                      inpt['y'])
                                        P1[var] = P1[var] \
                                                    [0:NY,0:NX,:,:]
                                    var = 6
                                    if PMD['read'][var]:
                                        for ii in range(self.NRU):
                                            for jj in range(2):
                                                r = interpolate. \
                                                     interp2d( \
                                                     temp['y'], \
                                                     temp['x'], \
                                                P1[var][:,:,ii,jj])
                                                P1[var] \
                                                  [0:NY,0:NX,ii,jj] \
                                                  = r(inpt['x'], \
                                                      inpt['y'])
                                        P1[var] = P1[var] \
                                                    [0:NY,0:NX,:,:]
                                    var = 7
                                    if PMD['read'][var]:
                                        for ii in range(9):
                                            r = interpolate. \
                                                 interp2d(temp['y'], \
                                                          temp['x'], \
                                                   P1[var][:,:,ii])
                                            P1[var][0:NY,0:NX,ii] = \
                                                       r(inpt['x'], \
                                                         inpt['y'])
                                        P1[var] = P1[var][0:NY,0:NX,:]
                                    P1['done'] = True


                                # Interpolate in vertical
                                w1 = (zz1 - zz)/(zz1 - zz0)
                                w2 = 1 - w1
                                for var in scal:
                                    if not PMD['read'][var]:
                                        continue
                                    inpt[var][izp,:,:] = \
                                             w1*P0[var] + w2*P1[var]
                                for var in vec:
                                    if not PMD['read'][var]:
                                        continue
                                    for ii in range(3):
                                        inpt[var][izp,:,:,ii] = \
                                                w1*P0[var][:,:,ii] + \
                                                w2*P1[var][:,:,ii]
                                var = 5
                                if PMD['read'][var]:
                                    for ii in range(self.NRL):
                                        for jj in range(2):
                                            inpt[var] \
                                                [izp,:,:,ii,jj] = \
                                             w1*P0[var][:,:,ii,jj] + \
                                             w2*P1[var][:,:,ii,jj]
                                var = 6
                                if PMD['read'][var]:
                                    for ii in range(self.NRU):
                                        for jj in range(2):
                                            inpt[var] \
                                                [izp,:,:,ii,jj] = \
                                             w1*P0[var][:,:,ii,jj] + \
                                             w2*P1[var][:,:,ii,jj]
                                var = 7
                                if PMD['read'][var]:
                                    for ii in range(9):
                                        inpt[var][izp,:,:,ii] = \
                                         w1*P0[var][:,:,ii] + \
                                         w2*P1[var][:,:,ii]

                        if iz < inpt['nodes0'][2] - 2:

                            # Shift planes
                            if Flags[iz+2]:
                                P0 = copy.deepcopy(P1)
                            else:
                                P0 = {}
                        
                        else:

                            P1 = {}
                            P0 = {}

                # Do not interpolate
                else:

                    for iz in range(inpt['nodes'][2]):

                        P0 = self.get_plane(f)

                        for ii in scal:
                            if PMD['read'][ii]:
                                inpt[ii][iz,:,:] = P0[ii]
                        for ii in (vec+[5,6,7]):
                            if PMD['read'][ii]:
                                inpt[ii][iz,:,:,:] = P0[ii]

                f.close()

                PMD['data'] = inpt
                return True

        except:

            raise
            return False

######################################################################
######################################################################

    def get_data_h5(self):
        ''' Reads the pmd (in HDF5) for the twolevel module given the
            header.
            Returns true if the reading went well and false otherwise.
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Initialize progress bar
        self.pgc = 0
        self.pgl = 0

        # Check if there is a file at all
        if not os.path.isfile(PMD['head']['name']):
            return False

        # Initialize output
        inpt = {}

        # Read it as a pmd file
        try:

            self.pg.update_idletasks()

            with h5py.File(PMD['head']['name'],'r') as f:

                inpt['mod_ver'] = f['Module'].attrs['TL_VERSION'][0]
                inpt['amass'] = f['Module'].attrs['ATOM_MASS'][0]
                inpt['Aul'] = f['Module'].attrs['A_UL'][0]
                inpt['Eul'] = f['Module'].attrs['E_UL'][0]

                inpt['Jl2'] = f['Module'].attrs['JL2'][0]
                inpt['NRL'] = (inpt['Jl2'] + 1)*(inpt['Jl2'] + 1)
                self.NRL = inpt['NRL']

                inpt['Ju2'] = f['Module'].attrs['JU2'][0]
                inpt['NRU'] = (inpt['Ju2'] + 1)*(inpt['Ju2'] + 1)
                self.NRU = inpt['NRU']

                inpt['gl'] = f['Module'].attrs['GL'][0]
                inpt['gu'] = f['Module'].attrs['GU'][0]
                inpt['Tref'] = f['Module'].attrs['TEMP'][0]

                inpt['nodes'] = (np.asscalar(PMD['head'] \
                                                ['nodes0'][0]), \
                                 np.asscalar(PMD['head'] \
                                                ['nodes0'][1]), \
                                 np.asscalar(PMD['head'] \
                                                ['nodes0'][2]))

                inpt['x'] = PMD['head']['x'][0:PMD['head'] \
                                                  ['nodes0'][0]]
                inpt['y'] = PMD['head']['y'][0:PMD['head'] \
                                                  ['nodes0'][1]]
                inpt['z'] = PMD['head']['z'][0:PMD['head'] \
                                                  ['nodes0'][2]]

                inpt['tempo'] = np.asarray(f['Module']['temp_bc'])
                inpt['nodes0'] = inpt['nodes']

                planesize = PMD['head']['nodes0'][0]* \
                            PMD['head']['nodes0'][1]* \
                            8*(22 + 2*(self.NRL + self.NRU))

                # Create geometry axis
                if CONF['interpol']:

                    # Change number of nodes
                    inpt['nodes'] = [int(CONF['nodes']['x']), \
                                     int(CONF['nodes']['y']), \
                                     int(CONF['nodes']['z'])]


                    # Check sizes
                    for ii in range(len(inpt['nodes'])):
                        if inpt['nodes'][ii] > inpt['nodes0'][ii] or \
                           inpt['nodes'][ii] <= 1:
                            tkMessageBox.showwarning("Load", \
                                         "The dimension of the " + \
                                         "interpolated data " + \
                                         "cannot be larger than " + \
                                         "the original or " + \
                                         "smaller than 2.", \
                                         parent=self)
                            return False

                    # Original grid in Mm
                    temp = {}
                    temp['x'] = np.array(inpt['x'])*1e-8
                    temp['y'] = np.array(inpt['y'])*1e-8
                    temp['z'] = np.array(inpt['z'])*1e-8

                    # Final grid
                    inpt['x'] = np.linspace(min(temp['x']), \
                                            max(temp['x']), \
                                            inpt['nodes'][0])
                    inpt['y'] = np.linspace(min(temp['y']), \
                                            max(temp['y']), \
                                            inpt['nodes'][1])
                    inpt['z'] = np.linspace(min(temp['z']), \
                                            max(temp['z']), \
                                            inpt['nodes'][2])

                    # Check what planes do we really need to read
                    Flags = np.zeros(inpt['nodes0'][2], dtype=bool)
                    liz1 = 0
                    for iz in range(inpt['nodes0'][2]-1):

                        for iz1 in range(liz1,inpt['nodes'][2]):

                            if inpt['z'][iz1] > temp['z'][iz+1]:
                                break

                            liz1 = iz1

                            if inpt['z'][iz1] >= temp['z'][iz]:
                                Flags[iz] = Flags[iz] or True
                                Flags[iz+1] = Flags[iz+1] or True

                else:

                    # Flag all planes for read
                    Flags = np.ones(inpt['nodes0'][2], dtype=bool)

                    # Transform into Mm
                    inpt['x'] = np.array(inpt['x'])*1e-8
                    inpt['y'] = np.array(inpt['y'])*1e-8
                    inpt['z'] = np.array(inpt['z'])*1e-8

                # Identify scalars and vectors
                scal = [0,1,2,8,9,10,11]
                vec = [3,4]

                # Create space to read data
                for ii,read in \
                    zip(range(len(PMD['read'])),PMD['read']):

                    if read:

                        if ii in scal:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0]])
                        elif ii in vec:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0], \
                                                 3])
                        elif ii == 5:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0], \
                                                 self.NRL, 2])
                        elif ii == 6:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0], \
                                                 self.NRU, 2])
                        elif ii == 7:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0], \
                                                 9])

                    else:
                        inpt[ii] = -1

                # Interpolating
                if CONF['interpol']:

                    # Auxiliar initialization
                    izps = 1
                    NX = inpt['nodes'][0]
                    NY = inpt['nodes'][1]
                    P0 = {}
                    P1 = {}

                    # With HDF5 I don't read the data sequentially as
                    # for the binary file, but I can read a whole
                    # plane easily if we know the correspoding iz, so
                    # izt is a counter to keep track of the plane to
                    # be read or skipped
                    izt = 0
                    
                    for iz in range(inpt['nodes0'][2] - 1):

                        # If the next two planes are not needed,
                        # skip the current plane if have not been
                        # read
                        if not Flags[iz] or not Flags[iz+1]:
                            try:
                                check = P1['done']
                                P1 = {}
                            except KeyError:
                                P0 = {}  # nothing to be done for
                                         # HDF5, but the
                                         # izt counter has to go up
                                izt = izt + 1
                            except :
                                raise
                            continue

                        # Get P0
                        try:
                            check = P0['done']
                        except KeyError:
                            P0 = self.get_plane_h5(f,izt)
                            izt = izt + 1
                        except:
                            raise

                        # Get P1
                        P1 = self.get_plane_h5(f,izt)
                        izt = izt + 1

                        zz0 = temp['z'][iz]
                        zz1 = temp['z'][iz+1]

                        for izp in range(izps-1,inpt['nodes'][2]):

                            izps = izp + 1
                            zz = inpt['z'][izp]

                            if zz > zz1:
                                break

                            if zz0 <= zz and zz1 >= zz:

                                # Interpolate in lower plane
                                if not P0['done']:
                                    for var in scal:
                                        if not PMD['read'][var]:
                                            continue
                                        r = interpolate.interp2d( \
                                             temp['y'],temp['x'], \
                                             P0[var])
                                        P0[var] = r(inpt['x'], \
                                                    inpt['y'])
                                    for var in vec:
                                        if not PMD['read'][var]:
                                            continue
                                        for ii in range(3):
                                            r = interpolate. \
                                                 interp2d( \
                                                    temp['y'], \
                                                    temp['x'], \
                                                    P0[var][:,:,ii])
                                            P0[var][0:NY,0:NX,ii] = \
                                                    r(inpt['x'], \
                                                      inpt['y'])
                                        P0[var] = P0[var][0:NY,0:NX,:]
                                    var = 5
                                    if PMD['read'][var]:
                                        for ii in range(self.NRL):
                                            for jj in range(2):
                                                r = interpolate.\
                                                     interp2d( \
                                                     temp['y'], \
                                                     temp['x'], \
                                                P0[var][:,:,ii,jj])
                                                P0[var] \
                                                  [0:NY,0:NX,ii,jj] \
                                                  = r(inpt['x'], \
                                                      inpt['y'])
                                        P0[var] = P0[var] \
                                                    [0:NY,0:NX,:,:]
                                    var = 6
                                    if PMD['read'][var]:
                                        for ii in range(self.NRU):
                                            for jj in range(2):
                                                r = interpolate. \
                                                     interp2d( \
                                                     temp['y'], \
                                                     temp['x'], \
                                                P0[var][:,:,ii,jj])
                                                P0[var] \
                                                  [0:NY,0:NX,ii,jj] \
                                                  = r(inpt['x'], \
                                                      inpt['y'])
                                        P0[var] = P0[var] \
                                                    [0:NY,0:NX,:,:]
                                    var = 7
                                    if PMD['read'][var]:
                                        for ii in range(9):
                                            r = interpolate. \
                                                 interp2d(temp['y'], \
                                                          temp['x'], \
                                                   P0[var][:,:,ii])
                                            P0[var][0:NY,0:NX,ii] = \
                                                       r(inpt['x'], \
                                                         inpt['y'])
                                        P0[var] = P0[var][0:NY,0:NX,:]
                                    P0['done'] = True

                                # Interpolate in upper plane
                                if not P1['done']:
                                    for var in scal:
                                        if not PMD['read'][var]:
                                            continue
                                        r = interpolate.interp2d( \
                                             temp['y'],temp['x'], \
                                             P1[var])
                                        P1[var] = r(inpt['x'], \
                                                    inpt['y'])
                                    for var in vec:
                                        if not PMD['read'][var]:
                                            continue
                                        for ii in range(3):
                                            r = interpolate. \
                                                 interp2d( \
                                                    temp['y'], \
                                                    temp['x'], \
                                                    P1[var][:,:,ii])
                                            P1[var][0:NY,0:NX,ii] = \
                                                    r(inpt['x'], \
                                                      inpt['y'])
                                        P1[var] = P1[var][0:NY,0:NX,:]
                                    var = 5
                                    if PMD['read'][var]:
                                        for ii in range(self.NRL):
                                            for jj in range(2):
                                                r = interpolate.\
                                                     interp2d( \
                                                     temp['y'], \
                                                     temp['x'], \
                                                P1[var][:,:,ii,jj])
                                                P1[var] \
                                                  [0:NY,0:NX,ii,jj] \
                                                  = r(inpt['x'], \
                                                      inpt['y'])
                                        P1[var] = P1[var] \
                                                    [0:NY,0:NX,:,:]
                                    var = 6
                                    if PMD['read'][var]:
                                        for ii in range(self.NRU):
                                            for jj in range(2):
                                                r = interpolate. \
                                                     interp2d( \
                                                     temp['y'], \
                                                     temp['x'], \
                                                P1[var][:,:,ii,jj])
                                                P1[var] \
                                                  [0:NY,0:NX,ii,jj] \
                                                  = r(inpt['x'], \
                                                      inpt['y'])
                                        P1[var] = P1[var] \
                                                    [0:NY,0:NX,:,:]
                                    var = 7
                                    if PMD['read'][var]:
                                        for ii in range(9):
                                            r = interpolate. \
                                                 interp2d(temp['y'], \
                                                          temp['x'], \
                                                   P1[var][:,:,ii])
                                            P1[var][0:NY,0:NX,ii] = \
                                                       r(inpt['x'], \
                                                         inpt['y'])
                                        P1[var] = P1[var][0:NY,0:NX,:]
                                    P1['done'] = True


                                # Interpolate in vertical
                                w1 = (zz1 - zz)/(zz1 - zz0)
                                w2 = 1 - w1
                                for var in scal:
                                    if not PMD['read'][var]:
                                        continue
                                    inpt[var][izp,:,:] = \
                                             w1*P0[var] + w2*P1[var]
                                for var in vec:
                                    if not PMD['read'][var]:
                                        continue
                                    for ii in range(3):
                                        inpt[var][izp,:,:,ii] = \
                                                w1*P0[var][:,:,ii] + \
                                                w2*P1[var][:,:,ii]
                                var = 5
                                if PMD['read'][var]:
                                    for ii in range(self.NRL):
                                        for jj in range(2):
                                            inpt[var] \
                                                [izp,:,:,ii,jj] = \
                                             w1*P0[var][:,:,ii,jj] + \
                                             w2*P1[var][:,:,ii,jj]
                                var = 6
                                if PMD['read'][var]:
                                    for ii in range(self.NRU):
                                        for jj in range(2):
                                            inpt[var] \
                                                [izp,:,:,ii,jj] = \
                                             w1*P0[var][:,:,ii,jj] + \
                                             w2*P1[var][:,:,ii,jj]
                                var = 7
                                if PMD['read'][var]:
                                    for ii in range(9):
                                        inpt[var][izp,:,:,ii] = \
                                         w1*P0[var][:,:,ii] + \
                                         w2*P1[var][:,:,ii]

                        if iz < inpt['nodes0'][2] - 2:

                            # Shift planes
                            if Flags[iz+2]:
                                P0 = copy.deepcopy(P1)
                            else:
                                P0 = {}
                        
                        else:

                            P1 = {}
                            P0 = {}

                # Do not interpolate
                else:

                    # Scalars
                    if PMD['read'][0]:
                        inpt[0] = np.asarray( \
                                     f['Module/g_data']['eps'])
                    if PMD['read'][1]:
                        inpt[1] = np.asarray( \
                                     f['Module/g_data']['temp'])
                    if PMD['read'][2]:
                        inpt[2] = np.asarray( \
                                     f['Module/g_data']['density'])
                    if PMD['read'][8]:
                        inpt[8] = np.asarray( \
                                     f['Module/g_data']['a_voigt'])
                    if PMD['read'][9]:
                        inpt[9] = np.asarray( \
                                     f['Module/g_data']['delta2'])
                    if PMD['read'][10]:
                        inpt[10] = np.asarray( \
                                      f['Module/g_data']['c_opac'])
                    if PMD['read'][11]:
                        inpt[11] = np.asarray( \
                                      f['Module/g_data']['c_emis'])

                    # Vectors
                    if PMD['read'][3]:
                        inpt[3] = np.stack( \
                                  (f['Module/g_data']['B'][:,:,:,0], \
                                   f['Module/g_data']['B'][:,:,:,1], \
                                   f['Module/g_data']['B'][:,:,:,2]),\
                                  axis=3)
                    if PMD['read'][4]:
                        inpt[4] = np.stack( \
                                  (f['Module/g_data']['V'][:,:,:,0], \
                                   f['Module/g_data']['V'][:,:,:,1], \
                                   f['Module/g_data']['V'][:,:,:,2]),\
                                  axis=3)

                    # Density matrix
                    if PMD['read'][5]:
                        inpt[5][:,:,:,0,0] = np.asarray( \
                                    f['Module/g_data']['dm'][:,:,:,0])
                        inpt[5][:,:,:,0,1] = np.asarray( \
                                    f['Module/g_data']['dm'][:,:,:,1])

                    if PMD['read'][6]:
                        inpt[6][:,:,:,0,0] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,2])
                        inpt[6][:,:,:,0,1] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,3])
                        inpt[6][:,:,:,1,0] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,4])
                        inpt[6][:,:,:,1,1] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,5])
                        inpt[6][:,:,:,2,0] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,6])
                        inpt[6][:,:,:,2,1] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,7])
                        inpt[6][:,:,:,3,0] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,8])
                        inpt[6][:,:,:,3,1] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,9])
                        inpt[6][:,:,:,4,0] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,10])
                        inpt[6][:,:,:,4,1] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,11])
                        inpt[6][:,:,:,5,0] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,12])
                        inpt[6][:,:,:,5,1] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,13])
                        inpt[6][:,:,:,6,0] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,14])
                        inpt[6][:,:,:,6,1] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,15])
                        inpt[6][:,:,:,7,0] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,16])
                        inpt[6][:,:,:,7,1] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,17])
                        inpt[6][:,:,:,8,0] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,18])
                        inpt[6][:,:,:,8,1] = np.asarray( \
                                   f['Module/g_data']['dm'][:,:,:,19])

                    # Radiation field
                    if PMD['read'][7]:
                        inpt[7][:,:,:,0] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,0])
                        inpt[7][:,:,:,1] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,1])
                        inpt[7][:,:,:,2] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,2])
                        inpt[7][:,:,:,3] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,3])
                        inpt[7][:,:,:,4] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,4])
                        inpt[7][:,:,:,5] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,5])
                        inpt[7][:,:,:,6] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,6])
                        inpt[7][:,:,:,7] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,7])
                        inpt[7][:,:,:,8] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,8])
                f.close()

                PMD['data'] = inpt
                return True

        except:

            raise
            return False
        
######################################################################
######################################################################

    def get_plane(self, f):
        ''' Load the data from a plane in the pmd file
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Initialize plane
        T = {}
        for ii in range(12):
            T[ii] = []
        T['done'] = False

        # Read plane
        for iy in range(PMD['head']['nodes0'][1]):
            for ix in range(PMD['head']['nodes0'][0]):
                bytes = f.read(8)
                if PMD['read'][0]:
                    T[0].append(struct.unpack( \
                                CONF['endian'] + 'd', bytes)[0])
                bytes = f.read(8)
                if PMD['read'][1]:
                    T[1].append(struct.unpack( \
                                CONF['endian'] + 'd', bytes)[0])
                bytes = f.read(8)
                if PMD['read'][2]:
                    T[2].append(struct.unpack( \
                                CONF['endian'] + 'd', bytes)[0])
                for ii in range(3):
                    bytes = f.read(8)
                    if PMD['read'][3]:
                        T[3].append(struct.unpack( \
                                    CONF['endian'] + 'd', bytes)[0])
                for ii in range(3):
                    bytes = f.read(8)
                    if PMD['read'][4]:
                        T[4].append(struct.unpack( \
                                    CONF['endian'] + 'd', bytes)[0])
                for ii in range(self.NRL*2):
                    bytes = f.read(8)
                    if PMD['read'][5]:
                        T[5].append(struct.unpack( \
                                    CONF['endian'] + 'd', bytes)[0])
                for ii in range(self.NRU*2):
                    bytes = f.read(8)
                    if PMD['read'][6]:
                        T[6].append(struct.unpack( \
                                    CONF['endian'] + 'd', bytes)[0])
                for ii in range(9):
                    bytes = f.read(8)
                    if PMD['read'][7]:
                        T[7].append(struct.unpack( \
                                    CONF['endian'] + 'd', bytes)[0])
                bytes = f.read(8)
                if PMD['read'][8]:
                    T[8].append(struct.unpack( \
                                CONF['endian'] + 'd', bytes)[0])
                bytes = f.read(8)
                if PMD['read'][9]:
                    T[9].append(struct.unpack( \
                                CONF['endian'] + 'd', bytes)[0])
                bytes = f.read(8)
                if PMD['read'][10]:
                    T[10].append(struct.unpack( \
                                 CONF['endian'] + 'd', bytes)[0])
                bytes = f.read(8)
                if PMD['read'][11]:
                    T[11].append(struct.unpack( \
                                 CONF['endian'] + 'd', bytes)[0])

        if PMD['read'][0]:
            T[0] = np.array(T[0]).reshape(PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0])
        if PMD['read'][1]:
            T[1] = np.array(T[1]).reshape(PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0])
        if PMD['read'][2]:
            T[2] = np.array(T[2]).reshape(PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0])
        if PMD['read'][3]:
            T[3] = np.array(T[3]).reshape(PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0], \
                                          3)
        if PMD['read'][4]:
            T[4] = np.array(T[4]).reshape(PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0], \
                                          3)
        if PMD['read'][5]:
            T[5] = np.array(T[5]).reshape(PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0], \
                                          self.NRL, 2)
        if PMD['read'][6]:
            T[6] = np.array(T[6]).reshape(PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0], \
                                          self.NRU, 2)
        if PMD['read'][7]:
            T[7] = np.array(T[7]).reshape(PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0], \
                                          9)
        if PMD['read'][8]:
            T[8] = np.array(T[8]).reshape(PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0])
        if PMD['read'][9]:
            T[9] = np.array(T[9]).reshape(PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0])
        if PMD['read'][10]:
            T[10] = np.array(T[10]).reshape( \
                                          PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0])
        if PMD['read'][11]:
            T[11] = np.array(T[11]).reshape( \
                                          PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0])

        self.pgc += 100./float(PMD['head']['nodes0'][2])
        pgc = int(self.pgc)
        if pgc > self.pgl:
            diff = pgc - self.pgl
            for ii in range(diff):
                if pgc <= 99:
                    self.pg.step()
                    self.pg.update_idletasks()
            self.pgl += diff

        return T

######################################################################
######################################################################

    def get_plane_h5(self, f, izt):
        ''' Load the data from plane izt in the pmd (HDF5) file
        '''

        global PMD
        PMD = source.general.PMD

        # Initialize plane
        T = {}
        for ii in range(12):
            T[ii] = []
        T['done'] = False

        if PMD['read'][0]:
            T[0] = np.asarray(f['Module/g_data']['eps'][izt,:,:])

        if PMD['read'][1]:
            T[1] = np.asarray(f['Module/g_data']['temp'][izt,:,:])

        if PMD['read'][2]:
            T[2] = np.asarray(f['Module/g_data']['density'][izt,:,:])

        if PMD['read'][3]:
            T[3] = np.asarray(f['Module/g_data']['B'][izt,:,:,:])

        if PMD['read'][4]:
            T[4] = np.asarray(f['Module/g_data']['V'][izt,:,:,:])

        T[5] = np.empty([PMD['head']['nodes0'][1], \
                         PMD['head']['nodes0'][0], \
                         self.NRL, 2])

        if PMD['read'][5]:
            T[5][:,:,0,0] = np.asarray( \
                                  f['Module/g_data']['dm'][izt,:,:,0])
            T[5][:,:,0,1] = np.asarray( \
                                  f['Module/g_data']['dm'][izt,:,:,1])


        T[6] = np.empty([PMD['head']['nodes0'][1], \
                         PMD['head']['nodes0'][0], \
                         self.NRU, 2])

        if PMD['read'][6]:
            T[6][:,:,0,0] = np.asarray( \
                                  f['Module/g_data']['dm'][izt,:,:,2])
            T[6][:,:,0,1] = np.asarray( \
                                  f['Module/g_data']['dm'][izt,:,:,3])
            T[6][:,:,1,0] = np.asarray( \
                                  f['Module/g_data']['dm'][izt,:,:,4])
            T[6][:,:,1,1] = np.asarray( \
                                  f['Module/g_data']['dm'][izt,:,:,5])
            T[6][:,:,2,0] = np.asarray( \
                                  f['Module/g_data']['dm'][izt,:,:,6])
            T[6][:,:,2,1] = np.asarray( \
                                  f['Module/g_data']['dm'][izt,:,:,7])
            T[6][:,:,3,0] = np.asarray( \
                                  f['Module/g_data']['dm'][izt,:,:,8])
            T[6][:,:,3,1] = np.asarray( \
                                  f['Module/g_data']['dm'][izt,:,:,9])
            T[6][:,:,4,0] = np.asarray( \
                                 f['Module/g_data']['dm'][izt,:,:,10])
            T[6][:,:,4,1] = np.asarray( \
                                 f['Module/g_data']['dm'][izt,:,:,11])
            T[6][:,:,5,0] = np.asarray( \
                                 f['Module/g_data']['dm'][izt,:,:,12])
            T[6][:,:,5,1] = np.asarray( \
                                 f['Module/g_data']['dm'][izt,:,:,13])
            T[6][:,:,6,0] = np.asarray( \
                                 f['Module/g_data']['dm'][izt,:,:,14])
            T[6][:,:,6,1] = np.asarray( \
                                 f['Module/g_data']['dm'][izt,:,:,15])
            T[6][:,:,7,0] = np.asarray( \
                                 f['Module/g_data']['dm'][izt,:,:,16])
            T[6][:,:,7,1] = np.asarray( \
                                 f['Module/g_data']['dm'][izt,:,:,17])
            T[6][:,:,8,0] = np.asarray( \
                                 f['Module/g_data']['dm'][izt,:,:,18])
            T[6][:,:,8,1] = np.asarray( \
                                 f['Module/g_data']['dm'][izt,:,:,19])

        if PMD['read'][7]:
            T[7] = np.asarray(f['Module/g_data']['jkq'][izt,:,:])

        if PMD['read'][8]:
            T[8] = np.asarray(f['Module/g_data']['a_voigt'][izt,:,:])


        if PMD['read'][9]:
            T[9] = np.asarray(f['Module/g_data']['delta2'][izt,:,:])

        if PMD['read'][10]:
            T[10] = np.asarray(f['Module/g_data']['c_opac'][izt,:,:])

        if PMD['read'][11]:
            T[11] = np.asarray(f['Module/g_data']['c_emis'][izt,:,:])

        self.pgc += 100./float(PMD['head']['nodes0'][2])
        pgc = int(self.pgc)
        if pgc > self.pgl:
            diff = pgc - self.pgl
            for ii in range(diff):
                if pgc <= 99:
                    self.pg.step()
                    self.pg.update_idletasks()
            self.pgl += diff

        return T

    
######################################################################
######################################################################


    def interpol_data(self):
        ''' Interpolates the pmd data that is loaded
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Initialize progress bar
        self.pgc = 0
        self.pgl = 0

        # Initialize auxiliar dictionary
        inpt = {}

        # Change number of nodes
        inpt['nodes'] = [int(CONF['nodes']['x']), \
                         int(CONF['nodes']['y']), \
                         int(CONF['nodes']['z'])]

        # Check sizes
        for ii in range(len(inpt['nodes'])):
            if inpt['nodes'][ii] > PMD['data']['nodes0'][ii] or \
               inpt['nodes'][ii] <= 1:
                tkMessageBox.showwarning("Load", \
                                         "The dimension of the " + \
                                         "interpolated data " + \
                                         "cannot be larger than " + \
                                         "the original or " + \
                                         "smaller than 2.", \
                                         parent=self)
                return False

        # Original grid in Mm
        temp = {}
        temp['x'] = PMD['data']['x']
        temp['y'] = PMD['data']['y']
        temp['z'] = PMD['data']['z']

        # Final grid
        inpt['x'] = np.linspace(min(temp['x']), \
                                max(temp['x']), \
                                inpt['nodes'][0])
        inpt['y'] = np.linspace(min(temp['y']), \
                                max(temp['y']), \
                                inpt['nodes'][1])
        inpt['z'] = np.linspace(min(temp['z']), \
                                max(temp['z']), \
                                inpt['nodes'][2])

        # Check that axes are different
        if np.array_equal(temp['x'],inpt['x']) and \
           np.array_equal(temp['y'],inpt['y']) and \
           np.array_equal(temp['z'],inpt['z']):
            return

        # Data pointer
        data = PMD['data']
        NX,NY,NZ = inpt['nodes']

        # Save axis
        data['x'] = inpt['x']
        data['y'] = inpt['y']
        data['z'] = inpt['z']
        data['nodes'] = inpt['nodes']
        inpt = 0

        # Identify scalars and vectors
        scal = [0,1,2,8,9,10,11]
        vec = [3,4]

        #
        # Variables are loaded, different system to save memory
        #

        # Get a mesh of the final grid
        zz,yy,xx = np.meshgrid(data['z'],data['y'],data['x'], \
                               indexing='ij')


        # scalars
        for ii in scal:
            if PMD['read'][ii]:
                r = interpolate.RegularGridInterpolator( \
                        (temp['z'],temp['y'],temp['x']), data[ii])
                data[ii] = r((zz,yy,xx))
            # Update bar
            self.pgc += 100./float(len(PMD['read']))
            pgc = int(self.pgc)
            if pgc > self.pgl:
                diff = pgc - self.pgl
                for ii in range(diff):
                    if pgc <= 99:
                        self.pg.step()
                        self.pg.update_idletasks()
                self.pgl += diff

        # vectors
        for ii in vec:
            if PMD['read'][ii]:
                aux = data[ii]
                for jj in range(3):
                    r = interpolate.RegularGridInterpolator( \
                            (temp['z'],temp['y'],temp['x']), \
                             aux[:,:,:,jj])
                    data[ii][:,:,:,jj] = r((zz,yy,xx))
            # Update bar
            self.pgc += 300./float(len(PMD['read']))
            pgc = int(self.pgc)
            if pgc > self.pgl:
                diff = pgc - self.pgl
                for ii in range(diff):
                    if pgc <= 99:
                        self.pg.step()
                        self.pg.update_idletasks()
                self.pgl += diff

        # rhol
        ii = 5
        if PMD['read'][ii]:
            aux = data[ii]
            for jj in range(data['NRL']):
                r = interpolate.RegularGridInterpolator( \
                        (temp['z'],temp['y'],temp['x']), \
                         aux[:,:,:,jj])
                data[ii][:,:,:,jj] = r((zz,yy,xx))
        # Update bar
        self.pgc += float(data['NRL'])*100./float(len(PMD['read']))
        pgc = int(self.pgc)
        if pgc > self.pgl:
            diff = pgc - self.pgl
            for ii in range(diff):
                if pgc <= 99:
                    self.pg.step()
                    self.pg.update_idletasks()
            self.pgl += diff

        # rhou
        ii = 6
        if PMD['read'][ii]:
            aux = data[ii]
            for jj in range(data['NRU']):
                r = interpolate.RegularGridInterpolator( \
                        (temp['z'],temp['y'],temp['x']), \
                         aux[:,:,:,jj])
                data[ii][:,:,:,jj] = r((zz,yy,xx))
        # Update bar
        self.pgc += float(data['NRU'])*100./float(len(PMD['read']))
        pgc = int(self.pgc)
        if pgc > self.pgl:
            diff = pgc - self.pgl
            for ii in range(diff):
                if pgc <= 99:
                    self.pg.step()
                    self.pg.update_idletasks()
            self.pgl += diff

        # J
        ii = 7
        if PMD['read'][ii]:
            aux = data[ii]
            for jj in range(9):
                r = interpolate.RegularGridInterpolator( \
                        (temp['z'],temp['y'],temp['x']), \
                         aux[:,:,:,jj])
                data[ii][:,:,:,jj] = r((zz,yy,xx))
        # Update bar
        self.pgc += 900./float(len(PMD['read']))
        pgc = int(self.pgc)
        if pgc > self.pgl:
            diff = pgc - self.pgl
            for ii in range(diff):
                if pgc <= 99:
                    self.pg.step()
                    self.pg.update_idletasks()
            self.pgl += diff

        return True


######################################################################
######################################################################

    def skip_plane(self, f, skip):
        ''' Skip a plane
        '''

        bytes = f.seek(skip, 1)

        self.pgc += 100./float(PMD['head']['nodes0'][2])
        pgc = int(self.pgc)
        if pgc > self.pgl:
            diff = pgc - self.pgl
            for ii in range(diff):
                if pgc <= 99:
                    self.pg.step()
                    self.pg.update_idletasks()
            self.pgl += diff

        return {}

######################################################################
######################################################################
######################################################################
######################################################################
