# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# contpol.py                                                         #
#                                                                    #
# Ángel de Vicente (angel.de.vicente@iac.es)                         #
# Tanausú del Pino Alemán                                            #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Manager for the contpol module                                     #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  10/03/2020 - V1.0.2 - Added colors to indicate focus (TdPA)       #
#                      - Solved focus problems (AdV)                 #
#                                                                    #
#  24/01/2020 - V1.0.1 - Added option to store vtk files (TdPA)      #
#                                                                    #
#  28/11/2019 - V1.0.0 - First version, based on V1.0.4 of the       #
#                        twolevel.py by TdPA. (AdV)                  #
#                                                                    #
######################################################################
######################################################################
######################################################################

import source.general
from source.loader import *
from source.tooltip import *
import h5py
from scipy.ndimage import zoom

######################################################################
######################################################################

class MODULE_class(Toplevel):
    ''' Class that defines the main body of the module class
    '''

######################################################################
######################################################################

    def __init__(self, parent, title = None):
        ''' Initializes the widget single tab
        '''

        global CONF
        CONF = source.general.CONF

        # Initialized a support window.
        Toplevel.__init__(self, parent)
        self.transient(parent)

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Set the title if any
        if title:
            self.title(title)

        # Active border
        self.configure(background=CONF['col_act'])

        self.parent = parent
        self.result = None
        self.plt_action = None

        head = Frame(self)
        body = Frame(head)
        self.up = Frame(body)
        self.mid = Frame(body)
        self.do = Frame(body)
        self.bot = Frame(body)
        self.head = head
        self.box = body
        self.initial_focus = self.body(body)
        head.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        body.pack(fill=BOTH,expand=1)

        self.grab_set()
        self.lift()
        
        if not self.initial_focus:
            self.initial_focus = self

        self.buttons(self.do)

        self.display(self.mid)

        self.optionbox(self.up)

        self.progressbar(self.bot)

        self.up.pack(fill=BOTH,expand=1,side=TOP)
        self.mid.pack(fill=BOTH,expand=1,side=TOP)
        self.do.pack(fill=BOTH,expand=1,side=BOTTOM)
        self.bot.pack(fill=BOTH,expand=1,side=BOTTOM)
        body.pack(fill=BOTH,expand=1)
        head.pack(fill=BOTH,expand=1)

        self.protocol("WM_DELETE_WINDOW", self.close)
        self.bind("<Escape>", self.close)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)


######################################################################
######################################################################

    def body(self, master):
        ''' Creates the dialog body and set initial focus.
        '''

        return master

######################################################################
######################################################################

    def close(self, event=None):
        ''' Method that handles the window closing.
        '''

        global PMD
        PMD = source.general.PMD

        if PMD['head']['loaded']:
            tkMessageBox.showwarning("Load", \
                                     "You must unload the data " + \
                                     "before closing!",parent=self)
            return

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()


######################################################################
######################################################################
######################################################################
######################################################################

    def optionbox(self, box):
        ''' Defines all the control widgets of PMD_class class.
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # List the variables for this module
        PMD['vars'] = {0: 'Nh', \
                       1: 'Ne', \
                       2: 'Temperature', \
                       3: 'Radiation'}
        PMD['file'] = {0: 'Nh', \
                       1: 'Ne', \
                       2: 'Temperature', \
                       3: 'Radiation field'}
        PMD['read'] = [0,0,0,0]

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # General label
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l = Label(box0, text="Load Manager", font=self.cFont, \
                    justify=CENTER)
        createToolTip(b0l,TOOLTIPS['1'])
        b0l.grid(row=row,column=col,columnspan=3,sticky=NSEW)
        # Loaded file
        # Status
        box00 = LabelFrame(box0)
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        Grid.rowconfigure(box00,0,weight=1)
        Grid.columnconfigure(box00,0,weight=1)
        if PMD['head']['loaded']:
            self.data0_name = Label(box00, text='Loaded', \
                                    anchor=CENTER, \
                                    font=self.cFont)
            createToolTip(self.data0_name, TOOLTIPS['5'])
        else:
            self.data0_name = Label(box00, text='Not loaded', \
                                    anchor=CENTER, \
                                    font=self.cFont)
            createToolTip(self.data0_name, TOOLTIPS['6'])
        self.data0_name.grid(row=0,column=0,sticky=NSEW)
        box00.grid(row=row,column=col,sticky=NSEW)
        # Load button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_load = Button(box0,text="Load",font=self.cFont, \
                                 command=self.load0)
        createToolTip(self.butt0_load,TOOLTIPS['7'])
        self.butt0_load.grid(row=row,column=col,sticky=NSEW)
        # Unload button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_unload = Button(box0,text="Unload", \
                                   font=self.cFont, \
                                   command=self.unload0)
        createToolTip(self.butt0_unload,TOOLTIPS['8'])
        self.butt0_unload.grid(row=row,column=col,sticky=NSEW)
        # Disable widgets if no data loaded
        if PMD['head']['loaded']:
            self.butt0_load.config(state='disabled')
            self.butt1_close.config(state='disabled')
            self.butt1_plot.config(state='normal')
            self.butt1_save.config(state='normal')
            self.butt0_unload.config(state='normal')
        else:
            self.butt0_load.config(state='normal')
            self.butt1_close.config(state='normal')
            self.butt1_plot.config(state='disabled')
            self.butt1_save.config(state='disabled')
            self.butt0_unload.config(state='disabled')
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)


        # Interpolation (box1)
        box1 = LabelFrame(box)
        row = 0
        col = 0
        # General label
        Grid.rowconfigure(box1,row,weight=1)
        Grid.columnconfigure(box1,col,weight=1)
        b1l = Label(box1, text="Interpolation parameters", \
                    font=self.cFont, \
                    justify=CENTER)
        b1l.grid(row=row,column=col,columnspan=8,sticky=NSEW)
        # Check for interpolation
        row += 1
        col = 0
        Grid.columnconfigure(box1,col,weight=1)
        self.int1_stat = IntVar()
        if CONF['interpol']:
            self.int1_stat.set(1)
        else:
            self.int1_stat.set(0)
        self.int1_check = Checkbutton(box1, \
                                      text="Interpolate data " + \
                                           "while reading     ", \
                                      anchor=CENTER, \
                                      font=self.cFont, \
                                      variable=self.int1_stat, \
                                      command=self.check1)
        createToolTip(self.int1_check,text=TOOLTIPS['27'])
        self.int1_check.grid(row=row,column=col,sticky=NSEW)
        #
        # Entries
        #
        # X
        # Label
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.b1l1 = Label(box1, text='    NX:', \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b1l1.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        CONF['nodes']['x']=PMD['head']['nodes0'][0]
        self.nx1_name = StringVar()
        self.nx1_name.set(CONF['nodes']['x'])
        self.nx1_name.trace('w',self.nx1_val)
        self.nx1_entry = Entry(box1, font=self.cFont, \
                               textvariable=self.nx1_name)
        self.nx1_entry.grid(row=row,column=col,sticky=NSEW)
        # Y
        # Label
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.b1l2 = Label(box1, text='    NY:', \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b1l2.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        CONF['nodes']['y']=PMD['head']['nodes0'][1]
        self.ny1_name = StringVar()
        self.ny1_name.set(CONF['nodes']['y'])
        self.ny1_name.trace('w',self.ny1_val)
        self.ny1_entry = Entry(box1, font=self.cFont, \
                               textvariable=self.ny1_name)
        self.ny1_entry.grid(row=row,column=col,sticky=NSEW)
        # Z
        # Label
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.b1l3 = Label(box1, text='    NZ:', \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b1l3.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        CONF['nodes']['z']=PMD['head']['nodes0'][2]
        self.nz1_name = StringVar()
        self.nz1_name.set(CONF['nodes']['z'])
        self.nz1_name.trace('w',self.nz1_val)
        self.nz1_entry = Entry(box1, font=self.cFont, \
                               textvariable=self.nz1_name)
        self.nz1_entry.grid(row=row,column=col,sticky=NSEW)
        # Interpolate button
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.butt1_int = Button(box1, text="Interpolate", \
                                font=self.cFont, command=self.int1)
        createToolTip(self.butt1_int,TOOLTIPS['40'])
        self.butt1_int.grid(row=row,column=col,sticky=NSEW)
        if PMD['head']['loaded']:
            self.butt1_int.config(state='normal')
            self.int1_check.config(state='disabled')
        else:
            self.butt1_int.config(state='disabled')
            self.int1_check.config(state='normal')
        # Pack the box
        box1.pack(fill=BOTH, expand=1, side = TOP)



        # Variables to read (box1)
        box2 = LabelFrame(box)
        row = 0
        col = 0
        jump = 4
        # General label
        Grid.rowconfigure(box2,row,weight=1)
        Grid.columnconfigure(box2,col,weight=1)
        b2l = Label(box2, text="Variables to read", \
                    font=self.cFont, \
                    justify=CENTER)
        b2l.grid(row=row,column=col,columnspan=jump,sticky=NSEW)
        # Check for interpolation
        row += 1
        col = -1
        self.var2_stat = []
        self.var2_check = []
        for ii in range(len(PMD['read'])):
            col += 1
            if col >= jump:
                row += 1
                col = 0
            self.var2_stat.append(IntVar())
            Grid.rowconfigure(box2,col,weight=1)
            Grid.columnconfigure(box2,col,weight=1)
            if PMD['read'][ii]:
                self.var2_stat[ii].set(1)
            else:
                self.var2_stat[ii].set(0)
            self.var2_check.append(Checkbutton(box2, \
                                   text=PMD['vars'][ii], \
                                   anchor=CENTER, \
                                   font=self.cFont, \
                                   variable=self.var2_stat[ii], \
                                   command=lambda jj=ii: \
                                           self.check2(jj)))
            createToolTip(self.var2_check[ii],text=TOOLTIPS['27'])
            self.var2_check[ii].grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box2.pack(fill=BOTH, expand=1, side = TOP)



        # General checks
        box3 = LabelFrame(box)
        row = 0
        col = 0
        # Check all
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.butt3_checka = Button(box3,text="Check all", \
                                   font=self.cFont, \
                                   command=self.checka3)
        createToolTip(self.butt0_unload,TOOLTIPS['46'])
        self.butt3_checka.grid(row=row,column=col,sticky=NSEW)
        # Uncheck all
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.butt3_uchecka = Button(box3,text="Uncheck all", \
                                   font=self.cFont, \
                                   command=self.uchecka3)
        createToolTip(self.butt0_unload,TOOLTIPS['47'])
        self.butt3_uchecka.grid(row=row,column=col,sticky=NSEW)
        # Toggle all
        col += 1
        Grid.columnconfigure(box3,col,weight=1)
        self.butt3_togglea = Button(box3,text="Toggle all", \
                                    font=self.cFont, \
                                    command=self.togglea3)
        createToolTip(self.butt0_unload,TOOLTIPS['48'])
        self.butt3_togglea.grid(row=row,column=col,sticky=NSEW)
        if PMD['head']['loaded']:
            self.butt3_checka.config(state='disabled')
            self.butt3_uchecka.config(state='disabled')
            self.butt3_togglea.config(state='disabled')
        else:
            self.butt3_checka.config(state='normal')
            self.butt3_uchecka.config(state='normal')
            self.butt3_togglea.config(state='normal')
        # Pack the box
        box3.pack(fill=BOTH, expand=1, side = TOP)


######################################################################
######################################################################

    def display(self, box):
        ''' Initializes the display of the header
        '''

        global PMD
        PMD = source.general.PMD
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Create information label frame
        box0 = LabelFrame(box)
        Grid.rowconfigure(box0,0,weight=1)
        Grid.columnconfigure(box0,0,weight=1)
        if not PMD['head']['loaded']:
            text = '\n\nLoad the pmd file\n\n'
            self.data_label = Label(box0, text=text, anchor=CENTER, \
                                    justify=CENTER, relief=FLAT, \
                                    bd=15, padx=3, pady=3, \
                                    font=self.cFont)
        else:
            data = PMD['data']
            module = PMD['head']['module']
            text = '\n'
            text += ' Module {0} version {1}\n'.format(module, \
                                               PMD['head']['pmd_ver'])
            text += ' Frequency: {0}\n'.format(data['frequency'])
            form = ' Original dimensions: {0}x{1}x{2} nodes\n'
            text += form.format(*data['nodes0'])
            form = ' Current dimensions: {0}x{1}x{2} nodes\n'
            text += form.format(*data['nodes'])
            text += '\n'
            self.data_label = Label(box0, text=text, anchor=CENTER, \
                              justify=LEFT, relief=FLAT, \
                              bd=15, padx=3, pady=3, \
                              font=self.cFont)
        self.data_label.grid(row=0,column=0,sticky=NSEW)
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def buttons(self, box):
        ''' Defines the control buttons of the widget
        '''

        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # Plot button
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_plot = Button(box0, text="Plot",  \
                                 font=self.cFont, \
                                 command=self.plot)
        createToolTip(self.butt1_plot,TOOLTIPS['42'])
        self.butt1_plot.grid(row=row,column=col,sticky=NSEW)
        # Plot with C
        '''
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_plotC = Button(box0, text="Plot with C",  \
                                 font=self.cFont, \
                                 command=self.plotC)
        createToolTip(self.butt1_plotC,TOOLTIPS['43'])
        self.butt1_plotC.grid(row=row,column=col,sticky=NSEW)
        # Check that the executable is there
        if os.path.isfile('extractor'):
            self.butt1_plotC.config(state='normal')
        else:
            self.butt1_plotC.config(state='disabled')
        '''
        # Save vtk
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_save = Button(box0, text="Save vtk",  \
                                 font=self.cFont, \
                                 command=self.save)
        createToolTip(self.butt1_save,TOOLTIPS['49'])
        self.butt1_save.grid(row=row,column=col,sticky=NSEW)
        # Close button
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_close = Button(box0, text="Close", \
                                  font=self.cFont, command=self.close)
        createToolTip(self.butt1_close,TOOLTIPS['11'])
        self.butt1_close.grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def progressbar(self, box):
        ''' Initializes the progress bar
        '''

        self.pgf = Frame(box)
        Grid.rowconfigure(self.pgf,0,weight=1)
        Grid.columnconfigure(self.pgf,0,weight=1)
        self.pg = Progressbar(self.pgf)
        self.pg.grid(row=0,column=0,sticky=NSEW)
        self.pgf.pack(fill=BOTH, expand=1, side = TOP)


######################################################################
######################################################################
######################################################################
######################################################################

    def load0(self):
        ''' Method that handles the Load button. Load the pmd file
            content from currently selected pmd file
        '''

        global PMD
        PMD = source.general.PMD

        # Check that you selected something
        if sum(PMD['read']) < 1:
            tkMessageBox.showwarning("Load", \
                                     "You must select at least " + \
                                     "one variable to read.", \
                                     parent=self)
            return

        # Signal in the GUI that we are loading
        self.data0_name.configure(text='Loading',font=self.cFont)
        self.data0_name.update_idletasks()

        # Try to read the data
        if PMD['head']['io'] == "hdf5":
            check = self.get_data_h5()
        else:
            check = self.get_data()
            
        if check:
            PMD['head']['loaded'] = True
        else:
            tkMessageBox.showwarning("Load", \
                                     "Could not read pmd file.", \
                                     parent=self)
            return

        self.set_top()
        self.set_mid()
        self.pg.step(-100)
        self.pg.update_idletasks()

        return

######################################################################
######################################################################

    def unload0(self):
        ''' Method that handles the Unload button. Unload the pmd
            data already loaded.
        '''

        global PMD
        PMD = source.general.PMD

        PMD['data'] = None
        PMD['head']['loaded'] = False
        self.set_top()
        self.set_mid()

        return

######################################################################
######################################################################

    def check1(self):
        ''' Check to interpolate while reading
        '''

        global CONF
        CONF = source.general.CONF

        CONF['interpol'] = self.int1_stat.get() == 1
        self.set_top()

        return

######################################################################
######################################################################

    def int1(self):
        ''' Button to interpolate read data
        '''

        # Try to interpolate the data
        check = self.interpol_data()
        if not check:
            tkMessageBox.showwarning("Load", \
                                     "Could not interpolate " + \
                                     "pmd data.",parent=self)
            return

        self.set_top()
        self.set_mid()
        self.pg.step(-100)
        self.pg.update_idletasks()

        return

######################################################################
######################################################################

    def check2(self, val):
        ''' Check to read a variable
        '''

        global PMD
        PMD = source.general.PMD

        PMD['read'][val] = self.var2_stat[val].get() == 1
        self.set_top()

        return

######################################################################
######################################################################

    def checka3(self):
        ''' Check to read all variables
        '''

        global PMD
        PMD = source.general.PMD

        for ii in range(len(PMD['read'])):
            PMD['read'][ii] = True
            self.var2_stat[ii].set(1)
        self.set_top()

        return

######################################################################
######################################################################

    def uchecka3(self):
        ''' Uncheck to read all variables
        '''

        global PMD
        PMD = source.general.PMD

        for ii in range(len(PMD['read'])):
            PMD['read'][ii] = False
            self.var2_stat[ii].set(0)
        self.set_top()

        return

######################################################################
######################################################################

    def togglea3(self):
        ''' Toggles to read all variables
        '''

        global PMD
        PMD = source.general.PMD

        for ii in range(len(PMD['read'])):
            PMD['read'][ii] = not PMD['read'][ii]
            if PMD['read'][ii]:
                self.var2_stat[ii].set(1)
            else:
                self.var2_stat[ii].set(0)
        self.set_top()

        return

######################################################################
######################################################################

    def plot(self):
        ''' Plot button
        '''

        global PMD
        PMD = source.general.PMD

        # Identify scalars and vectors
        scal = [0,1,2]

        # Data pointer
        data = PMD['data']

        # Initialize list of load files
        d = []

        # Open script file
        try:
            f = open('cache/dump.py','w')
            folder = 'cache/'
        except IOError:
            tkMessageBox.showwarning("Plot", \
                                     "I could not write in " + \
                                     "cache folder. I am going " + \
                                     "to write in current " + \
                                     "directory, but this is not " + \
                                     "normal behavior.",parent=self)
            f = open('dump.py','w')
            folder = ''
        except:
            raise

        # Script header
        f.write('# -*- coding: utf-8 -*-\n\n')
        f.write('from numpy import array\n')
        f.write('try:\n')
        f.write('    engine = mayavi.engine\n')
        f.write('except NameError:\n')
        f.write('    from mayavi.api import Engine\n')
        f.write('    engine = Engine()\n')
        f.write('    engine.start()\n')
        f.write('if len(engine.scenes) == 0:\n')
        f.write('    engine.new_scene()\n')
        f.write('scene = engine.scenes[0]\n')

        # Initialize file names list
        files = []

        # Create data files

        # Create grid once
        grid = vtk.RectilinearGrid(data['x'],data['y'],data['z'])

        # Create scalars
        for ii in scal:
            if not PMD['read'][ii]:
                continue
            pdata = vtk.PointData(vtk.Scalars(data[ii].flatten(), \
                                  name=PMD['file'][ii]))
            vtkdata = vtk.VtkData(grid, pdata)
            files.append(folder+PMD['file'][ii]+'.vtk')
            vtkdata.tofile(files[-1],'binary')
            f.write("vtk_file_reader = engine.open('" + \
                    files[-1] + "', scene)\n")

        # Create J
        ii = 3
        if PMD['read'][ii]:
            KQv = []
            for K in range(3):
                for Q in range(0,K+1):
                    for jj,R in zip(range(2),['  Real','  Imag']):
                        if Q == 0 and 'Imag' in R:
                            continue
                        KQv.append(' K='+str(K)+' Q='+str(Q)+R)
            for jj in range(9):
                KQ = KQv[jj]
                pdata = vtk.PointData(vtk.Scalars(\
                                 data[ii][:,:,:,jj].flatten(), \
                                 name=PMD['file'][ii]+KQ))
                vtkdata = vtk.VtkData(grid, pdata)
                files.append(folder+PMD['file'][ii]+KQ+'.vtk')
                vtkdata.tofile(files[-1],'binary')
                f.write("vtk_file_reader = engine.open('" + \
                        files[-1] + "', scene)\n")


        # Close script
        f.close()

        # Make current passive
        self.config(background=CONF['col_pas'])

        # Call mayavi2
        try:
            subprocess.call(["mayavi2","cache/dump.py"])
        except OSError:
            tkMessageBox.showwarning("Plot", \
                                     "I could not open Mayavi2. " + \
                                     "Ensure that Mayavi2 is " + \
                                     "correctly installed and " + \
                                     "that the command mayavi2 " + \
                                     "opens it.",parent=self)
            return
        except:
            raise

        # Make current active
        self.configure(background=CONF['col_act'])


        # Remove script
        try:
            os.remove(folder+'dump.py')
        except OSError:
            tkMessageBox.showwarning("Plot", \
                                     "I could not find my python " + \
                                     "script to delete it. If " + \
                                     "you moved it, I will not " + \
                                     "touch it.",parent=self)
        except:
            raise

        # Delete vtk files
        for fil in files:
            try:
                os.remove(fil)
            except OSError:
                pass
            except:
                raise

        return

######################################################################
######################################################################

    def save(self):
        ''' Save button
        '''

        global PMD
        PMD = source.general.PMD

        # Identify scalars and vectors
        scal = [0,1,2]

        # Data pointer
        data = PMD['data']

        # Initialize list of load files
        d = []

        # Ask for folder
        folder = tkFileDialog.askdirectory(initialdir= \
                                            PATH['pmd_dir'], \
                                            parent=self)

        # If no folder, return
        if not isinstance(folder,str):
            return
        if not os.path.isdir(folder):
            return
        if list(folder)[-1] != '/':
            folder += '/'

        # Initialize file names list
        files = []

        # Create data files

        # Create grid once
        grid = vtk.RectilinearGrid(data['x'],data['y'],data['z'])

        # Create scalars
        for ii in scal:
            if not PMD['read'][ii]:
                continue
            pdata = vtk.PointData(vtk.Scalars(data[ii].flatten(), \
                                  name=PMD['file'][ii]))
            vtkdata = vtk.VtkData(grid, pdata)
            files.append(folder+PMD['file'][ii]+'.vtk')
            vtkdata.tofile(files[-1],'binary')

        # Create J
        ii = 3
        if PMD['read'][ii]:
            KQv = []
            for K in range(3):
                for Q in range(0,K+1):
                    for jj,R in zip(range(2),['  Real','  Imag']):
                        if Q == 0 and 'Imag' in R:
                            continue
                        KQv.append(' K='+str(K)+' Q='+str(Q)+R)
            for jj in range(9):
                KQ = KQv[jj]
                pdata = vtk.PointData(vtk.Scalars(\
                                 data[ii][:,:,:,jj].flatten(), \
                                 name=PMD['file'][ii]+KQ))
                vtkdata = vtk.VtkData(grid, pdata)
                files.append(folder+PMD['file'][ii]+KQ+'.vtk')
                vtkdata.tofile(files[-1],'binary')

        return

######################################################################
######################################################################

    def plotC(self):
        ''' Plot button to call C code
        '''

        global PMD
        PMD = source.general.PMD

        # HDF5 not implemented in C
        if PMD['head']['io'] == "hdf5":
            tkMessageBox.showwarning("PlotC", \
                                     "Sorry, but the C method " + \
                                     "is only for the binary " + \
                                     "format.",parent=self)
            return

        # Identify scalars and vectors
        scal = [0,1,2]

        # Construct running string
        # Executable
        srun = ["./extractor"]
        # File name
        srun.append(PMD['head']['name'])
        # No verbose
        srun.append("0")
        # New grid nodes
        if CONF['interpol']:
            srun.append("{0}".format(CONF['nodes']['x']))
            srun.append("{0}".format(CONF['nodes']['y']))
            srun.append("{0}".format(CONF['nodes']['z']))
        else:
            srun.append("{0}".format(PMD['head']['nodes0'][0]))
            srun.append("{0}".format(PMD['head']['nodes0'][1]))
            srun.append("{0}".format(PMD['head']['nodes0'][2]))
        lref = len(srun)

        # Variables to plot
        for i in range(4):
            if PMD['read'][i]:
                srun.append("{0}".format(i))

        # If nothing to read, stop
        if len(srun) <= lref:
            tkMessageBox.showwarning("PlotC", \
                                     "Choose something to read", \
                                     parent=self)
            return

        # Call C code
        subprocess.call(srun)

        # Open script file
        try:
            f = open('cache/dump.py','w')
            folder = 'cache/'
        except IOError:
            tkMessageBox.showwarning("PlotC", \
                                     "I could not write in " + \
                                     "cache folder. I am going " + \
                                     "to write in current " + \
                                     "directory, but this is not " + \
                                     "normal behavior.",parent=self)
            f = open('dump.py','w')
            folder = ''
        except:
            raise

        # Script header
        f.write('# -*- coding: utf-8 -*-\n\n')
        f.write('from numpy import array\n')
        f.write('try:\n')
        f.write('    engine = mayavi.engine\n')
        f.write('except NameError:\n')
        f.write('    from mayavi.api import Engine\n')
        f.write('    engine = Engine()\n')
        f.write('    engine.start()\n')
        f.write('if len(engine.scenes) == 0:\n')
        f.write('    engine.new_scene()\n')
        f.write('scene = engine.scenes[0]\n')

        # Initialize file names list
        files = []

        # Scalars
        for ii in scal:
            if not PMD['read'][ii]:
                continue
            files.append(PMD['file'][ii]+'.vtk')
            f.write("vtk_file_reader = engine.open('" + \
                    files[-1] + "', scene)\n")

        # J
        ii = 3
        if PMD['read'][ii]:
            KQv = []
            for K in range(3):
                for Q in range(0,K+1):
                    for jj,R in zip(range(2),['  Real','  Imag']):
                        if Q == 0 and 'Imag' in R:
                            continue
                        KQv.append(' K='+str(K)+' Q='+str(Q)+R)
            for jj in range(9):
                KQ = KQv[jj]
                files.append(PMD['file'][ii]+KQ+'.vtk')
                f.write("vtk_file_reader = engine.open('" + \
                        files[-1] + "', scene)\n")

        # Close script
        f.close()

        # Check files are there
        check = True
        for fil in files:
            if not os.path.isfile(fil):
                check = False
                break

        # Call mayavi2
        if check:
            try:
                subprocess.call(["mayavi2","cache/dump.py"])
            except OSError:
                tkMessageBox.showwarning("PlotC", \
                                      "I could not open Mayavi2. " + \
                                      "Ensure that Mayavi2 is " + \
                                      "correctly installed and " + \
                                      "that the command mayavi2 " + \
                                         "opens it.",parent=self)
                return
            except:
                raise

        else:
            tkMessageBox.showwarning("PlotC", \
                                     "There was some problem " + \
                                     "running the C extractor.", \
                                     parent=self)

        # Remove script
        try:
            os.remove(folder+'dump.py')
        except OSError:
            tkMessageBox.showwarning("PlotC", \
                                     "I could not find my python " + \
                                     "script to delete it. If " + \
                                     "you moved it, I will not " + \
                                     "touch it.",parent=self)
        except:
            raise

        # Delete vtk files
        for fil in files:
            try:
                os.remove(fil)
            except OSError:
                pass
            except:
                raise

        return

######################################################################
######################################################################
######################################################################
######################################################################

    def nx1_val(self, *dumm):
        ''' Validates nx entry
        '''

        global CONF
        CONF = source.general.CONF

        val = self.nx1_name.get()
        try:
            num = int(val)
            if num < 0:
                raise ValueError
            CONF['nodes']['x'] = val
        except ValueError:
            if val!="":
                self.nx1_name.set(CONF['nodes']['x'])
            else:
                CONF['nodes']['x'] = 0
        except:
            raise

######################################################################
######################################################################

    def ny1_val(self, *dumm):
        ''' Validates ny entry
        '''

        global CONF
        CONF = source.general.CONF

        val = self.ny1_name.get()
        try:
            num = int(val)
            if num < 0:
                raise ValueError
            CONF['nodes']['y'] = val
        except ValueError:
            if val!="":
                self.ny1_name.set(CONF['nodes']['y'])
            else:
                CONF['nodes']['y'] = 0
        except:
            raise

######################################################################
######################################################################

    def nz1_val(self, *dumm):
        ''' Validates nz entry
        '''

        global CONF
        CONF = source.general.CONF

        val = self.nz1_name.get()
        try:
            num = int(val)
            if num < 0:
                raise ValueError
            CONF['nodes']['z'] = val
        except ValueError:
            if val!="":
                self.nz1_name.set(CONF['nodes']['z'])
            else:
                CONF['nodes']['z'] = 0
        except:
            raise

######################################################################
######################################################################

    def update_entry(self, event=None):
        ''' Really updates entries
        '''

        global CONF
        CONF = source.general.CONF

        # nx variable
        val = self.nx1_name.get()
        try:
            num = int(val)
            if num > 0:
                CONF['nodes']['x'] = val
        except ValueError:
            pass
        except:
            raise
        # ny variable
        val = self.ny1_name.get()
        try:
            num = int(val)
            if num > 0:
                CONF['nodes']['y'] = val
        except ValueError:
            pass
        except:
            raise
        # nz variable
        val = self.nz1_name.get()
        try:
            num = int(val)
            if num > 0:
                CONF['nodes']['z'] = val
        except ValueError:
            pass
        except:
            raise

######################################################################
######################################################################

    def set_top(self):
        ''' Reconfigures the top of the widget window
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Status
        if PMD['head']['loaded']:
            self.data0_name.config(text='Loaded', \
                                   font=self.cFont)
            createToolTip(self.data0_name, TOOLTIPS['5'])
            self.butt0_load.config(state='disabled')
            self.int1_check.config(state='disabled')
            self.butt1_int.config(state='normal')
            self.butt1_close.config(state='disabled')
            self.butt1_plot.config(state='normal')
            self.butt1_save.config(state='normal')
            self.butt0_unload.config(state='normal')
            for ii in range(len(PMD['read'])):
                self.var2_check[ii].config(state='disabled')
        else:
            self.data0_name.config(text='Not loaded', \
                                   font=self.cFont)
            createToolTip(self.data0_name, TOOLTIPS['6'])
            self.butt0_load.config(state='normal')
            self.int1_check.config(state='normal')
            self.butt1_int.config(state='disabled')
            self.butt1_close.config(state='normal')
            self.butt1_plot.config(state='disabled')
            self.butt1_save.config(state='disabled')
            self.butt0_unload.config(state='disabled')
            for ii in range(len(PMD['read'])):
                self.var2_check[ii].config(state='normal')

        # Check if you compiled extractor
        '''
        if os.path.isfile('extractor') :
            self.butt1_plotC.config(state='normal')
        else:
            self.butt1_plotC.config(state='disabled')
        '''

        # Interpolation
        if CONF['interpol']:
            self.int1_stat.set(1)
        else:
            self.int1_stat.set(0)

        # Variables to read
        for ii in range(len(PMD['read'])):
            if PMD['read'][ii]:
                self.var2_stat[ii].set(1)
            else:
                self.var2_stat[ii].set(0)

        # Check buttons status
        if PMD['head']['loaded']:
            self.butt3_checka.config(state='disabled')
            self.butt3_uchecka.config(state='disabled')
            self.butt3_togglea.config(state='disabled')
        else:
            self.butt3_checka.config(state='normal')
            self.butt3_uchecka.config(state='normal')
            self.butt3_togglea.config(state='normal')

        return

######################################################################
######################################################################

    def set_mid(self):
        ''' Reconfigures the display of the widget window
        '''

        global PMD
        PMD = source.general.PMD

        # Update information label frame
        if not PMD['head']['loaded']:
            text = '\n\nLoad the pmd file\n\n'
            self.data_label.config(justify=CENTER)
        else:
            data = PMD['data']
            module = PMD['head']['module']
            text = '\n'
            text += ' Module {0} version {1}\n'.format(module, \
                                               PMD['head']['pmd_ver'])
            text += ' Frequency: {0}\n'.format(data['frequency'])
            form = ' Original dimensions: {0}x{1}x{2} nodes\n'
            text += form.format(*data['nodes0'])
            form = ' Current dimensions: {0}x{1}x{2} nodes\n'
            text += form.format(*data['nodes'])
            text += '\n'
            self.data_label.config(justify=LEFT)
        self.data_label.config(text=text)

        # Check if you compiled extractor
        '''
        if os.path.isfile('extractor'):
            self.butt1_plotC.config(state='normal')
        else:
            self.butt1_plotC.config(state='disabled')
        '''

        return

######################################################################
######################################################################
######################################################################
######################################################################


    def get_data(self):
        ''' Reads the pmd for the continuum module given the header
            Returns true if the reading went well and false otherwise.
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Initialize progress bar
        self.pgc = 0
        self.pgl = 0

        # Check if there is a file at all
        if not os.path.isfile(PMD['head']['name']):
            return False

        # Initialize output
        inpt = {}

        # Read it as a pmd file
        try:

            self.pg.update_idletasks()

            with open(PMD['head']['name'],'rb') as f:

                f.seek(PMD['head']['size'])
                bytes = f.read(4)
                inpt['mod_ver'] = struct.unpack( \
                                  CONF['endian'] + 'I', bytes)[0]

                bytes = f.read(8)
                inpt['frequency'] = struct.unpack( \
                                CONF['endian'] + 'd', bytes)[0]
                inpt['nodes'] = []
                inpt['nodes'] = (PMD['head']['nodes0'][0], \
                                 PMD['head']['nodes0'][1], \
                                 PMD['head']['nodes0'][2])

                inpt['x'] = PMD['head']['x'] \
                                       [0:PMD['head']['nodes0'][0]]
                inpt['y'] = PMD['head']['y'] \
                                       [0:PMD['head']['nodes0'][1]]
                inpt['z'] = PMD['head']['z'] \
                                       [0:PMD['head']['nodes0'][2]]


                bytes = f.read(8*inpt['nodes'][0]*
                                 inpt['nodes'][1])
                inpt['tempo'] = np.array(struct.unpack( \
                                CONF['endian'] + 'd'* \
                                inpt['nodes'][0]* \
                                inpt['nodes'][1], bytes)). \
                                reshape(inpt['nodes'][1], \
                                        inpt['nodes'][0])
                inpt['nodes0'] = inpt['nodes']

                planesize = PMD['head']['nodes0'][0]* \
                            PMD['head']['nodes0'][1]* \
                            (4*3 + 8*9)

                # Create geometry axis
                if CONF['interpol']:

                    # Change number of nodes
                    inpt['nodes'] = [int(CONF['nodes']['x']), \
                                     int(CONF['nodes']['y']), \
                                     int(CONF['nodes']['z'])]

                    # Check sizes
                    for ii in range(len(inpt['nodes'])):
                        if inpt['nodes'][ii] > inpt['nodes0'][ii] or \
                           inpt['nodes'][ii] <= 1:
                            tkMessageBox.showwarning("Load", \
                                         "The dimension of the " + \
                                         "interpolated data " + \
                                         "cannot be larger than " + \
                                         "the original or " + \
                                         "smaller than 2.", \
                                         parent=self)
                            return False

                    # Original grid in Mm
                    temp = {}
                    temp['x'] = np.array(inpt['x'])*1e-8
                    temp['y'] = np.array(inpt['y'])*1e-8
                    temp['z'] = np.array(inpt['z'])*1e-8

                    # Final grid
                    inpt['x'] = np.linspace(min(temp['x']), \
                                            max(temp['x']), \
                                            inpt['nodes'][0])
                    inpt['y'] = np.linspace(min(temp['y']), \
                                            max(temp['y']), \
                                            inpt['nodes'][1])
                    inpt['z'] = np.linspace(min(temp['z']), \
                                            max(temp['z']), \
                                            inpt['nodes'][2])

                    # Check what planes do we really need to read
                    Flags = np.zeros(inpt['nodes0'][2], dtype=bool)
                    liz1 = 0
                    for iz in range(inpt['nodes0'][2]-1):

                        for iz1 in range(liz1,inpt['nodes'][2]):

                            if inpt['z'][iz1] > temp['z'][iz+1]:
                                break

                            liz1 = iz1

                            if inpt['z'][iz1] >= temp['z'][iz]:
                                Flags[iz] = Flags[iz] or True
                                Flags[iz+1] = Flags[iz+1] or True

                else:

                    # Flag all planes for read
                    Flags = np.ones(inpt['nodes0'][2], dtype=bool)

                    # Transform into Mm
                    inpt['x'] = np.array(inpt['x'])*1e-8
                    inpt['y'] = np.array(inpt['y'])*1e-8
                    inpt['z'] = np.array(inpt['z'])*1e-8

                # Identify scalars and vectors
                scal = [0,1,2]

                # Create space to read data
                for ii,read in \
                    zip(range(len(PMD['read'])),PMD['read']):

                    if read:

                        if ii in scal:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0]], \
                                                 dtype="float32")
                        elif ii == 3:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0], \
                                                 9])

                    else:
                        inpt[ii] = -1

                # Interpolating
                if CONF['interpol']:

                    # Auxiliar initialization
                    izps = 1
                    NX = inpt['nodes'][0]
                    NY = inpt['nodes'][1]
                    P0 = {}
                    P1 = {}

                    izt = 0
                    
                    for iz in range(inpt['nodes0'][2] - 1):

                        # If the next two planes are not needed,
                        # skip the current plane if have not been
                        # read
                        if not Flags[iz] or not Flags[iz+1]:
                            try:
                                check = P1['done']
                                P1 = {}
                            except KeyError:
                                P0 = self.skip_plane(f, planesize)
                                izt = izt + 1
                            except :
                                raise
                            continue

                        # Get P0
                        try:
                            check = P0['done']
                        except KeyError:
                            P0 = self.get_plane(f)
                            izt = izt + 1
                        except:
                            raise

                        # Get P1
                        P1 = self.get_plane(f)
                        izt = izt + 1

                        zz0 = temp['z'][iz]
                        zz1 = temp['z'][iz+1]

                        for izp in range(izps-1,inpt['nodes'][2]):

                            izps = izp + 1
                            zz = inpt['z'][izp]

                            if zz > zz1:
                                break

                            if zz0 <= zz and zz1 >= zz:

                                # Interpolate in lower plane
                                if not P0['done']:
                                    for var in scal:
                                        if not PMD['read'][var]:
                                            continue
                                        r = interpolate.interp2d( \
                                             temp['y'],temp['x'], \
                                             P0[var])
                                        P0[var] = r(inpt['x'], \
                                                    inpt['y'])
                                    var = 3
                                    if PMD['read'][var]:
                                        for ii in range(9):
                                            r = interpolate. \
                                                 interp2d(temp['y'], \
                                                          temp['x'], \
                                                   P0[var][:,:,ii])
                                            P0[var][0:NY,0:NX,ii] = \
                                                       r(inpt['x'], \
                                                         inpt['y'])
                                        P0[var] = P0[var][0:NY,0:NX,:]
                                    P0['done'] = True

                                # Interpolate in upper plane
                                if not P1['done']:
                                    for var in scal:
                                        if not PMD['read'][var]:
                                            continue
                                        r = interpolate.interp2d( \
                                             temp['y'],temp['x'], \
                                             P1[var])
                                        P1[var] = r(inpt['x'], \
                                                    inpt['y'])
                                    var = 3
                                    if PMD['read'][var]:
                                        for ii in range(9):
                                            r = interpolate. \
                                                 interp2d(temp['y'], \
                                                          temp['x'], \
                                                   P1[var][:,:,ii])
                                            P1[var][0:NY,0:NX,ii] = \
                                                       r(inpt['x'], \
                                                         inpt['y'])
                                        P1[var] = P1[var][0:NY,0:NX,:]
                                    P1['done'] = True


                                # Interpolate in vertical
                                w1 = (zz1 - zz)/(zz1 - zz0)
                                w2 = 1 - w1
                                for var in scal:
                                    if not PMD['read'][var]:
                                        continue
                                    inpt[var][izp,:,:] = \
                                             w1*P0[var] + w2*P1[var]
                                var = 3
                                if PMD['read'][var]:
                                    for ii in range(9):
                                        inpt[var][izp,:,:,ii] = \
                                         w1*P0[var][:,:,ii] + \
                                         w2*P1[var][:,:,ii]

                        if iz < inpt['nodes0'][2] - 2:

                            # Shift planes
                            if Flags[iz+2]:
                                P0 = copy.deepcopy(P1)
                            else:
                                P0 = {}
                        
                        else:

                            P1 = {}
                            P0 = {}

                # Do not interpolate
                else:

                    for iz in range(inpt['nodes'][2]):

                        P0 = self.get_plane(f)

                        for ii in scal:
                            if PMD['read'][ii]:
                                inpt[ii][iz,:,:] = P0[ii]
                        for ii in ([3]):
                            if PMD['read'][ii]:
                                inpt[ii][iz,:,:,:] = P0[ii]

                                
                f.close()

                PMD['data'] = inpt
                return True

        except:

            raise
            return False

######################################################################
######################################################################

    def get_data_h5(self):
        ''' Reads the pmd (in HDF5) for the continuum module given the
            header.
            Returns true if the reading went well and false otherwise.
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Initialize progress bar
        self.pgc = 0
        self.pgl = 0

        # Check if there is a file at all
        if not os.path.isfile(PMD['head']['name']):
            return False

        # Initialize output
        inpt = {}

        # Read it as a pmd file
        try:

            self.pg.update_idletasks()

            with h5py.File(PMD['head']['name'],'r') as f:

                inpt['mod_ver'] = f['Module'].attrs['MOD_VERSION'][0]
                inpt['frequency'] = f['Module'].attrs['FREQUENCY'][0]

                inpt['nodes'] = (np.asscalar(PMD['head'] \
                                                ['nodes0'][0]), \
                                 np.asscalar(PMD['head'] \
                                                ['nodes0'][1]), \
                                 np.asscalar(PMD['head'] \
                                                ['nodes0'][2]))

                inpt['x'] = PMD['head']['x'][0:PMD['head'] \
                                                  ['nodes0'][0]]
                inpt['y'] = PMD['head']['y'][0:PMD['head'] \
                                                  ['nodes0'][1]]
                inpt['z'] = PMD['head']['z'][0:PMD['head'] \
                                                  ['nodes0'][2]]

                inpt['tempo'] = np.asarray(f['Module']['temp_bc'])
                inpt['nodes0'] = inpt['nodes']

                planesize = PMD['head']['nodes0'][0]* \
                            PMD['head']['nodes0'][1]* \
                            (4*3 + 8*9)

                # Create geometry axis
                if CONF['interpol']:

                    # Change number of nodes
                    inpt['nodes'] = [int(CONF['nodes']['x']), \
                                     int(CONF['nodes']['y']), \
                                     int(CONF['nodes']['z'])]

                    # Check sizes
                    for ii in range(len(inpt['nodes'])):
                        if inpt['nodes'][ii] > inpt['nodes0'][ii] or \
                           inpt['nodes'][ii] <= 1:
                            tkMessageBox.showwarning("Load", \
                                         "The dimension of the " + \
                                         "interpolated data " + \
                                         "cannot be larger than " + \
                                         "the original or " + \
                                         "smaller than 2.", \
                                         parent=self)
                            return False

                    # Original grid in Mm
                    temp = {}
                    temp['x'] = np.array(inpt['x'])*1e-8
                    temp['y'] = np.array(inpt['y'])*1e-8
                    temp['z'] = np.array(inpt['z'])*1e-8

                    # Final grid
                    inpt['x'] = np.linspace(min(temp['x']), \
                                            max(temp['x']), \
                                            inpt['nodes'][0])
                    inpt['y'] = np.linspace(min(temp['y']), \
                                            max(temp['y']), \
                                            inpt['nodes'][1])
                    inpt['z'] = np.linspace(min(temp['z']), \
                                            max(temp['z']), \
                                            inpt['nodes'][2])

                    # Check what planes do we really need to read
                    Flags = np.zeros(inpt['nodes0'][2], dtype=bool)
                    liz1 = 0
                    for iz in range(inpt['nodes0'][2]-1):

                        for iz1 in range(liz1,inpt['nodes'][2]):

                            if inpt['z'][iz1] > temp['z'][iz+1]:
                                break

                            liz1 = iz1

                            if inpt['z'][iz1] >= temp['z'][iz]:
                                Flags[iz] = Flags[iz] or True
                                Flags[iz+1] = Flags[iz+1] or True

                    
                else:

                    # Flag all planes for read
                    Flags = np.ones(inpt['nodes0'][2], dtype=bool)

                    # Transform into Mm
                    inpt['x'] = np.array(inpt['x'])*1e-8
                    inpt['y'] = np.array(inpt['y'])*1e-8
                    inpt['z'] = np.array(inpt['z'])*1e-8


                # Identify scalars and vectors
                scal = [0,1,2]

                # Create space to read data
                for ii,read in \
                    zip(range(len(PMD['read'])),PMD['read']):

                    if read:

                        if ii in scal:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0]], \
                                                 dtype="float32")
                        elif ii == 3:
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0], \
                                                 9])

                    else:
                        inpt[ii] = -1

                # Interpolating
                if CONF['interpol']:

                    # Auxiliar initialization
                    izps = 1
                    NX = inpt['nodes'][0]
                    NY = inpt['nodes'][1]
                    P0 = {}
                    P1 = {}

                    # With HDF5 I don't read the data sequentially as
                    # for the binary file, but I can read a whole
                    # plane easily if we know the correspoding iz, so
                    # izt is a counter to keep track of the plane to
                    # be read or skipped
                    izt = 0
                    
                    for iz in range(inpt['nodes0'][2] - 1):

                        # If the next two planes are not needed,
                        # skip the current plane if have not been
                        # read
                        if not Flags[iz] or not Flags[iz+1]:
                            try:
                                check = P1['done']
                                P1 = {}
                            except KeyError:
                                P0 = {}  # nothing to be done for
                                         # HDF5, but the
                                         # izt counter has to go up
                                izt = izt + 1
                            except :
                                raise
                            continue

                        # Get P0
                        try:
                            check = P0['done']
                        except KeyError:
                            P0 = self.get_plane_h5(f,izt)
                            izt = izt + 1
                        except:
                            raise

                        # Get P1
                        P1 = self.get_plane_h5(f,izt)
                        izt = izt + 1

                        zz0 = temp['z'][iz]
                        zz1 = temp['z'][iz+1]

                        for izp in range(izps-1,inpt['nodes'][2]):

                            izps = izp + 1
                            zz = inpt['z'][izp]

                            if zz > zz1:
                                break

                            if zz0 <= zz and zz1 >= zz:

                                # Interpolate in lower plane
                                if not P0['done']:
                                    for var in scal:
                                        if not PMD['read'][var]:
                                            continue
                                        r = interpolate.interp2d( \
                                             temp['y'],temp['x'], \
                                             P0[var])
                                        P0[var] = r(inpt['x'], \
                                                    inpt['y'])
                                    var = 3
                                    if PMD['read'][var]:
                                        for ii in range(9):
                                            r = interpolate. \
                                                 interp2d(temp['y'], \
                                                          temp['x'], \
                                                   P0[var][:,:,ii])
                                            P0[var][0:NY,0:NX,ii] = \
                                                       r(inpt['x'], \
                                                         inpt['y'])
                                        P0[var] = P0[var][0:NY,0:NX,:]
                                    P0['done'] = True

                                # Interpolate in upper plane
                                if not P1['done']:
                                    for var in scal:
                                        if not PMD['read'][var]:
                                            continue
                                        r = interpolate.interp2d( \
                                             temp['y'],temp['x'], \
                                             P1[var])
                                        P1[var] = r(inpt['x'], \
                                                    inpt['y'])
                                    var = 3
                                    if PMD['read'][var]:
                                        for ii in range(9):
                                            r = interpolate. \
                                                 interp2d(temp['y'], \
                                                          temp['x'], \
                                                   P1[var][:,:,ii])
                                            P1[var][0:NY,0:NX,ii] = \
                                                       r(inpt['x'], \
                                                         inpt['y'])
                                        P1[var] = P1[var][0:NY,0:NX,:]
                                    P1['done'] = True


                                # Interpolate in vertical
                                w1 = (zz1 - zz)/(zz1 - zz0)
                                w2 = 1 - w1
                                for var in scal:
                                    if not PMD['read'][var]:
                                        continue
                                    inpt[var][izp,:,:] = \
                                             w1*P0[var] + w2*P1[var]
                                var = 3
                                if PMD['read'][var]:
                                    for ii in range(9):
                                        inpt[var][izp,:,:,ii] = \
                                         w1*P0[var][:,:,ii] + \
                                         w2*P1[var][:,:,ii]

                        if iz < inpt['nodes0'][2] - 2:

                            # Shift planes
                            if Flags[iz+2]:
                                P0 = copy.deepcopy(P1)
                            else:
                                P0 = {}
                        
                        else:

                            P1 = {}
                            P0 = {}

                        
                # Do not interpolate
                else:

                    # Scalars
                    if PMD['read'][0]:
                        inpt[0] = np.asarray( \
                             f['Module/g_data']['Nh'],dtype="float32")
                    if PMD['read'][1]:
                        inpt[1] = np.asarray( \
                             f['Module/g_data']['Ne'],dtype="float32")
                    if PMD['read'][2]:
                        inpt[2] = np.asarray( \
                              f['Module/g_data']['T'],dtype="float32")


                    # Radiation field
                    if PMD['read'][3]:
                        inpt[3][:,:,:,0] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,0])
                        inpt[3][:,:,:,1] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,1])
                        inpt[3][:,:,:,2] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,2])
                        inpt[3][:,:,:,3] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,3])
                        inpt[3][:,:,:,4] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,4])
                        inpt[3][:,:,:,5] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,5])
                        inpt[3][:,:,:,6] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,6])
                        inpt[3][:,:,:,7] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,7])
                        inpt[3][:,:,:,8] = np.asarray( \
                                  f['Module/g_data']['jkq'][:,:,:,8])
                f.close()

                PMD['data'] = inpt
                return True

        except:

            raise
            return False
        
######################################################################
######################################################################

    def get_plane(self, f):
        ''' Load the data from a plane in the pmd file
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Initialize plane
        T = {}
        for ii in range(4):
            T[ii] = []
        T['done'] = False

        # Read plane
        for iy in range(PMD['head']['nodes0'][1]):
            for ix in range(PMD['head']['nodes0'][0]):
                bytes = f.read(4)
                if PMD['read'][0]:
                    T[0].append(struct.unpack( \
                                CONF['endian'] + 'f', bytes)[0])
                bytes = f.read(4)
                if PMD['read'][1]:
                    T[1].append(struct.unpack( \
                                CONF['endian'] + 'f', bytes)[0])
                bytes = f.read(4)
                if PMD['read'][2]:
                    T[2].append(struct.unpack( \
                                CONF['endian'] + 'f', bytes)[0])
                for ii in range(9):
                    bytes = f.read(8)
                    if PMD['read'][3]:
                        T[3].append(struct.unpack( \
                                    CONF['endian'] + 'd', bytes)[0])

        if PMD['read'][0]:
            T[0] = np.array(T[0],dtype="float32").reshape( \
                                          PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0])
        if PMD['read'][1]:
            T[1] = np.array(T[1],dtype="float32").reshape( \
                                          PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0])
        if PMD['read'][2]:
            T[2] = np.array(T[2],dtype="float32").reshape( \
                                          PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0])
        if PMD['read'][3]:
            T[3] = np.array(T[3]).reshape(PMD['head']['nodes0'][1], \
                                          PMD['head']['nodes0'][0], \
                                          9)

        self.pgc += 100./float(PMD['head']['nodes0'][2])
        pgc = int(self.pgc)
        if pgc > self.pgl:
            diff = pgc - self.pgl
            for ii in range(diff):
                if pgc <= 99:
                    self.pg.step()
                    self.pg.update_idletasks()
            self.pgl += diff

        return T


######################################################################
######################################################################

    def get_plane_h5(self, f,izt):
        ''' Load the data from plane izt in the pmd (HDF5) file
        '''

        global PMD
        PMD = source.general.PMD

        # Initialize plane
        T = {}
        for ii in range(4):
            T[ii] = []
        T['done'] = False

        if PMD['read'][0]:
            T[0] = np.asarray(f['Module/g_data']['Nh'][izt,:,:], \
                              dtype="float32")
        if PMD['read'][1]:
            T[1] = np.asarray(f['Module/g_data']['Ne'][izt,:,:], \
                              dtype="float32")
        if PMD['read'][2]:
            T[2] = np.asarray(f['Module/g_data']['T'][izt,:,:], \
                              dtype="float32")
        if PMD['read'][3]:
            T[3] = np.asarray(f['Module/g_data']['jkq'][izt,:,:,:])

        self.pgc += 100./float(PMD['head']['nodes0'][2])
        pgc = int(self.pgc)
        if pgc > self.pgl:
            diff = pgc - self.pgl
            for ii in range(diff):
                if pgc <= 99:
                    self.pg.step()
                    self.pg.update_idletasks()
            self.pgl += diff

        return T
    
######################################################################
######################################################################


    def interpol_data(self):
        ''' Interpolates the pmd data that is loaded
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Initialize progress bar
        self.pgc = 0
        self.pgl = 0

        # Initialize auxiliar dictionary
        inpt = {}

        # Change number of nodes
        inpt['nodes'] = [int(CONF['nodes']['x']), \
                         int(CONF['nodes']['y']), \
                         int(CONF['nodes']['z'])]

        # Check sizes
        for ii in range(len(inpt['nodes'])):
            if inpt['nodes'][ii] > PMD['data']['nodes0'][ii] or \
               inpt['nodes'][ii] <= 1:
                tkMessageBox.showwarning("Load", \
                                         "The dimension of the " + \
                                         "interpolated data " + \
                                         "cannot be larger than " + \
                                         "the original or " + \
                                         "smaller than 2.", \
                                         parent=self)
                return False

        # Original grid in Mm
        temp = {}
        temp['x'] = PMD['data']['x']
        temp['y'] = PMD['data']['y']
        temp['z'] = PMD['data']['z']

        # Final grid
        inpt['x'] = np.linspace(min(temp['x']), \
                                max(temp['x']), \
                                inpt['nodes'][0])
        inpt['y'] = np.linspace(min(temp['y']), \
                                max(temp['y']), \
                                inpt['nodes'][1])
        inpt['z'] = np.linspace(min(temp['z']), \
                                max(temp['z']), \
                                inpt['nodes'][2])

        # Check that axes are different
        if np.array_equal(temp['x'],inpt['x']) and \
           np.array_equal(temp['y'],inpt['y']) and \
           np.array_equal(temp['z'],inpt['z']):
            return

        # Data pointer
        data = PMD['data']
        NX,NY,NZ = inpt['nodes']

        # Save axis
        data['x'] = inpt['x']
        data['y'] = inpt['y']
        data['z'] = inpt['z']
        data['nodes'] = inpt['nodes']
        inpt = 0

        # Identify scalars and vectors
        scal = [0,1,2]

        #
        # Variables are loaded, different system to save memory
        #

        # Get a mesh of the final grid
        zz,yy,xx = np.meshgrid(data['z'],data['y'],data['x'], \
                               indexing='ij')


        # scalars
        for ii in scal:
            if PMD['read'][ii]:
                r = interpolate.RegularGridInterpolator( \
                        (temp['z'],temp['y'],temp['x']), data[ii])
                data[ii] = r((zz,yy,xx))
            # Update bar
            self.pgc += 100./float(len(PMD['read']))
            pgc = int(self.pgc)
            if pgc > self.pgl:
                diff = pgc - self.pgl
                for ii in range(diff):
                    if pgc <= 99:
                        self.pg.step()
                        self.pg.update_idletasks()
                self.pgl += diff

        # J
        ii = 3
        if PMD['read'][ii]:
            aux = data[ii]
            for jj in range(9):
                r = interpolate.RegularGridInterpolator( \
                        (temp['z'],temp['y'],temp['x']), \
                         aux[:,:,:,jj])
                data[ii][:,:,:,jj] = r((zz,yy,xx))
        # Update bar
        self.pgc += 900./float(len(PMD['read']))
        pgc = int(self.pgc)
        if pgc > self.pgl:
            diff = pgc - self.pgl
            for ii in range(diff):
                if pgc <= 99:
                    self.pg.step()
                    self.pg.update_idletasks()
            self.pgl += diff

        return True


######################################################################
######################################################################

    def skip_plane(self, f, skip):
        ''' Skip a plane
        '''

        bytes = f.seek(skip, 1)

        self.pgc += 100./float(PMD['head']['nodes0'][2])
        pgc = int(self.pgc)
        if pgc > self.pgl:
            diff = pgc - self.pgl
            for ii in range(diff):
                if pgc <= 99:
                    self.pg.step()
                    self.pg.update_idletasks()
            self.pgl += diff

        return {}

######################################################################
######################################################################
######################################################################
######################################################################
