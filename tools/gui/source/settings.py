# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# settings.py                                                        #
#                                                                    #
# Tanausú del Pino Alemán                                            #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# GUI for the settings of the GUI.                                   #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  10/03/2020 - V1.0.1 - Added colors to indicate focus (TdPA)       #
#                      - Added selection of active and passive       #
#                        colors (TdPA)                               #
#                                                                    #
#  22/03/2018 - V1.0.0 - First Version. (TdPA)                       #
#                                                                    #
######################################################################
######################################################################
######################################################################

import source.general
from source.loader import *
from source.tooltip import *

######################################################################
######################################################################

class SETTINGS_class(Toplevel):
    ''' Class that defines the main body of the settings window
    '''

######################################################################
######################################################################

    def __init__(self, parent, title = None):
        ''' Initializes the widget single tab
        '''

        global CONF
        CONF = source.general.CONF

        # Initialized a support window.
        Toplevel.__init__(self, parent)
        self.transient(parent)

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Set the title if any
        if title:
            self.title(title)

        # Active border
        self.configure(background=CONF['col_act'])

        self.parent = parent
        self.result = None
        self.plt_action = None

        head = Frame(self)
        body = Frame(head)
        self.up = Frame(body)
        self.do = Frame(body)
        self.head = head
        self.box = body
        self.initial_focus = self.body(body)
        head.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        body.pack(fill=BOTH,expand=1)

        self.grab_set()
        self.lift()
        
        if not self.initial_focus:
            self.initial_focus = self

        # Configuration file manager
        self.optionbox(self.up)

        # Configuration buttons
        self.buttons(self.do)

        self.up.pack(fill=BOTH,expand=1,side=TOP)
        self.do.pack(fill=BOTH,expand=1,side=BOTTOM)
        body.pack(fill=BOTH,expand=1)
        head.pack(fill=BOTH,expand=1)

        self.protocol("WM_DELETE_WINDOW", self.close)
        self.bind("<Escape>", self.close)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)


######################################################################
######################################################################

    def body(self, master):
        ''' Creates the dialog body and set initial focus.
        '''

        return master

######################################################################
######################################################################

    def close(self, event=None):
        ''' Method that handles the window closing.
        '''

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

######################################################################
######################################################################
######################################################################
######################################################################

    def optionbox(self, box):
        ''' Defines all the control widgets of PSP_class class.
        '''

        global CONF
        CONF = source.general.CONF
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS


        # Widget padding
        padx = 5
        pady = padx
        ipadx = 3
        ipady = ipadx

        # Font Size
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l0 = Label(box0, text="GUI Font Size Selection", \
                     justify=CENTER, \
                     font=self.cFont, anchor=CENTER)
        b0l0.grid(row=row,column=col,columnspan=5,sticky=NSEW)
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l1 = Label(box0, text="Font Size:",justify=RIGHT, \
                     font=self.cFont, anchor=E)
        b0l1.grid(row=row,column=col,sticky=NSEW)
        col += 1
        # Number display
        Grid.columnconfigure(box0,col,weight=1)
        self.font0_val = IntVar()
        self.font0_val.set(CONF['wfont_size'])
        self.font0_label = Label(box0, \
                            text="{0}".format(CONF['wfont_size']), \
                            font=self.cFont)
        createToolTip(self.font0_label,TOOLTIPS['37'])
        self.font0_label.grid(row=row,column=col,sticky=NSEW)
        # Minus button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.font0_bmin = Button(box0, text="-", font=self.cFont, \
                                 command=self.min0)
        createToolTip(self.font0_bmin,TOOLTIPS['-'])
        self.font0_bmin.grid(row=row,column=col,sticky=NSEW)
        # Plus button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.font0_bplu = Button(box0, text="+", font=self.cFont, \
                                 command=self.plu0)
        createToolTip(self.font0_bplu,TOOLTIPS['+'])
        self.font0_bplu.grid(row=row,column=col,sticky=NSEW)
        # Pack box0
        box0.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)


        # Active/passive colors
        spaces = 25
        bw = 2
        box1 = LabelFrame(box)
        row = 0
        col = 0
        # Title
        Grid.rowconfigure(box1,row,weight=1)
        Grid.columnconfigure(box1,col,weight=1)
        b1l0 = Label(box1, text="GUI Border Color Selection", \
                     justify=CENTER, \
                     font=self.cFont, anchor=CENTER)
        b1l0.grid(row=row,column=col,columnspan=4,sticky=NSEW)
        # Active
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box1,row,weight=1)
        Grid.columnconfigure(box1,col,weight=1)
        b1l0 = Label(box1, text="Active color:  ",justify=RIGHT, \
                     font=self.cFont, anchor=E)
        b1l0.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.cact_name = StringVar()
        self.cact_name.set(CONF['col_act'])
        self.cact_name.trace('w',self.cact_val)
        self.cact_entry = Entry(box1, font=self.cFont, \
                                justify=CENTER, \
                                textvariable=self.cact_name)
        self.cact_entry.grid(row=row,column=col,sticky=NSEW)
        # Color (label)
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.cact_lab = Label(box1, text=" "*spaces, \
                              borderwidth=bw, relief='solid', \
                              background=CONF['col_act'], anchor=E)
        self.cact_lab.grid(row=row,column=col, \
                           columnspan=2,sticky=NSEW)
        # Passive
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box1,row,weight=1)
        Grid.columnconfigure(box1,col,weight=1)
        b1l1 = Label(box1, text="Passive color:  ",justify=RIGHT, \
                     font=self.cFont, anchor=E)
        b1l1.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.cpas_name = StringVar()
        self.cpas_name.set(CONF['col_pas'])
        self.cpas_name.trace('w',self.cpas_val)
        self.cpas_entry = Entry(box1, font=self.cFont, \
                                justify=CENTER, \
                                textvariable=self.cpas_name)
        self.cpas_entry.grid(row=row,column=col,sticky=NSEW)
        # Color (label)
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.cpas_lab = Label(box1, text=" "*spaces, \
                              borderwidth=bw, relief='solid', \
                              background=CONF['col_pas'], anchor=E)
        self.cpas_lab.grid(row=row,column=col, \
                           columnspan=2,sticky=NSEW)
        # Border thickness
        # Label
        row += 1
        col = 0
        Grid.rowconfigure(box1,row,weight=1)
        Grid.columnconfigure(box1,col,weight=1)
        b1l2 = Label(box1, text="Border thickness:",justify=RIGHT, \
                     font=self.cFont, anchor=E)
        b1l2.grid(row=row,column=col,sticky=NSEW)
        # Number display
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.thic1_val = IntVar()
        self.thic1_val.set(CONF['b_pad'])
        self.thic1_label = Label(box1, \
                            text="{0}".format(CONF['b_pad']), \
                            font=self.cFont)
        self.thic1_label.grid(row=row,column=col,sticky=NSEW)
        # Minus button
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.thic1_bmin = Button(box1, text="-", font=self.cFont, \
                                 command=self.min1)
        self.thic1_bmin.grid(row=row,column=col,sticky=NSEW)
        # Plus button
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.thic1_bplu = Button(box1, text="+", font=self.cFont, \
                                 command=self.plu1)
        createToolTip(self.thic1_bplu,TOOLTIPS['+'])
        self.thic1_bplu.grid(row=row,column=col,sticky=NSEW)
        # Pack box1
        box1.pack(fill=BOTH, expand=1, side = TOP, \
                  padx=padx, pady=pady, ipadx=ipadx, ipady=ipady)

######################################################################
######################################################################

    def buttons(self, box):
        ''' Defines the control buttons of the widget
        '''

        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Data load (box0)
        box0 = LabelFrame(box)
        col = 0
        row = 0
        # Close button
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_close = Button(box0, text="Close", \
                                  font=self.cFont, command=self.close)
        createToolTip(self.butt1_close,TOOLTIPS['11'])
        self.butt1_close.grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)


######################################################################
######################################################################
######################################################################
######################################################################

    def min0(self):
        ''' Reduce in 1 the font size
        '''

        global CONF
        CONF = source.general.CONF

        if CONF['wfont_size'] <= 1:
            return
        CONF['wfont_size'] -= 1
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])
        self.redraw()

        return

######################################################################
######################################################################

    def plu0(self):
        ''' Increse in 1 the number of plots
        '''

        global CONF
        CONF = source.general.CONF

        CONF['wfont_size'] += 1
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])
        self.redraw()

        return

######################################################################
######################################################################

    def min1(self):
        ''' Reduce in 1 the border size
        '''

        global CONF
        CONF = source.general.CONF

        if CONF['b_pad'] <= 1:
            return
        CONF['b_pad'] -= 1
        self.thic1_label.config(text='{0}'.format(CONF['b_pad']))
        self.redraw()

        return

######################################################################
######################################################################

    def plu1(self):
        ''' Increse in 1 the boder size
        '''

        global CONF
        CONF = source.general.CONF

        CONF['b_pad'] += 1
        self.thic1_label.config(text='{0}'.format(CONF['b_pad']))
        self.redraw()

        return

######################################################################
######################################################################

    def cact_val(self, *dumm):
        ''' Validates cact (active color) entry
        '''

        global CONF
        CONF = source.general.CONF

        ccol = CONF['col_act']

        val = self.cact_name.get()
        try:
            b = Frame(self)
            l0 = Label(b, background=val)
            CONF['col_act'] = val
            self.cact_lab.config(background=val)
            self.configure(background=val)
        except TclError:
            pass
        except:
            raise

######################################################################
######################################################################

    def cpas_val(self, *dumm):
        ''' Validates cpas (passive color) entry
        '''

        global CONF
        CONF = source.general.CONF

        ccol = CONF['col_pas']

        val = self.cpas_name.get()
        try:
            b = Frame(self)
            l0 = Label(b, background=val)
            CONF['col_pas'] = val
            self.cpas_lab.config(background=val)
            self.parent.configure(background=val)
        except TclError:
            pass
        except:
            raise

######################################################################
######################################################################
######################################################################
######################################################################

    def redraw(self):
        ''' Redraws the whole window
        '''

        global CONF
        CONF = source.general.CONF

        self.box.destroy()
        self.head.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        self.box = Frame(self.head)
        self.up = Frame(self.box)
        self.do = Frame(self.box)
        self.optionbox(self.up)
        self.buttons(self.do)
        self.up.pack(fill=BOTH,expand=1,side=TOP)
        self.do.pack(fill=BOTH,expand=1,side=BOTTOM)
        self.box.pack(fill=BOTH,expand=1)

######################################################################
######################################################################

    def set_top(self):
        ''' Updates the top part of the widget window
        '''

        global CONF
        CONF = source.general.CONF

        # Font size
        self.font0_val.set(CONF['wfont_size'])
        self.font0_label.configure( \
                                text="{0}".format(CONF['wfont_size']))

######################################################################
######################################################################
######################################################################
######################################################################
