/*
 * various useful functions for debugging an visual output
 */

/*********************************************************

PSP (Porta SPectrum file) format (v1):
---------------------------------
HEADER:
-------
char[3]:     "psp":                   magic string
int[1]:       psp version
char[1023]:   comment
double[2]:    theta, chi:             inclination and azimuth of the
                                       radiation propagation (in
                                       degrees [0-180, 0-360])
int[1]:       NWL:                    number of wavelengths
double[NWL]:                          array of discrete wavelengths
                                       (DECREASING order), in Angstroms
int[1]:       surface:                characterizes the surface (0-5)
                                       of the radiation emergence
double[1]:    DX                      domain X-size in cm
double[1]:    DY                      domain Y-size in cm
char[2]:                               periodic in x and y?
int[2]:       N1, N2:                 number of nodes per axis
double[N1]:   coordinates in #1 axis: coordinates in #1 axis in
                                       centimeters
double[N2]:   coordinates in #2 axis: coordinates in #2 axis in
                                       centimeters

SPECTRAL DATA:
--------------
float[NWL*N2*N1*4]:  Stokes parameters ordered as indicated (slowest loop:
                      wavelengths, fastest loop: Stokes parameter index)

**********************************************************

TAU (optical depth) file format (v1):

HEADER:
char[3]       "tau"      //magic string
int[1]:       tau version
char[1023]:   comment
double[2]:    theta, chi:             inclination and azimuth of the radiation
                                       propagation (in degrees [0-180, 0-360])
int[1]:       NWL:                    number of wavelengths
double[NWL]:                          array of discrete wavelengths (DECREASING
                                       order), in Angstroms
double[1]:    DX                      domain X-size in cm
double[1]:    DY                      domain Y-size in cm
char[2]:                              periodic in x and y?
int[2]:       N1, N2:                 number of nodes per axis
double[N1]:   coordinates in #1 axis: coordinates in #1 axis in centimeters
double[N2]:   coordinates in #2 axis: coordinates in #2 axis in centimeters

Coordinates:
[NWVL][NY][NX] array of [X,Y,Z] triplet of doubles containing coordinates of the
point [IY][IX] with given tau (in cm): float[NWVL*NY*NX*3] =
tau[iwvl*NY*NX*3+iy*NX*3+ix*3+n]   where n=0,1,2 (X,Y,Z)

**********************************************************/

#ifndef TOOLS_H_
#define TOOLS_H_

#include "def.h"

/* produce an emergent radiation at the iz=grid->nz-1 plane in a general
 * direction and a range of frequencies
 *
 * called by master
 *
 * data stored in PSP file format
 *
 * theta_los, chi_los: ray direction (in degrees)
 *
 * ifreq_min, ifreq_max: interval of frequencies to treat fname: filename of the
 * output PSP file
 *
 * return value: nonzero on success, 0 otherwise
 */
extern int too_SurfaceMap(double theta_los, double chi_los, int ifreq_min,
                          int ifreq_max, char fname[], char comment[]);

/* HDF5 version */
extern int too_SurfaceMap_h5(double theta_los, double chi_los, int ifreq_min,
                             int ifreq_max, char fname[], char comment[]);

/* Pointer function to either: too_SurfaceMap or too_SurfaceMap_h5 */
extern int (*too_SurfaceMap_fp)(double, double, int, int, char[], char[]);

/* spatially averaged CLV for a given azimuth and a frequency (called by master,
 * returns non-zero on success) */
extern int too_CLVFreq(double chi, double mu_min, double mu_max, int N_ray,
                       int id_fr, char fname[]);

/* spatially and azimuth averaged CLV for a frequency (called by master, returns
 * non-zero on success) */
extern int too_CLVFreqAV(double mu_min, double mu_max, int N_ray, int N_azim,
                         int id_fr, char fname[]);

/* spatially and azimuthally averaged Stokes signal (GRID POINTS MUST BE
 * EQUIDISTANT IN THE HORIZONTAL PLANE) */
extern int too_FSAZAV(double theta_los, int N_azim, int ifreq_min,
                      int ifreq_max, char fname[]);

/* produces a TAU-file containing coordinates of point with a given optical
 * depth along the line of sight for each wavelength starting in all surface
 * points
 *
 * called by master
 *
 * data stored in TAU file format
 *
 * tau: the desired optical depth
 *
 * theta_los, chi_los: ray direction (in degrees)
 *
 * ifreq_min, ifreq_max: interval of frequencies to treat
 *
 * fname: filename of the output TAU file
 */
extern int too_SurfaceTau(double tau, double theta_los, double chi_los,
                          int ifreq_min, int ifreq_max, char fname[],
                          char comment[]);

/* HDF5 version */
extern int too_SurfaceTau_h5(double tau, double theta_los, double chi_los,
                             int ifreq_min, int ifreq_max, char fname[],
                             char comment[]);

/* Pointer function to either: too_SurfaceTau or too_SurfaceTau_h5 */
extern int (*too_SurfaceTau_fp)(double tau, double theta_los, double chi_los,
                                int ifreq_min, int ifreq_max, char fname[],
                                char comment[]);

#endif /* TOOLS_H_ */
