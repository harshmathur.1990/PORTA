/* definitions of Stokes modes (included by def.h) */

#ifndef DEF_SMODE_H_
#define DEF_SMODE_H_

/* Stokes profiles definitions, needed only in function too_SurfaceMap_h5
   (tools.c) in order to easily mimic the I/O operation in too_SurfaceMap */
#define T_NS 4
#define T_SI 0
#define T_SQ 1
#define T_SU 2
#define T_SV 3

#if defined(SMODE_FULL)

/* Stokes vector components */
#define NSTOKES  4
#define STOKES_I 0
#define STOKES_Q 1
#define STOKES_U 2
#define STOKES_V 3
/* Propagation matrix elements */
#define NKMTX 7
#define ETA_I 0
#define ETA_Q 1
#define ETA_U 2
#define ETA_V 3
#define RHO_Q 4
#define RHO_U 5
#define RHO_V 6

#elif defined(SMODE_I)

/* Stokes vector components */
#define NSTOKES  1
#define STOKES_I 0

/* Propagation matrix elements */
#define NKMTX    1
#define ETA_I    0

#elif defined(SMODE_IQU_NOSA)

/* Stokes vector components */
#define NSTOKES  3
#define STOKES_I 0
#define STOKES_Q 1
#define STOKES_U 2
/* Propagation matrix elements */
#define NKMTX    1
#define ETA_I    0

#elif defined(SMODE_IQU_SA)

/* Stokes vector components */
#define NSTOKES  3
#define STOKES_I 0
#define STOKES_Q 1
#define STOKES_U 2
/* Propagation matrix elements */
#define NKMTX    3
#define ETA_I    0
#define ETA_Q    1
#define ETA_U    2

#elif defined(SMODE_IV)

/* Stokes vector components */
#define NSTOKES  2
#define STOKES_I 0
#define STOKES_V 1
/* Propagation matrix elements */
#define NKMTX    3
#define ETA_I    0
#define ETA_V    1
#define RHO_V    2

#else

#error "undefined Stokes mode"

#endif

#endif /* DEF_SMODE_H_ */
