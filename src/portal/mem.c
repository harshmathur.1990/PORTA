#include "mem.h"
#include "def.h"
#include "error.h"
#include <memory.h>

void *Malloc(size_t size)
{
    void *p;
    if (!(p = (void *)malloc(size))) {
        Error(E_ERROR, POR_AT,
              "malloc failed when trying to "
              "allocate %d bytes",
              size);
    }
    return p;
}

void *Realloc(void *ptr, size_t size)
{
    void *p;
    if (!(p = (void *)realloc(ptr, size))) {
        Error(E_ERROR, POR_AT,
              "realloc failed when trying to "
              "re-allocate %d bytes",
              size);
    }
    return p;
}

void *Memcpy(void *dst, const void *src, size_t size)
{
    if (dst && src && size > 0) {
        return memcpy(dst, src, size);
    } else if (!dst) {
        Error(E_ERROR, POR_AT, "dst==NULL");
        return NULL;
    } else if (!src) {
        Error(E_ERROR, POR_AT, "src==NULL");
        return NULL;
    } else /* if (size<=0) */
    {
        Error(E_ERROR, POR_AT, "invalid size: %d", (int)size);
        return NULL;
    }
}
