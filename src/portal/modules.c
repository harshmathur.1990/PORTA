/* this is the only non-portable file of the library */

#include "modules.h"
#include "def.h"
#include "global.h"
#include "process.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAMELEN    2047
#define INIT_STRING    "InitModule"    /* module initialization function */
#define INIT_STRING_H5 "InitModule_h5" /* module initialization function */
#define CLOSE_STRING   "CloseModule"   /* module final function */
#define CLOSE_STRING_H5                                                        \
    "CloseModule" /* module final function.                                    \
                     In case it is needed in the future, so far                \
                     the same as CLOSE_STRING for binary PMD files. */

static char mod_prefix[MAX_NAMELEN];
static char
    mod_suffix[MAX_NAMELEN]; /* platform dependent module file name suffix */

extern t_global_vars *g_global;

static char  module_name[MAX_NAMELEN] = "";
static char  module_path[MAX_NAMELEN] = "";
static void *module_handle            = NULL;

/* http://www.trilithium.com/johan/2004/12/problem-with-dlsym/ for discussion of
 * object -> function casts */
typedef int (*t_init_function)(t_afunc *, FILE *, int *);
typedef int (*t_init_function_h5)(t_afunc *, hid_t, int *);
typedef int (*t_close_function)(void);
static t_init_function ModuleInitFunction =
    NULL; /* module initialization function */
static t_init_function_h5 ModuleInitFunction_h5 =
    NULL; /* module initialization function */
static t_close_function ModuleCloseFunction = NULL; /* release the module */

int mod_IsModuleLoaded(void)
{
    if (!module_handle || !PorIsAFuncSet()) {
        return 0;
    } else {
        return 1;
    }
}

char *mod_GetModuleName(void)
{
    char *name;
    if (!mod_IsModuleLoaded()) {
        Error(E_ERROR, POR_AT, "no module loaded");
        return NULL;
    }
    name = (char *)Malloc(strlen(module_name) + 1);
    Memcpy(name, module_name, strlen(module_name) + 1);
    return name;
}

#if defined(_WIN32) || defined(WIN32) || defined(__CYGWIN__) ||                \
    defined(__MINGW32__) || defined(__BORLANDC__)

/* directory containing loadable modules (contains the "lib" file name prefix)
 */
#define DIR_MODULES "..\\..\\samo\\Debug\\lib"

#error "Windows libraries not yet implemented!"

int mod_LoadModule(const char *m_name)
{
    sprintf(&mod_prefix, DIR_MODULES);
    sprintf(&mod_suffix, ".dll");
}

int mod_UnlinkModule(void) {}

#elif defined(__linux__)

#include <dlfcn.h>

/* directory containing loadable modules (contains the "lib" file name prefix)
 */
#define DIR_MODULES "lib"

int mod_LoadModule(const char *m_name, FILE *file, int *node_pmd_size)
{
    stk_Add(IAM_MASTER, POR_AT, "loading the module %s", m_name);
    sprintf(mod_prefix, DIR_MODULES);
    sprintf(mod_suffix, ".so");
    if (strlen(m_name) + strlen(mod_prefix) + strlen(mod_suffix) >
        MAX_NAMELEN) {
        Error(E_ERROR, POR_AT, "module name too long (%s)", m_name);
        return 0;
    }
    if (module_handle) {
        mod_UnlinkModule();
    }
    strcpy(module_name, m_name);
    sprintf(module_path, "%s%s%s", mod_prefix, module_name, mod_suffix);
    module_handle = dlopen(module_path, RTLD_NOW);
    if (!module_handle) {
        char *err = dlerror();
        Error(E_ERROR, POR_AT, "cannot load the module %s (%s)", module_name,
              err);
        return 0;
    }
    if (IAM_MASTER)
        PrintfErr("%c%c Loaded: %s\n", COMMENT_CHAR, COMMENT_CHAR, module_path);

    /* ISO C forbids coversion of void* to function pointer: use a dirty cast
     * through long (may fail in some rare cases) */
    if (sizeof(long) != sizeof(void *)) {
        Error(E_ERROR, POR_AT,
              "sorry, cannot convert dlsym's void* to function pointer on your "
              "platform");
        return 0;
    }
    ModuleInitFunction =
        (t_init_function)(long)dlsym(module_handle, INIT_STRING);
    if (!ModuleInitFunction) {
        char *err = dlerror();
        Error(E_ERROR, POR_AT, "cannot find the %s function in %s (%s)",
              INIT_STRING, module_name, err);
        return 0;
    }
    ModuleCloseFunction =
        (t_close_function)(long)dlsym(module_handle, CLOSE_STRING);
    if (!ModuleCloseFunction) {
        char *err = dlerror();
        Error(E_ERROR, POR_AT, "cannot find the %s function in %s (%s)",
              CLOSE_STRING, module_name, err);
        return 0;
    }
    return ModuleInitFunction(&g_global->afunc, file, node_pmd_size);
}

/*****************************************************************************/
/**
 * @brief Load atomic module
 *
 * Routine called from io_LoadGrid_h5(). It tries to load the dynamically linked
 * library, and then calls the module initialization function via
 * ModuleInitFunction_h5(), which will be responsible for loading the module
 * header data and initializing the p_afunc structure to the functionality
 * routines provided by the module.
 *
 * @return 0 if errors found in this routine ; otherwise return the return value
 * of the module initialization function
 *****************************************************************************/
int mod_LoadModule_h5(const char *m_name, hid_t file_id, int *node_pmd_size)
{
    stk_Add(IAM_MASTER, POR_AT, "loading the module %s", m_name);
    sprintf(mod_prefix, DIR_MODULES);
    sprintf(mod_suffix, ".so");

    if (strlen(m_name) + strlen(mod_prefix) + strlen(mod_suffix) >
        MAX_NAMELEN) {
        Error(E_ERROR, POR_AT, "module name too long (%s)", m_name);
        return 0;
    }

    if (module_handle) {
        mod_UnlinkModule();
    }

    strcpy(module_name, m_name);
    sprintf(module_path, "%s%s%s", mod_prefix, module_name, mod_suffix);
    module_handle = dlopen(module_path, RTLD_NOW);

    if (!module_handle) {
        char *err = dlerror();
        Error(E_ERROR, POR_AT, "cannot load the module %s (%s)", module_name,
              err);
        return 0;
    }

    if (IAM_MASTER)
        PrintfErr("%c%c Loaded: %s\n", COMMENT_CHAR, COMMENT_CHAR, module_path);

    /* ISO C forbids coversion of void* to function pointer: use a dirty cast
     * through long (may fail in some rare cases) */
    if (sizeof(long) != sizeof(void *)) {
        Error(E_ERROR, POR_AT,
              "sorry, cannot convert dlsym's void* to function pointer on your "
              "platform");
        return 0;
    }

    ModuleInitFunction_h5 =
        (t_init_function_h5)(long)dlsym(module_handle, INIT_STRING_H5);

    if (!ModuleInitFunction_h5) {
        char *err = dlerror();
        Error(E_ERROR, POR_AT, "cannot find the %s function in %s (%s)",
              INIT_STRING_H5, module_name, err);
        return 0;
    }

    ModuleCloseFunction =
        (t_close_function)(long)dlsym(module_handle, CLOSE_STRING_H5);
    if (!ModuleCloseFunction) {
        char *err = dlerror();
        Error(E_ERROR, POR_AT, "cannot find the %s function in %s (%s)",
              CLOSE_STRING_H5, module_name, err);
        return 0;
    }

    return ModuleInitFunction_h5(&g_global->afunc, file_id, node_pmd_size);
}

int mod_UnlinkModule(void)
{
    int r = 1;
    if (!module_handle) {
        Error(E_ERROR, POR_AT, "no module is loaded");
        return 0;
    }
    stk_Add(IAM_MASTER, POR_AT, "unlinking the module %s", module_name);
    if (!ModuleCloseFunction()) {
        Error(E_ERROR, POR_AT, "the %s function of %s has failed", CLOSE_STRING,
              module_name);
        r = 0;
    }
    if (dlclose(module_handle)) {
        char *err = dlerror();
        Error(E_ERROR, POR_AT, "cannot unlink the module %s (%s)", module_name,
              err);
        r = 0;
    }
    module_handle       = NULL;
    module_name[0]      = 0;
    ModuleInitFunction  = NULL;
    ModuleCloseFunction = NULL;

    PorResetAFunc();

    // Release the memory for the 6D memoization jagged array
    mat_free_strip6D();

    return r;
}

#elif defined(__APPLE_CPP__) || defined(__APPLE_CC__) ||                       \
    defined(__MACOS_CLASSIC__)

#include <dlfcn.h>

/* directory containing loadable modules (contains the "lib" file name prefix)
 */
/*#define DIR_MODULES    "../../CaII5L/Debug/lib"*/
#define DIR_MODULES "./lib"

int mod_LoadModule(const char *m_name, FILE *file, int *node_pmd_size)
{
    stk_Add(IAM_MASTER, POR_AT, "loading the module %s", m_name);
    sprintf(mod_prefix, DIR_MODULES);
    sprintf(mod_suffix, ".dylib");
    if (strlen(m_name) + strlen(mod_prefix) + strlen(mod_suffix) >
        MAX_NAMELEN) {
        Error(E_ERROR, POR_AT, "module name too long (%s)", m_name);
        return 0;
    }
    if (module_handle) {
        mod_UnlinkModule();
    }
    strcpy(module_name, m_name);
    sprintf(module_path, "%s%s%s", mod_prefix, module_name, mod_suffix);
    module_handle = dlopen(module_path, RTLD_NOW);
    if (!module_handle) {
        char *err = dlerror();
        Error(E_ERROR, POR_AT, "cannot load the module %s (%s)", module_name,
              err);
        return 0;
    }
    if (IAM_MASTER)
        PrintfErr("%c%c Loaded: %s\n", COMMENT_CHAR, COMMENT_CHAR, module_path);

    /* ISO C forbids coversion of void* to function pointer: use a dirty cast
     * through long (may not work on some rare occasions) */
    if (sizeof(long) != sizeof(void *)) {
        Error(E_ERROR, POR_AT,
              "sorry, cannot convert dlsym's void* to function pointer on your "
              "platform");
    }
    ModuleInitFunction =
        (t_init_function)(long)dlsym(module_handle, INIT_STRING);
    if (!ModuleInitFunction) {
        char *err = dlerror();
        Error(E_ERROR, POR_AT, "cannot find the %s function in %s (%s)",
              INIT_STRING, module_name, err);
        return 0;
    }
    ModuleCloseFunction =
        (t_close_function)(long)dlsym(module_handle, CLOSE_STRING);
    if (!ModuleCloseFunction) {
        char *err = dlerror();
        Error(E_ERROR, POR_AT, "cannot find the %s function in %s (%s)",
              CLOSE_STRING, module_name, err);
        return 0;
    }
    return ModuleInitFunction(&g_global->afunc, file, node_pmd_size);
}

int mod_UnlinkModule(void)
{
    int r = 1;
    if (!module_handle) {
        Error(E_ERROR, POR_AT, "no module is loaded");
        return 0;
    }
    stk_Add(IAM_MASTER, POR_AT, "unlinking the module %s", module_name);
    if (!ModuleCloseFunction()) {
        Error(E_ERROR, POR_AT, "the %s function of %s has failed", CLOSE_STRING,
              module_name);
        r = 0;
    }
    if (dlclose(module_handle)) {
        char *err = dlerror();
        Error(E_ERROR, POR_AT, "cannot unload the module %s (%s)", module_name,
              err);
        r = 0;
    }
    module_handle       = NULL;
    module_name[0]      = 0;
    ModuleInitFunction  = NULL;
    ModuleCloseFunction = NULL;

    PorResetAFunc();

    // Release the memory for the 6D memoization jagged array
    mat_free_strip6D();

    return r;
}

#else

#error "unknown platform"

#endif
