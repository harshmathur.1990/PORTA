/* angular quadratures */

#ifndef DIRECTIONS_H_
#define DIRECTIONS_H_

#include "def.h"

/* calculates T^K_Q tensorial elements
 * st=sin(inclination), ct=cos(inclination), ca=cos(azimuth), sa=sin(azimuth)
 */
extern void dir_CalcTKQ(double st, double ct, double sa, double ca,
                        double tkq[NTKQ]);

/* extract the complex element T^K_Q(istokes) from the complete array of
 * geometrical tensors; for unphysical indices (such as K<0 or Q>K, etc,)
 * returns zero WARNING: stokes parameters are always indexed as I==0, Q==1,
 * U==2, V==3, not by STOKES_I etc. */
extern t_complex dir_GetTKQElement(int istokes, int K, int Q,
                                   const double tkq[NTKQ]);

/* calculates directions and quadratures
 *
 * input:
 * n_incl_oct number of inclination directions per octant
 * n_azim_oct number of azimuthal directions per octant
 *
 * output:
 * dirs pointer to t_directions structure to be calculated
 */
extern void dir_MakeDirections(int n_incl_oct, int n_azim_oct,
                               t_directions *dirs);

/* calculates directions and quadratures for several
 * sets of quadratures (e.g. published in Stepan etal 2020)
 *
 * input:
 * set index of the quadrature set
 *
 * output:
 * dirs pointer to t_directions structure to be calculated
 */
extern void dir_MakeDirectionsQuadSet(int set, t_directions *dirs);

/*
 * returns rotation matrix D^K_Q'Q(alpha,beta,gamma) for given Euler angles
 * dimensions of the matrix are (2*k+1)*(2*k+1)
 */
extern t_complex **dir_GetDKRot(int k, double alpha, double beta, double gamma);

/* calculates the vector "v" coordinates in the reference frame defined by the
 * vector "B" B-frame: created by rotation of the original frame by the Euler
 * angles alpha (azimuth of B in the old frame) and beta (inclination of B in
 * the old frame) around the axis of nodes.
 */
extern t_vec_3d dir_Vector2BFrame(const t_vec_3d v, const t_vec_3d B);

/*
 * Matrix of the form [[cos 2a, sin 2a], [-sin 2a, cos 2a]] giving
 * a transformation of the Stokes (Q,U) from the magnetic field reference frame
 * defined by the into the laboratory frame:
 *
 * [Q,U]_global = A . [Q,U]_local
 *
 * where
 *      cos 2a , sin 2a
 * A = -sin 2a , cos 2a
 *
 * dir: unit vector in the direction of ray propagation
 * B: local magnetic field vector
 */
void dir_StokesRotMatrix(const t_vec_3d dir, const t_vec_3d B, double *cos2a,
                         double *sin2a);

#endif /* DIRECTIONS_H_ */
