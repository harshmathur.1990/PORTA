#include "directions.h"
#include "def.h"
#include "matika.h"
#include "mem.h"
#include "stack.h"
#include "topology.h"
#include <stdio.h>
#include <stdlib.h>

void dir_CalcTKQ(double st, double ct, double sa, double ca, double tkq[NTKQ])
{
    double c2a = ca * ca - sa * sa, s2a = 2.0 * sa * ca, x;
    /* T00(I).re */
    tkq[T00I_RE] = 1.0;
    /* T20(I).re */
    tkq[T20I_RE] = 1.060660171779821287 * ct * ct - 3.535533905932737622e-1;
    /* T22(I).re & T22(I).im */
    x            = 4.330127018922193234e-1 * st * st;
    tkq[T22I_RE] = x * c2a;
    tkq[T22I_IM] = x * s2a;
    /* T20(Q).re */
    tkq[T20Q_RE] = -1.060660171779821287 * st * st;
    /* T21(Q).re & T21(Q).im */
    x            = -8.660254037844386468e-1 * ct * st;
    tkq[T21Q_RE] = x * ca;
    tkq[T21Q_IM] = x * sa;
    /* T22(Q).re & T22(Q).im */
    x            = -4.330127018922193234e-1 * (1.0 + ct * ct);
    tkq[T22Q_RE] = x * c2a;
    tkq[T22Q_IM] = x * s2a;
    /* T21(U).re, T21(U).im */
    x            = 8.660254037844386468e-1 * st;
    tkq[T21U_RE] = x * sa;
    tkq[T21U_IM] = -x * ca;
    /* T22(U).re, T22(U).im */
    x            = 8.660254037844386468e-1 * ct;
    tkq[T22U_RE] = x * s2a;
    tkq[T22U_IM] = -x * c2a;
    /* T10(V).re */
    tkq[T10V_RE] = 1.224744871391589049 * ct;
    /* T11(V).re & T11(V).im */
    x            = -8.660254037844386468e-1 * st;
    tkq[T11V_RE] = x * ca;
    tkq[T11V_IM] = x * sa;
}

t_complex dir_GetTKQElement(int istokes, int K, int Q, const double tkq[NTKQ])
{
    static const int re[4][3][3] = {/* I */
                                    {/* K = 0 */ {T00I_RE, -1, -1},
                                     /* K = 1 */ {-1, -1, -1},
                                     /* K = 2 */ {T20I_RE, T21I_RE, T22I_RE}},
                                    /* Q */
                                    {/* K = 0 */ {-1, -1, -1},
                                     /* K = 1 */ {-1, -1, -1},
                                     /* K = 2 */ {T20Q_RE, T21Q_RE, T22Q_RE}},
                                    /* U */
                                    {/* K = 0 */ {-1, -1, -1},
                                     /* K = 1 */ {-1, -1, -1},
                                     /* K = 2 */ {-1, T21U_RE, T22U_RE}},
                                    /* V */
                                    {/* K = 0 */ {-1, -1, -1},
                                     /* K = 1 */ {T10V_RE, T11V_RE, -1},
                                     /* K = 2 */ {-1, -1, -1}}};
    static const int im[4][3][3] = {/* I */
                                    {/* K = 0 */ {-1, -1, -1},
                                     /* K = 1 */ {-1, -1, -1},
                                     /* K = 2 */ {-1, T21I_IM, T22I_IM}},
                                    /* Q */
                                    {/* K = 0 */ {-1, -1, -1},
                                     /* K = 1 */ {-1, -1, -1},
                                     /* K = 2 */ {-1, T21Q_IM, T22Q_IM}},
                                    /* U */
                                    {/* K = 0 */ {-1, -1, -1},
                                     /* K = 1 */ {-1, -1, -1},
                                     /* K = 2 */ {-1, T21U_IM, T22U_IM}},
                                    /* V */
                                    {/* K = 0 */ {-1, -1, -1},
                                     /* K = 1 */ {-1, T11V_IM, -1},
                                     /* K = 2 */ {-1, -1, -1}}};
    t_complex t = {0.0};
    int              ire, iim;
    if (K < 0 || K > 2 || Q < -K || Q > K || istokes < 0 || istokes > 3)
        return t;
    ire  = re[istokes][K][abs(Q)];
    iim  = im[istokes][K][abs(Q)];
    t.re = (ire >= 0 ? tkq[ire] : 0.0);
    t.im = (iim >= 0 ? tkq[iim] : 0.0);
    if (Q < 0) {
        double ff = ((-Q) % 2 ? -1.0 : 1.0);
        t.re *= ff;
        t.im *= -ff;
    }
    return t;
}
/* Gaussian quadrature in inclination and trapezoidal rule in azimuths */
void dir_MakeDirections(int n_incl_oct, int n_azim_oct, t_directions *dirs)
{
    int     n_gl   = 2 * n_incl_oct, i, a, j;
    int     n_rays = n_gl * 4 * n_azim_oct;
    double *gl_mus, *gl_w, *inc, *azi;
    double  w_azim = 1.0 / (4.0 * n_azim_oct), ac;
    double *azim, dazim = 2.0 * M_PI * w_azim;
    double  st, ct, sa, ca;

    if (n_incl_oct * n_azim_oct * 8 > MAXNDIRS) {
        Error(E_ERROR, POR_AT, "too many directions (%d > %d)",
              n_incl_oct * n_azim_oct * 8, MAXNDIRS);
    } else if (n_incl_oct * n_azim_oct <= 0) {
        Error(E_ERROR, POR_AT,
              "invalid number of directions per octant (%d, %d)", n_incl_oct,
              n_azim_oct);
    }
    gl_mus = ArrayNew(n_gl);
    gl_w   = ArrayNew(n_gl);
    inc    = ArrayNew(n_rays);
    azi    = ArrayNew(n_rays);
    azim   = ArrayNew(4 * n_azim_oct);
    stk_Add(1, POR_AT, "generating directions quadrature [%d x %d] per octant",
            n_incl_oct, n_azim_oct);

    /* Gaussian from -1 to 0 and from 0 to 1 */
    mat_GauLeg(n_gl / 2, -1.0, 0.0, gl_mus, gl_w);
    for (i = n_gl / 2; i < n_gl; i++) {
        gl_mus[i] = -gl_mus[n_gl - i - 1];
        gl_w[i]   = gl_w[n_gl - i - 1];
    }

    /* azimuths */
    for (i = 0; i < 4 * n_azim_oct; i++) {
        azim[i] = dazim * (0.5 + (double)i);
    }
    j = 0;
    for (i = 0; i < n_gl; i++) {
        ac = acos(gl_mus[i]);
        for (a = 0; a < 4 * n_azim_oct; a++) {
            inc[j]          = ac;
            azi[j]          = azim[a];
            dirs->weight[j] = 0.5 * gl_w[i] * w_azim;
            j++;
        }
    }
    dirs->n_rays = n_rays;
    for (i = 0; i < n_rays; i++) {
        st = sin(inc[i]);
        ct = cos(inc[i]);
        sa = sin(azi[i]);
        ca = cos(azi[i]);
#ifndef KNEER_HEASLEY
        dirs->dir[i].x = st * ca;
        dirs->dir[i].y = st * sa;
        dirs->dir[i].z = ct;
#else
        dirs->dir[i].x = st * ca;
        dirs->dir[i].y = ct;
        dirs->dir[i].z = st * sa;
        if (NSTOKES > 1)
            Error(E_ERROR, POR_AT,
                  "TKQ TENZORY NEJSOU TIMHLE ZPUSOBEM SPRAVNE SPOCTENY: "
                  "OPRAV!!!");
#endif
        dir_CalcTKQ(st, ct, sa, ca, dirs->tkq[i]);
    }
    ac = 0.0;
    for (i = 0; i < n_rays; i++) {
        ac += dirs->weight[i];
    }
    for (i = 0; i < n_rays; i++) {
        dirs->weight[i] /= ac;
    }
    dirs->n_incl_oct = n_incl_oct;
    dirs->n_azim_oct = n_azim_oct;
    for (i = 0; i < n_rays; i++) {
        if (dirs->dir[i].x > 0.0 && dirs->dir[i].y > 0.0 &&
            dirs->dir[i].z > 0.0)
            dirs->octant[i] = OCT_RFU;
        else if (dirs->dir[i].x < 0.0 && dirs->dir[i].y > 0.0 &&
                 dirs->dir[i].z > 0.0)
            dirs->octant[i] = OCT_LFU;
        else if (dirs->dir[i].x > 0.0 && dirs->dir[i].y < 0.0 &&
                 dirs->dir[i].z > 0.0)
            dirs->octant[i] = OCT_RBU;
        else if (dirs->dir[i].x > 0.0 && dirs->dir[i].y > 0.0 &&
                 dirs->dir[i].z < 0.0)
            dirs->octant[i] = OCT_RFD;
        else if (dirs->dir[i].x < 0.0 && dirs->dir[i].y < 0.0 &&
                 dirs->dir[i].z < 0.0)
            dirs->octant[i] = OCT_LBD;
        else if (dirs->dir[i].x > 0.0 && dirs->dir[i].y < 0.0 &&
                 dirs->dir[i].z < 0.0)
            dirs->octant[i] = OCT_RBD;
        else if (dirs->dir[i].x < 0.0 && dirs->dir[i].y > 0.0 &&
                 dirs->dir[i].z < 0.0)
            dirs->octant[i] = OCT_LFD;
        else if (dirs->dir[i].x < 0.0 && dirs->dir[i].y < 0.0 &&
                 dirs->dir[i].z > 0.0)
            dirs->octant[i] = OCT_LBU;
#ifdef DEBUG
        else
            IERR;
#endif
    }

    for (i = 0; i < dirs->n_rays; i++) {
        if (fabs(dirs->dir[i].z) < 1.0) {
            double lxy      = sqrt(dirs->dir[i].x * dirs->dir[i].x +
                              dirs->dir[i].y * dirs->dir[i].y);
            dirs->cos_az[i] = dirs->dir[i].x / lxy;
            dirs->sin_az[i] = dirs->dir[i].y / lxy;
        } else {
            dirs->cos_az[i] = 1.0;
            dirs->sin_az[i] = 0.0;
        }
    }

    free(gl_mus);
    free(gl_w);
    free(inc);
    free(azi);
    free(azim);
}
/************************ CARLSON QUADRATURE **************************/

#define NRO_A2 1
#define NRO_A4 3
#define NRO_A6 6
#define NRO_A8 10
#define NRO_B4 3
#define NRO_B6 6
#define NRO_B8 10

#define SET_A2 0
#define SET_A4 1
#define SET_A6 2
#define SET_A8 3
#define SET_B4 4
#define SET_B6 5
#define SET_B8 6

/* local structure taken from Han's code RH */
typedef struct Geometry {
    int    Nrays; /* total in 4 octants */
    double mux[MAXNDIRS];
    double muy[MAXNDIRS];
    double muz[MAXNDIRS];
    double wmu[MAXNDIRS];
} Geometry;

/* Carlson quadrature (set: SET_A2 etc.) */
void dir_MakeDirectionsCarlson(int set, t_directions *dirs)
{
    int      mu, i, f;
    double   wnorm;
    Geometry geometry;

    /* --- Set A2 --                                     -------------- */

    double mux_A2[NRO_A2] = {0.57735026};
    double muy_A2[NRO_A2] = {0.57735026};
    double wmu_A2[NRO_A2] = {1.00000000};

    /* --- Set A4 --                                     -------------- */

    double mux_A4[NRO_A4] = {0.88191710, 0.33333333, 0.33333333};
    double muy_A4[NRO_A4] = {0.33333333, 0.88191710, 0.33333333};
    double wmu_A4[NRO_A4] = {0.33333333, 0.33333333, 0.33333333};

    /* --- Set A6 --                                     -------------- */

    double mux_A6[NRO_A6] = {0.93094934, 0.68313005, 0.25819889,
                             0.68313005, 0.25819889, 0.25819889};
    double muy_A6[NRO_A6] = {0.25819889, 0.68313005, 0.93094934,
                             0.25819889, 0.68313005, 0.25819889};
    double wmu_A6[NRO_A6] = {0.18333333, 0.15000000, 0.18333333,
                             0.15000000, 0.15000000, 0.18333333};

    /* --- Set A8 --                                     -------------- */

    double mux_A8[NRO_A8] = {0.21821789, 0.21821789, 0.21821789, 0.21821789,
                             0.57735027, 0.57735027, 0.57735027, 0.78679579,
                             0.78679579, 0.95118973};

    double muy_A8[NRO_A8] = {0.2182178,  0.57735027, 0.78679579, 0.95118973,
                             0.21821789, 0.57735027, 0.78679579, 0.21821789,
                             0.57735027, 0.21821789};

    double wmu_A8[NRO_A8] = {0.12698138,  0.091383524, 0.091383524, 0.12698138,
                             0.091383524, 0.070754692, 0.091383524, 0.091383524,
                             0.091383524, 0.12698138};

    /* --- Set B4 --                                     -------------- */

    double mux_B4[NRO_B4] = {0.70412415, 0.09175171, 0.09175171};
    double muy_B4[NRO_B4] = {0.09175171, 0.70412415, 0.09175171};
    double wmu_B4[NRO_B4] = {0.33333333, 0.33333333, 0.33333333};

    /* --- Set B6
     Weights from Jo Bruls, Apr 26 1999 --         -------------- */

    double mux_B6[NRO_B6] = {0.80847426, 0.57735027, 0.11417547,
                             0.57735027, 0.11417547, 0.11417547};
    double muy_B6[NRO_B6] = {0.11417547, 0.57735027, 0.80847426,
                             0.11417547, 0.57735027, 0.11417547};
    double wmu_B6[NRO_B6] = {0.22222222, 0.11111111, 0.22222222,
                             0.11111111, 0.11111111, 0.22222222};

    /* --- Set B8 --                                     -------------- */

    double mux_B8[NRO_B8] = {0.85708018, 0.70273364, 0.50307327, 0.11104440,
                             0.70273364, 0.50307327, 0.11104440, 0.50307327,
                             0.11104440, 0.11104440};
    double muy_B8[NRO_B8] = {0.11104440, 0.50307327, 0.70273364, 0.85708018,
                             0.11104440, 0.50307327, 0.70273364, 0.11104440,
                             0.50307327, 0.11104440};
    double wmu_B8[NRO_B8] = {0.17152232, 0.06900520, 0.06900520, 0.17152232,
                             0.06900520, 0.07140185, 0.06900520, 0.06900520,
                             0.06900520, 0.17152232};

    switch (set) {
    case SET_A2:
        geometry.Nrays = 4 * NRO_A2;
        break;
    case SET_A4:
        geometry.Nrays = 4 * NRO_A4;
        break;
    case SET_A6:
        geometry.Nrays = 4 * NRO_A6;
        break;
    case SET_A8:
        geometry.Nrays = 4 * NRO_A8;
        break;
    case SET_B4:
        geometry.Nrays = 4 * NRO_B4;
        break;
    case SET_B6:
        geometry.Nrays = 4 * NRO_B6;
        break;
    case SET_B8:
        geometry.Nrays = 4 * NRO_B8;
        break;
    default:
        geometry.Nrays = 0;
        Error(E_ERROR, POR_AT, "invalid ray set");
    }

    switch (set) {
    case SET_A2:
        for (mu = 0; mu < NRO_A2; mu++) {
            geometry.mux[mu] = mux_A2[mu];
            geometry.muy[mu] = muy_A2[mu];
            geometry.wmu[mu] = wmu_A2[mu];
        }
        break;
    case SET_A4:
        for (mu = 0; mu < NRO_A4; mu++) {
            geometry.mux[mu] = mux_A4[mu];
            geometry.muy[mu] = muy_A4[mu];
            geometry.wmu[mu] = wmu_A4[mu];
        }
        break;
    case SET_A6:
        for (mu = 0; mu < NRO_A6; mu++) {
            geometry.mux[mu] = mux_A6[mu];
            geometry.muy[mu] = muy_A6[mu];
            geometry.wmu[mu] = wmu_A6[mu];
        }
        break;
    case SET_A8:
        for (mu = 0; mu < NRO_A8; mu++) {
            geometry.mux[mu] = mux_A8[mu];
            geometry.muy[mu] = muy_A8[mu];
            geometry.wmu[mu] = wmu_A8[mu];
        }
        break;
    case SET_B4:
        for (mu = 0; mu < NRO_B4; mu++) {
            geometry.mux[mu] = mux_B4[mu];
            geometry.muy[mu] = muy_B4[mu];
            geometry.wmu[mu] = wmu_B4[mu];
        }
        break;
    case SET_B6:
        for (mu = 0; mu < NRO_B6; mu++) {
            geometry.mux[mu] = mux_B6[mu];
            geometry.muy[mu] = muy_B6[mu];
            geometry.wmu[mu] = wmu_B6[mu];
        }
        break;
    case SET_B8:
        for (mu = 0; mu < NRO_B8; mu++) {
            geometry.mux[mu] = mux_B8[mu];
            geometry.muy[mu] = muy_B8[mu];
            geometry.wmu[mu] = wmu_B8[mu];
        }
        break;
    default:;
    }

    /*****************************************/
    /* conversion to the t_directions format */
    dirs->n_rays     = geometry.Nrays * 2;
    dirs->n_azim_oct = 1;
    dirs->n_incl_oct = dirs->n_rays / 8;
    /* +x+y-z */
    f = 0;
    for (i = 0; i < dirs->n_rays / 8; i++) {
        dirs->dir[i + f].x = geometry.mux[i];
        dirs->dir[i + f].y = geometry.muy[i];
        dirs->dir[i + f].z =
            -sqrt(1.0 - dirs->dir[i + f].x * dirs->dir[i + f].x -
                  dirs->dir[i + f].y * dirs->dir[i + f].y);
        dirs->weight[i + f] = geometry.wmu[i];
    }
    /* -x+y-z */
    f = dirs->n_rays / 8;
    for (i = 0; i < dirs->n_rays / 8; i++) {
        dirs->dir[i + f].x = -geometry.mux[i];
        dirs->dir[i + f].y = geometry.muy[i];
        dirs->dir[i + f].z =
            -sqrt(1.0 - dirs->dir[i + f].x * dirs->dir[i + f].x -
                  dirs->dir[i + f].y * dirs->dir[i + f].y);
        dirs->weight[i + f] = geometry.wmu[i];
    }
    /* -x-y-z */
    f = 2 * dirs->n_rays / 8;
    for (i = 0; i < dirs->n_rays / 8; i++) {
        dirs->dir[i + f].x = -geometry.mux[i];
        dirs->dir[i + f].y = -geometry.muy[i];
        dirs->dir[i + f].z =
            -sqrt(1.0 - dirs->dir[i + f].x * dirs->dir[i + f].x -
                  dirs->dir[i + f].y * dirs->dir[i + f].y);
        dirs->weight[i + f] = geometry.wmu[i];
    }
    /* +x-y-z */
    f = 3 * dirs->n_rays / 8;
    for (i = 0; i < dirs->n_rays / 8; i++) {
        dirs->dir[i + f].x = geometry.mux[i];
        dirs->dir[i + f].y = -geometry.muy[i];
        dirs->dir[i + f].z =
            -sqrt(1.0 - dirs->dir[i + f].x * dirs->dir[i + f].x -
                  dirs->dir[i + f].y * dirs->dir[i + f].y);
        dirs->weight[i + f] = geometry.wmu[i];
    }
    /* upper hemisphere */
    /* +x+y+z */
    f = 4 * dirs->n_rays / 8;
    for (i = 0; i < dirs->n_rays / 8; i++) {
        dirs->dir[i + f].x = geometry.mux[i];
        dirs->dir[i + f].y = geometry.muy[i];
        dirs->dir[i + f].z =
            sqrt(1.0 - dirs->dir[i + f].x * dirs->dir[i + f].x -
                 dirs->dir[i + f].y * dirs->dir[i + f].y);
        dirs->weight[i + f] = geometry.wmu[i];
    }
    /* -x+y+z */
    f = 5 * dirs->n_rays / 8;
    for (i = 0; i < dirs->n_rays / 8; i++) {
        dirs->dir[i + f].x = -geometry.mux[i];
        dirs->dir[i + f].y = geometry.muy[i];
        dirs->dir[i + f].z =
            sqrt(1.0 - dirs->dir[i + f].x * dirs->dir[i + f].x -
                 dirs->dir[i + f].y * dirs->dir[i + f].y);
        dirs->weight[i + f] = geometry.wmu[i];
    }
    /* -x-y+z */
    f = 6 * dirs->n_rays / 8;
    for (i = 0; i < dirs->n_rays / 8; i++) {
        dirs->dir[i + f].x = -geometry.mux[i];
        dirs->dir[i + f].y = -geometry.muy[i];
        dirs->dir[i + f].z =
            sqrt(1.0 - dirs->dir[i + f].x * dirs->dir[i + f].x -
                 dirs->dir[i + f].y * dirs->dir[i + f].y);
        dirs->weight[i + f] = geometry.wmu[i];
    }
    /* +x-y+z */
    f = 7 * dirs->n_rays / 8;
    for (i = 0; i < dirs->n_rays / 8; i++) {
        dirs->dir[i + f].x = geometry.mux[i];
        dirs->dir[i + f].y = -geometry.muy[i];
        dirs->dir[i + f].z =
            sqrt(1.0 - dirs->dir[i + f].x * dirs->dir[i + f].x -
                 dirs->dir[i + f].y * dirs->dir[i + f].y);
        dirs->weight[i + f] = geometry.wmu[i];
    }
    /* renormalization of weights */
    wnorm = 0.0;
    for (i = 0; i < dirs->n_rays; i++)
        wnorm += dirs->weight[i];
    for (i = 0; i < dirs->n_rays; i++)
        dirs->weight[i] /= wnorm;
    /* remaining properties of rays */
    for (i = 0; i < dirs->n_rays; i++) {
        double theta, chi, x, y;
        theta = acos(dirs->dir[i].z);
        x     = dirs->dir[i].x / sqrt(dirs->dir[i].x * dirs->dir[i].x +
                                  dirs->dir[i].y * dirs->dir[i].y);
        y     = dirs->dir[i].y / sqrt(dirs->dir[i].x * dirs->dir[i].x +
                                  dirs->dir[i].y * dirs->dir[i].y);
        chi   = acos(x);
        if (y < 0)
            chi = 2.0 * M_PI - chi;
        dirs->cos_az[i] = cos(chi);
        dirs->sin_az[i] = sin(chi);
        if (dirs->dir[i].x > 0.0 && dirs->dir[i].y > 0.0 &&
            dirs->dir[i].z > 0.0)
            dirs->octant[i] = OCT_RFU;
        else if (dirs->dir[i].x < 0.0 && dirs->dir[i].y > 0.0 &&
                 dirs->dir[i].z > 0.0)
            dirs->octant[i] = OCT_LFU;
        else if (dirs->dir[i].x > 0.0 && dirs->dir[i].y < 0.0 &&
                 dirs->dir[i].z > 0.0)
            dirs->octant[i] = OCT_RBU;
        else if (dirs->dir[i].x > 0.0 && dirs->dir[i].y > 0.0 &&
                 dirs->dir[i].z < 0.0)
            dirs->octant[i] = OCT_RFD;
        else if (dirs->dir[i].x < 0.0 && dirs->dir[i].y < 0.0 &&
                 dirs->dir[i].z < 0.0)
            dirs->octant[i] = OCT_LBD;
        else if (dirs->dir[i].x > 0.0 && dirs->dir[i].y < 0.0 &&
                 dirs->dir[i].z < 0.0)
            dirs->octant[i] = OCT_RBD;
        else if (dirs->dir[i].x < 0.0 && dirs->dir[i].y > 0.0 &&
                 dirs->dir[i].z < 0.0)
            dirs->octant[i] = OCT_LFD;
        else if (dirs->dir[i].x < 0.0 && dirs->dir[i].y < 0.0 &&
                 dirs->dir[i].z > 0.0)
            dirs->octant[i] = OCT_LBU;
        else
            IERR;
        dir_CalcTKQ(sin(theta), cos(theta), sin(chi), cos(chi), dirs->tkq[i]);
    }
}

/******************* END OF CARLSON QUADRATURE **************************/

/************************************************************************/
/************************* NEW QUADRATURE *******************************/

/* NOAQ from Stepan 2020 */
/* These quadratures are defined with octant rotation on the upper
   hemishpere and then reflection on the lower hemisphere */
#define S1_i_L3n1      0
#define S1_i_L7n3      1
#define S1_i_L9n6      2
#define S1_i_L11n7     3
#define S1_i_L13n10    4
#define S1_i_L15n11    5
#define S1_iquv_L3n3   6
#define S1_iquv_L5n4   7
#define S1_iquv_L7n7   8
#define S1_iquv_L9n8   9
#define S1_iquv_L11n11 10
#define S1_iquv_L13n14 11
#define S1_iquv_L15n18 12
/*************************/

/* NOAQ from Jaume Bestard 2020 */
/* These quadratures are defined with octant rotation/reflection on the
   upper hemishpere and then reflection on the lower hemisphere */
/* #define S2_i_LXnY        13 */
/* #define S2_i_LXnY        14 */
/* #define S2_i_LXnY        15 */
/* #define S2_i_LXnY        16 */
/* #define S2_i_LXnY        17 */
/* #define S2_i_LXnY        18 */
/* #define S2_iquv_LXnY     19 */
/* #define S2_iquv_LXnY     20 */
/* #define S2_iquv_LXnY     21 */
/* #define S2_iquv_LXnY     22 */
/* #define S2_iquv_LXnY     23 */
/* #define S2_iquv_LXnY     24 */
/* #define S2_iquv_LXnY     25 */
/* /\* Number of rays per octant *\/ */
/* #define S1_NRO_i_LXnY       */
/* #define S1_NRO_i_LXnY      */
/* #define S1_NRO_i_LXnY       */
/* #define S1_NRO_i_LXnY      */
/* #define S1_NRO_i_LXnY      */
/* #define S1_NRO_i_LXnY      */
/* #define S1_NRO_iquv_LXnY   */
/* #define S1_NRO_iquv_LXnY   */
/* #define S1_NRO_iquv_LXnY   */
/* #define S1_NRO_iquv_LXnY   */
/* #define S1_NRO_iquv_LXnY   */
/* #define S1_NRO_iquv_LXnY   */
/* #define S1_NRO_iquv_LXnY   */
/********************************/

/* The angular quadratures defined in this function are generated applying
   reflection symmetry at the plane XY to generate the up/low hemishpere.
   The rays in the octants are generated according to the symmetry defined
   in the switch statement.
*/
void dir_MakeDirectionsQuadSet(int set, t_directions *dirs)
{
    Geometry geometry;
    int      ii, nrays, ind;
    double   mux, muy, muz, wnorm;

    /* --- i_L3 -- */
    const int S1_NRO_i_L3n1   = 1;
    double    w_S1_i_L3n1[]   = {0.125};
    double    inc_S1_i_L3n1[] = {0.9553166181245091};
    double    az_S1_i_L3n1[]  = {0.7853981633974483};

    /* --- i_L7 -- */
    const int S1_NRO_i_L7n3   = 3;
    double    w_S1_i_L7n3[]   = {0.036628086288183176, 0.04300154334144645,
                            0.04537037037037037};
    double    inc_S1_i_L7n3[] = {1.2708659824999833, 0.530641796307861,
                              1.183199640139716};
    double    az_S1_i_L7n3[]  = {1.035871449209156, 1.0358714492091556,
                             0.2504732858117075};

    /* --- i_L9 -- */
    const int S1_NRO_i_L9n6   = 6;
    double    w_S1_i_L9n6[]   = {0.022713607613686692, 0.022787199039712287,
                            0.022110192033907447, 0.022654670920532104,
                            0.018155933946312444, 0.01657839644584903};
    double    inc_S1_i_L9n6[] = {0.7485528580199929, 1.2888170628889766,
                              1.2999904357123682, 0.8340014655924524,
                              1.3442122067358753, 0.3154296410749302};
    double    az_S1_i_L9n6[]  = {0.4445736974954408, 0.16430830105252672,
                             0.7001470951619867, 1.2089902959755494,
                             1.2185035835829259, 1.450871851640431};

    /* --- i_L11 -- */
    const int S1_NRO_i_L11n7   = 7;
    double    w_S1_i_L11n7[]   = {0.017454557861781708, 0.012270710186718273,
                             0.018536963106887672, 0.01967895331380289,
                             0.019637910647233123, 0.015657498014902245,
                             0.02176340686867407};
    double    inc_S1_i_L11n7[] = {0.7751296528532481, 1.4488153659016076,
                               1.346366376612715,  0.35525678075798633,
                               0.821624608997173,  1.0649592082491075,
                               1.2981515075045882};
    double    az_S1_i_L11n7[]  = {0.3489463187972298, 0.6398916304897622,
                              1.1828579410012607, 0.8476496739125453,
                              1.2701264834923627, 0.7334490217610155,
                              0.1333757725608049};
    /* --- i_L13 -- */
    const int S1_NRO_i_L13n10   = 10;
    double    w_S1_i_L13n10[]   = {0.01234006323564141,  0.011977753205880952,
                              0.014450026269801278, 0.013253956366881255,
                              0.010201119400831222, 0.011938568025220213,
                              0.01023597311503247,  0.012959288726089032,
                              0.014092902977591758, 0.013550348677030402};
    double    inc_S1_i_L13n10[] = {1.0438490978762576, 1.3690431994433148,
                                0.9548035954758332, 1.3603227064970025,
                                1.39565959977705,   1.3873615346723913,
                                0.2513272248220528, 0.5714648659961621,
                                0.9743918879826562, 0.6383914103536829};
    double    az_S1_i_L13n10[]  = {0.5982902474246599,  0.16164381566022093,
                               0.08485683466451856, 1.338259659339096,
                               0.5315192394965379,  0.9271396772239698,
                               0.32390516806675235, 1.3362015359143413,
                               1.0988820625177396,  0.5861591726967976};
    /* --- i_L15 -- */
    const int S1_NRO_i_L15n11 = 11;
    double    w_S1_i_L15n11[] = {
        0.01130912468521607,  0.009652307741108415, 0.013486249257448579,
        0.011644240437362686, 0.012045724827157482, 0.012838609802518424,
        0.011977125696312044, 0.01283860980251842,  0.011309124685216068,
        0.010715526695526693, 0.007183356369615116};
    double inc_S1_i_L15n11[] = {
        0.9305348558251064, 1.4203920062663766, 0.5956343119629927,
        0.2718965527178955, 0.6558932789350811, 1.361652484920464,
        1.1331544629004193, 1.361652484920464,  0.9305348558251064,
        1.0697033135295395, 1.5003766359809885};
    double az_S1_i_L15n11[] = {
        1.4767259364407384,  1.084026854742014,   1.084026854742014,
        0.2986286913445658,  0.29862869134456604, 1.476725936440738,
        0.29862869134456577, 0.69132777304329,    0.69132777304329,
        1.084026854742014,   0.2986286913445657};

    /* --- iquv_L3 -- */
    const int S1_NRO_iquv_L3n3   = 3;
    double    w_S1_iquv_L3n3[]   = {0.031886790427685094, 0.04102987623898158,
                               0.052083333333333336};
    double    inc_S1_iquv_L3n3[] = {1.4339289660034116, 0.5268575702427947,
                                 1.1071487177940904};
    double    az_S1_iquv_L3n3[]  = {0.5841827142313937, 0.5841827142313939,
                                1.3695808776288423};

    /* --- iquv_L5 -- */
    const int S1_NRO_iquv_L5n4   = 4;
    double    w_S1_iquv_L5n4[]   = {0.03008835383493305, 0.042736793710787514,
                               0.02500191721861294, 0.027172935235666495};
    double    inc_S1_iquv_L5n4[] = {1.3355966682171894, 1.2064898976141172,
                                 0.4305523838928408, 0.771626574033839};
    double    az_S1_iquv_L5n4[]  = {1.060608635393824, 0.2752104719963756,
                                0.2752104719963755, 1.0606086353938236};

    /* --- iquv_L7 -- */
    const int S1_NRO_iquv_L7n7   = 7;
    double    w_S1_iquv_L7n7[]   = {0.015847015063176075, 0.013757295125958881,
                               0.019342447264483847, 0.01857500229460964,
                               0.013380933807854508, 0.02044375833584162,
                               0.023653548108075434};
    double    inc_S1_iquv_L7n7[] = {0.32352600010954236, 0.8803902036169932,
                                 1.3275232724723705,  0.6556637849195155,
                                 1.4907424636023907,  0.9483767871632462,
                                 1.2746387594075759};
    double    az_S1_iquv_L7n7[]  = {0.17623463470871215, 1.4200392660031687,
                                1.3664461237274743,  0.9342439730178802,
                                0.31765989589336724, 0.34867815833156374,
                                0.8463761755892827};

    /* --- iquv_L9 -- */
    const int S1_NRO_iquv_L9n8   = 8;
    double    w_S1_iquv_L9n8[]   = {0.019289356035900717, 0.015114566322221879,
                               0.019684767944196598, 0.010675866835903073,
                               0.01080008302319179,  0.014678112530955844,
                               0.019464988039525417, 0.015292259268104687};
    double    inc_S1_iquv_L9n8[] = {1.3324472922942312,  0.8414409990582649,
                                 1.327302299855276,   0.38573817504280783,
                                 0.35518946347819186, 0.8594813718457612,
                                 1.3304509775228786,  0.8462297337010777};
    double    az_S1_iquv_L9n8[]  = {1.2726473255677595,  0.8557186536717448,
                                0.7494185579509058,  1.4416008122140873,
                                0.6560886553928543,  1.3727224180752748,
                                0.22579748955950976, 0.3267814264929364};

    /* --- iquv_L11 -- */
    const int S1_NRO_iquv_L11n11 = 11;
    double    w_S1_iquv_L11n11[] = {
        0.013928998075380375, 0.011639454305572013, 0.011029475400118194,
        0.01117777914840798,  0.011639454305572,    0.011499122313789287,
        0.012557626403854785, 0.011968777966644249, 0.005651235410063879,
        0.013928998075380379, 0.009979078595216876};
    double inc_S1_iquv_L11n11[] = {
        1.3427368675366178, 0.8754615685291834, 1.004096235857327,
        1.3921151953032822, 0.8754615685291834, 1.0415944553976788,
        0.5024165304052693, 0.5392381925573729, 0.1416880536636461,
        1.3427368675366178, 1.4188676770520234};
    double az_S1_iquv_L11n11[] = {
        1.4096818409550547, 0.6242836775576062, 1.0169827592563307,
        1.0169827592563303, 1.4096818409550553, 0.23158459585888239,
        1.0169827592563303, 0.2315845958588824, 0.23158459585888239,
        0.6242836775576064, 0.23158459585888247};

    /* --- iquv_L13 -- */
    const int S1_NRO_iquv_L13n14 = 14;
    double    w_S1_iquv_L13n14[] = {
        0.00940198573013053,  0.009724739249628363, 0.00733638547728949,
        0.009364158915773805, 0.010888116068585385, 0.006645865611251284,
        0.006633848084763223, 0.010488373826850328, 0.010364129557869509,
        0.009137304992051887, 0.010079305268725575, 0.004602532138815957,
        0.01156144852555704,  0.008771806552707621};
    double inc_S1_iquv_L13n14[] = {
        0.6410976457715482,  1.007298431474643,  1.4053011364483594,
        0.6310041889736145,  1.3927167183129732, 0.3423002167989187,
        0.24418903191491584, 1.3994384514102136, 1.0091145988063805,
        0.7034566616102036,  1.0161618884209984, 1.3454164065667764,
        1.379262484235991,   1.0647611727441126};
    double az_S1_iquv_L13n14[] = {
        0.8891469032702116, 1.5332750900965901,  1.0521453596186656,
        1.4169303601348333, 0.629862018478126,   0.4029261445887608,
        1.186660740058489,  0.23773746939510743, 1.1326005624445419,
        0.3632804354531381, 0.7179415879770789,  0.9687881097142609,
        1.4208749253845392, 0.33147946673217654};

    /* --- iquv_L15 -- */
    const int S1_NRO_iquv_L15n18 = 18;
    double    w_S1_iquv_L15n18[] = {
        0.00591402443445905,  0.008596636090469512, 0.007113673482367161,
        0.006827258788941489, 0.006754747098551407, 0.007479599563267462,
        0.007204332706475753, 0.004857493646975331, 0.007336667967350494,
        0.007086892105928068, 0.006578810918075098, 0.006617138696419558,
        0.007781933481283711, 0.006596638597620959, 0.007274106171534564,
        0.007120519412160358, 0.007306722626653114, 0.006552804211466908};

    double inc_S1_iquv_L15n18[] = {
        0.3605855962795585, 1.3964574375296421,  1.1331025708537859,
        1.1195853157371156, 1.1297572049485878,  0.6924431086806494,
        0.83373147658116,   0.17699593016633913, 1.42262035521177,
        1.4274319584542177, 1.1659464714595242,  0.4923867076513321,
        1.037986358474255,  0.5513171221665994,  0.8602016438235026,
        0.8064064318730164, 1.4263645844435608,  1.4376444020777979};
    double az_S1_iquv_L15n18[] = {
        0.7495523149208301, 0.5879739850491212,   1.2568197566533268,
        0.3228375168855429, 0.016048903718414425, 0.6748846700402384,
        1.4505684954711642, 1.4896768095119137,   1.524417273218248,
        1.2106641057725533, 0.9260444832236767,   0.16002116599309352,
        0.6379557526677402, 1.2097288598059455,   1.033477053790857,
        0.2726407366475454, 0.27062651723728204,  0.8984582437176789};

    double *weights, *azimuths, *inclinations;
    int     nrays_octant, reflection, rotation;

    switch (set) {
    case (S1_i_L3n1): {
        nrays_octant = S1_NRO_i_L3n1;
        weights      = w_S1_i_L3n1;
        azimuths     = az_S1_i_L3n1;
        inclinations = inc_S1_i_L3n1;
        nrays        = nrays_octant * 8;
        reflection   = 0;
        rotation     = 1;
    } break;
    case (S1_i_L7n3): {
        nrays_octant = S1_NRO_i_L7n3;
        weights      = w_S1_i_L7n3;
        azimuths     = az_S1_i_L7n3;
        inclinations = inc_S1_i_L7n3;
        nrays        = nrays_octant * 8;
        reflection   = 0;
        rotation     = 1;
    } break;
    case (S1_i_L9n6): {
        nrays_octant = S1_NRO_i_L9n6;
        weights      = w_S1_i_L9n6;
        azimuths     = az_S1_i_L9n6;
        inclinations = inc_S1_i_L9n6;
        nrays        = nrays_octant * 8;
        reflection   = 0;
        rotation     = 1;
    } break;
    case (S1_i_L11n7): {
        nrays_octant = S1_NRO_i_L11n7;
        weights      = w_S1_i_L11n7;
        azimuths     = az_S1_i_L11n7;
        inclinations = inc_S1_i_L11n7;
        nrays        = nrays_octant * 8;
        reflection   = 0;
        rotation     = 1;
    } break;
    case (S1_i_L13n10): {
        nrays_octant = S1_NRO_i_L13n10;
        weights      = w_S1_i_L13n10;
        azimuths     = az_S1_i_L13n10;
        inclinations = inc_S1_i_L13n10;
        nrays        = nrays_octant * 8;
        reflection   = 0;
        rotation     = 1;
    } break;
    case (S1_i_L15n11): {
        nrays_octant = S1_NRO_i_L15n11;
        weights      = w_S1_i_L15n11;
        azimuths     = az_S1_i_L15n11;
        inclinations = inc_S1_i_L15n11;
        nrays        = nrays_octant * 8;
        reflection   = 0;
        rotation     = 1;
    } break;
    case (S1_iquv_L3n3): {
        nrays_octant = S1_NRO_iquv_L3n3;
        weights      = w_S1_iquv_L3n3;
        azimuths     = az_S1_iquv_L3n3;
        inclinations = inc_S1_iquv_L3n3;
        nrays        = nrays_octant * 8;
        reflection   = 0;
        rotation     = 1;
    } break;
    case (S1_iquv_L5n4): {
        nrays_octant = S1_NRO_iquv_L5n4;
        weights      = w_S1_iquv_L5n4;
        azimuths     = az_S1_iquv_L5n4;
        inclinations = inc_S1_iquv_L5n4;
        nrays        = nrays_octant * 8;
        reflection   = 0;
        rotation     = 1;
    } break;
    case (S1_iquv_L7n7): {
        nrays_octant = S1_NRO_iquv_L7n7;
        weights      = w_S1_iquv_L7n7;
        azimuths     = az_S1_iquv_L7n7;
        inclinations = inc_S1_iquv_L7n7;
        nrays        = nrays_octant * 8;
        reflection   = 0;
        rotation     = 1;
    } break;
    case (S1_iquv_L9n8): {
        nrays_octant = S1_NRO_iquv_L9n8;
        weights      = w_S1_iquv_L9n8;
        azimuths     = az_S1_iquv_L9n8;
        inclinations = inc_S1_iquv_L9n8;
        nrays        = nrays_octant * 8;
        reflection   = 0;
        rotation     = 1;
    } break;
    case (S1_iquv_L11n11): {
        nrays_octant = S1_NRO_iquv_L11n11;
        weights      = w_S1_iquv_L11n11;
        azimuths     = az_S1_iquv_L11n11;
        inclinations = inc_S1_iquv_L11n11;
        nrays        = nrays_octant * 8;
        reflection   = 0;
        rotation     = 1;
    } break;
    case (S1_iquv_L13n14): {
        nrays_octant = S1_NRO_iquv_L13n14;
        weights      = w_S1_iquv_L13n14;
        azimuths     = az_S1_iquv_L13n14;
        inclinations = inc_S1_iquv_L13n14;
        nrays        = nrays_octant * 8;
        reflection   = 0;
        rotation     = 1;
    } break;
    case (S1_iquv_L15n18): {
        nrays_octant = S1_NRO_iquv_L15n18;
        weights      = w_S1_iquv_L15n18;
        azimuths     = az_S1_iquv_L15n18;
        inclinations = inc_S1_iquv_L15n18;
        nrays        = nrays_octant * 8;
        reflection   = 0;
        rotation     = 1;
    } break;
    default:
        nrays = 0;
        Error(E_ERROR, POR_AT, "invalid ray set");
    }

    /* Imposing the set ID in n_incl_oct */
    dirs->n_rays     = nrays;
    dirs->n_azim_oct = -1;
    dirs->n_incl_oct = set;

    /* Direction cosines for the RFD octant: +x+y-z */
    for (ii = 0; ii < nrays_octant; ii++) {
        dirs->dir[ii].x  = sin(inclinations[ii]) * cos(azimuths[ii]);
        dirs->dir[ii].y  = sin(inclinations[ii]) * sin(azimuths[ii]);
        dirs->dir[ii].z  = -cos(inclinations[ii]);
        dirs->weight[ii] = weights[ii];
        dirs->octant[ii] = OCT_RFD;
        dirs->cos_az[ii] = cos(azimuths[ii]);
        dirs->sin_az[ii] = sin(azimuths[ii]);
    }
    /* WARNING: the first octant belongs to the DOWN HEMISPHERE. */
    /* -x+y-z: LFD */
    ind = nrays_octant;
    for (ii = 0; ii < nrays_octant; ii++) {
        if (reflection) {
            dirs->dir[ind + ii].x  = -dirs->dir[ii].x;
            dirs->dir[ind + ii].y  = +dirs->dir[ii].y;
            dirs->dir[ind + ii].z  = dirs->dir[ii].z;
            dirs->weight[ind + ii] = dirs->weight[ii];
            dirs->octant[ind + ii] = OCT_LFD;
            dirs->cos_az[ind + ii] = -dirs->cos_az[ii];
            dirs->sin_az[ind + ii] = dirs->sin_az[ii];
        } else if (rotation) {
            dirs->dir[ind + ii].x  = -dirs->dir[ii].y;
            dirs->dir[ind + ii].y  = +dirs->dir[ii].x;
            dirs->dir[ind + ii].z  = dirs->dir[ii].z;
            dirs->weight[ind + ii] = dirs->weight[ii];
            dirs->octant[ind + ii] = OCT_LFD;
            dirs->cos_az[ind + ii] = -dirs->sin_az[ii];
            dirs->sin_az[ind + ii] = dirs->cos_az[ii];
        }
    }
    /* -x-y-z: LBD */
    ind = 2 * nrays_octant;
    for (ii = 0; ii < nrays_octant; ii++) {
        if (rotation || reflection) {
            dirs->dir[ind + ii].x  = -dirs->dir[ii].x;
            dirs->dir[ind + ii].y  = -dirs->dir[ii].y;
            dirs->dir[ind + ii].z  = dirs->dir[ii].z;
            dirs->weight[ind + ii] = dirs->weight[ii];
            dirs->octant[ind + ii] = OCT_LBD;
            dirs->cos_az[ind + ii] = -dirs->cos_az[ii];
            dirs->sin_az[ind + ii] = -dirs->sin_az[ii];
        }
    }
    /* +x-y-z: RBD */
    ind = 3 * nrays_octant;
    for (ii = 0; ii < nrays_octant; ii++) {
        if (reflection) {
            dirs->dir[ind + ii].x  = +dirs->dir[ii].x;
            dirs->dir[ind + ii].y  = -dirs->dir[ii].y;
            dirs->dir[ind + ii].z  = dirs->dir[ii].z;
            dirs->weight[ind + ii] = dirs->weight[ii];
            dirs->octant[ind + ii] = OCT_RBD;
            dirs->cos_az[ind + ii] = +dirs->cos_az[ii];
            dirs->sin_az[ind + ii] = -dirs->sin_az[ii];
        } else if (rotation) {
            dirs->dir[ind + ii].x  = +dirs->dir[ii].y;
            dirs->dir[ind + ii].y  = -dirs->dir[ii].x;
            dirs->dir[ind + ii].z  = dirs->dir[ii].z;
            dirs->weight[ind + ii] = dirs->weight[ii];
            dirs->octant[ind + ii] = OCT_RBD;
            dirs->cos_az[ind + ii] = +dirs->sin_az[ii];
            dirs->sin_az[ind + ii] = -dirs->cos_az[ii];
        }
    }
    /* +x+y+z: RFU */
    ind = 4 * nrays_octant;
    for (ii = 0; ii < nrays_octant; ii++) {
        dirs->dir[ind + ii].x  = +dirs->dir[ii].x;
        dirs->dir[ind + ii].y  = +dirs->dir[ii].y;
        dirs->dir[ind + ii].z  = -dirs->dir[ii].z;
        dirs->weight[ind + ii] = dirs->weight[ii];
        dirs->octant[ind + ii] = OCT_RFU;
        dirs->cos_az[ind + ii] = +dirs->cos_az[ii];
        dirs->sin_az[ind + ii] = +dirs->sin_az[ii];
    }
    /* -x+y+z: LFU */
    ind = 5 * nrays_octant;
    for (ii = 0; ii < nrays_octant; ii++) {
        if (reflection) {
            dirs->dir[ind + ii].x  = -dirs->dir[ii].x;
            dirs->dir[ind + ii].y  = +dirs->dir[ii].y;
            dirs->dir[ind + ii].z  = -dirs->dir[ii].z;
            dirs->weight[ind + ii] = dirs->weight[ii];
            dirs->octant[ind + ii] = OCT_LFU;
            dirs->cos_az[ind + ii] = -dirs->cos_az[ii];
            dirs->sin_az[ind + ii] = +dirs->sin_az[ii];
        } else if (rotation) {
            dirs->dir[ind + ii].x  = -dirs->dir[ii].y;
            dirs->dir[ind + ii].y  = +dirs->dir[ii].x;
            dirs->dir[ind + ii].z  = -dirs->dir[ii].z;
            dirs->weight[ind + ii] = dirs->weight[ii];
            dirs->octant[ind + ii] = OCT_LFU;
            dirs->cos_az[ind + ii] = -dirs->sin_az[ii];
            dirs->sin_az[ind + ii] = +dirs->cos_az[ii];
        }
    }
    /* -x-y+z: LBU */
    ind = 6 * nrays_octant;
    for (ii = 0; ii < nrays_octant; ii++) {
        if (rotation || reflection) {
            dirs->dir[ind + ii].x  = -dirs->dir[ii].x;
            dirs->dir[ind + ii].y  = -dirs->dir[ii].y;
            dirs->dir[ind + ii].z  = -dirs->dir[ii].z;
            dirs->weight[ind + ii] = dirs->weight[ii];
            dirs->octant[ind + ii] = OCT_LBU;
            dirs->cos_az[ind + ii] = -dirs->cos_az[ii];
            dirs->sin_az[ind + ii] = -dirs->sin_az[ii];
        }
    }
    /* +x-y+z: RBU */
    ind = 7 * nrays_octant;
    for (ii = 0; ii < nrays_octant; ii++) {
        if (reflection) {
            dirs->dir[ind + ii].x  = +dirs->dir[ii].x;
            dirs->dir[ind + ii].y  = -dirs->dir[ii].y;
            dirs->dir[ind + ii].z  = -dirs->dir[ii].z;
            dirs->weight[ind + ii] = dirs->weight[ii];
            dirs->octant[ind + ii] = OCT_RBU;
            dirs->cos_az[ind + ii] = +dirs->cos_az[ii];
            dirs->sin_az[ind + ii] = -dirs->sin_az[ii];
        } else if (rotation) {
            dirs->dir[ind + ii].x  = +dirs->dir[ii].y;
            dirs->dir[ind + ii].y  = -dirs->dir[ii].x;
            dirs->dir[ind + ii].z  = -dirs->dir[ii].z;
            dirs->weight[ind + ii] = dirs->weight[ii];
            dirs->octant[ind + ii] = OCT_RBD;
            dirs->cos_az[ind + ii] = +dirs->sin_az[ii];
            dirs->sin_az[ind + ii] = -dirs->cos_az[ii];
        }
    }

    /* Renormalization of weights */
    wnorm = 0.0;
    double sinaz, cosaz, cosinc, sininc;
    for (ii = 0; ii < dirs->n_rays; ii++)
        wnorm += dirs->weight[ii];
    for (ii = 0; ii < dirs->n_rays; ii++) {
        dirs->weight[ii] /= wnorm;
        /* Computing the TKQ tensor */
        sinaz  = dirs->sin_az[ii];
        cosaz  = dirs->cos_az[ii];
        cosinc = dirs->dir[ii].z;
        sininc = sqrt(1.0 - cosinc * cosinc);
        dir_CalcTKQ(sininc, cosinc, sinaz, cosaz, dirs->tkq[ii]);
    }

    stk_Add(1, POR_AT, "generating quadrature set %d with %d rays in total",
            set, dirs->n_rays);
}
/********************** END NEW QUADRATURE ******************************/

/************************************************************************/
/************************************************************************/
/**************************  JIRI QUADRATURE   **************************/

static void MakeQuadratureX(t_directions *d)
{
    d->n_incl_oct = 5;
    d->n_azim_oct = 1;
    d->n_rays     = 40;
    d->dir[0].x   = 7.279497959492227110e-01;
    d->dir[0].y   = 5.323013134988791029e-01;
    d->dir[0].z   = 4.321393365858435809e-01;
    d->weight[0]  = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.123932616445856736e+00), cos(1.123932616445856736e+00),
                sin(6.313820011309434488e-01), cos(6.313820011309434488e-01),
                d->tkq[0]);
    d->octant[0] = 0;
    d->sin_az[0] = sin(6.313820011309434488e-01);
    d->cos_az[0] = cos(6.313820011309434488e-01);
    d->dir[5].x  = -7.279497959492227110e-01;
    d->dir[5].y  = 5.323013134988791029e-01;
    d->dir[5].z  = 4.321393365858435809e-01;
    d->weight[5] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.123932616445856736e+00), cos(1.123932616445856736e+00),
                sin(2.510210652458849445e+00), cos(2.510210652458849445e+00),
                d->tkq[5]);
    d->octant[5]  = 1;
    d->sin_az[5]  = sin(2.510210652458849445e+00);
    d->cos_az[5]  = cos(2.510210652458849445e+00);
    d->dir[10].x  = -7.279497959492227110e-01;
    d->dir[10].y  = -5.323013134988791029e-01;
    d->dir[10].z  = 4.321393365858435809e-01;
    d->weight[10] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.123932616445856736e+00), cos(1.123932616445856736e+00),
                sin(3.772974654720736787e+00), cos(3.772974654720736787e+00),
                d->tkq[10]);
    d->octant[10] = 2;
    d->sin_az[10] = sin(3.772974654720736787e+00);
    d->cos_az[10] = cos(3.772974654720736787e+00);
    d->dir[15].x  = 7.279497959492227110e-01;
    d->dir[15].y  = -5.323013134988791029e-01;
    d->dir[15].z  = 4.321393365858435809e-01;
    d->weight[15] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.123932616445856736e+00), cos(1.123932616445856736e+00),
                sin(5.651803306048642561e+00), cos(5.651803306048642561e+00),
                d->tkq[15]);
    d->octant[15] = 3;
    d->sin_az[15] = sin(5.651803306048642561e+00);
    d->cos_az[15] = cos(5.651803306048642561e+00);
    d->dir[20].x  = 7.279497959492227110e-01;
    d->dir[20].y  = 5.323013134988791029e-01;
    d->dir[20].z  = -4.321393365858435809e-01;
    d->weight[20] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.017660037143936158e+00), cos(2.017660037143936158e+00),
                sin(6.313820011309434488e-01), cos(6.313820011309434488e-01),
                d->tkq[20]);
    d->octant[0]  = 4;
    d->sin_az[0]  = sin(6.313820011309434488e-01);
    d->cos_az[0]  = cos(6.313820011309434488e-01);
    d->dir[25].x  = -7.279497959492227110e-01;
    d->dir[25].y  = 5.323013134988791029e-01;
    d->dir[25].z  = -4.321393365858435809e-01;
    d->weight[25] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.017660037143936158e+00), cos(2.017660037143936158e+00),
                sin(2.510210652458849445e+00), cos(2.510210652458849445e+00),
                d->tkq[25]);
    d->octant[25] = 5;
    d->sin_az[25] = sin(2.510210652458849445e+00);
    d->cos_az[25] = cos(2.510210652458849445e+00);
    d->dir[30].x  = -7.279497959492227110e-01;
    d->dir[30].y  = -5.323013134988791029e-01;
    d->dir[30].z  = -4.321393365858435809e-01;
    d->weight[30] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.017660037143936158e+00), cos(2.017660037143936158e+00),
                sin(3.772974654720736787e+00), cos(3.772974654720736787e+00),
                d->tkq[30]);
    d->octant[30] = 6;
    d->sin_az[30] = sin(3.772974654720736787e+00);
    d->cos_az[30] = cos(3.772974654720736787e+00);
    d->dir[35].x  = 7.279497959492227110e-01;
    d->dir[35].y  = -5.323013134988791029e-01;
    d->dir[35].z  = -4.321393365858435809e-01;
    d->weight[35] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.017660037143936158e+00), cos(2.017660037143936158e+00),
                sin(5.651803306048642561e+00), cos(5.651803306048642561e+00),
                d->tkq[35]);
    d->octant[35] = 7;
    d->sin_az[35] = sin(5.651803306048642561e+00);
    d->cos_az[35] = cos(5.651803306048642561e+00);
    d->dir[1].x   = 8.970451426192788125e-01;
    d->dir[1].y   = 6.811853755980193902e-03;
    d->dir[1].z   = 4.418864229092867579e-01;
    d->weight[1]  = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.113095870740461901e+00), cos(1.113095870740461901e+00),
                sin(7.593511747393682371e-03), cos(7.593511747393682371e-03),
                d->tkq[1]);
    d->octant[1] = 0;
    d->sin_az[1] = sin(7.593511747393682371e-03);
    d->cos_az[1] = cos(7.593511747393682371e-03);
    d->dir[6].x  = -8.970451426192788125e-01;
    d->dir[6].y  = 6.811853755980193902e-03;
    d->dir[6].z  = 4.418864229092867579e-01;
    d->weight[6] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.113095870740461901e+00), cos(1.113095870740461901e+00),
                sin(3.133999141842399361e+00), cos(3.133999141842399361e+00),
                d->tkq[6]);
    d->octant[6]  = 1;
    d->sin_az[6]  = sin(3.133999141842399361e+00);
    d->cos_az[6]  = cos(3.133999141842399361e+00);
    d->dir[11].x  = -8.970451426192788125e-01;
    d->dir[11].y  = -6.811853755980193902e-03;
    d->dir[11].z  = 4.418864229092867579e-01;
    d->weight[11] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.113095870740461901e+00), cos(1.113095870740461901e+00),
                sin(3.149186165337186871e+00), cos(3.149186165337186871e+00),
                d->tkq[11]);
    d->octant[11] = 2;
    d->sin_az[11] = sin(3.149186165337186871e+00);
    d->cos_az[11] = cos(3.149186165337186871e+00);
    d->dir[16].x  = 8.970451426192788125e-01;
    d->dir[16].y  = -6.811853755980193902e-03;
    d->dir[16].z  = 4.418864229092867579e-01;
    d->weight[16] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.113095870740461901e+00), cos(1.113095870740461901e+00),
                sin(6.275591795432192477e+00), cos(6.275591795432192477e+00),
                d->tkq[16]);
    d->octant[16] = 3;
    d->sin_az[16] = sin(6.275591795432192477e+00);
    d->cos_az[16] = cos(6.275591795432192477e+00);
    d->dir[21].x  = 8.970451426192788125e-01;
    d->dir[21].y  = 6.811853755980193902e-03;
    d->dir[21].z  = -4.418864229092867579e-01;
    d->weight[21] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.028496782849331215e+00), cos(2.028496782849331215e+00),
                sin(7.593511747393682371e-03), cos(7.593511747393682371e-03),
                d->tkq[21]);
    d->octant[1]  = 4;
    d->sin_az[1]  = sin(7.593511747393682371e-03);
    d->cos_az[1]  = cos(7.593511747393682371e-03);
    d->dir[26].x  = -8.970451426192788125e-01;
    d->dir[26].y  = 6.811853755980193902e-03;
    d->dir[26].z  = -4.418864229092867579e-01;
    d->weight[26] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.028496782849331215e+00), cos(2.028496782849331215e+00),
                sin(3.133999141842399361e+00), cos(3.133999141842399361e+00),
                d->tkq[26]);
    d->octant[26] = 5;
    d->sin_az[26] = sin(3.133999141842399361e+00);
    d->cos_az[26] = cos(3.133999141842399361e+00);
    d->dir[31].x  = -8.970451426192788125e-01;
    d->dir[31].y  = -6.811853755980193902e-03;
    d->dir[31].z  = -4.418864229092867579e-01;
    d->weight[31] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.028496782849331215e+00), cos(2.028496782849331215e+00),
                sin(3.149186165337186871e+00), cos(3.149186165337186871e+00),
                d->tkq[31]);
    d->octant[31] = 6;
    d->sin_az[31] = sin(3.149186165337186871e+00);
    d->cos_az[31] = cos(3.149186165337186871e+00);
    d->dir[36].x  = 8.970451426192788125e-01;
    d->dir[36].y  = -6.811853755980193902e-03;
    d->dir[36].z  = -4.418864229092867579e-01;
    d->weight[36] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.028496782849331215e+00), cos(2.028496782849331215e+00),
                sin(6.275591795432192477e+00), cos(6.275591795432192477e+00),
                d->tkq[36]);
    d->octant[36] = 7;
    d->sin_az[36] = sin(6.275591795432192477e+00);
    d->cos_az[36] = cos(6.275591795432192477e+00);
    d->dir[2].x   = 1.912901088399358573e-01;
    d->dir[2].y   = 1.904737296128635737e-01;
    d->dir[2].z   = 9.628747855185383342e-01;
    d->weight[2]  = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.733394583744201412e-01), cos(2.733394583744201412e-01),
                sin(7.832597262265261762e-01), cos(7.832597262265261762e-01),
                d->tkq[2]);
    d->octant[2] = 0;
    d->sin_az[2] = sin(7.832597262265261762e-01);
    d->cos_az[2] = cos(7.832597262265261762e-01);
    d->dir[7].x  = -1.912901088399358573e-01;
    d->dir[7].y  = 1.904737296128635737e-01;
    d->dir[7].z  = 9.628747855185383342e-01;
    d->weight[7] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.733394583744201412e-01), cos(2.733394583744201412e-01),
                sin(2.358332927363266940e+00), cos(2.358332927363266940e+00),
                d->tkq[7]);
    d->octant[7]  = 1;
    d->sin_az[7]  = sin(2.358332927363266940e+00);
    d->cos_az[7]  = cos(2.358332927363266940e+00);
    d->dir[12].x  = -1.912901088399358573e-01;
    d->dir[12].y  = -1.904737296128635737e-01;
    d->dir[12].z  = 9.628747855185383342e-01;
    d->weight[12] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.733394583744201412e-01), cos(2.733394583744201412e-01),
                sin(3.924852379816319292e+00), cos(3.924852379816319292e+00),
                d->tkq[12]);
    d->octant[12] = 2;
    d->sin_az[12] = sin(3.924852379816319292e+00);
    d->cos_az[12] = cos(3.924852379816319292e+00);
    d->dir[17].x  = 1.912901088399358573e-01;
    d->dir[17].y  = -1.904737296128635737e-01;
    d->dir[17].z  = 9.628747855185383342e-01;
    d->weight[17] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.733394583744201412e-01), cos(2.733394583744201412e-01),
                sin(5.499925580953060056e+00), cos(5.499925580953060056e+00),
                d->tkq[17]);
    d->octant[17] = 3;
    d->sin_az[17] = sin(5.499925580953060056e+00);
    d->cos_az[17] = cos(5.499925580953060056e+00);
    d->dir[22].x  = 1.912901088399358573e-01;
    d->dir[22].y  = 1.904737296128635737e-01;
    d->dir[22].z  = -9.628747855185383342e-01;
    d->weight[22] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.868253195215372919e+00), cos(2.868253195215372919e+00),
                sin(7.832597262265261762e-01), cos(7.832597262265261762e-01),
                d->tkq[22]);
    d->octant[2]  = 4;
    d->sin_az[2]  = sin(7.832597262265261762e-01);
    d->cos_az[2]  = cos(7.832597262265261762e-01);
    d->dir[27].x  = -1.912901088399358573e-01;
    d->dir[27].y  = 1.904737296128635737e-01;
    d->dir[27].z  = -9.628747855185383342e-01;
    d->weight[27] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.868253195215372919e+00), cos(2.868253195215372919e+00),
                sin(2.358332927363266940e+00), cos(2.358332927363266940e+00),
                d->tkq[27]);
    d->octant[27] = 5;
    d->sin_az[27] = sin(2.358332927363266940e+00);
    d->cos_az[27] = cos(2.358332927363266940e+00);
    d->dir[32].x  = -1.912901088399358573e-01;
    d->dir[32].y  = -1.904737296128635737e-01;
    d->dir[32].z  = -9.628747855185383342e-01;
    d->weight[32] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.868253195215372919e+00), cos(2.868253195215372919e+00),
                sin(3.924852379816319292e+00), cos(3.924852379816319292e+00),
                d->tkq[32]);
    d->octant[32] = 6;
    d->sin_az[32] = sin(3.924852379816319292e+00);
    d->cos_az[32] = cos(3.924852379816319292e+00);
    d->dir[37].x  = 1.912901088399358573e-01;
    d->dir[37].y  = -1.904737296128635737e-01;
    d->dir[37].z  = -9.628747855185383342e-01;
    d->weight[37] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.868253195215372919e+00), cos(2.868253195215372919e+00),
                sin(5.499925580953060056e+00), cos(5.499925580953060056e+00),
                d->tkq[37]);
    d->octant[37] = 7;
    d->sin_az[37] = sin(5.499925580953060056e+00);
    d->cos_az[37] = cos(5.499925580953060056e+00);
    d->dir[3].x   = 5.131248065222138166e-01;
    d->dir[3].y   = 7.741200428928012300e-01;
    d->dir[3].z   = 3.707304844805565658e-01;
    d->weight[3]  = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.191000897061868180e+00), cos(1.191000897061868180e+00),
                sin(9.854412833358154877e-01), cos(9.854412833358154877e-01),
                d->tkq[3]);
    d->octant[3] = 0;
    d->sin_az[3] = sin(9.854412833358154877e-01);
    d->cos_az[3] = cos(9.854412833358154877e-01);
    d->dir[8].x  = -5.131248065222138166e-01;
    d->dir[8].y  = 7.741200428928012300e-01;
    d->dir[8].z  = 3.707304844805565658e-01;
    d->weight[8] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.191000897061868180e+00), cos(1.191000897061868180e+00),
                sin(2.156151370253977628e+00), cos(2.156151370253977628e+00),
                d->tkq[8]);
    d->octant[8]  = 1;
    d->sin_az[8]  = sin(2.156151370253977628e+00);
    d->cos_az[8]  = cos(2.156151370253977628e+00);
    d->dir[13].x  = -5.131248065222138166e-01;
    d->dir[13].y  = -7.741200428928012300e-01;
    d->dir[13].z  = 3.707304844805565658e-01;
    d->weight[13] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.191000897061868180e+00), cos(1.191000897061868180e+00),
                sin(4.127033936925608160e+00), cos(4.127033936925608160e+00),
                d->tkq[13]);
    d->octant[13] = 2;
    d->sin_az[13] = sin(4.127033936925608160e+00);
    d->cos_az[13] = cos(4.127033936925608160e+00);
    d->dir[18].x  = 5.131248065222138166e-01;
    d->dir[18].y  = -7.741200428928012300e-01;
    d->dir[18].z  = 3.707304844805565658e-01;
    d->weight[18] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.191000897061868180e+00), cos(1.191000897061868180e+00),
                sin(5.297744023843771188e+00), cos(5.297744023843771188e+00),
                d->tkq[18]);
    d->octant[18] = 3;
    d->sin_az[18] = sin(5.297744023843771188e+00);
    d->cos_az[18] = cos(5.297744023843771188e+00);
    d->dir[23].x  = 5.131248065222138166e-01;
    d->dir[23].y  = 7.741200428928012300e-01;
    d->dir[23].z  = -3.707304844805565658e-01;
    d->weight[23] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.950591756527924936e+00), cos(1.950591756527924936e+00),
                sin(9.854412833358154877e-01), cos(9.854412833358154877e-01),
                d->tkq[23]);
    d->octant[3]  = 4;
    d->sin_az[3]  = sin(9.854412833358154877e-01);
    d->cos_az[3]  = cos(9.854412833358154877e-01);
    d->dir[28].x  = -5.131248065222138166e-01;
    d->dir[28].y  = 7.741200428928012300e-01;
    d->dir[28].z  = -3.707304844805565658e-01;
    d->weight[28] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.950591756527924936e+00), cos(1.950591756527924936e+00),
                sin(2.156151370253977628e+00), cos(2.156151370253977628e+00),
                d->tkq[28]);
    d->octant[28] = 5;
    d->sin_az[28] = sin(2.156151370253977628e+00);
    d->cos_az[28] = cos(2.156151370253977628e+00);
    d->dir[33].x  = -5.131248065222138166e-01;
    d->dir[33].y  = -7.741200428928012300e-01;
    d->dir[33].z  = -3.707304844805565658e-01;
    d->weight[33] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.950591756527924936e+00), cos(1.950591756527924936e+00),
                sin(4.127033936925608160e+00), cos(4.127033936925608160e+00),
                d->tkq[33]);
    d->octant[33] = 6;
    d->sin_az[33] = sin(4.127033936925608160e+00);
    d->cos_az[33] = cos(4.127033936925608160e+00);
    d->dir[38].x  = 5.131248065222138166e-01;
    d->dir[38].y  = -7.741200428928012300e-01;
    d->dir[38].z  = -3.707304844805565658e-01;
    d->weight[38] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.950591756527924936e+00), cos(1.950591756527924936e+00),
                sin(5.297744023843771188e+00), cos(5.297744023843771188e+00),
                d->tkq[38]);
    d->octant[38] = 7;
    d->sin_az[38] = sin(5.297744023843771188e+00);
    d->cos_az[38] = cos(5.297744023843771188e+00);
    d->dir[4].x   = 1.795629712085039797e-01;
    d->dir[4].y   = 8.647785187398833173e-01;
    d->dir[4].z   = 4.689512265650099265e-01;
    d->weight[4]  = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.082693361189593029e+00), cos(1.082693361189593029e+00),
                sin(1.366065123405460158e+00), cos(1.366065123405460158e+00),
                d->tkq[4]);
    d->octant[4] = 0;
    d->sin_az[4] = sin(1.366065123405460158e+00);
    d->cos_az[4] = cos(1.366065123405460158e+00);
    d->dir[9].x  = -1.795629712085039797e-01;
    d->dir[9].y  = 8.647785187398833173e-01;
    d->dir[9].z  = 4.689512265650099265e-01;
    d->weight[9] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.082693361189593029e+00), cos(1.082693361189593029e+00),
                sin(1.775527530184332958e+00), cos(1.775527530184332958e+00),
                d->tkq[9]);
    d->octant[9]  = 1;
    d->sin_az[9]  = sin(1.775527530184332958e+00);
    d->cos_az[9]  = cos(1.775527530184332958e+00);
    d->dir[14].x  = -1.795629712085039797e-01;
    d->dir[14].y  = -8.647785187398833173e-01;
    d->dir[14].z  = 4.689512265650099265e-01;
    d->weight[14] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.082693361189593029e+00), cos(1.082693361189593029e+00),
                sin(4.507657776995253052e+00), cos(4.507657776995253052e+00),
                d->tkq[14]);
    d->octant[14] = 2;
    d->sin_az[14] = sin(4.507657776995253052e+00);
    d->cos_az[14] = cos(4.507657776995253052e+00);
    d->dir[19].x  = 1.795629712085039797e-01;
    d->dir[19].y  = -8.647785187398833173e-01;
    d->dir[19].z  = 4.689512265650099265e-01;
    d->weight[19] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(1.082693361189593029e+00), cos(1.082693361189593029e+00),
                sin(4.917120183774126296e+00), cos(4.917120183774126296e+00),
                d->tkq[19]);
    d->octant[19] = 3;
    d->sin_az[19] = sin(4.917120183774126296e+00);
    d->cos_az[19] = cos(4.917120183774126296e+00);
    d->dir[24].x  = 1.795629712085039797e-01;
    d->dir[24].y  = 8.647785187398833173e-01;
    d->dir[24].z  = -4.689512265650099265e-01;
    d->weight[24] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.058899292400200309e+00), cos(2.058899292400200309e+00),
                sin(1.366065123405460158e+00), cos(1.366065123405460158e+00),
                d->tkq[24]);
    d->octant[4]  = 4;
    d->sin_az[4]  = sin(1.366065123405460158e+00);
    d->cos_az[4]  = cos(1.366065123405460158e+00);
    d->dir[29].x  = -1.795629712085039797e-01;
    d->dir[29].y  = 8.647785187398833173e-01;
    d->dir[29].z  = -4.689512265650099265e-01;
    d->weight[29] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.058899292400200309e+00), cos(2.058899292400200309e+00),
                sin(1.775527530184332958e+00), cos(1.775527530184332958e+00),
                d->tkq[29]);
    d->octant[29] = 5;
    d->sin_az[29] = sin(1.775527530184332958e+00);
    d->cos_az[29] = cos(1.775527530184332958e+00);
    d->dir[34].x  = -1.795629712085039797e-01;
    d->dir[34].y  = -8.647785187398833173e-01;
    d->dir[34].z  = -4.689512265650099265e-01;
    d->weight[34] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.058899292400200309e+00), cos(2.058899292400200309e+00),
                sin(4.507657776995253052e+00), cos(4.507657776995253052e+00),
                d->tkq[34]);
    d->octant[34] = 6;
    d->sin_az[34] = sin(4.507657776995253052e+00);
    d->cos_az[34] = cos(4.507657776995253052e+00);
    d->dir[39].x  = 1.795629712085039797e-01;
    d->dir[39].y  = -8.647785187398833173e-01;
    d->dir[39].z  = -4.689512265650099265e-01;
    d->weight[39] = 2.500000000000000139e-02;
    dir_CalcTKQ(sin(2.058899292400200309e+00), cos(2.058899292400200309e+00),
                sin(4.917120183774126296e+00), cos(4.917120183774126296e+00),
                d->tkq[39]);
    d->octant[39] = 7;
    d->sin_az[39] = sin(4.917120183774126296e+00);
    d->cos_az[39] = cos(4.917120183774126296e+00);
}

/************************************************************************/
/************************************************************************/

t_complex **dir_GetDKRot(int k, double alpha, double beta, double gamma)
{
    t_complex **dkqq;
    int         ne, i, j, n, m;
    double      s, c;
#ifdef DEBUG
    if (k < 0)
        Error(E_ERROR, POR_AT, "rank cannot be nagetive: %d", k);
#endif

    ne   = 2 * k + 1;
    dkqq = (t_complex **)Malloc(sizeof(t_complex *) * ne);
    for (i = 0; i < ne; i++) {
        dkqq[i] = (t_complex *)Malloc(sizeof(t_complex) * ne);
        for (j = 0; j < ne; j++) {
            dkqq[i][j].re = dkqq[i][j].im = 0.0;
        }
    }
    s = sin(beta / 2.0);
    c = cos(beta / 2.0);
    for (m = -k; m <= k; m++) {
        for (n = -k; n <= k; n++) {
            t_complex ag, cc;
            int       t_min = MAX2(0, m - n), t_max = MIN2(k + m, k - n), t;
            double    sum = 0.0, dKMN;
            double    f1, f2, f3, f4;
            cc.re = 0.0;
            cc.im = -(alpha * m + gamma * n);
            CLX_EXP(ag, cc);
            for (t = t_min; t <= t_max; t++) {
                f1 = mat_Factorial(k + m - t);
                f2 = mat_Factorial(k - n - t);
                f3 = mat_Factorial(t);
                f4 = mat_Factorial(t + n - m);
                sum += mat_PowN(c, 2 * k + m - n - 2 * t) *
                       mat_PowN(s, 2 * t - m + n) / (f1 * f2 * f3 * f4) *
                       (t % 2 == 0 ? 1 : -1);
            }
            f1   = mat_Factorial(k - m);
            f2   = mat_Factorial(k + n);
            f3   = mat_Factorial(k - n);
            f4   = mat_Factorial(k + m);
            dKMN = sum * sqrt(f4) * sqrt(f1) * sqrt(f2) * sqrt(f3);
            dkqq[m + k][n + k].re = ag.re * dKMN;
            dkqq[m + k][n + k].im = ag.im * dKMN;
        }
    }
    return dkqq;
}

/*************************************************************************/
/****   expressing Stokes parameters in a rotated 3D reference frame *****

Given a ray in the global reference frame K (quantization axis = the Z axis of
the grid) and a magnetic field vector B, we define a local reference frame K'
whose Z' axis is parallel to B. The coordinates of vectors in K are transformed
to K' by matrix : vector(K') = R . vector(K). Let B in K have an inclination
theta (measured from the positive Z axis) and azimuth chi (measured from
positive X). The K' system is created by (1) rotation of the frame by chi around
Z, so that the new X axis is aligned with the XY projection of B. (2) rotation
around the NEW Y axis (line of nodes) by theta. This corresponds to rotation by
Euler angles alpha=chi, beta=theta, gamma=0. The function StokesRotMatrix then
gives an angle by which the Stokes parameters, expressed in K', must be rotated
so that they are correctly expressed in the frame K.
*/

#define POLAR_ANGLE                                                            \
    (1.0e-8) /* angular deviation of two lines under which the lines are       \
                considered parallel */

/*
 * Constructs a 3x3 rotation matrix for calculation of coordinates of a vector
 * in a new reference frame K' defined by the magnetic field vector.
 * The new frame is defined by angles theta (measured from positive Z axis of
 * the K-frame) and chi (measured from positive X axis of K). Coordinates of the
 * vector in the new system: a_new = R . a_old
 *
 * Parameters: ct=cos(theta), st=sin(theta), cc=cos(chi), sc=sin(chi)
 */
static void RotMatrix(double st, double ct, double sc, double cc,
                      double R[3][3])
{
    R[0][0] = cc * ct;
    R[0][1] = ct * sc;
    R[0][2] = -st;
    R[1][0] = -sc;
    R[1][1] = cc;
    R[1][2] = 0.0;
    R[2][0] = cc * st;
    R[2][1] = sc * st;
    R[2][2] = ct;
}

/*************************************************************************/
t_vec_3d dir_Vector2BFrame(const t_vec_3d v, const t_vec_3d B)
{
    double   theta, chi, f, R[3][3];
    t_vec_3d b, v1;
    b.x = B.x;
    b.y = B.y;
    b.z = B.z;
    f   = sqrt(b.x * b.x + b.y * b.y + b.z * b.z);
#ifdef DEBUG
    if (f == 0.0)
        Error(E_ERROR, POR_AT, "The B-vector cannot be of zero length.");
#endif
    f = 1.0 / f;
    b.x *= f;
    b.y *= f;
    b.z *= f;
    VecUnit2Ang(b, &theta, &chi);
    /* todo: optimize this function */
    RotMatrix(sin(theta), cos(theta), sin(chi), cos(chi), R);
    v1.x = R[0][0] * v.x + R[0][1] * v.y + R[0][2] * v.z;
    v1.y = R[1][0] * v.x + R[1][1] * v.y + R[1][2] * v.z;
    v1.z = R[2][0] * v.x + R[2][1] * v.y + R[2][2] * v.z;
    return v1;
}

/*
 * angle alpha measured from the local reference direction of positive Q (vector
 * ea_l(K')) to the reference frame and the positive Q direction in the global
 * ref. frame K for a general ray direction defined by normalized vector (x,y,z)
 * in the global frame K. alpha is in the interval (-pi, pi)
 *
 * (x,y,z): ray propagation direction in K (x,y,z) must be normalized to 1!
 * R: rotation matrix for expressing vector coordinates in a the local reference
 * frame: vec(K') = R.vec(K)
 */
static double StokesAngle(double x, double y, double z, double R[3][3])
{
    double ea[3];      /* +Q reference vector in K */
    double ea1[3];     /* coordinates of ea expressed in K' */
    double ea_l[3];    /* local-frame K' +Q referenece direction */
    double Omega_l[3]; /* ray direction in K' : Omega_l = R.(x,y,z) */
    double alpha, prod;

    /* is the azimuth of the ray sufficiently well defined? (true for
     * non-vertical rays) */
    if (x * x + y * y >= POLAR_ANGLE * POLAR_ANGLE) {
        double norm = sqrt(x * x + y * y);
        double nx, ny;
        /* unit vector in the equatorial plane (with nz=0) pointing (in the xy
         * plane) counterclockwisely: */
        nx    = -y / norm;
        ny    = x / norm;
        ea[0] = -z * ny;
        ea[1] = z * nx;
        ea[2] = x * ny - y * nx;
    }
    /* the ray is practically vertical (going upwards) */
    else if (z > 0) {
        ea[0] = -1.0; /* -1 corresponds to azimuth 0 of the ray */
        ea[1] = ea[2] = 0.0;
    }
    /* the ray is practically vertical (going downwards) */
    else {
        ea[0] = 1.0;
        ea[1] = ea[2] = 0.0;
    }

    /* calculate the coordinates ea in the frame K': */
    ea1[0] = R[0][0] * ea[0] + R[0][1] * ea[1] + R[0][2] * ea[2];
    ea1[1] = R[1][0] * ea[0] + R[1][1] * ea[1] + R[1][2] * ea[2];
    ea1[2] = R[2][0] * ea[0] + R[2][1] * ea[1] + R[2][2] * ea[2];
    /* get the ray direction in the K' frame: */
    Omega_l[0] = R[0][0] * x + R[0][1] * y + R[0][2] * z;
    Omega_l[1] = R[1][0] * x + R[1][1] * y + R[1][2] * z;
    Omega_l[2] = R[2][0] * x + R[2][1] * y + R[2][2] * z;

    /* local-frame +Q direction: */
    if (Omega_l[0] * Omega_l[0] + Omega_l[1] * Omega_l[1] >
        POLAR_ANGLE * POLAR_ANGLE) {
        double theta = acos(Omega_l[2]), factor = 1.0 / tan(theta);
        ea_l[0] = -Omega_l[0] * factor;
        ea_l[1] = -Omega_l[1] * factor;
        ea_l[2] = sqrt(1.0 - ea_l[0] * ea_l[0] -
                       ea_l[1] * ea_l[1]); /* always positive */
    } else if (Omega_l[2] > 0) {
        ea_l[0] = -1.0; /* assume zero azimuth of the ray */
        ea_l[1] = ea_l[2] = 0.0;
    } else {
        ea_l[0] = 1.0;
        ea_l[1] = ea_l[2] = 0.0;
    }

    /* vector product ea_l x ea1 produces unit vector parallel (alpha>0) or
     * antiparallel (alpha<0) to Omega_l: */
    ea[0] = ea_l[1] * ea1[2] - ea_l[2] * ea1[1];
    ea[1] = ea_l[2] * ea1[0] - ea_l[0] * ea1[2];
    ea[2] = ea_l[0] * ea1[1] - ea_l[1] * ea1[0];
    prod  = ea_l[0] * ea1[0] + ea_l[1] * ea1[1] + ea_l[2] * ea1[2];
    if (prod > 1.0)
        prod = 1.0;
    else if (prod < -1.0)
        prod = -1.0;    /* this allows to avoid small numerical errors */
    alpha = acos(prod); /* angle between ea_l and ea1 in the interval (0,pi) */
    /* parallel? */
    if (ea[0] * Omega_l[0] + ea[1] * Omega_l[1] + ea[2] * Omega_l[2] >= 0.0)
        return alpha;
    /* anti-parallel? */
    else
        return -alpha;
}

/*
 * calculate the transformation matrix of Stokes Q and U from local frame
 * to Stokes prameters to the global ref. frame.
 *
 * dir: unit vector of direction of ray propagation in the global ref. frame K
 * B: magnetic field vector that defines the local ref. frame K' (B is parallel
 * to Z') A[2][2]: matrix transforming A.Stokes(local) -> Stokes(global); only
 * affects the (Q,U) vector
 */
void dir_StokesRotMatrix(const t_vec_3d dir, const t_vec_3d B, double *cos2a,
                         double *sin2a)
{
    double R[3][3] = {
        {1, 0, 0},
        {0, 1, 0},
        {0, 0, 1}}; /* transformation of vector(K) -> vector(K') */
    double bx = B.x, by = B.y, bz = B.z;
    double b = sqrt(bx * bx + by * by + bz * bz);
    double alpha, s2a, c2a;

    if (bx != 0.0 || by != 0.0) {
        double st, ct, sc, cc;
        double theta, chi, norm;
        ct    = bz / b;
        theta = acos(ct);
        st    = sin(theta);
        norm  = sqrt(bx * bx + by * by);
        bx    = bx / norm;
        by    = by / norm;
        if (by >= 0.0)
            chi = acos(bx);
        else
            chi = 2.0 * M_PI - acos(bx);
        sc = by;
        cc = bx;
        RotMatrix(st, ct, sc, cc, R);
        alpha  = StokesAngle(dir.x, dir.y, dir.z, R);
        s2a    = sin(2.0 * alpha);
        c2a    = cos(2.0 * alpha);
        *cos2a = c2a;
        *sin2a = s2a;
    } else if (bz >= 0.0) {
        /* K' = K */
        *cos2a = 1.0;
        *sin2a = 0.0;
    } else /* bx=by=0 and bz<0 */
    {
        RotMatrix(0.0, -1.0, 0.0, 1.0, R);
        alpha  = StokesAngle(dir.x, dir.y, dir.z, R);
        s2a    = sin(2.0 * alpha);
        c2a    = cos(2.0 * alpha);
        *cos2a = c2a;
        *sin2a = s2a;
    }
}

#undef POLAR_ANGLE

/****************************************************************************/
