#include "voigt.h"
#include "error.h"
#include "matika.h"
#include "mem.h"

t_complex vgt_Lorentz(double nu0, double nu, double Gamma)
{
    t_complex     p;
    static double piinv = 1.0 / M_PI;
    p.re = piinv * Gamma / ((nu0 - nu) * (nu0 - nu) + Gamma * Gamma);
    p.im = piinv * (nu0 - nu) / ((nu0 - nu) * (nu0 - nu) + Gamma * Gamma);
    return p;
}

static t_complex Approx1(t_complex t)
{
    t_complex c1, c2, c3;
    c1.re = 0.5;
    c1.im = 0.0;
    CLX_MUL(c2, t, t);
    CLX_ADD(c3, c1, c2);
    c1.re = t.re * 0.5641896;
    c1.im = t.im * 0.5641896;
    CLX_DIV(c2, c1, c3);
    return c2;
}

static t_complex Approx2(t_complex t, t_complex u)
{
    t_complex c1, c2, c3, c4;
    c1.re = 1.410474;
    c1.im = 0.0;
    c2.re = u.re * 0.5641896;
    c2.im = u.im * 0.5641896;
    CLX_ADD(c3, c1, c2);
    CLX_MUL(c1, t, c3);
    c2.re = 0.75;
    c2.im = 0.0;
    c4.re = 3.0;
    c4.im = 0.0;
    CLX_ADD(c3, c4, u);
    CLX_MUL(c4, u, c3);
    CLX_ADD(c3, c2, c4);
    CLX_DIV(c2, c1, c3);
    return c2;
}

static t_complex Approx3(t_complex t)
{
    t_complex c1, c2, c3, c4;
    c1.re = t.re * 0.5642236;
    c1.im = t.im * 0.5642236;
    c2.re = 3.778987;
    c2.im = 0.0;
    CLX_ADD(c3, c2, c1);
    CLX_MUL(c1, t, c3);
    c2.re = 11.96482;
    c2.im = 0.0;
    CLX_ADD(c3, c2, c1);
    CLX_MUL(c2, t, c3);
    c1.re = 20.20933;
    c1.im = 0.0;
    CLX_ADD(c3, c1, c2);
    CLX_MUL(c1, t, c3);
    c2.re = 16.4955;
    c2.im = 0.0;
    CLX_ADD(c3, c2, c1);
    c1.re = 6.699398;
    c1.im = 0.0;
    CLX_ADD(c2, c1, t);
    CLX_MUL(c4, t, c2);
    c1.re = 21.69274;
    c1.im = 0.0;
    CLX_ADD(c2, c1, c4);
    CLX_MUL(c1, t, c2);
    c2.re = 39.27121;
    c2.im = 0.0;
    CLX_ADD(c4, c2, c1);
    CLX_MUL(c1, t, c4);
    c2.re = 38.82363;
    c2.im = 0.0;
    CLX_ADD(c4, c2, c1);
    CLX_MUL(c1, t, c4);
    c2.re = 16.4955;
    c2.im = 0.0;
    CLX_ADD(c4, c2, c1);
    CLX_DIV(c1, c3, c4);
    return c1;
}

static t_complex Approx4(t_complex t, t_complex u)
{
    t_complex c1, c2, c3, c4;
    c1.re = u.re * 0.56419;
    c1.im = u.im * 0.56419;
    c2.re = 1.320522;
    c2.im = 0.0;
    CLX_SUB(c3, c2, c1);
    CLX_MUL(c1, u, c3);
    c2.re = 35.7668;
    c2.im = 0.0;
    CLX_SUB(c3, c2, c1);
    CLX_MUL(c1, u, c3);
    c2.re = 219.031;
    c2.im = 0.0;
    CLX_SUB(c3, c2, c1);
    CLX_MUL(c1, u, c3);
    c2.re = 1540.787;
    c2.im = 0.0;
    CLX_SUB(c3, c2, c1);
    CLX_MUL(c1, u, c3);
    c2.re = 3321.99;
    c2.im = 0.0;
    CLX_SUB(c3, c2, c1);
    CLX_MUL(c1, u, c3);
    c2.re = 36183.31;
    c2.im = 0.0;
    CLX_SUB(c3, c2, c1);
    CLX_MUL(c1, t, c3);
    c2.re = 1.84144;
    c2.im = 0.0;
    CLX_SUB(c3, c2, u);
    CLX_MUL(c2, u, c3);
    c3.re = 61.5704;
    c3.im = 0.0;
    CLX_SUB(c4, c3, c2);
    CLX_MUL(c2, u, c4);
    c3.re = 364.219;
    c3.im = 0.0;
    CLX_SUB(c4, c3, c2);
    CLX_MUL(c2, u, c4);
    c3.re = 2186.18;
    c3.im = 0.0;
    CLX_SUB(c4, c3, c2);
    CLX_MUL(c2, u, c4);
    c3.re = 9022.23;
    c3.im = 0.0;
    CLX_SUB(c4, c3, c2);
    CLX_MUL(c2, u, c4);
    c3.re = 24322.8;
    c3.im = 0.0;
    CLX_SUB(c4, c3, c2);
    CLX_MUL(c2, u, c4);
    c3.re = 32066.6;
    c3.im = 0.0;
    CLX_SUB(c4, c3, c2);
    CLX_DIV(c2, c1, c4);
    return c2;
}

/* complex complementary error function (W(v + i a) = H(v,a) + i L(v,a)
 *
 * (Abramowitz and Stegun, 1965) by optimized Humlicek w4 algorithm
 *
 * (J. Humlicek, JQSRT 27, 437, 1982).
 *
 * input:
 * n: number of discrete points
 * a: the "natural width" (\a a==0: gaussian)
 * x: array of x-coordinates
 *
 * output:
 * y: complex W function
 */
static void Humli(int n, double a, double *x, t_complex *y)
{
    double    s;
    t_complex t, u;
    int       i;
    int       maxn = n - 1;
#if defined(DEBUG)
    if (a < 0.0)
        Error(E_ERROR, POR_AT, "invalid parameter a=%e", a);
    if (n <= 0)
        Error(E_ERROR, POR_AT, "invalid array length n=%d", n);
    if (!x || !y)
        Error(E_ERROR, POR_AT, "array not allocated");
#endif
    if (a > 15.0) {
        for (i = 0; i <= maxn; i++) {
            t.re = a;
            t.im = -x[i];
            y[i] = Approx1(t);
        }
    } else if ((a < 15.0) && (a > 5.5)) {
        for (i = 0; i <= maxn; i++) {
            t.re = a;
            t.im = -x[i];
            s    = fabs(x[i]) + a;
            if (s >= 15.0)
                y[i] = Approx1(t);
            else {
                CLX_MUL(u, t, t);
                y[i] = Approx2(t, u);
            }
        }
    } else if ((a < 5.5) && (a > 0.75)) {
        for (i = 0; i <= maxn; i++) {
            t.re = a;
            t.im = -x[i];
            s    = fabs(x[i]) + a;
            if (s >= 15.0)
                y[i] = Approx1(t);
            else if (s < 5.5)
                y[i] = Approx3(t);
            else {
                CLX_MUL(u, t, t);
                y[i] = Approx2(t, u);
            }
        }
    } else {
        for (i = 0; i <= maxn; i++) {
            double ax = fabs(x[i]);
            t.re      = a;
            t.im      = -x[i];
            s         = ax + a;
            if (s >= 15.0)
                y[i] = Approx1(t);
            else if ((s < 15.0) && (s >= 5.5)) {
                CLX_MUL(u, t, t);
                y[i] = Approx2(t, u);
            } else if ((s < 5.5) && (a >= (0.195 * ax - 0.176)))
                y[i] = Approx3(t);
            else {
                t_complex c1, c2;
                CLX_MUL(u, t, t);
                c1 = Approx4(t, u);
                CLX_EXP(c2, u);
                CLX_SUB(y[i], c2, c1);
            }
        }
    }
    if (a == 0.0) {
        for (i = 0; i <= maxn; i++)
            y[i].re = exp(-x[i] * x[i]);
    }
}

/*
 * version which does not change n_max; it can lead to too crowdy data
 * near the pass from line core to wing
 */
double *vgt_SuggestLineFrequencies(int n, double nu0, double nu_d, double hw)
{
    double *x, chw, f, *wt, dc;
    int     c, i, ncore, nwing;
#if defined(DEBUG)
    if (n < 1)
        Error(E_ERROR, POR_AT, "invalid number of frequencies n=%d", n);
    if (n % 2 == 0)
        Error(E_ERROR, POR_AT, "number of wavelengths must be odd");
    /*
    if (nu0<=0)
        Error(E_ERROR, POR_AT, "invalid central frequency nu0=%e", nu0);
    */
    if (nu_d <= 0)
        Error(E_ERROR, POR_AT, "invalid Doppler width nu_d=%e", nu_d);
    if (hw <= 0)
        Error(E_ERROR, POR_AT, "invalid line half-width hw=%e", hw);
    /*
    if (nu0-hw<=0)
        Error(E_ERROR, POR_AT, "line is too broad: nu0=%e, hw=%e",
                nu0, hw);
    */
    if (nu_d / 2.0 >= hw)
        Error(E_ERROR, POR_AT,
              "line is too narrow: nu_d/2="
              "%e, line half-width=%e",
              nu_d / 2.0, hw);
    if (n == 3)
        Error(E_WARNING, POR_AT,
              "very small number of wavelengths: "
              "line profile will probably not be approximated correctly");
#endif
    x    = ArrayNew(n);
    c    = n / 2;
    x[c] = nu0;
    if (n == 1)
        return x;
    ncore = (int)(PROFILE_COREWING_RATE * c);
    nwing = c - ncore;
#if defined(DEBUG)
    if (!nwing)
        Error(E_WARNING, POR_AT,
              "there are no wing wavelengths; try to "
              "decrease PROFILE_COREWING_RATE (currently %e)",
              PROFILE_COREWING_RATE);
#endif
    chw = LINE_CORE_HALFWIDTH * nu_d;
    dc  = chw / ncore;
    for (i = 0; i < ncore; i++) {
        double dwl   = dc * (i + 1);
        x[c + i + 1] = nu0 + dwl;
        x[c - i - 1] = nu0 - dwl;
    }
    f     = LINE_WING_STEPF;
    wt    = ArrayNew(nwing);
    wt[0] = f;
    for (i = 1; i < nwing; i++) {
        wt[i] = wt[i - 1] + f;
        f *= LINE_WING_STEPF;
    }
    f = (hw - chw) / wt[nwing - 1];
    for (i = 0; i < nwing; i++)
        wt[i] *= f;
    if (wt[0] < dc) {
        wt[0] = dc;
        i     = 1;
        while (i < nwing && wt[i] - wt[i - 1] < dc) {
            wt[i] = wt[i - 1] + dc;
            ++i;
        }
        if (i == nwing)
            Error(E_ERROR, POR_AT, "too narrow wings");
    }
    for (i = 0; i < nwing; i++) {
        x[c + ncore + i + 1] = x[c + ncore] + wt[i];
        x[c - ncore - i - 1] = x[c - ncore] - wt[i];
    }
    free(wt);
    return x;
}

/* returns a value of the Voigt + dispersion profile at one frequency
 * nu0: line-center frequency
 * nu: frequency at which the profile is evaluated
 * nuD: Doppler with
 * a: Voigt parameter
 */
t_complex vgt_VoigtProfileNu(double nu0, double nu, double nuD, double a)
{
    double    v = (nu0 - nu) / nuD;
    double    fac;
    t_complex p;
    if (nuD > 0) {
        Humli(1, a, &v, &p);
        fac = 0.5641895835477563 / nuD;
        p.re *= fac;
        p.im *= fac;
    } else {
        Error(E_ERROR, POR_AT, "Pozor, jdou k vam kuratka (nuD=%e)!", nuD);
    }
    return p;
}

t_absprofile *vgt_VoigtProfile(int n, const double *freqs, double nu0,
                               double nu_d, double a, int dispersion_on)
{
    double *      v, *re, *im, norm = 1.0;
    t_complex *   pr;
    int           i;
    t_absprofile *p;
#ifdef DEBUG
    if (nu_d <= 0.0)
        Error(E_ERROR, POR_AT, "invalid Doppler width %e Hz", nu_d);
    if (a < 0.0)
        Error(E_ERROR, POR_AT, "invalid a-parameter %e", a);
    if (!freqs)
        Error(E_ERROR, POR_AT, "unallocated frequencies");
    if (n <= 0)
        Error(E_ERROR, POR_AT, "invalid number of frequencies");
    if (nu0 < freqs[0] || nu0 > freqs[n - 1])
        Error(E_ERROR, POR_AT, "central frequency %e out of range [%e, %e]",
              nu0, freqs[0], freqs[n - 1]);
#endif
    v  = ArrayNew(n);
    pr = (t_complex *)Malloc(sizeof(t_complex) * n);
    p  = (t_absprofile *)Malloc(sizeof(t_absprofile));
    for (i = 0; i < n; i++)
        v[i] = (nu0 - freqs[i]) / nu_d;
    Humli(n, a, v, pr);
    re = ArrayNew(n);
    im = ArrayNew(n);
    for (i = 0; i < n; i++) {
        re[i] = pr[i].re;
        im[i] = pr[i].im;
    };
    p->phi = re;
    p->psi = im;
    if (n > 1) {
        norm = Integrate(0, n, freqs, p->phi, Q_TRAPEZOIDAL, 0, NULL);
        /* TODO: this may warn about insufficiently sampled profiles */
    } else {
        norm      = 1.0;
        p->phi[0] = 1.0;
        p->psi[0] = 0.0;
    }
    ArrayMultD(n, p->phi, 1.0 / norm);
    if (dispersion_on) {
        ArrayMultD(n, p->psi, 1.0 / norm);
    } else {
        free(p->psi);
        p->psi = NULL;
    }
    free(v);
    free(pr);
    return p;
}
