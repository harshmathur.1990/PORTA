#ifndef IO_H_
#define IO_H_

#include "def.h"

#define IO_PMD_VERSION    2 /* the implemented version of the pmd file format */
#define PMD_HEADER_LEN    201844     /* length of the PMD main header */
#define IO_PMD_MAGIC_LEN  8          /* length of the .pmd magic string */
#define IO_PMD_MAGIC      "portapmd" /* magic string of the PMD file */
#define IO_PMD_MODULE_LEN 1023       /* length of the module name */
#define PMD_COMMENT_LEN   4096       /* PMD file header comment length */

#define IO_PSP_MAGIC_LEN 3
#define IO_PSP_MAGIC     "psp"
#define IO_PSP_VERSION   1
#define IO_PSP_COMMENT_LEN                                                     \
    1023 /* length of the comment in the porta spectrum (PSP) file header */

#define IO_TAU_MAGIC_LEN   3
#define IO_TAU_COMMENT_LEN 1023
#define IO_TAU_MAGIC       "tau"
#define IO_TAU_VERSION     1

/* creates a new stream for read&write containing "data" of length "size";
 * Internally: a temporary file containing the data;
 * You have to call pst_CloseStream() on this stream once you are done with it
 * in order to delete the temporary file;
 * In GNU C one should better use something like fmemopen() but that is not
 * portable...
 */
extern FILE *io_OpenTMPStream(int size, char *data);

/* complementary to pst_NewStreamR(): closes stream (and deletes the temporary
 * file) */
extern int io_CloseTMPStream(FILE *stream);

/* (master) save the current grid on dist into the PMD file with name fname
 * return value: 0 if error has occurred, non-zero otherwise. pmd and hdf5
 * versions */
extern int io_SaveGridMaster(char *fname);

extern int io_SaveGridMaster_h5(char *fname);

extern int (*io_SaveGridMaster_fp)(char *);

/* (slave) complementary to io_SaveGridMaster(); sends the subdomain nodes' data
   to master for saving. pmd and hdf5 versions */
extern int io_SaveGridSlave(void);

extern int io_SaveGridSlave_h5(void);

extern int (*io_SaveGridSlave_fp)(void);

/* master node: load grid from a pmd file fname;
 * return value: non-zero on success. pmd and hdf5 versions */
extern int io_LoadGridMaster(char *fname);

/* HDF5 version */
extern int io_LoadGridMaster_h5(char *fname);

/* Pointer function to either: io_LoadGridMaster or io_LoadGridMaster_h5 */
extern int (*io_LoadGridMaster_fp)(char *);

/* slave node: load grid
 * at the end of loading, pro_CollectOverlappingDM() is called to synchronize
 * the overlapping z-planes return value: non-zero on success. pmd and hdf5
 * versions */
extern int io_LoadGridSlave(void);

/* HDF5 version */
extern int io_LoadGridSlave_h5(void);

/* Pointer function to either: io_LoadGridSlave or io_LoadGridSlave_h5 */
extern int (*io_LoadGridSlave_fp)(void);

#endif /* IO_H_ */
