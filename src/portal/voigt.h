/* Voigt and Faraday-Voigt profiles */

#ifndef VOIGT_H_
#define VOIGT_H_

#include "def.h"

/* complex spectral line absorption profile */
typedef struct {
    double *phi; /* real part of the profile in frequencies */
    double *psi; /* imaginary part of the profile (may be NULL if dispersion
                    profiles are ignored) */
} t_absprofile;

/* Lorentzian profile */
extern t_complex vgt_Lorentz(double nu0, double nu, double Gamma);

/* allocates array of suitable frequencies for given line
 * central frequency nu0, Doppler width nu_d,
 * half-width of the line hw, number of frequency points n
 */
extern double *vgt_SuggestLineFrequencies(int n, double nu0, double nu_d,
                                          double hw);

/* returns a value of the Voigt + dispersion profile at one frequency
 * nu0: line-center frequency
 * nu: frequency at which the profile is evaluated
 * nuD: Doppler with
 * a: Voigt parameter
 */
extern t_complex vgt_VoigtProfileNu(double nu0, double nu, double nuD,
                                    double a);

/* generates Voigt profile for a prescribed set of frequencies
 *
 * n: number of frequency points
 * freqs: array with n elements containing frequencies
 * nu0: central line frequency
 * nu_d: Doppler width
 * a: Voigt profile parameter
 * dispersion_on: if 0, dispersion profile will not be allocated
 */
extern t_absprofile *vgt_VoigtProfile(int n, const double *freqs, double nu0,
                                      double nu_d, double a, int dispersion_on);

/* init a Voigt profiles pool for a given range of Doppler widths and natural
 * widths
 *
 * id_transition: arbitrary but unique number identifying a given profile: from
 * 0 to MAXN_PROFILES-1 you should call the function for every line transition
 */
extern void vgt_PoolBuild(int id_transition, int n, const double *freqs,
                          double nu0, double nu_d_min, double nu_d_max,
                          double a_min, double a_max, int dispersion_on);

/* release all the Voigt profiles in the pools */
extern void vgt_PoolClear(void);

/* get normalized absorption profile at a given frequency
 * prof[0] = phi, prof[1] = psi (zero if dispersion is neglected)
 * calc_nor: if non-zero, frequency integral over phi is calculated (slow!)
 * return value: INVERSE frequency integral over the phi profile
 * WARNING: ifreq is the frequency index WITHIN THE GIVEN VOIGT PROFILE, not the
 * global frequency index
 */
extern double vgt_PoolGetProfile(int id_transition, int ifreq, double nu_d,
                                 double a, double shift, double prof[2],
                                 int calc_norm);

/* returns the quadrature value at a given frequency
 * NOTE: ifreq is the index counted frem the first transition frequency, not the
 * glofal freq. index
 */
extern double vgt_PoolGetQuadrature(int id_transition, int ifreq);

#endif /* VOIGT_H_ */
