/* Jacobi iteration */

#include "error.h"
#include "ese.h"
#include "fs.h"
#include "grid.h"
#include "modules.h"
#include "portal.h"
#include "process.h"
#include "slv.h"
#include "stack.h"
#include <float.h>

/*******************************************************************************/
/* one Jacobi iteration in the grid g (formal solution + ESE solution (including
 * synchronization of the virtual layers of density matrices)) return value:
 * maximum relative change of populations in the whole grid
 */
static double JacobiIteration(t_grid *g)
{
    double rc;
    if (!G_MY_MPI_RANK)
        IERR;

    /* formal and ESE solution in my subdomain: */
    fs_ResetRadiationCounters(g);
    fs_FormalSolution(g);
    pro_CollectRadiationField(g);
    rc = ese_SolveGridPrecondESE(g);
    stk_Add(0, POR_AT,
            "maximum relative change in process %d (L=%d, M=%d) is %6.6e",
            G_MY_MPI_RANK, pro_Domain_Z_ID(G_MY_MPI_RANK), pro_MyFreqBand_ID(),
            rc);

    /* get the maximum relative change in the whole grid: */
    rc = pro_SynchronizeMaxQuantity(rc);

    /* synchronize the overlapping z-planes */
    pro_SynchronizeOverlappingDM(g);

    /* synchronize the DM in minz to the maxz in the lower z band */
    pro_Sync_DM(g);

    return rc;
}

/*******************************************************************************/
double slv_Jacobi(t_grid *p_grid, int max_it, double max_rc)
{
    int        i, j, nextit = 1;
    double     rc = 0.0;
    MPI_Status status;

    if (!G_MY_MPI_RANK)
        IERR;
    if (max_it < 1) {
        Error(E_ERROR, POR_AT, "invalid number of Jacobi iterations (%d)",
              max_it);
        return 0.0;
    }
    if (max_rc < 0) {
        Error(E_ERROR, POR_AT, "invalid maximum relative change (%6.6e)",
              max_rc);
        return 0.0;
    }

    stk_Add(0, POR_AT,
            "STARTING JACOBI ITERATION WITH %d ITERS. OR MAX. REL. CHANGE %e",
            max_it, max_rc);
    stk_Add(0, POR_AT, "STARTING TIME: %6.6e", LibRunningTime());
    if (IAMONE)
        PrintfErr(
            "STARTING JACOBI ITERATION WITH %d ITERS. AND MAX. REL. CHANGE "
            "%6.6e, T = %6.6e sec\n",
            max_it, max_rc, LibRunningTime());

    for (i = 0; i < max_it && nextit; i++) {
        stk_Add(0, POR_AT, "solving Jacobi iteration #%d", i + 1);
        rc = JacobiIteration(
            p_grid); /* max. relative change in the whole grid */
        /* decide whether to continue iterating: */
        if (G_MY_MPI_RANK > 1) {
            if (MPI_SUCCESS != MPI_Bcast(&nextit, 1, MPI_INT, 0, slaves_comm))
                CERR;
        } else /* if (G_MY_MPI_RANK==1) : process #1  (#0 in slaves_comm) is now
                  a "half-master" deciding to continue/finish iterations */
        {
            nextit = (rc <= max_rc ? 0 : 1);
            if (MPI_SUCCESS != MPI_Bcast(&nextit, 1, MPI_INT, 0, slaves_comm))
                CERR;
            stk_Add(0, POR_AT, "maximum relative change of populations: %6.6e",
                    rc);
        }
        stk_Add(0, POR_AT, "JACOBI ITERATION %4d FINISHED AT TIME: %6.6e",
                i + 1, LibRunningTime());
        if (IAMONE)
            PrintfErr("%4d\t%6.6e\t%6.6e\n", i + 1, LibRunningTime(), rc);
    }
    return rc;
}
