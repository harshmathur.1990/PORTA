/* definitions of the functions provided to the external modules */

#include "portal.h"
#include "def.h"
#include "error.h"
#include "global.h"
#include "mem.h"
#include "process.h"
#include "stack.h"
#include "topology.h"
#include <time.h>

extern t_global_vars *g_global;

int PorMeshInitialized(void) { return g_global->grid[0].node ? 1 : 0; }

void PorGetIDir(int iray, t_vec_3d *dir)
{
#ifdef DEBUG
    if (!dir)
        IERR;
    if (g_global->dirs.n_rays < 8)
        IERR;
    if (iray >= g_global->dirs.n_rays)
        IERR;
#endif
    dir->x = g_global->dirs.dir[iray].x;
    dir->y = g_global->dirs.dir[iray].y;
    dir->z = g_global->dirs.dir[iray].z;
}

void PorGetIGoni(int iray, double *mu, double *sin_az, double *cos_az)
{
#ifdef DEBUG
    if (!mu || !sin_az || !cos_az)
        IERR;
    if (g_global->dirs.n_rays < 8)
        IERR;
    if (iray >= g_global->dirs.n_rays)
        IERR;
#endif
    *mu     = g_global->dirs.dir[iray].z;
    *sin_az = g_global->dirs.sin_az[iray];
    *cos_az = g_global->dirs.cos_az[iray];
}

void PorGetTKQ(int iray, double tkq[NTKQ])
{
#ifdef DEBUG
    if (iray < 0 || iray >= g_global->dirs.n_rays) {
        Error(E_ERROR, POR_AT, "invalid ray index %d", iray);
    }
#endif
    Memcpy(tkq, &g_global->dirs.tkq[iray], DBLSIZE * NTKQ);
}

t_complex PorExtractTKQ(int istokes, int K, int Q, const double tkq[NTKQ])
{
    return dir_GetTKQElement(istokes, K, Q, tkq);
}

t_complex PorGetJKQ(int K, int Q, const t_jkq *jkq)
{
    t_complex jj = {0.0, 0.0};

    switch (K) {
    case 0:
        jj.re = (*jkq).jkq[J00_RE];
        break;
    case 1:
        switch (Q) {
        case -1:
            jj.re = -(*jkq).jkq[J11_RE];
            jj.im = (*jkq).jkq[J11_IM];
            break;
        case 0:
            jj.re = (*jkq).jkq[J10_RE];
            break;
        case 1:
            jj.re = (*jkq).jkq[J11_RE];
            jj.im = (*jkq).jkq[J11_IM];
            break;
        }
        break;
    case 2:
        switch (Q) {
        case -2:
            jj.re = (*jkq).jkq[J22_RE];
            jj.im = -(*jkq).jkq[J22_IM];
            break;
        case -1:
            jj.re = -(*jkq).jkq[J21_RE];
            jj.im = (*jkq).jkq[J21_IM];
            break;
        case 0:
            jj.re = (*jkq).jkq[J20_RE];
            break;
        case 1:
            jj.re = (*jkq).jkq[J21_RE];
            jj.im = (*jkq).jkq[J21_IM];
            break;
        case 2:
            jj.re = (*jkq).jkq[J22_RE];
            jj.im = (*jkq).jkq[J22_IM];
            break;
        }
        break;
    }
    return jj;
}

double PorAngQuadrature(int iray)
{
#ifdef DEBUG
    if (g_global->dirs.n_rays < 8)
        IERR;
    if (iray < 0 || iray >= g_global->dirs.n_rays)
        IERR;
#endif
    return g_global->dirs.weight[iray];
}

double PorDomainSizeX(void)
{
#ifdef DEBUG
    if (!g_global->current_n_grids)
        IERR;
#endif
    return g_global->domain_size.x;
}

double PorDomainSizeY(void)
{
#ifdef DEBUG
    if (!g_global->current_n_grids)
        IERR;
#endif
    return g_global->domain_size.y;
}

int PorNX(void) { return g_global->grid[0].nx; }

int PorNY(void) { return g_global->grid[0].ny; }

int PorNZ(void) { return g_global->grid[0].nz; }

double *PorGetXCoords(void) { return &g_global->grid[0].x[0]; }

double *PorGetYCoords(void) { return &g_global->grid[0].y[0]; }

double *PorGetZCoords(void) { return &g_global->grid[0].z[0]; }

void PorOrigin(t_vec_3d *r)
{
#ifdef DEBUG
    if (!g_global->current_n_grids)
        IERR;
    if (!r)
        IERR;
#endif
    *r = g_global->domain_origin;
}

int PorNFreqs(void) { return g_global->n_freq; }

int PorSetFreqs(int n, const double *freqs)
{
    stk_Add(1, POR_AT, "setting %d frequencies", n);
    if (n < 1 || n > MAXNFREQ) {
        Error(E_ERROR, POR_AT, "invalid number of frequencies (%d)", n);
        return 0;
    }
    if (!freqs) {
        Error(E_ERROR, POR_AT, "frequency array not initialized");
        return 0;
    }

    if (!Memcpy(&g_global->freq, freqs, n * DBLSIZE))
        return 0;
    g_global->n_freq = n;

    /* Make a shuffled list of the frequency indices. Used to balance the work
       done in every frequency band */
    if (!G_MY_MPI_RANK) {
        for (int f = 0; f < n; f++) {
            g_global->rfreq[f] = f;
        }

        int iswap, swap;
        for (int f = 0; f < n; f++) {
            iswap                  = rand() % n;
            swap                   = g_global->rfreq[iswap];
            g_global->rfreq[iswap] = g_global->rfreq[f];
            g_global->rfreq[f]     = swap;
        }
    }
    if (MPI_SUCCESS !=
        MPI_Bcast(&g_global->rfreq[0], n, MPI_INT, 0, MPI_COMM_WORLD))
        CERR;

    return 1;
}

const double *PorFreqs(void) { return &g_global->freq[0]; }

int PorNNodes(void)
{
    if (!PorMeshInitialized()) {
        Error(E_ERROR, POR_AT, "the grid is not properly initialized");
        return 0;
    }
    return g_global->grid[0].nx * g_global->grid[0].ny * g_global->grid[0].nz;
}

void PorResetAFunc(void)
{
    g_global->afunc.F_Init_p_data        = NULL;
    g_global->afunc.F_Free_p_data        = NULL;
    g_global->afunc.F_EncodeNodeData     = NULL;
    g_global->afunc.F_EncodeNodeData_h5  = NULL;
    g_global->afunc.F_DecodeNodeData     = NULL;
    g_global->afunc.F_DecodeNodeData_h5  = NULL;
    g_global->afunc.F_GetNodeDataSize    = NULL;
    g_global->afunc.F_ExternalIllum      = NULL;
    g_global->afunc.F_SKGeneralDirection = NULL;
    g_global->afunc.F_SK                 = NULL;
    g_global->afunc.F_AddFS              = NULL;
    g_global->afunc.F_GetESE             = NULL;
    g_global->afunc.F_SolveESE           = NULL;
    g_global->afunc.F_GetNDM             = NULL;
    g_global->afunc.F_GetPopsPositions   = NULL;
    g_global->afunc.F_GetDM              = NULL;
    g_global->afunc.F_SetDM              = NULL;
    g_global->afunc.F_GetTemperature     = NULL;
    g_global->afunc.F_GetMagneticVector  = NULL;
    g_global->afunc.F_GetRadiation       = NULL;
    g_global->afunc.F_SetRadiation       = NULL;
    g_global->afunc.F_ResetRadiation     = NULL;
    g_global->afunc.F_IO_WriteHeader     = NULL;
    g_global->afunc.F_IO_WriteHeader_h5  = NULL;
    g_global->afunc.F_SyncGlobals        = NULL;
    g_global->afunc.F_ESEBegins          = NULL;
    g_global->afunc.F_ESEEnds            = NULL;
}

int PorIsAFuncSet(void)
{
    if (!g_global->afunc.F_Init_p_data || !g_global->afunc.F_Free_p_data ||
        !g_global->afunc.F_EncodeNodeData ||
        !g_global->afunc.F_EncodeNodeData_h5 ||
        !g_global->afunc.F_DecodeNodeData ||
        !g_global->afunc.F_DecodeNodeData_h5 ||
        !g_global->afunc.F_GetNodeDataSize ||
        !g_global->afunc.F_ExternalIllum ||
        !g_global->afunc.F_SKGeneralDirection || !g_global->afunc.F_SK ||
        !g_global->afunc.F_AddFS || !g_global->afunc.F_GetESE ||
        !g_global->afunc.F_SolveESE || !g_global->afunc.F_GetNDM ||
        !g_global->afunc.F_GetPopsPositions || !g_global->afunc.F_GetDM ||
        !g_global->afunc.F_SetDM || !g_global->afunc.F_GetTemperature ||
        !g_global->afunc.F_GetMagneticVector ||
        !g_global->afunc.F_GetRadiation || !g_global->afunc.F_SetRadiation ||
        !g_global->afunc.F_ResetRadiation ||
        !g_global->afunc.F_IO_WriteHeader ||
        !g_global->afunc.F_IO_WriteHeader_h5 ||
        !g_global->afunc.F_SyncGlobals || !g_global->afunc.F_ESEBegins ||
        !g_global->afunc.F_ESEEnds) {
        return 0;
    } else {
        return 1;
    }
}

int PorGetProcessRank(void) { return G_MY_MPI_RANK; }

double PorGetDomainMax(double a)
{
    if (!G_MY_MPI_RANK)
        IERR;
    return pro_SynchronizeMaxQuantity(a);
}

void PorMinMaxIFreq(int *min_ifreq, int *max_ifreq)
{
    pro_MinMaxIFreqRank(G_MY_MPI_RANK, min_ifreq, max_ifreq);
}

t_vec_3d PorGetNodeCoords(t_node *p_node)
{
    t_vec_3d r;
    if (!p_node)
        IERR;
    r.x = p_node->p_grid->x[p_node->ix];
    r.y = p_node->p_grid->y[p_node->iy];
    r.z = p_node->p_grid->z[p_node->iz];
    return r;
}
