#include "physics.h"
#include <math.h>

double phy_PlanckFunction(double temp, double freq)
{
    return 2.0 * C_H * freq * freq * freq /
           (C_C * C_C * (exp(C_H * freq / (C_KB * temp)) - 1.0));
}

double phy_Larmor(double B) { return 1.3996e6 * B; }
