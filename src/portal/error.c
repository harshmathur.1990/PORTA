#include "error.h"
#include "def.h"
#include "global.h"
#include "process.h"
#include "stack.h"
#include "thread_io.h"
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* TODO: error messages should also be saved in the log file via stack */
void Error(enum t_error_level level, char *location, char *err, ...)
{
    va_list argptr;
    char    string[2048];
    /* change stdin to non blocking */
#ifdef FNDELAY
    fcntl(0, F_SETFL, fcntl(0, F_GETFL, 0) & ~FNDELAY);
#else
    fcntl(0, F_SETFL, fcntl(0, F_GETFL, 0) & ~O_NONBLOCK);
#endif
    va_start(argptr, err);
    vsprintf(string, err, argptr);
    va_end(argptr);
    switch (level) {
    case E_MESSAGE:
        if (location)
            fprintf(stderr, "[%d] MESSAGE(%s): ", G_MY_MPI_RANK, location);
        else
            fprintf(stderr, "MESSAGE(undefined location): ");
        fprintf(stderr, "%s\n", string);
        break;
    case E_WARNING:
        if (location)
            fprintf(stderr, "[%d] WARNING(%s): ", G_MY_MPI_RANK, location);
        else
            fprintf(stderr,
                    "[%d] WARNING(undefined location): ", G_MY_MPI_RANK);
        fprintf(stderr, "%s\n", string);
        break;
    case E_ERROR:
        if (location)
            fprintf(stderr, "[%d] ERROR(%s): ", G_MY_MPI_RANK, location);
        else
            fprintf(stderr, "[%d] ERROR(undefined location): ", G_MY_MPI_RANK);
        fprintf(stderr, "%s\n", string);
#if defined(STACK_ON)
        fprintf(stderr, "\n** [%d] stack dump:\n", G_MY_MPI_RANK);
        stk_Dump();
#endif
        fflush(stderr);
        /*MPI_Finalize();*/
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
        exit(EXIT_FAILURE);
        break;
    };
}

void Printf(char *fmt, ...)
{
    va_list        argptr;
    char           text[2048];
    unsigned char *p;
    va_start(argptr, fmt);
    vsprintf(text, fmt, argptr);
    va_end(argptr);
    if (strlen(text) > sizeof(text))
        Error(E_ERROR, POR_AT, "memory overwrite");
#ifdef THREAD_IO
    print_t0(io_comm, text, strlen(text) + 1,
             SERR_IO); // l+1 so we also send the null character
#else
    for (p = (unsigned char *)text; *p; p++)
        if ((*p > 128 || *p < 32) && *p != 10 && *p != 13 && *p != 9)
            printf("[%02x]", *p);
        else
            putc(*p, stdout);
#endif
}

void PrintfErr(char *fmt, ...)
{
    va_list        argptr;
    char           text[2048];
    unsigned char *p;
    va_start(argptr, fmt);
    vsprintf(text, fmt, argptr);
    va_end(argptr);
    if (strlen(text) > sizeof(text))
        Error(E_ERROR, POR_AT, "memory overwrite");
#ifdef THREAD_IO
    print_t0(io_comm, text, strlen(text) + 1,
             SERR_IO); // l+1 so we also send the null character
#else
    for (p = (unsigned char *)text; *p; p++)
        if ((*p > 128 || *p < 32) && *p != 10 && *p != 13 && *p != 9)
            fprintf(stderr, "[%02x]", *p);
        else
            putc(*p, stderr);
    fflush(stderr);
#endif
}

void SPrintf(char *str, char *fmt, ...)
{
    va_list argptr;
    char    text[2048];
    va_start(argptr, fmt);
    vsprintf(text, fmt, argptr);
    va_end(argptr);
    if (strlen(text) > sizeof(text))
        Error(E_ERROR, POR_AT, "memory overwrite");
    memcpy(str, (char *)text, sizeof(char) * (strlen(text) + 1));
}

int FClose(FILE *stream)
{
    int rv;
    if (!stream) {
        Error(E_ERROR, POR_AT, "stream uninitialized");
        return EOF;
    }
    rv = fclose(stream);
    if (rv) {
        Error(E_ERROR, POR_AT, "cannot close file (0x%x)", stream);
    }
    return rv;
}
