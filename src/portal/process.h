/* Parallel functionality providing info on the decomposed geometry and tools
 * for exchanging data between processes about the neighboring domains,
 * radiation frequencies etc. and providing data transfer between different
 * domains.
 */

#ifndef PROCESS_H_
#define PROCESS_H_

#include "def.h"

#define IAM_MASTER                                                             \
    (G_MY_MPI_RANK == 0                                                        \
         ? 1                                                                   \
         : 0) /* 1 if the current process is the master process (rank=0) */
#define IAMONE (G_MY_MPI_RANK == 1)

/* signals (integer constants) sent from the master node to the slaves for
 * initializing different operations */
#define SIG_ERROR      -1000 /* error occured while receiving a signal */
#define SIG_EXIT       0     /* exit Porta */
#define SIG_NONE       1     /* do nothing */
#define SIG_DIRECTIONS 1000  /* info on direction quadrature */
#define SIG_GRID                                                               \
    2000 /* grid coordinates, periodicity, domain size and origin */
#define SIG_SAVEGRID    3000 /* save model to a PMD file */
#define SIG_SAVEGRID_H5 3001 /* save model to a PMD (HDF5) file */
#define SIG_LOADGRID    4000 /* loading the model from a PMD file */
#define SIG_LOADGRID_H5 4001 /* loading the model from a PMD (HDF5) file */
#define SIG_JACOBI                                                             \
    5000 /* solve the loaded model using the Jacobi iteration algorithm */
#define SIG_FSRAY                                                              \
    10000 /* formal solution along a general direction and interval of         \
             wavelengths */
#define SIG_LC_FSRAY                                                           \
    10001 /* formal solution (long characteristics) along a general direction  \
             and interval of wavelengths */
#define SIG_FSCLV                                                              \
    10100 /* formal solution fo center-to-limb variation at a given frequency, \
             spatially averaged */
#define SIG_FSCLVAV                                                            \
    10200 /* formal solution fo center-to-limb variation at a given frequency, \
             spatially averaged, azimuth-averaged */
#define SIG_FSCLVAV_LC                                                         \
    10201 /* as SIG_FSCLVAV but with long-characteristics method */
#define SIG_FSAZAV                                                             \
    10300 /* formal solution at given inclination averaged over grid points    \
             and azimuths */
#define SIG_GETTAU                                                             \
    10400 /* get height at defined tau at each [iy][ix] position */

/*************************/
/* topological functions */
/*************************/

/* prints information about the current process to standard output */
extern void pro_PrintProcessInfo(void);

/* checks if the grid g can be coarsened (it must be "ideal" with proper number
 * of nodes per subdomain) return value: non-zero if it can be coaresend
 */
extern int pro_CanBeCoarsened(t_grid *g);

/* returns position of the subdomain "rank" with respect to the ground one
 * (which is =1); varies from 1 to G_MPI_L */
extern int pro_Domain_Z_ID(int rank);

/* returns ID of the frequency band maintained by the process; varies from 0 to
 * G_MPI_M-1 if called by master, it returns -1 */
extern int pro_MyFreqBand_ID(void);

/* returns the "typical" number of Z-planes per process (or 0 if division is
 * impossible) nz: total number of nodes in the model's Z axis NOTE the
 * uppermost domain may have smaller number of nodes per Z axis
 */
extern int pro_NSubdomainZNodes(int nz);

/* gets minimum and maximum Z-index of the subdomain with a given rank
 * nz: total grid points per Z axis
 * return value: zero if called by master/error occurs; if called by master then
 * min_iz==max_iz=-1
 */
extern int pro_MinMax_IZ(int rank, int nz, int *min_iz, int *max_iz);

/* indices (from 1 to G_MPI_L) if the subdomains into which the plane iz belongs
 * note that a given plane may belong to 2 subdomains!
 * if it only belongs to one subdomain then the other idz==0
 * nz: total number of nodes on the Z axis
 */
extern void pro_GetIZIndices(int nz, int iz, int *idz1, int *idz2);

/*
 * get minimum and maximum item to be treated by the calling rank, to globally
 * cover the range given by [min_range,max_range]
 */
extern void pro_MinMaxIRange(int min_range, int max_range, int *min_i,
                             int *max_i);

/* get minimum and maximum frequency treated in a given process rank
 * global frequency array must be initialized
 */
extern void pro_MinMaxIFreqRank(int rank, int *min_ifreq, int *max_ifreq);

/* similar to pro_MinMaxIFreqRank but for the current calling rank */
extern void pro_MinMaxIFreq(int *min_ifreq, int *max_ifreq);

/* for ESE parallelization, a whole 2D plane is treated as a 1D array,
 * that is divided amongst all the z-band processes (i.e. G_MPI_M).
 * Treated as a 1D array, each rank gets the minimum and maximum indices
 * of the plane that it will take care of for the ESE calculation.
 */
extern int pro_MinMax_ese(int rank, int nx, int ny, int *min_ese, int *max_ese);

/*************/
/* signaling */
/*************/

/* master node sends "signal" to all slaves (ignored if called by a slave
 * process) return value: nonzero on success, 0 otherwise
 */
extern int pro_SignalToAllSlaves(int signal);

/* (called by slave) waits for a signal from the master process and then it
 * returns it return value: SIG_ERROR on error, otherwise other SIG_... values
 */
extern int pro_ReceiveSignalFromMaster(void);

/***************************/
/* data transfer functions */
/***************************/

/*******************************************************************************/

/* master sends direction quadrature info to all slaves;
 * return value: nonzero on success */
extern int pro_M2S_SendDirs(void);

/* slave receives info on direction quadrature and initializes it;
 * return value: nonzero on success */
extern int pro_S_RecvDirs(void);

/*******************************************************************************/

/* send information from master to process_id on domain dimensions, number of
 * nodex/axis, periodicity, ma_n_grids, axes coordinates of the FINEST grid
 * NOTE: the grid geometry must already be initialized in the master node
 * return value: nonzero on success */
extern int pro_M2S_SendGridGeometry(int process_id);

/* slave receives data on the finest grid geometry from master (sent to it by
 * pro_M2S_SendGridGeometry) and initializes the grid including nodes return
 * value: non-zero on success */
extern int pro_S_ReceiveGridGeometry(void);

/*******************************************************************************/

/* send the linear buffer of z-plane radiation to process rank to_rank
 * len: number of elements in buffer = grid.nx * grid.ny * NSTOKES
 * WARNING: receiver must be synchronized with the sender by using same len
 * (same grid): must be assured by the solver return value: 0 on error
 */
extern int pro_SendRadiationBuffer(int len, double *buffer, int to_rank);

/* receive the linear buffer of z-plane radiation from process from_rank
 * buffer: must be allocated in advance with appropriate length
 * WARNING: receiver must be synchronized with the sender by using same len
 * (same grid) return value: 0 on error
 */
extern int pro_ReceiveRadiationBuffer(int len, double *buffer, int from_rank);

/*******************************************************************************/
/* collect the JKQ and psi operators from all frequency bands and set them
 * correctly in all the slave processes; called by each slave after formal
 * solution in all directions and dedicated frequencies is done
 */
extern void pro_CollectRadiationField(t_grid *g);

/*******************************************************************************/
/* collect the density matrix values - called from ese_SolveGridPrecondESE.
 * When calculating the ESE, the work at each domain band is parallelized
 * amongst the zband_comm processes, where each process calculates the ESE
 * for a chunk of the grid points. pro_CollectDM then gathers all the
 * values.
 */
extern void pro_CollectDM(t_grid *g, int min_ese, int max_ese);

/*******************************************************************************/
/* synchronize the density matrices in the overlaping (virtual) layers among
 * domains all subdomains called by all slaves after the ESE solution algorithm:
 * a domain sends its internal z-plane to a neighboring domain for which this
 * plane is a boundary one (i.e., min_iz-1 or max_iz+1) (only synchronization
 * within each frequency band is performed, not between different frequency
 * bands)
 */
extern void pro_SynchronizeOverlappingDM(t_grid *g);

/*******************************************************************************/
/* synchronize the density matrices (from minz to maxz)
 * this function is necessary to avoid problems with certain compiler
 * optimization options (for example, when using -xAVX with the Intel compiler)
 * Different z-bands share a z layer (i.e. if proc A is in z-band X and prob B
 * in z-band X+1 then minz in B is the same as maxz in A). When the compiler
 * uses non-value-safe optimizations, the density matrix values in minz (B) and
 * maxz (A) become slightly different. [One could make them equal with -xAVX if
 * aligning data to 32 bytes, but that would involve a lot of changes in PORTA].
 * This small difference grows up and makes the code unstable. To avoid this
 * problem we simply copy the density matrix values from minz in B to maxz in A
 * (we could have chosen the other way araound, but when saving models to a
 * file, it is the value in minz (B) the one that is saved, so we keep this
 * value here as well for consistency
 */
extern void pro_Sync_DM(t_grid *g);

/*******************************************************************************/
/* Similar to pro_SynchronizeOverlappingDM but synchronizing the whole nodes'
 * data NOTE: the virtual layes must be allocated in advance
 */
extern void pro_SynchronizeOverlappingNodes(t_grid *g);

/*******************************************************************************/
/* a function called by all slaves which finds the maximum value of my_quantity
 * among the domains (used for max. relative change, truncation error, max.
 * residuum)
 */
extern double pro_SynchronizeMaxQuantity(double my_quantity);

#endif /* PROCESS_H_ */
