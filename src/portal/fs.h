/* Formal solution of RTE */

#ifndef FS_H_
#define FS_H_

#include "def.h"
#include "global.h"
#include "plot.h"

/* set JKQ tensors and psi operators in the whole grid to 0 */
extern void fs_ResetRadiationCounters(t_grid *g);

/* formal solution in the given subdomain for all the ray directions and
 * frequencies treated by a given process; NOTE that synchronization of boundary
 * conditions with other processes of the same frequency band via MPI performed
 * in this function but NOT synchronization of the integrated radiation field
 * among various frequency bands that's done by calling
 * pro_CollectRadiationField() NOTE 2: call fs_ResetRadiationCounters() before
 * calling this function
 */
extern void fs_FormalSolution(t_grid *g);

/****************************************************/

/* formal solution at an arbitrary mesh node p_node in an arbitrary direction
 * and frequency ifreq; stokes contains the resulting radiation field tau1point
 * contains coordinates (within the real domain) of point where tau=1 along the
 * LOS at frequency ifreq (measured from p_node)
 */
extern void fs_FormalSolutionNode(t_node *p_node, const t_vec_3d direction,
                                  int ifreq, double stokes[NSTOKES],
                                  t_vec_3d *tau1point);

/* formal solution for the NX x NY 1D problems */
extern void fs_FormalSolution1D(t_grid *p_grid);

/* formal solution at the surface of the atmosphere for storage in PSP file */
extern void fs_FSSurfaceXY(t_vec_3d dir, int ifr_min, int ifr_max);

/* formal solution (long characteristics) at the surface of the atmosphere for
 * storage in PSP file */
extern void fs_FSSurface_LC_XY(t_vec_3d dir, int ifr_min, int ifr_max);

/* FS for range of inclinations, given azimuth and freuency; chi: azimuth in
 * degrees (works synchronously with too_CLVFreq) */
extern void fs_FSCLVFreq(double chi, double mu_min, double mu_max, int N_ray,
                         int ifreq);

/* FS for range of inclinations, given freuency; averaged over N_azim azimuths
 * (works synchronously with too_CLVFreqAV) */
extern void fs_FSCLVFreqAV(double mu_min, double mu_max, int N_incl, int N_azim,
                           int ifreq);

/* FS (long characteristics) for range of inclinations, given freuency; averaged
 * over N_azim azimuths (works synchronously with too_CLVFreqAV) */
extern void fs_FSCLVFreqAV_LC(double mu_min, double mu_max, int N_incl,
                              int N_azim, int ifreq);

/* FS for given inclination, averaged over space and azimuths (works
 * synchronously with too_FSAZAV) */
extern void fs_FSAZAV(double theta_los, int N_azim, int ifr_min, int ifr_max);

/* Get X,Y,Z where tau=desired_tau */
extern void fs_GetTau(double tau, t_vec_3d dir, int ifr_min, int ifr_max);

#endif /*FS_H_*/
