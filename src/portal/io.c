#include "io.h"
#include "directions.h"
#include "error.h"
#include "global.h"
#include "grid.h"
#include "hdf5.h"
#include "hdf5_hl.h"
#include "modules.h"
#include "process.h"
#include "topology.h"
#include <stdio.h>
#include <string.h>
#include <time.h>

#define WERR                                                                   \
    {                                                                          \
        Error(E_ERROR, POR_AT, "cannot write into the file");                  \
        fclose(f);                                                             \
        return 0;                                                              \
    }
#define WERR_H5                                                                \
    {                                                                          \
        Error(E_ERROR, POR_AT, "cannot write into the file");                  \
        H5Fclose(f_id);                                                        \
        return 0;                                                              \
    }
#define RERR                                                                   \
    {                                                                          \
        Error(E_ERROR, POR_AT, "cannot read from the file");                   \
        fclose(f);                                                             \
        return 0;                                                              \
    }
#define RERR_H5                                                                \
    {                                                                          \
        Error(E_ERROR, POR_AT, "cannot read from the file");                   \
        H5Fclose(file_id);                                                     \
        return 0;                                                              \
    }

extern t_global_vars *g_global;

/*******************************************************************************/
/* open PMD file for writing */
static FILE *OpenFileW(const char *fname)
{
    FILE *f = fopen(fname, "wb");
    if (!f) {
        Error(E_ERROR, POR_AT, "cannot open file %s", fname);
        return 0;
    }
    return f;
}

/* open PMD (HDF5) file for writing
   Requires parallel HDF5 as it is opened by all MPI_COMM_WORLD processes */
static hid_t OpenFileW_h5(const char *fname)
{
    hid_t f_id;     /* file identifier */
    hid_t plist_id; /* property list identifier */

    plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL);

    f_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
    H5Pclose(plist_id);

    if (f_id < 0) {
        fprintf(stderr, "cannot open HDF5 file %s\n", fname);
        return 0;
    }

    return f_id;
}

/*******************************************************************************/
/* open PMD file for reading */
static FILE *OpenFileR(const char *fname)
{
    FILE *f = fopen(fname, "rb");
    if (!f) {
        Error(E_ERROR, POR_AT, "cannot open file %s", fname);
        return 0;
    }
    return f;
}

/* open HDF5 file collectively
   Requires parallel HDF5 as it is opened by all MPI_COMM_WORLD processes */
static hid_t OpenFileR_h5(const char *fname)
{
    hid_t f_id;     /* file identifier */
    hid_t plist_id; /* property list identifier */

    plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL);

    f_id = H5Fopen(fname, H5F_ACC_RDONLY, plist_id);
    H5Pclose(plist_id);

    if (f_id < 0) {
        if (!G_MY_MPI_RANK) {
            Error(E_ERROR, POR_AT, "cannot open HDF5 file %s", fname);
            return 0;
        }
    }
    return f_id;
}

/*******************************************************************************/
FILE *io_OpenTMPStream(int size, char *data)
{
    FILE *f = tmpfile();
    if (!f)
        Error(E_ERROR, POR_AT, "cannot open temporary file");
    fwrite(data, 1, size, f);
    rewind(f);
    return f;
}

/*******************************************************************************/
int io_CloseTMPStream(FILE *stream)
{
    int res = !fclose(stream);
    if (!res)
        Error(E_ERROR, POR_AT, "cannot delete the temporary file");
    return res;
}

/*******************************************************************************/
static int StoreLocalTime(FILE *f)
{
    int        cas[6] = {-1, -1, -1, -1, -1, -1};
    struct tm *cr;
    time_t     tn = time(NULL);

    if (tn <= 0) {
        if (!fwrite(&cas, INTSIZE, 6, f))
            WERR;
        return -1;
    }
    cr     = localtime(&tn);
    cas[0] = cr->tm_year;
    cas[1] = cr->tm_mon;
    cas[2] = cr->tm_mday;
    cas[3] = cr->tm_hour;
    cas[4] = cr->tm_min;
    cas[5] = cr->tm_sec;
    if (!fwrite(&cas, INTSIZE, 6, f)) {
        WERR;
        return 0;
    } else {
        return 1;
    }
}

static int StoreLocalTime_h5(hid_t f_id)
{
    int        cas[6] = {-1, -1, -1, -1, -1, -1};
    struct tm *cr;
    time_t     tn = time(NULL);

    if (tn <= 0) {
        if (H5LTset_attribute_int(f_id, "/", "CREATION_DATE", cas, 6) < 0)
            WERR_H5;
        return -1;
    }
    cr     = localtime(&tn);
    cas[0] = cr->tm_year;
    cas[1] = cr->tm_mon;
    cas[2] = cr->tm_mday;
    cas[3] = cr->tm_hour;
    cas[4] = cr->tm_min;
    cas[5] = cr->tm_sec;
    if (H5LTset_attribute_int(f_id, "/", "CREATION_DATE", cas, 6) < 0) {
        WERR_H5;
        return 0;
    } else {
        return 1;
    }
}

/*******************************************************************************/
static int WriteHeader(FILE *f)
{
    char *cbuf, c;
    char  modname[IO_PMD_MODULE_LEN + 1];
    int   ui, uia[3];

    if (G_MY_MPI_RANK)
        IERR;
    /* magic */
    if (fwrite(IO_PMD_MAGIC, 1, IO_PMD_MAGIC_LEN, f) != IO_PMD_MAGIC_LEN)
        WERR;
    /* endians */
    c = (char)IS_BIG_ENDIAN;
    if (!fwrite(&c, 1, 1, f))
        WERR;
    /* size of int */
    c = (char)sizeof(int);
    if (!fwrite(&c, 1, 1, f))
        WERR;
    /* size of double */
    c = (char)DBLSIZE;
    if (!fwrite(&c, 1, 1, f))
        WERR;
    /* pmd file version */
    ui = IO_PMD_VERSION;
    if (!fwrite(&ui, INTSIZE, 1, f))
        WERR;
    /* time of pmd file creation */
    if (StoreLocalTime(f) < 0) {
        Error(E_WARNING, POR_AT, "local time is not available");
    }
    /* periodicity */
    if (2 != fwrite(&g_global->period, CHRSIZE, 2, f))
        WERR;
    /* domain size */
    if (fwrite(&g_global->domain_size.x, DBLSIZE, 1, f) != 1)
        WERR;
    if (fwrite(&g_global->domain_size.y, DBLSIZE, 1, f) != 1)
        WERR;
    if (fwrite(&g_global->domain_size.z, DBLSIZE, 1, f) != 1)
        WERR;
    /* domain origin */
    if (fwrite(&g_global->domain_origin.x, DBLSIZE, 1, f) != 1)
        WERR;
    if (fwrite(&g_global->domain_origin.y, DBLSIZE, 1, f) != 1)
        WERR;
    if (fwrite(&g_global->domain_origin.z, DBLSIZE, 1, f) != 1)
        WERR;
    /* number of nodes per axis */
    uia[0] = g_global->grid[0].nx;
    uia[1] = g_global->grid[0].ny;
    uia[2] = g_global->grid[0].nz;
    if (fwrite(&uia[0], INTSIZE, 3, f) != 3)
        WERR;
    /* nodes coordinates along all axes */
    if (fwrite(g_global->grid[0].x, DBLSIZE, MAX_N_ORDINATES, f) !=
        MAX_N_ORDINATES)
        WERR;
    if (fwrite(g_global->grid[0].y, DBLSIZE, MAX_N_ORDINATES, f) !=
        MAX_N_ORDINATES)
        WERR;
    if (fwrite(g_global->grid[0].z, DBLSIZE, MAX_N_ORDINATES, f) !=
        MAX_N_ORDINATES)
        WERR;
    /* ray directions */
    ui = g_global->dirs.n_incl_oct;
    if (!fwrite(&ui, INTSIZE, 1, f))
        WERR;
    ui = g_global->dirs.n_azim_oct;
    if (!fwrite(&ui, INTSIZE, 1, f))
        WERR;
    /* module name */
    cbuf = mod_GetModuleName();
    if (strlen(cbuf) > IO_PMD_MODULE_LEN) {
        Error(E_ERROR, POR_AT, "module name is too long");
        free(cbuf);
        return 0;
    }
    memset(modname, 0, IO_PMD_MODULE_LEN + 1);
    strcpy(modname, cbuf);
    free(cbuf);
    if (fwrite(modname, 1, IO_PMD_MODULE_LEN, f) != IO_PMD_MODULE_LEN)
        WERR;
    /* model comment */
    if (fwrite(&g_global->comment, CHRSIZE, PMD_COMMENT_LEN, f) !=
        PMD_COMMENT_LEN)
        WERR;
    /* size of the module header */
    if (!fwrite(&g_global->module_header_fsize, INTSIZE, 1, f))
        WERR;
    /* size of the single-node data */
    if (!fwrite(&g_global->module_node_fsize, INTSIZE, 1, f))
        WERR;
    return 1;
}

static int WriteHeader_h5(hid_t f_id)
{
    double d3[3];
    int    i3[3];
    int    ver = IO_PMD_VERSION;
    char * cbuf;

    // The attributes must be written collectively by all MPI_COMM_WORLD
    // processes

    if (H5LTset_attribute_string(f_id, "/", "PMD_MAGIC", IO_PMD_MAGIC) < 0)
        WERR_H5;
    if (H5LTset_attribute_int(f_id, "/", "PMD_VERSION", &ver, 1) < 0)
        WERR_H5;

    /* time of pmd file creation */
    if (StoreLocalTime_h5(f_id) < 0) {
        Error(E_WARNING, POR_AT, "local time is not available");
    }

    i3[0] = (int)g_global->period[0];
    i3[1] = (int)g_global->period[1];
    if (H5LTset_attribute_int(f_id, "/", "PERIODICITY", i3, 2) < 0)
        WERR_H5;

    d3[0] = g_global->domain_size.x;
    d3[1] = g_global->domain_size.y;
    d3[2] = g_global->domain_size.z;
    if (H5LTset_attribute_double(f_id, "/", "DOMAIN_SIZE", d3, 3) < 0)
        WERR_H5;

    d3[0] = g_global->domain_origin.x;
    d3[1] = g_global->domain_origin.y;
    d3[2] = g_global->domain_origin.z;
    if (H5LTset_attribute_double(f_id, "/", "DOMAIN_ORIGIN", d3, 3) < 0)
        WERR_H5;

    i3[0] = g_global->grid[0].nx;
    i3[1] = g_global->grid[0].ny;
    i3[2] = g_global->grid[0].nz;
    if (H5LTset_attribute_int(f_id, "/", "GRID_DIMENSIONS", i3, 3) < 0)
        WERR_H5;

    if (H5LTset_attribute_double(f_id, "/", "X_AXIS", g_global->grid[0].x,
                                 g_global->grid[0].nx) < 0)
        WERR_H5;
    if (H5LTset_attribute_double(f_id, "/", "Y_AXIS", g_global->grid[0].y,
                                 g_global->grid[0].ny) < 0)
        WERR_H5;
    if (H5LTset_attribute_double(f_id, "/", "Z_AXIS", g_global->grid[0].z,
                                 g_global->grid[0].nz) < 0)
        WERR_H5;

    if (H5LTset_attribute_int(f_id, "/", "POLAR_NODES",
                              &g_global->dirs.n_incl_oct, 1) < 0)
        WERR_H5;
    if (H5LTset_attribute_int(f_id, "/", "AZIMUTH_NODES",
                              &g_global->dirs.n_azim_oct, 1) < 0)
        WERR_H5;

    /* module name */
    cbuf = mod_GetModuleName();
    if (H5LTset_attribute_string(f_id, "/", "MODULE_NAME", cbuf) < 0)
        WERR_H5;
    free(cbuf);

    if (H5LTset_attribute_string(f_id, "/", "MODULE_COMMENT",
                                 g_global->comment) < 0)
        WERR_H5;

    if (H5LTset_attribute_int(f_id, "/", "MODULE_HEADER_SIZE",
                              &g_global->module_header_fsize, 1) < 0)
        WERR_H5;

    return 1;
}

/*******************************************************************************/
/* (master) saves the nodes data sent by SaveNodesSlave() of the processes 1 to
 * G_MPI_L into the PMD file f; complementary to SaveNodesSlave() returns 0 on
 * error
 */
static int SaveNodesMaster(FILE *f)
{
    t_grid *   g = &g_global->grid[0];
    int        iy, iz, idz;
    int        nsize = g_global->module_node_fsize, linelen = g->nx * nsize;
    char *     buf = (char *)Malloc(linelen);
    MPI_Status status;

    if (G_MY_MPI_RANK)
        IERR;
    if (!PorMeshInitialized() || !mod_IsModuleLoaded()) {
        Error(E_ERROR, POR_AT, "no grid created or any module loaded");
        return 0;
    }
    if (!f) {
        Error(E_ERROR, POR_AT, "file not opened");
        return 0;
    }
    for (idz = 1; idz <= G_MPI_L; idz++) {
        int min_iz, max_iz;
        if (!pro_MinMax_IZ(idz, g->nz, &min_iz, &max_iz))
            IERR;
        max_iz = (idz < G_MPI_L ? max_iz - 1 : max_iz);
        for (iz = min_iz; iz <= max_iz; iz++) {
            for (iy = 0; iy < g->ny; iy++) {
                if (MPI_SUCCESS != MPI_Recv(buf, linelen, MPI_BYTE, idz, 0,
                                            MPI_COMM_WORLD, &status)) {
                    free(buf);
                    CERR;
                    return 0;
                }
                if (fwrite(buf, 1, linelen, f) != linelen) {
                    free(buf);
                    Error(E_ERROR, POR_AT, "cannot write the nodes' data");
                    return 0;
                }
            }
        }
    }
    free(buf);
    return 1;
}

/*******************************************************************************/
/* (slave) sends nodes data do master for saving into a disk PMD file
 * complementary to SaveNodesMaster()
 * returns 0 on error
 */
static int SaveNodesSlave(void)
{
    /* the processes 1 to G_MPI_L-1 send only min_iz to max_iz-1 data, G_MPI_L
     * sends from min_iz to max_iz*/
    int     min_iz, max_iz, iz, iy, ix;
    t_grid *g   = &g_global->grid[0];
    int   nsize = g_global->afunc.F_GetNodeDataSize(), linelen = g->nx * nsize;
    char *buf, *p;

    if (G_MY_MPI_RANK < 1)
        IERR;
    if (G_MY_MPI_RANK > G_MPI_L)
        return 1; /* only the slaves 1 to G_MPI_L are sending the nodes' data */
    buf = (char *)Malloc(linelen);
    if (!pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz))
        IERR;
    max_iz = (G_MY_MPI_RANK < G_MPI_L ? max_iz - 1 : max_iz);
    for (iz = min_iz; iz <= max_iz; iz++) {
        for (iy = 0; iy < g->ny; iy++) {
            p = buf;
            for (ix = 0; ix < g->nx; ix++) {
                void *dat;
                if (!g->node[iz] || !g->node[iz][iy][ix].p_data)
                    IERR;
                /*if (g_global->afunc.F_IO_GetNodePMDData(&g->node[iz][iy][ix],
                 * (void*)p) < 0) IERR;*/
                dat = g_global->afunc.F_EncodeNodeData(
                    g->node[iz][iy][ix].p_data);
                Memcpy(p, dat, nsize);
                free(dat);
                p += nsize;
            }
            if (MPI_SUCCESS !=
                MPI_Send(buf, linelen, MPI_BYTE, 0, 0, MPI_COMM_WORLD)) {
                free(buf);
                CERR;
                return 0;
            }
        }
    }
    free(buf);
    return 1;
}

/*****************************************************************************/
/**
 * @brief Save a PMD model in HDF5 format (routine for the slave processes)
 *
 * In this routine (for slaves processes), we need to calculate min_iz and
 * max_iz, the minimum and maximum Z-indices that each process is responsible
 * for. Then each slave writes directly to the file their Z layers. There
 * is no communication with the master, so there is no equivalent master
 * routine, as it is not necessary
 *
 * @param file_id [in] HDF5 file identifier
 * @param grid_id [in] Grid data dataset ("/Module/g_data") identifier
 *
 * @todo check return status all all calls to h5 library
 *
 * @return 1 if successful
 *****************************************************************************/
static int SaveNodesSlave_h5(hid_t file_id, hid_t grid_id)
{
    t_grid *g = &g_global->grid[0];
    int     min_iz, max_iz;
    int     iz, iy, ix;
    char *  buf, *p;
    hid_t   pmd_type, dspace, mspace;
    herr_t  status_h5;
    hsize_t mspace_size;
    hsize_t start[3], stride[3], count[3], block[3];
    size_t  d_size;

    if (G_MY_MPI_RANK > G_MPI_L)
        IERR; /* only the processes from 1 to G_MPI_L should enter this routine
               */

    if (!pro_MinMax_IZ(G_MY_MPI_RANK, g_global->grid[0].nz, &min_iz, &max_iz))
        IERR;
    /* the processes 1 to G_MPI_L-1 send only min_iz to max_iz-1 data,
       G_MPI_L sends from min_iz to max_iz*/
    max_iz = (G_MY_MPI_RANK < G_MPI_L ? max_iz - 1 : max_iz);

    pmd_type = H5Dget_type(grid_id);  /* data type of g_data dataset */
    d_size   = H5Tget_size(pmd_type); /* size of module datatype */
    dspace   = H5Dget_space(grid_id); /* data space of g_data dataset */

    /* Allocate space for a whole Z-layer of data */
    buf = (char *)Malloc(d_size * g->nx * g->ny);

    for (iz = min_iz; iz <= max_iz; iz++) {
        /* indices for Z=iz layer hyperslab in the file */
        start[0]  = iz;
        start[1]  = 0;
        start[2]  = 0;
        stride[0] = 1;
        stride[1] = 1;
        stride[2] = 1;
        count[0]  = 1;
        count[1]  = 1;
        count[2]  = 1;
        block[0]  = 1;
        block[1]  = g->ny;
        block[2]  = g->nx;

        if ((status_h5 = H5Sselect_hyperslab(dspace, H5S_SELECT_SET, start,
                                             stride, count, block)) < 0) {
            Error(E_ERROR, POR_AT, "Proc %d hyperslab status ERROR %d \n",
                  G_MY_MPI_RANK, status_h5);
        }

        /* create a simple memory space corresponding to a Z-layer */
        mspace_size = g->nx * g->ny;
        if ((mspace = H5Screate_simple(1, &mspace_size, NULL)) < 0) {
            Error(E_ERROR, POR_AT, "Proc %d memspace creation ERROR %d \n",
                  G_MY_MPI_RANK, mspace);
        }

        /* linearize all the data from a Z-layer into buffer buf */
        p = buf;
        for (iy = 0; iy < g->ny; iy++) {
            for (ix = 0; ix < g->nx; ix++) {
                if (!g->node[iz] || !g->node[iz][iy][ix].p_data)
                    IERR;
                if (!g_global->afunc.F_EncodeNodeData_h5(
                        p, g->node[iz][iy][ix].p_data))
                    IERR;
                p += d_size;
            }
        }

        /* write Z=iz layer to dataset grid_id */
        if ((status_h5 = H5Dwrite(grid_id, pmd_type, mspace, dspace,
                                  H5P_DEFAULT, buf)) < 0) {
            Error(E_ERROR, POR_AT, "Proc %d cannot write to file ERROR \n",
                  G_MY_MPI_RANK);
        }
    }

    H5Sclose(mspace);
    H5Sclose(dspace);
    H5Tclose(pmd_type);

    free(buf);
    return 1;
}

/* pointer function to either io_SaveGridMaster or io_SaveGridMaster_h5 */
int (*io_SaveGridMaster_fp)(char *fname);

/*******************************************************************************/
int io_SaveGridMaster(char *fname)
{
    FILE *f;

    if (G_MY_MPI_RANK)
        IERR;
    stk_Add(1, POR_AT, "saving a PMD model to the file %s", fname);
    if (!PorMeshInitialized()) {
        Error(E_ERROR, POR_AT, "grid is not initialized");
        return 0;
    }
    if (!mod_IsModuleLoaded()) {
        Error(E_ERROR, POR_AT, "no module is loaded");
        return 0;
    }
    if (!(f = OpenFileW(fname)))
        return 0;
    if (!WriteHeader(f))
        return 0;
    if (g_global->afunc.F_IO_WriteHeader(f) < 0) {
        Error(E_ERROR, POR_AT, "cannot write the module header");
        fclose(f);
        return 0;
    }
    if (!SaveNodesMaster(f))
        WERR;
    if (fclose(f)) {
        Error(E_ERROR, POR_AT, "cannot save the file %s", fname);
        return 0;
    }
    return 1;
}

/* pointer function to either io_SaveGridSlave or io_SaveGridSlave_h5 */
int (*io_SaveGridSlave_fp)(void);

/*******************************************************************************/
int io_SaveGridSlave(void)
{
    if (!G_MY_MPI_RANK)
        IERR;
    if (G_MY_MPI_RANK > G_MPI_L)
        return 1; /* only the processes from 1 to G_MPI_L need to send the
                     nodes' data to master */
    if (!SaveNodesSlave()) {
        Error(E_ERROR, POR_AT,
              "cannot send the nodes' data to the master process");
        return 0;
    }
    return 1;
}

/*****************************************************************************/
/**
 * @brief Save a PMD model in HDF5 format (routine for the master process)
 *
 * Collectively the file is opened and the PORTA header and the module header
 * are written. The master does not write grid nodes data to the file, so it can
 * return right after writing the module header. The actual hard work is done by
 * the slaves (see io_SaveGridSlaves_h5)
 *
 * @param fname [in] Name of file
 *
 * @return 1 if successful ; 0 if not
 *****************************************************************************/
int io_SaveGridMaster_h5(char *fname)
{
    hid_t f_id, g_id;

    if (MPI_SUCCESS !=
        MPI_Bcast(fname, MAX_STRING + 1, MPI_BYTE, 0, MPI_COMM_WORLD))
        CERR;

    if (G_MY_MPI_RANK)
        IERR;
    stk_Add(1, POR_AT, "saving a PMD model to the file %s", fname);
    if (!PorMeshInitialized()) {
        Error(E_ERROR, POR_AT, "grid is not initialized");
        return 0;
    }
    if (!mod_IsModuleLoaded()) {
        Error(E_ERROR, POR_AT, "no module is loaded");
        return 0;
    }

    if (!(f_id = OpenFileW_h5(fname))) {
        Error(E_ERROR, POR_AT, "cannot open the file %s", fname);
        return 0;
    }

    if (!WriteHeader_h5(f_id))
        return 0;
    if ((g_id = g_global->afunc.F_IO_WriteHeader_h5(f_id)) < 0) {
        Error(E_ERROR, POR_AT, "cannot write the module header");
        H5Fclose(f_id);
        return 0;
    }

    if (H5Dclose(g_id) < 0) {
        Error(E_ERROR, POR_AT, "cannot close dataset /Module/g_data");
        return 0;
    }

    if (H5Fclose(f_id) < 0) {
        Error(E_ERROR, POR_AT, "cannot close the file %s", fname);
        return 0;
    }
    return 1;
}

/*****************************************************************************/
/**
 * @brief Save a PMD model in HDF5 format (routine for the slave processes)
 *
 * Only the processes with rank >= G_MPI_L will actually write data, by calling
 * SaveNodesSlave_h5, the other ones return immediately after writing the PORTA
 * and module headers
 *
 * @return 1 if successful ; 0 if not
 *****************************************************************************/
int io_SaveGridSlave_h5(void)
{
    hid_t f_id, g_id;
    char  fname[MAX_STRING + 1];

    if (MPI_SUCCESS !=
        MPI_Bcast(fname, MAX_STRING + 1, MPI_BYTE, 0, MPI_COMM_WORLD))
        CERR;

    if (!G_MY_MPI_RANK)
        IERR;

    if (!(f_id = OpenFileW_h5(fname))) {
        Error(E_ERROR, POR_AT, "cannot open the file %s", fname);
        return 0;
    }

    if (!WriteHeader_h5(f_id))
        return 0;
    if ((g_id = g_global->afunc.F_IO_WriteHeader_h5(f_id)) < 0) {
        Error(E_ERROR, POR_AT, "cannot write the module header");
        H5Fclose(f_id);
        return 0;
    }

    /* only the processes from 1 to G_MPI_L write node data */
    if (G_MY_MPI_RANK <= G_MPI_L) {
        if (!SaveNodesSlave_h5(f_id, g_id)) {
            Error(E_ERROR, POR_AT, "cannot write data to the file");
            return 0;
        }
    }

    if (H5Dclose(g_id) < 0) {
        Error(E_ERROR, POR_AT, "cannot close dataset /Module/g_data");
        return 0;
    }

    if (H5Fclose(f_id) < 0) {
        Error(E_ERROR, POR_AT, "cannot close the file %s", fname);
        return 0;
    }

    return 1;
}

/*******************************************************************************/
static int LoadFileTime(FILE *f)
{
    int ui[6] = {0, 0, 0, 0, 0, 0};
    if (fread(&ui, INTSIZE, 6, f) != 6)
        return 0;
    else
        return 1;
}

/*******************************************************************************/
/* read header of the PMD file stored in a buffer and perform the needed
 * initializations of the global variables (loading a module library, etc.);
 * output: module name, domain_size (size of the integration domain in X, Y, and
 * Z) usable in both master and slave nodes return: nonzero if successful
 */
static int ReadHeader(char buffer[], char modname[IO_PMD_MODULE_LEN + 1])
{
    char   c;
    char   magic[IO_PMD_MAGIC_LEN + 1];
    int    ui, uia[3];
    double x[MAX_N_ORDINATES], y[MAX_N_ORDINATES], z[MAX_N_ORDINATES];
    int    nx, ny, nz;
    FILE * f = io_OpenTMPStream(PMD_HEADER_LEN, buffer);

    if (!f)
        return 0;
    /* magic */
    if (fread(&magic, 1, IO_PMD_MAGIC_LEN, f) != IO_PMD_MAGIC_LEN)
        RERR;
    magic[IO_PMD_MAGIC_LEN] = 0;
    if (strcmp(magic, IO_PMD_MAGIC)) {
        Error(E_ERROR, POR_AT, "invalid PMD file");
        return 0;
    }
    /* endians */
    if (!fread(&c, 1, 1, f))
        RERR;
    if (c != (char)IS_BIG_ENDIAN) {
        Error(E_ERROR, POR_AT, "incompatible endians");
        return 0;
    }
    /* size of int */
    if (!fread(&c, 1, 1, f))
        RERR;
    if (c != (char)INTSIZE) {
        Error(E_ERROR, POR_AT, "incompatible integer length");
        return 0;
    }
    /* size of double */
    if (!fread(&c, 1, 1, f))
        RERR;
    if (c != (unsigned char)DBLSIZE) {
        Error(E_ERROR, POR_AT, "incompatible length of double");
        return 0;
    }
    /* format version of the pmd file */
    if (!fread(&ui, INTSIZE, 1, f))
        RERR;
    if (ui != IO_PMD_VERSION) {
        Error(
            E_ERROR, POR_AT,
            "Incompatible version of the model file: %d. The supported version "
            "is %d.",
            ui, IO_PMD_VERSION);
    }
    /* time of the PMD file creation */
    if (!LoadFileTime(f))
        RERR;
    /* periodicity */
    if (2 != fread(g_global->period, CHRSIZE, 2, f))
        RERR;
    if ((g_global->period[0] && !g_global->period[1]) ||
        (!g_global->period[0] && g_global->period[1])) {
        Error(E_ERROR, POR_AT,
              "periodicity along X must be the same as along Y");
    }

    /* Non-periodic boundaries not thoroughly tested. Limiting PORTA to periodic
       boundaries. Jan'20 [AdV]*/
    if ((!g_global->period[0] || !g_global->period[1])) {
        Error(E_ERROR, POR_AT,
              "This version of PORTA needs periodic boundaries along X and Y");
    }

    /* domain size */
    if (fread(&g_global->domain_size.x, DBLSIZE, 1, f) != 1)
        RERR;
    if (fread(&g_global->domain_size.y, DBLSIZE, 1, f) != 1)
        RERR;
    if (fread(&g_global->domain_size.z, DBLSIZE, 1, f) != 1)
        RERR;
    stk_Add(0, POR_AT, "domain size: (%12.12e, %12.12e, %12.12e) km",
            g_global->domain_size.x / 1e5, g_global->domain_size.y / 1e5,
            g_global->domain_size.z / 1e5);
    /* domain origin */
    if (fread(&g_global->domain_origin.x, DBLSIZE, 1, f) != 1)
        RERR;
    if (fread(&g_global->domain_origin.y, DBLSIZE, 1, f) != 1)
        RERR;
    if (fread(&g_global->domain_origin.z, DBLSIZE, 1, f) != 1)
        RERR;
    stk_Add(0, POR_AT, "domain origin: (%12.12e, %12.12e, %12.12e) km",
            g_global->domain_origin.x / 1e5, g_global->domain_origin.y / 1e5,
            g_global->domain_origin.z / 1e5);
    /* number of axis nodes */
    if (fread(&uia[0], INTSIZE, 3, f) != 3)
        RERR;
    nx = uia[0];
    ny = uia[1];
    nz = uia[2];
    stk_Add(0, POR_AT, "(nx,ny,nz): (%d,%d,%d)", nx, ny, nz);
    /* check the possibility of parallelization in Z */
    if (!pro_NSubdomainZNodes(nz)) {
        stk_Add(
            0, POR_AT,
            "grid with nz=%d nodes was not parallelizable with previous PORTA "
            "version using %d subdomains",
            nz, G_MPI_L);
    }
    /* coordinates of nodes along axes */
    if (fread(x, DBLSIZE, MAX_N_ORDINATES, f) != MAX_N_ORDINATES)
        RERR;
    if (fread(y, DBLSIZE, MAX_N_ORDINATES, f) != MAX_N_ORDINATES)
        RERR;
    if (fread(z, DBLSIZE, MAX_N_ORDINATES, f) != MAX_N_ORDINATES)
        RERR;
    stk_Add(0, POR_AT, "mix(x), max(x): %12.12e, %12.12e km", x[0] / 1e5,
            x[nx - 1] / 1e5);
    stk_Add(0, POR_AT, "mix(y), max(y): %12.12e, %12.12e km", y[0] / 1e5,
            y[ny - 1] / 1e5);
    stk_Add(0, POR_AT, "mix(z), max(z): %12.12e, %12.12e km", z[0] / 1e5,
            z[nz - 1] / 1e5);
    /* ray directions */
    if (fread(&uia[0], INTSIZE, 2, f) != 2)
        RERR;
    if ((uia[1] == -1) & (uia[0] >= 0)) {
        dir_MakeDirectionsQuadSet(uia[0], &g_global->dirs);
    } else {
        dir_MakeDirections(uia[0], uia[1], &g_global->dirs);
    }

    /* module name */
    memset(modname, 0, IO_PMD_MODULE_LEN + 1);
    if (fread(modname, 1, IO_PMD_MODULE_LEN, f) != IO_PMD_MODULE_LEN)
        RERR;
    /* comment */
    if (fread(&g_global->comment, CHRSIZE, PMD_COMMENT_LEN, f) !=
        PMD_COMMENT_LEN)
        RERR;
    g_global->comment[PMD_COMMENT_LEN] = 0;
    /* size of module header */
    if (1 != fread(&g_global->module_header_fsize, INTSIZE, 1, f))
        RERR;
    /* unused integer */
    if (1 != fread(&ui, INTSIZE, 1, f))
        RERR;
    /* maximum number of grids & current number of grids */
    g_global->max_n_grids     = 1;
    g_global->current_n_grids = 0;
    /* create a grid */
    grd_NewGridC(0, nx, ny, nz, x, y, z);
    /* delete the temporary file */
    if (!io_CloseTMPStream(f)) {
        Error(E_ERROR, POR_AT, "cannot close the temporary file");
        return 0;
    }

    return 1;
}

static int ReadHeader_h5(hid_t file_id, char modname[IO_PMD_MODULE_LEN + 1])
{
    size_t      type_size;
    H5T_class_t type_class;
    hsize_t *   dimsr = NULL;
    char *      magic = NULL; /* buffer to read MAGIC string */

    int     nx, ny, nz;
    double *x, *y, *z;

    /* magic */
    if (!G_MY_MPI_RANK) {
        if (H5LTget_attribute_info(file_id, "/", "PMD_MAGIC", dimsr,
                                   &type_class, &type_size) < 0)
            RERR_H5;
        /* HDF5 strings generated in Python are not null-terminated, so I
           initialize all to NULL This way, either null-terminated or
           null-padded (created in C) HDF5 strings will work */
        magic = malloc(type_size + 1);
        memset(magic, '\0', type_size + 1);
        if (H5LTget_attribute_string(file_id, "/", "PMD_MAGIC", magic) < 0)
            RERR_H5;
        if (strcmp(magic, IO_PMD_MAGIC)) {
            Error(E_ERROR, POR_AT, "invalid PMD file. Magic is %s", magic);
            return 0;
        }
    }

    /* format version of the pmd file */
    int ver;
    if (!G_MY_MPI_RANK) {
        if (H5LTget_attribute_int(file_id, "/", "PMD_VERSION", &ver) < 0)
            RERR_H5;
    }
    if (MPI_SUCCESS != MPI_Bcast(&ver, 1, MPI_INT, 0, MPI_COMM_WORLD))
        CERR;
    if (ver != IO_PMD_VERSION) {
        Error(
            E_ERROR, POR_AT,
            "Incompatible version of the model file: %d. The supported version "
            "is %d.",
            ver, IO_PMD_VERSION);
    }

    /* periodicity */
    int i2[2];
    if (!G_MY_MPI_RANK) {
        if (H5LTget_attribute_int(file_id, "/", "PERIODICITY", i2) < 0)
            RERR_H5;
    }
    if (MPI_SUCCESS != MPI_Bcast(i2, 2, MPI_INT, 0, MPI_COMM_WORLD))
        CERR;
    g_global->period[0] = (char)i2[0];
    g_global->period[1] = (char)i2[1];
    if ((g_global->period[0] && !g_global->period[1]) ||
        (!g_global->period[0] && g_global->period[1])) {
        Error(E_ERROR, POR_AT,
              "periodicity along X must be the same as along Y");
    }

    /* Non-periodic boundaries not thoroughly tested. Limiting PORTA to periodic
       boundaries. Jan'20 [AdV]*/
    if ((!g_global->period[0] || !g_global->period[1])) {
        Error(E_ERROR, POR_AT,
              "This version of PORTA needs periodic boundaries along X and Y");
    }

    /* domain size */
    double d3[3];
    if (!G_MY_MPI_RANK) {
        if (H5LTget_attribute_double(file_id, "/", "DOMAIN_SIZE", d3) < 0)
            RERR_H5;
    }
    if (MPI_SUCCESS != MPI_Bcast(d3, 3, MPI_DOUBLE, 0, MPI_COMM_WORLD))
        CERR;
    g_global->domain_size.x = d3[0];
    g_global->domain_size.y = d3[1];
    g_global->domain_size.z = d3[2];
    if (!G_MY_MPI_RANK)
        stk_Add(0, POR_AT, "domain size: (%12.12e, %12.12e, %12.12e) km",
                g_global->domain_size.x / 1e5, g_global->domain_size.y / 1e5,
                g_global->domain_size.z / 1e5);

    /* domain origin */
    if (!G_MY_MPI_RANK) {
        if (H5LTget_attribute_double(file_id, "/", "DOMAIN_ORIGIN", d3) < 0)
            RERR_H5;
    }
    if (MPI_SUCCESS != MPI_Bcast(d3, 3, MPI_DOUBLE, 0, MPI_COMM_WORLD))
        CERR;
    g_global->domain_origin.x = d3[0];
    g_global->domain_origin.y = d3[1];
    g_global->domain_origin.z = d3[2];
    if (!G_MY_MPI_RANK)
        stk_Add(0, POR_AT, "domain origin: (%12.12e, %12.12e, %12.12e) km",
                g_global->domain_origin.x / 1e5,
                g_global->domain_origin.y / 1e5,
                g_global->domain_origin.z / 1e5);

    /* number of axis nodes */
    int i3[3];
    if (!G_MY_MPI_RANK) {
        if (H5LTget_attribute_int(file_id, "/", "GRID_DIMENSIONS", i3) < 0)
            RERR_H5;
    }
    if (MPI_SUCCESS != MPI_Bcast(i3, 3, MPI_INT, 0, MPI_COMM_WORLD))
        CERR;
    nx = i3[0];
    ny = i3[1];
    nz = i3[2];
    if (!G_MY_MPI_RANK) {
        stk_Add(0, POR_AT, "(nx,ny,nz): (%d,%d,%d)", nx, ny, nz);
        /* check the possibility of parallelization in Z */
        if (!pro_NSubdomainZNodes(nz)) {
            stk_Add(
                0, POR_AT,
                "grid with nz=%d nodes was not parallelizable with previous "
                "PORTA version using %d subdomains",
                nz, G_MPI_L);
        }
    }

    /* coordinates of nodes along axes */
    x = (double *)malloc(nx * sizeof(x));
    y = (double *)malloc(ny * sizeof(y));
    z = (double *)malloc(nz * sizeof(z));
    if (!G_MY_MPI_RANK) {
        if (H5LTget_attribute_double(file_id, "/", "X_AXIS", x) < 0)
            RERR_H5;
        if (H5LTget_attribute_double(file_id, "/", "Y_AXIS", y) < 0)
            RERR_H5;
        if (H5LTget_attribute_double(file_id, "/", "Z_AXIS", z) < 0)
            RERR_H5;
    }
    if (MPI_SUCCESS != MPI_Bcast(x, nx, MPI_DOUBLE, 0, MPI_COMM_WORLD))
        CERR;
    if (MPI_SUCCESS != MPI_Bcast(y, ny, MPI_DOUBLE, 0, MPI_COMM_WORLD))
        CERR;
    if (MPI_SUCCESS != MPI_Bcast(z, nz, MPI_DOUBLE, 0, MPI_COMM_WORLD))
        CERR;

    if (!G_MY_MPI_RANK) {
        stk_Add(0, POR_AT, "mix(x), max(x): %12.12e, %12.12e km", x[0] / 1e5,
                x[nx - 1] / 1e5);
        stk_Add(0, POR_AT, "mix(y), max(y): %12.12e, %12.12e km", y[0] / 1e5,
                y[ny - 1] / 1e5);
        stk_Add(0, POR_AT, "mix(z), max(z): %12.12e, %12.12e km", z[0] / 1e5,
                z[nz - 1] / 1e5);
    }

    /* ray directions */
    if (!G_MY_MPI_RANK) {
        if (H5LTget_attribute_int(file_id, "/", "POLAR_NODES", &i2[0]) < 0)
            RERR_H5;
        if (H5LTget_attribute_int(file_id, "/", "AZIMUTH_NODES", &i2[1]) < 0)
            RERR_H5;
    }

    if (MPI_SUCCESS != MPI_Bcast(i2, 2, MPI_INT, 0, MPI_COMM_WORLD))
        CERR;
    if ((i2[1] == -1) & (i2[0] >= 0)) {
        dir_MakeDirectionsQuadSet(i2[0], &g_global->dirs);
    } else {
        dir_MakeDirections(i2[0], i2[1], &g_global->dirs);
    }

    /* module name */
    if (!G_MY_MPI_RANK) {
        if (H5LTget_attribute_info(file_id, "/", "MODULE_NAME", dimsr,
                                   &type_class, &type_size) < 0)
            RERR_H5;
    }
    if (MPI_SUCCESS != MPI_Bcast(&type_size, 1, MPI_AINT, 0, MPI_COMM_WORLD))
        CERR;
    if (!G_MY_MPI_RANK) {
        if (H5LTget_attribute_string(file_id, "/", "MODULE_NAME", modname) < 0)
            RERR_H5;
    }
    if (MPI_SUCCESS !=
        MPI_Bcast(modname, type_size, MPI_CHAR, 0, MPI_COMM_WORLD))
        CERR;

    /* comment */
    if (!G_MY_MPI_RANK) {
        if (H5LTget_attribute_info(file_id, "/", "MODULE_COMMENT", dimsr,
                                   &type_class, &type_size) < 0)
            RERR_H5;
    }
    if (MPI_SUCCESS != MPI_Bcast(&type_size, 1, MPI_AINT, 0, MPI_COMM_WORLD))
        CERR;
    if (!G_MY_MPI_RANK) {
        if (H5LTget_attribute_string(file_id, "/", "MODULE_COMMENT",
                                     g_global->comment) < 0)
            RERR_H5;
    }
    if (MPI_SUCCESS !=
        MPI_Bcast(g_global->comment, type_size, MPI_CHAR, 0, MPI_COMM_WORLD))
        CERR;

    /* size of module header
       The header size is not needed at all if we only work with HDF5 files, but
       we need to know the header size in case we load a .h5 file but want to
       write a .pmd file */
    if (!G_MY_MPI_RANK) {
        if (H5LTget_attribute_int(file_id, "/", "MODULE_HEADER_SIZE",
                                  &g_global->module_header_fsize) < 0)
            RERR_H5;
    }
    if (MPI_SUCCESS != MPI_Bcast(&g_global->module_header_fsize, 1, MPI_INT, 0,
                                 MPI_COMM_WORLD))
        CERR;

    /* maximum number of grids & current number of grids */
    g_global->max_n_grids     = 1;
    g_global->current_n_grids = 0;
    /* create a grid */
    grd_NewGridC(0, nx, ny, nz, x, y, z);

    free(x);
    free(y);
    free(z);
    free(magic);

    return 1;
}

/*******************************************************************************/
/* reads x-lines into buffers and sends them one by one to a the slaves
 * complementary to LoadNodesSlave()
 * return 0 if an error occures, nonzero otherwise
 */
static int LoadNodesMaster(FILE *f)
{
    int     iy, iz, frank;
    t_grid *g     = &g_global->grid[0];
    int     nsize = g_global->module_node_fsize, linelen = g->nx * nsize;
    char *  buf;

    if (G_MY_MPI_RANK)
        IERR;
    if (!f) {
        Error(E_ERROR, POR_AT, "file is not opened");
        return 0;
    }
    if (!mod_IsModuleLoaded()) {
        Error(E_ERROR, POR_AT, "no module loaded");
        return 0;
    }

    buf = (char *)Malloc(linelen);
    for (iz = 0; iz < g->nz; iz++) {
        int idz1,
            idz2; /* there are possibly two z-domains idz1 and idz2 (+ their
                     frequency bands) which need data for the current z-plane */
        pro_GetIZIndices(g->nz, iz, &idz1, &idz2);
        for (iy = 0; iy < g->ny; iy++) {
            if (fread(buf, 1, linelen, f) != linelen) {
                free(buf);
                Error(E_ERROR, POR_AT,
                      "cannot read from the PMD file [iz=%d, iy=%d]", iz, iy);
                return 0;
            }
            /* send to all the frequency bands of possibly two z-subdomains idz1
             * & idz2 */
            for (frank = idz1; idz1 && (frank < G_MPI_NPROCS);
                 frank += G_MPI_L) {
                if (MPI_SUCCESS != MPI_Send(buf, linelen, MPI_BYTE, frank, 0,
                                            MPI_COMM_WORLD)) {
                    free(buf);
                    CERR;
                    return 0;
                }
            }
            for (frank = idz2; idz2 && (frank < G_MPI_NPROCS);
                 frank += G_MPI_L) {
                if (MPI_SUCCESS != MPI_Send(buf, linelen, MPI_BYTE, frank, 0,
                                            MPI_COMM_WORLD)) {
                    free(buf);
                    CERR;
                    return 0;
                }
            }
        }
    }

    free(buf);
    return 1;
}

/*******************************************************************************/
/* receives the x-line buffers of raw nodes' data and initilizes the appropriate
 * nodes using the module functionality complementary to LoadNodesMaster()
 * return 0 if an error occures, nonzero otherwise
 */
static int LoadNodesSlave(void)
{
    int     min_iz, max_iz, iz, iy, ix;
    t_grid *g   = &g_global->grid[0];
    int   nsize = g_global->afunc.F_GetNodeDataSize(), linelen = g->nx * nsize;
    char *buf, *p;
    MPI_Status status;

    if (!G_MY_MPI_RANK)
        IERR;
    if (!mod_IsModuleLoaded()) {
        Error(E_ERROR, POR_AT, "no module loaded");
        return 0;
    }
    if (!mod_IsModuleLoaded()) {
        Error(E_ERROR, POR_AT, "no module loaded");
        return 0;
    }

    buf = (char *)Malloc(linelen);
    if (!pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz))
        IERR;

    /* regular internal nodes of the subdomain */
    for (iz = min_iz; iz <= max_iz; iz++) {
        if (!g->node[iz])
            IERR;
        for (iy = 0; iy < g->ny; iy++) {
            if (MPI_SUCCESS != MPI_Recv(buf, linelen, MPI_BYTE, 0, 0,
                                        MPI_COMM_WORLD, &status)) {
                free(buf);
                CERR;
                return 0;
            }
            p = buf;
            for (ix = 0; ix < g->nx; ix++) {
                if (!g->node[iz])
                    IERR;
                /*if (g_global->afunc.F_IO_SetNodePMDData(&g->node[iz][iy][ix],
                 * (void*)p) < 0) IERR;*/
                g_global->afunc.F_DecodeNodeData((void *)p,
                                                 &g->node[iz][iy][ix]);
                p += nsize;
            }
        }
    }

    /* outer z-planes min_iz-1 and max_iz+1 overlapping with another domain:
     * only allocate the nodes' memory */
    for (iy = 0; iy < g->ny; iy++) {
        for (ix = 0; ix < g->nx; ix++) {
            if (min_iz > 0)
                g_global->afunc.F_Init_p_data(&g->node[min_iz - 1][iy][ix]);
            if (max_iz < g->nz - 1)
                g_global->afunc.F_Init_p_data(&g->node[max_iz + 1][iy][ix]);
        }
    }

    free(buf);
    return 1;
}

/* pointer function to either io_LoadGridMaster or io_LoadGridMaster_h5 */
int (*io_LoadGridMaster_fp)(char *fname);

/*******************************************************************************/
int io_LoadGridMaster(char *fname)
{
    FILE *f, *f_mod;
    char  modname[IO_PMD_MODULE_LEN + 1];
    char  pmd_header[PMD_HEADER_LEN];
    char *mod_header;

    stk_Add(1, POR_AT, "loading a PMD model from %s", fname);
    if (G_MY_MPI_RANK)
        IERR;
    if (PorMeshInitialized())
        grd_ReleaseDomain();
    if (!(f = OpenFileR(fname))) {
        Error(E_ERROR, POR_AT, "cannot open the file %s", fname);
        return 0;
    }
    if (fread(pmd_header, 1, PMD_HEADER_LEN, f) != PMD_HEADER_LEN) {
        Error(E_ERROR, POR_AT, "cannot read the header data");
        fclose(f);
        return 0;
    }
    if (MPI_SUCCESS !=
        MPI_Bcast(pmd_header, PMD_HEADER_LEN, MPI_BYTE, 0, MPI_COMM_WORLD))
        CERR;
    if (!ReadHeader(pmd_header, modname))
        return 0;

    mod_header = (char *)Malloc(g_global->module_header_fsize);
    if (fread(mod_header, 1, g_global->module_header_fsize, f) !=
        g_global->module_header_fsize) {
        Error(E_ERROR, POR_AT, "cannot read the module header data");
        free(mod_header);
        fclose(f);
        return 0;
    }
    if (MPI_SUCCESS != MPI_Bcast(mod_header, g_global->module_header_fsize,
                                 MPI_BYTE, 0, MPI_COMM_WORLD))
        CERR;
    f_mod = io_OpenTMPStream(g_global->module_header_fsize, mod_header);
    free(mod_header);
    if (!mod_LoadModule(modname, f_mod, &g_global->module_node_fsize))
        RERR;
    if (!io_CloseTMPStream(f_mod))
        return 0;
    /*if (g_global->module_node_fsize != g_global->afunc.F_IO_NodePMDSize())
    {
        Error(E_ERROR, POR_AT, "incompatible length of the node data (header:
    %d, module: %d)", g_global->module_node_fsize,
    g_global->afunc.F_IO_NodePMDSize());
    }*/
    stk_Add(1, POR_AT, "loading nodes data for %dx%dx%d nodes",
            g_global->grid[0].nx, g_global->grid[0].ny, g_global->grid[0].nz);
    if (!LoadNodesMaster(f)) {
        Error(E_ERROR, POR_AT, "cannot load node data from the file");
        return 0;
    }

    fclose(f);
    return 1;
}

/* pointer function to either io_LoadGridSlave or io_LoadGridSlave_h5 */
int (*io_LoadGridSlave_fp)(void);

/*******************************************************************************/
int io_LoadGridSlave(void)
{
    FILE *f_mod;
    char  modname[IO_PMD_MODULE_LEN + 1];
    char  pmd_header[PMD_HEADER_LEN];
    char *mod_header;

    stk_Add(0, POR_AT, "loading a PMD model");
    if (!G_MY_MPI_RANK)
        IERR;
    if (PorMeshInitialized())
        grd_ReleaseDomain();
    if (MPI_SUCCESS !=
        MPI_Bcast(pmd_header, PMD_HEADER_LEN, MPI_BYTE, 0, MPI_COMM_WORLD))
        CERR;
    if (!ReadHeader(pmd_header, modname))
        return 0;

    mod_header = (char *)Malloc(g_global->module_header_fsize);
    if (MPI_SUCCESS != MPI_Bcast(mod_header, g_global->module_header_fsize,
                                 MPI_BYTE, 0, MPI_COMM_WORLD))
        CERR;
    f_mod = io_OpenTMPStream(g_global->module_header_fsize, mod_header);
    free(mod_header);
    if (!mod_LoadModule(modname, f_mod, &g_global->module_node_fsize))
        return 0;
    if (!io_CloseTMPStream(f_mod))
        return 0;

    stk_Add(0, POR_AT, "loading nodes data");
    /* receive the nodes' data from the master process */
    if (!LoadNodesSlave()) {
        Error(E_ERROR, POR_AT, "cannot load nodes");
        return 0;
    }

    /* synchronize the virtual layers node's data */
    pro_SynchronizeOverlappingNodes(&g_global->grid[0]);

    /* synchronize global variables (profile withs etc.) among the MPI processes
     */
    g_global->afunc.F_SyncGlobals();

    return 1;
}

/*****************************************************************************/
/**
 * @brief Loads PMD model in HDF5 format (routine for the slave processes)
 *
 * In this routine (for slaves processes), we need to calculate min_iz and
 * max_iz, the minimum and maximum Z-indices that each process is responsible
 * for. Then each slave reads directly from g_data dataset in the file the layer
 * (hyperslab) corresponding to each Z=iz layer. All the work is done by the
 * slaves and there is no communication with the master, so there is no
 * equivalent master routine, as it is not necessary
 *
 * @param f [in] HDF5 file identifier
 *
 * @return 1 if successful ; 0 otherwise
 *****************************************************************************/
static int LoadNodesSlave_h5(hid_t f)
{
    int        min_iz, max_iz, iz, iy, ix;
    t_grid *   g = &g_global->grid[0];
    char *     buf, *p;
    MPI_Status status;
    hid_t      g_data_set, g_data_type, dspace, mspace;
    hsize_t    start[3], stride[3], count[3], block[3];
    hsize_t    mspace_size;
    size_t     size_g_data;
    herr_t     status_h5;

    if (!G_MY_MPI_RANK)
        IERR;
    if (!mod_IsModuleLoaded()) {
        Error(E_ERROR, POR_AT, "no module loaded");
        return 0;
    }
    if (!mod_IsModuleLoaded()) {
        Error(E_ERROR, POR_AT, "no module loaded");
        return 0;
    }

    if ((g_data_set = H5Dopen(f, "/Module/g_data", H5P_DEFAULT)) < 0) {
        Error(E_ERROR, POR_AT, "cannot open g_data dataset");
        return 0;
    }

    if ((g_data_type = H5Dget_type(g_data_set)) < 0) {
        Error(E_ERROR, POR_AT, "cannot get data type of g_data dataset");
        return 0;
    }

    if ((size_g_data = H5Tget_size(g_data_type)) < 0) {
        Error(E_ERROR, POR_AT,
              "cannot get data size of data type in g_data dataset");
        return 0;
    }

    if ((dspace = H5Dget_space(g_data_set)) < 0) {
        Error(E_ERROR, POR_AT, "cannot get data space of g_data dataset");
        return 0;
    }

    buf = (char *)Malloc(size_g_data * g->nx * g->ny);
    if (!pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz))
        IERR;

    /* regular internal nodes of the subdomain */
    for (iz = min_iz; iz <= max_iz; iz++) {
        if (!g->node[iz])
            IERR;

        /* indices for Z=iz layer hyperslab in the file */
        start[0]  = iz;
        start[1]  = 0;
        start[2]  = 0;
        stride[0] = 1;
        stride[1] = 1;
        stride[2] = 1;
        count[0]  = 1;
        count[1]  = 1;
        count[2]  = 1;
        block[0]  = 1;
        block[1]  = g->ny;
        block[2]  = g->nx;

        if ((status_h5 = H5Sselect_hyperslab(dspace, H5S_SELECT_SET, start,
                                             stride, count, block)) < 0) {
            Error(E_ERROR, POR_AT, "Proc %d hyperslab status ERROR %d \n",
                  G_MY_MPI_RANK, status_h5);
        }

        /* create a simple memory space corresponding to a Z-layer */
        mspace_size = g->nx * g->ny;
        if ((mspace = H5Screate_simple(1, &mspace_size, NULL)) < 0) {
            Error(E_ERROR, POR_AT, "Proc %d memspace creation ERROR %d \n",
                  G_MY_MPI_RANK, mspace);
        }

        // Read the whole Z-plane into buffer 'buf'
        if ((status_h5 = H5Dread(g_data_set, g_data_type, mspace, dspace,
                                 H5P_DEFAULT, buf)) < 0) {
            Error(E_ERROR, POR_AT, "Proc %d read status ERROR %d \n",
                  G_MY_MPI_RANK, status_h5);
        }

        p = buf;
        for (iy = 0; iy < g->ny; iy++) {
            for (ix = 0; ix < g->nx; ix++) {
                if (!g->node[iz])
                    IERR;
                g_global->afunc.F_DecodeNodeData_h5((void *)p,
                                                    &g->node[iz][iy][ix]);
                p += size_g_data;
            }
        }
    }

    /* outer z-planes min_iz-1 and max_iz+1 overlapping with another domain:
     * only allocate the nodes' memory */
    for (iy = 0; iy < g->ny; iy++) {
        for (ix = 0; ix < g->nx; ix++) {
            if (min_iz > 0)
                g_global->afunc.F_Init_p_data(&g->node[min_iz - 1][iy][ix]);
            if (max_iz < g->nz - 1)
                g_global->afunc.F_Init_p_data(&g->node[max_iz + 1][iy][ix]);
        }
    }

    H5Sclose(mspace);
    H5Sclose(dspace);
    H5Tclose(g_data_type);
    H5Dclose(g_data_set);

    free(buf);
    return 1;
}

/*****************************************************************************/
/**
 * @brief Load grid data from a file (master process)
 *
 * This routine will get the module filename, which is broadcast to the slaves.
 * Then collectively the PORTA and module header will be read.
 * (at present the PORTA header is read only by the master process and then
 * broadcasted to the slave, for efficiency, but should be easier to just read
 * all the PORTA header attributes collectively. Need to measure performance)
 * Then, the grid data is read by the slaves.
 *
 * @param fname [in] Name of file to load data from
 *
 * @return 1 if successful
 *****************************************************************************/
int io_LoadGridMaster_h5(char *fname)
{
    hid_t file_id;
    char  modname[IO_PMD_MODULE_LEN + 1];

    if (MPI_SUCCESS !=
        MPI_Bcast(fname, MAX_STRING + 1, MPI_BYTE, 0, MPI_COMM_WORLD))
        CERR;

    if (G_MY_MPI_RANK)
        IERR;
    stk_Add(1, POR_AT, "loading a PMD (HDF5) model from %s", fname);
    if (PorMeshInitialized())
        grd_ReleaseDomain();

    if (!(file_id = OpenFileR_h5(fname))) {
        Error(E_ERROR, POR_AT, "cannot open the file %s", fname);
        return 0;
    }

    /* HDF5 strings generated in Python are not null-terminated, so I initialize
       all to NULL
       This way, either null-terminated or null-padded (created in C) HDF5
       strings will work */
    memset(modname, '\0', IO_PMD_MODULE_LEN + 1);
    if (!ReadHeader_h5(file_id, modname))
        return 0;
    if (!mod_LoadModule_h5(modname, file_id, &g_global->module_node_fsize))
        RERR_H5;

    stk_Add(1, POR_AT, "loading nodes data for %dx%dx%d nodes",
            g_global->grid[0].nx, g_global->grid[0].ny, g_global->grid[0].nz);

    if (H5Fclose(file_id) < 0) {
        Error(E_ERROR, POR_AT, "cannot close the file %s", fname);
        return 0;
    }

    return 1;
}

/*****************************************************************************/
/**
 * @brief Load grid data from a file (slaves processes)
 *
 * This routine will get the module filename from the master process.
 * Then collectively the PORTA and module header will be read.
 * Then, the grid data is read by the slaves (see LoadNodesSlave_h5)
 *
 * @return 1 if successful
 *****************************************************************************/
int io_LoadGridSlave_h5(void)
{
    hid_t file_id;
    char  modname[IO_PMD_MODULE_LEN + 1];
    char  fname[MAX_STRING + 1];

    if (MPI_SUCCESS !=
        MPI_Bcast(fname, MAX_STRING + 1, MPI_BYTE, 0, MPI_COMM_WORLD))
        CERR;

    stk_Add(0, POR_AT, "loading a PMD model");
    if (!G_MY_MPI_RANK)
        IERR;
    if (PorMeshInitialized())
        grd_ReleaseDomain();

    if (!(file_id = OpenFileR_h5(fname))) {
        Error(E_ERROR, POR_AT, "cannot open the file %s", fname);
        return 0;
    }

    /* HDF5 strings generated in Python are not null-terminated, so I initialize
       all to NULL
       This way, either null-terminated or null-padded (created in C) HDF5
       strings will work */
    memset(modname, '\0', IO_PMD_MODULE_LEN + 1);
    if (!ReadHeader_h5(file_id, modname))
        return 0;
    if (!mod_LoadModule_h5(modname, file_id, &g_global->module_node_fsize))
        RERR_H5;

    stk_Add(0, POR_AT, "loading nodes data");
    if (!LoadNodesSlave_h5(file_id)) {
        Error(E_ERROR, POR_AT, "cannot load nodes");
        return 0;
    }

    /* synchronize the virtual layers node's data */
    pro_SynchronizeOverlappingNodes(&g_global->grid[0]);

    /* synchronize global variables (profile withs etc.) among the MPI processes
     */
    g_global->afunc.F_SyncGlobals();

    if (H5Fclose(file_id) < 0) {
        Error(E_ERROR, POR_AT, "cannot close the file %s", fname);
        return 0;
    }

    return 1;
}
