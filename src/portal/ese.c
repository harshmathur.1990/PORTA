#include "ese.h"
#include "def.h"
#include "error.h"
#include "fs.h"
#include "global.h"
#include "modules.h"
#include "process.h"
#include <math.h>

extern t_global_vars *g_global;

/*******************************************************************************/
double ese_SolveGridPrecondESE(t_grid *g)
{
    int    ix, iy, ixy, iz, min_iz, max_iz, min_ese, max_ese;
    int    idz;
    double max_c = 0.0, rc;

    if (!mod_IsModuleLoaded()) {
        Error(E_ERROR, POR_AT, "no atomic module loaded");
    } else if (!PorMeshInitialized()) {
        Error(E_ERROR, POR_AT, "uninitialized grid");
    }

    g_global->afunc.F_ESEBegins();
    idz = pro_Domain_Z_ID(G_MY_MPI_RANK);
    pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz);
    pro_MinMax_ese(G_MY_MPI_RANK, g->nx, g->ny, &min_ese, &max_ese);
    max_iz = (idz < G_MPI_L ? max_iz - 1 : max_iz);

    for (iz = min_iz; iz <= max_iz; iz++) {
#ifdef DEBUG
        if (!g->node[iz])
            IERR;
#endif
        for (ixy = min_ese; ixy <= max_ese; ixy++) {
            ix = ixy % g->nx;
            iy = ixy / g->nx;

            t_node *p_node = &g->node[iz][iy][ix];
#ifdef DEBUG
            if (!p_node->p_data)
                IERR;
#endif
            rc = g_global->afunc.F_SolveESE(p_node);
            if (rc > max_c)
                max_c = rc;
        }
    }
    g_global->afunc.F_ESEEnds();

    pro_CollectDM(g, min_ese, max_ese);

    return max_c;
}

/*******************************************************************************/
double ese_MaxRelativeChange(int n, const int *pops, const double *old_dm,
                             const double *new_dm)
{
    int    i;
    double maxrc = 0.0;

    for (i = 0; i < n; i++) {
        if (pops[i]) {
            double rc = fabs((old_dm[i] - new_dm[i]) / new_dm[i]);
            if (rc > maxrc)
                maxrc = rc;
        }
    }
    return maxrc;
}
