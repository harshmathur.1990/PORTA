/* Math module for Porta */

#ifndef MATIKA_H_
#define MATIKA_H_

#include "def.h"
#include "error.h"
#include <math.h>
#include <stdlib.h>

#ifndef M_PI
#define M_PI (3.141592653589793238462643383)
#endif
#ifndef M_SQRT2
#define M_SQRT2 (1.4142135623730950488016887)
#endif
#ifndef M_SQRT3
#define M_SQRT3 (1.732050807568877293527446)
#endif

/* returns 1.0 if x >= 0, else -1.0 */
#define SIGN(x) ((x) >= 0.0 ? 1.0 : -1.0)

#define MIN2(a, b) ((a) <= (b) ? (a) : (b))
#define MAX2(a, b) ((a) >= (b) ? (a) : (b))

#define MIN3(a, b, c)                                                          \
    ((a) < (b) ? ((a) < (c) ? (a) : (c)) : ((b) < (c) ? (b) : (c)))
#define MID3(a, b, c)                                                          \
    ((((a) < (b) && (a) > (c)) || ((a) < (c) && (a) > (b)))                    \
         ? (a)                                                                 \
         : ((((b) < (a) && (b) > (c)) || ((b) < (c) && (b) > (a))) ? (b)       \
                                                                   : (c)))
#define MAX3(a, b, c)                                                          \
    ((a) > (b) ? ((a) > (c) ? (a) : (c)) : ((b) > (c) ? (b) : (c)))

/* regimes of interpolation: */
#define INTERP_QUAD_MONO                                                       \
    0                 /* quadratic interpolation with monotnonity correction */
#define INTERP_QUAD 1 /* quadratic interpolation */
#define INTERP_LIN  2 /* linear interpolation */

extern double mat_Minimum4(double a, double b, double c, double d);

/************************************************
 *                 random numbers               *
 ************************************************/

/* random number in the interval [0,1] */
#define RAND01 ((double)rand() / (double)RAND_MAX)

/************************************************
 *   complex numbers, powers, arrays, matrices  *
 ************************************************/

/* complex sum \a c=\a a+\a b */
#define CLX_ADD(c, a, b)                                                       \
    {                                                                          \
        (c).re = (a).re + (b).re;                                              \
        (c).im = (a).im + (b).im;                                              \
    }
/* complex subtraction \a c=\a a-\a b */
#define CLX_SUB(c, a, b)                                                       \
    {                                                                          \
        (c).re = (a).re - (b).re;                                              \
        (c).im = (a).im - (b).im;                                              \
    }
/* complex multiplication \a c=\a a*\a b */
#define CLX_MUL(c, a, b)                                                       \
    {                                                                          \
        (c).re = (a).re * (b).re - (a).im * (b).im;                            \
        (c).im = (a).im * (b).re + (a).re * (b).im;                            \
    }
/* complex division \a c=\a a/\a b */
#define CLX_DIV(c, a, b)                                                       \
    {                                                                          \
        double tmp1 = (b).re * (b).re + (b).im * (b).im;                       \
        (c).re      = ((a).re * (b).re + (a).im * (b).im) / tmp1;              \
        (c).im      = ((a).im * (b).re - (a).re * (b).im) / tmp1;              \
    }
/* complex exponential \a a=exp(\a b) */
#define CLX_EXP(a, b)                                                          \
    {                                                                          \
        double tmp1 = exp((b).re);                                             \
        (a).re      = tmp1 * cos((b).im);                                      \
        (a).im      = tmp1 * sin((b).im);                                      \
    }
/* complex conjugate */
#define CLX_CNJ(a)                                                             \
    {                                                                          \
        (a).im = -(a).im;                                                      \
    }

#define SQR(x) ((x) * (x))

/* accurate calculation of exp(-x) for x >= 0 */
extern double mat_AExp(double x);

/* accurate calculation of 1-exp(-x) for x >= 0 */
extern double mat_MExp(double x);

/* x^n for n>=0 */
extern double mat_PowN(double x, int n);

/* allocate double array with n elements and returns a pointer to it */
extern double *ArrayNew(int n);

/* set elements of the double array x with n elements a */
extern void ArraySet(int n, double *x, double a);

/* multiply array x of n doubles by a constant a */
extern void ArrayMultD(int n, double *x, double a);

/* add 2 double arrays x and y, resulting in x = x + y
 *
 * note: x and y must not overlap
 */
extern void ArrayAdd(int n, double *x, const double *y);

/* subtract 2 double arrays x and y, resulting in x = x - y */
extern void ArraySub(int n, double *x, double *y);

/* duplicate the double array a with n elements
 * and return pointer to this newly allocated copy
 */
extern double *ArrayDupl(int n, double *a);

/* returns dot product of two arrays a and b, both with n elements */
extern double DotProduct(int n, double *a, double *b);

/* returns pointer to a newly allocated nrows-times-ncols matrix */
extern double **MatrixNew(int nrows, int ncols);

/* duplicate matrix a of the size nrows-times-ncols */
extern double **MatrixDupl(int nrows, int ncols, double **a);

/* copy matrix a to matrix b */
extern void MatrixCopy(int nrows, int ncols, double **a, double **b);

/* release memory allocated for the nrows-times-ncols matrix m; if m==NULL,
 * nothing happens */
extern void MatrixFree(int nrows, int ncols, double **m);

/* returns pointer to a newly allocated nrows-times-ncols complex matrix */
extern t_complex **MatrixNewC(int nrows, int ncols);

/* release memory allocated for the nrows-times-ncols complex matrix m; if
 * m==NULL, nothing happens */
extern void MatrixFreeC(int nrows, int ncols, t_complex **m);

/* binary search for x in array xi
 *
 * returns highest index in the xi array of the point which is
 * smaller than x; if x lies outside the array, return value is -1
 *
 * note: if x == xi[0] then the return index value is 0
 */
extern int mat_ArraySearch(int n, const double *xi, double x);

/*******************************************************
 *     vector manipulation, computational geometry     *
 *******************************************************/

/* length of a 3d vector */
#define VEC_LEN_3D(v) sqrt((v).x *(v).x + (v).y * (v).y + (v).z * (v).z)

/* check if the 3d vector is of unit length */
#define VEC_IS_UNIT_3D(v)                                                      \
    (fabs(VEC_LEN_3D(v) - 1.0) < 3.0 * DBL_EPSILON ? 1 : 0)

/* distance of two 3d points */
#define DISTANCE3D(a, b)                                                       \
    sqrt(((a).x - (b).x) * ((a).x - (b).x) +                                   \
         ((a).y - (b).y) * ((a).y - (b).y) +                                   \
         ((a).z - (b).z) * ((a).z - (b).z))

/* normalize 3d vector (no error checking) */
#define VEC_NORMALIZE_3D(v)                                                    \
    {                                                                          \
        double tmp_ = 1.0 / VEC_LEN_3D(v);                                     \
        (v).x *= tmp_;                                                         \
        (v).y *= tmp_;                                                         \
        (v).z *= tmp_;                                                         \
    }

/* copy vector b to vector a */
#define VEC_COPY(a, b)                                                         \
    {                                                                          \
        (a).x = (b).x;                                                         \
        (a).y = (b).y;                                                         \
        (a).z = (b).z;                                                         \
    }

/* calculate inclination and azimuth of a unit 3d vector (length is not checked)
 *
 * n: unit vecotor in the 3d space
 *
 * output:
 * theta: inclination of the vector
 * chi:   azimuth of the vector
 */
extern void VecUnit2Ang(const t_vec_3d n, double *theta, double *chi);

/* calculate unit 3d vector from the given inclination and azimuth
 *
 * input:
 * theta: inclination of the vector
 * chi: azimuth of the vector
 *
 * output:
 * v: unit vecotor in the 3d space
 */
extern void Ang2VecUnit(double theta, double chi, t_vec_3d v);

/************************************************
 *              special functions               *
 ************************************************/

/* associated Legendre polynomial P^\a m_\a l(\a x)
 *
 * from Numerical Recipes in C, Press, W.H. (2002)
 */
extern double AssocLegendre(int l, int m, double x);

#ifdef UNDEF
/**
 * real spherical harmonic:
 *
 * Y_\a m\a l = sqrt(2) Re(Y^\a m_\a l)=sqrt(2) N^\a m_\a l cos(\a m\a chi) P^\a
 * m_\a l(cos \a theta) if \a m>0
 *
 * Y_\a m\a l = N^0_\a l P^0_\a l(cos \a theta) if \a m==0
 *
 * Y_\a m\a l = sqrt(2) Im(Y^\a m_\a l) = sqrt(2) N^|\a m |_\a l sin(|\a m |\a
 * chi) P^|\a m |_\a l(cos \a theta) if \a m<0
 *
 * where N^m_\a l is normalization calculated by NormYReal() in matika.c and
 * P^\a m_\a l(cos\a theta) is the associated Legendre polynomial
 * AssocLegendre()
 */
extern double YReal(int l, int m, double theta, double chi);
#endif

/* Exponential integral E1 (see Abramowitz & Stegun) */
extern double ExpIntE1(double x);

/************************************************
 *                  integration                 *
 ************************************************/

/* types of quadratures used in integrate */
enum t_quadrature { Q_UNDEFINED, Q_TRAPEZOIDAL };

/**
 * integral of \a y(\a x) function from \a x[\a first] through \a n points
 * using a specified quadrature; if \a q != NULL then array of quadratures
 * is used instead of \a qt (=faster solution);
 * \a x array can be NULL if \a q is provided
 * if \a equidist != 0 then \a x array is equally spaced and suggests to
 * integrate using a more efficient algorithm
 */
extern double Integrate(int first, int n, const double *x, const double *y,
                        enum t_quadrature qt, int equidist, const double *q);

/**
 * \a n -point Gauss-Laguerre quadrature
 *
 * input:
 * @param n number of ordinates
 * @param x1 lower limit of the interval
 * @param x2 upper limit of the interval
 *
 * output:
 * @param x array of \a n ordinates (must be allocated in advance)
 * @param w array of \a n weights (must be allocated in advance)
 *
 * Note: if x1 > x2 then the \a x values are dereasing from x1 to x2
 *       but the weights have negative sign: you have to multiply them
 *       by -1 in order to get correct weights (only tested for symmetric
 *       intervals like x1=1 and x2=-1).
 */
extern void mat_GauLeg(int n, double x1, double x2, double *x, double *w);

/**
 * adaptive 2d integration on a rectangle using refining Gaussian quadratures
 *
 * input:
 * @param f pointer to a function value at an (x,y) point
 * @param err maximum absolute error of integration
 * @param x1 lower left corner x-coordinate of the domain
 * @param y1 lower left corner y-coordinate of the domain
 * @param x2 upper right corner x-coordinate of the domain
 * @param y2 upper right corner y-coordinate of the domain
 *
 * output:
 * @param val value of the 2d integral
 *
 * @return 0 if desired accuracy can't be reached, otherwise number of gaussian
 * points used per axis in the finest quadrature
 *
 * note: the method should be optimized for speed following the approach of
 * Patterson (1969), Math. Comput. 23, 892; and by precalculating tables of the
 * Gaussian coordinates and weights (static + checking the boundaries?)
 */
extern int Integrate2D(double x1, double y1, double x2, double y2,
                       double (*f)(double, double), double err, double *val);

/************************************************
 *                 Racah algebra                *
 ************************************************/

/** factorial of \a n */
extern double mat_Factorial(int n);

/*
 * 3j symbol
 *
 *              ( ja jb jc )
 *              ( ma mb mc )
 */
extern double mat_Cou3j(int two_ja, int two_jb, int two_jc, int two_ma,
                        int two_mb, int two_mc);

void mat_free_strip6D();

/*
 * 6j symbol
 *
 *              { ja jb jc }
 *              { jd je jf }
 */
extern double mat_Cou6j(int two_ja, int two_jb, int two_jc, int two_jd,
                        int two_je, int two_jf);

/*
 * Racah W-coefficient
 * W(a b c d; e f) =
 *
 *              (-1)^{a+b+c+d}
 *
 *                   { a b e }
 *                   { d c f }
 */
extern double mat_CouRacahW(int two_ja, int two_jb, int two_jc, int two_jd,
                            int two_je, int two_jf);

/*
 * 9j symbol
 *
 *              { ja jb jc }
 *              { jd je jf }
 *              { jg jh ji }
 */
extern double mat_Cou9j(int two_ja, int two_jb, int two_jc, int two_jd,
                        int two_je, int two_jf, int two_jg, int two_jh,
                        int two_ji);

/*
   spherical tensors T^K_Q(Omega) (see Tab. 5.6 of LL04)
   for individual Stokes parameters

   input:
   -- theta, chi: inclination and azimuth of the direction (gamma=0 always)

   output:
   -- T: complex value of the spherical tensor
 */
extern void T20I(double theta, double chi, t_complex *T);
extern void T21I(double theta, double chi, t_complex *T);
extern void T22I(double theta, double chi, t_complex *T);
extern void T20Q(double theta, double chi, t_complex *T);
extern void T21Q(double theta, double chi, t_complex *T);
extern void T22Q(double theta, double chi, t_complex *T);
extern void T21U(double theta, double chi, t_complex *T);
extern void T22U(double theta, double chi, t_complex *T);
extern void T10V(double theta, double chi, t_complex *T);
extern void T11V(double theta, double chi, t_complex *T);

/************************************************
 *                linear algebra                *
 ************************************************/

/* transforms a general complex linear system ac*x=bc into a real system of 2x
 * as big; solution wil be (x[0].re,x[1].re, ..., x[0].im, x[1].im, ...) n: size
 * of the complex linear system
 * */
extern void Complex2RealLinSystem(int n, t_complex **ac, t_complex *bc,
                                  double **ar, double *br);

/* b contains the solution, a is destroyed
 * n: size of matrix and b
 * return value: condition number if singular, 0.0 otherwise
 */
extern double SvdSolve(int n, double **a, double *b);

/* solution of a.x=b set of linear equations by LU decomposition
 * less robust than svdsolve but 30x faster
 * b contains the solution, a is destroyed
 * n: size of matrix and b
 * if corr!=0 then correction of the solution using residuum is performed (slow)
 * return value: condition number if singular, 0.0 otherwise
 *
 * optimized for repetitive solution of the systems with the same dimension
 */
extern void LudSolve(int n, double **a, double *b, int corr);

/*
 * calculates inversion matrix a_inv of square matrix a
 *
 * input:
 * -- n: number of rows (columns)
 * -- a: n-times-n matrix
 *
 * output:
 * -- a_inv: n-times-n inversion matrix of a
 */
extern void LudInverse(int n, double **a, double **a_inv);

/************************************************
 *                interpolation                 *
 ************************************************/

/* linear interpolation */
extern void mat_LinearInterpolation(int nmod, double x, double *y, double x1,
                                    double x2, const double *y1,
                                    const double *y2);

/* bilinear interpolation of nmod models of the 4 rectangle points (x_min,
 * y_min) to (x_max, y_max) */
extern void mat_BilinearInterpolation(int nmod, double x, double y, double *z,
                                      double x_min, double x_max, double y_min,
                                      double y_max, const double *z_xmin_ymin,
                                      const double *z_xmax_ymin,
                                      const double *z_xmin_ymax,
                                      const double *z_xmax_ymax);

/* monotone quadratic Bezier interpolation at point (x,yo) from points (-h0,ym)
 * and (h1,yp); -h0 <= x <= 0 */
extern double QBezier(double h0, double h1, double ym, double yo, double yp,
                      double x);

/* calculates just the control value c0 in the interval (-h0,0) */
extern double mat_QBezierC0(double h0, double h1, double ym, double yo,
                            double yp);

/* calculates just the control values c0 and c1 in the interval (-h0,0) of the
 * cubic Bezier plynomomial */
extern void CBezierC01(double h0, double h1, double ym, double yo, double yp,
                       double *c0, double *c1);

/* bilinear interpolation in the 2D grid mesh
 * nx, ny: number of discrete coordinates
 * xc, yc: these coordinates
 * dx, dy: domain size (in case of non-periodic mesh, dx = x[nx-1]-x[0], dy =
 * y[ny-1]-y[0] z: values at mesh points is_periodic: if nonzero, periodic
 * boundary conditions are assumed x,y: point in which to calculate the
 * interpolation
 */
extern double mat_PlaneInterpolation(int nx, int ny, double *xc, double *yc,
                                     double dx, double dy, double **z,
                                     int is_periodic, double x, double y);

#endif /* MATIKA_H_ */
