#ifndef DEF_H_
#define DEF_H_

#include "def_smode.h"
#include "hdf5.h"
#include "hdf5_hl.h"
#include "thread_io.h"
#include <limits.h>
#include <mpi.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * system
 */

/* Porta is ANSI C89 compliant (almost see modules.c) */
#if !defined __STDC__
#error "ANSI C89 compiler required"
#endif

#if (INT_MAX < 2147483647L)
#error "at least 32-bit integer required"
#endif

/* check the little/big endian of the system: for the file I/O */
#define IS_BIG_ENDIAN ((unsigned char)(*(uint16_t *)"\0\xff" < 0x100))

#define CHRSIZE (sizeof(char))
#define INTSIZE (sizeof(int))
#define DBLSIZE (sizeof(double))
#define FLTSIZE (sizeof(float))
#define CLXSIZE (sizeof(t_complex))

/*
 * Porta version (numbers must fit unsigned char)
 */

#define POR_LABEL "Porta"
#define POR_VER   1 /* backwards-incompatible version */
#define POR_SUBVER                                                             \
    0 /* major or 'feature' release (largely compatible but introduces new     \
         features) */
#define POR_PATCH    4 /* bugfix & optimization release (no new features) */
#define POR_EXTRAVER ""
#define POR_AUTHOR   "The PORTA Authors (see AUTHORS file)"
#define COMMENT_CHAR '#'

/*
 * stack (stack.h, stack.c) and io
 */

#define STACK_ON      /* if STACK_ON is defined then function calls are recorded in \
                         stack.c */
#define STACK_SIZE    64  /* number of records in stack */
#define STACK_MAX_LEN 512 /* maximum length of a stack record */
/* stack output */
#define SOUT_OFF 0 /* turned off      */
#define SOUT_STD 1 /* standard output */
#define SOUT_ERR 2 /* error output    */
#define SOUT_LOG 4 /* log-file        */
#define SOUT_MAX                                                               \
    (SOUT_STD | SOUT_ERR |                                                     \
     SOUT_LOG) /* upper limit of combination of all possible stack outputs */
#define LOG_FILENAME "porta.log" /* filename of the log-file */

/*
 * thread_io
 */

#ifdef THREAD_IO

#ifdef OPEN_MPI
#if OMPI_MAJOR_VERSION < 3
#error ERROR:: This version of OpenMPI is < 3, which has a flaky support for MPI_THREAD_MULTIPLE. Use a more recent OpenMPI version or configure PORTA with --no_thread_io
#endif
#endif

#define IO_MAX_LEN 2048
#define SOUT_IO    1
#define SERR_IO    2
#define SLOG_IO    3
#endif

/*
 * limits
 */

#define MAX_STRING      8191 /* max. characters for command strings */
#define MAX_N_GRIDS     1    /* max. number of grids */
#define MAX_N_ORDINATES 8192 /* max. number of discrete ordinates per axis */
#define MAXNDIRS        8191 /* max. number of rays in the directions sampling */
#define MAXNFREQ        16383 /* max. number of radiation frequencies */
#define MAX_STOKES_BATH                                                        \
    131072 /* max. number of Stokes vectors handled by the module bath.c;      \
              comparable to number of nodes in the XY plane */
#define MAX_KMTX_BATH                                                          \
    65536 /* max. number of absorption matrices handled by the module bath.c;  \
             comparable to number of nodes in the XY plane */
#define MAX_DOUBLE_BATH                                                        \
    32768 /* max. number of double-arrays handled by the module bath.c;        \
             comparable to number of nodes in the XY plane */
#define MAXN_PROFILES                                                          \
    1024 /* max. number of line Voigt profiles handled by voigt_pool.c */

/* orientation of a plane of nodes */
#define ORI_XY 0
#define ORI_XZ 1
#define ORI_YZ 2

/*
 * default settings and switches
 */

/* initial number of ray inclination in one octant */
#define DEF_NDI_INCL 1

/* initial number of ray azimuthal directions in one octant */
#define DEF_NDI_AZIM 1

/* default stack output */
#define DEF_SOUT (SOUT_ERR | SOUT_LOG)

/* if defined, linear formal solver (DELO) is used instead of quadratic */
/*#define FS_LIN*/
/* if defined, the monotone quadratic Bezier is used */
#define FS_BEZIER
/* if defined, the DELOPAR formal solver is used */
/*#define FS_DELOPAR*/

/* check of consistency: only one type of formal sover can be used at a given
 * time */
#if (defined(FS_LIN) && defined(FS_BEZIER)) ||                                 \
    (defined(FS_LIN) && defined(FS_DELOPAR)) ||                                \
    (defined(FS_BEZIER) && defined(FS_DELOPAR))
#error "Multiple formal solvers defined!"
#endif

/* upwind point always on a horizontal plane; slow but may be more accurate
 * and/or stable */
/*#define ALWAYS_TO_AXIS*/

/* directory where to store temporary files with the boundary radiation field
 * (TODO: not in use) */
#define DEF_BC_DIR "./"

/* total number of processes in the parallel solution is L*M+1 where 1 of the
 * master process of rank 0 */
#define DEF_MPI_FREQ_BLOCK                                                     \
    MAXNFREQ /* max. number of frequencies treated simultaneously by one       \
                process (typically set to 1-4 in huge models) */

/*
 * complex number type
 */
typedef struct t_complex {
    double re; /* real part of the number */
    double im; /* imaginary part of the number */
} t_complex;

/* boolean type */
typedef enum boolean { FALSE, TRUE } t_boolean;

#if (!defined(NSTOKES) || (NSTOKES < 1) || (NSTOKES > 4))
#error "invalid number of Stokes parameters"
#endif

/* mean (profile-integrated) radiation radiation field tensors J^K_Q */
#define NJKQ 9
enum {
    J00_RE = 0,
    J10_RE = 1,
    J11_RE = 2,
    J11_IM = 3,
    J20_RE = 4,
    J21_RE = 5,
    J21_IM = 6,
    J22_RE = 7,
    J22_IM = 8
};

typedef struct t_jkq {
    double jkq[NJKQ]; /* real and imaginary parts of the J^K_Q tensor */
} t_jkq;

/*
 * defaults for the line profiles generator
 */

#define LINE_CORE_HALFWIDTH                                                    \
    (3.0)                     /* where line wing starts (Doppler width units) */
#define LINE_WING_STEPF (1.5) /* line wing frequency step factor */
#define PROFILE_COREWING_RATE                                                  \
    (0.75) /* fraction of profile points put into line core instead of wings   \
              (< 1) */
#define PROFILE_ACCURACY_THRESHOLD                                             \
    (0.1) /* warning is printed if abs. profile integral is not well           \
           * normalized                                                        \
           */

/* 3d vector */
typedef struct t_vec_3d {
    double x, y, z;
} t_vec_3d;

typedef struct t_grid t_grid_fwd;
typedef struct t_node t_node_fwd;

typedef struct t_intersect {
    int ix1; /* indices (ix1,iy1,iz1) of the nearby node */
    int iy1;
    int iz1;
    int orientation;   /* orientation of the intersection plane ORI_XY, ORI_XZ,
                          ORI_YZ */
    double   distance; /* distance of the intersection from the node */
    t_vec_3d inters;   /* the coordinates of the intersection point */
} t_intersect;

/* radiation field at a given frequency + transfer coefficients used by the
 * formal solver */
typedef struct t_fsdata {
    double i[NSTOKES]; /* Stokes vector */
    double s[NSTOKES]; /* source function */
    double k[NKMTX];   /* propagation K-matrix */
    double psi_star;   /* diagonal Psi^* (Lambda^*) operator calculated by FS */
} t_fsdata;

/*
 * rectilinear grid nodes
 */
typedef struct t_node {
    t_grid_fwd *p_grid;     /* every node belongs to exacty one grid */
    int         ix, iy, iz; /* node coordinate indices in a given grid */
    void *      p_data;     /* local atomic data */
    t_fsdata    fsdata; /* radiation field and transfer coefficients used by the
                           formal solver */
} t_node;

/* coordinates of a grid node */
#define NODE_X(p_node) ((p_node)->p_grid->x[(p_node)->ix])
#define NODE_Y(p_node) ((p_node)->p_grid->y[(p_node)->iy])
#define NODE_Z(p_node) ((p_node)->p_grid->z[(p_node)->iz])

/*
 * grid
 */
typedef struct t_grid {
    int    nx, ny, nz;         /* number of nodes in every axis direction */
    double x[MAX_N_ORDINATES]; /* arrays of X-node coordinates along axes (valid
                                  length = nx); changed 31/08/2012 */
    double    y[MAX_N_ORDINATES];
    double    z[MAX_N_ORDINATES];
    t_node ***node; /* 3D array of nodes data ordered as [z][y][x]; [z] array is
                       allocated even in the master process but not the grid
                       nodes (those are NULLs) */
} t_grid;

/*
 * directions
 */

/* aliases for components of the T^K_Q(i) tensors */
#define NTKQ    16 /* number of independent real T^K_Q components */
#define T00I_RE 0
#define T20I_RE 1
#define T21I_RE 5
#define T21I_IM 6
#define T22I_RE 2
#define T22I_IM 3
#define T20Q_RE 4
#define T21Q_RE 5
#define T21Q_IM 6
#define T22Q_RE 7
#define T22Q_IM 8
#define T21U_RE 9
#define T21U_IM 10
#define T22U_RE 11
#define T22U_IM 12
#define T10V_RE 13
#define T11V_RE 14
#define T11V_IM 15

/* directions of rays and the direction quadrature; inclination with respect to
 * +Z, azimuth with respect to +X
 *
 * WARNING: any algorithm generating t_directions has to end up with 1st half of
 * the rays going up/down and the 2nd haf going down/up along the Z direction
 */

#define IDIR_UNDEF  -2 /* idir undefined */
#define IDIR_GENDIR -1 /* flag to use a general direction */

typedef struct t_directions {
    int n_incl_oct; /* number of inclination directions per octant */
    int n_azim_oct; /* number of azimuthal directions per octant */
    int n_rays;     /* total number of rays (8*n_incl_oct*n_azim_oct) */
    t_vec_3d
           dir[MAXNDIRS]; /* spatial components of the unit direction vectors */
    double weight[MAXNDIRS];    /* ray quadrature coefficients (sum=1) */
    double tkq[MAXNDIRS][NTKQ]; /* geometrical tensors for NTKQ every ray
                                   direction-Stokes parameter combination */
    int    octant[MAXNDIRS];    /* index of octant to which the ray points to */
    double sin_az[MAXNDIRS];    /* sinus of the direction azimuth */
    double cos_az[MAXNDIRS];    /* cosinus of the direction azimuth */
} t_directions;

#endif /* DEF_H_ */
