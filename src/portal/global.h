/* Global variables */

#ifndef GLOBAL_H_
#define GLOBAL_H_

#include "def.h"
#include "io.h"
#include "portal.h"
#include <stdio.h>

/* global MPI variables */

extern int G_MY_MPI_RANK; /* rank of the current MPI process */
extern int G_MPI_NPROCS;  /* total number of MPI processes = mpi_L*mpi_M+1
                             (including the master node) */
extern MPI_Status G_MPI_STATUS;

extern int G_MPI_L; /* number of horizontal slices (default: DEF_MPI_PROCS_L) */
extern int G_MPI_M; /* number of wavelength blocks per horizontal slice
                       (default: DEF_MPI_PROCS_M) */

extern int G_MY_ZBAND_ID;    /* Z-band of my process [1:G_MPI_L] */
extern int G_MY_FREQBAND_ID; /* Frequency-band of my process [0:G_MPI_M-1] */

/*
 * zband_comm:    communicator with all processes sharing the same Z-band as my
 *                  process
 *
 * freqband_comm: communicator with all processes sharing the same
 * frequency-band as my process
 *
 * slaves_comm:   communicator with all the slaves (all processes but 0)
 */
extern MPI_Comm zband_comm, freqband_comm, slaves_comm;

#ifdef THREAD_IO
extern MPI_Comm io_comm;
#endif

extern FILE *G_POR; /* the model script file */

/* struct for the global variables
 *
 * all global variables are contained except for some constants such
 * as arrays describing topology of the grid etc.
 */
typedef struct {
    FILE *logfile;      /* log-file (stack messages go there by default) */
    int   stack_output; /* bit-wise combination of the SOUT_... constants */

    int    n_freq;         /* total number of frequencies in the model */
    double freq[MAXNFREQ]; /* array of profile frequencies */
    int rfreq[MAXNFREQ];   /* shuflled frequencies indices, to balance frequency
                              bands work */
    t_directions dirs;     /* ray directions and their quadratures */

    t_vec_3d domain_size;   /* x,y,z dimensions of the domain */
    t_vec_3d domain_origin; /* left-bottom-bac corner of the domain */
    char period[2]; /* periodicity 0/1=no/yes in the directions X and Y (index 0
                       and 1 respectively) */
    int max_n_grids;     /* maximum number of grids to be used */
    int current_n_grids; /* current number of grids */
    t_grid
        grid[MAX_N_GRIDS]; /* array of n_grids (defined as 1, but leaving open
                              to increase in the future for Multi-Grid */

    t_afunc afunc; /* functionality provided by the atomic physics code */
    int     module_header_fsize; /* length of the module header */
    int module_node_fsize; /* length of the module node data stored in the PMD
                              file (TODO: OBSOLETE)*/

    int mono_opacity; /* monotone interpolation is also applied to the
                         absorption matrix elements? */
    char comment[PMD_COMMENT_LEN +
                 1]; /* model comment to be stored in the PMD file */
} t_global_vars;

/* global struct (global.c) */
extern t_global_vars *g_global;

/* returns number of seconds which lasted since initialization of the portal
 * library */
extern double LibRunningTime(void);

/* initialize stack, global variables, and random number generator;
 * to be run once, in the very beginning of the program
 */
extern void InitPorta(void);

/* close stack, log file, release memory, quits Porta if stop!=0 */
extern void ExitPorta(int stop);

#endif /* GLOBAL_H_ */
