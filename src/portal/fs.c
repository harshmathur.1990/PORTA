/*
 * Last modification: 14/12/2012
 * Both linear (FS_LIN) and BESSER (FS_BEZIER) formal solvers are
 * implemented
 */

#include "fs.h"
#include "def.h"
#include "directions.h"
#include "error.h"
#include "global.h"
#include "grid.h"
#include "matika.h"
#include "mem.h"
#include "modules.h"
#include "physics.h"
#include "process.h"
#include "stack.h"
#include "topology.h"
#include <float.h>
#include <math.h>
#include <string.h>

#define PERR                                                                   \
    {                                                                          \
        Error(E_ERROR, POR_AT, "This block of code is not yet parallelized!"); \
    }

extern t_global_vars *g_global;

/* forward declarations */
static int ref_point_x(t_grid *g, double x);
static int ref_point_y(t_grid *g, double y);
static int ref_point_z(t_grid *g, double z);
static int NextIntersection(t_grid *g, t_intersect *curr_inters, t_vec_3d *dir,
                            t_intersect *next_inters);

/*******************************************************************************/
/* copy the radiation field at z-plain iz into a linear buffer */
static void FSToBuffer(t_grid *g, int iz, double *buffer)
{
    int       iy, ix;
    const int ncopy =
        DBLSIZE * NSTOKES; /* memory length of the stokes vector */
    if (!g || !buffer || !g->node[iz])
        IERR;
    for (iy = 0; iy < g->ny; iy++) {
        for (ix = 0; ix < g->nx; ix++) {
            memcpy(buffer, g->node[iz][iy][ix].fsdata.i,
                   ncopy); /* stokes vector */
            buffer[NSTOKES] =
                g->node[iz][iy][ix].fsdata.psi_star; /* lambda diagonal */
            buffer += NSTOKES + 1;
        }
    }
}

/*******************************************************************************/
/* copy the linear buffer into the z-plane nodes' radiation data */
static void BufferToFS(t_grid *g, int iz, double *buffer)
{
    int       iy, ix;
    const int ncopy = DBLSIZE * NSTOKES;
    if (!g || !buffer || !g->node[iz])
        IERR;
    for (iy = 0; iy < g->ny; iy++) {
        for (ix = 0; ix < g->nx; ix++) {
            memcpy(g->node[iz][iy][ix].fsdata.i, buffer, ncopy);
            g->node[iz][iy][ix].fsdata.psi_star = buffer[NSTOKES];
            buffer += NSTOKES + 1;
        }
    }
}

/*******************************************************************************/
void fs_ResetRadiationCounters(t_grid *g)
{
    int i, j, k, min_iz, max_iz;

    if (!g)
        IERR;
    pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz);
    for (k = min_iz; k <= max_iz; k++) {
        for (j = 0; j < g->ny; j++) {
            for (i = 0; i < g->nx; i++) {
                g_global->afunc.F_ResetRadiation(&g->node[k][j][i]);
            }
        }
    }
}

/*******************************************************************************/
static void CopyFSData(t_fsdata *dst, t_fsdata *src)
{
#ifdef DEBUG
    if (!dst || !src)
        IERR;
#endif
    Memcpy(dst, src, sizeof(t_fsdata));
}

/*******************************************************************************/
static void CopyIntersection(t_intersect *dst, t_intersect *src)
{
#ifdef DEBUG
    if (!dst || !src)
        IERR;
#endif
    Memcpy(dst, src, sizeof(t_intersect));
}

/* TODO: make the interpolations more efficient: unroll */
/*******************************************************************************/
static void InterpolateRadiationXY(t_grid *g, t_intersect *inters,
                                   t_fsdata *fsdata, int is_stokes)
{
    double  x1, x2, y1, y2, x, y;
    t_node *nod[2][2];
    int     i, j;
#ifdef DEBUG
    if (!inters || !fsdata)
        IERR;
    if (inters->orientation != ORI_XY)
        IERR;
#endif
    x  = inters->inters.x;
    y  = inters->inters.y;
    x1 = grd_XCycl(g, inters->ix1);
    x2 = grd_XCycl(g, inters->ix1 + 1);
    y1 = grd_YCycl(g, inters->iy1);
    y2 = grd_YCycl(g, inters->iy1 + 1);
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 2; j++) {
            nod[i][j] = &g->node[inters->iz1][grd_YIndex(g, inters->iy1 + j)]
                                [grd_XIndex(g, inters->ix1 + i)];
        }
    }
    if (is_stokes) {
        mat_BilinearInterpolation(NSTOKES, x, y, fsdata->i, x1, x2, y1, y2,
                                  nod[0][0]->fsdata.i, nod[1][0]->fsdata.i,
                                  nod[0][1]->fsdata.i, nod[1][1]->fsdata.i);
    }
    mat_BilinearInterpolation(NSTOKES, x, y, fsdata->s, x1, x2, y1, y2,
                              nod[0][0]->fsdata.s, nod[1][0]->fsdata.s,
                              nod[0][1]->fsdata.s, nod[1][1]->fsdata.s);
    mat_BilinearInterpolation(NKMTX, x, y, fsdata->k, x1, x2, y1, y2,
                              nod[0][0]->fsdata.k, nod[1][0]->fsdata.k,
                              nod[0][1]->fsdata.k, nod[1][1]->fsdata.k);
}

/*******************************************************************************/
static void InterpolateRadiationYZ(t_grid *g, t_intersect *inters,
                                   t_fsdata *fsdata, int is_stokes)
{
    double  y1, y2, z1, z2, y, z;
    t_node *nod[2][2];
    int     i, j, xidx;
#ifdef DEBUG
    if (!inters || !fsdata)
        IERR;
    if (inters->orientation != ORI_YZ)
        IERR;
#endif
    xidx = grd_XIndex(g, inters->ix1);
    y1   = grd_YCycl(g, inters->iy1);
    y2   = grd_YCycl(g, inters->iy1 + 1);
    z1   = g->z[inters->iz1];
    z2   = g->z[inters->iz1 + 1];
    y    = inters->inters.y;
    z    = inters->inters.z;
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 2; j++) {
            nod[i][j] =
                &g->node[inters->iz1 + j][grd_YIndex(g, inters->iy1 + i)][xidx];
        }
    }
    if (is_stokes) {
        mat_BilinearInterpolation(NSTOKES, y, z, fsdata->i, y1, y2, z1, z2,
                                  nod[0][0]->fsdata.i, nod[1][0]->fsdata.i,
                                  nod[0][1]->fsdata.i, nod[1][1]->fsdata.i);
    }
    mat_BilinearInterpolation(NSTOKES, y, z, fsdata->s, y1, y2, z1, z2,
                              nod[0][0]->fsdata.s, nod[1][0]->fsdata.s,
                              nod[0][1]->fsdata.s, nod[1][1]->fsdata.s);
    mat_BilinearInterpolation(NKMTX, y, z, fsdata->k, y1, y2, z1, z2,
                              nod[0][0]->fsdata.k, nod[1][0]->fsdata.k,
                              nod[0][1]->fsdata.k, nod[1][1]->fsdata.k);
}

/*******************************************************************************/
static void InterpolateRadiationXZ(t_grid *g, t_intersect *inters,
                                   t_fsdata *fsdata, int is_stokes)
{
    double  x1, x2, z1, z2, x, z;
    t_node *nod[2][2];
    int     i, j, yidx;
#ifdef DEBUG
    if (!inters || !fsdata)
        IERR;
    if (inters->orientation != ORI_XZ)
        IERR;
#endif
    yidx = grd_YIndex(g, inters->iy1);
    x1   = grd_XCycl(g, inters->ix1);
    x2   = grd_XCycl(g, inters->ix1 + 1);
    z1   = g->z[inters->iz1];
    z2   = g->z[inters->iz1 + 1];
    x    = inters->inters.x;
    z    = inters->inters.z;
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 2; j++) {
            nod[i][j] =
                &g->node[inters->iz1 + j][yidx][grd_XIndex(g, inters->ix1 + i)];
        }
    }
    if (is_stokes) {
        mat_BilinearInterpolation(NSTOKES, x, z, fsdata->i, x1, x2, z1, z2,
                                  nod[0][0]->fsdata.i, nod[1][0]->fsdata.i,
                                  nod[0][1]->fsdata.i, nod[1][1]->fsdata.i);
    }
    mat_BilinearInterpolation(NSTOKES, x, z, fsdata->s, x1, x2, z1, z2,
                              nod[0][0]->fsdata.s, nod[1][0]->fsdata.s,
                              nod[0][1]->fsdata.s, nod[1][1]->fsdata.s);
    mat_BilinearInterpolation(NKMTX, x, z, fsdata->k, x1, x2, z1, z2,
                              nod[0][0]->fsdata.k, nod[1][0]->fsdata.k,
                              nod[0][1]->fsdata.k, nod[1][1]->fsdata.k);
}

/*******************************************************************************/
static void InterpolateRadiation(t_grid *g, t_intersect *inters,
                                 t_fsdata *fsdata, int is_stokes)
{
#ifdef DEBUG
    if (!g)
        IERR;
    if (!inters)
        IERR;
    if (!fsdata)
        IERR;
#endif
    switch (inters->orientation) {
    case ORI_XY:
        InterpolateRadiationXY(g, inters, fsdata, is_stokes);
        break;
    case ORI_YZ:
        InterpolateRadiationYZ(g, inters, fsdata, is_stokes);
        break;
    case ORI_XZ:
        InterpolateRadiationXZ(g, inters, fsdata, is_stokes);
        break;
    default:
        IERR;
        break;
    }
#ifdef DEBUG
    /* if (fsdata->k[ETA_I] < 0) IERR; */
#endif
}

/*******************************************************************************/
/* formal solution with linear interpolation of the source function between M
 * and O, ex=exp(-t) */
#define PSI_M_LIN(ex, t)                                                       \
    (t > 0.11                                                                  \
         ? ((1.0 - ex * (1.0 + t)) / t)                                        \
         : ((t * (t * (t * (t * (t * (t * ((63.0 - 8.0 * t) * t - 432.0) +     \
                                      2520.0) -                                \
                                 12096.0) +                                    \
                            45360.0) -                                         \
                       120960.0) +                                             \
                  181440.0)) /                                                 \
            362880.0))
#define PSI_O_LIN(ex, t)                                                       \
    (t > 0.11                                                                  \
         ? ((ex + t - 1.0) / t)                                                \
         : ((t * (t * (t * (t * (t * (t * ((9.0 - t) * t - 72.0) + 504.0) -    \
                                 3024.0) +                                     \
                            15120.0) -                                         \
                       60480.0) +                                              \
                  181440.0)) /                                                 \
            362880.0))

static void FSLin(const t_fsdata *m, t_fsdata *o, double dm)
{
    double tm = dm * 0.5 * (m->k[ETA_I] + o->k[ETA_I] + 2.0 * VACUUM_OPACITY);
    double ex = exp(-tm);
    double psi_m_lin = PSI_M_LIN(ex, tm), psi_o_lin = PSI_O_LIN(ex, tm);
    /* every Stokes mode has it's own implementation in order to optimize the
     * speed */
#if defined(SMODE_I)
    o->i[STOKES_I] = m->i[STOKES_I] * ex + psi_m_lin * m->s[STOKES_I] +
                     psi_o_lin * o->s[STOKES_I];
    o->psi_star = psi_o_lin;

#elif defined(SMODE_IQU_NOSA)
    o->i[STOKES_I] = m->i[STOKES_I] * ex + psi_m_lin * m->s[STOKES_I] +
                     psi_o_lin * o->s[STOKES_I];
    o->i[STOKES_Q] = m->i[STOKES_Q] * ex + psi_m_lin * m->s[STOKES_Q] +
                     psi_o_lin * o->s[STOKES_Q];
    o->i[STOKES_U] = m->i[STOKES_U] * ex + psi_m_lin * m->s[STOKES_U] +
                     psi_o_lin * o->s[STOKES_U];
    o->psi_star = psi_o_lin;

#elif defined(SMODE_IQU_SA)
    double vec[3], kappa[3][3], id;
    double a = -psi_m_lin * m->k[ETA_Q] / (m->k[ETA_I] + VACUUM_OPACITY);
    double b = -psi_m_lin * m->k[ETA_U] / (m->k[ETA_I] + VACUUM_OPACITY);
    vec[0]   = ex * m->i[STOKES_I] + a * m->i[STOKES_Q] + b * m->i[STOKES_U];
    vec[1]   = a * m->i[STOKES_I] + ex * m->i[STOKES_Q];
    vec[2]   = b * m->i[STOKES_I] + ex * m->i[STOKES_U];
    vec[0] += psi_m_lin * m->s[STOKES_I] + psi_o_lin * o->s[STOKES_I];
    vec[1] += psi_m_lin * m->s[STOKES_Q] + psi_o_lin * o->s[STOKES_Q];
    vec[2] += psi_m_lin * m->s[STOKES_U] + psi_o_lin * o->s[STOKES_U];
    a = psi_o_lin * o->k[ETA_Q] / (o->k[ETA_I] + VACUUM_OPACITY);
    b = psi_o_lin * o->k[ETA_U] / (o->k[ETA_I] + VACUUM_OPACITY);
    if (0.0 == 1.0 - a * a - b * b)
        IERR;
    id          = 1.0 / (1.0 - a * a - b * b);
    kappa[0][0] = id;
    kappa[0][1] = -a * id;
    kappa[0][2] = -b * id;
    kappa[1][0] = kappa[0][1];
    kappa[1][1] = (1.0 - b * b) * id;
    kappa[1][2] = a * b * id;
    kappa[2][0] = kappa[0][2];
    kappa[2][1] = kappa[1][2];
    kappa[2][2] = (1.0 - a * a) * id;
    o->i[STOKES_I] =
        kappa[0][0] * vec[0] + kappa[0][1] * vec[1] + kappa[0][2] * vec[2];
    o->i[STOKES_Q] =
        kappa[1][0] * vec[0] + kappa[1][1] * vec[1] + kappa[1][2] * vec[2];
    o->i[STOKES_U] =
        kappa[2][0] * vec[0] + kappa[2][1] * vec[1] + kappa[2][2] * vec[2];
    o->psi_star = psi_o_lin;
#elif defined(SMODE_FULL)
    double vec[4], kappa[4][4], id;
    int    i, j;
    double a = -psi_m_lin * m->k[ETA_Q] / (m->k[ETA_I] + VACUUM_OPACITY);
    double b = -psi_m_lin * m->k[ETA_U] / (m->k[ETA_I] + VACUUM_OPACITY);
    double c = -psi_m_lin * m->k[ETA_V] / (m->k[ETA_I] + VACUUM_OPACITY);
    double s = -psi_m_lin * m->k[RHO_V] / (m->k[ETA_I] + VACUUM_OPACITY);
    double q = -psi_m_lin * m->k[RHO_U] / (m->k[ETA_I] + VACUUM_OPACITY);
    double r = -psi_m_lin * m->k[RHO_Q] / (m->k[ETA_I] + VACUUM_OPACITY);
    vec[0]   = ex * m->i[STOKES_I] + a * m->i[STOKES_Q] + b * m->i[STOKES_U] +
             c * m->i[STOKES_V];
    vec[1] = ex * m->i[STOKES_Q] + a * m->i[STOKES_I] + s * m->i[STOKES_U] -
             q * m->i[STOKES_V];
    vec[2] = ex * m->i[STOKES_U] + b * m->i[STOKES_I] - s * m->i[STOKES_Q] +
             r * m->i[STOKES_V];
    vec[3] = ex * m->i[STOKES_V] + c * m->i[STOKES_I] + q * m->i[STOKES_Q] -
             r * m->i[STOKES_U];

    vec[0] += psi_m_lin * m->s[STOKES_I] + psi_o_lin * o->s[STOKES_I];
    vec[1] += psi_m_lin * m->s[STOKES_Q] + psi_o_lin * o->s[STOKES_Q];
    vec[2] += psi_m_lin * m->s[STOKES_U] + psi_o_lin * o->s[STOKES_U];
    vec[3] += psi_m_lin * m->s[STOKES_V] + psi_o_lin * o->s[STOKES_V];

    a = psi_o_lin * o->k[ETA_Q] / (o->k[ETA_I] + VACUUM_OPACITY);
    b = psi_o_lin * o->k[ETA_U] / (o->k[ETA_I] + VACUUM_OPACITY);
    c = psi_o_lin * o->k[ETA_V] / (o->k[ETA_I] + VACUUM_OPACITY);
    s = psi_o_lin * o->k[RHO_V] / (o->k[ETA_I] + VACUUM_OPACITY);
    q = psi_o_lin * o->k[RHO_U] / (o->k[ETA_I] + VACUUM_OPACITY);
    r = psi_o_lin * o->k[RHO_Q] / (o->k[ETA_I] + VACUUM_OPACITY);

    id = -c * c * s * s + s * s - 2.0 * a * c * r * s - 2.0 * b * c * q * s -
         a * a * r * r + r * r - 2.0 * a * b * q * r - b * b * q * q + q * q -
         c * c - b * b - a * a + 1.0;
    if (0.0 == id)
        IERR;
    id          = 1.0 / id;
    kappa[0][0] = s * s + r * r + q * q + 1.0;
    kappa[0][1] = -c * r * s - b * s - a * r * r - b * q * r + c * q - a;
    kappa[0][2] = -c * q * s + a * s - a * q * r - c * r - b * q * q - b;
    kappa[0][3] = -c * s * s - a * r * s - b * q * s + b * r - a * q - c;
    kappa[1][0] = -c * r * s + b * s - a * r * r - b * q * r - c * q - a;
    kappa[1][1] = r * r - c * c - b * b + 1.0;
    kappa[1][2] = c * c * s - s + q * r + a * c * r + b * c * q + a * b;
    kappa[1][3] = r * s - b * c * s - a * b * r - b * b * q + q + a * c;
    kappa[2][0] = -c * q * s - a * s - a * q * r + c * r - b * q * q - b;
    kappa[2][1] = -c * c * s + s + q * r - a * c * r - b * c * q + a * b;
    kappa[2][2] = q * q - c * c - a * a + 1.0;
    kappa[2][3] = q * s + a * c * s + a * a * r - r + a * b * q + b * c;
    kappa[3][0] = -c * s * s - a * r * s - b * q * s - b * r + a * q - c;
    kappa[3][1] = r * s + b * c * s + a * b * r + b * b * q - q + a * c;
    kappa[3][2] = q * s - a * c * s - a * a * r + r - a * b * q + b * c;
    kappa[3][3] = s * s - b * b - a * a + 1.0;
    for (j = 0; j < 4; j++)
        for (i = 0; i < 4; i++)
            kappa[j][i] *= id;

    o->i[STOKES_I] = kappa[0][0] * vec[0] + kappa[0][1] * vec[1] +
                     kappa[0][2] * vec[2] + kappa[0][3] * vec[3];
    o->i[STOKES_Q] = kappa[1][0] * vec[0] + kappa[1][1] * vec[1] +
                     kappa[1][2] * vec[2] + kappa[1][3] * vec[3];
    o->i[STOKES_U] = kappa[2][0] * vec[0] + kappa[2][1] * vec[1] +
                     kappa[2][2] * vec[2] + kappa[2][3] * vec[3];
    o->i[STOKES_V] = kappa[3][0] * vec[0] + kappa[3][1] * vec[1] +
                     kappa[3][2] * vec[2] + kappa[3][3] * vec[3];
    o->psi_star = psi_o_lin;
#else
#error "ERROR: unknown/unimplemented Stokes mode"
#endif
}

#if defined(FS_DELOPAR)
/*******************************************************************************/
static void PSI_MOP(double ex, double tm, double tp, double *psi_m,
                    double *psi_o, double *psi_p)
{
    double w0 = 1.0 - ex, w1 = w0 - tm * ex, w2 = 2.0 * w1 - tm * tm * ex;
    *psi_m =
        tm > 0.10
            ? (w2 + w1 * tp) / (tm * (tm + tp))
            : (tm * tm *
                   (tm * (tm * (tm * (tm * (tm * (7.0 * tm - 48.0) + 280.0) -
                                      1344.0) +
                                5040.0) -
                          13440.0) +
                    20160.0) *
                   tp +
               tm * tm * tm *
                   (tm * (tm * (tm * ((240.0 - 42.0 * tm) * tm - 1120.0) +
                                4032.0) -
                          10080.0) +
                    13440.0)) /
                  40320.0 / (tm * (tm + tp));
    *psi_o =
        tm > 0.10
            ? w0 + (w1 * (tm - tp) - w2) / (tm * tp)
            : (tm *
               (tm * (tm * (tm * (tm * (tm * ((8.0 - tm) * tm - 56.0) + 336.0) -
                                  1680.0) +
                            6720.0) -
                      20160.0) +
                40320.0)) /
                      40320.0 +
                  ((tm * tm *
                        (tm * (tm * (tm * (tm * ((48.0 - 7.0 * tm) * tm -
                                                 280.0) +
                                           1344.0) -
                                     5040.0) +
                               13440.0) -
                         20160.0) *
                        tp +
                    tm * tm * tm *
                        (tm * (tm * (tm * ((40.0 - 6.0 * tm) * tm - 224.0) +
                                     1008.0) -
                               3360.0) +
                         6720.0)) /
                   40320.0) /
                      (tm * tp);
    *psi_p =
        tm > 0.10
            ? (w2 - w1 * tm) / (tp * (tm + tp))
            : ((tm * tm * tm *
                (tm * (tm * (tm * (tm * (3.0 * tm - 20.0) + 112.0) - 504.0) +
                       1680.0) -
                 3360.0)) /
               20160.0) /
                  (tp * (tm + tp));
}

/*******************************************************************************/
static void FSDELOPAR(const t_fsdata *m, t_fsdata *o, const t_fsdata *p,
                      double dm, double dp)
{
    double tm = dm * 0.5 * (m->k[ETA_I] + o->k[ETA_I] + 2.0 * VACUUM_OPACITY);
    double tp = dp * 0.5 * (p->k[ETA_I] + o->k[ETA_I] + 2.0 * VACUUM_OPACITY);
    double ex = exp(-tm);
    double psi_m, psi_o, psi_p;
#ifdef DEBUG
    /*
    if (!m || !o || !p) IERR;
    if (dm <= 0.0 || dp <= 0.0) IERR;
    */
#endif
    /* every Stokes mode has it's own implementation in order to optimize the
     * speed */
#ifdef SMODE_I
    PSI_MOP(ex, tm, tp, &psi_m, &psi_o, &psi_p);
    o->i->i[STOKES_I][f] =
        m->i->i[STOKES_I][f] * ex + psi_m * m->sf->i[STOKES_I][f] +
        psi_o * o->sf->i[STOKES_I][f] + psi_p * p->sf->i[STOKES_I][f];
    o->psi_star[f] = psi_o;
#elif defined(SMODE_IQU_NOSA)
    PSI_MOP(ex, tm, tp, &psi_m, &psi_o, &psi_p);
    o->i[STOKES_I] = m->i[STOKES_I] * ex + psi_m * m->s[STOKES_I] +
                     psi_o * o->s[STOKES_I] + psi_p * p->s[STOKES_I];
    o->i[STOKES_Q] = m->i[STOKES_Q] * ex + psi_m * m->s[STOKES_Q] +
                     psi_o * o->s[STOKES_Q] + psi_p * p->s[STOKES_Q];
    o->i[STOKES_U] = m->i[STOKES_U] * ex + psi_m * m->s[STOKES_U] +
                     psi_o * o->s[STOKES_U] + psi_p * p->s[STOKES_U];
    o->psi_star = psi_o;
#elif defined(SMODE_IQU_SA)
    double psi_m_lin = PSI_M_LIN(ex, tm), psi_o_lin = PSI_O_LIN(ex, tm);
    double vec[3], kappa[3][3], id;
    double a = -psi_m_lin * m->k[ETA_Q] / (m->k[ETA_I] + VACUUM_OPACITY);
    double b = -psi_m_lin * m->k[ETA_U] / (m->k[ETA_I] + VACUUM_OPACITY);
    PSI_MOP(ex, tm, tp, &psi_m, &psi_o, &psi_p);
    vec[0] = ex * m->i[STOKES_I] + a * m->i[STOKES_Q] + b * m->i[STOKES_U];
    vec[1] = a * m->i[STOKES_I] + ex * m->i[STOKES_Q];
    vec[2] = b * m->i[STOKES_I] + ex * m->i[STOKES_U];
    vec[0] += psi_m * m->s[STOKES_I] + psi_o * o->s[STOKES_I] +
              psi_p * p->s[STOKES_I];
    vec[1] += psi_m * m->s[STOKES_Q] + psi_o * o->s[STOKES_Q] +
              psi_p * p->s[STOKES_Q];
    vec[2] += psi_m * m->s[STOKES_U] + psi_o * o->s[STOKES_U] +
              psi_p * p->s[STOKES_U];
    a = psi_o_lin * o->k[ETA_Q] / (o->k[ETA_I] + VACUUM_OPACITY);
    b = psi_o_lin * o->k[ETA_U] / (o->k[ETA_I] + VACUUM_OPACITY);
    if (0.0 == 1.0 - a * a - b * b)
        IERR;
    id          = 1.0 / (1.0 - a * a - b * b);
    kappa[0][0] = id;
    kappa[0][1] = -a * id;
    kappa[0][2] = -b * id;
    kappa[1][0] = kappa[0][1];
    kappa[1][1] = (1.0 - b * b) * id;
    kappa[1][2] = a * b * id;
    kappa[2][0] = kappa[0][2];
    kappa[2][1] = kappa[1][2];
    kappa[2][2] = (1.0 - a * a) * id;
    o->i[STOKES_I] =
        kappa[0][0] * vec[0] + kappa[0][1] * vec[1] + kappa[0][2] * vec[2];
    o->i[STOKES_Q] =
        kappa[1][0] * vec[0] + kappa[1][1] * vec[1] + kappa[1][2] * vec[2];
    o->i[STOKES_U] =
        kappa[2][0] * vec[0] + kappa[2][1] * vec[1] + kappa[2][2] * vec[2];
    o->psi_star = psi_o;
#elif defined(SMODE_FULL)
#error "ERROR: not yet implemented!"
#else
#error "ERROR: unknown/unimplemented Stokes mode"
#endif
}

#elif defined(FS_BEZIER)

/*******************************************************************************/
/* quadratic-accuracy monotone Bezier formal solution of Stokes vector and Psi^*
 * at the point o from data of the successive points m, o, p (o->i and
 * o->psi_star are calculated); dm, dp are geometrical distances m-o and o-p;
 * data to be initialized in advance: (i,sf,kmtx) for m, (sf,kmtx) for p,
 * (sk,kmtx) for o;
 */
#define OMEGA_M(t)                                                             \
    (t > 0.14                                                                  \
         ? ((2.0 - ex * (t * t + 2.0 * t + 2.0)) / (t * t))                    \
         : ((t * (t * (t * (t * (t * (t * ((140.0 - 18.0 * t) * t - 945.0) +   \
                                      5400.0) -                                \
                                 25200.0) +                                    \
                            90720.0) -                                         \
                       226800.0) +                                             \
                  302400.0)) /                                                 \
            907200.0))
#define OMEGA_O(t)                                                             \
    (t > 0.18                                                                  \
         ? (1.0 - 2.0 * (t + ex - 1.0) / (t * t))                              \
         : ((t * (t * (t * (t * (t * (t * ((10.0 - t) * t - 90.0) + 720.0) -   \
                                 5040.0) +                                     \
                            30240.0) -                                         \
                       151200.0) +                                             \
                  604800.0)) /                                                 \
            1814400.0))
#define OMEGA_C(t)                                                             \
    (t > 0.18                                                                  \
         ? (2.0 * (t - 2.0 + ex * (t + 2.0)) / (t * t))                        \
         : ((t * (t * (t * (t * (t * (t * ((35.0 - 4.0 * t) * t - 270.0) +     \
                                      1800.0) -                                \
                                 10080.0) +                                    \
                            45360.0) -                                         \
                       151200.0) +                                             \
                  302400.0)) /                                                 \
            907200.0))
static void FSBezier(const t_fsdata *m, t_fsdata *o, const t_fsdata *p,
                     double dm, double dp)
{
    double tm = dm * 0.5 * (m->k[ETA_I] + o->k[ETA_I] + 2.0 * VACUUM_OPACITY);
    double tp = dp * 0.5 * (p->k[ETA_I] + o->k[ETA_I] + 2.0 * VACUUM_OPACITY);
    double ex = exp(-tm);
    double om_m = OMEGA_M(tm), om_o = OMEGA_O(tm), om_c = OMEGA_C(tm);

    /*
    #ifdef DEBUG
        if (!m || !o || !p) IERR;
        if (dm <= 0.0 || dp <= 0.0) IERR;
    #endif
    */

    /* every Stokes mode has it's own implementation in order to optimize the
     * speed of calculation */
#if defined(SMODE_I)
    double c0;
    c0 = mat_QBezierC0(tm, tp, m->s[STOKES_I], o->s[STOKES_I], p->s[STOKES_I]);
    /* Stokes I: */
    o->i[STOKES_I] = m->i[STOKES_I] * ex + om_m * m->s[STOKES_I] +
                     om_o * o->s[STOKES_I] + om_c * c0;
    /* operator diagonal: */
    o->psi_star = om_o + om_c;
#elif defined(SMODE_IQU_NOSA)
    {
        double c0;
        c0             = mat_QBezierC0(tm, tp, m->s[STOKES_I], o->s[STOKES_I],
                           p->s[STOKES_I]);
        o->i[STOKES_I] = m->i[STOKES_I] * ex + om_m * m->s[STOKES_I] +
                         om_o * o->s[STOKES_I] + om_c * c0;
        c0             = mat_QBezierC0(tm, tp, m->s[STOKES_Q], o->s[STOKES_Q],
                           p->s[STOKES_Q]);
        o->i[STOKES_Q] = m->i[STOKES_Q] * ex + om_m * m->s[STOKES_Q] +
                         om_o * o->s[STOKES_Q] + om_c * c0;
        c0             = mat_QBezierC0(tm, tp, m->s[STOKES_U], o->s[STOKES_U],
                           p->s[STOKES_U]);
        o->i[STOKES_U] = m->i[STOKES_U] * ex + om_m * m->s[STOKES_U] +
                         om_o * o->s[STOKES_U] + om_c * c0;
        o->psi_star = om_o + om_c;
    }
#elif defined(SMODE_IQU_SA)
    {
        double psi_m_lin = PSI_M_LIN(ex, tm), psi_o_lin = PSI_O_LIN(ex, tm);
        double vec[3], kappa[3][3], id, a, b;
        double c0[3];
        c0[0]  = mat_QBezierC0(tm, tp, m->s[STOKES_I], o->s[STOKES_I],
                              p->s[STOKES_I]);
        c0[1]  = mat_QBezierC0(tm, tp, m->s[STOKES_Q], o->s[STOKES_Q],
                              p->s[STOKES_Q]);
        c0[2]  = mat_QBezierC0(tm, tp, m->s[STOKES_U], o->s[STOKES_U],
                              p->s[STOKES_U]);
        a      = -psi_m_lin * m->k[ETA_Q] / (m->k[ETA_I] + VACUUM_OPACITY);
        b      = -psi_m_lin * m->k[ETA_U] / (m->k[ETA_I] + VACUUM_OPACITY);
        vec[0] = ex * m->i[STOKES_I] + a * m->i[STOKES_Q] + b * m->i[STOKES_U];
        vec[1] = ex * m->i[STOKES_Q] + a * m->i[STOKES_I];
        vec[2] = ex * m->i[STOKES_U] + b * m->i[STOKES_I];
        vec[0] += om_m * m->s[STOKES_I] + om_o * o->s[STOKES_I] + om_c * c0[0];
        vec[1] += om_m * m->s[STOKES_Q] + om_o * o->s[STOKES_Q] + om_c * c0[1];
        vec[2] += om_m * m->s[STOKES_U] + om_o * o->s[STOKES_U] + om_c * c0[2];
        a = psi_o_lin * o->k[ETA_Q] / (o->k[ETA_I] + VACUUM_OPACITY);
        b = psi_o_lin * o->k[ETA_U] / (o->k[ETA_I] + VACUUM_OPACITY);
        if (0.0 == 1.0 - a * a - b * b)
            IERR;
        id          = 1.0 / (1.0 - a * a - b * b);
        kappa[0][0] = id;
        kappa[0][1] = -a * id;
        kappa[0][2] = -b * id;
        kappa[1][0] = kappa[0][1];
        kappa[1][1] = (1.0 - b * b) * id;
        kappa[1][2] = a * b * id;
        kappa[2][0] = kappa[0][2];
        kappa[2][1] = kappa[1][2];
        kappa[2][2] = (1.0 - a * a) * id;
        o->i[STOKES_I] =
            kappa[0][0] * vec[0] + kappa[0][1] * vec[1] + kappa[0][2] * vec[2];
        o->i[STOKES_Q] =
            kappa[1][0] * vec[0] + kappa[1][1] * vec[1] + kappa[1][2] * vec[2];
        o->i[STOKES_U] =
            kappa[2][0] * vec[0] + kappa[2][1] * vec[1] + kappa[2][2] * vec[2];
        o->psi_star = om_o + om_c;
    }
#elif defined(SMODE_FULL)
    double vec[4], kappa[4][4], id, c0[4];
    int    i, j;
    double psi_m_lin = PSI_M_LIN(ex, tm), psi_o_lin = PSI_O_LIN(ex, tm);
    double a = -psi_m_lin * m->k[ETA_Q] / (m->k[ETA_I] + VACUUM_OPACITY);
    double b = -psi_m_lin * m->k[ETA_U] / (m->k[ETA_I] + VACUUM_OPACITY);
    double c = -psi_m_lin * m->k[ETA_V] / (m->k[ETA_I] + VACUUM_OPACITY);
    double s = -psi_m_lin * m->k[RHO_V] / (m->k[ETA_I] + VACUUM_OPACITY);
    double q = -psi_m_lin * m->k[RHO_U] / (m->k[ETA_I] + VACUUM_OPACITY);
    double r = -psi_m_lin * m->k[RHO_Q] / (m->k[ETA_I] + VACUUM_OPACITY);

    c0[0] =
        mat_QBezierC0(tm, tp, m->s[STOKES_I], o->s[STOKES_I], p->s[STOKES_I]);
    c0[1] =
        mat_QBezierC0(tm, tp, m->s[STOKES_Q], o->s[STOKES_Q], p->s[STOKES_Q]);
    c0[2] =
        mat_QBezierC0(tm, tp, m->s[STOKES_U], o->s[STOKES_U], p->s[STOKES_U]);
    c0[3] =
        mat_QBezierC0(tm, tp, m->s[STOKES_V], o->s[STOKES_V], p->s[STOKES_V]);

    vec[0] = ex * m->i[STOKES_I] + a * m->i[STOKES_Q] + b * m->i[STOKES_U] +
             c * m->i[STOKES_V];
    vec[1] = ex * m->i[STOKES_Q] + a * m->i[STOKES_I] + s * m->i[STOKES_U] -
             q * m->i[STOKES_V];
    vec[2] = ex * m->i[STOKES_U] + b * m->i[STOKES_I] - s * m->i[STOKES_Q] +
             r * m->i[STOKES_V];
    vec[3] = ex * m->i[STOKES_V] + c * m->i[STOKES_I] + q * m->i[STOKES_Q] -
             r * m->i[STOKES_U];

    vec[0] += om_m * m->s[STOKES_I] + om_o * o->s[STOKES_I] + om_c * c0[0];
    vec[1] += om_m * m->s[STOKES_Q] + om_o * o->s[STOKES_Q] + om_c * c0[1];
    vec[2] += om_m * m->s[STOKES_U] + om_o * o->s[STOKES_U] + om_c * c0[2];
    vec[3] += om_m * m->s[STOKES_V] + om_o * o->s[STOKES_V] + om_c * c0[3];

    id = psi_o_lin / (o->k[ETA_I] + VACUUM_OPACITY);
    a  = id * o->k[ETA_Q];
    b  = id * o->k[ETA_U];
    c  = id * o->k[ETA_V];
    s  = id * o->k[RHO_V];
    q  = id * o->k[RHO_U];
    r  = id * o->k[RHO_Q];

    id = -c * c * s * s + s * s - 2.0 * a * c * r * s - 2.0 * b * c * q * s -
         a * a * r * r + r * r - 2.0 * a * b * q * r - b * b * q * q + q * q -
         c * c - b * b - a * a + 1.0;
    if (0.0 == id)
        IERR;
    id          = 1.0 / id;
    kappa[0][0] = s * s + r * r + q * q + 1.0;
    kappa[0][1] = -c * r * s - b * s - a * r * r - b * q * r + c * q - a;
    kappa[0][2] = -c * q * s + a * s - a * q * r - c * r - b * q * q - b;
    kappa[0][3] = -c * s * s - a * r * s - b * q * s + b * r - a * q - c;
    kappa[1][0] = -c * r * s + b * s - a * r * r - b * q * r - c * q - a;
    kappa[1][1] = r * r - c * c - b * b + 1.0;
    kappa[1][2] = c * c * s - s + q * r + a * c * r + b * c * q + a * b;
    kappa[1][3] = r * s - b * c * s - a * b * r - b * b * q + q + a * c;
    kappa[2][0] = -c * q * s - a * s - a * q * r + c * r - b * q * q - b;
    kappa[2][1] = -c * c * s + s + q * r - a * c * r - b * c * q + a * b;
    kappa[2][2] = q * q - c * c - a * a + 1.0;
    kappa[2][3] = q * s + a * c * s + a * a * r - r + a * b * q + b * c;
    kappa[3][0] = -c * s * s - a * r * s - b * q * s - b * r + a * q - c;
    kappa[3][1] = r * s + b * c * s + a * b * r + b * b * q - q + a * c;
    kappa[3][2] = q * s - a * c * s - a * a * r + r - a * b * q + b * c;
    kappa[3][3] = s * s - b * b - a * a + 1.0;
    for (j = 0; j < 4; j++)
        for (i = 0; i < 4; i++)
            kappa[j][i] *= id;

    o->i[STOKES_I] = kappa[0][0] * vec[0] + kappa[0][1] * vec[1] +
                     kappa[0][2] * vec[2] + kappa[0][3] * vec[3];
    o->i[STOKES_Q] = kappa[1][0] * vec[0] + kappa[1][1] * vec[1] +
                     kappa[1][2] * vec[2] + kappa[1][3] * vec[3];
    o->i[STOKES_U] = kappa[2][0] * vec[0] + kappa[2][1] * vec[1] +
                     kappa[2][2] * vec[2] + kappa[2][3] * vec[3];
    o->i[STOKES_V] = kappa[3][0] * vec[0] + kappa[3][1] * vec[1] +
                     kappa[3][2] * vec[2] + kappa[3][3] * vec[3];
    o->psi_star = om_o + om_c;
#else
#error "ERROR: unknown/unimplemented Stokes mode"
#endif
}
#undef OMEGA_M
#undef OMEGA_O
#undef OMEGA_C

#elif !defined(FS_LIN) /* unknown formal solver */

#error "Undefined formal solver!"

#endif

/*******************************************************************************/
/* gives the first and last indexes in aech axis where to start and end the
 * formal solution; dix, diy, diz: stepping directions in FS */
static void GetSweepIndices(t_grid *g, int *ix_first, int *iy_first,
                            int *iz_first, int *ix_last, int *iy_last,
                            int *iz_last, int *dix, int *diy, int *diz,
                            const t_vec_3d *ray)
{
    int min_iz, max_iz;

    pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz);
    if (ray->z > 0.0) {
        *iz_first = min_iz;
        *iz_last  = max_iz;
        *diz      = 1;
    } else {
        *iz_first = max_iz;
        *iz_last  = min_iz;
        *diz      = -1;
    }

    if (ray->x > 0.0) {
        *ix_first = 0;
        *ix_last  = g->nx - 1;
        *dix      = 1;
    } else {
        *ix_first = g->nx - 1;
        *ix_last  = 0;
        *dix      = -1;
    }

    if (ray->y > 0.0) {
        *iy_first = 0;
        *iy_last  = g->ny - 1;
        *diy      = 1;
    } else {
        *iy_first = g->ny - 1;
        *iy_last  = 0;
        *diy      = -1;
    }
}

/*******************************************************************************/
/* initialize the z-boundary radiation of the current domain using the module
 * external illumination if dir!=NULL then iray is ignored and a general
 * direction is used instead
 */
static void SetBoundaryIllumination(t_grid *g, int iz, int iray, t_vec_3d *dir,
                                    int ifreq)
{
    int      ix, iy;
    t_vec_3d ray;

    if (!dir) {
        ray.x = g_global->dirs.dir[iray].x;
        ray.y = g_global->dirs.dir[iray].y;
        ray.z = g_global->dirs.dir[iray].z;
    } else {
        ray.x = dir->x;
        ray.y = dir->y;
        ray.z = dir->z;
    }
#ifdef DEBUG
    {
        int min_iz, max_iz;
        pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz);
        if (iz != min_iz && iz != max_iz)
            IERR;
    }
#endif
    for (iy = 0; iy < g->ny; iy++) {
        for (ix = 0; ix < g->nx; ix++) {
            t_node * p_node = &g->node[iz][iy][ix];
            t_vec_3d r;
            r.x = NODE_X(p_node);
            r.y = NODE_Y(p_node);
            r.z = NODE_Z(p_node);
            g_global->afunc.F_ExternalIllum(r, ray, ifreq, p_node->fsdata.i);
            p_node->fsdata.psi_star = 0.0;
        }
    }
}

/*******************************************************************************/
static void CalculateZLayerSFKmtx(t_grid *g, int iz, int iray, int ifreq)
{
    int ix, iy;
#ifdef DEBUG
    /**/
    {
        int min_iz, max_iz;
        pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz);
        if (iz < min_iz - 1 || iz > max_iz + 1 || iz < 0 || iz >= g->nz)
            IERR;
    }
#endif
    for (iy = 0; iy < g->ny; iy++) {
        for (ix = 0; ix < g->nx; ix++) {
            t_node *p_node;
#ifdef DEBUG
            if (!g || !g->node || !g->node[iz] || !g->node[iz][iy] ||
                !g->node[iz][iy][ix].p_data)
                IERR;
#endif
            p_node = &g->node[iz][iy][ix];
            g_global->afunc.F_SK(p_node, p_node->fsdata.s, p_node->fsdata.k,
                                 iray, ifreq);
        }
    }
    /*PrintfErr("  .%d, iz=%d idir=%d ifreq=%d\n", G_MY_MPI_RANK, iz, iray,
     * ifreq);*/
}

/*******************************************************************************/
static void AddZLayerContributions(t_grid *g, int iz, int iray, int ifreq)
{
    int ix, iy;
#ifdef DEBUG
    /**/
    {
        int min_iz, max_iz;
        pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz);
        if (iz < min_iz || iz > max_iz)
            IERR;
        if (ifreq < 0 || ifreq >= g_global->n_freq)
            IERR;
    }
#endif
    for (iy = 0; iy < g->ny; iy++) {
        for (ix = 0; ix < g->nx; ix++) {
            t_node *p_node = &g->node[iz][iy][ix];
            g_global->afunc.F_AddFS(p_node, iray, ifreq);
        }
    }
}

/*******************************************************************************/
/* if dir==NULL then iray is used instead of a general ray direction */
static void FSNode(t_node *p_node, int iray, t_vec_3d *dir)
{
    t_vec_3d    ray_dw, ray_uw;
    int         cx, cy;
    t_intersect uw_intersect; /* there MUST be an upwind point */
    t_fsdata    uw_data;
#ifndef FS_LIN
    int last_plane = 0; /* in quadratic solvers, one needs to check arriving to
                           the real boundary */
#endif

#ifdef DEBUG
    if (!p_node)
        IERR;
#endif

    if (!dir) {
        PorGetIDir(iray, &ray_dw);
    } else {
        ray_dw.x = dir->x;
        ray_dw.y = dir->y;
        ray_dw.z = dir->z;
    }
    ray_uw.x = -ray_dw.x;
    ray_uw.y = -ray_dw.y;
    ray_uw.z = -ray_dw.z;
#ifndef FS_LIN
    if ((ray_dw.z > 0.0 && p_node->iz == p_node->p_grid->nz - 1) ||
        (ray_dw.z < 0.0 && p_node->iz == 0))
        last_plane = 1;
#endif
    grd_FindIntersection(p_node, &ray_uw, 0, &uw_intersect, &cx, &cy);
    InterpolateRadiation(p_node->p_grid, &uw_intersect, &uw_data, 1);
#ifndef FS_LIN
    if (!last_plane) {
        t_intersect dw_intersect;
        t_fsdata    dw_data;
        grd_FindIntersection(p_node, &ray_dw, 0, &dw_intersect, &cx, &cy);
        InterpolateRadiation(p_node->p_grid, &dw_intersect, &dw_data, 0);
#ifdef FS_BEZIER
        FSBezier(&uw_data, &p_node->fsdata, &dw_data, uw_intersect.distance,
                 dw_intersect.distance);
#elif defined(FS_DELOPAR)
        FSDELOPAR(&uw_data, &p_node->fsdata, &dw_data, uw_intersect.distance,
                  dw_intersect.distance);
#else
#error "Undefined formal solver!"
#endif
    } else
#endif
    {
        FSLin(&uw_data, &p_node->fsdata, uw_intersect.distance);
    }
}

/*****************************************************************************/
/** if dir==NULL then iray is used instead of general ray downwind direction
 *
 * WARNING ----------------
 * So far, FSNode_lc only meant for rays with z > 0
 * And only for FSBezier
 ******************************************************************************/
static void FSNode_lc(t_node *p_node, int iray, t_vec_3d *dir, double *buf_xy)
{
    t_vec_3d    ray_dw, ray_uw;
    int         iz_first, cx, cy, min_iz, max_iz;
    int         n_prolongated;
    t_intersect uw_intersect;
    t_fsdata    uw_data;
    int         idz;
#ifndef FS_LIN /* in quadratic solvers we need also the downwind data */
    t_intersect dw_intersect;
    t_fsdata    dw_data;
    int         last_plane = 0;

    t_grid *     g = NULL;
    double       uw_lc_x, uw_lc_y, dw_lc_x, dw_lc_y;
    t_intersect  inters_a, inters_b, inters_c;
    t_intersect *int_m, *int_o, *int_p, *swap_int;

    t_fsdata  fsdata_a, fsdata_b, fsdata_c;
    t_fsdata *fsd_m, *fsd_o, *fsd_p, *swap_fsd;

    double  dist_a, dist_b;
    double *dm, *dp, *swap_d;

    double dx, dy, dz;
#endif

#ifdef DEBUG
    if (!p_node)
        IERR;
#endif
    if (!dir) {
        PorGetIDir(iray, &ray_dw);
    } else { /* general ray direction */
        ray_dw.x = dir->x;
        ray_dw.y = dir->y;
        ray_dw.z = dir->z;
    }

    ray_uw.x = -ray_dw.x;
    ray_uw.y = -ray_dw.y;
    ray_uw.z = -ray_dw.z;
    pro_MinMax_IZ(G_MY_MPI_RANK, p_node->p_grid->nz, &min_iz, &max_iz);
    if (ray_dw.z > 0.0)
        iz_first = min_iz;
    else
        iz_first = max_iz;
    if (p_node->iz == iz_first)
        IERR; /* radiation field at min_iz is always known, no need for FS */

    g   = p_node->p_grid;
    idz = pro_Domain_Z_ID(G_MY_MPI_RANK);

    // WARNING ----------------
    // So far, FSNode_lc only meant for rays with z > 0
    // And only for FSBezier
    // -------------------------
    if (ray_dw.z < 0.0)
        IERR;

    if ((ray_dw.z > 0 && p_node->iz == p_node->p_grid->nz - 1) ||
        (ray_dw.z < 0 && p_node->iz == 0))
        last_plane = 1;

    // First we need to know where the intersection in the XY plane
    // will happen for the upwind, but not coming from this p_node, but from the
    // equivalent node in the top surface

    // uw_lc_x, uw_lc_y: x,y positions of upwind point (one z-layer away from
    // us) dw_lc_x, dw_lc_y: x,y position of downwind point (at our z-layer)
    //                   in the last z-layer there should be the same as the
    //                   node positions
    uw_lc_x = grd_cycle_x(g, NODE_X(p_node) - // current x position of this node
                                 (g->z[g->nz - 1] - g->z[p_node->iz - 1]) *
                                     ray_uw.x / ray_uw.z);
    dw_lc_x =
        grd_cycle_x(g, NODE_X(p_node) - (g->z[g->nz - 1] - g->z[p_node->iz]) *
                                            ray_uw.x / ray_uw.z);

    uw_lc_y = grd_cycle_y(g, NODE_Y(p_node) - // current Y position of this node
                                 (g->z[g->nz - 1] - g->z[p_node->iz - 1]) *
                                     ray_uw.y / ray_uw.z);
    dw_lc_y =
        grd_cycle_y(g, NODE_Y(p_node) - (g->z[g->nz - 1] - g->z[p_node->iz]) *
                                            ray_uw.y / ray_uw.z);

    // Now, starting at [uw_lc_x,uw_lc_y,z-1] we have to calculate the
    // intensity up to [dw_lc_x,dw_lc_y,z]

    // Let's first just see if we can follow the ray by calling NextIntersection
    // and get to [dw_lc_x,dw_lc_y,z]
    inters_a.inters.x = uw_lc_x;
    inters_a.ix1      = ref_point_x(g, uw_lc_x);

    inters_a.inters.y = uw_lc_y;
    inters_a.iy1      = ref_point_y(g, uw_lc_y);

    inters_a.inters.z = g->z[p_node->iz - 1];
    inters_a.iz1      = p_node->iz - 1;

    inters_a.orientation = ORI_XY;

    int st_add = (p_node->iy * g->nx + p_node->ix) * NSTOKES;

    int_m = &inters_a;
    fsd_m = &fsdata_a;
    // Let's get intensity at the z-1 layer upwind (M point)
    if (idz == 1 && p_node->iz == iz_first + 1) {
        // We only interpolate stokes intensities for the first Z-layer at the
        // bottom of the domain.
        InterpolateRadiation(p_node->p_grid, int_m, fsd_m, 1);
    } else {
        // For other vertical layers we get the values from the intensity
        // calculated directly in fsd_o and saved to buf_xy (we just keep
        // overwriting it as we go up)
        InterpolateRadiation(p_node->p_grid, int_m, fsd_m, 0);
        memcpy(fsd_m->i, &(buf_xy[st_add]), DBLSIZE * NSTOKES);
    }

    // Let's get O-point intersection and fsdata (no Stokes)
    int_o = &inters_b;
    fsd_o = &fsdata_b;
    NextIntersection(g, int_m, &ray_dw, int_o);
    InterpolateRadiation(p_node->p_grid, int_o, fsd_o, 0);

    dm  = &dist_a;
    dx  = int_m->inters.x - int_o->inters.x;
    dy  = int_m->inters.y - int_o->inters.y;
    dz  = int_m->inters.z - int_o->inters.z;
    *dm = sqrt(dx * dx + dy * dy + dz * dz);

    // Initialize pointers for P point
    int_p = &inters_c;
    fsd_p = &fsdata_c;
    dp    = &dist_b;

    while (int_o->iz1 != p_node->iz) {
        // If we are here, O-point is still crossing vertical planes, and so we
        // can always apply Bezier

        // Let's find P-point and interpolate radiation (no Stokes)
        NextIntersection(g, int_o, &ray_dw, int_p);
        InterpolateRadiation(p_node->p_grid, int_p, fsd_p, 0);

        // Distance O-P
        dx  = int_o->inters.x - int_p->inters.x;
        dy  = int_o->inters.y - int_p->inters.y;
        dz  = int_o->inters.z - int_p->inters.z;
        *dp = sqrt(dx * dx + dy * dy + dz * dz);

        // Bezier, fsd_o updated
        FSBezier(fsd_m, fsd_o, fsd_p, *dm, *dp);

        // ---- ROTATE POINTERS
        // rotate the pointers to the intersections
        swap_int = int_m;
        int_m    = int_o;
        int_o    = int_p;
        int_p    = swap_int;
        // rotate the pointers to the fsdata
        swap_fsd = fsd_m;
        fsd_m    = fsd_o;
        fsd_o    = fsd_p;
        fsd_p    = swap_fsd;
        // rotate the pointers to the distances
        swap_d = dm;
        dm     = dp;
        dp     = swap_d;
    }

    /* Here int_o is the crossing point at the z-layer. If not the last
       plane we need to calculate one more intersection in order to use FSBezier
       to calculate fsdata at Z-layer. If this is the last plane, we just use
       Linear Interpolation and no need to calculate NextIntersection */

    if (!last_plane) {
        // We just apply the loop body one more time
        NextIntersection(g, int_o, &ray_dw, int_p);
        InterpolateRadiation(p_node->p_grid, int_p, fsd_p, 0);

        dx  = int_o->inters.x - int_p->inters.x;
        dy  = int_o->inters.y - int_p->inters.y;
        dz  = int_o->inters.z - int_p->inters.z;
        *dp = sqrt(dx * dx + dy * dy + dz * dz);

        FSBezier(fsd_m, fsd_o, fsd_p, *dm, *dp);

    } else {
        // we cannot use P point, we just use FSLin with points M-O

        FSLin(fsd_m, fsd_o, *dm);
    }

    // Store the data to the buffer XxY
    memcpy(&(buf_xy[st_add]), fsd_o->i, DBLSIZE * NSTOKES);
}

/*******************************************************************************/
/* if dir==NULL then iray is used instead of general ray downwind direction */
static void FSNodeToAxis(t_node *p_node, int iray, t_vec_3d *dir)
{
    t_vec_3d    ray_dw, ray_uw;
    int         iz_first, cx, cy, min_iz, max_iz;
    int         n_prolongated;
    t_intersect uw_intersect;
    t_fsdata    uw_data;
#ifndef FS_LIN /* in quadratic solvers we need also the downwind data */
    t_intersect dw_intersect;
    t_fsdata    dw_data;
    int         last_plane = 0;
#endif

#ifdef DEBUG
    if (!p_node)
        IERR;
#endif
    if (!dir) {
        PorGetIDir(iray, &ray_dw);
    } else /* general ray direction */
    {
        ray_dw.x = dir->x;
        ray_dw.y = dir->y;
        ray_dw.z = dir->z;
    }
    ray_uw.x = -ray_dw.x;
    ray_uw.y = -ray_dw.y;
    ray_uw.z = -ray_dw.z;
    pro_MinMax_IZ(G_MY_MPI_RANK, p_node->p_grid->nz, &min_iz, &max_iz);
    if (ray_dw.z > 0.0)
        iz_first = min_iz;
    else
        iz_first = max_iz;
    if (p_node->iz == iz_first)
        IERR; /* radiation field at min_iz is always known, no need for FS */
    grd_FindIntersection(p_node, &ray_uw, 1, &uw_intersect, &cx, &cy);
    if (cx ||
        cy) /* added on Oct 4, 2012: we have to use XZ or YZ intersections
               depending on the first intersection of the vertical plane */
    {
        t_intersect v_inters;
        int         ccx, ccy;
        grd_FindIntersection(p_node, &ray_uw, 0, &v_inters, &ccx, &ccy);
        if (v_inters.orientation == ORI_YZ)
            cy = 0;
        else
            cx = 0;
    }
    n_prolongated = MAX2(cx, cy);
    InterpolateRadiation(p_node->p_grid, &uw_intersect, &uw_data, 1);
#ifndef FS_LIN
    if ((ray_dw.z > 0 && p_node->iz == p_node->p_grid->nz - 1) ||
        (ray_dw.z < 0 && p_node->iz == 0))
        last_plane = 1;
    if (!last_plane) {
        int tmp1, tmp2;
        grd_FindIntersection(p_node, &ray_dw, 0, &dw_intersect, &tmp1, &tmp2);
        InterpolateRadiation(p_node->p_grid, &dw_intersect, &dw_data, 0);
    }
#endif
    if (n_prolongated) {
        int n_tot = n_prolongated + 2, ori = (cx >= cy ? ORI_YZ : ORI_XZ);
        t_intersect *ist = (t_intersect *)Malloc(n_tot * sizeof(t_intersect));
        t_fsdata *   rad = (t_fsdata *)Malloc(
            n_tot * sizeof(t_fsdata)); /* points of intersection: */
        t_grid *g = p_node->p_grid;
        int     i, dix = (ray_dw.x > 0 ? 1 : -1), diy = (ray_dw.y > 0 ? 1 : -1),
               diz = (ray_dw.z > 0 ? 1 : -1), pos;
        double dx, dy, dz;
        if (ori == ORI_YZ) {
            int iy1;
            if (diy > 0)
                iy1 = p_node->iy - diy;
            else
                iy1 = p_node->iy;
            for (i = p_node->ix - dix, pos = n_tot - 2; pos > 0; i -= dix,
                pos--) /* p_node is not set here neither the uw point */
            {
                ist[pos].orientation = ori;
                ist[pos].inters.x    = grd_XCycl(g, i);
                ist[pos].inters.y =
                    NODE_Y(p_node) +
                    ray_uw.y * (ist[pos].inters.x - NODE_X(p_node)) / ray_uw.x;
                ist[pos].inters.z =
                    NODE_Z(p_node) +
                    ray_uw.z * (ist[pos].inters.x - NODE_X(p_node)) / ray_uw.x;
                ist[pos].ix1 = i;
                if (diy == 1) {
                    while (grd_YCycl(g, iy1) >= ist[pos].inters.y)
                        iy1 -= diy;
                } else {
                    while (grd_YCycl(g, iy1 + 1) <= ist[pos].inters.y)
                        iy1 -= diy;
                }
                /*while ((grd_YCycl(g,iy1)-ist[pos].inters.y) * diy > 0.0) iy1
                 * -= diy;*/
                ist[pos].iy1 = iy1;
                ist[pos].iz1 = p_node->iz - (diz + 1) / 2;
#ifdef DEBUG
                if (ist[pos].iz1 < min_iz)
                    IERR;
                if (ist[pos].iz1 >= max_iz)
                    IERR;
#endif
            }
        } else if (ori == ORI_XZ) {
            int ix1;
            if (dix > 0)
                ix1 = p_node->ix - dix;
            else
                ix1 = p_node->ix;
            for (i = p_node->iy - diy, pos = n_tot - 2; pos > 0; i -= diy,
                pos--) /* p_node is not set here neither the uw point */
            {
                ist[pos].orientation = ori;
                ist[pos].inters.y    = grd_YCycl(g, i);
                ist[pos].inters.x =
                    NODE_X(p_node) +
                    ray_uw.x * (ist[pos].inters.y - NODE_Y(p_node)) / ray_uw.y;
                ist[pos].inters.z =
                    NODE_Z(p_node) +
                    ray_uw.z * (ist[pos].inters.y - NODE_Y(p_node)) / ray_uw.y;
                ist[pos].iy1 = i;
                if (dix == 1) {
                    while (grd_XCycl(g, ix1) >= ist[pos].inters.x)
                        ix1 -= dix;
                } else {
                    while (grd_XCycl(g, ix1 + 1) <= ist[pos].inters.x)
                        ix1 -= dix;
                }
                /*while ((grd_XCycl(g,ix1)-ist[pos].inters.x) * dix > 0.0) ix1
                 * -= dix;*/
                ist[pos].ix1 = ix1;
                ist[pos].iz1 = p_node->iz - (diz + 1) / 2;
#ifdef DEBUG
                if (ist[pos].iz1 < min_iz)
                    IERR;
                if (ist[pos].iz1 >= max_iz)
                    IERR;
#endif
            }
        } else {
            IERR;
        }
        /* position of the p_node for calculation of its distance: */
        ist[n_tot - 1].inters.x = NODE_X(p_node);
        ist[n_tot - 1].inters.y = NODE_Y(p_node);
        ist[n_tot - 1].inters.z = NODE_Z(p_node);
        CopyIntersection(&ist[0],
                         &uw_intersect); /* the same for the uw point */
        /* distances between the intersections: */
        ist[n_tot - 1].distance =
            -1.0e100; /* not used: already corresponds to the downwind step */
        for (i = n_tot - 2; i >= 0; i--) {
            dx              = ist[i].inters.x - ist[i + 1].inters.x;
            dy              = ist[i].inters.y - ist[i + 1].inters.y;
            dz              = ist[i].inters.z - ist[i + 1].inters.z;
            ist[i].distance = sqrt(dx * dx + dy * dy + dz * dz);
        }
        /* radiation data of the endpoints: */
        CopyFSData(&rad[0], &uw_data);
        CopyFSData(&rad[n_tot - 1], &p_node->fsdata);
        /* interpolate transfer coefficients in the intermediate points */
        for (i = 1; i <= n_tot - 2; i++) {
            InterpolateRadiation(p_node->p_grid, &ist[i], &rad[i], 0);
#ifdef DEBUG
            if (rad[i].k[ETA_I] < 0) {
                int ii;
                PrintfErr("etaI < 0 at intersection %d\n", i);
                PrintfErr("%d:(%d,%d,%d), ori: %d\n", ist[i].orientation,
                          ist[i].ix1, ist[i].iy1, ist[i].iz1, ori);
                PrintfErr("node ixyz: %d %d %d\n", p_node->ix, p_node->iy,
                          p_node->iz);
                PrintfErr("dix:%d  diy:%d diz:%d\n", dix, diy, diz);
                PrintfErr("dir: %e %e\n", ray_uw.x, ray_uw.y);
                PrintfErr("node xyz: %e %e %e\n", NODE_X(p_node),
                          NODE_Y(p_node), NODE_Z(p_node));
                PrintfErr("inters xyz: %e %e %e\n", ist[i].inters.x,
                          ist[i].inters.y, ist[i].inters.z);
                PrintfErr("cx: %d  cy: %d\n", cx, cy);
                PrintfErr("n_tot=%d\n", n_tot);
                for (ii = 0; ii < n_tot; ii++) {
                    PrintfErr("(%e,%e,%e): %d; ix1:%d iy1:%d iz1: %d\n",
                              ist[ii].inters.x, ist[ii].inters.y,
                              ist[ii].inters.z, ist[ii].orientation,
                              ist[ii].ix1, ist[ii].iy1, ist[ii].iz1);
                }
                IERR;
            }
#endif
        }
        /* solve the RTE down to p_node: */
#ifdef FS_LIN
        for (i = 1; i <= n_tot - 1; i++) {
            FSLin(&rad[i - 1], &rad[i], ist[i - 1].distance);
        }
        CopyFSData(&p_node->fsdata, &rad[n_tot - 1]);
#else
        for (i = 1; i <= n_tot - 2; i++) {
#if defined(FS_BEZIER)
            FSBezier(&rad[i - 1], &rad[i], &rad[i + 1], ist[i - 1].distance,
                     ist[i].distance);
#elif defined(FS_DELOPAR)
            FSDELOPAR(&rad[i - 1], &rad[i], &rad[i + 1], ist[i - 1].distance,
                      ist[i].distance);
#else
#error "N/I"
#endif
        }
        if (!last_plane) {
            double dx = dw_intersect.inters.x - NODE_X(p_node);
            double dy = dw_intersect.inters.y - NODE_Y(p_node);
            double dz = dw_intersect.inters.z - NODE_Z(p_node);
            double dist = sqrt(dx * dx + dy * dy + dz * dz);
#if defined(FS_BEZIER)
            FSBezier(&rad[n_tot - 2], &p_node->fsdata, &dw_data,
                     ist[n_tot - 2].distance, dist);
#elif defined(FS_DELOPAR)
            FSDELOPAR(&rad[n_tot - 2], &p_node->fsdata, &dw_data,
                      ist[n_tot - 2].distance, dist);
#else
#error "N/I"
#endif
        } else {
            FSLin(&rad[n_tot - 2], &rad[n_tot - 1], ist[n_tot - 2].distance);
            CopyFSData(&p_node->fsdata, &rad[n_tot - 1]);
        }
#endif
        free(ist);
        free(rad);
    } else {
#ifdef FS_LIN
        FSLin(&uw_data, &p_node->fsdata, uw_intersect.distance);
#else
        if (!last_plane) {
#if defined(FS_BEZIER)
            FSBezier(&uw_data, &p_node->fsdata, &dw_data, uw_intersect.distance,
                     dw_intersect.distance);
#elif defined(FS_DELOPAR)
            FSDELOPAR(&uw_data, &p_node->fsdata, &dw_data,
                      uw_intersect.distance, dw_intersect.distance);
#endif
        } else {
            FSLin(&uw_data, &p_node->fsdata, uw_intersect.distance);
        }
#endif
    }
}

/*******************************************************************************/
/* For a given grid node sets the Stokes parameters
 * and the Lambda diagonal for a given direction iray and frequency ifreq.
 * NOTE: Only needed for non-periodic boundaries.
 */
static void SetBoundaryNodeIllumination(t_node *p_node, t_vec_3d *dir, int iray,
                                        int ifreq)
{
    t_vec_3d ray;
    t_vec_3d r;

    if (!dir) {
        ray.x = g_global->dirs.dir[iray].x;
        ray.y = g_global->dirs.dir[iray].y;
        ray.z = g_global->dirs.dir[iray].z;
    } else {
        ray.x = dir->x;
        ray.y = dir->y;
        ray.z = dir->z;
    }
    r.x = NODE_X(p_node);
    r.y = NODE_Y(p_node);
    r.z = NODE_Z(p_node);
    g_global->afunc.F_ExternalIllum(r, ray, ifreq, p_node->fsdata.i);
    p_node->fsdata.psi_star = 0.0;
}

/*******************************************************************************/
/* there MUST be an upwind plane at iz-diz
 * if dir==NULL then iray is used instead of a general ray direction
 */
static void FSPlane(t_grid *g, int iray, t_vec_3d *dir, int iz, int dix,
                    int ix_first, int diy, int iy_first, int diz, int ifreq)
{
    int ix, ix_last = ix_first + dix * (g->nx - 1), iy,
            iy_last = iy_first + diy * (g->ny - 1);
    int period      = g_global->period[0] && g_global->period[1];

#ifdef DEBUG
    int min_iz, max_iz;
    pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz);
    if (diz > 0 && iz <= min_iz)
        IERR;
    if (diz < 0 && iz >= max_iz)
        IERR;
#endif

#ifdef ALWAYS_TO_AXIS
    if (!period) {
        Error(E_ERROR, POR_AT,
              "ALWAYS_TO_AXIS cannot be used with non-periodic grids.");
    }
#endif

    if ((g_global->period[0] && !g_global->period[1]) ||
        (!g_global->period[0] && g_global->period[1]))
        IERR;
    for (ix = ix_first; ix != ix_last + dix; ix += dix) {
        if (period) {
            FSNodeToAxis(
                &g->node[iz][grd_YIndex(g, iy_first)][grd_XIndex(g, ix)], iray,
                dir);
        } else {
            SetBoundaryNodeIllumination(
                &g->node[iz][grd_YIndex(g, iy_first)][grd_XIndex(g, ix)], dir,
                iray, ifreq);
        }
    }
    for (iy = iy_first + diy; iy != iy_last + diy; iy += diy) {
        if (period) {
            FSNodeToAxis(
                &g->node[iz][grd_YIndex(g, iy)][grd_XIndex(g, ix_first)], iray,
                dir);
        } else {
            SetBoundaryNodeIllumination(
                &g->node[iz][grd_YIndex(g, iy)][grd_XIndex(g, ix_first)], dir,
                iray, ifreq);
        }
    }
    for (iy = iy_first + diy; iy != iy_last + diy; iy += diy) {
        for (ix = ix_first + dix; ix != ix_last + dix; ix += dix) {
#ifndef ALWAYS_TO_AXIS
            FSNode(&g->node[iz][grd_YIndex(g, iy)][grd_XIndex(g, ix)], iray,
                   dir);
#else
            FSNodeToAxis(&g->node[iz][grd_YIndex(g, iy)][grd_XIndex(g, ix)],
                         iray, dir);
#endif
        }
    }
}

/*******************************************************************************/
/* there MUST be an upwind plane at iz-diz
 * if dir==NULL then iray is used instead of a general ray direction
 */
static void FSPlane_lc(t_grid *g, int iray, t_vec_3d *dir, int iz, int dix,
                       int ix_first, int diy, int iy_first, int diz, int ifreq,
                       double *buf_xy)
{
    int ix, ix_last = ix_first + dix * (g->nx - 1), iy,
            iy_last = iy_first + diy * (g->ny - 1);
    int period      = g_global->period[0] && g_global->period[1];

#ifdef DEBUG
    int min_iz, max_iz;
    pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz);
    if (diz > 0 && iz <= min_iz)
        IERR;
    if (diz < 0 && iz >= max_iz)
        IERR;
#endif

#ifdef ALWAYS_TO_AXIS
    if (!period) {
        Error(E_ERROR, POR_AT,
              "ALWAYS_TO_AXIS cannot be used with non-periodic grids.");
    }
#endif

    if ((g_global->period[0] && !g_global->period[1]) ||
        (!g_global->period[0] && g_global->period[1]))
        IERR;
    for (ix = ix_first; ix != ix_last + dix; ix += dix) {
        if (period) {
            FSNode_lc(&g->node[iz][grd_YIndex(g, iy_first)][grd_XIndex(g, ix)],
                      iray, dir, buf_xy);
        } else {
            SetBoundaryNodeIllumination(
                &g->node[iz][grd_YIndex(g, iy_first)][grd_XIndex(g, ix)], dir,
                iray, ifreq);
        }
    }

    for (iy = iy_first + diy; iy != iy_last + diy; iy += diy) {
        if (period) {
            FSNode_lc(&g->node[iz][grd_YIndex(g, iy)][grd_XIndex(g, ix_first)],
                      iray, dir, buf_xy);
        } else {
            SetBoundaryNodeIllumination(
                &g->node[iz][grd_YIndex(g, iy)][grd_XIndex(g, ix_first)], dir,
                iray, ifreq);
        }
    }

    for (iy = iy_first + diy; iy != iy_last + diy; iy += diy) {
        for (ix = ix_first + dix; ix != ix_last + dix; ix += dix) {
            FSNode_lc(&g->node[iz][grd_YIndex(g, iy)][grd_XIndex(g, ix)], iray,
                      dir, buf_xy);
        }
    }
}

/*******************************************************************************/
/* formal solution for a given ray direction and frequency
 * buf_xy_in: linear buffer containing the boundary radiation at the incoming XY
 * plane (if NULL then module boundary illumination is used) buf_xy_out: linear
 * buffer into which the outgoing radiation at the z-plane is stored (if NULL
 * then nothing happens)
 */
static void fs_FSRay(t_grid *g, int iray, int ifreq, double *buf_xy_in,
                     double *buf_xy_out)
{
    int ix_first, iy_first, iz_first, ix_last, iy_last, iz_last, dix, diy, diz,
        iz;
    t_vec_3d ray;

    if (!g)
        IERR;
    if (iray < 0 || iray >= g_global->dirs.n_rays || ifreq < 0 ||
        ifreq >= g_global->n_freq)
        IERR;

    PorGetIDir(iray, &ray);
    GetSweepIndices(g, &ix_first, &iy_first, &iz_first, &ix_last, &iy_last,
                    &iz_last, &dix, &diy, &diz, &ray);

#ifdef FS_LIN
    for (iz = iz_first; iz != iz_last + diz; iz += diz) {
        CalculateZLayerSFKmtx(g, iz, iray, ifreq);
    }
#else
    for (iz = iz_first; iz != iz_last + 2 * diz && iz >= 0 && iz <= g->nz - 1;
         iz += diz) {
        CalculateZLayerSFKmtx(g, iz, iray, ifreq);
    }
#endif

    if (buf_xy_in) {
        BufferToFS(g, iz_first, buf_xy_in);
    } else {
        SetBoundaryIllumination(g, iz_first, iray, NULL, ifreq);
    }
    AddZLayerContributions(g, iz_first, iray, ifreq);

    for (iz = iz_first + diz; iz != iz_last + diz; iz += diz) {
        FSPlane(g, iray, NULL, iz, dix, ix_first, diy, iy_first, diz, ifreq);
        AddZLayerContributions(g, iz, iray, ifreq);
    }
    if (buf_xy_out) {
        FSToBuffer(g, iz_last, buf_xy_out);
    }
}

/*******************************************************************************/
void fs_FormalSolution(t_grid *g)
{
    int         idir, rfreq, ifreq;
    int         min_ifreq, max_ifreq, idz, buflen;
    double *    buf_in, *buf_out, *buf_out_tmp;
    MPI_Status  status;
    MPI_Request req;
    double      t, t_comm = 0, t_fs = 0, t_tot = 0;
    int         wait, dir;

    if (!g)
        IERR;
    t_tot = LibRunningTime();
    idz   = pro_Domain_Z_ID(G_MY_MPI_RANK);
    pro_MinMaxIFreq(&min_ifreq, &max_ifreq);
    buflen = g->nx * g->ny * (NSTOKES + 1); /* apart from Stokes parameters, the
                                               lambda diagonal is also sent */
    buf_in      = ArrayNew(buflen);
    buf_out     = ArrayNew(buflen);
    buf_out_tmp = ArrayNew(buflen);

    wait = 0;
    dir  = g_global->dirs.dir[0].z > 0.0 ? 1 : -1;

    stk_Add(0, POR_AT, "---------- DEBUG --------- rays %d freqs %d %d",
            g_global->dirs.n_rays, min_ifreq, max_ifreq);

    for (idir = 0; idir < g_global->dirs.n_rays; idir++) {
        int prev_rank, next_rank;
        int dirup = (g_global->dirs.dir[idir].z > 0.0 ? 1 : 0);

        prev_rank = (dirup ? G_MY_MPI_RANK - 1 : G_MY_MPI_RANK + 1);
        if ((idz == 1 && dirup) || (idz == G_MPI_L && !dirup))
            prev_rank = 0;

        next_rank = (dirup ? G_MY_MPI_RANK + 1 : G_MY_MPI_RANK - 1);
        if ((idz == 1 && !dirup) || (idz == G_MPI_L && dirup))
            next_rank = 0;

        if (dir != (g_global->dirs.dir[idir].z > 0.0 ? 1 : -1)) {
            if (wait)
                MPI_Wait(&req, MPI_STATUS_IGNORE);
            wait = 0;
            dir  = -dir;
        }

        for (rfreq = min_ifreq; rfreq <= max_ifreq; rfreq++) {
            /* Since not all frequencies take the same time, in g_global->rfreq
               we have a shuffled list of frequency indices. So, we use that
               list to have a random group of frequencies in each frequency
               band. If we want to go back to the non-random frequency band,
               just replace the following line with ifreq = rfreq */
#ifdef NO_SHUFFLE_FREQ
            ifreq = rfreq;
#else
            ifreq = g_global->rfreq[rfreq];
#endif
            if (prev_rank) {
                t = LibRunningTime();
                if (MPI_SUCCESS != MPI_Recv(buf_in, buflen, MPI_DOUBLE,
                                            prev_rank, 0, MPI_COMM_WORLD,
                                            &status))
                    CERR;
                t_comm += LibRunningTime() - t;
            }
            t = LibRunningTime();
            fs_FSRay(g, idir, ifreq, prev_rank ? buf_in : NULL,
                     next_rank ? buf_out : NULL);
            t_fs += LibRunningTime() - t;
            if (next_rank) {
                t = LibRunningTime();
                if (wait)
                    MPI_Wait(&req, MPI_STATUS_IGNORE);
                Memcpy(buf_out_tmp, buf_out,
                       DBLSIZE *
                           buflen); /* buf_out may change before actual delivery
                                       -> need to make a copy of it */
                if (MPI_SUCCESS != MPI_Isend(buf_out_tmp, buflen, MPI_DOUBLE,
                                             next_rank, 0, MPI_COMM_WORLD,
                                             &req))
                    CERR;
                wait = 1;
                t_comm += LibRunningTime() - t;
            }
        }
    }
    if (wait)
        MPI_Wait(&req, MPI_STATUS_IGNORE);

    free(buf_in);
    free(buf_out);
    free(buf_out_tmp);
    t_tot = LibRunningTime() - t_tot;
    /*PrintfErr("Process ID %d (iz=%d): t(comm) = %e, t(fs) = %e, t(tot) =
     * %e\n", G_MY_MPI_RANK, pro_Domain_Z_ID(G_MY_MPI_RANK), t_comm, t_fs,
     * t_tot);*/
}

/* Helper function for NextIntersection below. Given a coordinate point, this
   will give the reference grid point (always either the intersection point, if
   virtually idential to the grid point, or the grid poing smaller

   x: coordinate in x. It MUST be within the limits of the domain */
static int ref_point_x(t_grid *g, double x)
{
    int ix;

    ix = g->nx - 1;
    while (1) {
        if (g->x[ix] <= x)
            break;
        ix--;
    }
    return ix;
}

static int ref_point_y(t_grid *g, double y)
{
    int iy;

    iy = g->ny - 1;
    while (1) {
        if (g->y[iy] <= y)
            break;
        iy--;
    }
    return iy;
}

static int ref_point_z(t_grid *g, double z)
{
    int iz;

    iz = g->nz - 1;
    while (1) {
        if (g->z[iz] <= z)
            break;
        iz--;
    }
    return iz;
}

/*
 * Find the next intersection with the mesh starting in curr_inters.
 *
 * Return 1 if a next intersection is found, 0 elsewhere.
 *
 * It works with the following asumptions: reference points ix1 iy1 iz1 are
 * always either the intersection point (if grid node and intersection point are
 * identical), or the node smaller.
 *
 * NOTE: this function does not use as input the ORIENTATION of the
 * intersection, but the ORIENTATION is set up according to the first
 * intersected plane.
 *
 * In case the intersection falls in a vertical edge, the orientation is set as
 * XZ.
 *
 * In case the intersection falls in a horizontal edge or a corner, then
 * orientation is set to XY.
 */
static int NextIntersection(t_grid *g, t_intersect *curr_inters, t_vec_3d *dir,
                            t_intersect *next_inters)
{
    int    px = curr_inters->ix1, py = curr_inters->iy1, pz = curr_inters->iz1;
    double t, tx, tz, ty; /* distances from the potential intersections */
    int    dix, diy, diz;

    /* It is not necessary to check the orientation. If we are in the last one,
       and we try to continue in that direction, then we should bail out */
    if ((curr_inters->iz1 == 0 && dir->z <= 0.0 &&
         fabs(curr_inters->inters.z - g->z[pz]) < 1e-30) ||
        (curr_inters->iz1 == g->nz - 1 && dir->z >= 0.0)) {
        return 0;
    }

    dix = (dir->x >= 0 ? 1 : -1);
    diy = (dir->y >= 0 ? 1 : -1);
    diz = (dir->z >= 0 ? 1 : -1);

    // Potentially we could end up getting to these planes.
    if (dix == 1) {
        px++;
    } else if (dix == -1) {
        // if we are right in the node, the potential plane would decrease this
        // is not likely but it can happen, for example, if we start at a grid
        // point and the angles are perpendicular to the grid axes
        if (curr_inters->inters.x == grd_XCycl(g, px)) {
            px--;
        } else {
            // if we are between nodes, and given that the reference plane is
            // below us, the potential intersection plane is the same as the
            // current one: no need to change px
        }
    }

    if (diy == 1) {
        py++;
    } else if (diy == -1) {
        if (curr_inters->inters.y == grd_YCycl(g, py)) {
            py--;
        } else {
            // no need to change py
        }
    }

    if (diz == 1) {
        pz++;
    } else if (diz == -1) {
        if (curr_inters->inters.z == g->z[pz]) {
            pz--;
        } else {
            // no need to change pz
        }
    }

    // steps to be taken in each coordinate to reach the next plane in that
    // coordinate
    tx = (fabs(dir->x) > DBL_EPSILON
              ? (grd_XCycl(g, px) - curr_inters->inters.x) / dir->x
              : DBL_MAX);
    ty = (fabs(dir->y) > DBL_EPSILON
              ? (grd_YCycl(g, py) - curr_inters->inters.y) / dir->y
              : DBL_MAX);
    tz = (fabs(dir->z) > DBL_EPSILON
              ? (g->z[pz] - curr_inters->inters.z) / dir->z
              : DBL_MAX);

    t                     = MIN3(tx, ty,
             tz); // with this number of steps we are hitting and intersection
    next_inters->distance = t;

    // We will intersect at this point, with the following reference point and
    // orientation. X. Same idea below for Y and Z
    next_inters->ix1 = px;
    if (t == tx) {
        /* if the smallest distance was in X, then we set the intersection
           exactly to the plane coordinate; the reference node x coordinate to
           it, and the orientation to the YZ plane. */
        next_inters->inters.x    = grd_XCycl(g, px);
        next_inters->orientation = ORI_YZ;
    } else {
        /* for most cases, if we are going in the positive direction, being here
           would mean that we have not reached the next plane, so we should get
           the reference node to px - 1.

           But in extreme cases where mathematically we would end up right in
           the node, numerically we can actually be below or above the node. If
           we are going in the positive direction and end up beyond the grid
           point we should fix it (tx was not the smallest of the t's, so we
           shouldn't be hitting the X plane). If we are going in the negative
           direction and end
           up below the grid point we also have to fix it for the same reason.
         */
        next_inters->inters.x = curr_inters->inters.x + t * dir->x;
        if (dix == 1) {
            if (grd_XCycl(g, px) > next_inters->inters.x) {
                (next_inters->ix1)--;
            } else {
                // We are acutally a little bit beyond the x grid coordinate,
                // though we shouldn't be, so this is really as if we just hit
                // the x plane
                next_inters->inters.x    = grd_XCycl(g, px);
                next_inters->orientation = ORI_YZ;
            }
        } else {
            if (grd_XCycl(g, px) > next_inters->inters.x) {
                // We are acutally a little bit below the x grid coordinate,
                // though we shouldn't be, so this is really as if we just hit
                // the x plane
                (next_inters->ix1)--;
                next_inters->inters.x    = grd_XCycl(g, px);
                next_inters->orientation = ORI_YZ;
            }
        }
    }

    next_inters->iy1 = py;
    if (t == ty) {
        next_inters->inters.y    = grd_YCycl(g, py);
        next_inters->orientation = ORI_XZ;
    } else {
        next_inters->inters.y = curr_inters->inters.y + t * dir->y;
        if (diy == 1) {
            if (grd_YCycl(g, py) > next_inters->inters.y) {
                (next_inters->iy1)--;
            } else {
                next_inters->inters.y    = grd_YCycl(g, py);
                next_inters->orientation = ORI_XZ;
            }
        } else {
            if (grd_YCycl(g, py) > next_inters->inters.y) {
                (next_inters->iy1)--;
                next_inters->inters.y    = grd_YCycl(g, py);
                next_inters->orientation = ORI_XZ;
            }
        }
    }

    // Assuming periodic boundaries, then it is always better to leave this one
    // for the end.  In case I end up hitting a corner, I will always consider
    // the plane XY for interpolation.

    // For non-periodic domains, we will have to be careful here to make sure
    // that the orientation plane is one where we have points to actually
    // interpoolate, in case of hitting and edge or a corner.
    next_inters->iz1 = pz;
    if (t == tz) {
        next_inters->inters.z    = g->z[pz];
        next_inters->orientation = ORI_XY;
    } else {
        next_inters->inters.z = curr_inters->inters.z + t * dir->z;
        if (diz == 1) {
            if (g->z[pz] > next_inters->inters.z) {
                (next_inters->iz1)--;
            } else {
                next_inters->inters.z    = g->z[pz];
                next_inters->orientation = ORI_XY;
            }
        } else {
            if (g->z[pz] > next_inters->inters.z) {
                (next_inters->iz1)--;
                next_inters->inters.z    = g->z[pz];
                next_inters->orientation = ORI_XY;
            }
        }
    }

    return 1;
}

/*******************************************************************************/
/* retrieve the nodes radiation data needed for array of intersections along the
 * long characteristic BILINEAR interpolation
 */
static void SetLongCharacteristic(t_grid *g, int n, t_intersect *inter,
                                  t_fsdata *fs, t_vec_3d dir, int ifreq)
{
    int          i;
    t_node *     pns[2][2] = {{NULL, NULL}, {NULL, NULL}};
    t_intersect *b         = &inter[0];

    for (i = 0; i < n; i++) {
        int p, q, ix1, iy1;
        ix1 = grd_XIndex(g, b->ix1);
        iy1 = grd_YIndex(g, b->iy1);
        switch (inter[i].orientation) {
        case ORI_XY:
            pns[0][0] = &g->node[b->iz1][iy1][ix1];
            pns[0][1] = &g->node[b->iz1][iy1][grd_XIndex(g, ix1 + 1)];
            pns[1][0] =
                &g->node[b->iz1][grd_YIndex(g, iy1 + 1)][grd_XIndex(g, ix1)];
            pns[1][1] = &g->node[b->iz1][grd_YIndex(g, iy1 + 1)]
                                [grd_XIndex(g, ix1 + 1)];
            break;
        case ORI_XZ:
            pns[0][0] = &g->node[b->iz1][iy1][ix1];
            pns[0][1] = &g->node[b->iz1][iy1][grd_XIndex(g, ix1 + 1)];
            pns[1][0] = &g->node[b->iz1 + 1][iy1][ix1];
            pns[1][1] = &g->node[b->iz1 + 1][iy1][grd_XIndex(g, ix1 + 1)];
            break;
        case ORI_YZ:
            pns[0][0] = &g->node[b->iz1][iy1][ix1];
            pns[0][1] = &g->node[b->iz1][grd_YIndex(g, iy1 + 1)][ix1];
            pns[1][0] = &g->node[b->iz1 + 1][iy1][ix1];
            pns[1][1] = &g->node[b->iz1 + 1][grd_YIndex(g, iy1 + 1)][ix1];
            break;
        default:
            IERR;
            break;
        }
        for (p = 0; p < 2; p++) {
            for (q = 0; q < 2; q++) {
                /* TODO: in some nodes the sf & kmtx may be calculated multiple
                 * times: inefficient! */
                g_global->afunc.F_SKGeneralDirection(
                    pns[p][q], pns[p][q]->fsdata.s, pns[p][q]->fsdata.k, dir,
                    ifreq, 0);
            }
        }
        b++;
    }
    /* index 0 corresponds to the last point (point of emerging radiation) =>
     * just copy sf and kmtx from the last node */
    CopyFSData(&fs[0], &g->node[inter[0].iz1][grd_YIndex(g, inter[0].iy1)]
                               [grd_XIndex(g, inter[0].ix1)]
                                   .fsdata);
    for (i = 1; i < n;
         i++) /* interpolate source functions along the long characteristic */
    {
        InterpolateRadiation(g, &inter[i], &fs[i], 0);
    }
}

/*******************************************************************************/
/* find point where tau=1 at frequency ifreq along the long characteristic
 * (radiation data in intersections must be set) returns the last intersection
 * point if the optical depth is < 1
 */
static void FindTau1Point(t_grid *g, int n, t_intersect *inter, t_fsdata *fs,
                          int ifreq_tau1, t_vec_3d *tau1point)
{
    int    i;
    double tau = 0.0, dtau = 0.0, ds = 0.0;
    for (i = 0; i < n - 1; i++) {
        ds   = sqrt((inter[i].inters.x - inter[i + 1].inters.x) *
                      (inter[i].inters.x - inter[i + 1].inters.x) +
                  (inter[i].inters.y - inter[i + 1].inters.y) *
                      (inter[i].inters.y - inter[i + 1].inters.y) +
                  (inter[i].inters.z - inter[i + 1].inters.z) *
                      (inter[i].inters.z - inter[i + 1].inters.z));
        dtau = 0.5 * (fs[i].k[ETA_I] + fs[i + 1].k[ETA_I]) * ds;
        if (tau + dtau >= 1.0) {
            break;
        } else {
            tau += dtau;
        }
    }
    /*PrintfErr(" tau = %e, tau+dtau = %e, ds = %e ifreq = %d\n", tau, tau+dtau,
     * ds, ifreq);*/
    if (tau + dtau >= 1.0) {
        t_vec_3d r0, r1, u;
        double   w;
        r0.x = inter[i].inters.x;
        r0.y = inter[i].inters.y;
        r0.z = inter[i].inters.z;
        r1.x = inter[i + 1].inters.x;
        r1.y = inter[i + 1].inters.y;
        r1.z = inter[i + 1].inters.z;
        u.x  = r1.x - r0.x;
        u.y  = r1.y - r0.y;
        u.z  = r1.z - r0.z;
        w    = (1.0 - tau) / dtau;
        u.x *= w;
        u.y *= w;
        u.z *= w;
        tau1point->x = r0.x + u.x;
        tau1point->y = r0.y + u.y;
        tau1point->z = r0.z + u.z;
        /* move point inside the real domain */
        while (tau1point->x >
               g_global->domain_origin.x + g_global->domain_size.x)
            tau1point->x -= g_global->domain_size.x;
        while (tau1point->x < g_global->domain_origin.x)
            tau1point->x += g_global->domain_size.x;
        while (tau1point->y >
               g_global->domain_origin.y + g_global->domain_size.y)
            tau1point->y -= g_global->domain_size.y;
        while (tau1point->y < g_global->domain_origin.y)
            tau1point->y += g_global->domain_size.y;
    } else {
        tau1point->x = inter[n - 1].inters.x;
        tau1point->y = inter[n - 1].inters.y;
        tau1point->z = inter[n - 1].inters.z;
    }
}

void fs_FormalSolutionNode(t_node *p_node, const t_vec_3d direction, int ifreq,
                           double stokes[NSTOKES], t_vec_3d *tau1point)
{
    /* NOTE: in the last point, linear interpolation is used even if the node is
     * inside the domain (consider improvement!) */
    t_vec_3d     dir, dir_opp;
    int          cnt   = 0, i, nalloc;
    t_intersect *inter = NULL,
                inter_tmp; /* array of intersections starting at p_node */
    t_fsdata *fs = NULL;   /* array of radiation field data */
    t_grid *  g  = NULL;
    t_vec_3d  rlast;
    PERR; /* you need to take into account the domain decomposition! */
    if (!g_global->period[0] || !g_global->period[1])
        Error(E_ERROR, POR_AT,
              "sorry, non-periodic gids are not yet supported");
    if (!PorMeshInitialized() || !mod_IsModuleLoaded())
        Error(E_ERROR, POR_AT, "uninitialized grid");
    if (!stokes)
        Error(E_ERROR, POR_AT, "Stokes parameters not initialized");
    if (!p_node)
        Error(E_ERROR, POR_AT, "the node is not set");
    if (!VEC_IS_UNIT_3D(direction))
        Error(E_WARNING, POR_AT, "the direction vector is not normalized");
    if (VEC_LEN_3D(direction) == 0.0)
        Error(E_ERROR, POR_AT, "zero-length vector not allowed");
    if (direction.z == 0.0)
        Error(E_ERROR, POR_AT,
              "horizontal rays not allowed in periodic models");

    g   = p_node->p_grid;
    dir = direction;
    VEC_NORMALIZE_3D(dir);
    dir_opp.x = -dir.x;
    dir_opp.y = -dir.y;
    dir_opp.z = -dir.z;
    rlast.x   = grd_XCycl(g, p_node->ix);
    rlast.y   = grd_YCycl(g, p_node->iy);
    rlast.z   = g->z[p_node->iz];
    /* no radiation passing through the domain? */
    if ((p_node->iz == g->nz - 1 && dir.z <= 0) ||
        (p_node->iz == 0 && dir.z >= 0)) {
        g_global->afunc.F_ExternalIllum(rlast, dir, ifreq, stokes);
        return;
    }
    inter                = (t_intersect *)Malloc(sizeof(t_intersect));
    inter[0].inters      = rlast;
    inter[0].ix1         = p_node->ix;
    inter[0].iy1         = p_node->iy;
    inter[0].iz1         = p_node->iz;
    inter[0].orientation = ORI_XY;
    inter[0].distance    = 0.0;
    cnt                  = 1;
    nalloc               = 1;
    while (NextIntersection(g, &inter[cnt - 1], &dir_opp, &inter_tmp)) {
        if (cnt == nalloc) {
            nalloc *= 2;
            inter = (t_intersect *)Realloc(inter, nalloc * sizeof(t_intersect));
        }
        cnt++;
        Memcpy(&inter[cnt - 1], &inter_tmp, sizeof(t_intersect));
    };
#ifdef DEBUG
    if (cnt == 1)
        IERR;
#endif
    fs = (t_fsdata *)Malloc(cnt * sizeof(t_fsdata));
    SetLongCharacteristic(g, cnt, inter, fs, dir, ifreq);
    if (tau1point) {
        FindTau1Point(g, cnt, inter, fs, ifreq, tau1point);
    }
    g_global->afunc.F_ExternalIllum(inter[cnt - 1].inters, dir, ifreq,
                                    fs[cnt - 1].i);
    if (cnt > 2) {
        for (i = cnt - 2; i > 0; i--) {
#if defined(FS_BEZIER)
            FSBezier(&fs[i + 1], &fs[i], &fs[i - 1], inter[i + 1].distance,
                     inter[i].distance);
#elif defined(FS_DELOPAR)
            FSDELOPAR(&fs[i + 1], &fs[i], &fs[i - 1], inter[i + 1].distance,
                      inter[i].distance);
#elif defined(FS_LIN)
            FSLin(&fs[i + 1], &fs[i], inter[i + 1].distance);
#else
#error "Unknown formal solver!"
#endif
        }
    }
    FSLin(&fs[1], &fs[0], inter[1].distance);
    Memcpy(stokes, fs[0].i, NSTOKES * DBLSIZE);
    free(fs);
    free(inter);
}

/*******************************************************************************/
void fs_FormalSolution1D(t_grid *p_grid) { PERR; }

/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/
/*
     The functions below serve for the purposes of visualization and
     formal solution along general ray directions.
 */
/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/

static void CalculateZLayerSFKmtxGeneralDir(t_grid *g, int iz, t_vec_3d ray,
                                            int ifreq, int tau)
{
    int ix, iy;
    for (iy = 0; iy < g->ny; iy++) {
        for (ix = 0; ix < g->nx; ix++) {
            t_node *p_node;
            p_node = &g->node[iz][iy][ix];
            g_global->afunc.F_SKGeneralDirection(
                p_node, p_node->fsdata.s, p_node->fsdata.k, ray, ifreq, tau);
        }
    }
}

/*******************************************************************************/
/* copy the radiation field at z-plain iz into a linear buffer (not Psi^*) */
static void FSToBufferGen(t_grid *g, int iz, double *buffer)
{
    int       iy, ix;
    const int ncopy =
        DBLSIZE * NSTOKES; /* memory length of the stokes vector */
    if (!g || !buffer || !g->node[iz])
        IERR;
    for (iy = 0; iy < g->ny; iy++) {
        for (ix = 0; ix < g->nx; ix++) {
            memcpy(buffer, g->node[iz][iy][ix].fsdata.i,
                   ncopy); /* stokes vector */
            buffer += NSTOKES;
        }
    }
}

/*******************************************************************************/
/* copy the linear buffer into the z-plane nodes' radiation data (not Psi^*) */
static void BufferToFSGen(t_grid *g, int iz, double *buffer)
{
    int       iy, ix;
    const int ncopy = DBLSIZE * NSTOKES;
    if (!g || !buffer || !g->node[iz])
        IERR;
    for (iy = 0; iy < g->ny; iy++) {
        for (ix = 0; ix < g->nx; ix++) {
            memcpy(g->node[iz][iy][ix].fsdata.i, buffer, ncopy);
            buffer += NSTOKES;
        }
    }
}

/*******************************************************************************/
static void fs_FSRayGeneralDir(t_grid *g, t_vec_3d ray, int ifreq,
                               double *buf_xy_in, double *buf_xy_out)
{
    int ix_first, iy_first, iz_first, ix_last, iy_last, iz_last, dix, diy, diz,
        iz;

    GetSweepIndices(g, &ix_first, &iy_first, &iz_first, &ix_last, &iy_last,
                    &iz_last, &dix, &diy, &diz, &ray);

#ifdef FS_LIN
    for (iz = iz_first; iz != iz_last + diz; iz += diz) {
        CalculateZLayerSFKmtxGeneralDir(g, iz, ray, ifreq, 0);
    }
#else
    for (iz = iz_first; iz != iz_last + 2 * diz && iz >= 0 && iz <= g->nz - 1;
         iz += diz) {
        CalculateZLayerSFKmtxGeneralDir(g, iz, ray, ifreq, 0);
    }
#endif

    if (buf_xy_in) {
        BufferToFSGen(g, iz_first, buf_xy_in);
    } else {
        SetBoundaryIllumination(g, iz_first, -1, &ray, ifreq);
    }

    /* TODO: non-periodic BC must be trated here!!! */
    if (!g_global->period[0] || !g_global->period[1])
        IERR;

    for (iz = iz_first + diz; iz != iz_last + diz; iz += diz) {
        FSPlane(g, -1, &ray, iz, dix, ix_first, diy, iy_first, diz, ifreq);
    }

    if (buf_xy_out) {
        FSToBufferGen(g, iz_last, buf_xy_out);
    }
}

/*******************************************************************************/
static void fs_FSRayGeneralDir_lc(t_grid *g, t_vec_3d ray, int ifreq,
                                  double *buf_xy)
{
    int ix_first, iy_first, iz_first, ix_last, iy_last, iz_last, dix, diy, diz,
        iz, idz;

    idz = pro_Domain_Z_ID(G_MY_MPI_RANK);

    GetSweepIndices(g, &ix_first, &iy_first, &iz_first, &ix_last, &iy_last,
                    &iz_last, &dix, &diy, &diz, &ray);

#ifdef FS_LIN
    for (iz = iz_first; iz != iz_last + diz; iz += diz) {
        CalculateZLayerSFKmtxGeneralDir(g, iz, ray, ifreq, 0);
    }
#else
    for (iz = iz_first; iz != iz_last + 2 * diz && iz >= 0 && iz <= g->nz - 1;
         iz += diz) {
        CalculateZLayerSFKmtxGeneralDir(g, iz, ray, ifreq, 0);
    }
#endif

    if (idz == 1) {
        // First vertical domain.  We should set buf_xy_in to boundary
        // illumination for first layer, and then pass it on to FSPlane_lc in
        // bux_xy_in so it can be updated for each consecutive layer
        SetBoundaryIllumination(g, iz_first, -1, &ray, ifreq);
        FSToBufferGen(g, iz_first, buf_xy);
    }

    /* TODO: non-periodic BC must be trated here!!! */
    if (!g_global->period[0] || !g_global->period[1])
        IERR;

    for (iz = iz_first + diz; iz != iz_last + diz; iz += diz) {
        FSPlane_lc(g, -1, &ray, iz, dix, ix_first, diy, iy_first, diz, ifreq,
                   buf_xy);
    }
}

/*******************************************************************************/
void fs_FSSurface_LC_XY(t_vec_3d dir, int ifr_min, int ifr_max)
{
    /* we can use blocking functions here */
    int        ifreq;
    int        min_ifreq, max_ifreq, idz, buflen;
    double *   buf_xy;
    MPI_Status status;
    t_grid *   g = &g_global->grid[0];
    int        prev_rank, next_rank;
    int        f1, f2;

    idz = pro_Domain_Z_ID(G_MY_MPI_RANK);
    pro_MinMaxIRange(ifr_min, ifr_max, &min_ifreq, &max_ifreq);

    fs_ResetRadiationCounters(g);

    buflen = g->nx * g->ny * NSTOKES;
    buf_xy = ArrayNew(buflen); /* eventually sent to master to be stored in a
                                  PSP file (if idz is equal to G_MPI_L) */

    prev_rank = G_MY_MPI_RANK - 1;
    if (idz == 1)
        prev_rank = 0;

    next_rank = G_MY_MPI_RANK + 1;
    if (idz == G_MPI_L)
        next_rank = 0;

    for (ifreq = min_ifreq; ifreq <= max_ifreq; ifreq++) {
        if (prev_rank) {
            if (MPI_SUCCESS != MPI_Recv(buf_xy, buflen, MPI_DOUBLE, prev_rank,
                                        MPI_ANY_TAG, MPI_COMM_WORLD, &status))
                CERR;
        }
        /* here we always want to fill the outgoing buffer */
        /* For long characteristics, we always want to have the buf_in, since we
           are going to use it in fs_FSRayGeneralDir_lc */
        fs_FSRayGeneralDir_lc(g, dir, ifreq, buf_xy);
        /* if next_rank==0 then data is send to master which is what we need */
        if (MPI_SUCCESS != MPI_Send(buf_xy, buflen, MPI_DOUBLE, next_rank,
                                    ifreq, MPI_COMM_WORLD))
            CERR;
    }

    free(buf_xy);
}

/*******************************************************************************/
void fs_FSSurfaceXY(t_vec_3d dir, int ifr_min, int ifr_max)
{
    /* we can use blocking functions here */
    int        ifreq;
    int        min_ifreq, max_ifreq, idz, buflen;
    double *   buf_in, *buf_out;
    MPI_Status status;
    t_grid *   g = &g_global->grid[0];
    int        prev_rank, next_rank;
    int        f1, f2;

    idz = pro_Domain_Z_ID(G_MY_MPI_RANK);
    pro_MinMaxIRange(ifr_min, ifr_max, &min_ifreq, &max_ifreq);

    fs_ResetRadiationCounters(g);

    buflen = g->nx * g->ny * NSTOKES;

    buf_in  = ArrayNew(buflen);
    buf_out = ArrayNew(buflen); /* eventually sent to master to be stored in a
                                   PSP file (if idz is equal to G_MPI_L) */

    prev_rank = G_MY_MPI_RANK - 1;
    if (idz == 1)
        prev_rank = 0;

    next_rank = G_MY_MPI_RANK + 1;
    if (idz == G_MPI_L)
        next_rank = 0;

    for (ifreq = min_ifreq; ifreq <= max_ifreq; ifreq++) {
        if (prev_rank) {
            if (MPI_SUCCESS != MPI_Recv(buf_in, buflen, MPI_DOUBLE, prev_rank,
                                        MPI_ANY_TAG, MPI_COMM_WORLD, &status))
                CERR;
        }
        /* here we always want to fill the outgoing buffer */
        fs_FSRayGeneralDir(g, dir, ifreq, prev_rank ? buf_in : NULL, buf_out);
        /* if next_rank==0 then data is send to master which is what we need */
        if (MPI_SUCCESS != MPI_Send(buf_out, buflen, MPI_DOUBLE, next_rank,
                                    ifreq, MPI_COMM_WORLD))
            CERR;
    }
    free(buf_in);
    free(buf_out);
}

/*******************************************************************************/
void fs_FSCLVFreq(double chi, double mu_min, double mu_max, int N_ray,
                  int ifreq)
{
    /* we can use blocking functions here */
    int        idz, buflen, min_idir, max_idir;
    double *   buf_in, *buf_out;
    MPI_Status status;
    t_grid *   g = &g_global->grid[0];
    int        prev_rank, next_rank;

    idz = pro_Domain_Z_ID(G_MY_MPI_RANK);

    buflen = g->nx * g->ny * NSTOKES;

    buf_in  = ArrayNew(buflen);
    buf_out = ArrayNew(
        buflen); /* this buffer is eventually sent to master to be stored
                    in a PSP file (if idz is equal to G_MPI_L) */

    prev_rank = G_MY_MPI_RANK - 1;
    if (idz == 1)
        prev_rank = 0;

    next_rank = G_MY_MPI_RANK + 1;
    if (idz == G_MPI_L)
        next_rank = 0;

    pro_MinMaxIRange(0, N_ray - 1, &min_idir, &max_idir);

    for (int idir = min_idir; idir <= max_idir; idir++) {
        t_vec_3d dir;
        double   mu, in, az = chi / 180.0 * M_PI;

        fs_ResetRadiationCounters(g);

        if (N_ray > 1)
            mu = mu_min + (mu_max - mu_min) * idir / (N_ray - 1);
        else
            mu = mu_min;

        in    = acos(mu);
        dir.x = sin(in) * cos(az);
        dir.y = sin(in) * sin(az);
        dir.z = cos(in);

        if (prev_rank) {
            if (MPI_SUCCESS != MPI_Recv(buf_in, buflen, MPI_DOUBLE, prev_rank,
                                        MPI_ANY_TAG, MPI_COMM_WORLD, &status))
                CERR;
        }
        /* here we always want to fill the outgoing buffer */
        fs_FSRayGeneralDir(g, dir, ifreq, prev_rank ? buf_in : NULL, buf_out);
        /* if next_rank==0 then data is send to master which is what we need */
        if (MPI_SUCCESS != MPI_Send(buf_out, buflen, MPI_DOUBLE, next_rank,
                                    idir, MPI_COMM_WORLD))
            CERR;
    }
    free(buf_in);
    free(buf_out);
}

/*******************************************************************************/
void fs_FSCLVFreqAV_LC(double mu_min, double mu_max, int N_incl, int N_azim,
                       int ifreq)
{
    /* we can use blocking functions here */
    int        idz, buflen, min_i, max_i;
    double *   buf_xy;
    MPI_Status status;
    t_grid *   g = &g_global->grid[0];
    int        prev_rank, next_rank;

    idz = pro_Domain_Z_ID(G_MY_MPI_RANK);

    buflen = g->nx * g->ny * NSTOKES;
    buf_xy = ArrayNew(
        buflen); /* this buffer is eventually sent to master to be stored
                    in a PSP file (if idz is equal to G_MPI_L) */

    prev_rank = G_MY_MPI_RANK - 1;
    if (idz == 1)
        prev_rank = 0;

    next_rank = G_MY_MPI_RANK + 1;
    if (idz == G_MPI_L)
        next_rank = 0;

    pro_MinMaxIRange(0, (N_incl * N_azim) - 1, &min_i, &max_i);

    for (int i = min_i; i <= max_i; i++) {
        int      i_inc, i_az;
        t_vec_3d dir;
        double   mu, in, az;

        fs_ResetRadiationCounters(g);

        i_inc = i / N_azim;
        i_az  = i % N_azim;

        az = 2.0 * M_PI * i_az / N_azim;
        if (N_incl > 1)
            mu = mu_min + (mu_max - mu_min) * i_inc / (N_incl - 1);
        else
            mu = mu_min;

        in    = acos(mu);
        dir.x = sin(in) * cos(az);
        dir.y = sin(in) * sin(az);
        dir.z = cos(in);

        if (prev_rank) {
            if (MPI_SUCCESS != MPI_Recv(buf_xy, buflen, MPI_DOUBLE, prev_rank,
                                        MPI_ANY_TAG, MPI_COMM_WORLD, &status))
                CERR;
        }
        /* here we always want to fill the outgoing buffer */
        /* For long characteristics, we always want to have the buf_in,
           since we are going to use it in fs_FSRayGeneralDir_lc */
        fs_FSRayGeneralDir_lc(g, dir, ifreq, buf_xy);
        /* if next_rank==0 then data is send to master which is what we need
         */
        if (MPI_SUCCESS !=
            MPI_Send(buf_xy, buflen, MPI_DOUBLE, next_rank, i, MPI_COMM_WORLD))
            CERR;
    }

    free(buf_xy);
}

/*******************************************************************************/
void fs_FSCLVFreqAV(double mu_min, double mu_max, int N_incl, int N_azim,
                    int ifreq)
{
    /* we can use blocking functions here */
    int        idz, buflen, min_i, max_i;
    double *   buf_in, *buf_out;
    MPI_Status status;
    t_grid *   g = &g_global->grid[0];
    int        prev_rank, next_rank;

    idz = pro_Domain_Z_ID(G_MY_MPI_RANK);

    buflen = g->nx * g->ny * NSTOKES;

    buf_in  = ArrayNew(buflen);
    buf_out = ArrayNew(
        buflen); /* this buffer is eventually sent to master to be stored
                    in a PSP file (if idz is equal to G_MPI_L) */

    prev_rank = G_MY_MPI_RANK - 1;
    if (idz == 1)
        prev_rank = 0;

    next_rank = G_MY_MPI_RANK + 1;
    if (idz == G_MPI_L)
        next_rank = 0;

    pro_MinMaxIRange(0, (N_incl * N_azim) - 1, &min_i, &max_i);

    for (int i = min_i; i <= max_i; i++) {
        int      i_inc, i_az;
        t_vec_3d dir;
        double   mu, in, az;

        fs_ResetRadiationCounters(g);

        i_inc = i / N_azim;
        i_az  = i % N_azim;

        az = 2.0 * M_PI * i_az / N_azim;
        if (N_incl > 1)
            mu = mu_min + (mu_max - mu_min) * i_inc / (N_incl - 1);
        else
            mu = mu_min;

        in    = acos(mu);
        dir.x = sin(in) * cos(az);
        dir.y = sin(in) * sin(az);
        dir.z = cos(in);

        if (prev_rank) {
            if (MPI_SUCCESS != MPI_Recv(buf_in, buflen, MPI_DOUBLE, prev_rank,
                                        MPI_ANY_TAG, MPI_COMM_WORLD, &status))
                CERR;
        }
        /* here we always want to fill the outgoing buffer */
        fs_FSRayGeneralDir(g, dir, ifreq, prev_rank ? buf_in : NULL, buf_out);
        /* if next_rank==0 then data is send to master which is what we need
         */
        if (MPI_SUCCESS !=
            MPI_Send(buf_out, buflen, MPI_DOUBLE, next_rank, i, MPI_COMM_WORLD))
            CERR;
    }

    free(buf_in);
    free(buf_out);
}

/*******************************************************************************/

void fs_FSAZAV(double theta_los, int N_azim, int ifr_min, int ifr_max)
{
    /* we can use blocking functions here */
    int        ifreq, i_az;
    int        nwl, min_i, max_i, idz, buflen;
    double *   buf_in, *buf_out;
    MPI_Status status;
    t_grid *   g = &g_global->grid[0];
    int        prev_rank, next_rank;
    int        f1, f2;

    idz = pro_Domain_Z_ID(G_MY_MPI_RANK);

    nwl = ifr_max - ifr_min + 1;

    /* we could just subdivide by frequency range, but in situations where
       the number of frequencies was not much larger than the number of
       frequency bands, then the subdivision would not be optimal. This
       involves a little bit more work, but subdivision of work is better */
    pro_MinMaxIRange(0, (N_azim * nwl) - 1, &min_i, &max_i);

    buflen = g->nx * g->ny * NSTOKES;

    buf_in  = ArrayNew(buflen);
    buf_out = ArrayNew(
        buflen); /* this buffer is eventually sent to master to be stored
                    in a PSP file (if idz is equal to G_MPI_L) */

    prev_rank = G_MY_MPI_RANK - 1;
    if (idz == 1)
        prev_rank = 0;

    next_rank = G_MY_MPI_RANK + 1;
    if (idz == G_MPI_L)
        next_rank = 0;

    theta_los = theta_los / 180.0 * M_PI;

    for (int i = min_i; i <= max_i; i++) {
        double   az;
        t_vec_3d dir;

        fs_ResetRadiationCounters(g);

        i_az  = i / nwl;
        ifreq = (i % nwl) +
                ifr_min; /* need to shift by ifr_min, in case ifr_min != 0 */

        az = 2.0 * M_PI * i_az / N_azim;

        dir.x = sin(theta_los) * cos(az);
        dir.y = sin(theta_los) * sin(az);
        dir.z = cos(theta_los);

        if (prev_rank) {
            if (MPI_SUCCESS != MPI_Recv(buf_in, buflen, MPI_DOUBLE, prev_rank,
                                        MPI_ANY_TAG, MPI_COMM_WORLD, &status))
                CERR;
        }
        /* here we always want to fill the outgoing buffer */
        fs_FSRayGeneralDir(g, dir, ifreq, prev_rank ? buf_in : NULL, buf_out);
        /* if next_rank==0 then data is send to master which is what we need
         */
        if (MPI_SUCCESS !=
            MPI_Send(buf_out, buflen, MPI_DOUBLE, next_rank, i, MPI_COMM_WORLD))
            CERR;
    }

    free(buf_in);
    free(buf_out);
}

static int TauSurfaceXY(t_grid *g, double *tau_map, double *xyz_map,
                        int *index_map, int *ori_map, double tau, int ifreq,
                        t_vec_3d dir)
{
    int ix_first, iy_first, iz_first, ix_last, iy_last, iz_last, dix, diy, diz,
        sendToMaster = 1;
    int      ix, iy, iz, period = g_global->period[0] && g_global->period[1];
    double   dtau;
    t_fsdata fsdata1, fsdata2;

    GetSweepIndices(g, &ix_first, &iy_first, &iz_first, &ix_last, &iy_last,
                    &iz_last, &dix, &diy, &diz, &dir);

#ifdef FS_LIN
    for (iz = iz_first; iz != iz_last + diz; iz += diz) {
        CalculateZLayerSFKmtxGeneralDir(g, iz, dir, ifreq, 1);
    }
#else
    for (iz = iz_first; iz != iz_last + 2 * diz && iz >= 0 && iz <= g->nz - 1;
         iz += diz) {
        CalculateZLayerSFKmtxGeneralDir(g, iz, dir, ifreq, 1);
    }
#endif

    /* printf("nx=%.3e ny=%.3e nz=%.3e\n", dir.x, dir.y, dir.z); */

    // Not allowed different periodicity in horizontal dimensions
    if ((g_global->period[0] && !g_global->period[1]) ||
        (!g_global->period[0] && g_global->period[1]))
        IERR;

    if (period) {
        for (iy = iy_first; iy != iy_last + diy; iy += diy) {
            for (ix = ix_first; ix != ix_last + dix; ix += dix) {
                t_intersect next_inters, curr_inters;
                double      xtau, ytau, ztau;
                // Stop calculation at this surface point if we've reached to
                // the desired tau
                if (tau_map[iy * g->nx + ix] >= tau)
                    continue;

                // Putting the input arrays in the intersection structure
                curr_inters.ix1      = index_map[iy * g->nx * 3 + ix * 3 + 0];
                curr_inters.iy1      = index_map[iy * g->nx * 3 + ix * 3 + 1];
                curr_inters.iz1      = index_map[iy * g->nx * 3 + ix * 3 + 2];
                curr_inters.distance = -1.0e100;
                curr_inters.orientation = ori_map[iy * g->nx + ix];
                curr_inters.inters.x    = xyz_map[iy * g->nx * 3 + ix * 3 + 0];
                curr_inters.inters.y    = xyz_map[iy * g->nx * 3 + ix * 3 + 1];
                curr_inters.inters.z    = xyz_map[iy * g->nx * 3 + ix * 3 + 2];

                /* printf("****iy=%d ix=%d****\n", iy, ix); */

                /* Stop ray tracing when:
                 * - the intersection is on the plane XY (the z coordinate of
                 * the intersection coincides with the node of reference in z)
                 * - and in the last z-plane of the current domain
                 */
                while (!(curr_inters.orientation == ORI_XY &&
                         curr_inters.iz1 == iz_last)) {
                    if (!NextIntersection(g, &curr_inters, &dir, &next_inters))
                        IERR;
                    /* printf("curr_inters: ori=%d ix=%d iy=%d iz=%d\n",
                     * curr_inters.orientation, curr_inters.ix1,
                     * curr_inters.iy1, curr_inters.iz1); */
                    /* printf("next_inters: ori=%d ix=%d iy=%d iz=%d\n",
                     * next_inters.orientation, next_inters.ix1,
                     * next_inters.iy1, next_inters.iz1); */

                    // Interpolating kmtx to the current intersection point and
                    // to the next intersection points
                    InterpolateRadiation(g, &curr_inters, &fsdata1, 0);
                    InterpolateRadiation(g, &next_inters, &fsdata2, 0);
                    // Optical depth between the two intersection points
                    dtau = 0.5 * (fsdata1.k[ETA_I] + fsdata2.k[ETA_I]) *
                           next_inters.distance;
                    tau_map[iy * g->nx + ix] += dtau;

                    /* printf("[%d] (%d,%d) ifreq=%d c_pos=(%d,%d,%d)
                     * n_pos=(%d,%d,%d)",
                     */
                    /*    G_MY_MPI_RANK, ix,iy, ifreq, */
                    /*    curr_inters.ix1,curr_inters.iy1,curr_inters.iz1, */
                    /*    next_inters.ix1,next_inters.iy1,next_inters.iz1); */

                    if (tau_map[iy * g->nx + ix] >= tau) {
                        xtau = (tau - (tau_map[iy * g->nx + ix] - dtau)) /
                                   dtau * next_inters.distance * dir.x +
                               curr_inters.inters.x;
                        ytau = (tau - (tau_map[iy * g->nx + ix] - dtau)) /
                                   dtau * next_inters.distance * dir.y +
                               curr_inters.inters.y;
                        ztau = (tau - (tau_map[iy * g->nx + ix] - dtau)) /
                                   dtau * next_inters.distance * dir.z +
                               curr_inters.inters.z;
                        break;
                    }
                    // Updating the current intersection to keep on computing
                    // the optical depth until we reach to the bottom of the
                    // z-domain
                    curr_inters = next_inters;
                }
                // Check if we've reached at the desired height in all the
                // surface points. If it is, we can send the results to the
                // master process, if not we have to keep on the calculations in
                // the other z-domains
                if (sendToMaster && tau_map[iy * g->nx + ix] >= tau)
                    sendToMaster = 1;
                else
                    sendToMaster = 0;

                // Puking the intersection structure information to the input
                // arrays
                if (tau_map[iy * g->nx + ix] >= tau) {
                    xyz_map[iy * g->nx * 3 + ix * 3 + 0] = xtau;
                    xyz_map[iy * g->nx * 3 + ix * 3 + 1] = ytau;
                    xyz_map[iy * g->nx * 3 + ix * 3 + 2] = ztau;
                } else {
                    xyz_map[iy * g->nx * 3 + ix * 3 + 0] = curr_inters.inters.x;
                    xyz_map[iy * g->nx * 3 + ix * 3 + 1] = curr_inters.inters.y;
                    xyz_map[iy * g->nx * 3 + ix * 3 + 2] = curr_inters.inters.z;
                }
                index_map[iy * g->nx * 3 + ix * 3 + 0] = curr_inters.ix1;
                index_map[iy * g->nx * 3 + ix * 3 + 1] = curr_inters.iy1;
                index_map[iy * g->nx * 3 + ix * 3 + 2] = curr_inters.iz1;
                ori_map[iy * g->nx + ix] = curr_inters.orientation;
            }
        }
    } else {
        Error(
            E_ERROR, POR_AT,
            "only periodic conditions are implemented in computation of tau.");
    }

    return sendToMaster;
}

void fs_GetTau(double tau, t_vec_3d dir, int ifr_min, int ifr_max)
{
    int ifreq, min_ifreq, max_ifreq, idz, iy, ix;

    double *tau_xyz_map; // "4D" array allocated array. tau_map and xyz_map
                         // point into it.
    double *tau_map; // [iy*Nx+ix] optical depth map along the ray started at
                     // each surface point
    double *xyz_map; // [iy*Nx*3+ix*3+{x,y,z}] position of the last intersection
    int *   index_map; // [iy*Nx*3+ix*3+{ix,iy,iz}] indexes of the corresponding
                       // node for each intersection
    int *ori_map; // [iy*Nx+ix] orientation of interesction at each ix,iy point
    MPI_Status status;
    t_grid *   g = &g_global->grid[0];
    int        prev_rank, next_rank;
    int        f1, f2, buflen;

    idz = pro_Domain_Z_ID(G_MY_MPI_RANK);
    pro_MinMaxIRange(ifr_min, ifr_max, &min_ifreq, &max_ifreq);

    fs_ResetRadiationCounters(g);

    if (dir.z > 0.0)
        Error(E_ERROR, POR_AT, "accepted only rays ingoing to the atmosphere.");

    buflen = g->ny * g->nx;

    tau_xyz_map = (double *)Malloc(sizeof(double) * buflen * 4);
    tau_map     = &tau_xyz_map[0];
    xyz_map     = &tau_xyz_map[buflen];
    index_map   = (int *)Malloc(sizeof(int) * buflen * 3);
    ori_map     = (int *)Malloc(sizeof(int) * buflen);

    prev_rank = G_MY_MPI_RANK + 1;
    if (idz == G_MPI_L)
        prev_rank = 0;

    next_rank = G_MY_MPI_RANK - 1;
    if (idz == 1)
        next_rank = 0;

    for (ifreq = min_ifreq; ifreq <= max_ifreq; ifreq++) {
        // Receiving arrays from upper z-domain, if it exists
        if (prev_rank) {
            int waitMsg;

            /* printf("I'm %d and I'm waiting if I will receive data of ifreq=%d
             * from %d\n", G_MY_MPI_RANK, ifreq, prev_rank); */
            if (MPI_SUCCESS != MPI_Recv(&waitMsg, 1, MPI_INT, prev_rank, ifreq,
                                        MPI_COMM_WORLD, &status))
                CERR;
            if (waitMsg) {
                /* printf("I'm %d and I'm waiting data of ifreq=%d from %d\n",
                 * G_MY_MPI_RANK, ifreq, prev_rank); */
                if (MPI_SUCCESS != MPI_Recv(tau_xyz_map, buflen * 4, MPI_DOUBLE,
                                            prev_rank, ifreq, MPI_COMM_WORLD,
                                            &status))
                    CERR;
                if (MPI_SUCCESS != MPI_Recv(index_map, buflen * 3, MPI_INT,
                                            prev_rank, ifreq, MPI_COMM_WORLD,
                                            &status))
                    CERR;
                if (MPI_SUCCESS != MPI_Recv(ori_map, buflen, MPI_INT, prev_rank,
                                            ifreq, MPI_COMM_WORLD, &status))
                    CERR;
            } else {
                /* printf("I'm %d and I'm not going to receive data of ifreq=%d
                 * from %d, so I'm going to notify to %d.\n", G_MY_MPI_RANK,
                 * ifreq, prev_rank, next_rank); */
                if (next_rank) {
                    if (MPI_SUCCESS != MPI_Send(&waitMsg, 1, MPI_INT, next_rank,
                                                ifreq, MPI_COMM_WORLD))
                        CERR;
                } else {
                    /* printf("I'm %d and now that I think of it, master does
                     * not care if receive or not data of ifreq=%d from me.\n",
                     * G_MY_MPI_RANK, ifreq);
                     */
                }
                continue;
            }
        } else {
            // Initialization of tau_map, xyz_map and index_map for the
            // processes treating the surface
            for (iy = 0; iy < g->ny; iy++) {
                for (ix = 0; ix < g->nx; ix++) {
                    t_node *p_node = &g->node[g->nz - 1][iy][ix];
                    tau_map[iy * g->nx + ix] =
                        0.0; // optical depth at the surface
                    xyz_map[iy * g->nx * 3 + ix * 3 + 0]   = NODE_X(p_node);
                    xyz_map[iy * g->nx * 3 + ix * 3 + 1]   = NODE_Y(p_node);
                    xyz_map[iy * g->nx * 3 + ix * 3 + 2]   = NODE_Z(p_node);
                    index_map[iy * g->nx * 3 + ix * 3 + 0] = ix;
                    index_map[iy * g->nx * 3 + ix * 3 + 1] = iy;
                    index_map[iy * g->nx * 3 + ix * 3 + 2] = g->nz - 1;
                    ori_map[iy * g->nx + ix]               = ORI_XY;
                }
            }
        }
        /* printf("I'm %d and I'm calculating tau at ifreq=%d in the
         * z-domain=%d\n", G_MY_MPI_RANK, ifreq, idz); */

        // Calculating tau_map at this z-domain
        // If all the points in tau_map are >= tau then we send tau_map and
        // xyz_map to the master process
        if (TauSurfaceXY(g, tau_map, xyz_map, index_map, ori_map, tau, ifreq,
                         dir) ||
            !next_rank) {
            if (next_rank) {
                int sendMsg = 0;
                /* printf("I'm %d and I'm telling to %d that will not receive
                 * data of ifreq=%d\n", G_MY_MPI_RANK, next_rank, ifreq); */
                if (MPI_SUCCESS != MPI_Send(&sendMsg, 1, MPI_INT, next_rank,
                                            ifreq, MPI_COMM_WORLD))
                    CERR;
            }
            /* printf("I'm %d and I'm sending data of ifreq=%d to master\n",
             * G_MY_MPI_RANK, ifreq); */
            if (MPI_SUCCESS != MPI_Send(tau_xyz_map, buflen * 4, MPI_DOUBLE, 0,
                                        ifreq, MPI_COMM_WORLD))
                CERR;
        }
        // If not, keep on the ray path to next z-domains
        else {
            int sendMsg = 1;
            /* printf("I'm %d and I'm sending data of ifreq=%d to %d\n",
             * G_MY_MPI_RANK, ifreq, next_rank); */
            // Optical depth map, position of the last intersection and the
            // corresponding node index calculated at the lower z plane of the
            // current z-domain sent to the lower z-domain or at the master
            // process if it proceeds
            if (MPI_SUCCESS != MPI_Send(&sendMsg, 1, MPI_INT, next_rank, ifreq,
                                        MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Send(tau_xyz_map, buflen * 4, MPI_DOUBLE,
                                        next_rank, ifreq, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Send(index_map, buflen * 3, MPI_INT,
                                        next_rank, ifreq, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Send(ori_map, buflen, MPI_INT, next_rank,
                                        ifreq, MPI_COMM_WORLD))
                CERR;
        }
    }

    // Freeing arrays
    free(tau_xyz_map);
    free(index_map);
    free(ori_map);

    return;
}
