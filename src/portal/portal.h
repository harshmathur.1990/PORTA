/* Copyright (C) 2013 by Jiří Štěpán, stepan@asu.cas.cz */

/* public header file to be included by the external atomic-functionality
 * modules */

#ifndef PORTAL_H_
#define PORTAL_H_

#include "def.h"        /* basic type definitions and system checks */
#include "directions.h" /* directions quantization                  */
#include "error.h"      /* message/warning/error reports            */
#include "matika.h"     /* mathematics                              */
#include "mem.h"        /* safe memory allocation routines          */
#include "physics.h"    /* physical constants                       */
#include "pstream.h"
#include "stack.h" /* stack of messages                        */
#include "voigt.h" /* line profiles                            */

/* return 1 if the grid mesh has been initialized : i.e., if grid[0] contains
 * some data */
extern int PorMeshInitialized(void);

/* get unit vector in the direction of iray */
extern void PorGetIDir(int iray, t_vec_3d *dir);

/* get ray mu, sin and cos of the azimuth */
extern void PorGetIGoni(int iray, double *mu, double *sin_az, double *cos_az);

/* set array of T^K_Q tensorial components for a given ray direction; tkq array
 * must have NTKQ elements */
extern void PorGetTKQ(int iray, double tkq[NTKQ]);

/* extract the complex element T^K_Q(istokes) from the complete array of
 * geometrical tensors; for unphysical indices (such as K<0 or Q>K, etc,)
 * returns zero; WARNING: stokes parameters are always indexed as I==0, Q==1,
 * U==2, V==3, not by STOKES_I etc. */
extern t_complex PorExtractTKQ(int istokes, int K, int Q,
                               const double tkq[NTKQ]);

/* get the complex value of the J^K_Q tensorial component from the t_jkq array
 */
extern t_complex PorGetJKQ(int K, int Q, const t_jkq *jkq);

/* angular quadrature of a given ray */
extern double PorAngQuadrature(int iray);

/* domain size along the X axis */
extern double PorDomainSizeX(void);

/* domain size along the Y axis */
extern double PorDomainSizeY(void);

/* number of grid point per X axis */
extern int PorNX(void);

/* number of grid point per Y axis */
extern int PorNY(void);

/* number of grid point per Z axis */
extern int PorNZ(void);

/* pointer to the array of X coordinates of the grid */
extern double *PorGetXCoords(void);

/* pointer to the array of Y coordinates of the grid */
extern double *PorGetYCoords(void);

/* pointer to the array of Z coordinates of the grid */
extern double *PorGetZCoords(void);

/* lower-left-front point of the domain */
extern void PorOrigin(t_vec_3d *r);

/* number of frequencies in the model */
extern int PorNFreqs(void);

/* set the model laboratory frequenies; returns 1 on success, 0 otherwise; (you
 * have to regenerate the line profiles afterwards) */
extern int PorSetFreqs(int n, const double *freqs);

/* pointer to the lab-frequencies array */
extern const double *PorFreqs(void);

/* total number of nodes in the grid; if grid is not initialized, returns 0 (and
 * a warning message) */
extern int PorNNodes(void);

/* initializes the global pointers to the module functions to NULL */
extern void PorResetAFunc(void);

/* checks if all g_global.afunc functions are set */
extern int PorIsAFuncSet(void);

/* get ID of the running process (can be used by modules to check what to print
 * to stdout/err) */
extern int PorGetProcessRank(void);

/* gets maximum value of a variable a among all the domains (called by all
 * slaves) */
extern double PorGetDomainMax(double a);

/* provides the minimum and maximum frequency indices of a given process */
extern void PorMinMaxIFreq(int *min_ifreq, int *max_ifreq);

/* position vector in a 3D space of a grid node p_node */
extern t_vec_3d PorGetNodeCoords(t_node *p_node);

/***************************************************************************/
/*                    interface for external modules                       */
/***************************************************************************/
typedef struct {
    /* allocate (if needed) and initialize the internal data structures of the
     * grid node p_node; note that the radiation field variables (J^K_Q tensors,
     * Lambda-diagonals, etc.) stored in p_node should be initialized to zero */
    void (*F_Init_p_data)(t_node * /* p_node */);

    /* free the memory allocated for the grid-node physical data */
    void (*F_Free_p_data)(void * /* p_data */);

    /* encodes all the node data contained in p_data (except for temporary
     * variables) into a buffer that can be understood by the particular module
     * (can be decoded by F_EncodeNodeData()); the buffer is of length
     * F_GetNodeDataSize() and must be freed by subsequently the caller */
    void *(*F_EncodeNodeData)(void * /* p_data */);

    /* HDF5 version, as F_EncodeNodeData above, but the buffer is passed as the
     * first argument */
    int (*F_EncodeNodeData_h5)(void * /* buf */, void * /* pdata */);

    /* decodes the data encoded by F_EncodeNodeData() and sets a given grid node
     */
    /* TODO: replace pointer p_node by p_data of the node */
    void (*F_DecodeNodeData)(void * /* buffer */, t_node * /* p_node */);

    /* HDF5 version */
    void (*F_DecodeNodeData_h5)(void * /* buffer */, t_node * /* p_node */);

    /* size of the node data returned by F_EncodeNodeData() and read by
     * F_DecodeNodeData(); equal to the size of node data in a PMD file */
    int (*F_GetNodeDataSize)(void);

    /* external illumination function at point r in the direction dir and
     * frequency ifreq; gives Stokes parameters stokes */
    void (*F_ExternalIllum)(const t_vec_3d /* r */, const t_vec_3d /* dir */,
                            int /* ifreq */, double[NSTOKES] /* stokes */);

    /* source function sf & K-matrix kmtx at the point p_node in a general
     * direction of unit vector dir, ifreq frequency index */
    void (*F_SKGeneralDirection)(t_node * /* p_node */,
                                 double[NSTOKES] /* sf */,
                                 double[NKMTX] /* kmtx */,
                                 const t_vec_3d /* dir */, int /* ifreq */,
                                 int /* tau */);

    /* source function sf & K-matrix kmtx at the point p_node in direction of
     * ray iray, ifreq frequency index */
    void (*F_SK)(t_node * /* p_node */, double[NSTOKES] /* sf */,
                 double[NKMTX] /* kmtx */, int /* iray */, int /* ifreq */);

    /* process the local Stokes vector and Psi^* stored in fsdata for a given
     * iray direction and ifreq frequency and store in the atomic data
     * structures */
    void (*F_AddFS)(t_node * /* p_node */, int /* iray */, int /* ifreq */);

    /* gets the preconditioned ESE matrix and the r.h.s of ESE (which must be
     * constant for use in MultiGrid); this function is only needed when MG
     * iteration is used */
    void (*F_GetESE)(t_node * /* p_node */, double ** /* ese */,
                     double * /* f */);

    /* solve the ESE in a given grid node and return maximum relative change of
     * populations; called by Jacobi solver after every formal solution of RT */
    double (*F_SolveESE)(t_node * /* p_node */);

    /* return number of real dm elements */
    int (*F_GetNDM)(void);

    /* returns pointer to array of F_GetNDM() dm elements equal to 1 if given
     * elements is a level population and 0 otherwise */
    int *(*F_GetPopsPositions)(void);

    /* get array of real density matrix data in given node; length of the
     * supplied array must be F_GetNDM() */
    void (*F_GetDM)(t_node * /* p_node */, double * /* data */);

    /* complementary to F_GetDM: sets the node data of p_node; length of the
     * double array is same as in F_GetDM() */
    void (*F_SetDM)(t_node * /* p_node */, double * /* data */);

    /* kinetic plasma temperature at a given node */
    double (*F_GetTemperature)(t_node * /* p_node */);

    /* magnetic field vector */
    t_vec_3d (*F_GetMagneticVector)(t_node * /* p_node */);

    /* array of radiation field tensors necessary for calculation of ESE in a
     * given module; typically the J^K_Q averaged over sp. lines & corresponding
     * psi/L-operators; n_data: number of elements in the returned array */
    double *(*F_GetRadiation)(t_node * /* p_node */, int * /* n_data */);

    /* array of radiation field data of the format of F_GetRadiation is being
     * set in the given node */
    void (*F_SetRadiation)(t_node * /* p_node */, double * /* data */);

    /* set the radiation tensors and psi-operator diagonals to zero */
    void (*F_ResetRadiation)(t_node * /* p_node */);

    /* write header data to a file; returns negative if error occurred */
    int (*F_IO_WriteHeader)(FILE *f);

    /* write header data to a file (HDF5 version); returns grid data dataset
       (g_data) identifier. negative if error occurred */
    hid_t (*F_IO_WriteHeader_h5)(hid_t file_id);

    /* called after the grid is loaded in order to synchronize global
     * module-dependent variables among the MPI processes */
    void (*F_SyncGlobals)(void);

    /* called in every Jacobi iteration just before the ESE solutions is started
     * (in ese_SolveGridPrecondESE and always once before all the F_SetDM in the
     * grid) (allows to initialize and synchronize module-dependent global
     * variables such as min&max profile widths) */
    void (*F_ESEBegins)(void);

    /* called in every Jacobi iteration just after solution of ESE (allows to
     * initialize and synchronize module-dependent global variables such as
     * min&max profile widths due to Stark broadening) */
    void (*F_ESEEnds)(void);

} t_afunc;

#endif /* PORTAL_H_ */
