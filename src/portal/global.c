#include "global.h"
#include "def.h"
#include "directions.h"
#include "error.h"
#include "grid.h"
#include "mem.h"
#include "process.h"
#include "stack.h"
#include "thread_io.h"
#include <string.h>
#include <time.h>

int        G_MY_MPI_RANK; /* rank of the current MPI process */
int        G_MPI_NPROCS;  /* total number of MPI processes = mpi_L*mpi_M+1*/
MPI_Status G_MPI_STATUS;

int G_MPI_L; /* number of horizontal slices (default: DEF_MPI_PROCS_L) */
int G_MPI_M; /* number of wavelength blocks per horizontal slice (default:
                DEF_MPI_PROCS_M) */

int G_MY_ZBAND_ID;    /* Z-band of my process */
int G_MY_FREQBAND_ID; /* Frequency-band of my process */

/* zband_comm:   communicator with all processes sharing the same Z-band as my
   process feqband_comm: communicator with all processes sharing the same
   Frequency-band as my process */
MPI_Comm zband_comm, freqband_comm, slaves_comm;

#ifdef THREAD_IO
MPI_Comm io_comm;
#endif

FILE *G_POR = NULL; /* the model script file */

t_global_vars *g_global = NULL;

static time_t g_start_time;

/* returns number of seconds which lasted since initialization of the portal
 * library */
double LibRunningTime(void)
{
    time_t now = time(NULL);
    return fabs(difftime(now, g_start_time));
}

void InitPorta(void)
{
    int i;
#ifdef DEBUG
    srand(0);
#else
    srand((unsigned)time(NULL));
#endif

    g_start_time = time(NULL);

    if (G_MY_MPI_RANK == 0) {
        PrintfErr("%c%c %s %d.%d.%d %s (build %s, %s"
#ifdef DEBUG
                  ", DEBUGGING BUILD"
#endif
                  ")\n",
                  COMMENT_CHAR, COMMENT_CHAR, POR_LABEL, POR_VER, POR_SUBVER,
                  POR_PATCH, POR_EXTRAVER, __DATE__, __TIME__);
        PrintfErr("%c%c Running %d slave processes (%d subdomains, %d "
                  "frequency bands)\n",
                  COMMENT_CHAR, COMMENT_CHAR, G_MPI_NPROCS - 1, G_MPI_L,
                  G_MPI_M);
    }
    stk_Init();
    g_global = (t_global_vars *)Malloc(sizeof(t_global_vars));

#ifdef THREAD_IO
    g_global->logfile = tmpfile();
    if (!g_global->logfile) {
        fprintf(stderr, "WARNING(%s): cannot open a log-file\n", POR_AT);
    }
#else
    g_global->logfile = fopen(LOG_FILENAME, "w");
    if (!g_global->logfile) {
        fprintf(stderr, "WARNING(%s): cannot open a log-file\n", POR_AT);
    }
#endif
    g_global->stack_output = DEF_SOUT;
    stk_Add(1, POR_AT, "initializing global variables");
    dir_MakeDirections(DEF_NDI_INCL, DEF_NDI_AZIM, &g_global->dirs);
    g_global->n_freq          = 0;
    g_global->max_n_grids     = 1;
    g_global->current_n_grids = 0;
    g_global->period[0] = g_global->period[1] = 1;
    g_global->domain_size.x                   = g_global->domain_size.y =
        g_global->domain_size.z               = 0.0;
    g_global->domain_origin.x                 = g_global->domain_origin.y =
        g_global->domain_origin.z             = 0.0;
    for (i = 0; i < MAX_N_GRIDS; i++) {
        g_global->grid[i].node = NULL;
        g_global->grid[i].nx = g_global->grid[i].ny = g_global->grid[i].nz = 0;
    }
    g_global->module_header_fsize = -1;
    g_global->module_node_fsize   = -1;
#ifdef DEF_MONOTONIC_OPACITY
    g_global->mono_opacity = DEF_MONOTONIC_OPACITY;
#else
    g_global->mono_opacity = 0;
#endif
    /* note that the following default values may be inconsistent with the total
     * number of processes */
    memset(g_global->comment, 0, PMD_COMMENT_LEN + 1);
    PorResetAFunc();
}

void ExitPorta(int stop)
{
    if (!G_MY_MPI_RANK)
        stk_Add(1, POR_AT, "exiting %s", POR_LABEL);
    grd_ReleaseDomain();
    /*CleanBath();*/
    stk_Free();
    if (fclose(g_global->logfile)) {
        fprintf(stderr, "ERROR(%s): cannot close a log-file", POR_AT);
    }
    free(g_global);
    g_global = NULL;
    if (!G_MY_MPI_RANK)
        PrintfErr("%c%c bye\n", COMMENT_CHAR, COMMENT_CHAR);
    /* TODO: use MPI_Abort() */
    if (stop) {
        MPI_Abort(MPI_COMM_WORLD, EXIT_SUCCESS);
        exit(EXIT_SUCCESS);
    }
}
