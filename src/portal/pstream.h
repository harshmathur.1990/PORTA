/*
 * provides simple implementation of reading & writing of the data streams
 */

#ifndef PSTREAM_H_
#define PSTREAM_H_

typedef struct t_pstream {
    char *buffer;    /* physically allocated buffer provided by the caller */
    char *offset;    /* current offset in the buffer */
    int   length;    /* total legth of the buffer */
    int   writeable; /* nonzero if the stream has been opened for writing */
} t_pstream;

#define PSTREAM_BYTE   0
#define PSTREAM_INT    1
#define PSTREAM_DOUBLE 2
#define PSTREAM_FLOAT  3

/* initialized the t_pstream structure (no memory allocation);\
 * no data is copied from *data to the stream
 * data has to be allocated and stuffed with
 * writeable: if nonzero, the data stream can be changed by writing
 */
extern t_pstream pst_NewStream(int length, char *data, int writeable);

/* reads a structure of a given type (PSTREAM_DOUBLE etc) into memory location
 * "result" the position of offset in the stream is shifted appropriately return
 * value: number of written objects (zero on error)
 */
extern int pst_Read(t_pstream *stream, int type, void *result);

/* analogous to pst_Read() for writing into the data stream
 * src: pointer to the variable of the given type to be written into the stream
 * return value: number of read objects (zero on error)
 */
extern int pst_Write(t_pstream *stream, int type, void *src);

#endif /* PSTREAM_H_ */
