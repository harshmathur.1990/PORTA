PORTA continuum polarization module         {#mainpage}
===================================

The continuum polarization module is described in section 
[contpol](https://polmag.gitlab.io/PORTA/Modules/index.html#contpol) of the PORTA User Manual.

@author Jiří Štěpán, stepan@asu.cas.cz
@author Tanausú del Pino Alemán, tanausu@iac.es
@author Ángel de Vicente, angel.de.vicente@iac.es

