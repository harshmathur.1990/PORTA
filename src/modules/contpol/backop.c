/*####################################################################
############################## HEADER ################################
######################################################################
#
#  Authors:
#     Tanausú del Pino Alemán (IAC) - TdPA
#  Start:
#     05/21/2019
#  Last version:
#     06/21/2019 V0.0.1
#
######################################################################
######################################################################
#
#  Changelog:
#
#     07/15/2019:    V0.0.3 - Turns out the static tag was needed for
#                             completely other (C) reasons (TdPA)
#
#     06/24/2019:    V0.0.2 - Removed not needed static tags (TdPA)
#
#     06/21/2019:    V0.0.1 - Explicitly initialize out in backop
#                             because it was not being reseted upon
#                             iterative calls (TdPA)
#                           - Same for other sub-functions (TdPA)
#
#     05/21/2019:    V0.0.0 - Started to write (TdPA)
#
######################################################################
######################################################################
#
#  Known bugs:
#
######################################################################
######################################################################
#
#  Data:
#
#    Module to compute some (the hydrogen and electron related)
#  continuum opacity quantities for a given temperature and H and
#  electron densities
#
#    This routines have been adapted from the ones in
#  backgroundaux_mod.f90 from the HanleRT code (del Pino et al. 2016)
#
######################################################################
######################################################################
#
#  doxygen meta-data:
*//*
* @file backop.c
* @brief Background H and e- opacity
* @author Tanausú del Pino Alemán, tanausu@iac.es
* @note Assumes LTE for H
*//*
#
######################################################################
######################################################################
####################################################################*/

/* Includes */
#include "portal/portal.h"
#include <float.h>
#include <math.h>
#include <memory.h>
#include <stdlib.h>

/* Global */
double        g_2hc2  = 1.47449919995e-47;        /**< @brief 2h/c^2 [cgs]*/
static double g_convF = 1.9864474e-11;            /**< @brief h*c for
                                                              [10^5 cm^-1]*/
static double g_kb = 1.38064852e-23;              /**< @brief Boltzmann Constant
                                                              [J K^-1]*/
static double g_ktoev = 5.03974756e3;             /**< @brief T[eV] to
                                                              g_ktoev/T[K]*/
static double g_h = 6.62607004e-34;               /**< @brief Planck constant
                                                              [J*s]*/
static double g_c = 2.99792458e-1;                /**< @brief Lightspeedi
                                                              [10^9 m s^-1]*/
static double g_c2 = 1.438769e1;                  /**< @brief h*c*10^-4/kb for
                                                              [10^5 cm^-1]*/
static double g_ryd = 1.09737405;                 /**< @brief Rydberg energy
                                                              [10^5 cm^-1]*/
static double g_fktoJ = 1.986445824e-18;          /**< [10^5 cm^-1] to [J]*/
static double g_me    = 9.10938356e-31;           /**< @brief e- mass [kg]*/
static double g_qel   = 1.60217662e-19;           /**<  @brief e- charge [C]*/
static double g_pi    = 3.14159265358979323846e0; /**< @brief pi*/
static double g_eps0  = 8.854187817e-12;          /**< @brief Vacuum
                                                              permittivity
                                                              [F m^-1] */
static double g_pi4eps0 = 1.11265005598e-10;      /**< @brief 4*pi*Vacuum
                                                              permittivity
                                                              [F m^-1]*/
static double g_dw = 2.5e5; /**< @brief Reference Doppler width
                                        [cm/s]*/

/* Atomic structure */
typedef struct bo_Atom_class {
    int     nlevel; /**< @brief Number of levels*/
    int     ntran;  /**< @brief Number of transitions*/
    int     nphot;  /**< @brief Number of photoionizations*/
    double *E;      /**< @brief Energy of levels [10^5 cm^-1]*/
    double *g;      /**< @brief Degeneration of levels*/
    int *   stg;    /**< @brief Ionization stage*/
    int *   p_il;   /**< @brief Lower level of photoionization
                                transition*/
    int *p_iu;      /**< @brief Upper level of photoionization
                                transition*/
    int *   r_il;   /**< @brief Lower level of b-b transition*/
    int *   r_iu;   /**< @brief Upper level of b-b transition*/
    double *p_edge; /**< @brief Edge frequency for photoionization
                                [10^5 cm^-1]*/
    double *p_l0;   /**< @brief Maximum frequency for photoionization
                                [10^5 cm^-1]*/
    double *Aul;    /**< @brief Einstein coefficient for
                                espontaneous emission [10^8 s^-1]*/
    double *r_Dw;   /**< @brief Total Doppler widths to cover with each
                                radiative transition*/
    double *r_nu;   /**< @brief Frequency of each radiative transition
                                [Hz]*/
    double *Nj;     /**< @brief NLTE population of each level [cm^-3]*/
    double *Nstarj; /**< @brief LTE population of each level [cm^-3]*/
} bo_Atom;

/****************************************************************************/
/**
 * Cubic spline interpolator, Gets the interpolation coefficients
 *
 * @brief Get cubic spline coefficients
 * @param [in] x Input x axis
 * @param [in] y Input y axis
 * @param [out] b Coefficient of spline interpolation
 * @param [out] c Coefficient of spline interpolation
 * @param [out] d Coefficient of spline interpolation
 * @param [in] n Size of x, y, b, c, and d
 * @return void
 *
 ****************************************************************************/
static void bo_splinecoeff(double *x, double *y, double *b, double *c,
                           double *d, int n)
{
    int    i, j, gap;
    double h;

    gap = n - 1;

    /* Check input */
    if (n < 2) {
        return;
    }
    if (n < 3) {
        /* Linear interpolation */
        b[0] = (y[1] - y[0]) / (x[1] - x[0]);
        c[0] = 0.0;
        d[0] = 0.0;
        b[1] = b[0];
        c[1] = 0.0;
        d[1] = 0.0;
        return;
    }

    /* Step 1: preparation */
    d[0] = x[1] - x[0];
    c[1] = (y[1] - y[0]) / d[0];
    for (i = 1; i < gap; i++) {
        d[i]     = x[i + 1] - x[i];
        b[i]     = 2.0 * (d[i - 1] + d[i]);
        c[i + 1] = (y[i + 1] - y[i]) / d[i];
        c[i]     = c[i + 1] - c[i];
    }

    /* Step 2: end conditions */
    b[0]     = -d[0];
    b[n - 1] = -d[n - 2];
    c[0]     = 0.0;
    c[n - 1] = 0.0;
    if (n != 3) {
        c[0] = c[2] / (x[3] - x[1]) - c[1] / (x[2] - x[0]);
        c[n - 1] =
            c[n - 2] / (x[n - 1] - x[n - 3]) - c[n - 3] / (x[n - 2] - x[n - 4]);
        c[0]     = c[0] * d[0] * d[0] / (x[3] - x[0]);
        c[n - 1] = -c[n - 1] * d[n - 2] * d[n - 2] / (x[n - 1] - x[n - 4]);
    }

    /* Step 3: forward elimination */
    for (i = 1; i < n; i++) {
        h    = d[i - 1] / b[i - 1];
        b[i] = b[i] - h * d[i - 1];
        c[i] = c[i] - h * c[i - 1];
    }

    /* Step 4: back substitution */
    c[n - 1] = c[n - 1] / b[n - 1];
    for (j = 1; j <= gap; j++) {
        i    = n - 1 - j;
        c[i] = (c[i] - d[i] * c[i + 1]) / b[i];
    }

    /* Step 5: compute spline coefficients */
    b[n - 1] = (y[n - 1] - y[gap - 1]) / d[gap - 1] +
               d[gap - 1] * (c[gap - 1] + 2.0 * c[n - 1]);
    for (i = 0; i < gap; i++) {
        b[i] = (y[i + 1] - y[i]) / d[i] - d[i] * (c[i + 1] + 2.0 * c[i]);
        d[i] = (c[i + 1] - c[i]) / d[i];
        c[i] = 3.0 * c[i];
    }
    c[n - 1] = 3.0 * c[n - 1];
    d[n - 1] = d[n - 2];
}

/****************************************************************************/
/**
 * Cubic spline interpolator, evaluates the interpolation
 *
 * @brief Evaluates cubic splines
 * @param [in] u Output x value
 * @param [in] x Input x axis
 * @param [in] y Input y axis
 * @param [in] b Coefficient of spline interpolation
 * @param [in] c Coefficient of spline interpolation
 * @param [in] d Coefficient of spline interpolation
 * @param [in] n Size of x, y, b, c, and d
 * @return double Interpolated value
 *
 ****************************************************************************/
static double bo_ispline(double u, double *x, double *y, double *b, double *c,
                         double *d, int n)
{
    int    i, j, k;
    double dx;

    /* If u is ouside the x() interval take a boundary value (left or
       right) */
    if (u <= x[0]) {
        return y[0];
    }
    if (u >= x[n - 1]) {
        return y[n - 1];
    }

    /* Binary search for for i, such that x(i) <= u <= x(i+1) */
    i = 0;
    j = n;
    while (j > i + 1) {
        k = (i + j) / 2;
        if (u < x[k]) {
            j = k;
        } else {
            i = k;
        }
    }

    /* Evaluate spline interpolation */
    dx = u - x[i];
    return y[i] + dx * (b[i] + dx * (c[i] + dx * d[i]));
}

/****************************************************************************/
/**
 * Bilinear interpolator
 *
 * @brief Does bilinear interpolation
 * @param [in] x1 Input axis 1
 * @param [in] x2 Input axis 2
 * @param [in] y Input values, second axis is the slow one
 * @param [in] z1 Output value for axis 1
 * @param [in] z2 Output value for axis 2
 * @param [in] n1 Size of x1 and inner dimension of y
 * @param [in] n2 Size of x2 and outer dimension of y
 * @return double Interpolated value
 *
 ****************************************************************************/
static double bo_bilinear(double *x1, double *x2, double *y, double z1,
                          double z2, int n1, int n2)
{
    int    ii = 0, i10 = 0, i11 = 0, i20 = 0, i21 = 0;
    double x10 = 0.0, x11 = 0.0, x20 = 0.0, x21 = 0.0;
    double yA = 0.0, yB = 0.0, dx1 = 0.0, dx2 = 0.0;

    /* Check that values are within bounds */
    if (z1 >= x1[0] && z1 <= x1[n1 - 1] && z2 >= x2[0] && z2 <= x2[n2 - 1]) {
        for (ii = 0; ii < n1 - 1; ii++) {
            if (z1 >= x1[ii] && z1 <= x1[ii + 1]) {
                i10 = ii;
                i11 = ii + 1;
                x10 = x1[i10];
                x11 = x1[i11];
                dx1 = 1.0 / (x11 - x10);
                break;
            }
        }
        for (ii = 0; ii < n2 - 1; ii++) {
            if (z2 >= x2[ii] && z2 <= x2[ii + 1]) {
                i20 = ii;
                i21 = ii + 1;
                x20 = x2[i20];
                x21 = x2[i21];
                dx2 = 1.0 / (x21 - x20);
                break;
            }
        }

        yA = ((y[i21 * n1 + i10] - y[i20 * n1 + i10]) * z2 +
              y[i20 * n1 + i10] * x21 - y[i21 * n1 + i10] * x20) *
             dx2;
        yB = ((y[i21 * n1 + i11] - y[i20 * n1 + i11]) * z2 +
              y[i20 * n1 + i11] * x21 - y[i21 * n1 + i11] * x20) *
             dx2;
        return ((yB - yA) * z1 + yA * x11 - yB * x10) * dx1;

    }
    /* Out of x2 bounds */
    else if (z1 >= x1[0] && z1 <= x1[n1 - 1]) {
        for (ii = 0; ii < n1 - 1; ii++) {
            if (z1 >= x1[ii] && z1 <= x1[ii + 1]) {
                i10 = ii;
                i11 = ii + 1;
                x10 = x1[i10];
                x11 = x1[i11];
                dx1 = 1.0 / (x11 - x10);
                break;
            }
        }

        if (z2 < x2[0]) {
            i20 = 0;
        } else if (z2 > x2[n2 - 1]) {
            i20 = n2 - 1;
        }

        return ((y[i20 * n1 + i11] - y[i20 * n1 + i10]) * z1 +
                y[i20 * n1 + i10] * x11 - y[i20 * n1 + i11] * x10) *
               dx1;
    }
    /* Out of x1 bounds */
    else if (z2 >= x2[0] && z2 <= x2[n2 - 1]) {
        for (ii = 0; ii < n2 - 1; ii++) {
            if (z2 >= x2[ii] && z2 <= x2[ii + 1]) {
                i20 = ii;
                i21 = ii + 1;
                x20 = x2[i20];
                x21 = x2[i21];
                dx2 = 1.0 / (x21 - x20);
                break;
            }
        }

        if (z1 < x1[0]) {
            i10 = 0;
        } else if (z1 > x1[n1 - 1]) {
            i10 = n1 - 1;
        }

        return ((y[i21 * n1 + i10] - y[i20 * n1 + i10]) * z2 +
                y[i20 * n1 + i10] * x21 - y[i21 * n1 + i10] * x20) *
               dx2;
    }
    /* Completely out of bounds */
    else {
        if (z1 < x1[0]) {
            i10 = 0;
        } else if (z1 > x1[n1 - 1]) {
            i10 = n1 - 1;
        }

        if (z2 < x2[0]) {
            i20 = 0;
        } else if (z2 > x2[n2 - 1]) {
            i20 = n2 - 1;
        }

        return y[i20 * n1 + i10];
    }
}

/****************************************************************************/
/**
 * Linear interpolator
 *
 * @brief Does linear interpolation
 * @param [in] x Input x axis
 * @param [in] y Input y values
 * @param [in] z Output value for x axis
 * @param [in] n Size of x and y
 * @return double Interpolated value
 *
 ****************************************************************************/
static double bo_linear(double *x, double *y, double z, int n)
{
    int    ii = 0, i0 = 0, i1 = 0;
    double y0 = 0.0, y1 = 0.0, x0 = 0.0, x1 = 0.0, dx = 0.0;

    /* Out of lower bound */
    if (z <= x[0]) {
        return y[0];
    }
    /* Out of upper bound */
    else if (z >= x[n - 1]) {
        return y[n - 1];
    }
    /* Within bounds */
    else {
        for (ii = 0; ii < n - 1; ii++) {
            if (z >= x[ii] && z <= x[ii + 1]) {
                i0 = ii;
                i1 = ii + 1;
                x0 = x[i0];
                x1 = x[i1];
                y0 = y[i0];
                y1 = y[i1];
                dx = 1.0 / (x1 - x0);
                break;
            }
        }
        return ((y1 - y0) * z + y0 * x1 - y1 * x0) * dx;
    }
}

/****************************************************************************/
/**
 * Computes LTE populations of a given atom
 *
 * @brief Compute LTE populations
 * @param [in] Atom Atomic data structure
 * @param [in] N Atom density [cm^-3]
 * @param [in] ne e- density [cm^-3]
 * @param [in] T Temperature [K]
 * @return void
 *
 ****************************************************************************/
static void bo_ltepops(bo_Atom *Atom, double N, double ne, double T)
{
    int     zm, iim, dZ;
    double  C0, C1, C2, dby, S, dE, gi0, arg;
    double *debey;

    /* Allocate */
    debey = (double *)malloc(Atom->nlevel * sizeof(double));

    /* Constants */
    C0 = g_h * g_h / 2.0 / g_pi / g_me / g_kb;
    C1 = sqrt(8.0 * g_pi / g_kb) * pow(g_qel * g_qel / g_pi4eps0, 1.5);

    /*                                                  */
    /* Compute Debey correction to ionization potencial */
    /*                                                  */

    /* For each level */
    for (int ii = 1; ii < Atom->nlevel; ii++) {
        debey[ii] = 0.0;

        /* Ignore ground level */
        if (ii == 0) {
            continue;
        }

        /* Determine the charge of the shell nucleus + rest of
           electrons and the change of stages between this level
           and the ground level of the model */
        zm  = Atom->stg[ii] - 1;
        iim = Atom->stg[ii] - Atom->stg[0];

        /* Add contribution to Debey correction */
        for (int jj = 0; jj < iim; jj++) {
            debey[ii] += zm;
            zm++;
        }
    }

    /*                         */
    /* Compute LTE populations */
    /*                         */

    /* Multiplicative factor for Debey correction */
    dby = C1 * sqrt(ne / T);
    /* Non atomic part of the Saha function */
    C2 = 0.5e6 * ne * pow(C0 / T, 1.5);
    /* Cumulative factor reset */
    S = 1.0;

    /* For each level */
    for (int ii = 1; ii < Atom->nlevel; ii++) {
        /* Determine ionization potential */
        dE = (Atom->E[ii] - Atom->E[0]) * g_fktoJ;

        /* Ratio of degeneration factors */
        gi0 = Atom->g[ii] / Atom->g[0];

        /* Determine the difference in charge with the ground state */
        dZ = Atom->stg[ii] - Atom->stg[0];

        /* Calculate the argument of the exponential, ionization
           potential with Debey correction */
        arg = (debey[ii] * dby - dE) / T / g_kb;

        /* Numerator of the population solution */
        Atom->Nstarj[ii] = gi0 * exp(arg);

        /* Denominator */
        for (int jj = 0; jj < dZ; jj++) {
            Atom->Nstarj[ii] /= C2;
        }

        /* Accumulate for the normalization equation */
        S += Atom->Nstarj[ii];
    }

    /* Get the ground level population from the accumulated factor */
    Atom->Nstarj[0] = N / S;

    /* Determine the population of each level from the population of
       the ground state */
    for (int ii = 1; ii < Atom->nlevel; ii++) {
        Atom->Nstarj[ii] *= Atom->Nstarj[0];
    }

    /* Free debey vector */
    free(debey);
}

/****************************************************************************/
/**
 * Computes the Thomson scattering coefficient
 *
 * @brief Thomson scattering
 * @param [in] ne e- density [cm^-3]
 * @return double Thomson scattering coefficient
 *
 ****************************************************************************/
static double thomson(double ne) { return 6.6524587158e-25 * ne; }

/****************************************************************************/
/**
 * Computes the H- bound-free absorption and emission coefficients
 *
 * @brief H- b-f absorption and emission
 * @param [in] freq Frequency [10^5 cm^-1]
 * @param [in] nhm H- density [cm^-3]
 * @param [in] T Temperature [K]
 * @return double* H- b-f absorption and emission coefficients
 *
 ****************************************************************************/
static double *hminus_bf(double freq, double nhm, double T)
{
    static double out[2];
    double        alpha, exu, Freq;

    out[0] = 0.0;
    out[1] = 0.0;

    /*                        */
    /* Geltman (1962) table 3 */
    /*                        */
    /* Wavelength nm */
    int    nn    = 34;
    double x[34] = {0e0,     5e1,    1e2,    1.5e2,  2e2,    2.5e2,   3e2,
                    3.5e2,   4e2,    4.5e2,  5e2,    5.5e2,  6e2,     6.5e2,
                    7e2,     7.5e2,  8e2,    8.5e2,  9e2,    9.5e2,   1e3,
                    1.050e3, 1.1e3,  1.15e3, 1.2e3,  1.25e3, 1.3e3,   1.35e3,
                    1.4e3,   1.45e3, 1.5e3,  1.55e3, 1.6e3,  1.6419e3};
    /* Cross section [1e19 m^2] */
    double y[34] = {0e0,    1.5e-1, 3.3e-1, 5.7e-1, 8.5e-1, 1.17e0, 1.52e0,
                    1.89e0, 2.23e0, 2.55e0, 2.84e0, 3.11e0, 3.35e0, 3.56e0,
                    3.71e0, 3.83e0, 3.92e0, 3.95e0, 3.93e0, 3.85e0, 3.73e0,
                    3.58e0, 3.38e0, 3.14e0, 2.85e0, 2.54e0, 2.20e0, 1.83e0,
                    1.46e0, 1.06e0, 7.1e-1, 4e-1,   1.7e-1, 0e0};
    double ba[nn], ca[nn], da[nn];

    /* Convert into wavelength [nm] */
    double lamb = 1e2 / freq;

    /* If out of boundaries, no contribution */
    if (lamb < x[0] || lamb > x[nn - 1]) {
        return out;
    }

    /* Convert frequency to Hz */
    Freq = freq * 1e5 * C_C;

    /* Get cross section and exponential */
    bo_splinecoeff(x, y, ba, ca, da, nn);
    alpha = bo_ispline(lamb, x, y, ba, ca, da, nn) * 1e-17;
    exu   = exp(-g_c2 * freq * 1e4);

    /* Get coefficients */
    out[0] = nhm * (1e0 - exu) * alpha;
    out[1] = nhm * exu * alpha * g_2hc2 * Freq * Freq * Freq;

    return out;
}

/****************************************************************************/
/**
 * Computes the H- free-free absorption coefficient
 *
 * @brief H- f-f absorption
 * @param [in] freq Frequency [10^5 cm^-1]
 * @param [in] nh H ground level density [cm^-3]
 * @param [in] ne e- density [cm^-3]
 * @param [in] T Temperature [K]
 * @return double H- f-f absorption coefficient
 *
 ****************************************************************************/
static double hminus_ff(double freq, bo_Atom Atom, double ne, double T)
{
    static double out;
    int           n1  = 16;
    int           n2  = 17;
    int           n3  = 6;
    double        sig = 0.0;
    double        nh;

    out = 0.0;

    /* Convert into wavelength [nm] */
    double lamb = 1e2 / freq;

    /* Stilley & Callaway (1970) table 1 */
    /* wavelength [nm] */
    double x2[17] = {0e0,     303.8e0, 455.6e0, 506.3e0, 569.5e0, 650.9e0,
                     759.4e0, 911.3e0, 1013e0,  1139e0,  1302e0,  1519e0,
                     1823e0,  2278e0,  3038e0,  4556e0,  9113e0};

    /* If below limits, no contribution */
    if (lamb < x2[0]) {
        return out;
    }

    /* Density of ground level */
    nh = Atom.Nj[0];

    /* If within limits, use table 1 from Stilley & Callaway (1970) */
    if (lamb <= x2[n2 - 1]) {
        /*                                   */
        /* Stilley & Callaway (1970) table 1 */
        /*                                   */
        /* Theta [eV] */
        double x1[16] = {5e-1,  6e-1,  7e-1,  8e-1,  9e-1,  1e0,
                         1.1e0, 1.2e0, 1.3e0, 1.4e0, 1.5e0, 1.6e0,
                         1.7e0, 1.8e0, 1.9e0, 2.0e0};
        /*int nn = 17*16;*/ /* slow*fast */
        double y[272] = {
            0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,
            0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,
            0.0,     0.0,     3.44e-2, 4.18e-2, 4.91e-2, 5.65e-2, 6.39e-2,
            7.13e-2, 7.87e-2, 8.62e-2, 9.36e-2, 1.01e-1, 1.08e-1, 1.16e-1,
            1.23e-1, 1.30e-1, 1.38e-1, 1.45e-1, 7.80e-2, 9.41e-2, 1.10e-1,
            1.25e-1, 1.40e-1, 1.56e-1, 1.71e-1, 1.86e-1, 2.01e-1, 2.16e-1,
            2.31e-1, 2.45e-1, 2.60e-1, 2.75e-1, 2.89e-1, 3.03e-1, 9.59e-2,
            1.16e-1, 1.35e-1, 1.53e-1, 1.72e-1, 1.90e-1, 2.08e-1, 2.25e-1,
            2.43e-1, 2.61e-1, 2.78e-1, 2.96e-1, 3.13e-1, 3.30e-1, 3.47e-1,
            3.64e-1, 1.21e-1, 1.45e-1, 1.69e-1, 1.92e-1, 2.14e-1, 2.36e-1,
            2.58e-1, 2.80e-1, 3.01e-1, 3.22e-1, 3.43e-1, 3.64e-1, 3.85e-1,
            4.06e-1, 4.26e-1, 4.46e-1, 1.56e-1, 1.88e-1, 2.18e-1, 2.47e-1,
            2.76e-1, 3.03e-1, 3.31e-1, 3.57e-1, 3.84e-1, 4.10e-1, 4.36e-1,
            4.62e-1, 4.87e-1, 5.12e-1, 5.37e-1, 5.62e-1, 2.10e-1, 2.53e-1,
            2.93e-1, 3.32e-1, 3.69e-1, 4.06e-1, 4.41e-1, 4.75e-1, 5.09e-1,
            5.43e-1, 5.76e-1, 6.08e-1, 6.40e-1, 6.72e-1, 7.03e-1, 7.34e-1,
            2.98e-1, 3.59e-1, 4.16e-1, 4.70e-1, 5.22e-1, 5.73e-1, 6.21e-1,
            6.68e-1, 7.15e-1, 7.60e-1, 8.04e-1, 8.47e-1, 8.90e-1, 9.32e-1,
            9.73e-1, 1.01e0,  3.65e-1, 4.39e-1, 5.09e-1, 5.75e-1, 6.39e-1,
            7.00e-1, 7.58e-1, 8.15e-1, 8.71e-1, 9.25e-1, 9.77e-1, 1.03e0,
            1.08e0,  1.13e0,  1.18e0,  1.23e0,  4.58e-1, 5.50e-1, 6.37e-1,
            7.21e-1, 8.00e-1, 8.76e-1, 9.49e-1, 1.02e0,  1.09e0,  1.15e0,
            1.22e0,  1.28e0,  1.34e0,  1.40e0,  1.46e0,  1.52e0,  5.92e-1,
            7.11e-1, 8.24e-1, 9.31e-1, 1.03e0,  1.13e0,  1.23e0,  1.32e0,
            1.40e0,  1.49e0,  1.57e0,  1.65e0,  1.73e0,  1.80e0,  1.88e0,
            1.95e0,  7.98e-1, 9.58e-1, 1.11e0,  1.25e0,  1.39e0,  1.52e0,
            1.65e0,  1.77e0,  1.89e0,  2.00e0,  2.11e0,  2.21e0,  2.32e0,
            2.42e0,  2.51e0,  2.61e0,  1.14e0,  1.36e0,  1.58e0,  1.78e0,
            1.98e0,  2.17e0,  2.34e0,  2.52e0,  2.68e0,  2.84e0,  3.00e0,
            3.15e0,  3.29e0,  3.43e0,  3.57e0,  3.70e0,  1.77e0,  2.11e0,
            2.44e0,  2.75e0,  3.05e0,  3.34e0,  3.62e0,  3.89e0,  4.14e0,
            4.39e0,  4.63e0,  4.86e0,  5.08e0,  5.30e0,  5.51e0,  5.71e0,
            3.10e0,  3.71e0,  4.29e0,  4.84e0,  5.37e0,  5.87e0,  6.36e0,
            6.83e0,  7.28e0,  7.72e0,  8.14e0,  8.55e0,  8.95e0,  9.33e0,
            9.71e0,  1.01e1,  6.92e0,  8.27e0,  9.56e0,  1.08e1,  1.19e1,
            1.31e1,  1.42e1,  1.52e1,  1.62e1,  1.72e1,  1.82e1,  1.91e1,
            2.00e1,  2.09e1,  2.17e1,  2.25e1,  2.75e1,  3.29e1,  3.80e1,
            4.28e1,  4.75e1,  5.19e1,  5.62e1,  6.04e1,  6.45e1,  6.84e1,
            7.23e1,  7.60e1,  7.97e1,  8.32e1,  8.67e1,  9.01e1};

        /* Get temperature in electronvolts; Theta [eV] */
        double tev = g_ktoev / T;

        /* If Energy is between the table limits */
        if (tev <= x1[0] && tev <= x1[n1 - 1]) {
            sig = bo_bilinear(x1, x2, y, tev, lamb, n1, n2);
        }
        /* If energy is above table limits */
        else if (tev < x1[0]) {
            double yin[n2];
            for (int i = 0; i < n2; i++) {
                yin[n2] = y[i * n1];
            }
            sig = bo_linear(x2, yin, lamb, n2);
        }
        /* If energy is below table limits */
        else if (tev > x1[n1 - 1]) {
            double yin[n2];
            for (int i = 0; i < n2; i++) {
                yin[n2] = y[i * n1 + n1 - 1];
            }
            sig = bo_linear(x2, yin, lamb, n2);
        }

        /* Calculate absorptivity */
        /* 1d-19 = 1d-29 (m^2) * 1d12 (cm^-3**2 -> m^-3**2) *  */
        /*         1d-2 (m^-1 -> cm^-1) */
        out = nh * 1e-19 * sig * ne * g_kb * T;
    }
    /* If above the wavelength limit, use table 1 from John (1988) */
    else {
        /*                     */
        /* John (1988) table 1 */
        /*                     */
        /* Coefficient for long wavelength cross section
           calculation */
        double ya[6] = {0e0,       2.483346e3, -3.449889e3,
                        2.20004e3, -6.96271e2, 8.8283e1};
        double yb[6] = {0e0,        2.85827e2, -1.158382e3,
                        2.427719e3, -1.8414e3, 4.44517e2};
        double yc[6] = {0e0,          -2.054291e3, 8.746523e3,
                        -1.3651105e4, 8.624970e3,  -1.863864e3};
        double yd[6] = {0e0,         2.827776e3,   -1.1485632e4,
                        1.6755524e4, -1.0051530e4, 2.095288e3};
        double ye[6] = {0e0,         -1.341537e3, 5.303609e3,
                        -7.510494e3, 4.400067e3,  -9.01788e2};
        double yf[6] = {0e0,        2.08952e2,  -8.12939e2,
                        1.132738e3, -6.55020e2, 1.32985e2};
        double yg[6] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

        /* Convert lambda to microns */
        lamb *= 1e-3;
        /* And store its inverse */
        double ilamb = 1e0 / lamb;

        /* Compute the cross section Theta coefficients using the
           table */
        for (int i = 0; i < n3; i++) {
            yg[i] = lamb * lamb * ya[i] + yb[i] +
                    ilamb * (yc[i] +
                             ilamb * (yd[i] + ilamb * (ye[i] + ilamb * yf[i])));
        }

        double tev = sqrt(g_ktoev / T);
        sig        = 1.0;
        for (int i = 1; i < n3; i++) {
            sig *= tev;
            out += sig * yg[i];
        }

        /* Multiplicative constant */
        /* 1d-22 = 1d-34 cm^-1 * 1d12 cm^-3 -> m^-3 */
        out *= nh * ne * g_kb * g_ktoev * 1e-22;
    }
    return out;
}

/****************************************************************************/
/**
 * Computes hydrogen Gaunt factor for bound-free transitions
 *
 * @brief H b-f Gaunt factor
 * @param [in] freq Frequency [10^5 cm^-1]
 * @param [in] n principal quantum number
 * @param [in] Z ion charge
 * @return double H b-f Gaunt factor
 *
 ****************************************************************************/
static double ghi_bf(double freq, double n, double Z)
{
    double x = freq / g_ryd / Z / Z;
    double y = 1e0 / x / n / n;
    x        = pow(x, 1.0 / 3.0);

    return 1.0 + 0.1728 * x * (1.0 - 2.0 * y) -
           0.0496 * x * x * (1.0 - (1.0 - y) * 2.0 * y / 3.0);
}

/****************************************************************************/
/**
 * Computes the hydrogen bound-free absorption and emission
 * coefficients
 *
 * @brief H b-f absorption and emission
 * @param [in] freq Frequency [10^5 cm^-1]
 * @param [in] Atom atomic data
 * @param [in] T Temperature [K]
 * @return double H b-f absorption and emission coefficients
 *
 ****************************************************************************/
static double *hi_bf(double freq, bo_Atom Atom, double T)
{
    static double out[2];
    double        c0, nps, np, nts, nt, neff, clambda, sig, gbf;
    double        exu, gij, Freq, pE;

    out[0] = 0.0;
    out[1] = 0.0;

    /* Cross section constant part */
    /* 1d-5 = 1d-9 (real_c -> c) * 1d4 (m^2 -> cm^2) */
    c0 = 16e-5 * g_qel * g_qel * g_h / sqrt(27.0) / g_pi4eps0 / g_me / g_c /
         g_ryd / g_fktoJ;

    /* Compute frequency in Hz and energy factor */
    Freq = freq * 1e5 * C_C;
    pE   = g_2hc2 * Freq * Freq * Freq;

    /* Compute thermal exponential */
    exu = exp(-g_c2 * freq * 1e4 / T);

    /* LTE population of HII */
    nps = Atom.Nstarj[Atom.nlevel - 1];
    /* Population of HII */
    np = Atom.Nj[Atom.nlevel - 1];

    /* For each photoionization */
    for (int iphot = 0; iphot < Atom.nphot; iphot++) {
        /* If the frequency is out of the range of this transition,
           skip it */
        if (freq < Atom.p_edge[iphot] || freq > Atom.p_l0[iphot]) {
            continue;
        }

        /* Get population of lower level */
        nt = Atom.Nj[Atom.p_il[iphot]];
        /* Get LTE population of lower level */
        nts = Atom.Nstarj[Atom.p_il[iphot]];

        /* Get the efective quantum number */
        neff =
            sqrt(g_ryd / (Atom.E[Atom.nlevel - 1] - Atom.E[Atom.p_il[iphot]]));

        /* Gaunt factor */
        gbf = ghi_bf(freq, neff, 1e0);

        /* Quotient between edge and current frequencies */
        clambda = Atom.p_edge[iphot] / freq;

        /* Compute cross section */
        sig = c0 * neff * gbf * clambda * clambda * clambda;

        /* Compute statistical weight ratio */
        gij = nts * exu / nps;

        /* Add contribution to coefficients */
        out[0] += sig * (1.0 - exu) * nt;
        out[1] += gij * sig * np * pE;
    }
    return out;
}

/****************************************************************************/
/**
 * Computes hydrogen Gaunt factor for free-free transitions
 *
 * @brief H f-f Gaunt factor
 * @param [in] freq Frequency [10^5 cm^-1]
 * @param [in] Z ion charge
 * @param [in] T Temperature [K]
 * @return double H f-f Gaunt factor
 *
 ****************************************************************************/
static double ghi_ff(double freq, double Z, double T)
{
    double x = freq / g_ryd / Z / Z;
    double y = 2.0 * g_kb * T * 1e7 / g_convF / freq;
    x        = pow(x, 1.0 / 3.0);

    double out = 1.0 + 0.1728 * x * (1.0 + y) -
                 0.0496 * x * x * (1.0 + (1.0 + y) * y / 3.0);

    /* Cannot be lesser than 1 */
    if (out < 1.0)
        return 1.0;
    return out;
}

/****************************************************************************/
/**
 * Computes the hydrogen free-free absorption coefficient
 *
 * @brief H f-f absorption
 * @param [in] freq Frequency [10^5 cm^-1]
 * @param [in] Atom atomic data
 * @param [in] T Temperature [K]
 * @param [in] ne e- density [cm^-1]
 * @return double H f-f absorption coefficient
 *
 ****************************************************************************/
static double hi_ff(double freq, bo_Atom Atom, double T, double ne)
{
    double exu, freq3, c0, np, gff;

    /* Frequency related quantities */
    exu   = exp(-g_c2 * freq * 1e4 / T);
    freq3 = freq * freq * freq * g_c * g_c * g_c * 1e48;
    freq3 = 1.0 / freq3;

    /* Constant part of cross section */
    c0 = g_qel * g_qel / g_pi4eps0 / sqrt(g_me);
    /* 1d1 = 1d-9 (real_c -> c) * 1d12 (m^6 -> cm^6) *
             1d-2 (m^-1 -> cm^-1) */
    c0 = sqrt(32.0 * g_pi / 27.0 / g_kb) * c0 * c0 * c0 * 1e1 / g_h / g_c;

    /* Population of HII */
    np = Atom.Nj[Atom.nlevel - 1];

    /* Gaunt factor */
    gff = ghi_ff(freq, 1.0, T);

    /* Coefficient */
    return c0 * gff * (1.0 - exu) * freq3 * ne * np / sqrt(T);
}

/****************************************************************************/
/**
 * Computes the hydrogen Rayleigh scattering coefficient
 *
 * @brief H Rayleigh scattering
 * @param [in] freq Frequency [10^5 cm^-1]
 * @param [in] Atom atomic data
 * @param [in] T Temperature [K]
 * @return double H Rayleigh scattering coefficient
 *
 ****************************************************************************/
static double rayleigh(double freq, bo_Atom Atom, double T)
{
    double  fij = 0.0;
    double  flimit, DwT, n0, deg0, deg1, icfreq, lambda0, f;
    double  c0, CfA;
    double *freqR;

    /* Allocate memory */
    freqR = (double *)malloc(Atom.ntran * sizeof(double));

    /* Check if there are transitions in the model atom */
    if (Atom.ntran < 1)
        return 0.0;

    /* Upper limit of frequency */
    flimit = 1e-4;

    /* Compute Thermal Doppler width of reference */
    DwT = g_dw * 1e-9 / g_c;

    /* Look for the red limits of the lines of the Lyman series */
    for (int itran = 0; itran < Atom.ntran; itran++) {
        /* It not Lyman line, skip */
        if (Atom.r_il[itran] > 0) {
            continue;
        }

        /* Red side requency */
        freqR[itran] = Atom.r_nu[itran] * (1.0 - DwT * Atom.r_Dw[itran]);

        if (freqR[itran] > flimit) {
            flimit = freqR[itran];
        }
    }

    /* If the frequency is larger than the larger red wing, no
       contribution */
    if (freq >= flimit) {
        return 0.0;
    }

    /* Population of ground level */
    n0 = Atom.Nj[0];

    /* Constants */
    CfA = 2.0 * g_pi * g_qel * g_qel * 1e-9 / g_eps0 / g_me / g_c;
    c0  = g_qel * g_qel * 1e-18 / g_me / g_c / g_c / g_pi4eps0;
    c0  = 8.0 * g_pi * c0 * c0 / 3.0;

    /* Degeneration of ground level */
    deg0 = Atom.g[0];

    /* For each upper term in the Lyman series */
    for (int itran = 0; itran < Atom.ntran; itran++) {
        /* It not Lyman line, skip */
        if (Atom.r_il[itran] > 0) {
            continue;
        }

        /* Check frequency lower than red wing */
        if (freq >= freqR[itran]) {
            continue;
        }

        /* Degeneration upper level */
        deg1 = Atom.g[Atom.r_iu[itran]];

        /* Frequency quantities */
        icfreq  = Atom.r_nu[itran] / freq;
        icfreq  = 1.0 / (icfreq * icfreq - 1.0);
        lambda0 = 1e-7 / Atom.r_nu[itran];

        /* Oscillator strength */
        f = Atom.Aul[itran] * 1e8 * lambda0 * lambda0 * deg1 / deg0 / CfA;

        /* Add to Rayleigh coefficient */
        fij += f * icfreq * icfreq;
    }

    /* Free memory */
    free(freqR);

    /* Return Rayleigh cross section */
    /* 1d4 (m^2 -> cm^2) */
    return fij * c0 * 1e4 * n0;
}

/****************************************************************************/
/**
 * Computes the hydrogen + proton free-free absorption coefficient
 *
 * @brief H H+p+ absorption
 * @param [in] freq Frequency [10^5 cm^-1]
 * @param [in] Atom atomic data
 * @param [in] T Temperature [K]
 * @return double H+p+ absorption coefficient
 *
 ****************************************************************************/
static double hhp_ff(double freq, bo_Atom Atom, double T)
{
    int    n2 = 10;
    int    n1 = 15;
    double lambda, np, n0, sig;

    /* Get wavelength in nm */
    lambda = 1e2 / freq;

    /* Lambda axis */
    double x1[15] = {0.0,   384.6,  555.6,  833.3, 1111.1, 1428.6, 1666.7, 2e3,
                     2.5e3, 2857.1, 3333.3, 4e3,   5e3,    6666.7, 1e4};

    /* If above limits, no contribution */
    if (lambda < x1[0]) {
        return 0.0;
    }

    double x2[10] = {2.5e3, 3e3, 3.5e3, 4e3, 5e3, 6e3, 7e3, 8e3, 1e4, 1.2e4};
    /*int nn = 15*10; */ /* slow*fast */
    double y[150] = {
        0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.46, 0.46,
        0.42, 0.39, 0.36, 0.33, 0.32, 0.30, 0.27, 0.25, 0.70, 0.62, 0.59, 0.56,
        0.51, 0.43, 0.41, 0.39, 0.35, 0.34, 0.92, 0.86, 0.80, 0.76, 0.70, 0.64,
        0.59, 0.55, 0.48, 0.43, 1.11, 1.04, 0.96, 0.91, 0.82, 0.74, 0.68, 0.62,
        0.53, 0.46, 1.26, 1.19, 1.09, 1.02, 0.90, 0.80, 0.72, 0.66, 0.55, 0.48,
        1.37, 1.25, 1.15, 1.07, 0.93, 0.83, 0.74, 0.67, 0.56, 0.49, 1.44, 1.32,
        1.21, 1.12, 0.97, 0.84, 0.75, 0.67, 0.56, 0.48, 1.54, 1.39, 1.26, 1.15,
        0.98, 0.85, 0.75, 0.67, 0.55, 0.46, 1.58, 1.42, 1.27, 1.16, 0.98, 0.84,
        0.74, 0.66, 0.54, 0.45, 1.62, 1.43, 1.28, 1.15, 0.97, 0.83, 0.72, 0.64,
        0.52, 0.44, 1.63, 1.43, 1.27, 1.14, 0.95, 0.80, 0.70, 0.62, 0.50, 0.42,
        1.62, 1.40, 1.23, 1.10, 0.90, 0.77, 0.66, 0.59, 0.48, 0.39, 1.55, 1.33,
        1.16, 1.03, 0.84, 0.71, 0.60, 0.53, 0.43, 0.36, 1.39, 1.18, 1.02, 0.90,
        0.73, 0.60, 0.52, 0.46, 0.37, 0.31};

    /* Get population of HII */
    np = Atom.Nj[Atom.nlevel - 1];

    /* Get population of ground level */
    n0 = Atom.Nj[0];

    /* Bilinear interpolation of cross section */
    sig = bo_bilinear(x2, x1, y, T, lambda, n2, n1);

    /* Compute absorptivity
       1d-39 = 1d-23 (1d6 * 1d-29) * 1d-14 (1d6 * 1d-20)
               1d-2 (m^-1 -> cm^-1) */
    return n0 * np * sig * 1e-39;
}

/****************************************************************************/
/**
 * Fill the hard-coded H atom
 *
 * @brief Configures the H atom
 * @param [in] Atom Atomic structure to fill
 * @return void
 *
 ****************************************************************************/
static void bo_configure_h_atom(bo_Atom *Atom)
{
    int ii;

    /* Sizes */
    Atom->nlevel = 6;
    Atom->ntran  = 10;
    Atom->nphot  = 5;

    /* Level quantities */
    Atom->E      = (double *)malloc(Atom->nlevel * sizeof(double));
    Atom->E[0]   = 0.0;
    Atom->E[1]   = 0.82258211;
    Atom->E[2]   = 0.97491219;
    Atom->E[3]   = 1.02822766;
    Atom->E[4]   = 1.05290508;
    Atom->E[5]   = 1.09677617;
    Atom->g      = (double *)malloc(Atom->nlevel * sizeof(double));
    Atom->g[0]   = 2.0;
    Atom->g[1]   = 8.0;
    Atom->g[2]   = 18.0;
    Atom->g[3]   = 32.0;
    Atom->g[4]   = 50.0;
    Atom->g[5]   = 1.0;
    Atom->Nstarj = (double *)malloc(Atom->nlevel * sizeof(double));
    /* There are not NLTE populations, so just point to LTE ones */
    Atom->Nj  = Atom->Nstarj;
    Atom->stg = (int *)malloc(Atom->nlevel * sizeof(int));
    for (ii = 0; ii < Atom->nlevel; ii++) {
        Atom->stg[ii] = 1;
    }
    Atom->stg[Atom->nlevel - 1] = 2;

    /* Radiative transition quantities */
    Atom->r_il = (int *)malloc(Atom->ntran * sizeof(int));
    Atom->r_iu = (int *)malloc(Atom->ntran * sizeof(int));
    Atom->Aul  = (double *)malloc(Atom->ntran * sizeof(double));
    Atom->r_Dw = (double *)malloc(Atom->ntran * sizeof(double));
    Atom->r_nu = (double *)malloc(Atom->ntran * sizeof(double));
    /* Lyman alpha */
    ii             = 0;
    Atom->r_il[ii] = 0;
    Atom->r_iu[ii] = 1;
    Atom->Aul[ii]  = 4.6986;
    Atom->r_Dw[ii] = 600.0;
    /* Lyman beta */
    ii             = 1;
    Atom->r_il[ii] = 0;
    Atom->r_iu[ii] = 2;
    Atom->Aul[ii]  = 5.5751e-1;
    Atom->r_Dw[ii] = 250.0;
    /* Lymman gamma */
    ii             = 2;
    Atom->r_il[ii] = 0;
    Atom->r_iu[ii] = 3;
    Atom->Aul[ii]  = 1.2785e-1;
    Atom->r_Dw[ii] = 100.0;
    /* Lymman epsilon */
    ii             = 3;
    Atom->r_il[ii] = 0;
    Atom->r_iu[ii] = 4;
    Atom->Aul[ii]  = 4.125e-2;
    Atom->r_Dw[ii] = 100.0;
    /* Balmer alpha */
    ii             = 4;
    Atom->r_il[ii] = 1;
    Atom->r_iu[ii] = 2;
    Atom->Aul[ii]  = 4.4101e-1;
    Atom->r_Dw[ii] = 250.0;
    /* Balmer beta */
    ii             = 5;
    Atom->r_il[ii] = 1;
    Atom->r_iu[ii] = 3;
    Atom->Aul[ii]  = 8.4193e-2;
    Atom->r_Dw[ii] = 250.0;
    /* Balmer gamma */
    ii             = 6;
    Atom->r_il[ii] = 1;
    Atom->r_iu[ii] = 4;
    Atom->Aul[ii]  = 2.5304e-2;
    Atom->r_Dw[ii] = 250.0;
    /* Paschen alpha */
    ii             = 7;
    Atom->r_il[ii] = 2;
    Atom->r_iu[ii] = 3;
    Atom->Aul[ii]  = 8.986e-2;
    Atom->r_Dw[ii] = 30.0;
    /* Paschen beta */
    ii             = 8;
    Atom->r_il[ii] = 2;
    Atom->r_iu[ii] = 4;
    Atom->Aul[ii]  = 2.2008e-2;
    Atom->r_Dw[ii] = 30.0;
    /* Brackett alpha */
    ii             = 9;
    Atom->r_il[ii] = 3;
    Atom->r_iu[ii] = 4;
    Atom->Aul[ii]  = 2.6993e-2;
    Atom->r_Dw[ii] = 30.0;
    for (ii = 0; ii < Atom->ntran; ii++) {
        Atom->r_nu[ii] = Atom->E[Atom->r_iu[ii]] - Atom->E[Atom->r_il[ii]];
    }

    /* Photoionization quantities */
    Atom->p_il   = (int *)malloc(Atom->ntran * sizeof(int));
    Atom->p_iu   = (int *)malloc(Atom->ntran * sizeof(int));
    Atom->p_edge = (double *)malloc(Atom->ntran * sizeof(double));
    Atom->p_l0   = (double *)malloc(Atom->ntran * sizeof(double));
    /* Level 0 */
    ii             = 0;
    Atom->p_il[ii] = 0;
    Atom->p_l0[ii] = 1e2 / 22.794;
    /* Level 1 */
    ii             = 1;
    Atom->p_il[ii] = 1;
    Atom->p_l0[ii] = 1e2 / 91.176;
    /* Level 2 */
    ii             = 2;
    Atom->p_il[ii] = 2;
    Atom->p_l0[ii] = 1e2 / 205.147;
    /* Level 3 */
    ii             = 3;
    Atom->p_il[ii] = 3;
    Atom->p_l0[ii] = 1e2 / 364.705;
    /* Level 4 */
    ii             = 4;
    Atom->p_il[ii] = 4;
    Atom->p_l0[ii] = 1e2 / 569.852;
    for (ii = 0; ii < Atom->nphot; ii++) {
        Atom->p_iu[ii]   = Atom->nlevel - 1;
        Atom->p_edge[ii] = Atom->E[Atom->p_iu[ii]] - Atom->E[Atom->p_il[ii]];
    }
}

/****************************************************************************/
/**
 * Frees the memory in the atomic structure
 *
 * @brief Frees atomic structure memory
 * @param [in] Atom Atomic structure to free
 * @return void
 *
 ****************************************************************************/
static void bo_free_h_atom(bo_Atom Atom)
{
    free(Atom.E);
    free(Atom.g);
    free(Atom.stg);
    free(Atom.Nstarj);
    /* There are not NLTE populations, so no need to free */
    /*free(Atom.Nj);*/
    free(Atom.r_il);
    free(Atom.r_iu);
    free(Atom.Aul);
    free(Atom.r_Dw);
    free(Atom.r_nu);
    free(Atom.p_il);
    free(Atom.p_iu);
    free(Atom.p_edge);
    free(Atom.p_l0);
}

/****************************************************************************/
/**
 * Computes the radiative transfer coefficients for the continuum
 * due to electrons and Hydrogen atoms
 *
 * @brief Atomic H and e- continuum opacities
 * @param [in] freq Frequency [Hz]
 * @param [in] T Temperature [K]
 * @param [in] NH Hydrogen number density [cm^-3]
 * @param [in] Ne Electron number density [cm^-3]
 * @return double* Absorptivity [cm^-1],
 *                 scattering coefficient [cm^-1],
 *                 and thermal emissivity [erg cm^-3 s^-1 str^-1 Hz^-1]
 *
 ****************************************************************************/
double *backop(double freq, double T, double NH, double Ne)
{
    static double out[3];
    double *      out2;
    double        C0, phiHm, NHm;
    double        Freq;

    out[0] = 0.0;
    out[1] = 0.0;
    out[2] = 0.0;

    /* Define Hydrogen atom */
    bo_Atom bo_H;
    bo_configure_h_atom(&bo_H);

    /* Compute LTE populations */
    bo_ltepops(&bo_H, NH, Ne, T);

    /* Convert the frequency into 10^5 cm^-1 */
    Freq = 1e-5 * freq / C_C;

    /* Hminus population */
    C0    = g_h * g_h / 2.0 / g_pi / g_me / g_kb;
    phiHm = 0.25e6 * (pow(C0 / T, 1.5)) * exp(8.74980963338e3 / T);
    NHm   = Ne * NH * phiHm;

    /* Thomson s */
    out[1] += thomson(Ne);

    /* H- b-f k and e */
    out2 = hminus_bf(Freq, NHm, T);
    out[0] += out2[0];
    out[2] += out2[1];

    /* H- b-f k */
    out[0] += hminus_ff(Freq, bo_H, Ne, T);

    /* HI b-f k and e */
    out2 = hi_bf(Freq, bo_H, T);
    out[0] += out2[0];
    out[2] += out2[1];

    /* HI f-f k */
    out[0] += hi_ff(Freq, bo_H, T, Ne);

    /* HI Rayleigh */
    out[1] += rayleigh(Freq, bo_H, T);

    /* H + p+ f-f k */
    out[0] += hhp_ff(Freq, bo_H, T);

    /* Free memory of atomic structure */
    bo_free_h_atom(bo_H);

    return out;
}
