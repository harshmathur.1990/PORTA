/* PORTA module for continuum intensity and scattering polarization in the
 * stratified atmospheres with periodic horizontal boundary conditions.
 *
 * Authors:
 *    Jiří Štěpán (ASCR)
 *    Tanausú del Pino Alemán (IAC) - TdPA
 *    Ángel de Vicente (IAC)
 *
 * ----------------------------------------------------------------------------------------------
 *
 * Module header:
 *
 *  int VER              version of the module
 *  float FREQ           radiation frequency
 *  float[NY][NX]  T     2D array of temperatures at the bottom of the domain
 *                          (needed for black-body boundary illumination)
 *
 *  Grid-node storage in the PMD file:
 *
 *  float nh             total hydrogen density (neutral + protons; cm^-3)
 *  float ne             electron density
 *  float temp           kinetic temperature of the plasma
 *  t_jkq jkq            radiative tensor J^K_Q at the working frequency
 */

#include "backop.h"
#include "portal/portal.h"
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>

#define CONTPOL_VERSION 1

typedef struct {
    float nh;
    float ne;
    float temp;
    t_jkq jkq;
    t_jkq jkq_tmp;
    float op[3];
} t_node_data;

#define NODE_PMD_SIZE (3 * FLTSIZE + NJKQ * DBLSIZE)
#define N_DM          NJKQ

static double   g_frequency = 0;
static double **g_temp      = NULL;

static int allocTemp(void)
{
    int i;
    g_temp = (double **)Malloc(sizeof(double *) * PorNY());
    for (i = 0; i < PorNY(); i++)
        g_temp[i] = (double *)Malloc(DBLSIZE * PorNX());
}

static void freeTemp(void)
{
    int i;
    for (i = 0; i < PorNY(); i++)
        free(g_temp[i]);
    free(g_temp);
    g_temp = NULL;
}

#if 0
static void SKGeneral(t_node* p_node, double sf[NSTOKES], double kmtx[NKMTX], double tkq[NTKQ], const t_vec_3d *dir, int idir, int ifreq)
{
    /*
     * The total source function for Stokes parameter "i" can be written in the form
     *
     *      S_i = r * S^c_i + (1-r)*B_Planck*delta_{i0}
     *
     * where
     *
     *     r = s_c / (s_c + kappa_c)
     *
     * with s_c being the coherent-scattering opacity (Thomson+Rayleigh)
     * and kappa_c being the non-coherent opacity. The coherent part of the source function, S^c_i,
     * of the Stokes parameter i, reads
     *
     *     S^c_i = \Sum_{KQ} T^K_Q(i,dir) J^K_{-Q} (-1)^Q
     *
     * Stimulated emission is neglected.
     */

    t_node_data *p = (t_node_data*)p_node->p_data;
    static double nu = -1;
    static double wl = -1, sigma_R = -1;
    if (nu != g_frequency)
    {
      nu = g_frequency;
      wl = C_C/nu * 1e8;
      sigma_R = C_THOMSON * mat_PowN(966.0/wl,4) * (1.0 + mat_PowN(1566.0/wl,2) + mat_PowN(1480.0/wl,4));    /* hydrogen Rayleigh cross-section (only valid for wl>2000 A) */
    }
    double s_c = C_THOMSON * p->ne + sigma_R * p->nH1;
    double eta_I = s_c + p->kappa_c + VACUUM_OPACITY;
    double r = s_c/eta_I;

    ArraySet(NSTOKES, sf, 0.0);
    ArraySet(NKMTX, kmtx, 0.0);
    kmtx[ETA_I] = eta_I;
    sf[STOKES_I] = (tkq[T00I_RE]*p->jkq.jkq[J00_RE] + tkq[T20I_RE]*p->jkq.jkq[J20_RE] +
            2.0*(tkq[T21I_RE]*p->jkq.jkq[J21_RE] - tkq[T21I_IM]*p->jkq.jkq[J21_IM] +
            tkq[T22I_RE]*p->jkq.jkq[J22_RE] - tkq[T22I_IM]*p->jkq.jkq[J22_IM]));
    sf[STOKES_I] = r * sf[STOKES_I] + (1.0-r) * phy_PlanckFunction(p->T, nu);
#ifdef STOKES_Q
    sf[STOKES_Q] = (tkq[T20Q_RE]*p->jkq.jkq[J20_RE] +
            2.0*(tkq[T21Q_RE]*p->jkq.jkq[J21_RE] - tkq[T21Q_IM]*p->jkq.jkq[J21_IM] +
            tkq[T22Q_RE]*p->jkq.jkq[J22_RE] - tkq[T22Q_IM]*p->jkq.jkq[J22_IM]));
    sf[STOKES_Q] *= r;
#endif
#ifdef STOKES_U
    sf[STOKES_U] = 2.0*(tkq[T21U_RE]*p->jkq.jkq[J21_RE] - tkq[T21U_IM]*p->jkq.jkq[J21_IM] +
            tkq[T22U_RE]*p->jkq.jkq[J22_RE] - tkq[T22U_IM]*p->jkq.jkq[J22_IM]);
    sf[STOKES_U] *= r;
#endif
}
#endif

static void SKGeneral(t_node *p_node, double sf[NSTOKES], double kmtx[NKMTX],
                      double tkq[NTKQ], const t_vec_3d *dir, int idir,
                      int ifreq)
{
    /*
     * The total source function for Stokes parameter "i" can be written in the
     * form
     *
     *      S_i = r * S^c_i + (1-r)*B_Planck*delta_{i0}
     *
     * where
     *
     *     r = s_c / (s_c + kappa_c)
     *
     * with s_c being the coherent-scattering opacity (Thomson+Rayleigh)
     * and kappa_c being the non-coherent opacity. The coherent part of the
     * source function, S^c_i, of the Stokes parameter i, reads
     *
     *     S^c_i = \Sum_{KQ} T^K_Q(i,dir) J^K_{-Q} (-1)^Q
     *
     * Stimulated emission is neglected.
     */

    t_node_data *p     = (t_node_data *)p_node->p_data;
    double       eta_I = p->op[0] + p->op[1] + VACUUM_OPACITY;
    double       r     = p->op[1] / eta_I;
    ArraySet(NSTOKES, sf, 0.0);
    ArraySet(NKMTX, kmtx, 0.0);
    kmtx[ETA_I] = eta_I;
    sf[STOKES_I] =
        (tkq[T00I_RE] * p->jkq.jkq[J00_RE] + tkq[T20I_RE] * p->jkq.jkq[J20_RE] +
         2.0 * (tkq[T21I_RE] * p->jkq.jkq[J21_RE] +
                tkq[T21I_IM] * p->jkq.jkq[J21_IM] +
                tkq[T22I_RE] * p->jkq.jkq[J22_RE] +
                tkq[T22I_IM] * p->jkq.jkq[J22_IM]));
    sf[STOKES_I] =
        r * sf[STOKES_I] + (1.0 - r) * phy_PlanckFunction(p->temp, g_frequency);
#ifdef STOKES_Q
    sf[STOKES_Q] = (tkq[T20Q_RE] * p->jkq.jkq[J20_RE] +
                    2.0 * (tkq[T21Q_RE] * p->jkq.jkq[J21_RE] +
                           tkq[T21Q_IM] * p->jkq.jkq[J21_IM] +
                           tkq[T22Q_RE] * p->jkq.jkq[J22_RE] +
                           tkq[T22Q_IM] * p->jkq.jkq[J22_IM]));
    sf[STOKES_Q] *= r;
#endif
#ifdef STOKES_U
    sf[STOKES_U] =
        2.0 *
        (tkq[T21U_RE] * p->jkq.jkq[J21_RE] + tkq[T21U_IM] * p->jkq.jkq[J21_IM] +
         tkq[T22U_RE] * p->jkq.jkq[J22_RE] + tkq[T22U_IM] * p->jkq.jkq[J22_IM]);
    sf[STOKES_U] *= r;
#endif
}

static void F_ResetRadiation(t_node *p_node)
{
    ArraySet(NJKQ, ((t_node_data *)p_node->p_data)->jkq_tmp.jkq, 0.0);
}

static void F_Init_p_data(t_node *p_node)
{
    p_node->p_data = (void *)Malloc(sizeof(t_node_data));
    F_ResetRadiation(p_node);
}

static void F_Free_p_data(void *p_data) { free((t_node_data *)p_data); }

static int F_GetNodeDataSize(void) { return NODE_PMD_SIZE; }

/*****************************************************************************/
/**
 * @brief Encode all the data pertaining to a node, in order to dump it into a
 * file
 *
 * The data is read from p_data, after casting it to t_node_data
 * The data is copied to buf contiguously, following the H5T_COMPOUND type, as
 *   defined in function F_IO_WriteHeader_h5
 *
 * @param buf [out] serialized buffer corresponding to data in p_data
 * @param p_data [in] Physical data of a node
 *
 * @return 1 if successful ; 0 if not
 *****************************************************************************/
static int F_EncodeNodeData_h5(void *buf, void *p_data)
{
    t_node_data *pdata = (t_node_data *)p_data;
    int          i;
    char *       bp;

    if (!p_data) {
        Error(E_ERROR, POR_AT, "unallocated node data");
        return 0;
    }

    bp = buf;
    memcpy((void *)bp, (void *)&pdata->nh, sizeof(float));
    bp += sizeof(float);
    memcpy((void *)bp, (void *)&pdata->ne, sizeof(float));
    bp += sizeof(float);
    memcpy((void *)bp, (void *)&pdata->temp, sizeof(float));
    bp += sizeof(float);
    for (i = 0; i < NJKQ; i++) {
        memcpy((void *)bp, (void *)&pdata->jkq.jkq[i], sizeof(double));
        bp += sizeof(double);
    }

    return 1;
}

static void *F_EncodeNodeData(void *p_data)
{
    int          i;
    t_node_data *p      = (t_node_data *)p_data;
    void *       buffer = Malloc(F_GetNodeDataSize());
    t_pstream    f      = pst_NewStream(F_GetNodeDataSize(), buffer, 1);
    pst_Write(&f, PSTREAM_FLOAT, &p->nh);
    pst_Write(&f, PSTREAM_FLOAT, &p->ne);
    pst_Write(&f, PSTREAM_FLOAT, &p->temp);
    for (i = 0; i < NJKQ; i++)
        pst_Write(&f, PSTREAM_DOUBLE, &p->jkq.jkq[i]);
    return buffer;
}

/*****************************************************************************/
/**
 * @brief Decode all the data pertaining to a node, as read from a file, in
 * order to update the node data
 *
 * @return void
 *****************************************************************************/
static void F_DecodeNodeData_h5(void *buffer, t_node *p_node)
{
    int          i;
    t_node_data *pdata;
    char *       bp;
    double *     ops;

    if (!buffer) {
        Error(E_ERROR, POR_AT, "uninitialized buffer");
        return;
    }

    bp = buffer;

    if (!p_node) {
        Error(E_ERROR, POR_AT, "unallocated node");
        return;
    }

    if (!p_node->p_data)
        F_Init_p_data(p_node);
    pdata = p_node->p_data;

    memcpy((void *)&pdata->nh, (void *)bp, sizeof(float));
    bp += sizeof(float);
    memcpy((void *)&pdata->ne, (void *)bp, sizeof(float));
    bp += sizeof(float);
    memcpy((void *)&pdata->temp, (void *)bp, sizeof(float));
    bp += sizeof(float);
    for (i = 0; i < NJKQ; i++) {
        memcpy((void *)&pdata->jkq.jkq[i], (void *)bp, sizeof(double));
        bp += sizeof(double);
    }
    ops = backop(g_frequency, pdata->temp, pdata->nh, pdata->ne);
    for (i = 0; i < 3; i++)
        pdata->op[i] = (float)ops[i];
}

static void F_DecodeNodeData(void *buffer, t_node *p_node)
{
    int          i;
    t_node_data *p;
    t_pstream    f = pst_NewStream(F_GetNodeDataSize(), buffer, 0);
    double *     ops;
    if (!p_node->p_data)
        F_Init_p_data(p_node);
    p = (t_node_data *)p_node->p_data;
    pst_Read(&f, PSTREAM_FLOAT, &p->nh);
    pst_Read(&f, PSTREAM_FLOAT, &p->ne);
    pst_Read(&f, PSTREAM_FLOAT, &p->temp);
    for (i = 0; i < NJKQ; i++)
        pst_Read(&f, PSTREAM_DOUBLE, &p->jkq.jkq[i]);
    ops = backop(g_frequency, p->temp, p->nh, p->ne);
    for (i = 0; i < 3; i++)
        p->op[i] = (float)ops[i];
}

static void F_ExternalIllum(const t_vec_3d r, const t_vec_3d dir, int ifreq,
                            double i[NSTOKES])
{
    double temp;
    ArraySet(NSTOKES, i, 0.0);
    if (dir.z > 0.0) {
        temp        = mat_PlaneInterpolation(PorNX(), PorNY(), PorGetXCoords(),
                                      PorGetYCoords(), PorDomainSizeX(),
                                      PorDomainSizeY(), g_temp, 1, r.x, r.y);
        i[STOKES_I] = phy_PlanckFunction(temp, g_frequency);
    }
}

static void F_SKGeneralDirection(t_node *p_node, double sf[NSTOKES],
                                 double kmtx[NKMTX], const t_vec_3d dir,
                                 int ifreq)
{
    double tkq[NTKQ];
    double theta, chi;
    VecUnit2Ang(dir, &theta, &chi);
    dir_CalcTKQ(sin(theta), cos(theta), sin(chi), cos(chi), tkq);
    SKGeneral(p_node, sf, kmtx, tkq, &dir, IDIR_GENDIR, ifreq);
}

static void F_SK(t_node *p_node, double sf[NSTOKES], double kmtx[NKMTX],
                 int idir, int ifreq)
{
    double   tkq[NTKQ];
    t_vec_3d dir;
    PorGetTKQ(idir, tkq);
    PorGetIDir(idir, &dir);
    SKGeneral(p_node, sf, kmtx, tkq, &dir, idir, ifreq);
}

static void F_AddFS(t_node *p_node, int iray, int ifreq)
{
    t_node_data *p = (t_node_data *)p_node->p_data;
    double       tkq[NTKQ], di, dq, du;

    PorGetTKQ(iray, tkq);
    di = p_node->fsdata.i[STOKES_I] * PorAngQuadrature(iray);
#ifdef STOKES_Q
    dq = p_node->fsdata.i[STOKES_Q] * PorAngQuadrature(iray);
#endif
#ifdef STOKES_U
    du = p_node->fsdata.i[STOKES_U] * PorAngQuadrature(iray);
#endif
    p->jkq_tmp.jkq[J00_RE] += di;
    p->jkq_tmp.jkq[J20_RE] += tkq[T20I_RE] * di
#ifdef STOKES_Q
                              + tkq[T20Q_RE] * dq
#endif
        ;
    p->jkq_tmp.jkq[J21_RE] += tkq[T21I_RE] * di
#ifdef STOKES_Q
                              + tkq[T21Q_RE] * dq
#endif
#ifdef STOKES_U
                              + tkq[T21U_RE] * du
#endif
        ;
    p->jkq_tmp.jkq[J21_IM] += tkq[T21I_IM] * di
#ifdef STOKES_Q
                              + tkq[T21Q_IM] * dq
#endif
#ifdef STOKES_U
                              + tkq[T21U_IM] * du
#endif
        ;
    p->jkq_tmp.jkq[J22_RE] += tkq[T22I_RE] * di
#ifdef STOKES_Q
                              + tkq[T22Q_RE] * dq
#endif
#ifdef STOKES_U
                              + tkq[T22U_RE] * du
#endif
        ;
    p->jkq_tmp.jkq[J22_IM] += tkq[T22I_IM] * di
#ifdef STOKES_Q
                              + tkq[T22Q_IM] * dq
#endif
#ifdef STOKES_U
                              + tkq[T22U_IM] * du
#endif
        ;
}

static void F_GetESE(t_node *p_node, double **ese, double *f) {}

static double F_SolveESE(t_node *p_node)
{
    t_node_data *p  = (t_node_data *)p_node->p_data;
    double       rc = fabs((p->jkq_tmp.jkq[J00_RE] - p->jkq.jkq[J00_RE]) /
                     p->jkq_tmp.jkq[J00_RE]);
    Memcpy(&p->jkq, &p->jkq_tmp, sizeof(t_jkq));
    return rc;
}

static int F_GetNDM(void) { return N_DM; }

static int *F_GetPopsPositions(void)
{
    int *pops, i;
    pops = (int *)Malloc(N_DM * INTSIZE);
    for (i = 0; i < N_DM; i++)
        pops[i] = 0;
    pops[J00_RE] = 1;
    return pops;
}

static void F_GetDM(t_node *p_node, double *data)
{
    Memcpy(data, ((t_node_data *)p_node->p_data)->jkq.jkq, NJKQ * DBLSIZE);
}

static void F_SetDM(t_node *p_node, double *data)
{
    Memcpy(((t_node_data *)p_node->p_data)->jkq.jkq, data, NJKQ * DBLSIZE);
}

static double F_GetTemperature(t_node *p_node)
{
    return ((t_node_data *)p_node->p_data)->temp;
}

static t_vec_3d F_GetMagneticVector(t_node *p_node)
{
    return (t_vec_3d){0, 0, 0};
}

static double *F_GetRadiation(t_node *p_node, int *n_data)
{
    t_node_data *p = (t_node_data *)p_node->p_data;
    double *     r = ArrayNew(NJKQ);
    Memcpy(r, p->jkq.jkq, NJKQ * DBLSIZE);
    *n_data = NJKQ;
    return r;
}

static void F_SetRadiation(t_node *p_node, double *data)
{
    t_node_data *p = (t_node_data *)p_node->p_data;
    Memcpy(p->jkq.jkq, data, NJKQ * DBLSIZE);
}

/*****************************************************************************/
/**
 * @brief Write Model header data
 *
 * Write Model Header data to a HDF5 file (/Module group attributes, plus
 * /Module/temp_bc dataset).
 *
 * Also the g_data dataset is created (though empty)
 *
 * @param f_id [in] HDF5 file identifier
 * @return hdf5 identifier of g_data dataset ; -1 if not
 *
 * @todo check return status all all calls to h5t_insert, etc.
 *****************************************************************************/
static hid_t F_IO_WriteHeader_h5(hid_t f_id)
{
    int     i, j, offset;
    int     ver = CONTPOL_VERSION;
    hid_t   group_id, memtype, jkq_type;
    hid_t   space, dset;
    hsize_t locdims[1];
    herr_t  status;
    int     g_nx = PorNX(), g_ny = PorNY(), g_nz = PorNZ();
    hsize_t dims[3];

    group_id =
        H5Gcreate(f_id, "/Module", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5LTset_attribute_int(f_id, "/Module", "MOD_VERSION", &ver, 1);
    H5LTset_attribute_double(f_id, "/Module", "FREQUENCY", &g_frequency, 1);

    double *mdata_hdf;
    mdata_hdf = (double *)malloc(g_nx * g_ny * sizeof(*mdata_hdf));
    for (j = 0; j < g_ny; j++) {
        for (i = 0; i < g_nx; i++) {
            mdata_hdf[j * g_nx + i] = g_temp[j][i];
        }
    }
    hsize_t mdims[2] = {g_ny, g_nx};
    H5LTmake_dataset(f_id, "/Module/temp_bc", 2, mdims, H5T_NATIVE_DOUBLE,
                     mdata_hdf);
    free(mdata_hdf);

    /* Create the array components for g_data */
    locdims[0] = NJKQ;
    jkq_type   = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, locdims);

    /* Create the memtype */
    offset  = 0;
    memtype = H5Tcreate(H5T_COMPOUND, NODE_PMD_SIZE);
    status  = H5Tinsert(memtype, "Nh", offset, H5T_NATIVE_FLOAT);
    offset += FLTSIZE;
    status = H5Tinsert(memtype, "Ne", offset, H5T_NATIVE_FLOAT);
    offset += FLTSIZE;
    status = H5Tinsert(memtype, "T", offset, H5T_NATIVE_FLOAT);
    offset += FLTSIZE;
    status = H5Tinsert(memtype, "jkq", offset, jkq_type);

    /* Pack datatype, so there will be no padding between fields */
    H5Tpack(memtype);

    /* Create /Module/g_data dataset. Empty, to be filled by SaveNodesSlave_h5
     * function */
    dims[0] = g_nz;
    dims[1] = g_ny;
    dims[2] = g_nx;
    space   = H5Screate_simple(3, dims, NULL);
    dset    = H5Dcreate(f_id, "/Module/g_data", memtype, space, H5P_DEFAULT,
                     H5P_DEFAULT, H5P_DEFAULT);

    status = H5Sclose(space);
    status = H5Tclose(memtype);
    status = H5Tclose(jkq_type);

    if ((status = H5Gclose(group_id)) < 0) {
        Error(E_ERROR, POR_AT, "cannot close group /Module \n");
        return -1;
    }

    return dset;
}

int F_IO_WriteHeader(FILE *f)
{
    int i, written = 0;
    int ver = CONTPOL_VERSION;
    written += fwrite(&ver, INTSIZE, 1, f);
    written += fwrite(&g_frequency, DBLSIZE, 1, f);
    for (i = 0; i < PorNY(); i++) {
        written += fwrite(g_temp[i], DBLSIZE, PorNX(), f);
    }
    if (written == 2 + PorNX() * PorNY()) {
        return 1;
    } else {
        Error(E_ERROR, POR_AT, "Cannot write the module header (%d vs %d).",
              written, 2 + PorNX() * PorNY());
        return -1;
    }
}

static int LoadHeader(FILE *f)
{
    int i, cnt = 0;
    int ver;
    cnt += fread(&ver, INTSIZE, 1, f);
    if (ver != CONTPOL_VERSION) {
        Error(E_ERROR, POR_AT,
              "incompatible version %d of the module (supported version: %d)",
              ver, CONTPOL_VERSION);
        return 0;
    }
    cnt += fread(&g_frequency, DBLSIZE, 1, f);
    for (i = 0; i < PorNY(); i++) {
        cnt += fread(g_temp[i], DBLSIZE, PorNX(), f);
    }
    return (2 + PorNX() * PorNY() == cnt);
}

/*****************************************************************************/
/**
 * @brief Load Model header data
 *
 * Load Model Header data (/Module group attributes, plus /Module/temp_bc
 * dataset)
 *
 * Called collectively (all MPI_COMM_WORLD processes)
 *
 * @param file_id [in] HDF5 file identifier
 * @return 1 if successful ; 0 if not
 *****************************************************************************/
static int LoadHeader_h5(hid_t file_id)
{
    int i, j;
    int ver;
    int g_nx = PorNX(), g_ny = PorNY();

    /*stk_Add(1, POR_AT, "contpol: loading the model header");*/
    H5LTget_attribute_int(file_id, "/Module", "MOD_VERSION", &ver);
    if (ver != CONTPOL_VERSION) {
        Error(E_ERROR, POR_AT,
              "incompatible version %d of the format (supported version: %d)",
              ver, CONTPOL_VERSION);
        return 0;
    }

    H5LTget_attribute_double(file_id, "/Module", "FREQUENCY", &g_frequency);

    double *mdata_hdf;
    mdata_hdf = (double *)malloc(g_nx * g_ny * sizeof(*mdata_hdf));
    if (H5LTread_dataset_double(file_id, "/Module/temp_bc", mdata_hdf) < 0) {
        Error(E_ERROR, POR_AT, "cannot read from file");
        return 0;
    }
    for (j = 0; j < g_ny; j++) {
        for (i = 0; i < g_nx; i++) {
            g_temp[j][i] = mdata_hdf[j * g_nx + i];
        }
    }
    free(mdata_hdf);

    return 1;
}

static void F_SyncGlobals(void) {}

static void F_ESEBegins(void) {}

static void F_ESEEnds(void) {}

int InitModule(t_afunc *p_afunc, FILE *file, int *node_pmd_size)
{
    if (!file)
        Error(E_ERROR, POR_AT,
              "you have to load a PMD file containing the model");
    allocTemp();
    if (!LoadHeader(file))
        Error(E_ERROR, POR_AT, "cannot load the module header");
    if (!p_afunc)
        Error(E_ERROR, POR_AT, "global variables not initialized");
    if (!PorMeshInitialized())
        Error(E_ERROR, POR_AT, "grid mesh not initialized");

    PorSetFreqs(1, &g_frequency);

    p_afunc->F_Init_p_data        = F_Init_p_data;
    p_afunc->F_Free_p_data        = F_Free_p_data;
    p_afunc->F_EncodeNodeData     = F_EncodeNodeData;
    p_afunc->F_EncodeNodeData_h5  = F_EncodeNodeData_h5;
    p_afunc->F_DecodeNodeData     = F_DecodeNodeData;
    p_afunc->F_DecodeNodeData_h5  = F_DecodeNodeData_h5;
    p_afunc->F_GetNodeDataSize    = F_GetNodeDataSize;
    p_afunc->F_ExternalIllum      = F_ExternalIllum;
    p_afunc->F_SKGeneralDirection = F_SKGeneralDirection;
    p_afunc->F_SK                 = F_SK;
    p_afunc->F_AddFS              = F_AddFS;
    p_afunc->F_GetESE             = F_GetESE;
    p_afunc->F_SolveESE           = F_SolveESE;
    p_afunc->F_GetNDM             = F_GetNDM;
    p_afunc->F_GetPopsPositions   = F_GetPopsPositions;
    p_afunc->F_GetDM              = F_GetDM;
    p_afunc->F_SetDM              = F_SetDM;
    p_afunc->F_GetTemperature     = F_GetTemperature;
    p_afunc->F_GetMagneticVector  = F_GetMagneticVector;
    p_afunc->F_GetRadiation       = F_GetRadiation;
    p_afunc->F_SetRadiation       = F_SetRadiation;
    p_afunc->F_ResetRadiation     = F_ResetRadiation;
    p_afunc->F_IO_WriteHeader     = F_IO_WriteHeader;
    p_afunc->F_IO_WriteHeader_h5  = F_IO_WriteHeader_h5;
    p_afunc->F_SyncGlobals        = F_SyncGlobals;
    p_afunc->F_ESEBegins          = F_ESEBegins;
    p_afunc->F_ESEEnds            = F_ESEEnds;

    *node_pmd_size = F_GetNodeDataSize();
    return 1;
}

/*****************************************************************************/
/**
 * @brief The exported module initialization function
 *
 * Initializes module global variables and functions provided by this
 * (contpol) module
 *
 * @param p_afunc [out] Pointer to struct holding external module's functions
 * (all these functions start with F_)
 * @param file_id [in] HDF5 file identifier
 * @param [out] node_pmd_size Pointer to size (in bytes) of each node
 *
 * @return 1 if successful ; 0 if not
 * @todo this is almost identical to the binary format one. See if we can
 * combine them
 *****************************************************************************/
int InitModule_h5(t_afunc *p_afunc, hid_t file_id, int *node_pmd_size)
{
    int i;

    if (!file_id) {
        Error(E_ERROR, POR_AT,
              "Sorry, you need to load a PMD file containing the model "
              "atmosphere.");
        return 0;
    }

    allocTemp();

    /* load model header */
    if (!LoadHeader_h5(file_id)) {
        Error(E_ERROR, POR_AT, "cannot load the module header");
        return 0;
    }

    if (!p_afunc) {
        Error(E_ERROR, POR_AT, "global variables not initialized");
        return 0;
    }

    if (!PorMeshInitialized()) {
        Error(E_ERROR, POR_AT, "grid mesh not initialized");
        return 0;
    }

    PorSetFreqs(1, &g_frequency);

    p_afunc->F_Init_p_data        = F_Init_p_data;
    p_afunc->F_Free_p_data        = F_Free_p_data;
    p_afunc->F_EncodeNodeData     = F_EncodeNodeData;
    p_afunc->F_EncodeNodeData_h5  = F_EncodeNodeData_h5;
    p_afunc->F_DecodeNodeData     = F_DecodeNodeData;
    p_afunc->F_DecodeNodeData_h5  = F_DecodeNodeData_h5;
    p_afunc->F_GetNodeDataSize    = F_GetNodeDataSize;
    p_afunc->F_ExternalIllum      = F_ExternalIllum;
    p_afunc->F_SKGeneralDirection = F_SKGeneralDirection;
    p_afunc->F_SK                 = F_SK;
    p_afunc->F_AddFS              = F_AddFS;
    p_afunc->F_GetESE             = F_GetESE;
    p_afunc->F_SolveESE           = F_SolveESE;
    p_afunc->F_GetNDM             = F_GetNDM;
    p_afunc->F_GetPopsPositions   = F_GetPopsPositions;
    p_afunc->F_GetDM              = F_GetDM;
    p_afunc->F_SetDM              = F_SetDM;
    p_afunc->F_GetTemperature     = F_GetTemperature;
    p_afunc->F_GetMagneticVector  = F_GetMagneticVector;
    p_afunc->F_GetRadiation       = F_GetRadiation;
    p_afunc->F_SetRadiation       = F_SetRadiation;
    p_afunc->F_ResetRadiation     = F_ResetRadiation;
    p_afunc->F_IO_WriteHeader     = F_IO_WriteHeader;
    p_afunc->F_IO_WriteHeader_h5  = F_IO_WriteHeader_h5;
    p_afunc->F_SyncGlobals        = F_SyncGlobals;
    p_afunc->F_ESEBegins          = F_ESEBegins;
    p_afunc->F_ESEEnds            = F_ESEEnds;

    *node_pmd_size = F_GetNodeDataSize();
    return 1;
}

int CloseModule(void)
{
    freeTemp();
    return 1;
}
