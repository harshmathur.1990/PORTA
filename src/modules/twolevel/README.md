PORTA twolevel module         {#mainpage}
======================

The twolevel module is described in section 
[twolevel](https://polmag.gitlab.io/PORTA/Modules/index.html#twolevel) of the PORTA User Manual.

@author Jiří Štěpán, stepan@asu.cas.cz
@author Ángel de Vicente, angel.de.vicente@iac.es
@author Tanausú del Pino Alemán, tanausu@iac.es

