/*****************************************************************************/
/**
 * @file twolevel.c
 * @brief 2-level module for PORTA
 *
 * @author Jiří Štěpán, stepan@asu.cas.cz
 * @author Ángel de Vicente, angel.de.vicente@iac.es
 * @author Tanausú del Pino Alemán, tanausu@iac.es
 *
 * 2-level atom, IQU scattering polarization including lower level
 * polarization, stimulated emission is neglected
 *
 * Magnetic field is uniform in the atmosphere
 *
 * density matrix storage:
 *
 * complex array for each level organized as follows:
 *
 * rho00, rho1-1, rho 10, rho11, rho2-2, rho2-1, rho20, rho21, rho22, etc.
 *
 * the REAL representation is (negative Q's excluded):
 * REAL(rho00, rho10, rho11, rho20, rho21, rho22, ...), IMAGINARY(rho00, rho20,
 * rho21, rho22, ...)  ... Q=0 multipoles are taken as complex
 *
 * Number of the independent real components is equal to the total number of
 * complex components but we also assume the imaginary parts of the Q=0
 * elements, hence the total number of real elements (g_nreal) equals 2 * the
 * number of Q>=0 elements
 *
 * Module header data format:
 *
 *      int version_number;    // == 1
 *      double amass;          // atomic mass (in the atomic units)
 *      double Aul;            // Einstein coefficient for spontaneous emission
 *      double Eul;            // energy of the upper level
 *      int Jl2;               // 2*angular momentum of the lower level
 *      int Ju2;               // 2*angular momentum of the upper level
 *      double gl;             // lower-level Lande factor
 *      double gu;             // upper-level Lande factor
 *      double temp_ref;       // reference temperature
 *      double temp[ny][nx];   // matrix of ground (iz=0) for Planckian boundary
 *                                 condition
 *****************************************************************************/

#include "hdf5.h"
#include "hdf5_hl.h"
#include "portal/portal.h"
#include "portal/process.h"
#include "portal/pstream.h"
#include <float.h>
#include <math.h>
#include <memory.h>
#include <stdlib.h>

#ifdef SMODE_FULL

// HANLE_ZEEMAN_FS defined by default. Can be deactivated via ./configure
// --no_hz #define HANLE_ZEEMAN_FS /**< @brief Activates Hanle-Zeeman in formal
// solution*/

#if defined(HANLE_ZEEMAN_FS)
/* Local aliases for general components of the T^K_Q(i) tensors */
#define ZNTKQ    19
#define ZT00I_RE 0
#define ZT20I_RE 1
#define ZT21I_RE 2
#define ZT21I_IM 3
#define ZT22I_RE 4
#define ZT22I_IM 5
#define ZT20Q_RE 6
#define ZT21Q_RE 7
#define ZT21Q_IM 8
#define ZT22Q_RE 9
#define ZT22Q_IM 10
#define ZT20U_RE 11
#define ZT21U_RE 12
#define ZT21U_IM 13
#define ZT22U_RE 14
#define ZT22U_IM 15
#define ZT10V_RE 16
#define ZT11V_RE 17
#define ZT11V_IM 18
#endif

#define TWOLEVELVERSION 2 /**< @brief version of the module */

#define JL2D 0         /**< @brief 2-times the lower level angular momentum */
#define JU2D 2         /**< @brief 2-times the upper level angular momentum */
static int JL2 = JL2D; /**< @brief lower level ang. mom. multiplied by 2 */
static int JU2 = JU2D; /**< @brief upper level ang. mom. multiplied by 2 */

static double G_GL = 0.0; /**< @brief lande factor of the lower level */
static double G_GU = 0.0; /**< @brief lande factor of the upper level */

#define LANDE_L (G_GL) /**< @brief Lande factor of the lower level */
#define LANDE_U (G_GU) /**< @brief Lande factor of the upper level */
#define D2_L(pdata)                                                            \
    ((pdata)->delta2 *                                                         \
     g_A_UL) /**< @brief el. depolarizing rate of the lower level */
#define D2_U(pdata)                                                            \
    ((pdata)->delta2 *                                                         \
     g_A_UL) /**< @brief el. depolarizing rate of the upper level */

#define MAX_K_RANK (MAX2(JL2D, JU2D))

static double g_E_UL = 0; /**< @brief energy of the upper level */

#define NU   (g_E_UL / C_H) /**< @brief frequency of the line */
#define A_UL (g_A_UL)       /**< @brief einstein coef., academic line case */
double A_MASS = 1.00794;    /**< @brief atomic mass [atomic mass units] */

#define FREQ_LINE NU

static double g_TEMP_REF = 5.0e3; /**< @brief reference temperature (for
                                     estimating typical doppler width) */
#define DOPPLER0                                                               \
    (NU / C_C *                                                                \
     sqrt(2.0 * C_KB * g_TEMP_REF /                                            \
          (A_MASS * C_AU_MASS))) /**< @brief default Doppler line width */
#define REF_DOPPLER_WIDTH                                                      \
    (3.0 * DOPPLER0) /**< @brief reference line Doppler width [Hz] */
#define MAX_LINE_WIDTH                                                         \
    10.0 /**< @brief max. line frequency in the Doppler width units */

#define BFAC                                                                   \
    (2.0 * C_H * NU * NU * NU / C_C /                                          \
     C_C) /**< @brief 2*h*nu^3/c^2  ... the emission factor in the Planck f.   \
           */
#define B_UL (A_UL / BFAC)
#define B_LU (B_UL * (JU2 + 1) / (JL2 + 1))

#define N_LINE_FREQ                                                            \
    (101) /**< @brief number of line frequencies (1 => coherent scattering) */

#define MAX_NXY 1024 /**< @brief maximum number of nodes per x/y axis */

#if ((N_LINE_FREQ > MAXNFREQ) || (N_LINE_FREQ < 1))
#error "invalid number of frequencies"
#endif

static int g_globals_synced =
    0; /**< @brief set to 1 after the grid is loaded (no need to synchronize
          anything after every iteration) */

#define ZNAM(i) (abs(i) % 2 ? -1.0 : 1.0)

#define L_LIMIT                                                                \
    ((JL2D + 1) *                                                              \
     (JL2D + 1)) /**< @brief number of lower-level density matrix elements */
#define U_LIMIT                                                                \
    ((JU2D + 1) *                                                              \
     (JU2D + 1)) /**< @brief number of upper-level density matrix elements */

/* #define NO_CONT_OPAC   /\**< @brief if defined, no continuum opacity and
 * emissivity is considered *\/ */

/* node atomic data: */
typedef struct t_pmddata {
    double eps;     /**< @brief collisional destruction probability */
    double temp;    /**< @brief temperature */
    double density; /**< @brief atomic volum density [cm-3] */
    double B[3];    /**< @brief magnetic field [G] */
    double V[3];    /**< @brief velocity [cm/s] */
    double dm[2 * (L_LIMIT + U_LIMIT)];
    double jkq[NJKQ];
    double a_voigt; /**< @brief Voigt-profile a-parameter */
    double delta2;  /**< @brief collisional depolarization delta = D^2/A_UL */
    double c_opac;  /**< @brief continuum opacity */
    double c_emis;  /**< @brief continuum emisivity */
} t_pmddata;

#define NODE_PMD_SIZE (sizeof(double) * (13 + 2 * (L_LIMIT + U_LIMIT) + NJKQ))

/* node atomic data: */
typedef struct t_rosdata {
    double    eps;           /**< @brief collisional destruction probability */
    double    temp;          /**< @brief temperature */
    double    density;       /**< @brief atomic volum density [cm-3] */
    t_complex dm_l[L_LIMIT]; /**< @brief lower-level density matrix complex
                                elements (# of element: upper limit) */
    t_complex dm_u[U_LIMIT]; /**< @brief upper-level density matrix complex
                                elements (# of element: upper limit) */
    double Bx, By, Bz;       /**< @brief magnetic field [G] */
    double vx, vy, vz;       /**< @brief velocity [cm/s] */
    t_jkq  jkq;     /**< @brief profile-averaged radiation field tensor */
    double a_voigt; /**< @brief Voigt-profile a-parameter */
    double delta2;  /**< @brief collisional depolarization delta = D^2/A_UL */
    double c_opac;  /**< @brief continuum opacity */
    double c_emis;  /**< @brief continuum emisivity */
                    /* temporary data (not stored in the PMD file): */
    double nu_d;    /**< @brief profile Doppler width */
    double pnorm;   /**< @brief inverse of the profiles' frequency integrals */
    double psi_eta; /**< @brief integral over frequency of (Lambda^* phi /
                       eta_I) */
    int idir_last;  /**< @brief last direction index of ray used as an indicator
                       of  wether it is necessary to calculate profile
                       normalization */
/* Hanle Zeeman rotation flag */
#if defined(HANLE_ZEEMAN_FS)
    int rotated; /**< @brief Flag to indicate if the rhoKQ tensors have been
                    rotated */
#endif
} t_nodedata;

static double g_A_UL =
    0.0; /**< @brief Einstein coefficient of spont. emission */

static int g_nlower =
    0; /**< @brief number of COMPLEX dm elements of the lower level */
static int g_nupper =
    0; /**< @brief number of COMPLEX dm elements of the upper level */
static int g_nreal = 0; /**< @brief total number of REAL dm matrix elements (q<0
                           multipoles removed) */
static t_complex ** g_ese_c    = NULL;
static t_complex *  g_rhs_c    = NULL;
static double **    g_ese_r    = NULL;
static double *     g_rhs_r    = NULL;
static t_complex ***g_kernel_l = NULL; /**< @brief array of (Jl2+1) matrices of
                                          the magn, kernel of the lower level */
static t_complex ***g_kernel_u = NULL;
static double       g_L_factor =
    0; /**< @brief factor by which the L_coef differs from psi-operaotr diagonal
          (= emission factor) except local density */

static double g_max_d = -1e200,
              g_min_d = 1e200; /**< @brief limit of doppler widths */
static double g_max_a = -1e200,
              g_min_a = 1e200; /**< @brief limit of a-parameter */
static int g_widths_set =
    0; /**< @brief set to non-zero after loading of the model when max_d,
          g_min_d, g_max_a, g_min_a are calculated */
static double **g_temp   = NULL; /**< @brief ground plane temperature */
static double   C_H4PIS3 = C_H * 1.732050807568877 / 4. / M_PI; /**< @brief
                                                        Constant factor in
                                                        radiative transfer
                                                        coefficients */

static double
    g_Prof[2]; /**< @brief line profile (the real and the complex components) */

/* forward declarations */
static void F_ExternalIllum(const t_vec_3d r, const t_vec_3d dir, int ifreq,
                            double i[NSTOKES]);
static void F_SK(t_node *p_node, double sf[NSTOKES], double kmtx[NKMTX],
                 int iray, int ifreq);
static void F_AddFS(t_node *p_node, int iray, int ifreq);
static void F_ResetRadiation(t_node *p_node);
static void UpdateWidths(t_nodedata *pdata);
static void DM2BFrame(t_nodedata *pdata);
static void DM2ZFrame(t_nodedata *pdata);
static void rotate_dm(t_node *p_node, double alphaB, double betaB,
                      double gammaB);

/*******************************************************************************/
static void allocTemp(void)
{
    int i;
    g_temp = (double **)Malloc(PorNY() * sizeof(double *));
    for (i = 0; i < PorNY(); i++)
        g_temp[i] = (double *)Malloc(DBLSIZE * PorNX());
}

static void freeTemp(void)
{
    int i;
    for (i = 0; i < PorNY(); i++)
        free(g_temp[i]);
    free(g_temp);
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static void MakeFrequencies()
{
    double *freqs;
    freqs =
        vgt_SuggestLineFrequencies(N_LINE_FREQ, FREQ_LINE, REF_DOPPLER_WIDTH,
                                   MAX_LINE_WIDTH * REF_DOPPLER_WIDTH);
#if (N_LINE_FREQ < 2)
#error "at least 2 line frequencies required"
#endif
    PorSetFreqs(N_LINE_FREQ, freqs);
    free(freqs);
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static int NReals(int J2)
{
    return 2 + 3 * J2 +
           J2 * J2; /* we count both the real and imaginary parts */
}

/*****************************************************************************/
/**
 * @brief Calculate number of complex dm elements of level with J2/2 angular
 * momentum
 *
 * @param [in] J2
 * @return Number of complex dm elements
 *
 * @todo Add descriptions
 *****************************************************************************/
static int NLevelDMElements(int J2) { return (J2 + 1) * (J2 + 1); }

/*****************************************************************************/
/**
 * @brief Gives dm index in the global indexing of the density matrix (for ESE
 * indexing)
 *
 * @param [in] level
 * @param [in] K
 * @param [in] Q
 *****************************************************************************/
static int GetDMIndex(int level, int K, int Q)
{
    int i, k, q;
    int J2;
    if (level == 0) {
        J2 = JL2;
        i  = 0;
    } else {
        J2 = JU2;
        i  = NLevelDMElements(JL2);
    }

    for (k = 0; k <= J2; k++) {
        for (q = -k; q <= k; q++) {
            if (k == K && q == Q)
                return i;
            i++;
        }
    }
    IERR; /* we should never be here */
    return -1;
}

/*****************************************************************************/
/**
 * @brief Gives index of a density matrix element RELATIVE TO BEGINNING OF A
 * GIVEN LEVEL
 *
 * @param [in] K
 * @param [in] Q
 * @return Index of the density matrix element
 *
 * @todo Add descriptions to params
 *****************************************************************************/
static int GetDmIndexLevel(int K, int Q) { return K * K + Q + K; }

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static void F_Init_p_data(t_node *p_node)
{
    if (!p_node)
        IERR;
    if (!p_node->p_data) {
        p_node->p_data = (void *)Malloc(sizeof(t_nodedata));
/* Hanle Zeeman rotation flag */
#if defined(HANLE_ZEEMAN_FS)
        t_nodedata *p;
        p          = (t_nodedata *)p_node->p_data;
        p->rotated = 0;
#endif
        F_ResetRadiation(p_node);
    }
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static void F_Free_p_data(void *p_data)
{
    t_nodedata *p = (t_nodedata *)p_data;
    free(p);
}

/*****************************************************************************/
/**
 * @brief Encode all the data pertaining to a node, in order to dump it into a
 * file
 *
 * PMD-file storage order:
 *
 *      epsilon
 *      temperature
 *      density
 *      bx, by, bz
 *      vx, vy, vz
 *      dm_l
 *      dm_u
 *      jkq
 *      a_voigt
 *      delta2
 *      c_opac
 *      c_emis
 *
 * @return void
 *****************************************************************************/
static void *F_EncodeNodeData(void *p_data)
{
    t_nodedata *pdata   = (t_nodedata *)p_data;
    int         written = 0, i;
    t_pstream   f;
    char *      buffer = (char *)Malloc(NODE_PMD_SIZE);

    if (!p_data) {
        Error(E_ERROR, POR_AT, "unallocated node data");
        return NULL;
    }

/* If HANLE_ZEEMAN, check if we rotated rhoKQ */
#if defined(HANLE_ZEEMAN_FS)
    /* If rhoKQ are rotated in this node */
    if (pdata->rotated) {
        t_vec_3d Bdir_norm, Bvec;
        t_node * p_node;
        double   Btheta, Bchi;

        p_node->p_data = p_data;

        /* Magnetic field components */
        Bvec.x = pdata->Bx;
        Bvec.y = pdata->By;
        Bvec.z = pdata->Bz;

        /* Angle of B in the laboratory frame */
        Bdir_norm = Bvec;
        VEC_NORMALIZE_3D(Bdir_norm);
        VecUnit2Ang(Bdir_norm, &Btheta, &Bchi);

        /* Rotate back */
        rotate_dm(p_node, -Bchi, -Btheta, 0.0);

        /* De-flag */
        pdata->rotated = 0;
    }
#endif

    f = pst_NewStream(NODE_PMD_SIZE, (char *)buffer, 1);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->eps);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->temp);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->density);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->Bx);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->By);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->Bz);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->vx);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->vy);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->vz);
    DM2ZFrame(pdata); /* dm -> global ref. frame */
    for (i = 0; i < L_LIMIT; i++) {
        written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->dm_l[i].re);
        written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->dm_l[i].im);
    }
    for (i = 0; i < U_LIMIT; i++) {
        written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->dm_u[i].re);
        written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->dm_u[i].im);
    }
    DM2BFrame(pdata); /* dm back to B-frame */
    for (i = 0; i < NJKQ; i++) {
        written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->jkq.jkq[i]);
    }
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->a_voigt);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->delta2);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->c_opac);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->c_emis);
    if (written == (13 + 2 * (L_LIMIT + U_LIMIT) + NJKQ)) {
        return buffer;
    } else {
        IERR;
        free(buffer);
        return NULL;
    }
}

/*****************************************************************************/
/**
 * @brief Encode all the data pertaining to a node, in order to dump it into a
 * file
 *
 * The data is read from p_data, after casting it to t_nodedata
 * The data is copied to buf contiguously, following the H5T_COMPOUND type, as
 *  defined in function F_IO_WriteHeader_h5
 *
 * @param buf [out] serialized buffer corresponding to data in p_data
 * @param p_data [in] Physical data of a node
 *
 * @return 1 if successful ; 0 if not
 *****************************************************************************/
static int F_EncodeNodeData_h5(void *buf, void *p_data)
{
    t_nodedata *pdata = (t_nodedata *)p_data;
    int         i;
    char *      bp;

    if (!p_data) {
        Error(E_ERROR, POR_AT, "unallocated node data");
        return 0;
    }

/* If HANLE_ZEEMAN, check if we rotated rhoKQ */
#if defined(HANLE_ZEEMAN_FS)
    /* If rhoKQ are rotated in this node */
    if (pdata->rotated) {
        t_vec_3d Bdir_norm, Bvec;
        t_node * p_node;
        double   Btheta, Bchi;

        p_node->p_data = p_data;

        /* Magnetic field components */
        Bvec.x = pdata->Bx;
        Bvec.y = pdata->By;
        Bvec.z = pdata->Bz;

        /* Angle of B in the laboratory frame */
        Bdir_norm = Bvec;
        VEC_NORMALIZE_3D(Bdir_norm);
        VecUnit2Ang(Bdir_norm, &Btheta, &Bchi);

        /* Rotate back */
        rotate_dm(p_node, -Bchi, -Btheta, 0.0);

        /* De-flag */
        pdata->rotated = 0;
    }
#endif

    bp = buf;
    memcpy((void *)bp, (void *)&pdata->eps, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->temp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->density, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->Bx, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->By, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->Bz, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->vx, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->vy, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->vz, sizeof(double));
    bp += sizeof(double);

    DM2ZFrame(pdata); /* dm -> global ref. frame */
    for (i = 0; i < L_LIMIT; i++) {
        memcpy((void *)bp, (void *)&pdata->dm_l[i].re, sizeof(double));
        bp += sizeof(double);
        memcpy((void *)bp, (void *)&pdata->dm_l[i].im, sizeof(double));
        bp += sizeof(double);
    }

    for (i = 0; i < U_LIMIT; i++) {
        memcpy((void *)bp, (void *)&pdata->dm_u[i].re, sizeof(double));
        bp += sizeof(double);
        memcpy((void *)bp, (void *)&pdata->dm_u[i].im, sizeof(double));
        bp += sizeof(double);
    }
    DM2BFrame(pdata); /* dm back to B-frame */

    for (i = 0; i < NJKQ; i++) {
        memcpy((void *)bp, (void *)&pdata->jkq.jkq[i], sizeof(double));
        bp += sizeof(double);
    }

    memcpy((void *)bp, (void *)&pdata->a_voigt, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->delta2, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->c_opac, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->c_emis, sizeof(double));
    bp += sizeof(double);

    return 1;
}

/*****************************************************************************/
/**
 * @brief Decode all the data pertaining to a node, as read from a file, in
 * order to update the node data
 *
 * @return void
 *****************************************************************************/
static void F_DecodeNodeData(void *buffer, t_node *p_node)
{
    t_nodedata *pdata;
    t_pstream   f;
    int         read = 0, i;

    if (!buffer) {
        Error(E_ERROR, POR_AT, "uninitialized buffer");
        return;
    }
    if (!p_node) {
        Error(E_ERROR, POR_AT, "unallocated node");
        return;
    }
    f = pst_NewStream(NODE_PMD_SIZE, (char *)buffer, 0);
    F_Init_p_data(p_node);
    pdata = p_node->p_data;
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->eps);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->temp);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->density);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->Bx);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->By);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->Bz);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->vx);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->vy);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->vz);
    for (i = 0; i < L_LIMIT; i++) {
        read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->dm_l[i].re);
        read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->dm_l[i].im);
    }
    for (i = 0; i < U_LIMIT; i++) {
        read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->dm_u[i].re);
        read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->dm_u[i].im);
    }
    DM2BFrame(pdata);
    for (i = 0; i < NJKQ; i++) {
        read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->jkq.jkq[i]);
    }
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->a_voigt);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->delta2);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->c_opac);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->c_emis);
    UpdateWidths(pdata);

    if (read != (13 + 2 * (L_LIMIT + U_LIMIT) + NJKQ))
        Error(E_ERROR, POR_AT, "cannot read buffer");
}

/*****************************************************************************/
/**
 * @brief Decode all the data pertaining to a node, as read from a file, in
 * order to update the node data
 *
 * @return void
 *****************************************************************************/
static void F_DecodeNodeData_h5(void *buffer, t_node *p_node)
{
    int         i;
    t_nodedata *pdata;
    char *      bp;

    if (!buffer) {
        Error(E_ERROR, POR_AT, "uninitialized buffer");
        return;
    }

    bp = buffer;

    if (!p_node) {
        Error(E_ERROR, POR_AT, "unallocated node");
        return;
    }

    F_Init_p_data(p_node);
    pdata = p_node->p_data;

    memcpy((void *)&pdata->eps, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->temp, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->density, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->Bx, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->By, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->Bz, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->vx, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->vy, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->vz, (void *)bp, sizeof(double));
    bp += sizeof(double);

    for (i = 0; i < L_LIMIT; i++) {
        memcpy((void *)&pdata->dm_l[i].re, (void *)bp, sizeof(double));
        bp += sizeof(double);
        memcpy((void *)&pdata->dm_l[i].im, (void *)bp, sizeof(double));
        bp += sizeof(double);
    }

    for (i = 0; i < U_LIMIT; i++) {
        memcpy((void *)&pdata->dm_u[i].re, (void *)bp, sizeof(double));
        bp += sizeof(double);
        memcpy((void *)&pdata->dm_u[i].im, (void *)bp, sizeof(double));
        bp += sizeof(double);
    }
    DM2BFrame(pdata); /* dm back to B-frame */

    for (i = 0; i < NJKQ; i++) {
        memcpy((void *)&pdata->jkq.jkq[i], (void *)bp, sizeof(double));
        bp += sizeof(double);
    }

    memcpy((void *)&pdata->a_voigt, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->delta2, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->c_opac, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->c_emis, (void *)bp, sizeof(double));
    bp += sizeof(double);

    UpdateWidths(pdata);
}

/*****************************************************************************/
/**
 * @brief External illumination function: planckian for all upward directions
 *and zero otherwise
 *
 * @todo Add better description
 *
 *****************************************************************************/
static void F_ExternalIllum(const t_vec_3d r, const t_vec_3d dir, int ifreq,
                            double i[NSTOKES])
{
    ArraySet(NSTOKES, i, 0.0);
    if (dir.z > 0.0)
        i[STOKES_I] = phy_PlanckFunction(
            mat_PlaneInterpolation(PorNX(), PorNY(), PorGetXCoords(),
                                   PorGetYCoords(), PorDomainSizeX(),
                                   PorDomainSizeY(), g_temp, 1, r.x, r.y),
            PorFreqs()[ifreq]);
}

/*****************************************************************************/
/**
 * @brief MakeProfilesPool
 *
 * @todo Add descriptions
 *****************************************************************************/
static void MakeProfilesPool(void)
{
    stk_Add(0, POR_AT,
            "initializing profiles pool: mind=%e, maxd=%e, mina=%e, maxa=%e\n",
            g_min_d, g_max_d, g_min_a, g_max_a);
    if (g_max_a == 0)
        g_max_a = 1e-10;
    vgt_PoolClear();
    vgt_PoolBuild(0, N_LINE_FREQ, PorFreqs(), NU, g_min_d, g_max_d, g_min_a,
                  g_max_a, 1);
}

/*****************************************************************************/
/**
 * Computes Zeeman shift for a transition component given the magnetic
 * number and indexes of the levels involved and the magnetic field
 *
 * @brief Shift of the Zeeman component of the line due to magnetic
 *        field B
 * @param [in] gl Lower level Lande factor
 * @param [in] gu Upper level Lande factor
 * @param [in] Ml2 Twice the lower level magnetic number
 * @param [in] Mu2 Twice the upper level magnetic number
 * @param [in] B Magnetic field strength [G]
 * @return double Frequency shift
 *
 *****************************************************************************/
double ZeemanProfileShift(double gl, double gu, int Ml2, int Mu2, double B)
{
    return C_BOHRMAG * B * 0.5 * (gu * ((double)Mu2) - gl * ((double)Ml2)) /
           C_H;
}

/****************************************************************************/
/**
 * Compute the normalization of the Voigt profile and the value at a given
 * requested frequency for a given node and shift.
 *
 * @brief Calculate normalized exact line profiles in a given node in the
 *        laboratory frame
 * @param [in] p_node Node data
 * @param [in] shift Frequency shift
 * @param [in] ifreq Frequency index
 * @return void
 *
 ****************************************************************************/
static void tl_VoigtProfileNuNorm(t_node *p_node, double shift, int ifreq)
{
    t_nodedata *  p = (t_nodedata *)p_node->p_data;
    t_complex     pr;
    double        nu = FREQ_LINE + shift;
    double        W;
    const double *freqs;

    /* Get frequency array */
    freqs = PorFreqs();

    /* Initialize */
    p->pnorm = 0e0;

    /* First point */

    /* Weight */
    W = freqs[1] - freqs[0];

    /* Compute profile */
    pr = vgt_VoigtProfileNu(nu, freqs[0], p->nu_d, p->a_voigt);

    /* Add to norm */
    p->pnorm += pr.re * W;

    /* Is the requested? */
    if (ifreq == 0) {
        g_Prof[0] = pr.re;
        g_Prof[1] = pr.im;
    }

    /* For each frequency in the transition which is not boundary, compute
       profile and add to norm */
    for (int jfreq = 1; jfreq < N_LINE_FREQ - 1; jfreq++) {
        W = freqs[jfreq + 1] - freqs[jfreq - 1];

        /* Compute profile */
        pr = vgt_VoigtProfileNu(nu, freqs[jfreq], p->nu_d, p->a_voigt);

        /* Add to norm */
        p->pnorm += pr.re * W;

        /* Is the requested? */
        if (ifreq == jfreq) {
            g_Prof[0] = pr.re;
            g_Prof[1] = pr.im;
        }
    }

    /* Last point */

    /* Weight */
    W = freqs[N_LINE_FREQ - 2] - freqs[N_LINE_FREQ - 1];

    /* Compute profile */
    pr = vgt_VoigtProfileNu(nu, freqs[N_LINE_FREQ - 1], p->nu_d, p->a_voigt);

    /* Add to norm */
    p->pnorm += pr.re * W;

    /* Is the requested? */
    if (ifreq == N_LINE_FREQ - 1) {
        g_Prof[0] = pr.re;
        g_Prof[1] = pr.im;
    }

    /* Reverse the norm constant */
    p->pnorm = 2e0 / p->pnorm;
}

/*****************************************************************************/
/**
 * @brief Calculate the line profiles in a given node in the laboratory frame
 *
 * @note When called for the first time, it initializes the pool of profiles
 *
 * @param dir unit vector in the direction of radiation propagation
 * @param comoving: co-moving reference frame (no Doppler shifts)
 * @param idir if >=0 then it indicates and index of the ray direction
 *
 * @return void
 *****************************************************************************/
static void CalculateNodeProfiles(t_node *p_node, int ifreq,
                                  const t_vec_3d *dir, int idir, int comoving)
{
    t_nodedata *p     = (t_nodedata *)p_node->p_data;
    double      shift = 0.0;

#if defined(EXACT_PROFILES_NONORM)

    t_complex pr;
    shift = 0.0;
    if (dir)
        shift = (dir->x * p->vx + dir->y * p->vy + dir->z * p->vz) / C_C *
                FREQ_LINE;
    pr = vgt_VoigtProfileNu(FREQ_LINE + shift, PorFreqs()[ifreq], p->nu_d,
                            p->a_voigt);
    g_Prof[0] = pr.re;
    g_Prof[1] = pr.im;
    return;

#elif defined(EXACT_PROFILES)

    shift = 0.0;
    if (dir)
        shift = (dir->x * p->vx + dir->y * p->vy + dir->z * p->vz) / C_C *
                FREQ_LINE;

    if (dir >= 0 && p->idir_last >= 0 && p->idir_last == idir) {
        t_complex pr;
        pr = vgt_VoigtProfileNu(FREQ_LINE + shift, PorFreqs()[ifreq], p->nu_d,
                                p->a_voigt);
        g_Prof[0] = pr.re;
        g_Prof[1] = pr.im;
    }

    else {
        tl_VoigtProfileNuNorm(p_node, shift, ifreq);
        p->idir_last = idir;
    }
    g_Prof[0] *= p->pnorm;
    g_Prof[1] *= p->pnorm;

    return;

#else
    double wt, nu_d;
    if (!g_widths_set) {
        MakeProfilesPool();
        g_widths_set = 1;
    }

    wt   = sqrt(2.0 * C_KB * p->temp / (A_MASS * C_AU_MASS));
    nu_d = NU * wt / C_C;
    if (dir) {
        shift = (dir->x * p->vx + dir->y * p->vy + dir->z * p->vz) / C_C *
                FREQ_LINE;
    }
    if (comoving ||
        shift ==
            0.0) /* no frequency shift, no need to renormalize the profile */
    {
        /* the first global line frequency is N_CONT_FREQ */
        vgt_PoolGetProfile(0, ifreq, nu_d, p->a_voigt, 0.0, g_Prof, 0);
    } else /* existing frequency shift: need of recalculation of the profiles
              normalization? */
    {
        /* We know the normalization factor from the previous steps;
             idir == IDIR_GENDIR (-1) or a quadrature (see def.h) */
        if (p->idir_last == idir) {
            vgt_PoolGetProfile(0, ifreq, nu_d, p->a_voigt, shift, g_Prof, 0);
            g_Prof[0] *= p->pnorm;
            g_Prof[1] *= p->pnorm;
        }
        /* We need to calculate normalization of the profiles
           idir = IDIR_UNDEF (-2) (see def.h) */
        else {
            p->pnorm = vgt_PoolGetProfile(0, ifreq, nu_d, p->a_voigt, shift,
                                          g_Prof, 1);
            g_Prof[0] *= p->pnorm;
            g_Prof[1] *= p->pnorm;
            p->idir_last = idir;
        }
    }
#endif
}

#if defined(HANLE_ZEEMAN_FS)

/****************************************************************************/
/**
 * Compute the geometrical tensors with the general expresions (the
 * routine in directions.c assumes gamma=0)
 *
 * @brief Common function to compute source functions given the
 *        geometry tensors
 * @param [in] sin of polar angle of the propagation with respect to
 *             the reference axis
 * @param [in] cos of polar angle of the propagation with respect to
 *             the reference axis
 * @param [in] sin of azimuth angle of the propagation with respect to
 *             the reference axis
 * @param [in] cos of azimuth angle of the propagation with respect to
 *             the reference axis
 * @param [in] sin of angle of the polarization reference with respect
 *             to the meridian
 * @param [in] cos of angle of the polarization reference with respect
 *             to the meridian
 * @return void
 *
 ****************************************************************************/
void CalcTKQ(double st, double ct, double sa, double ca, double sy, double cy,
             double tkq[ZNTKQ])
{
    double c2a = ca * ca - sa * sa, s2a = 2.0 * sa * ca;
    double c2y = cy * cy - sy * sy, s2y = 2.0 * cy * sy;
    double cctp1 = (1.0 + ct * ct);
    double cct = ct * ct, sst = st * st;
    double x, y, z;

    /* T00(I).re */
    tkq[ZT00I_RE] = 1.0;
    /* T20(I).re */
    tkq[ZT20I_RE] = 1.060660171779821287 * cct - 3.535533905932737622e-1;
    /* T21(I).re & T21(I).im */
    x             = -8.660254037844386468e-1 * ct * st;
    tkq[ZT21I_RE] = x * ca;
    tkq[ZT21I_IM] = x * sa;
    /* T22(I).re & T22(I).im */
    x             = 4.330127018922193234e-1 * sst;
    tkq[ZT22I_RE] = x * c2a;
    tkq[ZT22I_IM] = x * s2a;

    /* T20(Q).re */
    tkq[ZT20Q_RE] = -1.060660171779821287 * sst * c2y;
    /* T21(Q).re & T21(Q).im */
    x             = -8.660254037844386468e-1 * st;
    y             = c2y * ct;
    tkq[ZT21Q_RE] = x * (y * ca - s2y * sa);
    tkq[ZT21Q_IM] = x * (y * sa + s2y * ca);
    /* T22(Q).re & T22(Q).im */
    x             = -4.330127018922193234e-1;
    y             = c2y * cctp1;
    z             = 2.0 * s2y * ct;
    tkq[ZT22Q_RE] = x * (y * c2a - z * s2a);
    tkq[ZT22Q_IM] = x * (y * s2a + z * c2a);

    /* T20(U).re */
    tkq[ZT20U_RE] = 1.060660171779821287 * sst * s2y;
    /* T21(U).re, T21(U).im */
    x             = 8.660254037844386468e-1 * st;
    y             = s2y * ct;
    tkq[ZT21U_RE] = x * (y * ca + c2y * sa);
    tkq[ZT21U_IM] = x * (y * sa - c2y * ca);
    /* T22(U).re, T22(U).im */
    x             = 4.330127018922193234e-1;
    y             = s2y * cctp1;
    z             = 2.0 * c2y * ct;
    tkq[ZT22U_RE] = x * (y * c2a + z * s2a);
    tkq[ZT22U_IM] = x * (y * s2a - z * c2a);

    /* T10(V).re */
    tkq[ZT10V_RE] = 1.224744871391589049 * ct;
    /* T11(V).re & T11(V).im */
    x             = -8.660254037844386468e-1 * st;
    tkq[ZT11V_RE] = x * ca;
    tkq[ZT11V_IM] = x * sa;
}

/****************************************************************************/
/**
 * Function that rotates the rhoKQ tensors to the magnetic field
 * reference frame, given the node and the rotation angles
 *
 * @brief Common function to compute source functions given the
 *        geometry tensors
 * @param [in] p_node Node data
 * @param [in] alpha angle of rotation
 * @param [in] beta angle of rotation
 * @param [in] gamma angle of rotation
 * @return void
 *
 ****************************************************************************/
static void rotate_dm(t_node *p_node, double alphaB, double betaB,
                      double gammaB)
{
    t_nodedata *p;
    int         nQ, Ri;
    t_complex **DKQ[3];
    t_complex   rhoKQa[3];
    t_complex   rhoKQ[5];

    /* Get pointer to node data */
    p = (t_nodedata *)p_node->p_data;

    /* Get rotation matrices */
    DKQ[1] = dir_GetDKRot(1, -alphaB, -betaB, -gammaB);
    DKQ[2] = dir_GetDKRot(2, -alphaB, -betaB, -gammaB);

    /* Lower level */

    /* For every multipole up to K=2 */
    for (int K = 1; K <= MIN2(2, JL2); K++) {
        /* Number of Q components for given K */
        nQ = 2 * K + 1;

        /* Translate to complex */

        /* Q == 0 */
        Ri          = GetDmIndexLevel(K, 0);
        rhoKQ[K].re = p->dm_l[Ri].re;
        rhoKQ[K].im = 0.0;

        /* Signs for Q negative */
        double sgre = 1.0;
        double sgim = -1.0;

        /* For each Q>0 */
        for (int Q = 1; Q <= K; Q++) {
            /* Get Index */
            Ri = GetDmIndexLevel(K, Q);

            /* Invert signs */
            sgre = -sgre;
            sgim = -sgim;

            /* Q negative */
            rhoKQ[K - Q].re = p->dm_l[Ri].re * sgre;
            rhoKQ[K - Q].im = p->dm_l[Ri].im * sgim;

            /* Q positive */
            rhoKQ[K + Q].re = p->dm_l[Ri].re;
            rhoKQ[K + Q].im = p->dm_l[Ri].im;
        }

        t_complex a;

        /* For each non-negative Q */
        for (int Q = 0; Q <= K; Q++) {
            rhoKQa[Q].re = 0.0;
            rhoKQa[Q].im = 0.0;
            for (int Q1 = 0; Q1 < nQ; Q1++) {
                CLX_MUL(a, DKQ[K][K + Q][Q1], rhoKQ[Q1]);
                rhoKQa[Q].re += a.re;
                rhoKQa[Q].im += a.im;
            }
        }

        /* Put it back in node structure */

        /* Q == 0 */
        Ri             = GetDmIndexLevel(K, 0);
        p->dm_l[Ri].re = rhoKQa[0].re;

        /* For each Q>0 */
        for (int Q = 1; Q <= K; Q++) {
            Ri = GetDmIndexLevel(K, Q);

            /* Q positive */
            p->dm_l[Ri].re = rhoKQa[Q].re;
            p->dm_l[Ri].im = rhoKQa[Q].im;
        }
    } /* For every K>0 */

    /* Upper level */

    /* For every multipole up to K=2 */
    for (int K = 1; K <= MIN2(2, JU2); K++) {
        /* Number of Q components for given K */
        nQ = 2 * K + 1;

        /* Translate to complex */

        /* Q == 0 */
        Ri          = GetDmIndexLevel(K, 0);
        rhoKQ[K].re = p->dm_u[Ri].re;
        rhoKQ[K].im = 0.0;

        /* Signs for Q negative */
        double sgre = 1.0;
        double sgim = -1.0;

        /* For each Q>0 */
        for (int Q = 1; Q <= K; Q++) {
            /* Get Index */
            Ri = GetDmIndexLevel(K, Q);

            /* Invert signs */
            sgre = -sgre;
            sgim = -sgim;

            /* Q negative */
            rhoKQ[K - Q].re = p->dm_u[Ri].re * sgre;
            rhoKQ[K - Q].im = p->dm_u[Ri].im * sgim;

            /* Q positive */
            rhoKQ[K + Q].re = p->dm_u[Ri].re;
            rhoKQ[K + Q].im = p->dm_u[Ri].im;
        }

        t_complex a;

        /* For each non-negative Q */
        for (int Q = 0; Q <= K; Q++) {
            rhoKQa[Q].re = 0.0;
            rhoKQa[Q].im = 0.0;
            for (int Q1 = 0; Q1 < nQ; Q1++) {
                CLX_MUL(a, DKQ[K][K + Q][Q1], rhoKQ[Q1]);
                rhoKQa[Q].re += a.re;
                rhoKQa[Q].im += a.im;
            }
        }

        /* Put it back in node structure */

        /* Q == 0 */
        Ri             = GetDmIndexLevel(K, 0);
        p->dm_u[Ri].re = rhoKQa[0].re;

        /* For each Q>0 */
        for (int Q = 1; Q <= K; Q++) {
            Ri = GetDmIndexLevel(K, Q);

            /* Q positive */
            p->dm_u[Ri].re = rhoKQa[Q].re;
            p->dm_u[Ri].im = rhoKQa[Q].im;
        }
    } /* For every K>0 */

    /* Free DKQ */
    MatrixFreeC(3, 3, DKQ[1]);
    MatrixFreeC(5, 5, DKQ[2]);
}

/****************************************************************************/
/**
 * Function that actually computes the source function and absorption
 * matrix given the node, the direction, and the frequency in the
 * Hanle-Zeeman regime
 *
 * @brief Common function to compute source functions given the
 *        geometry tensors
 * @param [in] p_node Node data
 * @param [in] tkq Geometrical tensors
 * @param [in] dir Director cosines of the propagation direction
 * @param [in] idir Last direction for which the absorption profiles
 *                  were computed
 * @param [in] ifreq Frequency index for which the radiation transfer
 *                   coefficients are requested
 * @param [out] sf Source function
 * @param [out] kmtx Absorption matrix
 * @return void
 *
 ****************************************************************************/
static void SKGeneral_HZ(t_node *p_node, double sf[NSTOKES], double kmtx[NKMTX],
                         double tkq[ZNTKQ], const t_vec_3d *dir, int idir,
                         int ifreq, double modB)
{
    t_nodedata *  p;
    int           sign, signla1, signua1, signla3, signua3, signua2, signla2;
    int           Ri, nQ, Mu2, Ml2;
    int           RR, II;
    double        Vdir, dnuB, nu0;
    double        eta[4], eps[4], rho[3], etal[4], rhol[3], etau[4], rhou[3];
    double        Kr21, RPre, RPim;
    double        J31, J31K, J32, J33, J3, fl, fu;
    static double fact;
    t_complex     Phi;
    const double *freqs;

    /* Initializations */
    p     = (t_nodedata *)p_node->p_data;
    freqs = PorFreqs();
    for (int i = 0; i < 4; i++) {
        eta[i] = 0.0;
        eps[i] = 0.0;
    }
    for (int i = 0; i < 3; i++) {
        rho[i] = 0.0;
    }
    sf[STOKES_I] = sf[STOKES_Q] = sf[STOKES_U] = sf[STOKES_V] = kmtx[ETA_I] =
        kmtx[ETA_Q] = kmtx[ETA_U] = kmtx[ETA_V] = kmtx[RHO_Q] = kmtx[RHO_U] =
            kmtx[RHO_V]                                       = 0.0;

    /* Continuum */
    eta[0] = p->c_opac;
    eps[0] = p->c_emis;

    /* Get Doppler shift */
    Vdir = (dir->x * p->vx + dir->y * p->vy + dir->z * p->vz) / C_C;

    /* Common factors */

    /* Get sign for emission (and partial sign for absorption) */
    signla3 = 2 + JL2;
    signua3 = 2 + JU2;

    /* Get multiplicative factor */
    fact = C_H4PIS3 * FREQ_LINE * p->density;

    /* Non-magnetic frequency with Doppler shift */
    nu0 = FREQ_LINE * (1.0 + Vdir);

    /* Initialize */
    for (int i = 0; i < 4; i++) {
        etal[i] = 0.0;
        etau[i] = 0.0;
    }
    for (int i = 0; i < 3; i++) {
        rhol[i] = 0.0;
        rhou[i] = 0.0;
    }

    /* For each Mu */
    Mu2 = -JU2 - 2;
    for (int iMu = 0; iMu < JU2 + 1; iMu++) {
        double gu = G_GU;

        Mu2 += 2;

        signua2 = signua3 - Mu2 - Mu2;
        signla2 = signla3 - Mu2;

        /* For each Ml */
        Ml2 = -JL2 - 2;
        for (int iMl = 0; iMl < JL2 + 1; iMl++) {
            double gl = G_GL;

            Ml2 += 2;

            /* selection rule */
            if (abs(Ml2 - Mu2) > 2)
                continue;

            signua1 = signua2 + Ml2;
            signla1 = signla2;

            /* Zeeman shift */
            dnuB = ZeemanProfileShift(gl, gu, Ml2, Mu2, modB);

            /* Profile */
            Phi = vgt_VoigtProfileNu(nu0 + dnuB, freqs[ifreq], p->nu_d,
                                     p->a_voigt);

            /* Compute 3J */
            J31 = mat_Cou3j(JU2, JL2, 2, -Mu2, Ml2, Mu2 - Ml2);

            /*              */
            /* Absorptivity */
            /*              */

            /* For each K lower level */
            for (int Kl = 0; Kl <= JL2; Kl++) {
                int maxQ = MIN2(Kl, 2);
                Kr21     = (double)(2 * Kl + 1);

                /* For each Q */
                for (int Q = -maxQ; Q <= maxQ; Q++) {
                    int    aQ, Q2 = Q * 2;
                    double sg;

                    if (Q < 0) {
                        aQ = -Q;
                        sg = -1.0;
                    } else {
                        aQ = Q;
                        sg = 1.0;
                    }

                    sign = ZNAM((signla1 - Q2) / 2);

                    /* Index */
                    Ri = GetDmIndexLevel(Kl, aQ);

                    /* Product rhou*Phi */
                    if (Q == 0) {
                        /* Product rhou*Phi */
                        RPre = Phi.re * p->dm_l[Ri].re;
                        RPim = Phi.im * p->dm_l[Ri].re;
                    } else if (aQ == 1) {
                        RPre = Phi.re * p->dm_l[Ri].re * sg -
                               Phi.im * p->dm_l[Ri].im;
                        RPim = Phi.re * p->dm_l[Ri].im +
                               Phi.im * p->dm_l[Ri].re * sg;
                    } else {
                        RPre = Phi.re * p->dm_l[Ri].re -
                               Phi.im * p->dm_l[Ri].im * sg;
                        RPim = Phi.re * p->dm_l[Ri].im * sg +
                               Phi.im * p->dm_l[Ri].re;
                    }

                    /* Check if values */
                    RR = (fabs(RPre) > 0.0) ? 1 : 0;
                    II = (fabs(RPim) > 0.0) ? 1 : 0;

                    /* Check non-zero */
                    if (!(RR || II))
                        continue;

                    /* 3J */
                    J32 =
                        mat_Cou3j(JU2, JL2, 2, -Mu2, Ml2 - Q2, Mu2 - Ml2 + Q2);
                    J32 *= mat_Cou3j(JL2, JL2, 2 * Kl, Ml2, Q2 - Ml2, -Q2) *
                           ((double)sign);
                    if (fabs(J32) <= 0.0)
                        continue;

                    /* For each K geometric */
                    for (int K = 0; K <= 2; K++) {
                        /* Check valid K */
                        if (K < aQ)
                            continue;

                        /* 3J and K factors */
                        J31K = J31 * sqrt(((double)(2 * K + 1)) * Kr21);
                        J33  = mat_Cou3j(2, 2, 2 * K, Ml2 - Mu2, Mu2 + Q2 - Ml2,
                                        -Q2);
                        J3   = J31K * J32 * J33;

                        if (K == 0) {
                            etal[0] += J3 * RPre;
                        } else if (K == 1) {
                            if (Q == 0) {
                                etal[3] += J3 * RPre * tkq[ZT10V_RE];
                                rhol[2] += J3 * RPim * tkq[ZT10V_RE];
                            } else {
                                etal[3] += J3 * (RPre * tkq[ZT11V_RE] * sg -
                                                 RPim * tkq[ZT11V_IM]);
                                rhol[2] += J3 * (RPre * tkq[ZT11V_IM] +
                                                 RPim * tkq[ZT11V_RE] * sg);
                            }
                        } else if (K == 2) {
                            if (Q == 0) {
                                etal[0] += J3 * RPre * tkq[ZT20I_RE];
                                etal[1] += J3 * RPre * tkq[ZT20Q_RE];
                                etal[2] += J3 * RPre * tkq[ZT20U_RE];
                                rhol[0] += J3 * RPim * tkq[ZT20Q_RE];
                                rhol[1] += J3 * RPim * tkq[ZT20U_RE];
                            } else if (aQ == 1) {
                                etal[0] += J3 * (RPre * tkq[ZT21I_RE] * sg -
                                                 RPim * tkq[ZT21I_IM]);
                                etal[1] += J3 * (RPre * tkq[ZT21Q_RE] * sg -
                                                 RPim * tkq[ZT21Q_IM]);
                                etal[2] += J3 * (RPre * tkq[ZT21U_RE] * sg -
                                                 RPim * tkq[ZT21U_IM]);
                                rhol[0] += J3 * (RPre * tkq[ZT21Q_IM] +
                                                 RPim * tkq[ZT21Q_RE] * sg);
                                rhol[1] += J3 * (RPre * tkq[ZT21U_IM] +
                                                 RPim * tkq[ZT21U_RE] * sg);
                            } else {
                                etal[0] += J3 * (RPre * tkq[ZT22I_RE] -
                                                 RPim * tkq[ZT22I_IM] * sg);
                                etal[1] += J3 * (RPre * tkq[ZT22Q_RE] -
                                                 RPim * tkq[ZT22Q_IM] * sg);
                                etal[2] += J3 * (RPre * tkq[ZT22U_RE] -
                                                 RPim * tkq[ZT22U_IM] * sg);
                                rhol[0] += J3 * (RPre * tkq[ZT22Q_IM] * sg +
                                                 RPim * tkq[ZT22Q_RE]);
                                rhol[1] += J3 * (RPre * tkq[ZT22U_IM] * sg +
                                                 RPim * tkq[ZT22U_RE]);
                            }
                        }
                    } /* K geometric */
                }     /* Q multipole */
            }         /* K lower level */

            /*            */
            /* Emissivity */
            /*            */

            /* For each K upper level */
            for (int Ku = 0; Ku <= JU2; Ku++) {
                int maxQ = MIN2(Ku, 2);
                Kr21     = (double)(2 * Ku + 1);

                /* For each Q */
                for (int Q = -maxQ; Q <= maxQ; Q++) {
                    int    aQ, Q2 = Q * 2;
                    double sg;

                    if (Q < 0) {
                        aQ = -Q;
                        sg = -1.0;
                    } else {
                        aQ = Q;
                        sg = 1.0;
                    }

                    sign = ZNAM((signua1 - Q2) / 2);

                    /* Index */
                    Ri = GetDmIndexLevel(Ku, aQ);

                    /* Product rhou*Phi */
                    if (Q == 0) {
                        /* Product rhou*Phi */
                        RPre = Phi.re * p->dm_u[Ri].re;
                        RPim = Phi.im * p->dm_u[Ri].re;
                    } else if (aQ == 1) {
                        RPre = Phi.re * p->dm_u[Ri].re * sg -
                               Phi.im * p->dm_u[Ri].im;
                        RPim = Phi.re * p->dm_u[Ri].im +
                               Phi.im * p->dm_u[Ri].re * sg;
                    } else {
                        RPre = Phi.re * p->dm_u[Ri].re -
                               Phi.im * p->dm_u[Ri].im * sg;
                        RPim = Phi.re * p->dm_u[Ri].im * sg +
                               Phi.im * p->dm_u[Ri].re;
                    }

                    /* Check if values */
                    RR = (fabs(RPre) > 0.0) ? 1 : 0;
                    II = (fabs(RPim) > 0.0) ? 1 : 0;

                    /* Check non-zero */
                    if (!(RR || II))
                        continue;

                    /* 3J */
                    J32 =
                        mat_Cou3j(JU2, JL2, 2, -Mu2 - Q2, Ml2, Mu2 - Ml2 + Q2);
                    J32 *= mat_Cou3j(JU2, JU2, 2 * Ku, Mu2 + Q2, -Mu2, -Q2) *
                           ((double)sign);
                    if (fabs(J32) <= 0.0)
                        continue;

                    /* For each K geometric */
                    for (int K = 0; K <= 2; K++) {
                        /* Check valid K */
                        if (K < aQ)
                            continue;

                        /* 3J and K factors */
                        J31K = J31 * sqrt(((double)(2 * K + 1)) * Kr21);
                        J33  = mat_Cou3j(2, 2, 2 * K, Ml2 - Mu2, Mu2 + Q2 - Ml2,
                                        -Q2);
                        J3   = J31K * J32 * J33;

                        if (K == 0) {
                            etau[0] += J3 * RPre;
                        } else if (K == 1) {
                            if (Q == 0) {
                                etau[3] += J3 * RPre * tkq[ZT10V_RE];
                                rhou[2] += J3 * RPim * tkq[ZT10V_RE];
                            } else {
                                etau[3] += J3 * (RPre * tkq[ZT11V_RE] * sg -
                                                 RPim * tkq[ZT11V_IM]);
                                rhou[2] += J3 * (RPre * tkq[ZT11V_IM] +
                                                 RPim * tkq[ZT11V_RE] * sg);
                            }
                        } else if (K == 2) {
                            if (Q == 0) {
                                etau[0] += J3 * RPre * tkq[ZT20I_RE];
                                etau[1] += J3 * RPre * tkq[ZT20Q_RE];
                                etau[2] += J3 * RPre * tkq[ZT20U_RE];
                                rhou[0] += J3 * RPim * tkq[ZT20Q_RE];
                                rhou[1] += J3 * RPim * tkq[ZT20U_RE];
                            } else if (aQ == 1) {
                                etau[0] += J3 * (RPre * tkq[ZT21I_RE] * sg -
                                                 RPim * tkq[ZT21I_IM]);
                                etau[1] += J3 * (RPre * tkq[ZT21Q_RE] * sg -
                                                 RPim * tkq[ZT21Q_IM]);
                                etau[2] += J3 * (RPre * tkq[ZT21U_RE] * sg -
                                                 RPim * tkq[ZT21U_IM]);
                                rhou[0] += J3 * (RPre * tkq[ZT21Q_IM] +
                                                 RPim * tkq[ZT21Q_RE] * sg);
                                rhou[1] += J3 * (RPre * tkq[ZT21U_IM] +
                                                 RPim * tkq[ZT21U_RE] * sg);
                            } else {
                                etau[0] += J3 * (RPre * tkq[ZT22I_RE] -
                                                 RPim * tkq[ZT22I_IM] * sg);
                                etau[1] += J3 * (RPre * tkq[ZT22Q_RE] -
                                                 RPim * tkq[ZT22Q_IM] * sg);
                                etau[2] += J3 * (RPre * tkq[ZT22U_RE] -
                                                 RPim * tkq[ZT22U_IM] * sg);
                                rhou[0] += J3 * (RPre * tkq[ZT22Q_IM] * sg +
                                                 RPim * tkq[ZT22Q_RE]);
                                rhou[1] += J3 * (RPre * tkq[ZT22U_IM] * sg +
                                                 RPim * tkq[ZT22U_RE]);
                            }
                        }
                    } /* K geometry */
                }     /* Q multipole */
            }         /* K upper level */

        } /* Ml lower level magnetic moment */
    }     /* Mu upper level magnetic moment */

    /* Constants for absorption and emission */
    fl = fact * ((double)JL2 + 1.0) * B_LU;
    fu = fact * ((double)JU2 + 1.0) * A_UL;

    /* Take into account stimulated contributions
       and add to RT coefficients */
    for (int i = 0; i < 4; i++) {
        eta[i] += (etal[i] - etau[i]) * fl;
        eps[i] += etau[i] * fu;
    }
    for (int i = 0; i < 3; i++) {
        rho[i] += (rhol[i] - rhou[i]) * fl;
    }

    /* Complete the computation */
    kmtx[ETA_I]  = eta[0];
    kmtx[ETA_Q]  = eta[1];
    kmtx[ETA_U]  = eta[2];
    kmtx[ETA_V]  = eta[3];
    kmtx[RHO_Q]  = rho[0];
    kmtx[RHO_U]  = rho[1];
    kmtx[RHO_V]  = rho[2];
    sf[STOKES_I] = eps[0] / (kmtx[ETA_I] + VACUUM_OPACITY);
    sf[STOKES_Q] = eps[1] / (kmtx[ETA_I] + VACUUM_OPACITY);
    sf[STOKES_U] = eps[2] / (kmtx[ETA_I] + VACUUM_OPACITY);
    sf[STOKES_V] = eps[3] / (kmtx[ETA_I] + VACUUM_OPACITY);
}

#endif

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static void SKGeneral(t_node *p_node, double sf[NSTOKES], double kmtx[NKMTX],
                      double tkq[NTKQ], const t_vec_3d *dir, int idir,
                      int ifreq)
{
    t_nodedata *p;
    double      etaA[4] = {0, 0, 0, 0}, etaS[4] = {0, 0, 0, 0},
           eps[4] = {0, 0, 0, 0};
    double        fA, fS, nu, nu3;
    static double cA0 = -1e101, cA2 = 0.0, cS0 = 0.0, cS2 = 0.0;
    const double *freqs;

    if (cA0 < -1e100) {
        cA0 = mat_Cou6j(2, 2, 0, JL2, JL2, JU2);
        cA2 = mat_Cou6j(2, 2, 4, JL2, JL2, JU2);
        cS0 = mat_Cou6j(2, 2, 0, JU2, JU2, JL2);
        cS2 = mat_Cou6j(2, 2, 4, JU2, JU2, JL2);
    }

    p  = (t_nodedata *)p_node->p_data;
    fA = C_H /*NU*/ / (4.0 * M_PI) * p->density * B_LU * 1.732050807568877 *
         (JL2 + 1);
    fS = C_H /*NU*/ / (4.0 * M_PI) * p->density * B_UL * 1.732050807568877 *
         (JU2 + 1);
    /* absorption: */
    /* K=0: */
    etaA[0] =
        ZNAM(1 + (JU2 + JL2) / 2) * cA0 * p->dm_l[GetDmIndexLevel(0, 0)].re;
    etaA[1] = 0;
    etaA[2] = 0;
    etaA[3] = 0;
    /* K=2: */
    if (JL2D >= 2) {
        etaA[0] += ZNAM(1 + (JU2 + JL2) / 2 + 2) * cA2 *
                   (2.0 * (tkq[T22I_RE] * p->dm_l[GetDmIndexLevel(2, 2)].re -
                           tkq[T22I_IM] * p->dm_l[GetDmIndexLevel(2, 2)].im) +
                    2.0 * (tkq[T21I_RE] * p->dm_l[GetDmIndexLevel(2, 1)].re -
                           tkq[T21I_IM] * p->dm_l[GetDmIndexLevel(2, 1)].im) +
                    tkq[T20I_RE] * p->dm_l[GetDmIndexLevel(2, 0)].re);
        etaA[1] += ZNAM(1 + (JU2 + JL2) / 2 + 2) * cA2 *
                   (2.0 * (tkq[T22Q_RE] * p->dm_l[GetDmIndexLevel(2, 2)].re -
                           tkq[T22Q_IM] * p->dm_l[GetDmIndexLevel(2, 2)].im) +
                    2.0 * (tkq[T21Q_RE] * p->dm_l[GetDmIndexLevel(2, 1)].re -
                           tkq[T21Q_IM] * p->dm_l[GetDmIndexLevel(2, 1)].im) +
                    tkq[T20Q_RE] * p->dm_l[GetDmIndexLevel(2, 0)].re);
        etaA[2] += ZNAM(1 + (JU2 + JL2) / 2 + 2) * cA2 *
                   (2.0 * (tkq[T22U_RE] * p->dm_l[GetDmIndexLevel(2, 2)].re -
                           tkq[T22U_IM] * p->dm_l[GetDmIndexLevel(2, 2)].im) +
                    2.0 * (tkq[T21U_RE] * p->dm_l[GetDmIndexLevel(2, 1)].re -
                           tkq[T21U_IM] * p->dm_l[GetDmIndexLevel(2, 1)].im));
    }
    etaA[0] *= fA;
    etaA[1] *= fA;
    etaA[2] *= fA;
    etaA[3] *= fA;

    /* (stimulated) emission: */
    /* K=0: */
    etaS[0] =
        ZNAM(1 + (JU2 + JL2) / 2) * cS0 * p->dm_u[GetDmIndexLevel(0, 0)].re;
    etaS[1] = 0;
    etaS[2] = 0;
    etaS[3] = 0;
    /* K=2: */
    if (JU2 >= 2) {
        etaS[0] += ZNAM(1 + (JU2 + JL2) / 2) * cS2 *
                   (2.0 * (tkq[T22I_RE] * p->dm_u[GetDmIndexLevel(2, 2)].re -
                           tkq[T22I_IM] * p->dm_u[GetDmIndexLevel(2, 2)].im) +
                    2.0 * (tkq[T21I_RE] * p->dm_u[GetDmIndexLevel(2, 1)].re -
                           tkq[T21I_IM] * p->dm_u[GetDmIndexLevel(2, 1)].im) +
                    tkq[T20I_RE] * p->dm_u[GetDmIndexLevel(2, 0)].re);
        etaS[1] += ZNAM(1 + (JU2 + JL2) / 2) * cS2 *
                   (2.0 * (tkq[T22Q_RE] * p->dm_u[GetDmIndexLevel(2, 2)].re -
                           tkq[T22Q_IM] * p->dm_u[GetDmIndexLevel(2, 2)].im) +
                    2.0 * (tkq[T21Q_RE] * p->dm_u[GetDmIndexLevel(2, 1)].re -
                           tkq[T21Q_IM] * p->dm_u[GetDmIndexLevel(2, 1)].im) +
                    tkq[T20Q_RE] * p->dm_u[GetDmIndexLevel(2, 0)].re);
        etaS[2] += ZNAM(1 + (JU2 + JL2) / 2) * cS2 *
                   (2.0 * (tkq[T22U_RE] * p->dm_u[GetDmIndexLevel(2, 2)].re -
                           tkq[T22U_IM] * p->dm_u[GetDmIndexLevel(2, 2)].im) +
                    2.0 * (tkq[T21U_RE] * p->dm_u[GetDmIndexLevel(2, 1)].re -
                           tkq[T21U_IM] * p->dm_u[GetDmIndexLevel(2, 1)].im));
    }
    etaS[0] *= fS;
    etaS[1] *= fS;
    etaS[2] *= fS;
    etaS[3] *= fS;

    /* x freq is included below */
    eps[0] = 2.0 * C_H / (C_C * C_C) * etaS[0];
    eps[1] = 2.0 * C_H / (C_C * C_C) * etaS[1];
    eps[2] = 2.0 * C_H / (C_C * C_C) * etaS[2];
    eps[3] = 0.0;

    freqs = PorFreqs();

    sf[STOKES_I] = sf[STOKES_Q] = sf[STOKES_U] = sf[STOKES_V] = kmtx[ETA_I] =
        kmtx[ETA_Q] = kmtx[ETA_U] = kmtx[ETA_V] = kmtx[RHO_Q] = kmtx[RHO_U] =
            kmtx[RHO_V]                                       = 0.0;
    CalculateNodeProfiles(p_node, ifreq, dir, idir, 0);

    nu  = freqs[ifreq];
    nu3 = nu * nu * nu;
    /**/
    /* STIMULATED EMISSION IGNORED */
    kmtx[ETA_I] = g_Prof[0] * nu * (etaA[0] /*- etaS[0]*/)
#ifndef NO_CONT_OPAC
                  + p->c_opac
#endif
        ;
    kmtx[ETA_Q] = g_Prof[0] * nu * (etaA[1] /*- etaS[1]*/);
    kmtx[ETA_U] = g_Prof[0] * nu * (etaA[2] /*- etaS[1]*/);
    kmtx[ETA_V] = 0;
    kmtx[RHO_Q] = 0;
    kmtx[RHO_U] = 0;
    kmtx[RHO_V] = 0;

    /*kmtx[ETA_I] = etaA[STOKES_I];*/

    sf[STOKES_I] = ((nu3 * eps[0]) * nu * g_Prof[0]
#ifndef NO_CONT_OPAC
                    + p->c_emis
#endif
                    ) /
                   (kmtx[ETA_I] + VACUUM_OPACITY);
    sf[STOKES_Q] =
        (nu3 * eps[1]) * nu * g_Prof[0] / (kmtx[ETA_I] + VACUUM_OPACITY);
    sf[STOKES_U] =
        (nu3 * eps[2]) * nu * g_Prof[0] / (kmtx[ETA_I] + VACUUM_OPACITY);
    sf[STOKES_V] = 0.0;
}
/*#endif*/

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static void SKGeneral_tau(t_node *p_node, double sf[NSTOKES],
                          double kmtx[NKMTX], const t_vec_3d *dir, int idir,
                          int ifreq)
{
    t_nodedata *p;
    double      etaA[4] = {0, 0, 0, 0}, etaS[4] = {0, 0, 0, 0},
           eps[4] = {0, 0, 0, 0};
    double        fA, fS, nu, nu3;
    static double cA0 = -1e101, cS0 = 0.0;
    const double *freqs;

    if (cA0 < -1e100) {
        cA0 = mat_Cou6j(2, 2, 0, JL2, JL2, JU2);
        cS0 = mat_Cou6j(2, 2, 0, JU2, JU2, JL2);
    }

    p  = (t_nodedata *)p_node->p_data;
    fA = C_H /*NU*/ / (4.0 * M_PI) * p->density * B_LU * 1.732050807568877 *
         (JL2 + 1);
    fS = C_H /*NU*/ / (4.0 * M_PI) * p->density * B_UL * 1.732050807568877 *
         (JU2 + 1);
    /* absorption: */
    /* K=0: */
    etaA[0] =
        ZNAM(1 + (JU2 + JL2) / 2) * cA0 * p->dm_l[GetDmIndexLevel(0, 0)].re;
    etaA[0] *= fA;

    /* (stimulated) emission: */
    /* K=0: */
    etaS[0] =
        ZNAM(1 + (JU2 + JL2) / 2) * cS0 * p->dm_u[GetDmIndexLevel(0, 0)].re;
    etaS[0] *= fS;

    /* x freq is included below */
    eps[0] = 2.0 * C_H / (C_C * C_C) * etaS[0];

    freqs = PorFreqs();

    sf[STOKES_I] = sf[STOKES_Q] = sf[STOKES_U] = sf[STOKES_V] = kmtx[ETA_I] =
        kmtx[ETA_Q] = kmtx[ETA_U] = kmtx[ETA_V] = kmtx[RHO_Q] = kmtx[RHO_U] =
            kmtx[RHO_V]                                       = 0.0;
    CalculateNodeProfiles(p_node, ifreq, dir, idir, 0);

    nu  = freqs[ifreq];
    nu3 = nu * nu * nu;
    /**/
    /* STIMULATED EMISSION IGNORED */
    kmtx[ETA_I] = g_Prof[0] * nu * (etaA[0] /*- etaS[0]*/)
#ifndef NO_CONT_OPAC
                  + p->c_opac
#endif
        ;
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @param [in] tau  if 1, even in the case where HANLE_ZEEMAN_FS is defined, we
 *will take into account only the Hanle effect (useful for calculation of TAU
 *surface (see issue #61)
 *
 * @todo Add descriptions
 *****************************************************************************/
static void F_SKGeneralDirection(t_node *p_node, double sf[NSTOKES],
                                 double kmtx[NKMTX], const t_vec_3d dir,
                                 int ifreq, int tau)
{
    double   theta, chi;
    t_vec_3d dir_norm;

/* Hanle-Zeeman exact branch */
#if defined(HANLE_ZEEMAN_FS)

    /* Check if asking for tau */
    if (tau == 1) {
        SKGeneral_tau(p_node, sf, kmtx, &dir_norm, -1, ifreq);

    } else {
        /* Get magnetic field */
        double      B;
        t_nodedata *p;
        p = (t_nodedata *)p_node->p_data;

        /* Get magnetic field components */
        B = p->Bx * p->Bx + p->By * p->By + p->Bz * p->Bz;
        B = sqrt(B);

        /* Get propagation vector */
        dir_norm = dir;
        VEC_NORMALIZE_3D(dir_norm);
        VecUnit2Ang(dir_norm, &theta, &chi);

        /* If magnetic field not zero */
        if (B > 0.0) {
            double   thetaB, chiB;
            double   Btheta, Bchi;
            double   ct, st, Bct, Bst, ctB, stB, caB, saB;
            double   cBaa, sBaa, cy, sy;
            double   tkq[ZNTKQ];
            t_vec_3d Bdir_norm, dirB_norm;
            t_vec_3d Bvec;

            /* Cosine and sine of propagation direction in
               laboratory frame */
            ct = cos(theta);
            st = sin(theta);

            /* Magnetic field components */
            Bvec.x = p->Bx;
            Bvec.y = p->By;
            Bvec.z = p->Bz;

            /* If non-vertical field */
            if ((Bvec.x * Bvec.x + Bvec.y * Bvec.y) > 0.0) {
                /* Angle of B in the laboratory frame */
                Bdir_norm = Bvec;
                VEC_NORMALIZE_3D(Bdir_norm);
                VecUnit2Ang(Bdir_norm, &Btheta, &Bchi);
                Bct  = cos(Btheta);
                Bst  = sin(Btheta);
                cBaa = cos(Bchi - chi);
                sBaa = sin(Bchi - chi);

                /* Propagation direction in B frame */
                dirB_norm = dir_Vector2BFrame(dir_norm, Bvec);
                VEC_NORMALIZE_3D(dirB_norm);
                VecUnit2Ang(dirB_norm, &thetaB, &chiB);
                ctB = cos(thetaB);
                stB = sin(thetaB);
                caB = cos(chiB);
                saB = sin(chiB);

                /* Cosine and sine of gamma angle */
                // e1*e1|B
                // cy = ctB*caB*(ct*Bct*cBaa + st*Bst) -
                //     ctB*saB*ct*sBaa -
                //     stB*(ct*Bst*cBaa - st*Bct);
                // e2*e1|B
                // sy = ctB*caB*Bct*sBaa + ctB*saB*cBaa - stB*Bst*sBaa;

                // e2*e2|B
                cy = caB * cBaa - saB * Bct * sBaa;
                // e1*e2|B
                sy = -saB * (ct * Bct * cBaa + st * Bst) - caB * ct * sBaa;

                /* Compute TKQ tensors */
                CalcTKQ(stB, ctB, saB, caB, sy, cy, tkq);

                /* Rotate the rhoKQ if necessary */
                if (!p->rotated) {
                    rotate_dm(p_node, 0.0, Btheta, Bchi);
                    p->rotated = 1;
                }
            } else {
                /* Compute TKQ tensors */
                CalcTKQ(sin(theta), cos(theta), sin(chi), cos(chi), 0.0, 1.0,
                        tkq);
            }

            SKGeneral_HZ(p_node, sf, kmtx, tkq, &dir_norm, -1, ifreq, B);

        } else {
            double tkq[NTKQ];

            /* TKQ in laboratory reference frame */
            dir_CalcTKQ(sin(theta), cos(theta), sin(chi), cos(chi), tkq);

            SKGeneral(p_node, sf, kmtx, tkq, &dir_norm, -1, ifreq);
        }
    }

/* Hanle regime */
#else

    /* Calling just for tau */
    if (tau == 1) {
        SKGeneral_tau(p_node, sf, kmtx, &dir_norm, IDIR_GENDIR, ifreq);

    } else {
        double tkq[NTKQ];

        dir_norm = dir;
        VEC_NORMALIZE_3D(dir_norm);
        VecUnit2Ang(dir_norm, &theta, &chi);
        dir_CalcTKQ(sin(theta), cos(theta), sin(chi), cos(chi), tkq);
        SKGeneral(p_node, sf, kmtx, tkq, &dir_norm, IDIR_GENDIR, ifreq);
    }

#endif
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static void F_SK(t_node *p_node, double sf[NSTOKES], double kmtx[NKMTX],
                 int iray, int ifreq)
{
    double   tkq[NTKQ];
    t_vec_3d dir;
#ifdef DEBUG
    if (!p_node || !p_node->p_data)
        IERR;
#endif
    PorGetTKQ(iray, tkq);
    PorGetIDir(iray, &dir);
    SKGeneral(p_node, sf, kmtx, tkq, &dir, iray, ifreq);
}

/*****************************************************************************/
/**
 * @brief Add J^K_Q and Psi^* contributions of a ray into the t_mojdata
 * structure
 *
 * @todo Add better description
 *****************************************************************************/
static void F_AddFS(t_node *p_node, int iray, int ifreq)
{
    t_nodedata *  p;
    double        di = 0, dq = 0, du = 0, dp = 0;
    const double *freq = PorFreqs();
    double        t, nu, dirq;
    double        tkq[NTKQ];
    t_vec_3d      dir;
    double        phi; /* profile in the co-moving frame */

    p  = (t_nodedata *)p_node->p_data;
    nu = freq[ifreq];
    PorGetIDir(iray, &dir);
    dirq = PorAngQuadrature(iray);
    /* profiles in the co-moving frame: */
    CalculateNodeProfiles(p_node, ifreq, &dir, iray, 0);
    phi = g_Prof[0];

    t = phi * vgt_PoolGetQuadrature(0, ifreq) *
        dirq /* (FREQ_LINE*FREQ_LINE/(freq[ifreq]*freq[ifreq]))*/;
    di = p_node->fsdata.i[STOKES_I] * t;
    dq = p_node->fsdata.i[STOKES_Q] * t;
    du = p_node->fsdata.i[STOKES_U] * t;
    dp = p_node->fsdata.psi_star * t * nu * nu * nu * nu * phi /
         (p_node->fsdata.k[ETA_I] + VACUUM_OPACITY);

    PorGetTKQ(iray, tkq);
    p->jkq.jkq[J00_RE] += di;
    /*
    p->jkq.jkq[J10_RE] += tkq[T10V_RE]*dv;
    p->jkq.jkq[J11_RE] += tkq[T11V_RE]*dv;
    p->jkq.jkq[J11_IM] += tkq[T11V_IM]*dv;
    */
    p->jkq.jkq[J20_RE] += tkq[T20I_RE] * di + tkq[T20Q_RE] * dq;
    p->jkq.jkq[J21_RE] +=
        tkq[T21I_RE] * di + tkq[T21Q_RE] * dq + tkq[T21U_RE] * du;
    p->jkq.jkq[J21_IM] +=
        tkq[T21I_IM] * di + tkq[T21Q_IM] * dq + tkq[T21U_IM] * du;
    p->jkq.jkq[J22_RE] +=
        tkq[T22I_RE] * di + tkq[T22Q_RE] * dq + tkq[T22U_RE] * du;
    p->jkq.jkq[J22_IM] +=
        tkq[T22I_IM] * di + tkq[T22Q_IM] * dq + tkq[T22U_IM] * du;
#ifndef FIXED_POP /* no preconditioning in case of fixed populations */
    p->psi_eta += dp;
#endif
}

/*****************************************************************************/
/**
 * @brief Absorption to the upper level
 *
 * @param cta coefficient in ftont of J^0_0: for use in preconditioning
 *
 * @todo Add better description
 *****************************************************************************/
static t_complex TA(int K, int Q, int Kl, int Ql, t_jkq *jkq, double *cta)
{
    t_complex ta = {0.0, 0.0};
    int       Kr, Qr;

    for (Kr = 0; Kr <= 2; Kr++) {
        for (Qr = -Kr; Qr <= Kr; Qr++) {
            double f =
                B_LU * (JL2 + 1) *
                sqrt(3.0 * (2 * K + 1) * (2 * Kl + 1) * (2 * Kr + 1)) *
                ZNAM(Kl + Ql) *
                mat_Cou9j(JU2, JL2, 2, JU2, JL2, 2, 2 * K, 2 * Kl, 2 * Kr) *
                mat_Cou3j(2 * K, 2 * Kl, 2 * Kr, -2 * Q, 2 * Ql, -2 * Qr);
            t_complex jj = PorGetJKQ(Kr, Qr, jkq);
            ta.re += f * jj.re;
            ta.im += f * jj.im;
            if (Kr == 0) {
                *cta = f;
            }
        }
    }
    return ta;
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static double TE(int K, int Q, int Ku, int Qu)
{
    if (K == Ku && Q == Qu) {
        return A_UL * (JU2 + 1) * ZNAM(1 + (JU2 + JL2) / 2 + K) *
               mat_Cou6j(JU2, JU2, 2 * K, JL2, JL2, 2);
    } else {
        return 0;
    }
}

/*****************************************************************************/
/**
 * @brief Lower level relaxation by absorption
 *
 * @param cra coefficient in fron tof J^0_0: for use in preconditioning
 *
 * @todo Add better description
 *****************************************************************************/
static t_complex RA(int K, int Q, int Kp, int Qp, t_jkq *jkq, double *cra)
{
    t_complex ra = {0.0, 0.0};
    int       Kr, Qr;

    for (Kr = 0; Kr <= 2; Kr++) {
        if ((K + Kp + Kr) % 2)
            continue;
        for (Qr = -Kr; Qr <= Kr; Qr++) {
            double f = B_LU * (JL2 + 1) *
                       sqrt(3.0 * (2 * K + 1) * (2 * Kp + 1) * (2 * Kr + 1)) *
                       ZNAM(1 + (JU2 - JL2) / 2 + Kr + Qp) *
                       mat_Cou6j(2 * K, 2 * Kp, 2 * Kr, JL2, JL2, JL2) *
                       mat_Cou6j(2, 2, 2 * Kr, JL2, JL2, JU2) *
                       mat_Cou3j(2 * K, 2 * Kp, 2 * Kr, 2 * Q, -2 * Qp, 2 * Qr);
            t_complex jj = PorGetJKQ(Kr, Qr, jkq);
            ra.re += f * jj.re;
            ra.im += f * jj.im;
            if (Kr == 0) {
                *cra = f;
            }
        }
    }
    return ra;
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static double RE(int K, int Q, int Kp, int Qp)
{
    if (K == Kp && Q == Qp) {
        return A_UL;
    } else {
        return 0.0;
    }
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static t_complex ***AllocMagneticKernel(int j2, double lande_fac)
{
    int          ik, j, kmax = j2;
    t_complex ***k;

    k = (t_complex ***)Malloc(sizeof(t_complex **) * (kmax + 1));
    for (ik = 0; ik <= kmax; ik++) {
        k[ik] = (t_complex **)Malloc(sizeof(t_complex *) * (2 * ik + 1));
        for (j = 0; j < 2 * ik + 1; j++) {
            int m;
            k[ik][j] = (t_complex *)Malloc(sizeof(t_complex) * (2 * ik + 1));
            for (m = 0; m < 2 * ik + 1; m++) {
                k[ik][j][m].re = k[ik][j][m].im = 0;
            }
        }
    }
    return k;
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static void SetMagneticKernel(t_complex ***ker, t_node *p_node, int j2,
                              double lande_fac)
{
    int         ik, j, kmax = j2;
    t_nodedata *p = (t_nodedata *)p_node->p_data;
    double      b, larmor;
    t_complex   fac;
    double      chi_b, theta_b;
    t_vec_3d    bv;

    b      = sqrt(p->Bx * p->Bx + p->By * p->By + p->Bz * p->Bz);
    larmor = 1.3996e6 * b;
    /* init the kernel */
    for (ik = 0; ik <= kmax; ik++) {
        for (j = 0; j < 2 * ik + 1; j++) {
            int m;
            for (m = 0; m < 2 * ik + 1; m++) {
                ker[ik][j][m].re = ker[ik][j][m].im = 0;
            }
        }
    }
    if (b == 0.0)
        return; /* no need to calculate the kernel */

    bv.x = p->Bx / b;
    bv.y = p->By / b;
    bv.z = p->Bz / b;
    VecUnit2Ang(bv, &theta_b, &chi_b);
    fac.re = 0.0;
    fac.im = -2.0 * M_PI * larmor * lande_fac;

    for (ik = 0; ik <= kmax; ik++) {
        t_complex **dkrotm = dir_GetDKRot(ik, 0.0, -theta_b, -chi_b);
        int         q, q1, q2;
        /* see Eq. 7.79 of Ll04 (2004) */
        for (q = -ik; q <= ik; q++) {
            for (q1 = -ik; q1 <= ik; q1++) {
                t_complex tc;
                for (q2 = -ik; q2 <= ik; q2++) {
                    t_complex dk_con, dk, mul;
                    dk_con.re = dkrotm[ik + q2][ik + q].re;
                    dk_con.im = -dkrotm[ik + q2][ik + q].im;
                    dk.re     = dkrotm[ik + q2][ik + q1].re;
                    dk.im     = dkrotm[ik + q2][ik + q1].im;
                    CLX_MUL(mul, dk_con, dk);
                    ker[ik][ik + q][ik + q1].re += mul.re * q2;
                    ker[ik][ik + q][ik + q1].im += mul.im * q2;
                }
                CLX_MUL(tc, ker[ik][ik + q][ik + q1], fac);
                ker[ik][ik + q][ik + q1] = tc;
            }
        }
        for (q = 0; q < 2 * ik + 1; q++)
            free(dkrotm[q]);
        free(dkrotm);
    }
}

/*****************************************************************************/
/**
 * @brief Add magnetic kernel to complex ESE
 *
 * @todo Add better description
 *****************************************************************************/
static void AddKernel(int ilevel, int k)
{
    int          idm = GetDMIndex(ilevel, k, -k);
    int          i, j;
    t_complex ***ker = (ilevel == 0 ? g_kernel_l : g_kernel_u);

    if (k == 0)
        return;

    for (j = 0; j < 2 * k + 1; j++) {
        for (i = 0; i < 2 * k + 1; i++) {
            g_ese_c[idm + j][idm + i].re += ker[k][j][i].re;
            g_ese_c[idm + j][idm + i].im += ker[k][j][i].im;
        }
    }
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static void AddAllKernels(void)
{
    int k;
    for (k = 0; k <= JL2; k++) {
        AddKernel(0, k);
    }
    for (k = 0; k <= JU2; k++) {
        AddKernel(1, k);
    }
}

/*****************************************************************************/
/**
 * @brief Transfrom complex ESE to a real system
 *
 * @todo Add better description
 * @todo This function is INCORRECT! at least if magnetic kernel is used
 *****************************************************************************/
static void MakeESEReal(void)
{
    /* use only Q>=0 multipoles */
    int         k, q, i, j, ir, jr;
    int         nc   = g_nlower + g_nupper;
    int *       incl = (int *)Malloc(sizeof(int) * nc);
    t_complex **smaller_c =
        (t_complex **)Malloc(sizeof(t_complex *) * g_nreal / 2);
    t_complex *smaller_rhs =
        (t_complex *)Malloc(sizeof(t_complex) * g_nreal / 2);

    for (i = 0; i < g_nreal / 2; i++)
        smaller_c[i] = (t_complex *)Malloc(sizeof(t_complex) * g_nreal / 2);

    /* select which multipoles will be included */
    i = 0;
    for (k = 0; k <= JL2; k++) {
        for (q = -k; q <= k; q++) {
            if (q >= 0)
                incl[i] = 1;
            else
                incl[i] = 0;
            i++;
        }
    }
    for (k = 0; k <= JU2; k++) {
        for (q = -k; q <= k; q++) {
            if (q >= 0)
                incl[i] = 1;
            else
                incl[i] = 0;
            i++;
        }
    }

    if (i != nc)
        IERR;

    /* make the smaller complex matrix */
    ir = 0;
    for (i = 0; i < nc; i++) {
        if (!incl[i])
            continue;
        jr = 0;
        for (j = 0; j < nc; j++) {
            if (!incl[j])
                continue;
            if (jr >= g_nreal / 2 || ir >= g_nreal / 2)
                IERR;
            if (i >= nc || j >= nc)
                IERR;
            smaller_c[ir][jr] = g_ese_c[i][j];
            jr++;
        }
        smaller_rhs[ir] = g_rhs_c[i];
        ir++;
    }

    /* make the real matrix */
    Complex2RealLinSystem(g_nreal / 2, smaller_c, smaller_rhs, g_ese_r,
                          g_rhs_r);
    /* uncoupled and/or undefined components ==> zero: */
    for (i = 0; i < g_nreal; i++) {
        int nzer = 0;
        for (j = 0; j < g_nreal; j++) {
            if (g_ese_r[i][j] == 0)
                nzer++;
        }
        if (nzer == g_nreal) {
            IERR;
            g_ese_r[i][i] = 1.0;
            g_rhs_r[i]    = 0.0;
        }
    }

    /* free memory */
    free(smaller_rhs);
    for (i = 0; i < g_nreal / 2; i++)
        free(smaller_c[i]);
    free(smaller_c);
    free(incl);
}

#ifdef NONTHERMAL

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static double sigm(double a, double d, double x)
{
    return 1.0 / (1.0 + exp(-(x - d) / a));
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static double NonthermalFactor(t_node *p_node)
{
    double fac_z = 4.0 * exp(-SQR((double)p_node->iz - 77) / SQR(15));
    /*double fac_x = exp( - SQR((PorGetNodeCoords(p_node).x/1e5) / W_RIBBON)
     * );*/
    double x     = PorGetNodeCoords(p_node).x / 1e5;
    double fac_x = sigm(150, -3000, x) * sigm(-150, 3000, x);
    return 1.0 + fac_z * fac_x;
}
#endif

/*****************************************************************************/
/**
 * @brief For the known J^K_Q and Psi^* calculates the ESE matrix and the rhs f
 *
 * @warning don't use for getting the real ese and f! only use the
 * g_ese_c. DON'T use in multigrid
 *
 * @todo Add better description
 *****************************************************************************/
static void F_GetESE(t_node *p_node, double **ese, double *f)
{
    t_nodedata *p;
    int         K, Q, i, j;
    int         nc = g_nlower + g_nupper;
    double      bf, C, L_coef;

#ifdef DEBUG
    if (!p_node || !p_node->p_data || !ese || !f)
        IERR;
#endif
    p      = (t_nodedata *)p_node->p_data;
    bf     = exp(C_H * FREQ_LINE / C_KB / p->temp);
    C      = p->eps * A_UL / (1.0 - p->eps);
    C      = C * (JU2 + 1) / (JL2 + 1) / bf; /* excitation coll. rate */
    L_coef = g_L_factor * p->density * p->psi_eta;

    for (i = 0; i < nc; i++) {
        for (j = 0; j < nc; j++) {
            g_ese_c[i][j].re = g_ese_c[i][j].im = 0;
        }
    }
    /* rows of the lower level: */
    i = 0;
    for (K = 0; K <= JL2; K++) {
        for (Q = -K; Q <= K; Q++) {
            int Ku, Qu, Kp, Qp;
            /* transfer from the upper level: */
            j = NLevelDMElements(JL2);
            for (Ku = 0; Ku <= JU2; Ku++) {
                for (Qu = -Ku; Qu <= Ku; Qu++) {
                    double te;
                    /* spontaneous radiative decay: */
                    te = TE(K, Q, Ku, Qu);
                    g_ese_c[i][j].re += te;
                    /* collisional de-excitation: */
                    if (K == 0 && Ku == 0) {
                        g_ese_c[i][j].re +=
                            sqrt(((double)(JL2 + 1)) / (JU2 + 1)) * bf * C;
                    }
                    j++;
                }
            }
            /* relaxation of the lower level: */
            for (Kp = 0; Kp <= JL2; Kp++) {
                for (Qp = -Kp; Qp <= Kp; Qp++) {
                    double cra           = 0;
                    int    index_00u     = GetDMIndex(1, 0, 0),
                        pi               = GetDMIndex(0, Kp, Qp);
                    double    rho00u_old = p->dm_u[0].re;
                    t_complex rhoKQl_old = p->dm_l[pi];
                    /* radiative: */
                    t_complex ra = RA(K, Q, Kp, Qp, &p->jkq, &cra);
                    if (Kp != 0)
                        cra = 0;
                    /*cra = 0;*/
                    g_ese_c[i][pi].re -= ra.re;
                    g_ese_c[i][pi].re -= -cra * L_coef * rho00u_old;
                    g_ese_c[i][pi].im -= ra.im;
                    g_ese_c[i][index_00u].re -= cra * L_coef * rhoKQl_old.re;
                    g_ese_c[i][index_00u].im -= cra * L_coef * rhoKQl_old.im;
                }
            }
            /* collisional relaxation */
            g_ese_c[i][i].re -= C
#ifdef NONTHERMAL
                                    * NonthermalFactor(p_node)
#endif
                                + D2_L(p) * (K == 0 ? 0.0 : 1.0) *
                                      (K == 1 ? 0.3333 : 1.0)
#ifdef NONTHERMAL

#ifdef NTDEPOL
                                      * NonthermalFactor(p_node)
#endif

#endif
                ;
            i++;
        }
    }
    /* rows of the upper level */
    for (K = 0; K <= JU2; K++) {
        for (Q = -K; Q <= K; Q++) {
            int Kl, Ql, Kp, Qp;
            /* transfer from the lower level: */
            j = 0;
            for (Kl = 0; Kl <= JL2; Kl++) {
                for (Ql = -Kl; Ql <= Kl; Ql++) {
                    /* radiative: */
                    int       index_00u  = GetDMIndex(1, 0, 0);
                    double    cta        = 0;
                    double    rho00u_old = p->dm_u[0].re;
                    t_complex rhoKQl_old = p->dm_l[j];
                    t_complex ta         = TA(K, Q, Kl, Ql, &p->jkq, &cta);
                    g_ese_c[i][j].re += ta.re - cta * L_coef * rho00u_old;
                    g_ese_c[i][j].im += ta.im;
                    g_ese_c[i][index_00u].re += cta * L_coef * rhoKQl_old.re;
                    g_ese_c[i][index_00u].im += cta * L_coef * rhoKQl_old.im;
                    /* collisional excitation: */
                    if (K == 0 && Kl == 0) {
                        g_ese_c[i][j].re +=
                            sqrt(((double)(JL2 + 1)) / (JU2 + 1)) * C
#ifdef NONTHERMAL
                            * NonthermalFactor(p_node)
#endif
                            ;
                    }
                    j++;
                }
            }
            /* relaxation of the upper level */
            for (Kp = 0; Kp <= JU2; Kp++) {
                for (Qp = -Kp; Qp <= Kp; Qp++) {
                    int    pi;
                    double re;
                    pi = GetDMIndex(1, Kp, Qp);
                    /* spontaneous radiative relaxation: */
                    re = RE(K, Q, Kp, Qp);
                    g_ese_c[i][pi].re -= re;
                }
            }
            /* collisional relaxation */
            g_ese_c[i][i].re -= bf * (JL2 + 1) / (JU2 + 1) * C +
                                D2_U(p) * (K == 0 ? 0.0 : 1.0) *
                                    (K == 1 ? 0.33333 : 1.0)
#ifdef NONTHERMAL
#ifdef NTDEPOL
                                    * NonthermalFactor(p_node)
#endif
#endif
                ;
            i++;
        }
    }

    /* add magnetic filed: */
    SetMagneticKernel(g_kernel_l, p_node, JL2, LANDE_L);
    SetMagneticKernel(g_kernel_u, p_node, JU2, LANDE_U);
    AddAllKernels();

    /* normalization */
    for (i = 0; i < nc; i++) {
        g_ese_c[0][i].re = g_ese_c[0][i].im = 0.0;
        g_rhs_c[i].re = g_rhs_c[i].im = 0.0;
    }
    g_ese_c[0][GetDMIndex(0, 0, 0)].re = sqrt((double)JL2 + 1);
    g_ese_c[0][GetDMIndex(1, 0, 0)].re = sqrt((double)JU2 + 1);
    g_rhs_c[0].re                      = 1.0;

    /* "realization" */
    MakeESEReal();

#ifdef DEBUG
    if (!f)
        IERR;
#endif
    for (i = 0; i < g_nreal; i++) {
        Memcpy(ese[i], g_ese_r[i], DBLSIZE * g_nreal);
    }
    Memcpy(f, g_rhs_r, DBLSIZE * g_nreal);
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
int F_GetNDM(void) { return g_nreal; }

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
int *F_GetPopsPositions(void)
{
    int *pops, i, k, q;
    pops = (int *)Malloc(g_nreal * INTSIZE);
    for (i = 0; i < g_nreal; i++)
        pops[i] = 0;
    i = 0;
    /* populations only appear in the real part of dm */
    for (k = 0; k <= JL2; k++) {
        for (q = 0; q <= k; q++) {
            if (k == 0)
                pops[i] = 1;
            i++;
        }
    }
    for (k = 0; k <= JU2; k++) {
        for (q = 0; q <= k; q++) {
            if (k == 0)
                pops[i] = 1;
            i++;
        }
    }
    if (i != g_nreal / 2)
        Error(E_ERROR, POR_AT, "i=%d, g_nreal=%d", i, g_nreal);
    return pops;
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static void F_GetDM(t_node *p_node, double *data)
{
    t_nodedata *p;
    int         k, q, ir;
#ifdef DEBUG
    if (!p_node || !data)
        IERR;
#endif
    p  = (t_nodedata *)p_node->p_data;
    ir = 0;
    for (k = 0; k <= JL2; k++) {
        for (q = 0; q <= k; q++) {
            int i                  = GetDmIndexLevel(/*0,*/ k, q);
            data[ir]               = p->dm_l[i].re;
            data[ir + g_nreal / 2] = p->dm_l[i].im;
            ir++;
        }
    }
    for (k = 0; k <= JU2; k++) {
        for (q = 0; q <= k; q++) {
            int i                  = GetDmIndexLevel(/*1,*/ k, q);
            data[ir]               = p->dm_u[i].re;
            data[ir + g_nreal / 2] = p->dm_u[i].im;
            ir++;
        }
    }
    if (ir != g_nreal / 2)
        IERR;
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static void F_SetDM(t_node *p_node, double *data)
{
    t_nodedata *p;
    int         ir, k, q;
#ifdef DEBUG
    if (!p_node || !data)
        IERR;
#endif
    p = (t_nodedata *)p_node->p_data;

    ir = 0;
    for (k = 0; k <= JL2; k++) {
        for (q = 0; q <= k; q++) {
            int i         = GetDmIndexLevel(/*0,*/ k, q);
            p->dm_l[i].re = data[ir];
            p->dm_l[i].im = data[ir + g_nreal / 2];
            if (q > 0) {
                i             = GetDmIndexLevel(/*0,*/ k, -q);
                p->dm_l[i].re = ZNAM(q) * data[ir];
                p->dm_l[i].im = -ZNAM(q) * data[ir + g_nreal / 2];
            }
            ir++;
        }
    }
    for (k = 0; k <= JU2; k++) {
        for (q = 0; q <= k; q++) {
            int i         = GetDmIndexLevel(/*1,*/ k, q);
            p->dm_u[i].re = data[ir];
            p->dm_u[i].im = data[ir + g_nreal / 2];
            if (q > 0) {
                i             = GetDmIndexLevel(/*1,*/ k, -q);
                p->dm_u[i].re = ZNAM(q) * data[ir];
                p->dm_u[i].im = -ZNAM(q) * data[ir + g_nreal / 2];
            }
            ir++;
        }
    }
    if (ir != g_nreal / 2)
        IERR;

    /* kill the imaginary noise which may cause instabilities */
    for (k = 0; k <= JL2; k++)
        p->dm_l[GetDmIndexLevel(/*0,*/ k, 0)].im = 0;
    for (k = 0; k <= JU2; k++)
        p->dm_u[GetDmIndexLevel(/*0,*/ k, 0)].im = 0;
}

#ifdef FIXED_POP

/*****************************************************************************/
/**
 * @brief ESE solver with fixed atomic level populations.
 *
 * Solution: calculate the new full density matrix, restore the populations and
 * keep the rhoKQ/rho00
 *
 * @todo Add better description
 *****************************************************************************/
double F_SolveESE(t_node *p_node)
{
    double **ese = NULL, *f = NULL, *dmold, maxrc = 0;
    int *    pops = F_GetPopsPositions();
    int      n    = F_GetNDM();

    ese   = MatrixNew(n, n);
    f     = ArrayNew(n);
    dmold = ArrayNew(n);
    F_GetDM(p_node, dmold);
    F_GetESE(p_node, ese, f);

    {
        int i, li = GetDMIndex(0, 0, 0), ui = GetDMIndex(1, 0, 0),
               nc = g_nlower + g_nupper;
        for (i = 0; i < nc; i++) {
            g_ese_c[0][i].re = g_ese_c[0][i].im = g_ese_c[ui][i].re =
                g_ese_c[ui][i].im               = 0;
            g_rhs_c[i].re = g_rhs_c[i].im = 0;
        }
        g_ese_c[li][li].re = 1.0;
        g_ese_c[ui][ui].re = 1.0;
        g_rhs_c[li].re     = ((t_nodedata *)p_node->p_data)->dm_l[0].re;
        g_rhs_c[ui].re     = ((t_nodedata *)p_node->p_data)->dm_u[0].re;
    }

    {
        int nr = 2 * (g_nlower + g_nupper), k, q, idx = 0,
            cd        = NLevelDMElements(JL2) + NLevelDMElements(JU2), nnr;
        double **rese = MatrixNew(nr, nr), *rrhs = ArrayNew(nr);
        Complex2RealLinSystem(nr / 2, g_ese_c, g_rhs_c, rese, rrhs);
        LudSolve(nr, rese, rrhs, 1);
        nnr = 0;
        for (k = 0; k <= JL2; k++)
            for (q = 0; q <= k; q++)
                nnr++;
        for (k = 0; k <= JU2; k++)
            for (q = 0; q <= k; q++)
                nnr++;
        for (k = 0; k <= JL2; k++) {
            for (q = 0; q <= k; q++) {
                f[idx]       = rrhs[GetDMIndex(0, k, q)];
                f[idx + nnr] = rrhs[GetDMIndex(0, k, q) + cd];
                idx++;
            }
        }
        for (k = 0; k <= JU2; k++) {
            for (q = 0; q <= k; q++) {
                f[idx]       = rrhs[GetDMIndex(1, k, q)];
                f[idx + nnr] = rrhs[GetDMIndex(1, k, q) + cd];
                idx++;
            }
        }
        free(rrhs);
        MatrixFree(nr, nr, rese);
    }

    /* maximum relative change = 0 */
    maxrc = 0.0;

    F_SetDM(p_node, f);

    MatrixFree(n, n, ese);
    free(f);
    free(dmold);
    free(pops);
    return maxrc;
}

#else

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
double F_SolveESE(t_node *p_node)
{
    double **ese = NULL, *f = NULL, *dmold, maxrc = 0;
    int *pops = F_GetPopsPositions();
    int n = F_GetNDM(), i;

    ese = MatrixNew(n, n);
    f = ArrayNew(n);
    dmold = ArrayNew(n);
    F_GetDM(p_node, dmold);
    F_GetESE(p_node, ese, f);

    {
        /**/
        /* bug fixed on Sep. 20, 2014: treatment of full ESE instead of its
         * restricted real version in MakeESEReal() */
        int nr = 2 * (g_nlower + g_nupper), k, q, idx = 0,
            cd = NLevelDMElements(JL2) + NLevelDMElements(JU2), nnr;
        double **rese = MatrixNew(nr, nr), *rrhs = ArrayNew(nr);
        Complex2RealLinSystem(nr / 2, g_ese_c, g_rhs_c, rese, rrhs);
        LudSolve(nr, rese, rrhs, 1);
        nnr = 0;
        for (k = 0; k <= JL2; k++)
            for (q = 0; q <= k; q++)
                nnr++;
        for (k = 0; k <= JU2; k++)
            for (q = 0; q <= k; q++)
                nnr++;
        for (k = 0; k <= JL2; k++) {
            for (q = 0; q <= k; q++) {
                f[idx] = rrhs[GetDMIndex(0, k, q)];
                f[idx + nnr] = rrhs[GetDMIndex(0, k, q) + cd];
                idx++;
            }
        }
        for (k = 0; k <= JU2; k++) {
            for (q = 0; q <= k; q++) {
                f[idx] = rrhs[GetDMIndex(1, k, q)];
                f[idx + nnr] = rrhs[GetDMIndex(1, k, q) + cd];
                idx++;
            }
        }
        free(rrhs);
        MatrixFree(nr, nr, rese);
    }

    for (i = 0; i < n; i++) /* maximum relative change */
    {
        if ((pops[i] && f[i] <= 0) || isnan(f[i])) {
            Error(E_WARNING, POR_AT,
                  "invalid density matrix element %e (at i=%d) in grid node "
                  "(%d,%d,%d)",
                  f[i], i, p_node->ix, p_node->iy, p_node->iz);
            /* in case of negative population, try to use the previous DM
             * instead */
            F_SetDM(p_node, dmold);
            maxrc = 0;
            goto F_SolveESE_emerg_exit;
        }
        if (pops[i] && fabs(f[i] - dmold[i]) / f[i] > maxrc) {
            maxrc = fabs(f[i] - dmold[i]) / f[i];
        }
    }
    F_SetDM(p_node, f);
F_SolveESE_emerg_exit:
    MatrixFree(n, n, ese);
    free(f);
    free(dmold);
    free(pops);
    return maxrc;
}

#endif

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static void DM2BFrame(t_nodedata *pdata) {}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * rotate the density matrix to the Z-frame
 *
 * @todo Add descriptions
 *****************************************************************************/
static void DM2ZFrame(t_nodedata *pdata) {}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static double F_GetTemperature(t_node *p_node)
{
    t_nodedata *p;
#ifdef DEBUG
    if (!p_node)
        IERR;
#endif
    p = (t_nodedata *)p_node->p_data;
    return p->temp;
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static t_vec_3d F_GetMagneticVector(t_node *p_node)
{
    t_nodedata *p;
    t_vec_3d    B;
#ifdef DEBUG
    if (!p_node || !p_node->p_data)
        IERR;
#endif
    p   = (t_nodedata *)p_node->p_data;
    B.x = p->Bx;
    B.y = p->By;
    B.z = p->Bz;
    return B;
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static double *F_GetRadiation(t_node *p_node, int *ndata)
{
    t_nodedata *p;
    double *    r = ArrayNew(10);
    p             = (t_nodedata *)p_node->p_data;
    r[0]          = p->jkq.jkq[J00_RE];
    r[1]          = p->jkq.jkq[J10_RE];
    r[2]          = p->jkq.jkq[J11_RE];
    r[3]          = p->jkq.jkq[J11_IM];
    r[4]          = p->jkq.jkq[J20_RE];
    r[5]          = p->jkq.jkq[J21_RE];
    r[6]          = p->jkq.jkq[J21_IM];
    r[7]          = p->jkq.jkq[J22_RE];
    r[8]          = p->jkq.jkq[J22_IM];
    r[9]          = p->psi_eta;
    *ndata        = 10;
    return r;
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static void F_SetRadiation(t_node *p_node, double *data)
{
    t_nodedata *p;
    p                  = (t_nodedata *)p_node->p_data;
    p->jkq.jkq[J00_RE] = data[0];
    p->jkq.jkq[J10_RE] = data[1];
    p->jkq.jkq[J11_RE] = data[2];
    p->jkq.jkq[J11_IM] = data[3];
    p->jkq.jkq[J20_RE] = data[4];
    p->jkq.jkq[J21_RE] = data[5];
    p->jkq.jkq[J21_IM] = data[6];
    p->jkq.jkq[J22_RE] = data[7];
    p->jkq.jkq[J22_IM] = data[8];
    p->psi_eta         = data[9];
}

/*****************************************************************************/
/**
 * @brief To be done
 *
 * @todo Add descriptions
 *****************************************************************************/
static void F_ResetRadiation(t_node *p_node)
{
    t_nodedata *p;
#ifdef DEBUG
    if (!p_node)
        IERR;
#endif
    p = (t_nodedata *)p_node->p_data;
    ArraySet(NJKQ, p->jkq.jkq, 0.0);
    p->psi_eta   = 0;
    p->idir_last = IDIR_UNDEF;
}

/*****************************************************************************/
/**
 * @brief Write Model header data
 *
 * Write Model Header data to a .pmd binary file
 *
 * @param f [in] File identifier
 * @return 1 if successful ; -1 if not
 *****************************************************************************/
static int F_IO_WriteHeader(FILE *f)
{
    int i, written = 0;
    int ver = TWOLEVELVERSION;
    written += fwrite(&ver, INTSIZE, 1, f);
    written += fwrite(&A_MASS, DBLSIZE, 1, f);
    written += fwrite(&g_A_UL, DBLSIZE, 1, f);
    written += fwrite(&g_E_UL, DBLSIZE, 1, f);
    written += fwrite(&JL2, INTSIZE, 1, f);
    written += fwrite(&JU2, INTSIZE, 1, f);
    written += fwrite(&G_GL, DBLSIZE, 1, f);
    written += fwrite(&G_GU, DBLSIZE, 1, f);
    written += fwrite(&g_TEMP_REF, DBLSIZE, 1, f);
    for (i = 0; i < PorNY(); i++) {
        written += fwrite(g_temp[i], DBLSIZE, PorNX(), f);
    }
    if (written == 9 + PorNX() * PorNY())
        return 1;
    else
        return -1;
}

/*****************************************************************************/
/**
 * @brief Write Model header data
 *
 * Write Model Header data to a HDF5 file (/Module group attributes, plus
 * /Module/temp_bc dataset).
 *
 * Also the g_data dataset is created (though empty)
 *
 * @param f_id [in] HDF5 file identifier
 * @return hdf5 identifier of g_data dataset ; -1 if not
 *
 * @todo check return status all all calls to h5t_insert, etc.
 *****************************************************************************/
static hid_t F_IO_WriteHeader_h5(hid_t f_id)
{
    int     i2[2], i, j;
    int     ver = TWOLEVELVERSION;
    hid_t   group_id, memtype, B_type, V_type, dm_type, jkq_type;
    hid_t   space, dset;
    hsize_t locdims[1];
    herr_t  status;
    int     g_nx = PorNX(), g_ny = PorNY(), g_nz = PorNZ();
    hsize_t dims[3];

    group_id =
        H5Gcreate(f_id, "/Module", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5LTset_attribute_int(f_id, "/Module", "TL_VERSION", &ver, 1);
    H5LTset_attribute_double(f_id, "/Module", "ATOM_MASS", &A_MASS, 1);
    H5LTset_attribute_double(f_id, "/Module", "A_UL", &g_A_UL, 1);
    H5LTset_attribute_double(f_id, "/Module", "E_UL", &g_E_UL, 1);
    H5LTset_attribute_int(f_id, "/Module", "JL2", &JL2, 1);
    H5LTset_attribute_int(f_id, "/Module", "JU2", &JU2, 1);
    H5LTset_attribute_double(f_id, "/Module", "GL", &G_GL, 1);
    H5LTset_attribute_double(f_id, "/Module", "GU", &G_GU, 1);
    H5LTset_attribute_double(f_id, "/Module", "TEMP", &g_TEMP_REF, 1);

    double *mdata_hdf;
    mdata_hdf = (double *)malloc(g_nx * g_ny * sizeof(*mdata_hdf));
    for (j = 0; j < g_ny; j++) {
        for (i = 0; i < g_nx; i++) {
            mdata_hdf[j * g_nx + i] = g_temp[j][i];
        }
    }
    hsize_t mdims[2] = {g_ny, g_nx};
    H5LTmake_dataset(f_id, "/Module/temp_bc", 2, mdims, H5T_NATIVE_DOUBLE,
                     mdata_hdf);
    free(mdata_hdf);

    /* Create the array components for g_data */
    locdims[0] = 3;
    B_type     = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, locdims);
    V_type     = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, locdims);

    locdims[0] = 2 * (L_LIMIT + U_LIMIT);
    dm_type    = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, locdims);

    locdims[0] = NJKQ;
    jkq_type   = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, locdims);

    /* Create the memtype - based on the t_pmddata struct */
    memtype = H5Tcreate(H5T_COMPOUND, NODE_PMD_SIZE);
    status =
        H5Tinsert(memtype, "eps", HOFFSET(t_pmddata, eps), H5T_NATIVE_DOUBLE);
    status =
        H5Tinsert(memtype, "temp", HOFFSET(t_pmddata, temp), H5T_NATIVE_DOUBLE);
    status = H5Tinsert(memtype, "density", HOFFSET(t_pmddata, density),
                       H5T_NATIVE_DOUBLE);
    status = H5Tinsert(memtype, "B", HOFFSET(t_pmddata, B), B_type);
    status = H5Tinsert(memtype, "V", HOFFSET(t_pmddata, V), V_type);
    status = H5Tinsert(memtype, "dm", HOFFSET(t_pmddata, dm), dm_type);
    status = H5Tinsert(memtype, "jkq", HOFFSET(t_pmddata, jkq), jkq_type);
    status = H5Tinsert(memtype, "a_voigt", HOFFSET(t_pmddata, a_voigt),
                       H5T_NATIVE_DOUBLE);
    status = H5Tinsert(memtype, "delta2", HOFFSET(t_pmddata, delta2),
                       H5T_NATIVE_DOUBLE);
    status = H5Tinsert(memtype, "c_opac", HOFFSET(t_pmddata, c_opac),
                       H5T_NATIVE_DOUBLE);
    status = H5Tinsert(memtype, "c_emis", HOFFSET(t_pmddata, c_emis),
                       H5T_NATIVE_DOUBLE);

    /* Pack datatype, so there will be no padding between fields */
    H5Tpack(memtype);

    /* Create /Module/g_data dataset. Empty, to be filled by SaveNodesSlave_h5
     * function */
    dims[0] = g_nz;
    dims[1] = g_ny;
    dims[2] = g_nx;
    space   = H5Screate_simple(3, dims, NULL);
    dset    = H5Dcreate(f_id, "/Module/g_data", memtype, space, H5P_DEFAULT,
                     H5P_DEFAULT, H5P_DEFAULT);

    status = H5Sclose(space);
    status = H5Tclose(memtype);
    status = H5Tclose(jkq_type);
    status = H5Tclose(dm_type);
    status = H5Tclose(V_type);
    status = H5Tclose(B_type);

    if ((status = H5Gclose(group_id)) < 0) {
        Error(E_ERROR, POR_AT, "cannot close group /Module \n");
        return -1;
    }

    return dset;
}

/*****************************************************************************/
/**
 * @brief Load Model header data
 *
 * Load Model Header data from the .pmd binary file
 *
 * @param f [in] File identifier
 * @return 1 if successful ; 0 if not
 *****************************************************************************/
static int LoadHeader(FILE *f)
{
    int i, cnt = 0, jl2, ju2;
    int ver;

    /*stk_Add(1, POR_AT, "twolevel: loading the model header");*/
    cnt += fread(&ver, INTSIZE, 1, f);
    if (ver != TWOLEVELVERSION) {
        Error(E_ERROR, POR_AT,
              "incompatible version %d of the format (supported version: %d)",
              ver, TWOLEVELVERSION);
        return 0;
    }
    cnt += fread(&A_MASS, DBLSIZE, 1, f);
    cnt += fread(&g_A_UL, DBLSIZE, 1, f);
    cnt += fread(&g_E_UL, DBLSIZE, 1, f);

    cnt += fread(&jl2, INTSIZE, 1, f);
    cnt += fread(&ju2, INTSIZE, 1, f);
    if (jl2 != JL2 || ju2 != JU2) {
        Error(
            E_ERROR, POR_AT,
            "incompatible angular momenta of the levels (%d %d (file) vs %d %d "
            "(code))",
            jl2, ju2, JL2, JU2);
        return 0;
    }

    cnt += fread(&G_GL, DBLSIZE, 1, f);
    cnt += fread(&G_GU, DBLSIZE, 1, f);
    cnt += fread(&g_TEMP_REF, DBLSIZE, 1, f);
    for (i = 0; i < PorNY(); i++) {
        int cc = fread(g_temp[i], DBLSIZE, PorNX(), f);
        if (cc != PorNX()) {
            Error(E_ERROR, POR_AT, "cannot read from file");
            return 0;
        }
        cnt += cc;
    }
    if (cnt != 9 + PorNX() * PorNY())
        return 0;
    else
        return 1;
}

/*****************************************************************************/
/**
 * @brief Load Model header data
 *
 * Load Model Header data (/Module group attributes, plus /Module/temp_bc
 * dataset)
 *
 * Called collectively (all MPI_COMM_WORLD processes)
 *
 * @param file_id [in] HDF5 file identifier
 * @return 1 if successful ; 0 if not
 *****************************************************************************/
static int LoadHeader_h5(hid_t file_id)
{
    int i, j, jl2, ju2;
    int ver;
    int g_nx = PorNX(), g_ny = PorNY();

    /*stk_Add(1, POR_AT, "twolevel: loading the model header");*/
    H5LTget_attribute_int(file_id, "/Module", "TL_VERSION", &ver);
    if (ver != TWOLEVELVERSION) {
        Error(E_ERROR, POR_AT,
              "incompatible version %d of the format (supported version: %d)",
              ver, TWOLEVELVERSION);
        return 0;
    }

    H5LTget_attribute_double(file_id, "/Module", "ATOM_MASS", &A_MASS);
    H5LTget_attribute_double(file_id, "/Module", "A_UL", &g_A_UL);
    H5LTget_attribute_double(file_id, "/Module", "E_UL", &g_E_UL);

    H5LTget_attribute_int(file_id, "/Module", "JL2", &jl2);
    H5LTget_attribute_int(file_id, "/Module", "JU2", &ju2);
    if (jl2 != JL2 || ju2 != JU2) {
        Error(
            E_ERROR, POR_AT,
            "incompatible angular momenta of the levels (%d %d (file) vs %d %d "
            "(code))",
            jl2, ju2, JL2, JU2);
        return 0;
    }

    H5LTget_attribute_double(file_id, "/Module", "GL", &G_GL);
    H5LTget_attribute_double(file_id, "/Module", "GU", &G_GU);
    H5LTget_attribute_double(file_id, "/Module", "TEMP", &g_TEMP_REF);

    double *mdata_hdf;
    mdata_hdf = (double *)malloc(g_nx * g_ny * sizeof(*mdata_hdf));
    if (H5LTread_dataset_double(file_id, "/Module/temp_bc", mdata_hdf) < 0) {
        Error(E_ERROR, POR_AT, "cannot read from file");
        return 0;
    }
    for (j = 0; j < g_ny; j++) {
        for (i = 0; i < g_nx; i++) {
            g_temp[j][i] = mdata_hdf[j * g_nx + i];
        }
    }
    free(mdata_hdf);

    return 1;
}

/*****************************************************************************/
/**
 * @brief Returns size (in bytes) of the node data in a PMD file
 *
 * Size of the node data returned by F_EncodeNodeData() and read by
 * F_DecodeNodeData(); equal to the size of node data in a PMD file
 *
 * @todo Need to think how to calculate this properly with the HDF5 I/O (h5)
 * We can get this information as in SaveNodesSlave_h5
 *****************************************************************************/
static int F_GetNodeDataSize(void) { return NODE_PMD_SIZE; }

/*****************************************************************************/
/**
 * @brief Synchronize global module-dependent variables among MPI procs
 *
 * Called after the grid is loaded in order to synchronize global
 * module-dependent variables among the MPI processes
 *****************************************************************************/
void F_SyncGlobals(void)
{
    if (g_globals_synced)
        return;

    g_max_d = PorGetDomainMax(g_max_d);
    g_min_d = -PorGetDomainMax(-g_min_d);
    g_max_a = PorGetDomainMax(g_max_a);
    g_min_a = -PorGetDomainMax(-g_min_a);

    MakeProfilesPool();
    g_widths_set     = 1;
    g_globals_synced = 1;
}

/*****************************************************************************/
/**
 * @brief Initialization and synchronization module global variables before ESE
 *
 * Called in every Jacobi iteration just before the ESE solutions is started
 * (in ese_SolveGridPrecondESE and always once before all the F_SetDM in the
 * grid) (allows to initialize and synchronize module-dependent global
 * variables such as min&max profile widths)
 *
 * @note no-op in twolevel module
 *****************************************************************************/
static void F_ESEBegins(void) {}

/*****************************************************************************/
/**
 * @brief Initialization and synchronization module global variables after ESE
 *
 * Called in every Jacobi iteration just after solution of ESE (allows to
 * initialize and synchronize module-dependent global variables such as min&max
 * profile widths due to Stark broadening)
 *
 * @note no-op in twolevel module
 *****************************************************************************/
static void F_ESEEnds(void) {}

/*****************************************************************************/
/**
 * @brief Update limits of the profile widths
 *
 * When loading a new node data (F_DecodeNodeData and F_DecodeNodeData_h5()),
 * update limits of the profile widths
 *
 * @param pdata [in,out] Atomic data struct
 *****************************************************************************/
static void UpdateWidths(t_nodedata *pdata)
{
    double wt, nu_d;

    wt      = sqrt(2.0 * C_KB * pdata->temp / (A_MASS * C_AU_MASS));
    nu_d    = FREQ_LINE * wt / C_C;
    g_min_d = nu_d < g_min_d ? (nu_d * 0.99999) : g_min_d;
    g_max_d = nu_d > g_max_d ? (nu_d * 1.00001) : g_max_d;
    g_min_a = pdata->a_voigt < g_min_a ? (pdata->a_voigt * 0.99999) : g_min_a;
    g_max_a = pdata->a_voigt > g_max_a ? (pdata->a_voigt * 1.00001) : g_max_a;

    pdata->nu_d = nu_d;
}

/********************************************************
 ********************************************************
 *                                                      *
 * THE FUNCTIONS VISIBLE FROM THE OUTSIDE FOLLOW BELOW: *
 *                                                      *
 ********************************************************
 ********************************************************/

/*****************************************************************************/
/**
 * @brief The exported module initialization function
 *
 * Initializes module global variables and functions provided by this
 * (twolevel) module
 *
 * @param p_afunc [out] Pointer to struct holding external module's functions
 * @param file [in] File pointer
 * @param node_pmd_size [out] Pointer to size (in bytes) of each node
 *
 * @return 1 if successful ; 0 if not
 *****************************************************************************/
int InitModule(t_afunc *p_afunc, FILE *file, int *node_pmd_size)
{
    int i;

    if (!file) {
        Error(E_ERROR, POR_AT,
              "Sorry, you need to load a PMD file containing the model "
              "atmosphere.");
    }

    allocTemp();

    /* load model header */
    if (!LoadHeader(file)) {
        Error(E_ERROR, POR_AT, "cannot load the module header");
        return 0;
    }

    if (!p_afunc) {
        Error(E_ERROR, POR_AT, "global variables not initialized");
        return 0;
    }

    if (!PorMeshInitialized()) {
        Error(E_ERROR, POR_AT, "grid mesh not initialized");
        return 0;
    }

    g_globals_synced = 0;
    /* initialize the profile width limits: modified once new node data are
     * loaded
     */
    g_widths_set = 0;
    g_max_d      = -1.0;
    g_min_d      = DBL_MAX;

    g_nlower = NLevelDMElements(JL2);
    g_nupper = NLevelDMElements(JU2);
    g_nreal  = NReals(JL2) + NReals(JU2); /*g_nlower + g_nupper;*/
    g_ese_r  = MatrixNew(g_nreal, g_nreal);
    g_rhs_r  = ArrayNew(g_nreal);
    g_ese_c = (t_complex **)Malloc(sizeof(t_complex *) * (g_nlower + g_nupper));
    for (i = 0; i < g_nlower + g_nupper; i++)
        g_ese_c[i] =
            (t_complex *)Malloc(sizeof(t_complex) * (g_nlower + g_nupper));
    g_rhs_c    = (t_complex *)Malloc(sizeof(t_complex) * (g_nlower + g_nupper));
    g_L_factor = C_H * C_H /*NU*NU*NU*NU*/ / (2.0 * M_PI * C_C * C_C) *
                 (JU2 + 1) * B_UL * sqrt(3.0) * ZNAM(1 + (JU2 + JL2) / 2) *
                 mat_Cou6j(2, 2, 0, JU2, JU2, JL2);

    p_afunc->F_Init_p_data        = F_Init_p_data;
    p_afunc->F_Free_p_data        = F_Free_p_data;
    p_afunc->F_EncodeNodeData     = F_EncodeNodeData;
    p_afunc->F_EncodeNodeData_h5  = F_EncodeNodeData_h5;
    p_afunc->F_DecodeNodeData     = F_DecodeNodeData;
    p_afunc->F_DecodeNodeData_h5  = F_DecodeNodeData_h5;
    p_afunc->F_GetNodeDataSize    = F_GetNodeDataSize;
    p_afunc->F_ExternalIllum      = F_ExternalIllum;
    p_afunc->F_SKGeneralDirection = F_SKGeneralDirection;
    p_afunc->F_SK                 = F_SK;
    p_afunc->F_AddFS              = F_AddFS;
    p_afunc->F_GetESE             = F_GetESE;
    p_afunc->F_SolveESE           = F_SolveESE;
    p_afunc->F_GetNDM             = F_GetNDM;
    p_afunc->F_GetPopsPositions   = F_GetPopsPositions;
    p_afunc->F_GetDM              = F_GetDM;
    p_afunc->F_SetDM              = F_SetDM;
    p_afunc->F_GetTemperature     = F_GetTemperature;
    p_afunc->F_GetMagneticVector  = F_GetMagneticVector;
    p_afunc->F_GetRadiation       = F_GetRadiation;
    p_afunc->F_SetRadiation       = F_SetRadiation;
    p_afunc->F_ResetRadiation     = F_ResetRadiation;
    p_afunc->F_IO_WriteHeader     = F_IO_WriteHeader;
    p_afunc->F_IO_WriteHeader_h5  = F_IO_WriteHeader_h5;
    p_afunc->F_SyncGlobals        = F_SyncGlobals;
    p_afunc->F_ESEBegins          = F_ESEBegins;
    p_afunc->F_ESEEnds            = F_ESEEnds;

    /* global lab frequencies */
    MakeFrequencies();

    g_kernel_l = AllocMagneticKernel(JL2, LANDE_L);
    g_kernel_u = AllocMagneticKernel(JU2, LANDE_U);

    *node_pmd_size = F_GetNodeDataSize();

    return 1;
}

/*****************************************************************************/
/**
 * @brief The exported module initialization function
 *
 * Initializes module global variables and functions provided by this
 * (twolevel) module
 *
 * @param p_afunc [out] Pointer to struct holding external module's functions
 * (all these functions start with F_)
 * @param file_id [in] HDF5 file identifier
 * @param [out] node_pmd_size Pointer to size (in bytes) of each node
 *
 * @return 1 if successful ; 0 if not
 * @todo this is almost identical to the binary format one. See if we can
 * combine them
 *****************************************************************************/
int InitModule_h5(t_afunc *p_afunc, hid_t file_id, int *node_pmd_size)
{
    int i;

    if (!file_id) {
        Error(E_ERROR, POR_AT,
              "Sorry, you need to load a PMD file containing the model "
              "atmosphere.");
        return 0;
    }

    allocTemp();

    /* load model header */
    if (!LoadHeader_h5(file_id)) {
        Error(E_ERROR, POR_AT, "cannot load the module header");
        return 0;
    }

    if (!p_afunc) {
        Error(E_ERROR, POR_AT, "global variables not initialized");
        return 0;
    }

    if (!PorMeshInitialized()) {
        Error(E_ERROR, POR_AT, "grid mesh not initialized");
        return 0;
    }

    g_globals_synced = 0;
    /* initialize the profile width limits: modified once new node data are
     * loaded
     */
    g_widths_set = 0;
    g_max_d      = -1.0;
    g_min_d      = DBL_MAX;

    g_nlower = NLevelDMElements(JL2);
    g_nupper = NLevelDMElements(JU2);
    g_nreal  = NReals(JL2) + NReals(JU2); /*g_nlower + g_nupper;*/
    g_ese_r  = MatrixNew(g_nreal, g_nreal);
    g_rhs_r  = ArrayNew(g_nreal);
    g_ese_c = (t_complex **)Malloc(sizeof(t_complex *) * (g_nlower + g_nupper));
    for (i = 0; i < g_nlower + g_nupper; i++)
        g_ese_c[i] =
            (t_complex *)Malloc(sizeof(t_complex) * (g_nlower + g_nupper));
    g_rhs_c    = (t_complex *)Malloc(sizeof(t_complex) * (g_nlower + g_nupper));
    g_L_factor = C_H * C_H /*NU*NU*NU*NU*/ / (2.0 * M_PI * C_C * C_C) *
                 (JU2 + 1) * B_UL * sqrt(3.0) * ZNAM(1 + (JU2 + JL2) / 2) *
                 mat_Cou6j(2, 2, 0, JU2, JU2, JL2);

    p_afunc->F_Init_p_data        = F_Init_p_data;
    p_afunc->F_Free_p_data        = F_Free_p_data;
    p_afunc->F_EncodeNodeData     = F_EncodeNodeData;
    p_afunc->F_EncodeNodeData_h5  = F_EncodeNodeData_h5;
    p_afunc->F_DecodeNodeData     = F_DecodeNodeData;
    p_afunc->F_DecodeNodeData_h5  = F_DecodeNodeData_h5;
    p_afunc->F_GetNodeDataSize    = F_GetNodeDataSize;
    p_afunc->F_ExternalIllum      = F_ExternalIllum;
    p_afunc->F_SKGeneralDirection = F_SKGeneralDirection;
    p_afunc->F_SK                 = F_SK;
    p_afunc->F_AddFS              = F_AddFS;
    p_afunc->F_GetESE             = F_GetESE;
    p_afunc->F_SolveESE           = F_SolveESE;
    p_afunc->F_GetNDM             = F_GetNDM;
    p_afunc->F_GetPopsPositions   = F_GetPopsPositions;
    p_afunc->F_GetDM              = F_GetDM;
    p_afunc->F_SetDM              = F_SetDM;
    p_afunc->F_GetTemperature     = F_GetTemperature;
    p_afunc->F_GetMagneticVector  = F_GetMagneticVector;
    p_afunc->F_GetRadiation       = F_GetRadiation;
    p_afunc->F_SetRadiation       = F_SetRadiation;
    p_afunc->F_ResetRadiation     = F_ResetRadiation;
    p_afunc->F_IO_WriteHeader     = F_IO_WriteHeader;
    p_afunc->F_IO_WriteHeader_h5  = F_IO_WriteHeader_h5;
    p_afunc->F_SyncGlobals        = F_SyncGlobals;
    p_afunc->F_ESEBegins          = F_ESEBegins;
    p_afunc->F_ESEEnds            = F_ESEEnds;

    /* global lab frequencies */
    MakeFrequencies();

    g_kernel_l = AllocMagneticKernel(JL2, LANDE_L);
    g_kernel_u = AllocMagneticKernel(JU2, LANDE_U);

    *node_pmd_size = F_GetNodeDataSize();

    return 1;
}

/*****************************************************************************/
/**
 * @brief The exported module closing function
 *
 * This function will be called by the PORTA library to clean up before it
 * calls the ```dlclose``` function to actually close the twolevel-module
 *
 * @return 1
 *****************************************************************************/
int CloseModule(void)
{
    vgt_PoolClear();
    freeTemp();
    return 1;
}

#endif /* SMODE_FULL */
