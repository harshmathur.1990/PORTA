/*####################################################################
############################## HEADER ################################
######################################################################
#
#  Authors:
#     Tanausú del Pino Alemán (IAC) - TdPA
#     Jiří Štěpán (ASCR)
#     Ángel de Vicente (IAC)
#  Start:
#     05/21/2019
#  Last version:
#     11/11/2020 V0.9.3
#
######################################################################
######################################################################
#
#  Changelog:
#
#     11/11/2020:    V0.9.3 - Bugfix: Missing minus sign in RAf and
#                             RSf in the 3J for the Qr < 0 branch.
#                             Pointed out by Flavio Calvo (TdPA)
#
#     16/09/2020:    V0.9.2 - Bugfix: The call to the Voigt profiles
#                             in CalculateNodeProfiles had the wrong
#                             frequency index for the EXACT_PROFILES
#                             branch of the routine (TdPA)
#                           - Added normalization to the profiles
#                             when EXACT_PROFILES is defined. To skip
#                             this use EXACT_PROFILES_NONORM instead.
#                             The normalization is carried out by the
#                             new ml_VoigtProfileNuNorm routine (TdPA)
#                           - Added a routine specific to compute the
#                             RT coefficients for the gettau
#                             command (TdPA)
#
#     21/01/2020:    V0.9.1 - Removed unused macro (TdPA)
#                           - Revised doxygen text (TdPA)
#
#     13/01/2020:    V0.9.0 - Added local indexing for generalized
#                             TKQ in the header (TdPA)
#                           - Introduced flag for rotation of density
#                             matrix in the node structure (TdPA)
#                           - Simplified ZeemanProfileShift function
#                             and changed inputs (TdPA)
#                           - Added local CalcTKQ function to compute
#                             general TKQ geometry tensors (TdPA)
#                           - Added local rotate_dm function to
#                             rotate the elements of the density
#                             matrix (TdPA)
#                           - Fully changed SKGeneral_HZ to speed it
#                             up and avoid copying memory (TdPA)
#                           - Moved geometry logic in Hanle-Zeeman
#                             to SKGeneralDirection (TdPA)
#                           - Fixed the computation of the magnetic
#                             Kernel (TdPA)
#                           - In matika.c, added memory storage for
#                             6J with memoization (TdPA)
#
#     28/11/2019:    V0.8.0 - Added routine to compute the radiative
#                             transfer coefficients in the Hanle-
#                             Zeeman regime, only called if activated
#                             and if doing only formal solution (TdPA)
#                           - Added EXACT_PROFILES flag with the same
#                             use than in the twolevel module (TdPA)
#                           - Changed some fabs that were being used
#                             with integers to abs (TdPA)
#
#     09/11/2019:    V0.7.0 - Bugfix: I think there was a bug when
#                             skipping the recalculation of the
#                             normalization of profiles in
#                             CalculateNodeProfiles, as one has to be
#                             sure we are still dealing with the same
#                             transition. Made idir_last within
#                             nodes dependent on the transition (TdPA)
#
#     06/25/2019:    V0.6.0 - Update the header comments regarding the
#                             pmd structure (TdPA)
#
#     06/22/2019:    V0.5.0 - Introduced hdf5 I/O (AdV)
#
#     06/21/2019:    V0.4.1 - Simplified Jacobi terms in TA and TS for
#                             the only K=0 particular case (TdPA)
#
#     06/20/2019:    V0.4.0 - Bugfix: The relaxation rates of
#                             collisions were inverted, that is, Cul
#                             for the lower level and Clu for the
#                             upper level (TdPA)
#                           - Added microturbulence to the node
#                             information. Now it is included in the
#                             Doppler widths (TdPA)
#
#     06/19/2019:    V0.3.0 - Bugfix: The rhoKQ in F_GetESE were
#                             ordered differently (-K,-K+1,...,0,...,
#                             K-1,K) to the rest of the code (0,1,-1,
#                             2,-2,...,K,-K) and that was a problem,
#                             obviously... (TdPA)
#                           - Bugfix: In TE, in the sign operation,
#                             the angular momentum of the upper level
#                             was taken twice, instead of once the
#                             upper and once the lower (TdPA)
#
#     05/21/2019:    V0.2.0 - Debugged with Mg II h-k and extensively
#                             documented (TdPA)
#
#     05/17/2019:    V0.2.0 - Significant debugging, SEE had the
#                             transfer rates interchanged between
#                             upper and lower levels. Now it
#                             converges, albeit slower than my 1D
#                             code, so the Lambda operator must be
#                             checked (TdPA)
#
#     05/16/2019:    V0.1.0 - First version to run Jacobi iterations,
#                             but without testing that they are
#                             correct (TdPA)
#
#     05/08/2019:    V0.0.0 - Started to write (TdPA)
#
######################################################################
######################################################################
#
#  Known bugs:
#
######################################################################
######################################################################
#
#  Data:
#
#    Module to solve the problem of generation and transfer of
#  polarized radiation in the Hanle regime (at least regarding
#  the iterative process) for a multi-level atom
#
######################################################################
######################################################################
#
#  doxygen meta-data:
*//**
* @file multilevel.c
* @brief multi-level module for PORTA
* @author Tanausú del Pino Alemán, tanausu@iac.es
* @author Jiří Štěpán, stepan@asu.cas.cz
* @author Ángel de Vicente, angel.de.vicente@iac.es
* @note Orientation quantities (in SKGeneral and F_AddFS) are
*       hard-coded to zero
* @todo See todo in F_GetESE
*//*
#
######################################################################
######################################################################
#
#   Module data format:
#
#  Header data:
#    int*1 version
#    double*1 atomic mass (AMU)
#    int*1 number of levels [atom->nlevel]
#    int*1 number of total transitions [atom->ntran]
#    int*1 number of radiative transitions [atom->rtran]
#    double*atom->nlevel Energy (erg)
#    double*atom->nlevel Lande
#    int*atom->nlevel J*2 (twice angular momentum) [atom->*J2]
#    int*atom->ntran*2 (iu,il) pairs of indexes for transitions
#    double*atom->rtran Aul Einstein coefficients
#    int*atom->rtran number of frequencies for each transition
#    int*atom->rtran number of frequencies for the core of each
#                    transition
#    double*atom->rtran Line width range
#    double*atom->rtran Line core width range
#    double*atom->rtran Reference temperature
#    int*1 Maximum K to consider [g_Kcut]
#    double*NX*NY Bottom layer temperature
#
#  Grid data:
#    double*1 temperature (K)
#    double*1 density of ion (cm^-3)
#    double*3 magnetic field vector (G)
#    double*3 velocity vector (cm/s)
#    double*1 microturbulent velocity (cm/s)
#    for each level i:
#      double*min(g_Kcut^2+2*g_Kcut+1,
#                 atom->J2[i]^2+2*atom->J2[i] + 1)  rho
#    for each radiative transition i:
#      double*9 JKQ[i]
#    for each radiative transition i:
#      double*1 Voigt damping parameter
#    for each transition i:
#      double*1 Cul (s^-1) collisional rate
#    for each level i:
#      double*1 depolarizing collisional rate (s^-1)
#    for each transition i:
#      double*1 continuum absorptivity (cm^-1)
#    for each transition i:
#      double*1 continuum emissivity (cgs)
#
#   Multipolar components are always real and follow the following
# order: rho00, rho10, Re[rho11], Im[rho11], rho20, Re[rho21], ...
#
######################################################################
######################################################################
####################################################################*/

/* Includes */
#include "portal/portal.h"
#include "portal/pstream.h"
#include <float.h>
#include <math.h>
#include <memory.h>
#include <stdlib.h>

#ifdef SMODE_FULL /**< @brief This module should only be used with             \
                              full Stokes in its current                       \
                              implementation */

// HANLE_ZEEMAN_FS defined by default. Can be deactivated via ./configure
// --no_hz #define HANLE_ZEEMAN_FS /**< @brief Activates Hanle-Zeeman in formal
// solution*/

#if defined(HANLE_ZEEMAN_FS)
/* Local aliases for general components of the T^K_Q(i) tensors */
#define ZNTKQ    19
#define ZT00I_RE 0
#define ZT20I_RE 1
#define ZT21I_RE 2
#define ZT21I_IM 3
#define ZT22I_RE 4
#define ZT22I_IM 5
#define ZT20Q_RE 6
#define ZT21Q_RE 7
#define ZT21Q_IM 8
#define ZT22Q_RE 9
#define ZT22Q_IM 10
#define ZT20U_RE 11
#define ZT21U_RE 12
#define ZT21U_IM 13
#define ZT22U_RE 14
#define ZT22U_IM 15
#define ZT10V_RE 16
#define ZT11V_RE 17
#define ZT11V_IM 18
#endif

/* Defines */
#define MULTILEVELVERSION 1 /**< @brief Version of the module */
#define nu_resol                                                               \
    C_C * 1e-3 /**< @brief Minimum distance between two                        \
                 adjacent frequency nodes */
#define ZNAM(i) (abs(i) % 2 ? -1.0 : 1.0) /**< @brief macro (-1)^i */

/* Atomic data */
typedef struct m_Atom_class {
    /* PMD file data*/
    double mass;   /**< @brief Atomic mass [AMU]*/
    int    nlevel; /**< @brief Number of levels*/
    int    ntran;  /**< @brief Number of transitions*/
    int    rtran;  /**< @brief Number of radiative transitions*/
    int    Kcut;   /**< @brief Maximum K multipolar component
                               to take into account*/
    double *E;     /**< @brief Energy of levels [erg]*/
    double *gL;    /**< @brief Land\'e factor of levels*/
    int *   J2;    /**< @brief Twice the angular momentum of the levels*/
    int *   iu;    /**< @brief Index of the upper level of each
                               transition*/
    int *il;       /**< @brief Index of the lower level of each
                               transition*/
    double *Aul;   /**< @brief Einstein coefficient for
                               espontaneous emission [s^-1]*/
    int *nfreq;    /**< @brief Total number of frequencies for each
                               radiative transition*/
    int *nfreqc;   /**< @brief Number of frequencies for the core of
                               each radiative transition*/
    double *Dw;    /**< @brief Total Doppler widths to cover with each
                               radiative transition*/
    double *Dwc;   /**< @brief Doppler widths to cover with the core of
                               each radiative transition*/
    double *rDw;   /**< @brief Temperature for the transition. It
                               determines the conversion from Doppler
                               widths to frequencies*/
    /* runtime data*/
    int rdim;     /**< @brief Dimensions of the statistical equilibrium
                              equations*/
    int  jdim;    /**< @brief Dimension of the radiation field tensors*/
    int *i0;      /**< @brief First index of frequency for each radiative
                              transition*/
    int *i1;      /**< @brief Last index of frequency for each radiative
                              transition*/
    int    MaxK;  /**< @brief Maximum value of K for the atom*/
    int *  maxK;  /**< @brief Maximum value of K for each level*/
    int ***i_rho; /**< @brief Indexing of the rho^K_Q vector given
                              level, K, and Q*/
    int ***i_J;   /**< @brief Indexing of the radiation field tensors
                              given the transition, K, and Q*/
    double *dEc;  /**< @brief Constant exponent quantity to transform
                              upper->lower collisions into
                              lower->upper collisions [K]*/
    double *glu;  /**< @brief Ratio between statistical weights of
                              the lower and upper levels of each
                              transition*/
    double *Bul;  /**< @brief Einstein coefficient for stimulated
                              emission for each radiative
                              transition [cgs]*/
    double *Blu;  /**< @brief Einstein coefficient for absorption for
                              each radiative transition [cgs]*/
    double *nu;   /**< @brief Frequency of each radiative transition
                              [Hz]*/
    double *RDw;  /**< @brief Frequency Doppler width for each
                              radiative transition [Hz]*/
/* Hanle Zeeman magnetic data */
#if defined(HANLE_ZEEMAN_FS)
    int *nM;  /**< @brief Number of magnetic components per
                          atomic level*/
    int **M2; /**< @brief Magnetic quantum numbers */
#endif

} m_Atom;

/* Grid format */
typedef struct m_Node_class {
    /* PMD file data */
    double T;          /**< @brief Temperature [K]*/
    double N;          /**< @brief Number density of ion [cm^-3]*/
    double Bx, By, Bz; /**< @brief Cartesian components of the
                                   magnetic field vector [G]*/
    double vx, vy, vz; /**< @brief Cartesian components of the
                                   velocity vector [cm/s]*/
    double  vmi;       /**< @brief Microturbulent velocity [cm/s]*/
    double *rho;       /**< @brief Density matrix components*/
    double *J;         /**< @brief Radiation field tensors [erg/cm^2/s/Hz]*/
    double *a_voigt;   /**< @brief Damping parameter of Voigt profile*/
    double *Cul;       /**< @brief Inelastic collisional rate upper->lower
                                   [s^-1]*/
    double *DK;        /**< @brief Depolarizing elastic collisional rate
                                   for each level [s^-1]*/
    double *eta_c;     /**< @brief Absorptivity of the continuum at the
                                   line center frequency [cm^-1]*/
    double *eps_c;     /**< @brief Thermal emissivity of the continuum at
                                   the line center frequency
                                   [erg/cm^3/s/str/Hz]*/
    /* runtime data */
    int *idir_last;  /**< @brief Last direction index of ray used as
                                 an indicator of wether it is necessary
                                 to calculate profile normalization*/
    double *nu_d;    /**< @brief Doppler width of each radiative
                                 transition*/
    double *psi_eta; /**< @brief Lambda operator for each radiative
                                 transition*/
    double *pnorm;   /**< @brief Inverse of the profile frequency
                                 integral*/
/* Hanle Zeeman rotation flag */
#if defined(HANLE_ZEEMAN_FS)
    int rotated; /**< @brief Flag to indicate if the rhoKQ tensors
                             have been rotated to the magnetic
                             field reference frame*/
#endif

} m_Node;

/* global variables */
static int g_Kcut;               /**< @brief Maximum K multipole to take into
                                             account*/
static double **g_temp;          /**< @brief Temperature of the bottom plane
                                             [K]*/
static m_Atom  g_atom;           /**< @brief Atomic data*/
static double *g_max_d;          /**< @brief Maximum Doppler width for each
                                             radiative transition*/
static double *g_min_d;          /**< @brief Minimum Doppler width for each
                                             radiative transition*/
static double *g_max_a;          /**< @brief Maximum damping parameter for
                                             each radiative transition*/
static double *g_min_a;          /**< @brief Minimum damping parameter for
                                             each radiative transition*/
static int *g_Qind;              /**< @brief Q values by index*/
static int  g_nodesize;          /**< @brief Size of the node in the pmd*/
static int  g_widths_set = 0;    /**< @brief Flag to indicate that the
                                             g_min/max_d/a has been
                                             calculated*/
static int g_globals_synced = 0; /**< @brief Flag to indicate that
                                             the grid is loaded */
static double C_H4PI = C_H / 4. / M_PI; /**< @brief Constant factor in
                                                    radiative transfer
                                                    coefficients */
static double C_H4PIS3 = C_H * 1.732050807568877 / 4. / M_PI; /**< @brief
                                                      Constant factor in
                                                      radiative transfer
                                                      coefficients */
static double g_Prof[2]; /**< @brief line profile, Voigt [0] and
                                     Faraday-Voigt [1]*/

/****************************************************************************/
/**
 * Function to compare two double precision numbers. It returns 1 if
 * the first input is larger, and -1 otherwise
 *
 * @brief Comparison function for qsort()
 * @param [in] a First element of comparison
 * @param [in] b Second element of comparison
 * @return int 1 if a>b, -1 if b<=a
 *
 ****************************************************************************/
int compare(const void *a, const void *b)
{
    double fa = *(const double *)a;
    double fb = *(const double *)b;
    return (fa < fb) ? -1 : (fa > fb);
}

/****************************************************************************/
/**
 * Compute the Doppler width of every radiative transition in the node
 * and updates the g_min_d/a and g_max_d/a limits of Doppler widths and
 * damping parameters
 *
 * @brief Update limits of the profile widths and damping parameters
 * @param [in] p_data Node structure
 * @return void
 *
 ****************************************************************************/
static void UpdateWidths(m_Node *pdata)
{
    double wt, nu_d;

    for (int i = 0; i < g_atom.rtran; i++) {
        wt         = 2.0 * C_KB * pdata->T / (g_atom.mass * C_AU_MASS);
        nu_d       = g_atom.nu[i] * sqrt(wt + pdata->vmi * pdata->vmi) / C_C;
        g_min_d[i] = nu_d < g_min_d[i] ? (nu_d * 0.99999) : g_min_d[i];
        g_max_d[i] = nu_d > g_max_d[i] ? (nu_d * 1.00001) : g_max_d[i];
        g_min_a[i] = pdata->a_voigt[i] < g_min_a[i]
                         ? (pdata->a_voigt[i] * 0.99999)
                         : g_min_a[i];
        g_max_a[i] = pdata->a_voigt[i] > g_max_a[i]
                         ? (pdata->a_voigt[i] * 1.00001)
                         : g_max_a[i];
        pdata->nu_d[i] = nu_d;
    }
}

/****************************************************************************/
/**
 * Creates a frequency axis for a transition given the number of
 * frequencies (total and for the core), Doppler width to cover (total
 * and for the core), frequency of the line, and Doppler width
 *
 * @brief Builds the laboratory frame frequency axis for a single
 *        transition
 * @param [in] n Total number of frequencies
 * @param [in] nc Number of frequencies for the core region
 * @param [in] nu0 Resonance frequency of the transition [Hz]
 * @param [in] nu_d Doppler width of the transition [Hz]
 * @param [in] hw Total Doppler width to cover at each side of the
 *                resonance
 * @param [in] hwc Doppler width to cover in the core region at each
 *                 side of the resonance
 * @return double* Frequency axis
 *
 ****************************************************************************/
double *ml_SuggestLineFrequencies(int n, int nc, double nu0, double nu_d,
                                  double hw, double hwc)
{
    double  dx, v, dv;
    double *x;
    int     ii0, hn, hnc;

    /* Allocate output frequency */
    x = (double *)malloc(n * sizeof(double));

    /* Divide frequencies by two */
    hn  = n / 2;
    hnc = nc / 2;

    /* Step for core part */
    dx = hwc / ((double)hnc);

    /* Linear part */
    ii0    = hn;
    x[ii0] = 0.0;
    for (int i = 1; i <= hnc; i++) {
        x[ii0 + i] = x[ii0 + i - 1] + dx;
        x[ii0 - i] = x[ii0 - i + 1] - dx;
    }

    /* Logarithmic part */
    v  = log10(hwc);
    dv = log10(hw) - v;
    dx = dv / ((double)(hn - hnc));
    for (int i = hnc + 1; i <= hn; i++) {
        v          = v + dx;
        x[ii0 + i] = pow(10.0, v);
        x[ii0 - i] = -x[ii0 + i];
    }

    /* Convert into real frequencies */
    for (int i = 0; i < n; i++) {
        x[i] = x[i] * nu_d + nu0;
    }

    return x;
}

/****************************************************************************/
/**
 * Build the laboratory frequency axis for the given atom
 *
 * @brief Builds the laboratory frame frequency axis
 * @return void
 *
 ****************************************************************************/
static void MakeFrequencies()
{
    int *   flags;
    double *Freqs;
    double *freqs;
    double *f0;
    double *f1;
    int     i0 = 0;
    int     nfreq, i1;

    /* Allocate frequency limits for transitions */
    f0 = (double *)malloc(g_atom.rtran * sizeof(double));
    f1 = (double *)malloc(g_atom.rtran * sizeof(double));

    /* Count potential number of frequencies and allocate memory */
    nfreq = 0;
    for (int i = 0; i < g_atom.rtran; i++) {
        nfreq += g_atom.nfreq[i];
    }
    Freqs = (double *)malloc(nfreq * sizeof(double));
    flags = (int *)malloc(nfreq * sizeof(int));

    /* For each transition add relevant frequencies */
    for (int i = 0; i < g_atom.rtran; i++) {
        /* Get frequency section from voigt.c */
        freqs = ml_SuggestLineFrequencies(g_atom.nfreq[i], g_atom.nfreqc[i],
                                          g_atom.nu[i], g_atom.RDw[i],
                                          g_atom.Dw[i], g_atom.Dwc[i]);

        /* Store the frequency limits for the transition */
        f0[i] = freqs[0];
        f1[i] = freqs[g_atom.nfreq[i] - 1];

        /* Add frequencies to global list */
        i1 = i0 + g_atom.nfreq[i] - 1;
        for (int i = i0; i <= i1; i++) {
            int j    = i - i0;
            Freqs[i] = freqs[j];
        }
        i0 = i1 + 1;

        /* Free temporal per line frequency */
        free(freqs);
    }

    /* Sort frequencies */
    qsort(Freqs, nfreq, sizeof(double), compare);

    /* Keep frequencies that are not overlapping from the resolution
       definition */
    {
        /* Flag frequencies to keep */
        int    Nfreq = 0;
        double lfreq = 0.;
        for (int j = 0; j < nfreq; j++) {
            if ((Freqs[j] - lfreq) > nu_resol) {
                flags[j] = 1;
                Nfreq++;
                lfreq = Freqs[j];
            } else {
                flags[j] = 0;
            }
        }

        /* Allocate final axis */
        freqs = (double *)malloc(Nfreq * sizeof(double));

        /* Build final axis */
        int i = -1;
        for (int j = 0; j < nfreq; j++) {
            if (flags[j]) {
                i++;
                freqs[i] = Freqs[j];
            }
        }

        /* Set frequency axis */
        PorSetFreqs(Nfreq, freqs);

        /* Store the beginning and ending indexes for each
           radiative transition */
        for (int j = 0; j < g_atom.rtran; j++) {
            double lmin0 = 1e99;
            double lmin1 = 1e99;
            for (int k = 0; k < Nfreq; k++) {
                if (fabs(f0[j] - freqs[k]) < lmin0) {
                    g_atom.i0[j] = k;
                    lmin0        = fabs(f0[j] - freqs[k]);
                }
                if (fabs(f1[j] - freqs[k]) < lmin1) {
                    g_atom.i1[j] = k;
                    lmin1        = fabs(f1[j] - freqs[k]);
                } else {
                    break;
                }
            }
        }
    }

    /* Free allocations */
    free(Freqs);
    free(flags);
    free(freqs);
    free(f0);
    free(f1);
}

/****************************************************************************/
/**
 * Computes Zeeman shift for a transition component given the magnetic
 * number and indexes of the levels involved and the magnetic field
 *
 * @brief Shift of the Zeeman component of the line due to magnetic
 *        field B
 * @param [in] gl Lower level Lande factor
 * @param [in] gu Upper level Lande factor
 * @param [in] Ml2 Twice the lower level magnetic number
 * @param [in] Mu2 Twice the upper level magnetic number
 * @param [in] B Magnetic field strength [G]
 * @return double Frequency shift
 *
 ****************************************************************************/
double ZeemanProfileShift(double gl, double gu, int Ml2, int Mu2, double B)
{
    return C_BOHRMAG * B * 0.5 * (gu * ((double)Mu2) - gl * ((double)Ml2)) /
           C_H;
}

/****************************************************************************/
/**
 * Creates the pool of profiles for all the radiative transitions
 *
 * @brief Creates pool of Voigt profiles
 * @return static void
 *
 ****************************************************************************/
static void MakeProfilesPool(void)
{
    const double *Freqs;
    double *      freqs;

    /* Request frequency axis */
    Freqs = PorFreqs();

    /* Clear current pool */
    vgt_PoolClear();

    for (int i = 0; i < g_atom.rtran; i++) {
        /* Message into stack */
        stk_Add(0, POR_AT,
                "initializing profiles pool: mind=%e, maxd=%e, "
                "mina=%e, maxa=%e\n",
                g_min_d[i], g_max_d[i], g_min_a[i], g_max_a[i]);

        /* Deal with line limits and get local frequencies*/
        int i0    = g_atom.i0[i];
        int i1    = g_atom.i1[i];
        int nfreq = i1 - i0 + 1;
        freqs     = (double *)malloc(nfreq * sizeof(double));
        for (int j = 0; j < nfreq; j++) {
            freqs[j] = Freqs[i0 + j];
        }

        /* Control underflow of damping parameter */
        if (g_max_a[i] == 0)
            g_max_a[i] = 1e-10;

        /* Build Pool for this line */
        vgt_PoolBuild(i, nfreq, freqs, g_atom.nu[i], g_min_d[i], g_max_d[i],
                      g_min_a[i], g_max_a[i], 1);

        /* Free freqs */
        free(freqs);
    }
}

/****************************************************************************/
/**
 * Compute the normalization of the Voigt profile and the value at a given
 * requested frequency for a given node, transition, and shift.
 *
 * @brief Calculate normalized exact line profiles in a given node in the
 *        laboratory frame
 * @param [in] p_node Node data
 * @param [in] itran Transition index
 * @param [in] shift Frequency shift
 * @param [in] ifreq Frequency index
 * @return void
 *
 ****************************************************************************/
static void ml_VoigtProfileNuNorm(t_node *p_node, int itran, double shift,
                                  int ifreq)
{
    m_Node *      p = (m_Node *)p_node->p_data;
    t_complex     pr;
    double        nu = g_atom.nu[itran] + shift;
    double        W;
    const double *freqs;

    /* Get frequency array */
    freqs = PorFreqs();

    /* Initialize */
    p->pnorm[itran] = 0e0;

    /* First point */

    /* Weight */
    W = freqs[g_atom.i0[itran] + 1] - freqs[g_atom.i0[itran]];

    /* Compute profile */
    pr = vgt_VoigtProfileNu(nu, freqs[g_atom.i0[itran]], p->nu_d[itran],
                            p->a_voigt[itran]);

    /* Add to norm */
    p->pnorm[itran] += pr.re * W;

    /* Is the requested? */
    if (ifreq == g_atom.i0[itran]) {
        g_Prof[0] = pr.re;
        g_Prof[1] = pr.im;
    }

    /* For each frequency in the transition which is not boundary, compute
       profile and add to norm */
    for (int jfreq = g_atom.i0[itran] + 1; jfreq < g_atom.i1[itran]; jfreq++) {
        W = freqs[jfreq + 1] - freqs[jfreq - 1];

        /* Compute profile */
        pr = vgt_VoigtProfileNu(nu, freqs[jfreq], p->nu_d[itran],
                                p->a_voigt[itran]);

        /* Add to norm */
        p->pnorm[itran] += pr.re * W;

        /* Is the requested? */
        if (ifreq == jfreq) {
            g_Prof[0] = pr.re;
            g_Prof[1] = pr.im;
        }
    }

    /* Last point */

    /* Weight */
    W = freqs[g_atom.i1[itran]] - freqs[g_atom.i1[itran] - 1];

    /* Compute profile */
    pr = vgt_VoigtProfileNu(nu, freqs[g_atom.i1[itran]], p->nu_d[itran],
                            p->a_voigt[itran]);

    /* Add to norm */
    p->pnorm[itran] += pr.re * W;

    /* Is the requested? */
    if (ifreq == g_atom.i1[itran]) {
        g_Prof[0] = pr.re;
        g_Prof[1] = pr.im;
    }

    /* Reverse the norm constant */
    p->pnorm[itran] = 2e0 / p->pnorm[itran];
}

/****************************************************************************/
/**
 * Compute the profile for a given node, transition, direction, and
 * frequency. It initialized the pool of profiles on its first call
 *
 * @brief Calculate the line profiles in a given node in the laboratory
 *        frame
 * @param [in] p_node Node data
 * @param [in] itran Transition index
 * @param [in] ifreq Frequency index
 * @param [in] dir Director cosines for the propagation direction
 * @param [in] idir Last direction for which this function was called
 * @param [in] comoving Forces to ignore the Doppler shift
 * @return void
 *
 ****************************************************************************/
static void CalculateNodeProfiles(t_node *p_node, int itran, int ifreq,
                                  const t_vec_3d *dir, int idir, int comoving)
{
    m_Node *p     = (m_Node *)p_node->p_data;
    double  shift = 0.0;

/* Ignore normalization */
#if defined(EXACT_PROFILES_NONORM)

    int       jfreq = ifreq + g_atom.i0[itran];
    t_complex pr;

    if (dir) {
        shift = (dir->x * p->vx + dir->y * p->vy + dir->z * p->vz) *
                g_atom.nu[itran] / C_C;
    }

    /* Compute profile */
    pr = vgt_VoigtProfileNu(g_atom.nu[itran] + shift, PorFreqs()[jfreq],
                            p->nu_d[itran], p->a_voigt[itran]);

    /* Get real and imaginary parts */
    g_Prof[0] = pr.re;
    g_Prof[1] = pr.im;

    /* Return */
    return;

/* Compute exact Voigt profiles */
#elif defined(EXACT_PROFILES)

    int jfreq = ifreq + g_atom.i0[itran];

    if (dir) {
        shift = (dir->x * p->vx + dir->y * p->vy + dir->z * p->vz) *
                g_atom.nu[itran] / C_C;
    }

    /* No need to renormalize */
    if (idir >= 0 && p->idir_last[itran] >= 0 && p->idir_last[itran] == idir) {
        t_complex pr;

        /* Compute profile */
        pr = vgt_VoigtProfileNu(g_atom.nu[itran] + shift, PorFreqs()[jfreq],
                                p->nu_d[itran], p->a_voigt[itran]);

        /* Get real and imaginary parts */
        g_Prof[0] = pr.re;
        g_Prof[1] = pr.im;

    } else {
        ml_VoigtProfileNuNorm(p_node, itran, shift, ifreq);
        p->idir_last[itran] = idir;
    }

    /* Normalize */
    g_Prof[0] *= p->pnorm[itran];
    g_Prof[1] *= p->pnorm[itran];

    /* Return */
    return;

/* Get Voigt profiles from profile pool */
#else

    if (!g_widths_set) {
        MakeProfilesPool();
        g_widths_set = 1;
    }

    /* If there is a director vector */
    if (dir) {
        shift = (dir->x * p->vx + dir->y * p->vy + dir->z * p->vz) *
                g_atom.nu[itran] / C_C;
    }

    /* Check if re-normalization is not needed */
    if (comoving || shift == 0.0) {
        /* the first global line frequency is N_CONT_FREQ */
        vgt_PoolGetProfile(itran, ifreq, p->nu_d[itran], p->a_voigt[itran], 0.0,
                           g_Prof, 0);
    }
    /* Need of recalculation of the profiles normalization? */
    else {
        /* We know the normalization factor from the previous steps;
           idir == IDIR_GENDIR (-1) or a quadrature (see def.h) */
        if (p->idir_last[itran] == idir) {
            vgt_PoolGetProfile(itran, ifreq, p->nu_d[itran], p->a_voigt[itran],
                               shift, g_Prof, 0);
            g_Prof[0] *= p->pnorm[itran];
            g_Prof[1] *= p->pnorm[itran];
        }
        /* We need to calculate normalization of the profiles
         idir = IDIR_UNDEF (-2) (see def.h) */
        else {
            p->pnorm[itran] =
                vgt_PoolGetProfile(itran, ifreq, p->nu_d[itran],
                                   p->a_voigt[itran], shift, g_Prof, 1);
            g_Prof[0] *= p->pnorm[itran];
            g_Prof[1] *= p->pnorm[itran];
            p->idir_last[itran] = idir;
        }
    }
#endif
}

#if defined(HANLE_ZEEMAN_FS)

/****************************************************************************/
/**
 * Compute the geometrical tensors with the general expresions (the
 * routine in directions.c assumes gamma=0)
 *
 * @brief Function to compute geometrical tensors for three general
 *        propagation angles
 * @param [in] sin of polar angle of the propagation with respect to
 *             the reference axis
 * @param [in] cos of polar angle of the propagation with respect to
 *             the reference axis
 * @param [in] sin of azimuth angle of the propagation with respect to
 *             the reference axis
 * @param [in] cos of azimuth angle of the propagation with respect to
 *             the reference axis
 * @param [in] sin of angle of the polarization reference with respect
 *             to the meridian
 * @param [in] cos of angle of the polarization reference with respect
 *             to the meridian
 * @param [out] tkq geometrical tensors
 * @return void
 *
 ****************************************************************************/
void CalcTKQ(double st, double ct, double sa, double ca, double sy, double cy,
             double tkq[ZNTKQ])
{
    double c2a = ca * ca - sa * sa, s2a = 2.0 * sa * ca;
    double c2y = cy * cy - sy * sy, s2y = 2.0 * cy * sy;
    double cctp1 = (1.0 + ct * ct);
    double cct = ct * ct, sst = st * st;
    double x, y, z;

    /* T00(I).re */
    tkq[ZT00I_RE] = 1.0;
    /* T20(I).re */
    tkq[ZT20I_RE] = 1.060660171779821287 * cct - 3.535533905932737622e-1;
    /* T21(I).re & T21(I).im */
    x             = -8.660254037844386468e-1 * ct * st;
    tkq[ZT21I_RE] = x * ca;
    tkq[ZT21I_IM] = x * sa;
    /* T22(I).re & T22(I).im */
    x             = 4.330127018922193234e-1 * sst;
    tkq[ZT22I_RE] = x * c2a;
    tkq[ZT22I_IM] = x * s2a;

    /* T20(Q).re */
    tkq[ZT20Q_RE] = -1.060660171779821287 * sst * c2y;
    /* T21(Q).re & T21(Q).im */
    x             = -8.660254037844386468e-1 * st;
    y             = c2y * ct;
    tkq[ZT21Q_RE] = x * (y * ca - s2y * sa);
    tkq[ZT21Q_IM] = x * (y * sa + s2y * ca);
    /* T22(Q).re & T22(Q).im */
    x             = -4.330127018922193234e-1;
    y             = c2y * cctp1;
    z             = 2.0 * s2y * ct;
    tkq[ZT22Q_RE] = x * (y * c2a - z * s2a);
    tkq[ZT22Q_IM] = x * (y * s2a + z * c2a);

    /* T20(U).re */
    tkq[ZT20U_RE] = 1.060660171779821287 * sst * s2y;
    /* T21(U).re, T21(U).im */
    x             = 8.660254037844386468e-1 * st;
    y             = s2y * ct;
    tkq[ZT21U_RE] = x * (y * ca + c2y * sa);
    tkq[ZT21U_IM] = x * (y * sa - c2y * ca);
    /* T22(U).re, T22(U).im */
    x             = 4.330127018922193234e-1;
    y             = s2y * cctp1;
    z             = 2.0 * c2y * ct;
    tkq[ZT22U_RE] = x * (y * c2a + z * s2a);
    tkq[ZT22U_IM] = x * (y * s2a - z * c2a);

    /* T10(V).re */
    tkq[ZT10V_RE] = 1.224744871391589049 * ct;
    /* T11(V).re & T11(V).im */
    x             = -8.660254037844386468e-1 * st;
    tkq[ZT11V_RE] = x * ca;
    tkq[ZT11V_IM] = x * sa;
}

/****************************************************************************/
/**
 * Function that rotates the rhoKQ tensors to or from the magnetic
 * field reference frame, given the node and the rotation angles
 *
 * @brief Function to change the density matrix reference system
 * @param [inout] p_node Node data
 * @param [in] alphaB angle of rotation
 * @param [in] betaB angle of rotation
 * @param [in] gammaB angle of rotation
 * @param [in] gammaB angle of rotation
 * @return void
 *
 ****************************************************************************/
static void rotate_dm(t_node *p_node, double alphaB, double betaB,
                      double gammaB)
{
    m_Node *    p;
    int         nQ, Ri;
    t_complex **DKQ[3];
    t_complex   rhoKQa[3];
    t_complex   rhoKQ[5];

    /* Get pointer to node data */
    p = (m_Node *)p_node->p_data;

    /* Get rotation matrices */
    DKQ[1] = dir_GetDKRot(1, -alphaB, -betaB, -gammaB);
    DKQ[2] = dir_GetDKRot(2, -alphaB, -betaB, -gammaB);

    /* For every level */
    for (int i = 0; i < g_atom.nlevel; i++) {
        /* For every multipole up to K=2 */
        for (int K = 1; K <= MIN2(2, g_atom.maxK[i]); K++) {
            /* Initial index for this K */
            Ri = g_atom.i_rho[i][K][0];

            /* Number of Q components for given K */
            nQ = 2 * K + 1;

            /* Translate to complex */

            /* Q == 0 */
            rhoKQ[K].re = p->rho[Ri];
            rhoKQ[K].im = 0.0;

            /* Signs for Q negative */
            double sgre = 1.0;
            double sgim = -1.0;

            /* For each Q>0 */
            for (int Q = 1; Q <= K; Q++) {
                int im = Ri + 2 * Q;
                int re = im - 1;

                /* Invert signs */
                sgre = -sgre;
                sgim = -sgim;

                /* Q negative */
                rhoKQ[K - Q].re = p->rho[re] * sgre;
                rhoKQ[K - Q].im = p->rho[im] * sgim;

                /* Q positive */
                rhoKQ[K + Q].re = p->rho[re];
                rhoKQ[K + Q].im = p->rho[im];
            }

            t_complex a;

            /* For each non-negative Q */
            for (int Q = 0; Q <= K; Q++) {
                rhoKQa[Q].re = 0.0;
                rhoKQa[Q].im = 0.0;
                for (int Q1 = 0; Q1 < nQ; Q1++) {
                    CLX_MUL(a, DKQ[K][K + Q][Q1], rhoKQ[Q1]);
                    rhoKQa[Q].re += a.re;
                    rhoKQa[Q].im += a.im;
                }
            }

            /* Put it back in node structure */

            /* Q == 0 */
            p->rho[Ri] = rhoKQa[0].re;

            /* For each Q>0 */
            for (int Q = 1; Q <= K; Q++) {
                int im = Ri + 2 * Q;
                int re = im - 1;

                /* Q positive */
                p->rho[re] = rhoKQa[Q].re;
                p->rho[im] = rhoKQa[Q].im;
            }
        } /* For every K>0 */
    }     /* For every level */

    /* Free DKQ */
    MatrixFreeC(3, 3, DKQ[1]);
    MatrixFreeC(5, 5, DKQ[2]);
}

/****************************************************************************/
/**
 * Function that actually computes the source function and absorption
 * matrix given the node, the direction, and the frequency in the
 * Hanle-Zeeman regime
 *
 * @brief Computes radiation transfer coefficients in Hanle-Zeeman
 *        regime
 * @param [in] p_node Node data
 * @param [in] tkq Geometrical tensors in the magnetic reference frame
 * @param [in] dir Director cosines of the propagation direction
 * @param [in] idir Last direction for which the absorption profiles
 *                  were computed
 * @param [in] ifreq Frequency index for which the radiation transfer
 *                   coefficients are requested
 * @param [in] modB Magnetic field module
 * @param [out] sf Source function
 * @param [out] kmtx Absorption matrix
 * @return void
 *
 ****************************************************************************/
static void SKGeneral_HZ(t_node *p_node, double sf[NSTOKES], double kmtx[NKMTX],
                         double tkq[ZNTKQ], const t_vec_3d *dir, int idir,
                         int ifreq, double modB)
{
    m_Node *      p;
    int           nc, il, iu, sign, signla1, signua1;
    int           signla3, signua3, signua2, signla2;
    int           Ri, nQ;
    int           flag[g_atom.rtran];
    int           RR, II;
    double        Vdir, dnuB, nu0;
    double        eta[4], eps[4], rho[3], etal[4], rhol[3], etau[4], rhou[3];
    double        Kr21, RPre, RPim;
    double        J31, J31K, J32, J33, J3, fl, fu;
    static double fact;
    t_complex     Phi;
    const double *freqs;

    /* Initializations */
    p     = (m_Node *)p_node->p_data;
    freqs = PorFreqs();
    for (int i = 0; i < 4; i++) {
        eta[i] = 0.0;
        eps[i] = 0.0;
    }
    for (int i = 0; i < 3; i++) {
        rho[i] = 0.0;
    }
    sf[STOKES_I] = sf[STOKES_Q] = sf[STOKES_U] = sf[STOKES_V] = kmtx[ETA_I] =
        kmtx[ETA_Q] = kmtx[ETA_U] = kmtx[ETA_V] = kmtx[RHO_Q] = kmtx[RHO_U] =
            kmtx[RHO_V]                                       = 0.0;

    /* Continuum */
    nc = 0;
    for (int itran = 0; itran < g_atom.rtran; itran++) {
        /* Check if frequency in this transition */
        if (ifreq >= g_atom.i0[itran] && ifreq <= g_atom.i1[itran]) {
            flag[itran] = 0;
            nc += 1;
        } else {
            flag[itran] = 1;
        }
    }

    /* If more than one transition, average continuums */
    if (nc > 1) {
        int    i0 = -1, i1 = -1;
        double x0 = 0., x1 = 1e100;
        for (int itran = 0; itran < g_atom.rtran; itran++) {
            if (flag[itran]) {
                continue;
            }

            if (g_atom.nu[itran] < freqs[ifreq] && g_atom.nu[itran] > x0) {
                x0 = g_atom.nu[itran];
                i0 = itran;
            } else if (g_atom.nu[itran] > freqs[ifreq] &&
                       g_atom.nu[itran] < x1) {
                x1 = g_atom.nu[itran];
                i1 = itran;
            }
        }

        /* Interpolate */
        if (i0 < 0) {
            eta[0] = p->eta_c[i1];
            eps[0] = p->eps_c[i1];
        } else if (i1 < 0) {
            eta[0] = p->eta_c[i0];
            eps[0] = p->eps_c[i0];
        } else {
            eta[0] = p->eta_c[i0] + (p->eta_c[i1] - p->eta_c[i0]) *
                                        (freqs[ifreq] - x0) / (x1 - x0);
            eps[0] = p->eps_c[i0] + (p->eps_c[i1] - p->eps_c[i0]) *
                                        (freqs[ifreq] - x0) / (x1 - x0);
        }
    }

    /* Just one transition */
    else if (nc == 1) {
        for (int itran = 0; itran < g_atom.rtran; itran++) {
            if (flag[itran]) {
                continue;
            }

            eta[0] = p->eta_c[itran];
            eps[0] = p->eps_c[itran];
            break;
        }
    }
    /* No transition */
    else {
        kmtx[ETA_I] = VACUUM_OPACITY;
        return;
    }

    /* Get Doppler shift */
    Vdir = (dir->x * p->vx + dir->y * p->vy + dir->z * p->vz) / C_C;

    /* For each transition */
    for (int itran = 0; itran < g_atom.rtran; itran++) {
        /* Check if frequency in this transition */
        if (flag[itran]) {
            continue;
        }

        /* Simplify indexes for levels */
        il = g_atom.il[itran];
        iu = g_atom.iu[itran];

        /* Common factors */

        /* Get sign for emission (and partial sign for absorption) */
        signla3 = 2 + g_atom.J2[il];
        signua3 = 2 + g_atom.J2[iu];

        /* Get multiplicative factor */
        fact = C_H4PIS3 * g_atom.nu[itran] * p->N;

        /* Non-magnetic frequency with Doppler shift */
        nu0 = g_atom.nu[itran] * (1.0 + Vdir);

        /* Initialize */
        for (int i = 0; i < 4; i++) {
            etal[i] = 0.0;
            etau[i] = 0.0;
        }
        for (int i = 0; i < 3; i++) {
            rhol[i] = 0.0;
            rhou[i] = 0.0;
        }

        /* For each Mu */
        for (int iMu = 0; iMu < g_atom.nM[iu]; iMu++) {
            int    Mu2 = g_atom.M2[iu][iMu];
            double gu  = g_atom.gL[iu];
            signua2    = signua3 - Mu2 - Mu2;
            signla2    = signla3 - Mu2;

            /* For each Ml */
            for (int iMl = 0; iMl < g_atom.nM[il]; iMl++) {
                int    Ml2 = g_atom.M2[il][iMl];
                double gl  = g_atom.gL[il];

                /* selection rule */
                if (abs(Ml2 - Mu2) > 2)
                    continue;

                signua1 = signua2 + Ml2;
                signla1 = signla2;

                /* Zeeman shift */
                dnuB = ZeemanProfileShift(gl, gu, Ml2, Mu2, modB);

                /* Profile */
                Phi = vgt_VoigtProfileNu(nu0 + dnuB, freqs[ifreq],
                                         p->nu_d[itran], p->a_voigt[itran]);

                /* Compute 3J */
                J31 = mat_Cou3j(g_atom.J2[iu], g_atom.J2[il], 2, -Mu2, Ml2,
                                Mu2 - Ml2);

                /*              */
                /* Absorptivity */
                /*              */

                /* For each K lower level */
                for (int Kl = 0; Kl <= g_atom.maxK[il]; Kl++) {
                    int maxQ = MIN2(Kl, 2);
                    Kr21     = (double)(2 * Kl + 1);

                    /* Initial index of lower level */
                    Ri = g_atom.i_rho[il][Kl][0];

                    /* For each Q */
                    for (int Q = -maxQ; Q <= maxQ; Q++) {
                        int    aQ, Q2 = Q * 2;
                        double sg;

                        if (Q < 0) {
                            aQ = -Q;
                            sg = -1.0;
                        } else {
                            aQ = Q;
                            sg = 1.0;
                        }

                        sign = ZNAM((signla1 - Q2) / 2);

                        /* Product rhou*Phi */
                        if (Q == 0) {
                            /* Product rhou*Phi */
                            RPre = Phi.re * p->rho[Ri];
                            RPim = Phi.im * p->rho[Ri];
                        } else if (aQ == 1) {
                            RPre = Phi.re * p->rho[Ri + 1] * sg -
                                   Phi.im * p->rho[Ri + 2];
                            RPim = Phi.re * p->rho[Ri + 2] +
                                   Phi.im * p->rho[Ri + 1] * sg;
                        } else {
                            RPre = Phi.re * p->rho[Ri + 3] -
                                   Phi.im * p->rho[Ri + 4] * sg;
                            RPim = Phi.re * p->rho[Ri + 4] * sg +
                                   Phi.im * p->rho[Ri + 3];
                        }

                        /* Check if values */
                        RR = (fabs(RPre) > 0.0) ? 1 : 0;
                        II = (fabs(RPim) > 0.0) ? 1 : 0;

                        /* Check non-zero */
                        if (!(RR || II))
                            continue;

                        /* 3J */
                        J32 = mat_Cou3j(g_atom.J2[iu], g_atom.J2[il], 2, -Mu2,
                                        Ml2 - Q2, Mu2 - Ml2 + Q2);
                        J32 *= mat_Cou3j(g_atom.J2[il], g_atom.J2[il], 2 * Kl,
                                         Ml2, Q2 - Ml2, -Q2) *
                               ((double)sign);
                        if (fabs(J32) <= 0.0)
                            continue;

                        /* For each K geometric */
                        for (int K = 0; K <= 2; K++) {
                            /* Check valid K */
                            if (K < aQ)
                                continue;

                            /* 3J and K factors */
                            J31K = J31 * sqrt(((double)(2 * K + 1)) * Kr21);
                            J33  = mat_Cou3j(2, 2, 2 * K, Ml2 - Mu2,
                                            Mu2 + Q2 - Ml2, -Q2);
                            J3   = J31K * J32 * J33;

                            if (K == 0) {
                                etal[0] += J3 * RPre;
                            } else if (K == 1) {
                                if (Q == 0) {
                                    etal[3] += J3 * RPre * tkq[ZT10V_RE];
                                    rhol[2] += J3 * RPim * tkq[ZT10V_RE];
                                } else {
                                    etal[3] += J3 * (RPre * tkq[ZT11V_RE] * sg -
                                                     RPim * tkq[ZT11V_IM]);
                                    rhol[2] += J3 * (RPre * tkq[ZT11V_IM] +
                                                     RPim * tkq[ZT11V_RE] * sg);
                                }
                            } else if (K == 2) {
                                if (Q == 0) {
                                    etal[0] += J3 * RPre * tkq[ZT20I_RE];
                                    etal[1] += J3 * RPre * tkq[ZT20Q_RE];
                                    etal[2] += J3 * RPre * tkq[ZT20U_RE];
                                    rhol[0] += J3 * RPim * tkq[ZT20Q_RE];
                                    rhol[1] += J3 * RPim * tkq[ZT20U_RE];
                                } else if (aQ == 1) {
                                    etal[0] += J3 * (RPre * tkq[ZT21I_RE] * sg -
                                                     RPim * tkq[ZT21I_IM]);
                                    etal[1] += J3 * (RPre * tkq[ZT21Q_RE] * sg -
                                                     RPim * tkq[ZT21Q_IM]);
                                    etal[2] += J3 * (RPre * tkq[ZT21U_RE] * sg -
                                                     RPim * tkq[ZT21U_IM]);
                                    rhol[0] += J3 * (RPre * tkq[ZT21Q_IM] +
                                                     RPim * tkq[ZT21Q_RE] * sg);
                                    rhol[1] += J3 * (RPre * tkq[ZT21U_IM] +
                                                     RPim * tkq[ZT21U_RE] * sg);
                                } else {
                                    etal[0] += J3 * (RPre * tkq[ZT22I_RE] -
                                                     RPim * tkq[ZT22I_IM] * sg);
                                    etal[1] += J3 * (RPre * tkq[ZT22Q_RE] -
                                                     RPim * tkq[ZT22Q_IM] * sg);
                                    etal[2] += J3 * (RPre * tkq[ZT22U_RE] -
                                                     RPim * tkq[ZT22U_IM] * sg);
                                    rhol[0] += J3 * (RPre * tkq[ZT22Q_IM] * sg +
                                                     RPim * tkq[ZT22Q_RE]);
                                    rhol[1] += J3 * (RPre * tkq[ZT22U_IM] * sg +
                                                     RPim * tkq[ZT22U_RE]);
                                }
                            }
                        } /* K geometric */
                    }     /* Q multipole */
                }         /* K lower level */

                /*            */
                /* Emissivity */
                /*            */

                /* For each K upper level */
                for (int Ku = 0; Ku <= g_atom.maxK[iu]; Ku++) {
                    int maxQ = MIN2(Ku, 2);
                    Kr21     = (double)(2 * Ku + 1);

                    /* Initial index of upper level */
                    Ri = g_atom.i_rho[iu][Ku][0];

                    /* For each Q */
                    for (int Q = -maxQ; Q <= maxQ; Q++) {
                        int    aQ, Q2 = Q * 2;
                        double sg;

                        if (Q < 0) {
                            aQ = -Q;
                            sg = -1.0;
                        } else {
                            aQ = Q;
                            sg = 1.0;
                        }

                        sign = ZNAM((signua1 - Q2) / 2);

                        /* Product rhou*Phi */
                        if (Q == 0) {
                            /* Product rhou*Phi */
                            RPre = Phi.re * p->rho[Ri];
                            RPim = Phi.im * p->rho[Ri];
                        } else if (aQ == 1) {
                            RPre = Phi.re * p->rho[Ri + 1] * sg -
                                   Phi.im * p->rho[Ri + 2];
                            RPim = Phi.re * p->rho[Ri + 2] +
                                   Phi.im * p->rho[Ri + 1] * sg;
                        } else {
                            RPre = Phi.re * p->rho[Ri + 3] -
                                   Phi.im * p->rho[Ri + 4] * sg;
                            RPim = Phi.re * p->rho[Ri + 4] * sg +
                                   Phi.im * p->rho[Ri + 3];
                        }

                        /* Check if values */
                        RR = (fabs(RPre) > 0.0) ? 1 : 0;
                        II = (fabs(RPim) > 0.0) ? 1 : 0;

                        /* Check non-zero */
                        if (!(RR || II))
                            continue;

                        /* 3J */
                        J32 = mat_Cou3j(g_atom.J2[iu], g_atom.J2[il], 2,
                                        -Mu2 - Q2, Ml2, Mu2 - Ml2 + Q2);
                        J32 *= mat_Cou3j(g_atom.J2[iu], g_atom.J2[iu], 2 * Ku,
                                         Mu2 + Q2, -Mu2, -Q2) *
                               ((double)sign);
                        if (fabs(J32) <= 0.0)
                            continue;

                        /* For each K geometric */
                        for (int K = 0; K <= 2; K++) {
                            /* Check valid K */
                            if (K < aQ)
                                continue;

                            /* 3J and K factors */
                            J31K = J31 * sqrt(((double)(2 * K + 1)) * Kr21);
                            J33  = mat_Cou3j(2, 2, 2 * K, Ml2 - Mu2,
                                            Mu2 + Q2 - Ml2, -Q2);
                            J3   = J31K * J32 * J33;

                            if (K == 0) {
                                etau[0] += J3 * RPre;
                            } else if (K == 1) {
                                if (Q == 0) {
                                    etau[3] += J3 * RPre * tkq[ZT10V_RE];
                                    rhou[2] += J3 * RPim * tkq[ZT10V_RE];
                                } else {
                                    etau[3] += J3 * (RPre * tkq[ZT11V_RE] * sg -
                                                     RPim * tkq[ZT11V_IM]);
                                    rhou[2] += J3 * (RPre * tkq[ZT11V_IM] +
                                                     RPim * tkq[ZT11V_RE] * sg);
                                }
                            } else if (K == 2) {
                                if (Q == 0) {
                                    etau[0] += J3 * RPre * tkq[ZT20I_RE];
                                    etau[1] += J3 * RPre * tkq[ZT20Q_RE];
                                    etau[2] += J3 * RPre * tkq[ZT20U_RE];
                                    rhou[0] += J3 * RPim * tkq[ZT20Q_RE];
                                    rhou[1] += J3 * RPim * tkq[ZT20U_RE];
                                } else if (aQ == 1) {
                                    etau[0] += J3 * (RPre * tkq[ZT21I_RE] * sg -
                                                     RPim * tkq[ZT21I_IM]);
                                    etau[1] += J3 * (RPre * tkq[ZT21Q_RE] * sg -
                                                     RPim * tkq[ZT21Q_IM]);
                                    etau[2] += J3 * (RPre * tkq[ZT21U_RE] * sg -
                                                     RPim * tkq[ZT21U_IM]);
                                    rhou[0] += J3 * (RPre * tkq[ZT21Q_IM] +
                                                     RPim * tkq[ZT21Q_RE] * sg);
                                    rhou[1] += J3 * (RPre * tkq[ZT21U_IM] +
                                                     RPim * tkq[ZT21U_RE] * sg);
                                } else {
                                    etau[0] += J3 * (RPre * tkq[ZT22I_RE] -
                                                     RPim * tkq[ZT22I_IM] * sg);
                                    etau[1] += J3 * (RPre * tkq[ZT22Q_RE] -
                                                     RPim * tkq[ZT22Q_IM] * sg);
                                    etau[2] += J3 * (RPre * tkq[ZT22U_RE] -
                                                     RPim * tkq[ZT22U_IM] * sg);
                                    rhou[0] += J3 * (RPre * tkq[ZT22Q_IM] * sg +
                                                     RPim * tkq[ZT22Q_RE]);
                                    rhou[1] += J3 * (RPre * tkq[ZT22U_IM] * sg +
                                                     RPim * tkq[ZT22U_RE]);
                                }
                            }
                        } /* K geometry */
                    }     /* Q multipole */
                }         /* K upper level */

            } /* Ml lower level magnetic moment */
        }     /* Mu upper level magnetic moment */

        /* Constants for absorption and emission */
        fl = fact * ((double)g_atom.J2[il] + 1.0) * g_atom.Blu[itran];
        fu = fact * ((double)g_atom.J2[iu] + 1.0) * g_atom.Aul[itran];

        /* Take into account stimulated contributions
           and add to RT coefficients */
        for (int i = 0; i < 4; i++) {
            eta[i] += (etal[i] - etau[i]) * fl;
            eps[i] += etau[i] * fu;
        }
        for (int i = 0; i < 3; i++) {
            rho[i] += (rhol[i] - rhou[i]) * fl;
        }

    } /* For every transition */

    /* Complete the computation */
    kmtx[ETA_I]  = eta[0];
    kmtx[ETA_Q]  = eta[1];
    kmtx[ETA_U]  = eta[2];
    kmtx[ETA_V]  = eta[3];
    kmtx[RHO_Q]  = rho[0];
    kmtx[RHO_U]  = rho[1];
    kmtx[RHO_V]  = rho[2];
    sf[STOKES_I] = eps[0] / (kmtx[ETA_I] + VACUUM_OPACITY);
    sf[STOKES_Q] = eps[1] / (kmtx[ETA_I] + VACUUM_OPACITY);
    sf[STOKES_U] = eps[2] / (kmtx[ETA_I] + VACUUM_OPACITY);
    sf[STOKES_V] = eps[3] / (kmtx[ETA_I] + VACUUM_OPACITY);
}

#endif

/****************************************************************************/
/**
 * Function that actually computes the source function and absorption
 * matrix given the node, the direction, and the frequency
 *
 * @brief Computes radiation transfer coefficients in Hanle regime
 * @param [in] p_node Node data
 * @param [in] tkq Geometrical tensors in the vertical reference frame
 * @param [in] dir Director cosines of the propagation direction
 * @param [in] idir Last direction for which the absorption profiles
 *                  were computed
 * @param [in] ifreq Frequency index for which the radiation transfer
 *                   coefficients are requested
 * @param [out] sf Source function
 * @param [out] kmtx Absorption matrix
 * @return void
 *
 ****************************************************************************/
static void SKGeneral(t_node *p_node, double sf[NSTOKES], double kmtx[NKMTX],
                      double tkq[NTKQ], const t_vec_3d *dir, int idir,
                      int ifreq)
{
    m_Node *      p;
    int           nc, il, iu, sign, jfreq, Ri;
    int           flag[g_atom.rtran];
    double        eta[4], etal[4], eps[4], epsl[4];
    static double fact;
    /*static double w6jA[g_atom.MaxK+1], w6jS[g_atom.MaxK+1];*/
    static double w6jA[3], w6jS[3];
    const double *freqs;

    /* Initializations */
    p     = (m_Node *)p_node->p_data;
    freqs = PorFreqs();
    for (int i = 0; i < 4; i++) {
        eta[i] = 0.0;
        eps[i] = 0.0;
    }
    sf[STOKES_I] = sf[STOKES_Q] = sf[STOKES_U] = sf[STOKES_V] = kmtx[ETA_I] =
        kmtx[ETA_Q] = kmtx[ETA_U] = kmtx[ETA_V] = kmtx[RHO_Q] = kmtx[RHO_U] =
            kmtx[RHO_V]                                       = 0.0;

    /* Continuum */
    nc = 0;
    for (int itran = 0; itran < g_atom.rtran; itran++) {
        /* Check if frequency in this transition */
        if (ifreq >= g_atom.i0[itran] && ifreq <= g_atom.i1[itran]) {
            flag[itran] = 0;
            nc += 1;
        } else {
            flag[itran] = 1;
        }
    }

    /* If more than one transition, average continuums */
    if (nc > 1) {
        int    i0 = -1, i1 = -1;
        double x0 = 0., x1 = 1e100;
        for (int itran = 0; itran < g_atom.rtran; itran++) {
            if (flag[itran]) {
                continue;
            }

            if (g_atom.nu[itran] < freqs[ifreq] && g_atom.nu[itran] > x0) {
                x0 = g_atom.nu[itran];
                i0 = itran;
            } else if (g_atom.nu[itran] > freqs[ifreq] &&
                       g_atom.nu[itran] < x1) {
                x1 = g_atom.nu[itran];
                i1 = itran;
            }
        }

        /* Interpolate */
        if (i0 < 0) {
            eta[0] = p->eta_c[i1];
            eps[0] = p->eps_c[i1];
        } else if (i1 < 0) {
            eta[0] = p->eta_c[i0];
            eps[0] = p->eps_c[i0];
        } else {
            eta[0] = p->eta_c[i0] + (p->eta_c[i1] - p->eta_c[i0]) *
                                        (freqs[ifreq] - x0) / (x1 - x0);
            eps[0] = p->eps_c[i0] + (p->eps_c[i1] - p->eps_c[i0]) *
                                        (freqs[ifreq] - x0) / (x1 - x0);
        }
    }

    /* Just one transition */
    else if (nc == 1) {
        for (int itran = 0; itran < g_atom.rtran; itran++) {
            if (flag[itran]) {
                continue;
            }

            eta[0] = p->eta_c[itran];
            eps[0] = p->eps_c[itran];
            break;
        }
    }
    /* No transition */
    else {
        kmtx[ETA_I] = VACUUM_OPACITY;
        return;
    }

    /* For each transition */
    for (int itran = 0; itran < g_atom.rtran; itran++) {
        /* Check if frequency in this transition */
        if (flag[itran]) {
            continue;
        }

        /* Simplify indexes for levels */
        il = g_atom.il[itran];
        iu = g_atom.iu[itran];

        /* Shift the frequency index */
        jfreq = ifreq - g_atom.i0[itran];

        /* Compute 6J absorption */
        for (int K = 0; K <= g_atom.maxK[il]; K++) {
            /* Limit imposed by TKQ */
            if (K > 2) {
                break;
            }

            w6jA[K] = mat_Cou6j(2, 2, 2 * K, g_atom.J2[il], g_atom.J2[il],
                                g_atom.J2[iu]);
        }

        /* Compute 6J emission */
        if (g_atom.J2[il] == g_atom.J2[iu]) {
            for (int K = 0; K <= g_atom.maxK[iu]; K++) {
                if (K > 2) {
                    break;
                }
                w6jS[K] = w6jA[K];
            }
        } else {
            for (int K = 0; K <= g_atom.maxK[iu]; K++) {
                if (K > 2) {
                    break;
                }
                w6jS[K] = mat_Cou6j(2, 2, 2 * K, g_atom.J2[iu], g_atom.J2[iu],
                                    g_atom.J2[il]);
            }
        }

        /* Get sign for emission (and partial sign for absorption) */
        sign = ZNAM(1 + (g_atom.J2[iu] + g_atom.J2[il]) / 2);

        /* Get multiplicative factor */
        fact = C_H4PIS3 * g_atom.nu[itran] * p->N * ((double)sign);

        /* Emission part */
        Ri      = g_atom.i_rho[iu][0][0];
        epsl[0] = w6jS[0] * p->rho[Ri];
        epsl[1] = 0.;
        epsl[2] = 0.;
        epsl[3] = 0.;

        Ri++;
        for (int K = 1; K <= g_atom.maxK[iu]; K++) {
            /* Alignment contribution */
            if (K == 2) {
                epsl[0] += w6jS[2] * (tkq[T20I_RE] * p->rho[Ri] +
                                      2.0 * (tkq[T21I_RE] * p->rho[Ri + 1] -
                                             tkq[T21I_IM] * p->rho[Ri + 2]) +
                                      2.0 * (tkq[T22I_RE] * p->rho[Ri + 3] -
                                             tkq[T22I_IM] * p->rho[Ri + 4]));
                epsl[1] += w6jS[2] * (tkq[T20Q_RE] * p->rho[Ri] +
                                      2.0 * (tkq[T21Q_RE] * p->rho[Ri + 1] -
                                             tkq[T21Q_IM] * p->rho[Ri + 2]) +
                                      2.0 * (tkq[T22Q_RE] * p->rho[Ri + 3] -
                                             tkq[T22Q_IM] * p->rho[Ri + 4]));
                epsl[2] += w6jS[2] * (2.0 * (tkq[T21U_RE] * p->rho[Ri + 1] -
                                             tkq[T21U_IM] * p->rho[Ri + 2]) +
                                      2.0 * (tkq[T22U_RE] * p->rho[Ri + 3] -
                                             tkq[T22U_IM] * p->rho[Ri + 4]));
                Ri += 5;
            }
            /* Orientation contribution */
            else if (K == 1)

                /*
                    epsl[3] += w6jS[1]*
                              (tkq[T10U_RE]*p->rho[Ri] +
                               2.0*(tkq[T11V_RE]*p->rho[Ri+1] -
                                    tkq[T11V_IM]*p->rho[Ri+2]);
                */
                Ri += 3;
            /* Limit imposed by TKQ in multi-level K<=2 */
            else {
                continue;
            }
        }

        /* Absorption part */
        Ri      = g_atom.i_rho[il][0][0];
        etal[0] = w6jA[0] * p->rho[Ri];
        etal[1] = 0.;
        etal[2] = 0.;
        etal[3] = 0.;

        Ri++;
        for (int K = 1; K <= g_atom.maxK[il]; K++) {
            /* Alignment contribution */
            if (K == 2) {
                etal[0] += w6jA[2] * (tkq[T20I_RE] * p->rho[Ri] +
                                      2.0 * (tkq[T21I_RE] * p->rho[Ri + 1] -
                                             tkq[T21I_IM] * p->rho[Ri + 2]) +
                                      2.0 * (tkq[T22I_RE] * p->rho[Ri + 3] -
                                             tkq[T22I_IM] * p->rho[Ri + 4]));
                etal[1] += w6jA[2] * (tkq[T20Q_RE] * p->rho[Ri] +
                                      2.0 * (tkq[T21Q_RE] * p->rho[Ri + 1] -
                                             tkq[T21Q_IM] * p->rho[Ri + 2]) +
                                      2.0 * (tkq[T22Q_RE] * p->rho[Ri + 3] -
                                             tkq[T22Q_IM] * p->rho[Ri + 4]));
                etal[2] += w6jA[2] * (2.0 * (tkq[T21U_RE] * p->rho[Ri + 1] -
                                             tkq[T21U_IM] * p->rho[Ri + 2]) +
                                      2.0 * (tkq[T22U_RE] * p->rho[Ri + 3] -
                                             tkq[T22U_IM] * p->rho[Ri + 4]));
                Ri += 5;
            }
            /* Orientation contribution */
            else if (K == 1)

                /*
                    etal[3] -= w6jA[1]*
                              (tkq[T10U_RE]*p->rho[Ri] +
                               2.0*(tkq[T11V_RE]*p->rho[Ri+1] -
                                    tkq[T11V_IM]*p->rho[Ri+2]);
                */
                Ri += 3;
            /* Limit imposed by TKQ in multi-level K<=2 */
            else {
                continue;
            }
        }

        CalculateNodeProfiles(p_node, itran, jfreq, dir, idir, 0);

        /* Multiply by constants */
        for (int i = 0; i < 4; i++) {
            etal[i] = (etal[i] - epsl[i]) * fact *
                      ((double)g_atom.J2[il] + 1.0) * g_atom.Blu[itran] *
                      g_Prof[0];
            eta[i] += etal[i];

            epsl[i] *= fact * ((double)g_atom.J2[iu] + 1.0) *
                       g_atom.Aul[itran] * g_Prof[0];
            eps[i] += epsl[i];
        }
    }

    /* Complete the computation */
    kmtx[ETA_I]  = eta[0];
    kmtx[ETA_Q]  = eta[1];
    kmtx[ETA_U]  = eta[2];
    kmtx[ETA_V]  = 0.0;
    kmtx[RHO_Q]  = 0.0;
    kmtx[RHO_U]  = 0.0;
    kmtx[RHO_V]  = 0.0;
    sf[STOKES_I] = eps[0] / (kmtx[ETA_I] + VACUUM_OPACITY);
    sf[STOKES_Q] = eps[1] / (kmtx[ETA_I] + VACUUM_OPACITY);
    sf[STOKES_U] = eps[2] / (kmtx[ETA_I] + VACUUM_OPACITY);
    sf[STOKES_V] = 0.0;
}

/****************************************************************************/
/**
 * Function that computes the absorption intensity coefficient  given the node,
 * the direction, and the frequency, to feed the tau calculations
 *
 * @brief Computes radiation transfer coefficients in Hanle regime
 * @param [in] p_node Node data
 * @param [in] dir Director cosines of the propagation direction
 * @param [in] idir Last direction for which the absorption profiles
 *                  were computed
 * @param [in] ifreq Frequency index for which the radiation transfer
 *                   coefficients are requested
 * @param [out] sf Source function
 * @param [out] kmtx Absorption matrix
 * @return void
 *
 ****************************************************************************/
static void SKGeneral_tau(t_node *p_node, double sf[NSTOKES],
                          double kmtx[NKMTX], const t_vec_3d *dir, int idir,
                          int ifreq)
{
    m_Node *      p;
    int           nc, il, iu, sign, jfreq, Ri;
    int           flag[g_atom.rtran];
    double        eta, etal, epsl, Jl2, Ju2;
    static double fact;
    const double *freqs;

    /* Initializations */
    p            = (m_Node *)p_node->p_data;
    freqs        = PorFreqs();
    eta          = 0.0;
    sf[STOKES_I] = sf[STOKES_Q] = sf[STOKES_U] = sf[STOKES_V] = kmtx[ETA_I] =
        kmtx[ETA_Q] = kmtx[ETA_U] = kmtx[ETA_V] = kmtx[RHO_Q] = kmtx[RHO_U] =
            kmtx[RHO_V]                                       = 0.0;

    /* Continuum */
    nc = 0;
    for (int itran = 0; itran < g_atom.rtran; itran++) {
        /* Check if frequency in this transition */
        if (ifreq >= g_atom.i0[itran] && ifreq <= g_atom.i1[itran]) {
            flag[itran] = 0;
            nc += 1;
        } else {
            flag[itran] = 1;
        }
    }

    /* If more than one transition, average continuums */
    if (nc > 1) {
        int    i0 = -1, i1 = -1;
        double x0 = 0., x1 = 1e100;
        for (int itran = 0; itran < g_atom.rtran; itran++) {
            if (flag[itran]) {
                continue;
            }

            if (g_atom.nu[itran] < freqs[ifreq] && g_atom.nu[itran] > x0) {
                x0 = g_atom.nu[itran];
                i0 = itran;
            } else if (g_atom.nu[itran] > freqs[ifreq] &&
                       g_atom.nu[itran] < x1) {
                x1 = g_atom.nu[itran];
                i1 = itran;
            }
        }

        /* Interpolate */
        if (i0 < 0) {
            eta = p->eta_c[i1];
        } else if (i1 < 0) {
            eta = p->eta_c[i0];
        } else {
            eta = p->eta_c[i0] + (p->eta_c[i1] - p->eta_c[i0]) *
                                     (freqs[ifreq] - x0) / (x1 - x0);
        }
    }

    /* Just one transition */
    else if (nc == 1) {
        for (int itran = 0; itran < g_atom.rtran; itran++) {
            if (flag[itran]) {
                continue;
            }

            eta = p->eta_c[itran];
            break;
        }
    }
    /* No transition */
    else {
        kmtx[ETA_I] = VACUUM_OPACITY;
        return;
    }

    /* For each transition */
    for (int itran = 0; itran < g_atom.rtran; itran++) {
        /* Check if frequency in this transition */
        if (flag[itran]) {
            continue;
        }

        /* Simplify indexes for levels */
        il = g_atom.il[itran];
        iu = g_atom.iu[itran];

        /* Get J values */
        Jl2 = (double)g_atom.J2[il];
        Ju2 = (double)g_atom.J2[iu];

        /* Shift the frequency index */
        jfreq = ifreq - g_atom.i0[itran];

        /* Get multiplicative factor */
        fact = C_H4PI * g_atom.nu[itran] * p->N;

        /* Emission part */
        Ri   = g_atom.i_rho[iu][0][0];
        epsl = p->rho[Ri] * sqrt(Ju2 + 1.0);

        /* Absorption part */
        Ri   = g_atom.i_rho[il][0][0];
        etal = p->rho[Ri] * sqrt(Jl2 + 1.0);

        CalculateNodeProfiles(p_node, itran, jfreq, dir, idir, 0);

        /* Multiply by constants */
        etal = (etal - epsl * (Jl2 + 1.0) / (Ju2 + 1.0)) * fact *
               g_atom.Blu[itran] * g_Prof[0];
        eta += etal;
    }

    /* Complete the computation */
    kmtx[ETA_I] = eta;
}

/****************************************************************************/
/**
 * Computes the source function and absorption matrix in a general
 * direction, in a given node, for a given frequency. It calls
 * SKGeneral or SKGeneral_HZ, the function that does the actual
 * calculation, after computing the geometrical tensors for the
 * requested direction and corresponding reference frame
 *
 * @brief Source function and absorption matrix at the point p_node in
 *        a general direction
 * @param [in] p_node Node data
 * @param [in] dir Direction in which to compute the radiation transfer
 *                 coefficients
 * @param [in] ifreq Frequency index for which the radiation transfer
 *                   coefficients are requested
 * @param [in] tau if 1, even in the case where HANLE_ZEEMAN_FS is defined, we
 *will take into account only the Hanle effect (useful for calculation of TAU
 *surface (see issue #61)
 * @param [out] sf Source function
 * @param [out] kmtx Absorption matrix
 * @return void
 *
 ****************************************************************************/
static void F_SKGeneralDirection(t_node *p_node, double sf[NSTOKES],
                                 double kmtx[NKMTX], const t_vec_3d dir,
                                 int ifreq, int tau)
{
    double   theta, chi;
    t_vec_3d dir_norm;

/* Hanle-Zeeman exact branch */
#if defined(HANLE_ZEEMAN_FS)

    /* Check if asking for tau */
    if (tau == 1) {
        SKGeneral_tau(p_node, sf, kmtx, &dir_norm, -1, ifreq);

    } else {
        /* Get magnetic field */
        double  B;
        m_Node *p;
        p = (m_Node *)p_node->p_data;

        /* Get magnetic field components */
        B = p->Bx * p->Bx + p->By * p->By + p->Bz * p->Bz;
        B = sqrt(B);

        /* Get propagation vector */
        dir_norm = dir;
        VEC_NORMALIZE_3D(dir_norm);
        VecUnit2Ang(dir_norm, &theta, &chi);

        /* If magnetic field not zero */
        if (B > 0.0) {
            double   thetaB, chiB;
            double   Btheta, Bchi;
            double   ct, st, Bct, Bst, ctB, stB, caB, saB;
            double   cBaa, sBaa, cy, sy;
            double   tkq[ZNTKQ];
            t_vec_3d Bdir_norm, dirB_norm;
            t_vec_3d Bvec;

            /* Cosine and sine of propagation direction in
               laboratory frame */
            ct = cos(theta);
            st = sin(theta);

            /* Magnetic field components */
            Bvec.x = p->Bx;
            Bvec.y = p->By;
            Bvec.z = p->Bz;

            /* If non-vertical field */
            if ((Bvec.x * Bvec.x + Bvec.y * Bvec.y) > 0.0) {
                /* Angle of B in the laboratory frame */
                Bdir_norm = Bvec;
                VEC_NORMALIZE_3D(Bdir_norm);
                VecUnit2Ang(Bdir_norm, &Btheta, &Bchi);
                Bct  = cos(Btheta);
                Bst  = sin(Btheta);
                cBaa = cos(Bchi - chi);
                sBaa = sin(Bchi - chi);

                /* Propagation direction in B frame */
                dirB_norm = dir_Vector2BFrame(dir_norm, Bvec);
                VEC_NORMALIZE_3D(dirB_norm);
                VecUnit2Ang(dirB_norm, &thetaB, &chiB);
                ctB = cos(thetaB);
                stB = sin(thetaB);
                caB = cos(chiB);
                saB = sin(chiB);

                /* Cosine and sine of gamma angle */
                // e1*e1|B
                // cy = ctB*caB*(ct*Bct*cBaa + st*Bst) -
                //     ctB*saB*ct*sBaa -
                //     stB*(ct*Bst*cBaa - st*Bct);
                // e2*e1|B
                // sy = ctB*caB*Bct*sBaa + ctB*saB*cBaa - stB*Bst*sBaa;

                // e2*e2|B
                cy = caB * cBaa - saB * Bct * sBaa;
                // e1*e2|B
                sy = -saB * (ct * Bct * cBaa + st * Bst) - caB * ct * sBaa;

                /* Compute TKQ tensors */
                CalcTKQ(stB, ctB, saB, caB, sy, cy, tkq);

                /* Rotate the rhoKQ if necessary */
                if (!p->rotated) {
                    rotate_dm(p_node, 0.0, Btheta, Bchi);
                    p->rotated = 1;
                }

            } else {
                /* Compute TKQ tensors */
                CalcTKQ(sin(theta), cos(theta), sin(chi), cos(chi), 0.0, 1.0,
                        tkq);
            }

            SKGeneral_HZ(p_node, sf, kmtx, tkq, &dir_norm, IDIR_GENDIR, ifreq,
                         B);

        } else {
            double tkq[NTKQ];

            /* TKQ in laboratory reference frame */
            dir_CalcTKQ(sin(theta), cos(theta), sin(chi), cos(chi), tkq);

            SKGeneral(p_node, sf, kmtx, tkq, &dir_norm, IDIR_GENDIR, ifreq);
        }
    }

/* Hanle regime */
#else

    /* Calling for just tau */
    if (tau == 1) {
        SKGeneral_tau(p_node, sf, kmtx, &dir_norm, -1, ifreq);

    } else {
        double tkq[NTKQ];

        dir_norm = dir;
        VEC_NORMALIZE_3D(dir_norm);
        VecUnit2Ang(dir_norm, &theta, &chi);
        dir_CalcTKQ(sin(theta), cos(theta), sin(chi), cos(chi), tkq);
        SKGeneral(p_node, sf, kmtx, tkq, &dir_norm, IDIR_GENDIR, ifreq);
    }

#endif
}

/****************************************************************************/
/**
 * Computes the source function and absorption matrix in a quadrature
 * direction, in a given node, for a given frequency. It calls
 * SKGeneral, the function that does the actual calculation, after
 * computing the geometrical tensors for the requested quadrature
 * direction
 *
 * @brief Source function and absorption matrix at the point p_node in
 *        a quadrature direction
 * @param [in] p_node Node data
 * @param [in] iray Index of the direction in which to compute the
 *                  radiation transfer coefficients
 * @param [in] ifreq Frequency index for which the radiation transfer
 *                   coefficients are requested
 * @param [out] sf Source function
 * @param [out] kmtx Absorption matrix
 * @return void
 *
 ****************************************************************************/
static void F_SK(t_node *p_node, double sf[NSTOKES], double kmtx[NKMTX],
                 int iray, int ifreq)
{
    double   tkq[NTKQ];
    t_vec_3d dir;
    PorGetTKQ(iray, tkq);
    PorGetIDir(iray, &dir);
    SKGeneral(p_node, sf, kmtx, tkq, &dir, iray, ifreq);
}

/****************************************************************************/
/**
 * Adds the contribution of a given node, frequency, and direction to
 * the integrated radiation field tensors and Lambda operators
 *
 * @brief Process the local Stokes vector and Psi^* stored in fsdata
 *        for a given iray direction and ifreq frequency and store in
 *        the atomic data structures
 * @param [in] p_node Node data
 * @param [in] iray Index of the direction in the quadrature
 * @param [in] ifreq Index of the frequency
 * @return void
 *
 ****************************************************************************/
static void F_AddFS(t_node *p_node, int iray, int ifreq)
{
    m_Node * p;
    int      jfreq;
    double   Wi = 0, Wq = 0, Wu = 0, Wp = 0;
    double   W, dirq, fact, cfact;
    double   tkq[NTKQ];
    t_vec_3d dir;

    /* Point to node data */
    p = (m_Node *)p_node->p_data;

    /* Direction, and TKQ tensors */
    PorGetIDir(iray, &dir);
    dirq = PorAngQuadrature(iray);
    PorGetTKQ(iray, tkq);

    /* Factor common to all transitions for Lambda operator */
    cfact = C_H4PI * p->N / (p_node->fsdata.k[ETA_I] + VACUUM_OPACITY);

    /* For each transition */
    for (int itran = 0; itran < g_atom.rtran; itran++) {
        /* Check frequency is within ranges */
        if (ifreq < g_atom.i0[itran] || ifreq > g_atom.i1[itran]) {
            continue;
        }

        /* Shift frequency index */
        jfreq = ifreq - g_atom.i0[itran];

        /* profiles in the co-moving frame: */
        CalculateNodeProfiles(p_node, itran, jfreq, &dir, iray, 0);

        /* Weight */
        W = g_Prof[0] * vgt_PoolGetQuadrature(itran, jfreq) * dirq;

        /* Factor for the Lambda operator */
        fact = cfact * sqrt(((double)g_atom.J2[g_atom.iu[itran]]) + 1.0) *
               g_atom.Aul[itran] * g_Prof[0] * g_atom.nu[itran];

        /* Integral function */
        Wi = p_node->fsdata.i[STOKES_I] * W;
        Wq = p_node->fsdata.i[STOKES_Q] * W;
        Wu = p_node->fsdata.i[STOKES_U] * W;
        Wp = p_node->fsdata.psi_star * fact * W;

        int Ri = g_atom.i_J[itran][0][0];

        p->J[Ri] += Wi;
        /* Orientation
        p->J[Ri+1] += tkq[T10V_RE]*dv;
        p->J[Ri+2] += tkq[T11V_RE]*dv;
        p->J[Ri+3] += tkq[T11V_IM]*dv;
        */
        p->J[Ri + 4] += tkq[T20I_RE] * Wi + tkq[T20Q_RE] * Wq;
        p->J[Ri + 5] +=
            tkq[T21I_RE] * Wi + tkq[T21Q_RE] * Wq + tkq[T21U_RE] * Wu;
        p->J[Ri + 6] +=
            tkq[T21I_IM] * Wi + tkq[T21Q_IM] * Wq + tkq[T21U_IM] * Wu;
        p->J[Ri + 7] +=
            tkq[T22I_RE] * Wi + tkq[T22Q_RE] * Wq + tkq[T22U_RE] * Wu;
        p->J[Ri + 8] +=
            tkq[T22I_IM] * Wi + tkq[T22Q_IM] * Wq + tkq[T22U_IM] * Wu;
        p->psi_eta[itran] += Wp;
    }
}

/****************************************************************************/
/**
 * Computes the absorption transfer rate given the multipolar indexes,
 * the transition, the radiation field tensors, and if the real or the
 * imaginary part must be computed
 *
 * @brief Reduced absorption transfer rate
 * @param [in] itran Index of the transition
 * @param [in] K K index of the row of the statistical equilibrium
 *               equation
 * @param [in] Kl K index of the column of the statistical equilibrium
 *                equation
 * @param [in] Q Q index of the multipolar component for which the row
 *               of the statistical equilibrium equation is being
 *               calculated
 * @param [in] Ql Q index of the multipolar component for which the
 *                column of the statistical equilibrium equation is
 *                being calculated
 * @param [in] real 1 if the real part of the rate is to be computed,
 *                  -1 if the imaginary part is to be computed
 * @param [in] jkq Radiation field tensors for the itran transition
 * @return double Reduced absorption transfer rate
 *
 ****************************************************************************/
static double TAf(int itran, int K, int Kl, int Q, int Ql, int real,
                  double *jkq)
{
    double ta = 0.0;
    int    minKr, maxKr, Qr, J2u, J2l, jind;

    /* Angular momentums */
    J2u = g_atom.J2[g_atom.iu[itran]];
    J2l = g_atom.J2[g_atom.il[itran]];

    /* Compute Qr value */
    Qr = Ql - Q;

    /* Check if we have to return */
    if (Qr < -2 || Qr > 2) {
        return ta;
    }

    /* Compute limits for K */
    minKr = abs(K - Kl);
    maxKr = K + Kl;
    if (maxKr > 2) {
        maxKr = 2;
    }
    if (minKr < abs(Qr)) {
        minKr = abs(Qr);
    }
    if (minKr > maxKr) {
        return ta;
    }

    /* For each value of Kr */
    for (int Kr = minKr; Kr <= maxKr; Kr++) {
        double fact =
            sqrt(2.0 * ((double)Kr) + 1.0) *
            mat_Cou9j(J2u, J2l, 2, J2u, J2l, 2, 2 * K, 2 * Kl, 2 * Kr);

        /* J index for Kr0 */
        if (Kr > 1) {
            jind = 4;
        } else {
            jind = Kr;
        }

        /* Branch Qr */
        if (Qr < 0) {
            /* Get absolute and factor */
            Qr = -Qr;
            double tal =
                ((double)ZNAM(Qr)) * fact *
                mat_Cou3j(K * 2, Kl * 2, Kr * 2, -Q * 2, Ql * 2, Qr * 2);

            /* Real part */
            if (real > 0) {
                ta += tal * jkq[jind + Qr * 2 - 1];
            }
            /* Imaginary part */
            else {
                ta -= tal * jkq[jind + Qr * 2];
            }
        } else {
            if (Qr > 0) {
                /* Get factor */
                double tal = fact * mat_Cou3j(K * 2, Kl * 2, Kr * 2, -Q * 2,
                                              Ql * 2, -Qr * 2);

                /* Real part */
                if (real > 0) {
                    ta += tal * jkq[jind + Qr * 2 - 1];
                }
                /* Imaginary part */
                else {
                    ta += tal * jkq[jind + Qr * 2];
                }
            } else {
                /* Real part */
                if (real > 0) {
                    ta += fact * jkq[jind] *
                          mat_Cou3j(K * 2, Kl * 2, Kr * 2, -Q * 2, Ql * 2, 0);
                }
            }
        }
    }
    return ta;
}

/****************************************************************************/
/**
 * Computes the absorption transfer rate given the multipolar indexes,
 * the transition and the radiation field tensors, and also the
 * coefficient needed for the Jacobi method. It calls the reduced
 * absorption transfer rate routine in the way necessary to get the
 * only real statistical equilibrium equations
 *
 * @brief Absorption transfer rate
 * @param [in] K K index of the row of the statistical equilibrium
 *               equation
 * @param [in] Kl K index of the column of the statistical equilibrium
 *                equation
 * @param [in] Q Q index of the multipolar component for which the row
 *               of the statistical equilibrium equation is being
 *               calculated
 * @param [in] Ql Q index of the multipolar component for which the
 *                column of the statistical equilibrium equation is
 *                being calculated
 * @param [in] itran Index of the transition
 * @param [in] jkq Radiation field tensors for the itran transition
 * @param [out] cta Coefficient for Jacobi iteration
 * @return double Absorption transfer rate
 *
 ****************************************************************************/
static double TA(int K, int Q, int Kl, int Ql, int itran, double *jkq,
                 double *cta)
{
    double ta = 0.0;

    *cta = 0.0;

    /* Imaginary equation */
    if (Q < 0) {
        if (Ql < 0) {
            ta = TAf(itran, K, Kl, -Q, -Ql, 1, jkq) -
                 ((double)ZNAM(-Q)) * TAf(itran, K, Kl, Q, -Ql, 1, jkq);
        } else {
            ta = TAf(itran, K, Kl, -Q, Ql, -1, jkq);

            if (Ql > 0) {
                ta -= ((double)ZNAM(-Q)) * TAf(itran, K, Kl, Q, Ql, -1, jkq);
            }
        }
    }
    /* Real equation */
    else {
        if (Q > 0) {
            if (Ql < 0) {
                ta = -TAf(itran, K, Kl, Q, -Ql, -1, jkq) -
                     ((double)ZNAM(Q)) * TAf(itran, K, Kl, -Q, -Ql, -1, jkq);
            } else {
                ta = TAf(itran, K, Kl, Q, Ql, 1, jkq);

                if (Ql > 0) {
                    ta += ((double)ZNAM(Q)) * TAf(itran, K, Kl, -Q, Ql, 1, jkq);
                }
            }
        } else {
            if (Ql < 0) {
                ta = -2.0 * TAf(itran, K, Kl, Q, -Ql, -1, jkq);
            } else {
                ta = TAf(itran, K, Kl, Q, Ql, 1, jkq);

                if (Ql > 0) {
                    ta *= 2.0;
                }
            }
        }
    }

    /* Jacobi iteration coefficient */
    if (K == 0) {
        if (K == Kl && Q == Ql) {
            /*
            *cta = g_atom.Blu[itran]*
                   ((double)g_atom.J2[g_atom.il[itran]] + 1.0)*
                   sqrt((double)K*2.0 + 1.0)*
                   ((double)ZNAM(1+K+
                                 (g_atom.J2[g_atom.il[itran]] +
                                  g_atom.J2[g_atom.iu[itran]])/2))*
                   mat_Cou6j(g_atom.J2[g_atom.iu[itran]],
                             g_atom.J2[g_atom.iu[itran]],2*K,
                             g_atom.J2[g_atom.il[itran]],
                             g_atom.J2[g_atom.il[itran]],2);
            */
            *cta = g_atom.Blu[itran] *
                   sqrt(((double)g_atom.J2[g_atom.il[itran]] + 1.0) /
                        ((double)g_atom.J2[g_atom.iu[itran]] + 1.0));
        }
    }

    /* Common factors */
    ta *= g_atom.Blu[itran] * ((double)g_atom.J2[g_atom.il[itran]] + 1.0) *
          sqrt(3.0 * (2.0 * ((double)K) + 1.0) * (2.0 * ((double)Kl) + 1.0)) *
          ((double)ZNAM(Kl + Ql));

    return ta;
}

/****************************************************************************/
/**
 * Computes the stimulated emission transfer rate given the multipolar
 * indexes, the transition, the radiation field tensors, and if the
 * real or the imaginary part must be computed
 *
 * @brief Reduced stimulated emission transfer rate
 * @param [in] itran Index of the transition
 * @param [in] K K index of the row of the statistical equilibrium
 *               equation
 * @param [in] Ku K index of the column of the statistical equilibrium
 *                equation
 * @param [in] Q Q index of the multipolar component for which the row
 *               of the statistical equilibrium equation is being
 *               calculated
 * @param [in] Qu Q index of the multipolar component for which the
 *                column of the statistical equilibrium equation is
 *                being calculated
 * @param [in] real 1 if the real part of the rate is to be computed,
 *                  -1 if the imaginary part is to be computed
 * @param [in] jkq Radiation field tensors for the itran transition
 * @return double Reduced stimulated emission transfer rate
 *
 ****************************************************************************/
static double TSf(int itran, int K, int Ku, int Q, int Qu, int real,
                  double *jkq)
{
    double ts = 0.0;
    int    minKr, maxKr, Qr, J2u, J2l, jind;

    /* Angular momentums */
    J2u = g_atom.J2[g_atom.iu[itran]];
    J2l = g_atom.J2[g_atom.il[itran]];

    /* Compute Qr value */
    Qr = Qu - Q;

    /* Check if we have to return */
    if (Qr < -2 || Qr > 2) {
        return ts;
    }

    /* Compute limits for K */
    minKr = abs(K - Ku);
    maxKr = K + Ku;
    if (maxKr > 2) {
        maxKr = 2;
    }
    if (minKr < abs(Qr)) {
        minKr = abs(Qr);
    }
    if (minKr > maxKr) {
        return ts;
    }

    /* For each value of Kr */
    for (int Kr = minKr; Kr <= maxKr; Kr++) {
        double fact =
            sqrt(2.0 * ((double)Kr) + 1.0) * ((double)ZNAM(Kr)) *
            mat_Cou9j(J2l, J2u, 2, J2l, J2u, 2, 2 * K, 2 * Ku, 2 * Kr);

        /* J index for Kr0 */
        if (Kr > 1) {
            jind = 4;
        } else {
            jind = Kr;
        }

        /* Branch Qr */
        if (Qr < 0) {
            /* Get absolute and factor */
            Qr = -Qr;
            double tsl =
                ((double)ZNAM(Qr)) * fact *
                mat_Cou3j(K * 2, Ku * 2, Kr * 2, -Q * 2, Qu * 2, Qr * 2);

            /* Real part */
            if (real > 0) {
                ts += tsl * jkq[jind + Qr * 2 - 1];
            }
            /* Imaginary part */
            else {
                ts -= tsl * jkq[jind + Qr * 2];
            }
        } else {
            if (Qr > 0) {
                /* Get factor */
                double tsl = fact * mat_Cou3j(K * 2, Ku * 2, Kr * 2, -Q * 2,
                                              Qu * 2, -Qr * 2);

                /* Real part */
                if (real > 0) {
                    ts += tsl * jkq[jind + Qr * 2 - 1];
                }
                /* Imaginary part */
                else {
                    ts += tsl * jkq[jind + Qr * 2];
                }
            } else {
                /* Real part */
                if (real > 0) {
                    ts += fact * jkq[jind] *
                          mat_Cou3j(K * 2, Ku * 2, Kr * 2, -Q * 2, Qu * 2, 0);
                }
            }
        }
    }
    return ts;
}

/****************************************************************************/
/**
 * Computes the stimulated emission transfer rate given the multipolar
 * indexes, the transition and the radiation field tensors, and also
 * the coefficient needed for the Jacobi method. It calls the reduced
 * stimulated emission transfer rate routine in the way necessary to
 * get the only real statistical equilibrium equations
 *
 * @brief Stimulated emission transfer rate
 * @param [in] K K index of the row of the statistical equilibrium
 *               equation
 * @param [in] Ku K index of the column of the statistical equilibrium
 *                equation
 * @param [in] Q Q index of the multipolar component for which the row
 *               of the statistical equilibrium equation is being
 *               calculated
 * @param [in] Qu Q index of the multipolar component for which the
 *                column of the statistical equilibrium equation is
 *                being calculated
 * @param [in] itran Index of the transition
 * @param [in] jkq Radiation field tensors for the itran transition
 * @param [out] cts Coefficient for Jacobi iteration
 * @return double Stimulated emission transfer rate
 *
 ****************************************************************************/
static double TS(int K, int Q, int Ku, int Qu, int itran, double *jkq,
                 double *cts)
{
    double ts = 0.0;

    *cts = 0.0;

    /* Imaginary equation */
    if (Q < 0) {
        if (Qu < 0) {
            ts = TSf(itran, K, Ku, -Q, -Qu, 1, jkq) -
                 ((double)ZNAM(-Q)) * TSf(itran, K, Ku, Q, -Qu, 1, jkq);
        } else {
            ts = TSf(itran, K, Ku, -Q, Qu, -1, jkq);

            if (Qu > 0) {
                ts -= ((double)ZNAM(-Q)) * TSf(itran, K, Ku, Q, Qu, -1, jkq);
            }
        }
    }
    /* Real equation */
    else {
        if (Q > 0) {
            if (Qu < 0) {
                ts = -TSf(itran, K, Ku, Q, -Qu, -1, jkq) -
                     ((double)ZNAM(Q)) * TSf(itran, K, Ku, -Q, -Qu, -1, jkq);
            } else {
                ts = TSf(itran, K, Ku, Q, Qu, 1, jkq);

                if (Qu > 0) {
                    ts += ((double)ZNAM(Q)) * TSf(itran, K, Ku, -Q, Qu, 1, jkq);
                }
            }
        } else {
            if (Qu < 0) {
                ts = -2.0 * TSf(itran, K, Ku, Q, -Qu, -1, jkq);
            } else {
                ts = TSf(itran, K, Ku, Q, Qu, 1, jkq);

                if (Qu > 0) {
                    ts *= 2.0;
                }
            }
        }
    }

    /* Jacobi iteration coefficient */
    if (K == 0) {
        if (K == Ku && Q == Qu) {
            /*
            *cts = g_atom.Bul[itran]*
                  ((double)g_atom.J2[g_atom.iu[itran]] + 1.0)*
                  ((double)ZNAM(1+K+(g_atom.J2[g_atom.il[itran]] +
                                     g_atom.J2[g_atom.iu[itran]])/2))*
                   mat_Cou6j(g_atom.J2[g_atom.il[itran]],
                             g_atom.J2[g_atom.il[itran]],2*K,
                             g_atom.J2[g_atom.iu[itran]],
                             g_atom.J2[g_atom.iu[itran]],2);
            */
            *cts = g_atom.Bul[itran] *
                   sqrt(((double)g_atom.J2[g_atom.iu[itran]] + 1.0) /
                        ((double)g_atom.J2[g_atom.il[itran]] + 1.0));
        }
    }

    /* Common factors */
    ts *= g_atom.Bul[itran] * ((double)g_atom.J2[g_atom.iu[itran]] + 1.0) *
          sqrt(3.0 * (2.0 * ((double)K) + 1.0) * (2.0 * ((double)Ku) + 1.0)) *
          ((double)ZNAM(Ku + Qu));

    return ts;
}

/****************************************************************************/
/**
 * Computes the spontaneous emission transfer rate given the multipolar
 * indexes and the transition
 *
 * @brief Spontaneous emission transfer rate
 * @param [in] K K index of the row of the statistical equilibrium
 *               equation
 * @param [in] Ku K index of the column of the statistical equilibrium
 *                equation
 * @param [in] Q Q index of the multipolar component for which the row
 *               of the statistical equilibrium equation is being
 *               calculated
 * @param [in] Qu Q index of the multipolar component for which the
 *                column of the statistical equilibrium equation is
 *                being calculated
 * @param [in] itran Index of the transition
 * @return double Spontaneous emission transfer rate
 *
 ****************************************************************************/
static double TE(int K, int Q, int Ku, int Qu, int itran)
{
    double te = 0.0;

    /* Has to be diagonal in multipoles */
    if (K != Ku) {
        return te;
    }
    if (Q != Qu) {
        return te;
    }

    te = ((double)g_atom.J2[g_atom.iu[itran]] + 1.0) * g_atom.Aul[itran] *
         ((double)ZNAM(
             1 + K +
             (g_atom.J2[g_atom.iu[itran]] + g_atom.J2[g_atom.il[itran]]) / 2)) *
         mat_Cou6j(g_atom.J2[g_atom.iu[itran]], g_atom.J2[g_atom.iu[itran]],
                   K * 2, g_atom.J2[g_atom.il[itran]],
                   g_atom.J2[g_atom.il[itran]], 2);

    return te;
}

/****************************************************************************/
/**
 * Computes the absorption relaxation rate given the multipolar
 * indexes, the transition, the radiation field tensors, and if the
 * real or the imaginary part must be computed
 *
 * @brief Reduced absorption relaxation rate
 * @param [in] itran Index of the transition
 * @param [in] K K index of the row of the statistical equilibrium
 *               equation
 * @param [in] Kp K index of the column of the statistical equilibrium
 *                equation
 * @param [in] Q Q index of the multipolar component for which the row
 *               of the statistical equilibrium equation is being
 *               calculated
 * @param [in] Qp Q index of the multipolar component for which the
 *                column of the statistical equilibrium equation is
 *                being calculated
 * @param [in] real 1 if the real part of the rate is to be computed,
 *                  -1 if the imaginary part is to be computed
 * @param [in] jkq Radiation field tensors for the itran transition
 * @return double Reduced absorption relaxation rate
 *
 ****************************************************************************/
static double RAf(int itran, int K, int Kp, int Q, int Qp, int real,
                  double *jkq)
{
    double ra = 0.0;
    int    minKr, maxKr, Qr, J2u, J2l, jind;

    /* Angular momentums */
    J2u = g_atom.J2[g_atom.iu[itran]];
    J2l = g_atom.J2[g_atom.il[itran]];

    /* Compute Qr value */
    Qr = Qp - Q;

    /* Check if we have to return */
    if (Qr < -2 || Qr > 2) {
        return ra;
    }

    /* Compute limits for K */
    minKr = abs(K - Kp);
    maxKr = K + Kp;
    if (maxKr > 2) {
        maxKr = 2;
    }
    if (minKr < abs(Qr)) {
        minKr = abs(Qr);
    }
    if (minKr > maxKr) {
        return ra;
    }

    /* For each value of Kr */
    for (int Kr = minKr; Kr <= maxKr; Kr++) {
        /* Epsilon plus factor */
        if ((K + Kp + Kr) % 2) {
            continue;
        }

        double fact = sqrt(2.0 * ((double)Kr) + 1.0) * ((double)ZNAM(Kr)) *
                      mat_Cou6j(K * 2, Kp * 2, Kr * 2, J2l, J2l, J2l) *
                      mat_Cou6j(2, 2, Kr * 2, J2l, J2l, J2u);

        /* J index for Kr0 */
        if (Kr > 1) {
            jind = 4;
        } else {
            jind = Kr;
        }

        /* Branch Qr */
        if (Qr < 0) {
            /* Get absolute and factor */
            Qr = -Qr;
            double ral =
                ((double)ZNAM(Qr)) * fact *
                mat_Cou3j(K * 2, Kp * 2, Kr * 2, Q * 2, -Qp * 2, -Qr * 2);

            /* Real part */
            if (real > 0) {
                ra += ral * jkq[jind + Qr * 2 - 1];

            }
            /* Imaginary part */
            else {
                ra -= ral * jkq[jind + Qr * 2];
            }
        } else {
            if (Qr > 0) {
                /* Get factor */
                double ral = fact * mat_Cou3j(K * 2, Kp * 2, Kr * 2, Q * 2,
                                              -Qp * 2, Qr * 2);

                /* Real part */
                if (real > 0) {
                    ra += ral * jkq[jind + Qr * 2 - 1];
                }
                /* Imaginary part */
                else {
                    ra += ral * jkq[jind + Qr * 2];
                }
            } else {
                /* Real part */
                if (real > 0) {
                    ra += fact * jkq[jind] *
                          mat_Cou3j(K * 2, Kp * 2, Kr * 2, Q * 2, -Qp * 2, 0);
                }
            }
        }
    }
    return ra;
}

/****************************************************************************/
/**
 * Computes the absorption relaxation rate given the multipolar
 * indexes, the transition and the radiation field tensors, and also
 * the coefficient needed for the Jacobi method. It calls the reduced
 * absorption transfer rate routine in the way necessary to get the
 * only real statistical equilibrium equations
 *
 * @brief Absorption relaxation rate
 * @param [in] K K index of the row of the statistical equilibrium
 *               equation
 * @param [in] Kp K index of the column of the statistical equilibrium
 *                equation
 * @param [in] Q Q index of the multipolar component for which the row
 *               of the statistical equilibrium equation is being
 *               calculated
 * @param [in] Qp Q index of the multipolar component for which the
 *                column of the statistical equilibrium equation is
 *                being calculated
 * @param [in] itran Index of the transition
 * @param [in] jkq Radiation field tensors for the itran transition
 * @param [out] cra Coefficient for Jacobi iteration
 * @return double Absorption relaxation rate
 *
 ****************************************************************************/
static double RA(int K, int Q, int Kp, int Qp, int itran, double *jkq,
                 double *cra)
{
    double ra = 0.0;

    *cra = 0.0;

    /* Imaginary equation */
    if (Q < 0) {
        if (Qp < 0) {
            ra = RAf(itran, K, Kp, -Q, -Qp, 1, jkq) -
                 ((double)ZNAM(-Q)) * RAf(itran, K, Kp, Q, -Qp, 1, jkq);
        } else {
            ra = RAf(itran, K, Kp, -Q, Qp, -1, jkq);

            if (Qp > 0) {
                ra -= ((double)ZNAM(-Q)) * RAf(itran, K, Kp, Q, Qp, -1, jkq);
            }
        }
    }
    /* Real equation */
    else {
        if (Q > 0) {
            if (Qp < 0) {
                ra = -RAf(itran, K, Kp, Q, -Qp, -1, jkq) -
                     ((double)ZNAM(Q)) * RAf(itran, K, Kp, -Q, -Qp, -1, jkq);
            } else {
                ra = RAf(itran, K, Kp, Q, Qp, 1, jkq);

                if (Qp > 0) {
                    ra += ((double)ZNAM(Q)) * RAf(itran, K, Kp, -Q, Qp, 1, jkq);
                }
            }
        } else {
            if (Qp < 0) {
                ra = -2.0 * RAf(itran, K, Kp, Q, -Qp, -1, jkq);
            } else {
                ra = RAf(itran, K, Kp, Q, Qp, 1, jkq);

                if (Qp > 0) {
                    ra *= 2.0;
                }
            }
        }
    }

    /* Jacobi iteration coefficient */
    if (K == 0) {
        if (K == Kp && Q == Qp) {
            *cra = g_atom.Blu[itran];
        }
    }

    /* Common factors */
    ra *= g_atom.Blu[itran] * ((double)g_atom.J2[g_atom.il[itran]] + 1.0) *
          sqrt(3.0 * (2.0 * ((double)K) + 1.0) * (2.0 * ((double)Kp) + 1.0)) *
          ((double)ZNAM(
              1 + Qp +
              (g_atom.J2[g_atom.iu[itran]] - g_atom.J2[g_atom.il[itran]]) / 2));

    return ra;
}

/****************************************************************************/
/**
 * Computes the stimulated emission relaxation rate given the
 * multipolar indexes, the transition, the radiation field tensors, and
 * if the real or the imaginary part must be computed
 *
 * @brief Reduced stimulated emission relaxation rate
 * @param [in] itran Index of the transition
 * @param [in] K K index of the row of the statistical equilibrium
 *               equation
 * @param [in] Kp K index of the column of the statistical equilibrium
 *                equation
 * @param [in] Q Q index of the multipolar component for which the row
 *               of the statistical equilibrium equation is being
 *               calculated
 * @param [in] Qp Q index of the multipolar component for which the
 *                column of the statistical equilibrium equation is
 *                being calculated
 * @param [in] real 1 if the real part of the rate is to be computed,
 *                  -1 if the imaginary part is to be computed
 * @param [in] jkq Radiation field tensors for the itran transition
 * @return double Reduced stimulated emission relaxation rate
 *
 ****************************************************************************/
static double RSf(int itran, int K, int Kp, int Q, int Qp, int real,
                  double *jkq)
{
    double rs = 0.0;
    int    minKr, maxKr, Qr, J2u, J2l, jind;

    /* Angular momentums */
    J2u = g_atom.J2[g_atom.iu[itran]];
    J2l = g_atom.J2[g_atom.il[itran]];

    /* Compute Qr value */
    Qr = Qp - Q;

    /* Check if we have to return */
    if (Qr < -2 || Qr > 2) {
        return rs;
    }

    /* Compute limits for K */
    minKr = abs(K - Kp);
    maxKr = K + Kp;
    if (maxKr > 2) {
        maxKr = 2;
    }
    if (minKr < abs(Qr)) {
        minKr = abs(Qr);
    }
    if (minKr > maxKr) {
        return rs;
    }

    /* For each value of Kr */
    for (int Kr = minKr; Kr <= maxKr; Kr++) {
        /* Epsilon plus factor */
        if ((K + Kp + Kr) % 2) {
            continue;
        }

        double fact = sqrt(2.0 * ((double)Kr) + 1.0) *
                      mat_Cou6j(K * 2, Kp * 2, Kr * 2, J2u, J2u, J2u) *
                      mat_Cou6j(2, 2, Kr * 2, J2u, J2u, J2l);

        /* J index for Kr0 */
        if (Kr > 1) {
            jind = 4;
        } else {
            jind = Kr;
        }

        /* Branch Qr */
        if (Qr < 0) {
            /* Get absolute and factor */
            Qr = -Qr;
            double rsl =
                ((double)ZNAM(Qr)) * fact *
                mat_Cou3j(K * 2, Kp * 2, Kr * 2, Q * 2, -Qp * 2, -Qr * 2);

            /* Real part */
            if (real > 0) {
                rs += rsl * jkq[jind + Qr * 2 - 1];
            }
            /* Imaginary part */
            else {
                rs -= rsl * jkq[jind + Qr * 2];
            }
        } else {
            if (Qr > 0) {
                /* Get factor */
                double rsl = fact * mat_Cou3j(K * 2, Kp * 2, Kr * 2, Q * 2,
                                              -Qp * 2, Qr * 2);

                /* Real part */
                if (real > 0) {
                    rs += rsl * jkq[jind + Qr * 2 - 1];
                }
                /* Imaginary part */
                else {
                    rs += rsl * jkq[jind + Qr * 2];
                }
            } else {
                /* Real part */
                if (real > 0) {
                    rs += fact * jkq[jind] *
                          mat_Cou3j(K * 2, Kp * 2, Kr * 2, Q * 2, -Qp * 2, 0);
                }
            }
        }
    }
    return rs;
}

/****************************************************************************/
/**
 * Computes the stimulated emission relaxation rate given the
 * multipolar indexes, the transition and the radiation field tensors,
 * and also the coefficient needed for the Jacobi method. It calls the
 * reduced absorption transfer rate routine in the way necessary to get
 * the only real statistical equilibrium equations
 *
 * @brief Stimulated emission relaxation rate
 * @param [in] K K index of the row of the statistical equilibrium
 *               equation
 * @param [in] Kp K index of the column of the statistical equilibrium
 *                equation
 * @param [in] Q Q index of the multipolar component for which the row
 *               of the statistical equilibrium equation is being
 *               calculated
 * @param [in] Qp Q index of the multipolar component for which the
 *                column of the statistical equilibrium equation is
 *                being calculated
 * @param [in] itran Index of the transition
 * @param [in] jkq Radiation field tensors for the itran transition
 * @param [out] crs Coefficient for Jacobi iteration
 * @return double Stimulated emission relaxation rate
 *
 ****************************************************************************/
static double RS(int K, int Q, int Kp, int Qp, int itran, double *jkq,
                 double *crs)
{
    double rs = 0.0;

    *crs = 0.0;

    /* Imaginary equation */
    if (Q < 0) {
        if (Qp < 0) {
            rs = RSf(itran, K, Kp, -Q, -Qp, 1, jkq) -
                 ((double)ZNAM(-Q)) * RSf(itran, K, Kp, Q, -Qp, 1, jkq);
        } else {
            rs = RSf(itran, K, Kp, -Q, Qp, -1, jkq);

            if (Qp > 0) {
                rs -= ((double)ZNAM(-Q)) * RSf(itran, K, Kp, Q, Qp, -1, jkq);
            }
        }
    }
    /* Real equation */
    else {
        if (Q > 0) {
            if (Qp < 0) {
                rs = -RSf(itran, K, Kp, Q, -Qp, -1, jkq) -
                     ((double)ZNAM(Q)) * RSf(itran, K, Kp, -Q, -Qp, -1, jkq);
            } else {
                rs = RSf(itran, K, Kp, Q, Qp, 1, jkq);

                if (Qp > 0) {
                    rs += ((double)ZNAM(Q)) * RSf(itran, K, Kp, -Q, Qp, 1, jkq);
                }
            }
        } else {
            if (Qp < 0) {
                rs = -2.0 * RSf(itran, K, Kp, Q, -Qp, -1, jkq);
            } else {
                rs = RSf(itran, K, Kp, Q, Qp, 1, jkq);

                if (Qp > 0) {
                    rs *= 2.0;
                }
            }
        }
    }

    /* Jacobi iteration coefficient */
    if (K == 0) {
        if (K == Kp && Q == Qp) {
            *crs = g_atom.Bul[itran];
        }
    }

    /* Common factors */
    rs *= g_atom.Bul[itran] * ((double)g_atom.J2[g_atom.iu[itran]] + 1.0) *
          sqrt(3.0 * (2.0 * ((double)K) + 1.0) * (2.0 * ((double)Kp) + 1.0)) *
          ((double)ZNAM(
              1 + Qp +
              (g_atom.J2[g_atom.il[itran]] - g_atom.J2[g_atom.iu[itran]]) / 2));

    return rs;
}

/****************************************************************************/
/**
 * Computes the spontaneous emission relaxation rate given the
 * multipolar indexes and the transition index
 *
 * @brief Spontaneous emission relaxation rate
 * @param [in] K K index of the row of the statistical equilibrium
 *               equation
 * @param [in] Kp K index of the column of the statistical equilibrium
 *                equation
 * @param [in] Q Q index of the multipolar component for which the row
 *               of the statistical equilibrium equation is being
 *               calculated
 * @param [in] Qp Q index of the multipolar component for which the
 *                column of the statistical equilibrium equation is
 *                being calculated
 * @param [in] itran Index of the transition
 * @return double Spontaneous emission relaxation rate
 *
 ****************************************************************************/
static double RE(int K, int Q, int Kp, int Qp, int itran)
{
    double re = 0.0;

    if (K != Kp) {
        return re;
    }
    if (Q != Qp) {
        return re;
    }

    re = g_atom.Aul[itran];

    return re;
}

/****************************************************************************/
/**
 * Compute a rotation tensor element to be used in the calculation of
 * the coefficients of the Magnetic Kernel
 *
 * @brief Rotation tensor element for magnetic Kernel
 * @param [in] j K Multipolar component of the rotation
 * @param [in] m Q Multipolar component of the rotation
 * @param [in] n Q' Multipolar component of the rotation
 * @param [in] beta Angle of the rotation
 * return double Rotation tensor element
 *
 ****************************************************************************/
static double drot(int j, int m, int n, double beta)
{
    int    tmin, tmax;
    double c0, s0, ssum, c, s, out, sgn, d;

    out = 0.0;

    if (abs(m) > j || abs(n) > j) {
        return out;
    }

    c0 = cos(0.5 * beta);
    s0 = sin(0.5 * beta);

    ssum = 0.0;
    tmin = MAX2(0, m - n);
    tmax = MIN2(j - n, j + m);

    for (int t = tmin; t <= tmax; t++) {
        c = 1.0;
        s = 1.0;

        for (int i = 1; i <= (2 * j + m - n - 2 * t); i++) {
            c *= c0;
        }
        for (int i = 1; i <= (2 * t - m + n); i++) {
            s *= s0;
        }

        /* Sign (-1)^t */
        if (t % 2 == 0) {
            sgn = 1.0;
        } else {
            sgn = -1.0;
        }

        c = sgn * c * s;
        s = mat_Factorial(j + m - t);
        s *= mat_Factorial(j - n - t);
        s *= mat_Factorial(t);
        s *= mat_Factorial(t + n - m);
        c /= s;

        ssum += c;
    }

    d = mat_Factorial(j + m);
    d *= mat_Factorial(j - m);
    d *= mat_Factorial(j + n);
    d *= mat_Factorial(j - n);
    d = sqrt(d);
    d *= ssum;

    return d;
}

/****************************************************************************/
/**
 * Computes the magnetic kernel for a given level, multipolar
 * component, and magnetic field in a node
 *
 * @brief Computes magnetic kernel
 * @param [in] ilv Atomic level index
 * @param [in] K K multipolar component of the row/column of the
 *               statistical equilibrium equation
 * @param [in] p_node Node data
 * @param [out] MKQQp Magnetic kernel for the requested level and
 *                    K multipolar component
 * @return void
 *
 ****************************************************************************/
static void GetMKernel(int ilv, int K, t_node *p_node, double **MKQQp)
{
    m_Node * p;
    int      maxK, len, sind, sindp;
    double   B, larmor, chi_b, theta_b, sign, ct, st, cc, sc;
    double **aa;
    t_vec_3d Bv;

    /* Zero matrix */
    /*maxK = g_atom.maxK[ilv];
      len = (maxK + 1)*(maxK + 1);*/
    len = (2 * K + 1);
    for (int i = 0; i < len; i++) {
        for (int j = 0; j < len; j++) {
            MKQQp[i][j] = 0.0;
        }
    }

    /* Check K */
    if (K == 0) {
        return;
    }

    /* Magnetic field */
    p = (m_Node *)p_node->p_data;
    B = sqrt(p->Bx * p->Bx + p->By * p->By + p->Bz * p->Bz);

    /* Check if there is field */
    if (B <= 0.0) {
        return;
    }

    /* Get larmor element */
    larmor = -2.0 * M_PI * 1.3996e6 * B * g_atom.gL[ilv];

    /* Allocate auxiliar rotations */
    aa    = (double **)malloc((K + 1) * sizeof(double *));
    aa[0] = (double *)malloc((K + 1) * (2 * K + 1) * sizeof(double));
    for (int i = 1; i < (K + 1); i++) {
        aa[i] = aa[0] + i * (2 * K + 1);
    }

    /* Get magnetic field direction */
    Bv.x = p->Bx / B;
    Bv.y = p->By / B;
    Bv.z = p->Bz / B;
    VecUnit2Ang(Bv, &theta_b, &chi_b);

    /* Compute auxiliar rotation matrices */
    for (int Q = 0; Q <= K; Q++) {
        for (int Qp = -K; Qp <= K; Qp++) {
            aa[Q][Qp + K] = 0.0;

            for (int Qpp = -K; Qpp <= K; Qpp++) {
                aa[Q][Qp + K] += ((double)Qpp) * drot(K, Qpp, Q, -theta_b) *
                                 drot(K, Qpp, Qp, -theta_b);
            }
        }
    }

    /* Compute actual Kernel */

    /* Q=Qp=0 */
    MKQQp[0][0] = aa[0][K];

    /* Q=0 */
    sindp = 0;
    for (int Qp = 1; Qp <= K; Qp++) {
        double Qpchi = ((double)Qp) * chi_b;

        sign = (double)ZNAM(Qp);
        cc   = cos(Qpchi);
        sc   = sin(Qpchi);

        /* Real */
        sindp += 1;
        MKQQp[0][sindp] = -aa[0][Qp + K] * sc + sign * aa[0][K - Qp] * sc;
        /* Imaginary */
        sindp += 1;
        MKQQp[0][sindp] = -aa[0][Qp + K] * cc + sign * aa[0][K - Qp] * cc;
    }

    /* Rest of matrix */
    sind = 0;
    for (int Q = 1; Q <= K; Q++) {
        double Qchi = ((double)Q) * chi_b;

        ct = cos(Qchi);
        st = sin(Qchi);

        sind += 1;
        MKQQp[sind][0] = aa[Q][K] * st;
        sind += 1;
        MKQQp[sind][0] = aa[Q][K] * ct;

        sindp = 1;
        for (int Qp = 1; Qp <= K; Qp++) {
            int    sindi = sind - 1;
            double Qpchi = ((double)Qp) * chi_b;

            sign = (double)ZNAM(Qp);
            cc   = cos(Qpchi);
            sc   = sin(Qpchi);

            MKQQp[sindi][sindp] = -aa[Q][Qp + K] * (ct * sc - st * cc) +
                                  aa[Q][K - Qp] * (ct * sc + st * cc) * sign;
            sindp += 1;
            MKQQp[sindi][sindp] = -aa[Q][Qp + K] * (ct * cc + st * sc) +
                                  aa[Q][K - Qp] * (ct * cc - st * sc) * sign;
            sindp -= 1;
            sindi += 1;
            MKQQp[sindi][sindp] = aa[Q][Qp + K] * (ct * cc + st * sc) +
                                  aa[Q][K - Qp] * (ct * cc - st * sc) * sign;
            sindp += 1;
            MKQQp[sindi][sindp] = -aa[Q][Qp + K] * (ct * sc - st * cc) -
                                  aa[Q][K - Qp] * (ct * sc + st * cc) * sign;
            sindp += 1;
        }
    }

    /* Apply factor */
    for (int i = 0; i < len; i++) {
        for (int j = 0; j < len; j++) {
            MKQQp[i][j] *= larmor;
        }
    }

    /* Free memory */
    free(aa[0]);
    free(aa);
}

/****************************************************************************/
/**
 * Puts into an array the radiation field tensors for a given
 * transition
 *
 * @brief Returns radiation field tensors of a transition in an array
 * @param [in] p_node Node data
 * @param [in] itran Transition index
 * @param [out] jkq Radiation field tensors for the requested
 *                  transition
 * @return void
 *
 ****************************************************************************/
static void GetJKQ(t_node *p_node, int itran, double *jkq)
{
    m_Node *p;
    int     Ri;

    /* Get node data */
    p = (m_Node *)p_node->p_data;

    /* Translate into t_jkq */
    Ri = g_atom.i_J[itran][0][0];

    for (int i = 0; i < NJKQ; i++) {
        jkq[i] = p->J[Ri + i];
    }
}

/****************************************************************************/
/**
 * Build the system of statistical equilibrium equations. It is build
 * in such a way that only real quantities are computed and used. The
 * statistical equilibrium equations have been combined such to have
 * the following variables: rho(0)^0_0, rho(0)^1_0, Re[rho(0)^1_1],
 * Im[rho(0)^1_1], rho(0)^2_0, ..., rho(1)^0_0,... It applies the
 * Jacobi iteration scheme
 *
 * @brief Gets the preconditioned statistical equilibrium equations
 *        matrix and the right hand side
 * @param [in] p_node Node data
 * @param [out] ese Statistical equilibrium equations
 * @param [out] f Right hand side of statistical equilibrium equations
 * @return void
 * @todo Right now, the depolarizing collisions are considered the same
 *       for every K multipole, and only one is in the input for each
 *       level. This needs to be discussed
 *
 ****************************************************************************/
static void F_GetESE(t_node *p_node, double **ese, double *f)
{
    m_Node * p;
    int      i, j, itran, il, iu, K, Kp, Q, Qp, Ri, Rj, Ril, Riu, iQ, iQp;
    double   cta, cra, cts, crs;
    double   gl, gu, Clu, w6j, w6jul, w6jlu, d1, d2;
    double   rho00l, rho00u, rhoKQold;
    double   g[g_atom.nlevel];
    double   jkq[NJKQ];
    double **MKQQp;

    /* Get node data */
    p = (m_Node *)p_node->p_data;

    /* Allocate magnetic kernel */
    {
        int len  = (2 * g_atom.MaxK + 1);
        MKQQp    = (double **)malloc(len * sizeof(double *));
        MKQQp[0] = (double *)malloc(len * len * sizeof(double));
        for (int i = 1; i < len; i++) {
            MKQQp[i] = MKQQp[0] + i * len;
        }
    }

    /* First row and pre-compute statistical weights */
    Ri = 0;
    for (j = 0; j < g_atom.nlevel; j++) {
        Rj          = g_atom.i_rho[j][0][0];
        g[j]        = ((double)g_atom.J2[j]) + 1.0;
        ese[Ri][Rj] = sqrt(g[j]);
    }

    /* For each existing transition of any kind */
    for (itran = 0; itran < g_atom.ntran; itran++) {
        /* Get upper and lower levels */
        il  = g_atom.il[itran];
        Ril = g_atom.i_rho[il][0][0];
        iu  = g_atom.iu[itran];
        Riu = g_atom.i_rho[iu][0][0];

        /* Get statistical weights */
        gl = g[il];
        gu = g[iu];

        /* Collisional contribution */
        if (p->Cul[itran] > 0.0) {
            /* Reverse rate */
            Clu = p->Cul[itran] * gu * exp(g_atom.dEc[itran] / p->T) / gl;

            /* Compute w-6j for K=0 */
            w6jlu = mat_Cou6j(g_atom.J2[iu], g_atom.J2[iu], 0, g_atom.J2[il],
                              g_atom.J2[il], 2);
            w6jul = mat_Cou6j(g_atom.J2[il], g_atom.J2[il], 0, g_atom.J2[iu],
                              g_atom.J2[iu], 2);

            /* Equation for lower level */
            Ri = Ril - 1;
            Rj = Riu - 1;

            /* For each K */
            for (K = 0; K <= g_atom.maxK[il]; K++) {
                /* 6J factors */
                /* Skip C^(K) if it is not permitted by
                   electric dipole rules */
                if (K > 0 && itran < g_atom.rtran) {
                    if (K <= g_atom.maxK[iu]) {
                        d1 = ((double)ZNAM(K)) *
                             mat_Cou6j(g_atom.J2[il], g_atom.J2[il], 2 * K,
                                       g_atom.J2[iu], g_atom.J2[iu], 2) /
                             w6jul;
                    } else {
                        d1 = 1.0;
                    }
                } else {
                    d1 = 1.0;
                }

                /* For each Q */
                for (Q = 0; Q < (2 * K + 1); Q++) {
                    Ri++;
                    Rj++;

                    /* Do not touch first row */
                    if (Ri == 0) {
                        continue;
                    }

                    /* Relaxation rate */
                    ese[Ri][Ri] -= Clu;

                    /* Check K of upper level can be diagonal */
                    if (K > g_atom.maxK[iu]) {
                        continue;
                    }

                    /* Skip C^(K) if it is not permitted by electric
                       dipole rules */
                    if (K > 0 && itran > g_atom.rtran) {
                        continue;
                    }

                    /* Transfer rate */
                    ese[Ri][Rj] += sqrt(gu / gl) * p->Cul[itran] * d1;

                } /* Q lower level */
            }     /* K lower level */

            /* Equation for upper level */
            Ri = Riu - 1;
            Rj = Ril - 1;

            /* For each K */
            for (K = 0; K <= g_atom.maxK[iu]; K++) {
                /* 6J factors */
                /* Skip C^(K) if it is not permitted by
                   electric dipole rules */
                if (K > 0 && itran < g_atom.rtran) {
                    if (K <= g_atom.maxK[il]) {
                        d1 = ((double)ZNAM(K)) *
                             mat_Cou6j(g_atom.J2[iu], g_atom.J2[iu], 2 * K,
                                       g_atom.J2[il], g_atom.J2[il], 2) /
                             w6jlu;
                    } else {
                        d1 = 1.0;
                    }
                } else {
                    d1 = 1.0;
                }

                /* For each Q */
                for (Q = 0; Q < (2 * K + 1); Q++) {
                    Ri++;
                    Rj++;

                    /* Relaxation rate */
                    ese[Ri][Ri] -= p->Cul[itran];

                    /* Check K of lower level can be diagonal */
                    if (K > g_atom.maxK[il]) {
                        continue;
                    }

                    /* Skip C^(K) if it is not permitted by electric
                       dipole rules */
                    if (K > 0 && itran > g_atom.rtran) {
                        continue;
                    }

                    /* Transfer rate */
                    ese[Ri][Rj] += sqrt(gl / gu) * Clu * d1;

                } /* Q upper level */
            }     /* K upper level */
        }         /* If non-zero collisional rate */

        /* If no more radiative transitions, skip */
        if (itran >= g_atom.rtran) {
            continue;
        }

        /* Get rho00 for ALI */
        rho00l = p->rho[Ril];
        rho00u = p->rho[Riu];

        /* Get jkq into local vector */
        GetJKQ(p_node, itran, jkq);

        /**/
        /* Transfer rate for lower level */
        /**/
        Ri = Ril - 1;

        /* For each K */
        for (K = 0; K <= g_atom.maxK[il]; K++) {
            /* For each Q */
            for (iQ = 0; iQ < (2 * K + 1); iQ++) {
                Q = g_Qind[iQ];
                Ri++;

                /* Do not touch first row */
                if (Ri == 0) {
                    continue;
                }

                Rj = Riu - 1;

                /* For each K' */
                for (Kp = 0; Kp <= g_atom.maxK[iu]; Kp++) {
                    /* For each Q' */
                    for (iQp = 0; iQp < (2 * Kp + 1); iQp++) {
                        Qp = g_Qind[iQp];
                        Rj++;
                        ese[Ri][Rj] += TE(K, Q, Kp, Qp, itran);
                        ese[Ri][Rj] += TS(K, Q, Kp, Qp, itran, jkq, &cts);

                        /* ALI corrections */
                        if (K == Kp && Q == Qp && Q > 0) {
                            rhoKQold = p->rho[Rj];
                            ese[Ri][Riu] += cts * rhoKQold * p->psi_eta[itran];
                            ese[Ri][Rj] -= cts * rho00u * p->psi_eta[itran];
                        }
                    }
                }
            }
        }

        /**/
        /* Transfer rate for upper level */
        /**/
        Ri = Riu - 1;

        /* For each K */
        for (K = 0; K <= g_atom.maxK[iu]; K++) {
            /* For each Q */
            for (iQ = 0; iQ < (2 * K + 1); iQ++) {
                Q = g_Qind[iQ];
                Ri++;
                Rj = Ril - 1;

                /* For each K' */
                for (Kp = 0; Kp <= g_atom.maxK[il]; Kp++) {
                    /* For each Q' */
                    for (iQp = 0; iQp < (2 * Kp + 1); iQp++) {
                        Qp = g_Qind[iQp];
                        Rj++;
                        ese[Ri][Rj] += TA(K, Q, Kp, Qp, itran, jkq, &cta);

                        /* ALI corrections */
                        if (K == Kp && Q == Qp) {
                            rhoKQold = p->rho[Rj];
                            ese[Ri][Riu] += cta * rhoKQold * p->psi_eta[itran];
                            ese[Ri][Rj] -= cta * rho00u * p->psi_eta[itran];
                        }
                    }
                }
            }
        }

        /**/
        /* Relaxation rate for lower level */
        /**/
        Ri = Ril - 1;

        /* For each K */
        for (K = 0; K <= g_atom.maxK[il]; K++) {
            /* For each Q */
            for (iQ = 0; iQ < (2 * K + 1); iQ++) {
                Q = g_Qind[iQ];
                Ri++;

                /* Do not touch first row */
                if (Ri == 0) {
                    continue;
                }

                Rj = Ril - 1;

                /* For each K' */
                for (Kp = 0; Kp <= g_atom.maxK[il]; Kp++) {
                    /* For each Q' */
                    for (iQp = 0; iQp < (2 * Kp + 1); iQp++) {
                        Qp = g_Qind[iQp];
                        Rj++;
                        ese[Ri][Rj] -= RA(K, Q, Kp, Qp, itran, jkq, &cra);

                        /* ALI corrections */
                        if (K == Kp && Q == Qp) {
                            rhoKQold = p->rho[Rj];
                            ese[Ri][Riu] -= cra * rhoKQold * p->psi_eta[itran];
                            ese[Ri][Rj] += cra * rho00u * p->psi_eta[itran];
                        }
                    }
                }
            }
        }

        /**/
        /* Relaxation rate for upper level */
        /**/
        Ri = Riu - 1;

        /* For each K */
        for (K = 0; K <= g_atom.maxK[iu]; K++) {
            /* For each Q */
            for (iQ = 0; iQ < (2 * K + 1); iQ++) {
                Q = g_Qind[iQ];
                Ri++;
                Rj = Riu - 1;

                /* For each K' */
                for (Kp = 0; Kp <= g_atom.maxK[iu]; Kp++) {
                    /* For each Q' */
                    for (iQp = 0; iQp < (2 * Kp + 1); iQp++) {
                        Qp = g_Qind[iQp];
                        Rj++;
                        ese[Ri][Rj] -= RE(K, Q, Kp, Qp, itran);
                        ese[Ri][Rj] -= RS(K, Q, Kp, Qp, itran, jkq, &crs);

                        /* ALI corrections */
                        if (K == Kp && Q == Qp && Q > 0) {
                            rhoKQold = p->rho[Rj];
                            ese[Ri][Riu] -= crs * rhoKQold * p->psi_eta[itran];
                            ese[Ri][Rj] += crs * rho00u * p->psi_eta[itran];
                        }
                    }
                }
            }
        }
    } /* For every transition */

    /* Magnetic Kernel and elastic collisions */
    Ri = -1;
    for (i = 0; i < g_atom.nlevel; i++) {
        /* Skip the K=0 */
        Ri++;

        for (K = 1; K <= g_atom.maxK[i]; K++) {
            /* Save last index */
            Ril = Ri;

            /* Get Magnetic Kernel */
            GetMKernel(i, K, p_node, MKQQp);

            il = -1;
            for (Q = -K; Q <= K; Q++) {
                Ri++;
                il++;
                Rj = Ril;
                iu = -1;

                /* Elastic collisions */
                ese[Ri][Ri] -= p->DK[i];

                for (Qp = -K; Qp <= K; Qp++) {
                    Rj++;
                    iu++;
                    ese[Ri][Rj] += MKQQp[il][iu];
                }
            }
        }
    }
    /* End of Magnetic Kernel and elastic collisions */

    /* Finish right hand side */
    f[0] = 1.0;
    for (i = 1; i < g_atom.rdim; i++) {
        f[i] = 0.0;
    }

    /* Free magnetic Kernel */
    free(MKQQp[0]);
    free(MKQQp);
}

/****************************************************************************/
/**
 * Return number of real density matrix elements
 *
 * @brief Return number of real density matrix elements
 * @return int Number of density matrix components
 *
 ****************************************************************************/
int F_GetNDM(void) { return g_atom.rdim; }

/****************************************************************************/
/**
 * Gets a flag array with 1 in the indexes of the population components
 * in the density matrix array and 0 otherwise
 *
 * @brief Returns pointer to array of F_GetNDM() density matrix
 *        elements with flagged population positions
 * @return int* Array of flags for population indexes
 *
 ****************************************************************************/
int *F_GetPopsPositions(void)
{
    int *pops;

    /* Initialize pointer */
    pops = (int *)Malloc(g_atom.rdim * sizeof(int));
    for (int i = 0; i < g_atom.rdim; i++)
        pops[i] = 0;

    /* Identify populations */
    for (int i = 0; i < g_atom.nlevel; i++) {
        pops[g_atom.i_rho[i][0][0]] = 1;
    }
    return pops;
}

/****************************************************************************/
/**
 * Gets the density matrix elements in a vector from a given node
 *
 * @brief Get array of real density matrix data in given node
 * @param [in] p_node Node data
 * @param [out] data Array with the density matrix elements of the node
 * @return void
 *
 ****************************************************************************/
static void F_GetDM(t_node *p_node, double *data)
{
    m_Node *p;

    p = (m_Node *)p_node->p_data;

    for (int ir = 0; ir < g_atom.rdim; ir++) {
        data[ir] = p->rho[ir];
    }
}

/****************************************************************************/
/**
 * Changes the density matrix elements of a node to the values
 * indicated
 *
 * @brief Sets the density matrix elements of a node
 * @param [in] p_node Node data
 * @param [in] data Array with the density matrix elements to be set
 *
 ****************************************************************************/
static void F_SetDM(t_node *p_node, double *data)
{
    m_Node *p;

    p = (m_Node *)p_node->p_data;

    for (int ir = 0; ir < g_atom.rdim; ir++) {
        p->rho[ir] = data[ir];
    }
}

/****************************************************************************/
/**
 * Solves the statistical equilibrium equations for the node and
 * returns the maximum relative change of the populations
 *
 * @brief Solve the statistical equilibrium equations in a given node
 *        and return maximum relative change of populations
 * @param [in] p_node Node data
 * @return double Maximum relative change of populations
 *
 ****************************************************************************/
double F_SolveESE(t_node *p_node)
{
    m_Node * p;
    double   mrc, maxrc = 0.0;
    double   rhs[g_atom.rdim];
    double   rho_old[g_atom.rdim];
    double **ese;

    /* Get node data */
    p = (m_Node *)p_node->p_data;

    /* Allocate SEE */
    ese    = (double **)malloc(g_atom.rdim * sizeof(double *));
    ese[0] = (double *)malloc(g_atom.rdim * g_atom.rdim * sizeof(double));
    for (int i = 0; i < g_atom.rdim; i++) {
        if (i > 0) {
            ese[i] = ese[0] + i * g_atom.rdim;
        }
        for (int j = 0; j < g_atom.rdim; j++) {
            ese[i][j] = 0.0;
        }
    }

    /* Get SEE */
    F_GetESE(p_node, ese, rhs);

    /* Save old rho */
    F_GetDM(p_node, rho_old);

    /* Solve */
    LudSolve(g_atom.rdim, ese, rhs, 1);

    /* Compute MRC */
    for (int i = 0; i < g_atom.nlevel; i++) {
        int Ri = g_atom.i_rho[i][0][0];

        if (rhs[Ri] <= 0.0 || isnan(rhs[Ri])) {
            Error(E_WARNING, POR_AT,
                  "invalid density matrix element "
                  "%e (at i=%d) in grid node "
                  "(%d,%d,%d)",
                  rhs[Ri], i, p_node->ix, p_node->iy, p_node->iz);
            /* in case of negative population, try to use the previous
               DM instead */
            F_SetDM(p_node, rho_old);
            maxrc = 0;
            goto F_SolveESE_emerg_exit;
        }

        mrc = fabs(rhs[Ri] - rho_old[Ri]) / rhs[Ri];
        if (mrc > maxrc) {
            maxrc = mrc;
        }
    }
    F_SetDM(p_node, rhs);
F_SolveESE_emerg_exit:

    free(ese[0]);
    free(ese);
    return maxrc;
}

/****************************************************************************/
/**
 * Computed the boundary illumination given the propagation direction,
 * the frequency index, and the bottom temperature. This boundary
 * condition assumes that there is planckian illumination from the
 * bottom, and no illumination from any other direction
 *
 * @brief External illumination function at point r in the direction
 *        dir and frequency ifreq; gives Stokes parameters stokes
 * @param [in] r Position vector
 * @param [in] dir Directional vector in the propagation direction
 * @param [in] ifreq Frequency index in which to compute the boundary
 *                   illumination
 * @param [out] i Stokes profiles for the boundary condition
 * @return void
 *
 ****************************************************************************/
static void F_ExternalIllum(const t_vec_3d r, const t_vec_3d dir, int ifreq,
                            double i[NSTOKES])
{
    ArraySet(NSTOKES, i, 0.0);
    if (dir.z > 0.0) {
        double T = mat_PlaneInterpolation(
            PorNX(), PorNY(), PorGetXCoords(), PorGetYCoords(),
            PorDomainSizeX(), PorDomainSizeY(), g_temp, 1, r.x, r.y);
        i[STOKES_I] = phy_PlanckFunction(T, PorFreqs()[ifreq]);
    }
}

/****************************************************************************/
/**
 * Returns the temperature of a given node
 *
 * @brief Kinetic plasma temperature at a given node
 * @param [in] p_node Node data
 * @return double Plasma temperature [K]
 *
 ****************************************************************************/
static double F_GetTemperature(t_node *p_node)
{
    m_Node *p;
    p = (m_Node *)p_node->p_data;
    return p->T;
}

/****************************************************************************/
/**
 * Returns the magnetic field vector of a given node
 *
 * @brief Magnetic field vector at a given node
 * @param [in] p_node Node data
 * @return t_vec_3d Magnetic field vector [G]
 *
 ****************************************************************************/
static t_vec_3d F_GetMagneticVector(t_node *p_node)
{
    m_Node * p;
    t_vec_3d B;
    p   = (m_Node *)p_node->p_data;
    B.x = p->Bx;
    B.y = p->By;
    B.z = p->Bz;
    return B;
}

/****************************************************************************/
/**
 * Gets the radiation field tensors and lambda operators of a given
 * node into an array of doubles
 *
 * @brief Array of radiation field tensors and lambda operators
 *        necessary for calculation of statistical equilibrium
 *        equations in a given module
 * @param [in] p_node Node data
 * @param [in] ndata Size of the radiation field tensors plus size of
 *             the lambda operators
 * @return double Array with radiation field tensors and lambda
 *                operators
 *
 ****************************************************************************/
static double *F_GetRadiation(t_node *p_node, int *ndata)
{
    m_Node *p;
    int     len;
    double *r;
    p = (m_Node *)p_node->p_data;

    len = g_atom.jdim + g_atom.rtran;
    r   = (double *)malloc(len * sizeof(double));

    for (int i = 0; i < g_atom.jdim; i++) {
        r[i] = p->J[i];
    }
    for (int i = g_atom.jdim; i < len; i++) {
        r[i] = p->psi_eta[i - g_atom.jdim];
    }
    *ndata = len;
    return r;
}

/****************************************************************************/
/**
 * Sets the radiation field tensors and lambda operators of a given
 * node to the values specified as input
 *
 * @brief Array of radiation field data of the format of F_GetRadiation
 *        is being set in the given node
 * @param [in] p_node Node data
 * @param [in] data Array with the values to be set
 * @return void
 *
 ****************************************************************************/
static void F_SetRadiation(t_node *p_node, double *data)
{
    m_Node *p;
    int     len;

    p = (m_Node *)p_node->p_data;

    for (int i = 0; i < g_atom.jdim; i++) {
        p->J[i] = data[i];
    }
    len = g_atom.jdim + g_atom.rtran;
    for (int i = g_atom.jdim; i < len; i++) {
        p->psi_eta[i - g_atom.jdim] = data[i];
    }
}

/****************************************************************************/
/**
 * Sets all the radiation field tensors and lambda operators to zero
 *
 * @brief Set the radiation tensors and lambda operators to zero
 * @param [in] p_node Node data
 *
 ****************************************************************************/
static void F_ResetRadiation(t_node *p_node)
{
    m_Node *p;
    p = (m_Node *)p_node->p_data;
    for (int i = 0; i < g_atom.jdim; i++) {
        p->J[i] = 0.0;
    }
    for (int i = 0; i < g_atom.rtran; i++) {
        p->psi_eta[i]   = 0.0;
        p->idir_last[i] = IDIR_UNDEF;
    }
}

/****************************************************************************/
/**
 * Allocates (if needed) and initialize the internal data structures
 * of a given grid node
 *
 * @brief Allocates and initialize the internal data structures of
 *        a given node
 * @param [in] p_node Node data
 * @return void
 * @note The radiation field variables (radiation field tensors, Lambda
 *       operators, etc.) should be initialized to zero
 *
 ****************************************************************************/
static void F_Init_p_data(t_node *p_node)
{
    m_Node *p;

    if (!p_node)
        IERR;
    if (!p_node->p_data) {
        p_node->p_data = (void *)Malloc(g_nodesize);
        p              = (m_Node *)p_node->p_data;
        p->rho         = (double *)malloc(g_atom.rdim * sizeof(double));
        p->J           = (double *)malloc(g_atom.jdim * sizeof(double));
        p->psi_eta     = (double *)malloc(g_atom.rtran * sizeof(double));
        p->a_voigt     = (double *)malloc(g_atom.rtran * sizeof(double));
        p->Cul         = (double *)malloc(g_atom.ntran * sizeof(double));
        p->DK          = (double *)malloc(g_atom.nlevel * sizeof(double));
        p->eta_c       = (double *)malloc(g_atom.rtran * sizeof(double));
        p->eps_c       = (double *)malloc(g_atom.rtran * sizeof(double));
        p->idir_last   = (int *)malloc(g_atom.rtran * sizeof(int));
        p->nu_d        = (double *)malloc(g_atom.rtran * sizeof(double));
        p->pnorm       = (double *)malloc(g_atom.rtran * sizeof(double));
        F_ResetRadiation(p_node);
/* Hanle Zeeman rotation flag */
#if defined(HANLE_ZEEMAN_FS)
        p->rotated = 0;
#endif
    }
}

/****************************************************************************/
/**
 * Frees the memory allocated within a node
 *
 * @brief Free the memory allocated for the grid-node physical data
 * @param [in] p_data Node data
 * @return void
 *
 ****************************************************************************/
static void F_Free_p_data(void *p_data)
{
    m_Node *p = (m_Node *)p_data;
    free(p->rho);
    free(p->J);
    free(p->psi_eta);
    free(p->a_voigt);
    free(p->Cul);
    free(p->DK);
    free(p->eta_c);
    free(p->eps_c);
    free(p->idir_last);
    free(p->nu_d);
    free(p->pnorm);
    free(p);
}

/****************************************************************************/
/**
 * Encodes all the node data contained in p_data (except for temporary
 * variables) into a buffer that can be understood by the particular
 * module (can be decoded by F_EncodeNodeData()); the buffer is of
 * length F_GetNodeDataSize() and must be freed by subsequently the
 * caller
 *
 * @brief Encodes the node non-temporary data contained in the node
 *        into a buffer
 * @param [in] p_data Node data
 * @return void* Buffer with node data
 *
 ****************************************************************************/
static void *F_EncodeNodeData(void *p_data)
{
    m_Node *  pdata   = (m_Node *)p_data;
    int       written = 0;
    t_pstream f;
    char *    buffer = (char *)Malloc(g_nodesize);

    if (!p_data) {
        Error(E_ERROR, POR_AT, "unallocated node data");
        return NULL;
    }

/* If HANLE_ZEEMAN, check if we rotated rhoKQ */
#if defined(HANLE_ZEEMAN_FS)
    /* If rhoKQ are rotated in this node */
    if (pdata->rotated) {
        t_vec_3d Bdir_norm, Bvec;
        t_node * p_node;
        double   Btheta, Bchi;

        p_node->p_data = p_data;

        /* Magnetic field components */
        Bvec.x = pdata->Bx;
        Bvec.y = pdata->By;
        Bvec.z = pdata->Bz;

        /* Angle of B in the laboratory frame */
        Bdir_norm = Bvec;
        VEC_NORMALIZE_3D(Bdir_norm);
        VecUnit2Ang(Bdir_norm, &Btheta, &Bchi);

        /* Rotate back */
        rotate_dm(p_node, -Bchi, -Btheta, 0.0);

        /* De-flag */
        pdata->rotated = 0;
    }
#endif

    f = pst_NewStream(g_nodesize, (char *)buffer, 1);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->T);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->N);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->Bx);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->By);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->Bz);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->vx);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->vy);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->vz);
    written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->vmi);
    for (int i = 0; i < g_atom.rdim; i++) {
        written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->rho[i]);
    }
    for (int i = 0; i < g_atom.jdim; i++) {
        written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->J[i]);
    }
    for (int i = 0; i < g_atom.rtran; i++) {
        written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->a_voigt[i]);
    }
    for (int i = 0; i < g_atom.ntran; i++) {
        written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->Cul[i]);
    }
    for (int i = 0; i < g_atom.nlevel; i++) {
        written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->DK[i]);
    }
    for (int i = 0; i < g_atom.rtran; i++) {
        written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->eta_c[i]);
    }
    for (int i = 0; i < g_atom.rtran; i++) {
        written += pst_Write(&f, PSTREAM_DOUBLE, &pdata->eps_c[i]);
    }

    /* Check the amount of writing */
    if (written == (9 + g_atom.rdim + g_atom.jdim + g_atom.rtran * 3 +
                    g_atom.ntran + g_atom.nlevel)) {
        return buffer;
    } else {
        IERR;
        free(buffer);
        return NULL;
    }
}

/****************************************************************************/
/**
 * Decodes the data encoded by F_EncodeNodeData() and sets a given grid
 * node
 *
 * @brief Sets a given grid node
 * @param [in] buffer Buffer with the node data
 * @param [in] p_node Node data
 * @return void
 *
 ****************************************************************************/
static void F_DecodeNodeData(void *buffer, t_node *p_node)
{
    m_Node *  pdata;
    t_pstream f;
    int       read = 0;

    if (!buffer) {
        Error(E_ERROR, POR_AT, "uninitialized buffer");
        return;
    }
    if (!p_node) {
        Error(E_ERROR, POR_AT, "unallocated node");
        return;
    }
    f = pst_NewStream(g_nodesize, (char *)buffer, 0);
    F_Init_p_data(p_node);
    pdata = p_node->p_data;
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->T);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->N);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->Bx);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->By);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->Bz);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->vx);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->vy);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->vz);
    read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->vmi);
    for (int i = 0; i < g_atom.rdim; i++) {
        read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->rho[i]);
    }
    for (int i = 0; i < g_atom.jdim; i++) {
        read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->J[i]);
    }
    for (int i = 0; i < g_atom.rtran; i++) {
        read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->a_voigt[i]);
    }
    for (int i = 0; i < g_atom.ntran; i++) {
        read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->Cul[i]);
    }
    for (int i = 0; i < g_atom.nlevel; i++) {
        read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->DK[i]);
    }
    for (int i = 0; i < g_atom.rtran; i++) {
        read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->eta_c[i]);
    }
    for (int i = 0; i < g_atom.rtran; i++) {
        read += pst_Read(&f, PSTREAM_DOUBLE, &pdata->eps_c[i]);
    }
    UpdateWidths(pdata);

    /* Check amount of reads */
    if (read != (9 + g_atom.rdim + g_atom.jdim + g_atom.rtran * 3 +
                 g_atom.ntran + g_atom.nlevel)) {
        Error(E_ERROR, POR_AT, "cannot read buffer");
    }
}

/****************************************************************************/
/**
 * @brief Encode all the data pertaining to a node, in order to dump
 *        it into a file
 *        The data is read from p_data, after casting it to m_Node
 *        The data is copied to buf contiguously, following the
 *        H5T_COMPOUND type, as defined in function F_IO_WriteHeader_h5
 * @param buf [out] serialized buffer corresponding to data in p_data
 * @param p_data [in] Physical data of a node
 * @return 1 if successful ; 0 if not
 *
 ****************************************************************************/
static int F_EncodeNodeData_h5(void *buf, void *p_data)
{
    m_Node *pdata = (m_Node *)p_data;
    int     i;
    char *  bp;

    if (!p_data) {
        Error(E_ERROR, POR_AT, "unallocated node data");
        return 0;
    }

/* If HANLE_ZEEMAN, check if we rotated rhoKQ */
#if defined(HANLE_ZEEMAN_FS)
    /* If rhoKQ are rotated in this node */
    if (pdata->rotated) {
        t_vec_3d Bdir_norm, Bvec;
        t_node * p_node;
        double   Btheta, Bchi;

        p_node->p_data = p_data;

        /* Magnetic field components */
        Bvec.x = pdata->Bx;
        Bvec.y = pdata->By;
        Bvec.z = pdata->Bz;

        /* Angle of B in the laboratory frame */
        Bdir_norm = Bvec;
        VEC_NORMALIZE_3D(Bdir_norm);
        VecUnit2Ang(Bdir_norm, &Btheta, &Bchi);

        /* Rotate back */
        rotate_dm(p_node, -Bchi, -Btheta, 0.0);

        /* De-flag */
        pdata->rotated = 0;
    }
#endif

    bp = buf;
    memcpy((void *)bp, (void *)&pdata->T, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->N, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->Bx, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->By, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->Bz, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->vx, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->vy, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->vz, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)bp, (void *)&pdata->vmi, sizeof(double));
    bp += sizeof(double);

    for (int i = 0; i < g_atom.rdim; i++) {
        memcpy((void *)bp, (void *)&pdata->rho[i], sizeof(double));
        bp += sizeof(double);
    }

    for (int i = 0; i < g_atom.jdim; i++) {
        memcpy((void *)bp, (void *)&pdata->J[i], sizeof(double));
        bp += sizeof(double);
    }

    for (int i = 0; i < g_atom.rtran; i++) {
        memcpy((void *)bp, (void *)&pdata->a_voigt[i], sizeof(double));
        bp += sizeof(double);
    }

    for (int i = 0; i < g_atom.ntran; i++) {
        memcpy((void *)bp, (void *)&pdata->Cul[i], sizeof(double));
        bp += sizeof(double);
    }

    for (int i = 0; i < g_atom.nlevel; i++) {
        memcpy((void *)bp, (void *)&pdata->DK[i], sizeof(double));
        bp += sizeof(double);
    }

    for (int i = 0; i < g_atom.rtran; i++) {
        memcpy((void *)bp, (void *)&pdata->eta_c[i], sizeof(double));
        bp += sizeof(double);
    }

    for (int i = 0; i < g_atom.rtran; i++) {
        memcpy((void *)bp, (void *)&pdata->eps_c[i], sizeof(double));
        bp += sizeof(double);
    }

    return 1;
}

/****************************************************************************/
/**
 * @brief Decode all the data pertaining to a node, as read from a
 *        file, in order to update the node data (for hdf5 format)
 * @param [in] buffer stream of data
 * @param [out] p_node Node data
 * @return void
 *
 ****************************************************************************/
static void F_DecodeNodeData_h5(void *buffer, t_node *p_node)
{
    int     i;
    m_Node *pdata;
    char *  bp;

    if (!buffer) {
        Error(E_ERROR, POR_AT, "uninitialized buffer");
        return;
    }

    bp = buffer;

    if (!p_node) {
        Error(E_ERROR, POR_AT, "unallocated node");
        return;
    }

    F_Init_p_data(p_node);
    pdata = p_node->p_data;

    memcpy((void *)&pdata->T, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->N, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->Bx, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->By, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->Bz, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->vx, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->vy, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->vz, (void *)bp, sizeof(double));
    bp += sizeof(double);
    memcpy((void *)&pdata->vmi, (void *)bp, sizeof(double));
    bp += sizeof(double);

    for (int i = 0; i < g_atom.rdim; i++) {
        memcpy((void *)&pdata->rho[i], (void *)bp, sizeof(double));
        bp += sizeof(double);
    }

    for (int i = 0; i < g_atom.jdim; i++) {
        memcpy((void *)&pdata->J[i], (void *)bp, sizeof(double));
        bp += sizeof(double);
    }

    for (int i = 0; i < g_atom.rtran; i++) {
        memcpy((void *)&pdata->a_voigt[i], (void *)bp, sizeof(double));
        bp += sizeof(double);
    }

    for (int i = 0; i < g_atom.ntran; i++) {
        memcpy((void *)&pdata->Cul[i], (void *)bp, sizeof(double));
        bp += sizeof(double);
    }

    for (int i = 0; i < g_atom.nlevel; i++) {
        memcpy((void *)&pdata->DK[i], (void *)bp, sizeof(double));
        bp += sizeof(double);
    }

    for (int i = 0; i < g_atom.rtran; i++) {
        memcpy((void *)&pdata->eta_c[i], (void *)bp, sizeof(double));
        bp += sizeof(double);
    }

    for (int i = 0; i < g_atom.rtran; i++) {
        memcpy((void *)&pdata->eps_c[i], (void *)bp, sizeof(double));
        bp += sizeof(double);
    }

    UpdateWidths(pdata);
}

/****************************************************************************/
/**
 * Returns size (in bytes) of the node data in the pmd file
 *
 * @brief Returns size (in bytes) of the node data in a PMD file
 * @return int Size of the node data in bytes
 *
 ****************************************************************************/
static int F_GetNodeDataSize(void) { return g_nodesize; }

/****************************************************************************/
/**
 * Called after the grid is loaded in order to synchronize global
 * module-dependent variables among the MPI processes
 *
 * @brief Synchronize global module-dependent variables
 * @return void
 *
 ****************************************************************************/
void F_SyncGlobals(void)
{
    if (g_globals_synced)
        return;

    for (int itran = 0; itran < g_atom.rtran; itran++) {
        g_max_d[itran] = PorGetDomainMax(g_max_d[itran]);
        g_min_d[itran] = -PorGetDomainMax(-g_min_d[itran]);
        g_max_a[itran] = PorGetDomainMax(g_max_a[itran]);
        g_min_a[itran] = -PorGetDomainMax(-g_min_a[itran]);
    }
    MakeProfilesPool();
    g_widths_set     = 1;
    g_globals_synced = 1;
}

/****************************************************************************/
/**
 * Called in every Jacobi iteration just before the statistical
 * equilibrium solutions is started (in ese_SolveGridPrecondESE and
 * always once before all the F_SetDM in the grid). It allows to
 * initialize and synchronize module-dependent global variables
 *
 * @brief Initialize and Synchronize module-dependent global variables
 * @return void
 * @note Not used in this module
 *
 ****************************************************************************/
static void F_ESEBegins(void) {}

/****************************************************************************/
/**
 * Called in every Jacobi iteration just after solution of statistical
 * equilibrium equations. It allows to initialize and synchronize
 * module-dependent global variables
 *
 * @brief Initialize and Synchronize module-dependent global variables
 * @return void
 * @note Not used in this module
 *
 ****************************************************************************/
static void F_ESEEnds(void) {}

/*####################################################################
#                                                                    #
#                           INPUT/OUTPUT                             #
#                                                                    #
####################################################################*/

/****************************************************************************/
/**
 * Writes the module header into a file
 *
 * @brief Write the module header data into a file
 * @param [in] f File to write into
 * @return int 1 if success, -1 if failed
 *
 ****************************************************************************/
static int F_IO_WriteHeader(FILE *f)
{
    int i, written = 0;
    int ver = MULTILEVELVERSION;

    written += fwrite(&ver, INTSIZE, 1, f);
    written += fwrite(&g_atom.mass, DBLSIZE, 1, f);
    written += fwrite(&g_atom.nlevel, INTSIZE, 1, f);
    written += fwrite(&g_atom.ntran, INTSIZE, 1, f);
    written += fwrite(&g_atom.rtran, INTSIZE, 1, f);
    written += fwrite(g_atom.E, DBLSIZE, g_atom.nlevel, f);
    written += fwrite(g_atom.gL, DBLSIZE, g_atom.nlevel, f);
    written += fwrite(g_atom.J2, INTSIZE, g_atom.nlevel, f);
    for (i = 0; i < g_atom.ntran; i++) {
        written += fwrite(&g_atom.iu[i], INTSIZE, 1, f);
        written += fwrite(&g_atom.il[i], INTSIZE, 1, f);
    }
    written += fwrite(g_atom.Aul, DBLSIZE, g_atom.rtran, f);
    written += fwrite(g_atom.nfreq, INTSIZE, g_atom.rtran, f);
    written += fwrite(g_atom.nfreqc, INTSIZE, g_atom.rtran, f);
    written += fwrite(g_atom.Dw, DBLSIZE, g_atom.rtran, f);
    written += fwrite(g_atom.Dwc, DBLSIZE, g_atom.rtran, f);
    written += fwrite(g_atom.rDw, DBLSIZE, g_atom.rtran, f);
    written += fwrite(&g_atom.Kcut, INTSIZE, 1, f);
    for (i = 0; i < PorNY(); i++) {
        written += fwrite(g_temp[i], DBLSIZE, PorNX(), f);
    }
    if (written == 6 + 3 * g_atom.nlevel + 2 * g_atom.ntran + 6 * g_atom.rtran +
                       PorNX() * PorNY()) {
        return 1;
    } else {
        return -1;
    }
}

/****************************************************************************/
/**
 * Loads the module header data from a pmd file
 *
 * @brief Loads module header data
 * @param f [in] File to read from
 * @return int 1 if success, 0 if failed
 *
 ****************************************************************************/
static int LoadHeader(FILE *f)
{
    int i, cnt = 0;
    int ver;

    cnt += fread(&ver, INTSIZE, 1, f);
    if (ver != MULTILEVELVERSION) {
        Error(E_ERROR, POR_AT,
              "incompatible version %d of the "
              "format (supported version: %d)",
              ver, MULTILEVELVERSION);
        return 0;
    }

    /* Read atom mass */
    cnt += fread(&g_atom.mass, DBLSIZE, 1, f);

    /* Read atom dimensions */
    cnt += fread(&g_atom.nlevel, INTSIZE, 1, f);
    cnt += fread(&g_atom.ntran, INTSIZE, 1, f);
    cnt += fread(&g_atom.rtran, INTSIZE, 1, f);

    /* Dimension check */
    if (g_atom.nlevel < 1) {
        Error(E_ERROR, POR_AT,
              "Number of levels must be a natural "
              "number: %d < 1",
              g_atom.nlevel);
        return 0;
    }
    if (g_atom.ntran < 1) {
        Error(E_ERROR, POR_AT,
              "Number of transitions must be a "
              "natural number: %d < 1",
              g_atom.ntran);
        return 0;
    }
    if (g_atom.rtran < 1) {
        Error(E_ERROR, POR_AT,
              "Number of radiative transitions must "
              "be a natural number: %d < 1",
              g_atom.rtran);
        return 0;
    }

    /* Allocate */
    g_atom.E      = (double *)malloc(g_atom.nlevel * DBLSIZE);
    g_atom.gL     = (double *)malloc(g_atom.nlevel * DBLSIZE);
    g_atom.J2     = (int *)malloc(g_atom.nlevel * INTSIZE);
    g_atom.iu     = (int *)malloc(g_atom.ntran * INTSIZE);
    g_atom.il     = (int *)malloc(g_atom.ntran * INTSIZE);
    g_atom.Aul    = (double *)malloc(g_atom.rtran * DBLSIZE);
    g_atom.nfreq  = (int *)malloc(g_atom.rtran * INTSIZE);
    g_atom.nfreqc = (int *)malloc(g_atom.rtran * INTSIZE);
    g_atom.i0     = (int *)malloc(g_atom.rtran * INTSIZE);
    g_atom.i1     = (int *)malloc(g_atom.rtran * INTSIZE);
    g_atom.Dw     = (double *)malloc(g_atom.rtran * DBLSIZE);
    g_atom.Dwc    = (double *)malloc(g_atom.rtran * DBLSIZE);
    g_atom.rDw    = (double *)malloc(g_atom.rtran * DBLSIZE);

    /* Read atom */
    cnt += fread(g_atom.E, DBLSIZE, g_atom.nlevel, f);
    cnt += fread(g_atom.gL, DBLSIZE, g_atom.nlevel, f);
    cnt += fread(g_atom.J2, INTSIZE, g_atom.nlevel, f);

    for (i = 0; i < g_atom.ntran; i++) {
        cnt += fread(&g_atom.iu[i], INTSIZE, 1, f);
        cnt += fread(&g_atom.il[i], INTSIZE, 1, f);

        if (g_atom.il[i] >= g_atom.iu[i]) {
            Error(E_ERROR, POR_AT,
                  "upper level index "
                  "cannot be smaller "
                  "than lower level "
                  "index");
            return 0;
        }
    }
    cnt += fread(g_atom.Aul, DBLSIZE, g_atom.rtran, f);
    cnt += fread(g_atom.nfreq, INTSIZE, g_atom.rtran, f);
    cnt += fread(g_atom.nfreqc, INTSIZE, g_atom.rtran, f);
    cnt += fread(g_atom.Dw, DBLSIZE, g_atom.rtran, f);
    cnt += fread(g_atom.Dwc, DBLSIZE, g_atom.rtran, f);
    cnt += fread(g_atom.rDw, DBLSIZE, g_atom.rtran, f);
    cnt += fread(&g_atom.Kcut, INTSIZE, 1, f);
    if (g_atom.Kcut < 0) {
        g_Kcut = 2000;
    } else {
        g_Kcut = g_atom.Kcut;
    }

    /* Bottom horizontal plane temperature. Last points
       are made identical with the 0th one: for array searches */
    g_temp = (double **)malloc((PorNY() + 1) * sizeof(double *));
    g_temp[0] =
        (double *)malloc((PorNY() + 1) * (PorNX() + 1) * sizeof(double));
    for (i = 1; i <= PorNY(); i++)
        g_temp[i] = g_temp[0] + i * (PorNX() + 1);

    for (i = 0; i < PorNY(); i++) {
        int cc = fread(g_temp[i], DBLSIZE, PorNX(), f);

        if (cc != PorNX()) {
            Error(E_ERROR, POR_AT, "cannot read from file");
            return 0;
        }
        cnt += cc;
    }
    for (i = 0; i < PorNY(); i++) {
        g_temp[i][PorNX()] = g_temp[i][0];
    }
    for (i = 0; i < PorNX(); i++) {
        g_temp[PorNY()][i] = g_temp[0][i];
    }
    g_temp[PorNY()][PorNX()] = g_temp[0][0];

    if (cnt != 6 + 3 * g_atom.nlevel + 2 * g_atom.ntran + 6 * g_atom.rtran +
                   PorNX() * PorNY()) {
        return 0;
    } else {
        return 1;
    }
}

/****************************************************************************/
/**
 * Write Model Header data to a HDF5 file (/Module group attributes,
 * plus /Module/temp_bc dataset).
 *
 * Also the g_data dataset is created (though empty)
 *
 * @brief Write Model header data
 * @param f_id [in] HDF5 file identifier
 * @return hdf5 identifier of g_data dataset ; -1 if not
 * @todo check return status all all calls to h5t_insert, etc.
 *
 ****************************************************************************/
static hid_t F_IO_WriteHeader_h5(hid_t f_id)
{
    int   i, j;
    int   ver = MULTILEVELVERSION;
    hid_t group_id, memtype, B_type, V_type, rho_type, J_type,
        voigt_eta_eps_type, Cul_type, DK_type;
    hid_t   space, dset;
    hsize_t locdims[1];
    herr_t  status;
    int *   atom_iul;
    int     g_nx = PorNX(), g_ny = PorNY(), g_nz = PorNZ();
    hsize_t dims[3];
    int     offset;

    group_id =
        H5Gcreate(f_id, "/Module", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    H5LTset_attribute_int(f_id, "/Module", "ML_VERSION", &ver, 1);
    H5LTset_attribute_double(f_id, "/Module", "ATOM_MASS", &g_atom.mass, 1);
    H5LTset_attribute_int(f_id, "/Module", "NL", &g_atom.nlevel, 1);
    H5LTset_attribute_int(f_id, "/Module", "NT", &g_atom.ntran, 1);
    H5LTset_attribute_int(f_id, "/Module", "NR", &g_atom.rtran, 1);
    H5LTset_attribute_double(f_id, "/Module", "E", g_atom.E, g_atom.nlevel);
    H5LTset_attribute_double(f_id, "/Module", "GL", g_atom.gL, g_atom.nlevel);
    H5LTset_attribute_int(f_id, "/Module", "J2", g_atom.J2, g_atom.nlevel);

    atom_iul = (int *)malloc(g_atom.ntran * 2 * INTSIZE);
    for (i = 0; i < g_atom.ntran; i++) {
        atom_iul[i * 2]     = g_atom.iu[i];
        atom_iul[i * 2 + 1] = g_atom.il[i];
    }
    H5LTset_attribute_int(f_id, "/Module", "IUL", atom_iul, g_atom.ntran * 2);
    free(atom_iul);

    H5LTset_attribute_double(f_id, "/Module", "AUL", g_atom.Aul, g_atom.rtran);
    H5LTset_attribute_int(f_id, "/Module", "NFREQ", g_atom.nfreq, g_atom.rtran);
    H5LTset_attribute_int(f_id, "/Module", "NFREQ_C", g_atom.nfreqc,
                          g_atom.rtran);
    H5LTset_attribute_double(f_id, "/Module", "DW", g_atom.Dw, g_atom.rtran);
    H5LTset_attribute_double(f_id, "/Module", "DW_C", g_atom.Dwc, g_atom.rtran);
    H5LTset_attribute_double(f_id, "/Module", "TEMP_REF", g_atom.rDw,
                             g_atom.rtran);
    H5LTset_attribute_int(f_id, "/Module", "K_CUT", &g_atom.Kcut, 1);

    double *mdata_hdf;
    mdata_hdf = (double *)malloc(PorNX() * PorNY() * sizeof(*mdata_hdf));
    for (j = 0; j < PorNY(); j++) {
        for (i = 0; i < PorNX(); i++) {
            mdata_hdf[j * PorNX() + i] = g_temp[j][i];
        }
    }
    hsize_t mdims[2] = {PorNY(), PorNX()};
    H5LTmake_dataset(f_id, "/Module/temp_bc", 2, mdims, H5T_NATIVE_DOUBLE,
                     mdata_hdf);
    free(mdata_hdf);

    /* Create the array components for g_data */
    locdims[0] = 3;
    B_type     = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, locdims);
    V_type     = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, locdims);

    locdims[0] = g_atom.rdim;
    rho_type   = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, locdims);

    locdims[0] = g_atom.jdim;
    J_type     = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, locdims);

    locdims[0]         = g_atom.rtran;
    voigt_eta_eps_type = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, locdims);

    locdims[0] = g_atom.ntran;
    Cul_type   = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, locdims);

    locdims[0] = g_atom.nlevel;
    DK_type    = H5Tarray_create(H5T_NATIVE_DOUBLE, 1, locdims);

    /* Create the memtype - based on the m_Node_class struct
       Offsets for dynamic arrays cannot be calculated with HOFFSET,
       so I have to calculate them manually */
    memtype = H5Tcreate(H5T_COMPOUND, g_nodesize);

    status = H5Tinsert(memtype, "T", HOFFSET(m_Node, T), H5T_NATIVE_DOUBLE);
    status = H5Tinsert(memtype, "N", HOFFSET(m_Node, N), H5T_NATIVE_DOUBLE);
    status = H5Tinsert(memtype, "B", HOFFSET(m_Node, Bx), B_type);
    status = H5Tinsert(memtype, "v", HOFFSET(m_Node, vx), V_type);
    status = H5Tinsert(memtype, "vmi", HOFFSET(m_Node, vmi), H5T_NATIVE_DOUBLE);

    status = H5Tinsert(memtype, "rho", HOFFSET(m_Node, rho), rho_type);
    offset = HOFFSET(m_Node, rho) + sizeof(double) * g_atom.rdim;

    status = H5Tinsert(memtype, "J", offset, J_type);
    offset += sizeof(double) * g_atom.jdim;

    status = H5Tinsert(memtype, "a_voigt", offset, voigt_eta_eps_type);
    offset += sizeof(double) * g_atom.rtran;

    status = H5Tinsert(memtype, "Cul", offset, Cul_type);
    offset += sizeof(double) * g_atom.ntran;

    status = H5Tinsert(memtype, "DK", offset, DK_type);
    offset += sizeof(double) * g_atom.nlevel;

    status = H5Tinsert(memtype, "eta_c", offset, voigt_eta_eps_type);
    offset += sizeof(double) * g_atom.rtran;

    status = H5Tinsert(memtype, "eps_c", offset, voigt_eta_eps_type);

    /* Pack datatype, so there will be no padding between fields */
    H5Tpack(memtype);

    /* Create /Module/g_data dataset. Empty, to be filled by
       SaveNodesSlave_h5 function */
    dims[0] = g_nz;
    dims[1] = g_ny;
    dims[2] = g_nx;
    space   = H5Screate_simple(3, dims, NULL);
    dset    = H5Dcreate(f_id, "/Module/g_data", memtype, space, H5P_DEFAULT,
                     H5P_DEFAULT, H5P_DEFAULT);

    status = H5Sclose(space);
    status = H5Tclose(memtype);
    status = H5Tclose(B_type);
    status = H5Tclose(V_type);
    status = H5Tclose(rho_type);
    status = H5Tclose(J_type);
    status = H5Tclose(voigt_eta_eps_type);
    status = H5Tclose(Cul_type);
    status = H5Tclose(DK_type);

    status = H5Gclose(group_id);
    if (status < 0) {
        fprintf(stderr, "error: cannot close group /Module \n");
        return -1;
    }
    return dset;
}

/****************************************************************************/
/**
 * Load Model Header data (/Module group attributes, plus /Module/temp_bc
 * dataset)
 *
 * Called collectively (all MPI_COMM_WORLD processes)
 *
 * @brief Load Model header data
 * @param file_id [in] HDF5 file identifier
 * @return 1 if successful ; 0 if not
 *
 ****************************************************************************/
static int LoadHeader_h5(hid_t file_id)
{
    int     i, j, cnt;
    int     ver;
    int *   atom_iul;
    hsize_t dim[1];
    herr_t  status;

    H5LTget_attribute_int(file_id, "/Module", "ML_VERSION", &ver);
    if (ver != MULTILEVELVERSION) {
        Error(E_ERROR, POR_AT,
              "incompatible version %d of the "
              "format (supported version: %d)",
              ver, MULTILEVELVERSION);
        return 0;
    }

    /* Read atom mass */
    H5LTget_attribute_double(file_id, "/Module", "ATOM_MASS", &g_atom.mass);

    /* Read atom dimensions */
    H5LTget_attribute_int(file_id, "/Module", "NL", &g_atom.nlevel);
    H5LTget_attribute_int(file_id, "/Module", "NT", &g_atom.ntran);
    H5LTget_attribute_int(file_id, "/Module", "NR", &g_atom.rtran);

    /* Dimension check */
    if (g_atom.nlevel < 1) {
        Error(E_ERROR, POR_AT,
              "Number of levels must be a natural "
              "number: %d < 1",
              g_atom.nlevel);
        return 0;
    }
    if (g_atom.ntran < 1) {
        Error(E_ERROR, POR_AT,
              "Number of transitions must be a "
              "natural number: %d < 1",
              g_atom.ntran);
        return 0;
    }
    if (g_atom.rtran < 1) {
        Error(E_ERROR, POR_AT,
              "Number of radiative transitions must "
              "be a natural number: %d < 1",
              g_atom.rtran);
        return 0;
    }

    /* Allocate */
    g_atom.E      = (double *)malloc(g_atom.nlevel * DBLSIZE);
    g_atom.gL     = (double *)malloc(g_atom.nlevel * DBLSIZE);
    g_atom.J2     = (int *)malloc(g_atom.nlevel * INTSIZE);
    g_atom.iu     = (int *)malloc(g_atom.ntran * INTSIZE);
    g_atom.il     = (int *)malloc(g_atom.ntran * INTSIZE);
    g_atom.Aul    = (double *)malloc(g_atom.rtran * DBLSIZE);
    g_atom.nfreq  = (int *)malloc(g_atom.rtran * INTSIZE);
    g_atom.nfreqc = (int *)malloc(g_atom.rtran * INTSIZE);
    g_atom.i0     = (int *)malloc(g_atom.rtran * INTSIZE);
    g_atom.i1     = (int *)malloc(g_atom.rtran * INTSIZE);
    g_atom.Dw     = (double *)malloc(g_atom.rtran * DBLSIZE);
    g_atom.Dwc    = (double *)malloc(g_atom.rtran * DBLSIZE);
    g_atom.rDw    = (double *)malloc(g_atom.rtran * DBLSIZE);

    /* Read atom */
    if (H5LTget_attribute_double(file_id, "/Module", "E", g_atom.E) < 0) {
        Error(E_ERROR, POR_AT, "READ ERROR");
    }

    H5LTget_attribute_double(file_id, "/Module", "GL", g_atom.gL);
    H5LTget_attribute_int(file_id, "/Module", "J2", g_atom.J2);

    atom_iul = (int *)malloc(g_atom.ntran * 2 * INTSIZE);
    H5LTget_attribute_int(file_id, "/Module", "IUL", atom_iul);

    for (i = 0; i < g_atom.ntran; i++) {
        cnt          = i * 2;
        g_atom.iu[i] = atom_iul[cnt];
        g_atom.il[i] = atom_iul[cnt + 1];

        if (g_atom.il[i] >= g_atom.iu[i]) {
            Error(E_ERROR, POR_AT,
                  "upper level index "
                  "cannot be smaller "
                  "than lower level "
                  "index");
            return 0;
        }
    }
    free(atom_iul);

    H5LTget_attribute_double(file_id, "/Module", "AUL", g_atom.Aul);
    H5LTget_attribute_int(file_id, "/Module", "NFREQ", g_atom.nfreq);
    H5LTget_attribute_int(file_id, "/Module", "NFREQ_C", g_atom.nfreqc);
    H5LTget_attribute_double(file_id, "/Module", "DW", g_atom.Dw);
    H5LTget_attribute_double(file_id, "/Module", "DW_C", g_atom.Dwc);
    H5LTget_attribute_double(file_id, "/Module", "TEMP_REF", g_atom.rDw);
    H5LTget_attribute_int(file_id, "/Module", "K_CUT", &g_atom.Kcut);

    if (g_atom.Kcut < 0) {
        g_Kcut = 2000;
    } else {
        g_Kcut = g_atom.Kcut;
    }

    double *mdata_hdf;
    mdata_hdf = (double *)malloc(PorNX() * PorNY() * sizeof(*mdata_hdf));
    if (H5LTread_dataset_double(file_id, "/Module/temp_bc", mdata_hdf) < 0) {
        Error(E_ERROR, POR_AT, "cannot read from file");
        return 0;
    }

    /* Bottom horizontal plane temperature. Last points
       are made identical with the 0th one: for array searches */
    g_temp = (double **)malloc((PorNY() + 1) * sizeof(double *));
    g_temp[0] =
        (double *)malloc((PorNY() + 1) * (PorNX() + 1) * sizeof(double));
    for (i = 1; i <= PorNY(); i++)
        g_temp[i] = g_temp[0] + i * (PorNX() + 1);

    for (j = 0; j < PorNY(); j++) {
        for (i = 0; i < PorNX(); i++) {
            g_temp[j][i] = mdata_hdf[j * PorNX() + i];
        }
    }
    free(mdata_hdf);

    for (i = 0; i < PorNY(); i++) {
        g_temp[i][PorNX()] = g_temp[i][0];
    }

    for (i = 0; i < PorNX(); i++) {
        g_temp[PorNY()][i] = g_temp[0][i];
    }
    g_temp[PorNY()][PorNX()] = g_temp[0][0];

    return 1;
}

/********************************************************************/

/*####################################################################
#                                                                    #
#                         MODULE INTERFACE                           #
#                                                                    #
####################################################################*/

/****************************************************************************/
/**
 * Initializes module global variables and functions provided by this
 * module
 *
 * @brief The exported module initialization function
 * @param [out] p_afunc Pointer to struct holding external module's
 *        functions
 * @param [in] file File pointer
 * @param [out] node_pmd_size Pointer to size (in bytes) of each node
 * @return int 1 if success, 0 if failed
 *
 ****************************************************************************/
int InitModule(t_afunc *p_afunc, FILE *file, int *node_pmd_size)
{
    if (!file) {
        Error(E_ERROR, POR_AT,
              "Sorry, you need to load a PMD file"
              "containing the model atmosphere.");
    }

    /* load model header */
    if (!LoadHeader(file)) {
        Error(E_ERROR, POR_AT, "cannot load the module header");
        return 0;
    }

    if (!p_afunc) {
        Error(E_ERROR, POR_AT, "global variables not initialized");
        return 0;
    }

    if (!PorMeshInitialized()) {
        Error(E_ERROR, POR_AT, "grid mesh not initialized");
        return 0;
    }

    /* Save function dictionary into global variable */
    p_afunc->F_Init_p_data        = F_Init_p_data;
    p_afunc->F_Free_p_data        = F_Free_p_data;
    p_afunc->F_EncodeNodeData     = F_EncodeNodeData;
    p_afunc->F_EncodeNodeData_h5  = F_EncodeNodeData_h5;
    p_afunc->F_DecodeNodeData     = F_DecodeNodeData;
    p_afunc->F_DecodeNodeData_h5  = F_DecodeNodeData_h5;
    p_afunc->F_GetNodeDataSize    = F_GetNodeDataSize;
    p_afunc->F_ExternalIllum      = F_ExternalIllum;
    p_afunc->F_SKGeneralDirection = F_SKGeneralDirection;
    p_afunc->F_SK                 = F_SK;
    p_afunc->F_AddFS              = F_AddFS;
    p_afunc->F_GetESE             = F_GetESE;
    p_afunc->F_SolveESE           = F_SolveESE;
    p_afunc->F_GetNDM             = F_GetNDM;
    p_afunc->F_GetPopsPositions   = F_GetPopsPositions;
    p_afunc->F_GetDM              = F_GetDM;
    p_afunc->F_SetDM              = F_SetDM;
    p_afunc->F_GetTemperature     = F_GetTemperature;
    p_afunc->F_GetMagneticVector  = F_GetMagneticVector;
    p_afunc->F_GetRadiation       = F_GetRadiation;
    p_afunc->F_SetRadiation       = F_SetRadiation;
    p_afunc->F_ResetRadiation     = F_ResetRadiation;
    p_afunc->F_IO_WriteHeader     = F_IO_WriteHeader;
    p_afunc->F_IO_WriteHeader_h5  = F_IO_WriteHeader_h5;
    p_afunc->F_SyncGlobals        = F_SyncGlobals;
    p_afunc->F_ESEBegins          = F_ESEBegins;
    p_afunc->F_ESEEnds            = F_ESEEnds;

    g_globals_synced = 0;
    g_widths_set     = 0;
    /* Complete the atomic structure */
    g_atom.jdim  = 9;
    g_atom.maxK  = (int *)malloc(g_atom.nlevel * sizeof(int));
    g_atom.i_rho = (int ***)malloc(g_atom.nlevel * sizeof(int **));
    g_atom.i_J   = (int ***)malloc(g_atom.rtran * sizeof(int **));
    g_atom.Bul   = (double *)malloc(g_atom.rtran * sizeof(double));
    g_atom.Blu   = (double *)malloc(g_atom.rtran * sizeof(double));
    g_atom.RDw   = (double *)malloc(g_atom.rtran * sizeof(double));
    g_atom.dEc   = (double *)malloc(g_atom.ntran * sizeof(double));
    g_atom.glu   = (double *)malloc(g_atom.ntran * sizeof(double));
    g_atom.nu    = (double *)malloc(g_atom.ntran * sizeof(double));
    g_min_d      = (double *)malloc(g_atom.rtran * sizeof(double));
    g_max_d      = (double *)malloc(g_atom.rtran * sizeof(double));
    g_min_a      = (double *)malloc(g_atom.rtran * sizeof(double));
    g_max_a      = (double *)malloc(g_atom.rtran * sizeof(double));
/* Magnetic quantum numbers */
#if defined(HANLE_ZEEMAN_FS)
    g_atom.nM = (int *)malloc(g_atom.nlevel * sizeof(int));
    g_atom.M2 = (int **)malloc(g_atom.nlevel * sizeof(int *));
#endif
    {
        int    i, j, K, Q, Qb, iu, il, J2;
        double dE, dH, nu, C1;

        /* Transition quantities */
        C1 = sqrt(2.0 * C_KB / g_atom.mass / C_AU_MASS) / C_C;
        j  = -1;
        for (i = 0; i < g_atom.ntran; i++) {
            /* Compute transition quantities */
            iu = g_atom.iu[i];
            il = g_atom.il[i];
            g_atom.glu[i] =
                ((double)g_atom.J2[il] + 1.0) / ((double)g_atom.J2[iu] + 1.0);
            dE            = g_atom.E[iu] - g_atom.E[il];
            g_atom.dEc[i] = -dE / C_KB;
            nu            = dE / C_H;
            g_atom.nu[i]  = nu;

            if (i < g_atom.rtran) {
                /* Initialize limits */
                g_min_d[i] = 1e200;
                g_min_a[i] = 1e200;
                g_max_d[i] = -1e200;
                g_max_a[i] = -1e200;

                dH            = 2.0 * C_H * nu * nu * nu / C_C / C_C;
                g_atom.Bul[i] = g_atom.Aul[i] / dH;
                g_atom.Blu[i] = g_atom.Bul[i] / g_atom.glu[i];
                g_atom.RDw[i] = C1 * nu * sqrt(g_atom.rDw[i]);

                /* Radiation indexing */
                g_atom.i_J[i] = (int **)malloc(3 * sizeof(int *));

                for (K = 0; K <= 2; K++) {
                    Qb               = 2 * K + 1;
                    g_atom.i_J[i][K] = (int *)malloc(Qb * sizeof(int));
                    for (Q = 0; Q < Qb; Q++) {
                        j++;
                        g_atom.i_J[i][K][Q] = j;
                    }
                }
            }
            g_atom.jdim = j + 1;
        }

        /* Level quantities */
        j           = -1;
        g_atom.MaxK = 0;
        for (i = 0; i < g_atom.nlevel; i++) {
            /* K limit */
            J2 = g_atom.J2[i];
            if (J2 < g_Kcut) {
                g_atom.maxK[i] = J2;
            } else {
                g_atom.maxK[i] = g_Kcut;
            }

            if (g_atom.maxK[i] > g_atom.MaxK) {
                g_atom.MaxK = g_atom.maxK[i];
            }

            g_atom.i_rho[i] =
                (int **)malloc((g_atom.maxK[i] + 1) * sizeof(int *));

            for (K = 0; K <= g_atom.maxK[i]; K++) {
                Qb                 = 2 * K + 1;
                g_atom.i_rho[i][K] = (int *)malloc(Qb * sizeof(int));
                for (Q = 0; Q < Qb; Q++) {
                    j++;
                    g_atom.i_rho[i][K][Q] = j;
                }
            }

/* Magnetic quantum numbers */
#if defined(HANLE_ZEEMAN_FS)
            g_atom.nM[i]    = J2 + 1;
            g_atom.M2[i]    = (int *)malloc(g_atom.nM[i] * sizeof(int));
            g_atom.M2[i][0] = -J2;
            for (K = 1; K < g_atom.nM[i]; K++) {
                g_atom.M2[i][K] = g_atom.M2[i][K - 1] + 2;
            }
#endif
        }

        /* SEE dimension */
        g_atom.rdim = j + 1;

        /* Allocate and define Qind */
        g_Qind    = (int *)malloc((2 * g_atom.MaxK + 1) * sizeof(int));
        g_Qind[0] = 0;
        i         = 0;
        for (K = 1; K <= g_atom.MaxK; K++) {
            i++;
            g_Qind[i] = K;
            i++;
            g_Qind[i] = -K;
        }
    }

    /* Indicate size of node */
    g_nodesize =
        sizeof(double) * (9 + g_atom.rdim + g_atom.jdim + g_atom.rtran * 3 +
                          g_atom.ntran + g_atom.nlevel);

    /*               */
    /* Sanity checks */
    /*               */

    /* Atom mass must be positive */
    if (g_atom.mass <= 0) {
        Error(E_ERROR, POR_AT,
              "atomic mass must be positive: "
              "%e <= 0",
              g_atom.mass);
    }

    /* Level quantities */
    for (int i = 0; i < g_atom.nlevel; i++) {
        if (i < (g_atom.nlevel - 1)) {
            /* Energy must be ordered */
            if (g_atom.E[i] >= g_atom.E[i + 1]) {
                Error(E_ERROR, POR_AT,
                      "energy must be in increasing "
                      "order: E[%d]=%e >= E[%d]=%e",
                      i, g_atom.E[i], i + 1, g_atom.E[i + 1]);
                return 0;
            }
        }

        /* J2 must be positive */
        if (g_atom.J2[i] < 0) {
            Error(E_ERROR, POR_AT,
                  "angular momentum cannot be "
                  "negative: J2[%d]=%d < 0",
                  i, g_atom.J2[i]);
            return 0;
        }
    }

    /* Transition quantities */
    for (int i = 0; i < g_atom.ntran; i++) {
        /* Indexes must be in nlevel range */
        if (g_atom.il[i] < 0 || g_atom.il[i] >= g_atom.nlevel) {
            Error(E_ERROR, POR_AT,
                  "lower level index for transition "
                  "%d must be in the range of the "
                  "levels: %d < 0 || %d > %d",
                  i, g_atom.il[i], g_atom.il[i], g_atom.nlevel);
            return 0;
        }

        /* Indexes must be in nlevel range */
        if (g_atom.iu[i] < 0 || g_atom.iu[i] >= g_atom.nlevel) {
            Error(E_ERROR, POR_AT,
                  "upper level index for transition "
                  "%d must be in the range of the "
                  "levels: %d < 0 || %d > %d",
                  i, g_atom.iu[i], g_atom.iu[i], g_atom.nlevel);
            return 0;
        }

        /* Indexes must be iu->il */
        if (g_atom.il[i] >= g_atom.iu[i]) {
            Error(E_ERROR, POR_AT,
                  "level indexes for transition "
                  "%d must be upper->lower: "
                  "%d >= %d",
                  i, g_atom.il[i], g_atom.iu[i]);
            return 0;
        }

        /* Radiative quantities */
        if (i >= g_atom.rtran) {
            continue;
        }

        /* Einstein coefficient must be positive */
        if (g_atom.Aul[i] <= 0.0) {
            Error(E_ERROR, POR_AT,
                  "Einstein coefficient for "
                  "transition %d must be "
                  "positive: %d <= 0",
                  i, g_atom.Aul[i]);
            return 0;
        }

        /* Core frequencies must be odd */
        if (g_atom.nfreqc[i] % 2 == 0) {
            Error(E_WARNING, POR_AT,
                  "number of frequencies for the "
                  "core of transition number %d "
                  "is even, must be odd, changed "
                  "%d -> %d",
                  i, g_atom.nfreqc[i], g_atom.nfreqc[i] + 1);
            g_atom.nfreqc[i] = g_atom.nfreqc[i] + 1;
        }

        /* Frequencies must be odd */
        if (g_atom.nfreq[i] % 2 == 0) {
            Error(E_WARNING, POR_AT,
                  "number of frequencies for "
                  "transition number %d "
                  "is even, must be odd, changed "
                  "%d -> %d",
                  i, g_atom.nfreq[i], g_atom.nfreq[i] + 1);
            g_atom.nfreq[i] = g_atom.nfreq[i] + 1;
        }

        /* At least 3 frequency points */
        if (g_atom.nfreqc[i] <= 2) {
            Error(E_ERROR, POR_AT,
                  "number of frequencies for the "
                  "core of transition number %d "
                  "is too small: %d <= 2",
                  i, g_atom.nfreqc[i]);
            return 0;
        }

        /* At least 3 frequency points */
        if (g_atom.nfreq[i] <= 2) {
            Error(E_ERROR, POR_AT,
                  "number of frequencies for "
                  "transition number %d "
                  "is too small: %d <= 2",
                  i, g_atom.nfreq[i]);
            return 0;
        }

        /* Core must be less than total */
        if (g_atom.nfreqc[i] > g_atom.nfreq[i]) {
            Error(E_ERROR, POR_AT,
                  "number of frequencies for the "
                  "core of transition number %d is "
                  "larger than total: %d > %d",
                  i, g_atom.nfreqc[i], g_atom.nfreq[i]);
            return 0;
        }

        /* Core must be less than total */
        if (g_atom.Dwc[i] > g_atom.Dw[i]) {
            Error(E_ERROR, POR_AT,
                  "Doppler width for the core "
                  "of transition number %d is "
                  "larger than total: %e > %e",
                  i, g_atom.Dwc[i], g_atom.Dw[i]);
            return 0;
        }

        /* Core Doppler width must be positive */
        if (g_atom.Dwc[i] <= 0.0) {
            Error(E_ERROR, POR_AT,
                  "Doppler width for the core "
                  "of transition number %d is "
                  "non-positive: %e <= 0",
                  i, g_atom.Dwc[i]);
            return 0;
        }

        /* Doppler width must be positive */
        if (g_atom.Dw[i] <= 0.0) {
            Error(E_ERROR, POR_AT,
                  "Doppler width for "
                  "of transition number %d is "
                  "non-positive: %e <= 0",
                  i, g_atom.Dw[i]);
            return 0;
        }
    }

    /* global lab frequencies */
    MakeFrequencies();

    /* Get node data size */
    *node_pmd_size = F_GetNodeDataSize();

    /* Return success */
    return 1;
}

/****************************************************************************/
/**
 * This function will be called by the PORTA library to clean up
 * before it calls the ```dlclose``` function to actually close the
 * module
 *
 * @brief The exported module closing function
 * @return int 1
 *
 ****************************************************************************/
int CloseModule(void)
{
    /* Clean pool of profiles */
    vgt_PoolClear();

    /* Clean module mallocs */
    free(g_atom.E);
    free(g_atom.gL);
    free(g_atom.J2);
    free(g_atom.iu);
    free(g_atom.il);
    free(g_atom.Aul);
    free(g_atom.nfreq);
    free(g_atom.nfreqc);
    free(g_atom.i0);
    free(g_atom.i1);
    free(g_atom.Dw);
    free(g_atom.Dwc);
    free(g_atom.rDw);
    free(g_atom.RDw);
    free(g_atom.Bul);
    free(g_atom.Blu);
    free(g_atom.dEc);
    free(g_atom.glu);
    free(g_atom.nu);
    for (int i = 0; i < g_atom.nlevel; i++) {
        for (int j = 0; j <= g_atom.maxK[i]; j++) {
            free(g_atom.i_rho[i][j]);
        }
        free(g_atom.i_rho[i]);
#if defined(HANLE_ZEEMAN_FS)
        free(g_atom.M2[i]);
#endif
    }
    free(g_atom.i_rho);
#if defined(HANLE_ZEEMAN_FS)
    free(g_atom.nM);
    free(g_atom.M2);
#endif
    for (int i = 0; i < g_atom.rtran; i++) {
        for (int j = 0; j <= 2; j++) {
            free(g_atom.i_J[i][j]);
        }
        free(g_atom.i_J[i]);
    }
    free(g_atom.i_J);
    free(g_atom.maxK);
    free(g_temp[0]);
    free(g_temp);
    free(g_min_d);
    free(g_max_d);
    free(g_min_a);
    free(g_max_a);
    free(g_Qind);

    /* Return success */
    return 1;
}

/****************************************************************************/
/**
 * Initializes module global variables and functions provided by this
 * module
 *
 * @brief The exported module initialization function
 * @param p_afunc [out] Pointer to struct holding external module's
 *                      functions (all these functions start with F_)
 * @param file_id [in] HDF5 file identifier
 * @param [out] node_pmd_size Pointer to size (in bytes) of each node
 * @return 1 if successful ; 0 if not
 * @todo this is almost identical to the binary format one. See if we
 *       can combine them
 *
 ****************************************************************************/
int InitModule_h5(t_afunc *p_afunc, hid_t file_id, int *node_pmd_size)
{
    if (!file_id) {
        Error(E_ERROR, POR_AT,
              "Sorry, you need to load a PMD file"
              "containing the model atmosphere.");
    }

    /* load model header */
    if (!LoadHeader_h5(file_id)) {
        Error(E_ERROR, POR_AT, "cannot load the module header");
        return 0;
    }

    if (!p_afunc) {
        Error(E_ERROR, POR_AT, "global variables not initialized");
        return 0;
    }

    if (!PorMeshInitialized()) {
        Error(E_ERROR, POR_AT, "grid mesh not initialized");
        return 0;
    }

    /* Save function dictionary into global variable */
    p_afunc->F_Init_p_data        = F_Init_p_data;
    p_afunc->F_Free_p_data        = F_Free_p_data;
    p_afunc->F_EncodeNodeData     = F_EncodeNodeData;
    p_afunc->F_EncodeNodeData_h5  = F_EncodeNodeData_h5;
    p_afunc->F_DecodeNodeData     = F_DecodeNodeData;
    p_afunc->F_DecodeNodeData_h5  = F_DecodeNodeData_h5;
    p_afunc->F_GetNodeDataSize    = F_GetNodeDataSize;
    p_afunc->F_ExternalIllum      = F_ExternalIllum;
    p_afunc->F_SKGeneralDirection = F_SKGeneralDirection;
    p_afunc->F_SK                 = F_SK;
    p_afunc->F_AddFS              = F_AddFS;
    p_afunc->F_GetESE             = F_GetESE;
    p_afunc->F_SolveESE           = F_SolveESE;
    p_afunc->F_GetNDM             = F_GetNDM;
    p_afunc->F_GetPopsPositions   = F_GetPopsPositions;
    p_afunc->F_GetDM              = F_GetDM;
    p_afunc->F_SetDM              = F_SetDM;
    p_afunc->F_GetTemperature     = F_GetTemperature;
    p_afunc->F_GetMagneticVector  = F_GetMagneticVector;
    p_afunc->F_GetRadiation       = F_GetRadiation;
    p_afunc->F_SetRadiation       = F_SetRadiation;
    p_afunc->F_ResetRadiation     = F_ResetRadiation;
    p_afunc->F_IO_WriteHeader     = F_IO_WriteHeader;
    p_afunc->F_IO_WriteHeader_h5  = F_IO_WriteHeader_h5;
    p_afunc->F_SyncGlobals        = F_SyncGlobals;
    p_afunc->F_ESEBegins          = F_ESEBegins;
    p_afunc->F_ESEEnds            = F_ESEEnds;

    g_globals_synced = 0;
    g_widths_set     = 0;
    /* Complete the atomic structure */
    g_atom.jdim  = 9;
    g_atom.maxK  = (int *)malloc(g_atom.nlevel * sizeof(int));
    g_atom.i_rho = (int ***)malloc(g_atom.nlevel * sizeof(int **));
    g_atom.i_J   = (int ***)malloc(g_atom.rtran * sizeof(int **));
    g_atom.Bul   = (double *)malloc(g_atom.rtran * sizeof(double));
    g_atom.Blu   = (double *)malloc(g_atom.rtran * sizeof(double));
    g_atom.RDw   = (double *)malloc(g_atom.rtran * sizeof(double));
    g_atom.dEc   = (double *)malloc(g_atom.ntran * sizeof(double));
    g_atom.glu   = (double *)malloc(g_atom.ntran * sizeof(double));
    g_atom.nu    = (double *)malloc(g_atom.ntran * sizeof(double));
    g_min_d      = (double *)malloc(g_atom.rtran * sizeof(double));
    g_max_d      = (double *)malloc(g_atom.rtran * sizeof(double));
    g_min_a      = (double *)malloc(g_atom.rtran * sizeof(double));
    g_max_a      = (double *)malloc(g_atom.rtran * sizeof(double));
/* Magnetic quantum numbers */
#if defined(HANLE_ZEEMAN_FS)
    g_atom.nM = (int *)malloc(g_atom.nlevel * sizeof(int));
    g_atom.M2 = (int **)malloc(g_atom.nlevel * sizeof(int *));
#endif
    {
        int    i, j, K, Q, Qb, iu, il, J2;
        double dE, dH, nu, C1;

        /* Transition quantities */
        C1 = sqrt(2.0 * C_KB / g_atom.mass / C_AU_MASS) / C_C;
        j  = -1;
        for (i = 0; i < g_atom.ntran; i++) {
            /* Compute transition quantities */
            iu = g_atom.iu[i];
            il = g_atom.il[i];
            g_atom.glu[i] =
                ((double)g_atom.J2[il] + 1.0) / ((double)g_atom.J2[iu] + 1.0);
            dE            = g_atom.E[iu] - g_atom.E[il];
            g_atom.dEc[i] = -dE / C_KB;
            nu            = dE / C_H;
            g_atom.nu[i]  = nu;

            if (i < g_atom.rtran) {
                /* Initialize limits */
                g_min_d[i] = 1e200;
                g_min_a[i] = 1e200;
                g_max_d[i] = -1e200;
                g_max_a[i] = -1e200;

                dH            = 2.0 * C_H * nu * nu * nu / C_C / C_C;
                g_atom.Bul[i] = g_atom.Aul[i] / dH;
                g_atom.Blu[i] = g_atom.Bul[i] / g_atom.glu[i];
                g_atom.RDw[i] = C1 * nu * sqrt(g_atom.rDw[i]);

                /* Radiation indexing */
                g_atom.i_J[i] = (int **)malloc(3 * sizeof(int *));

                for (K = 0; K <= 2; K++) {
                    Qb               = 2 * K + 1;
                    g_atom.i_J[i][K] = (int *)malloc(Qb * sizeof(int));
                    for (Q = 0; Q < Qb; Q++) {
                        j++;
                        g_atom.i_J[i][K][Q] = j;
                    }
                }
            }
            g_atom.jdim = j + 1;
        }

        /* Level quantities */
        j           = -1;
        g_atom.MaxK = 0;
        for (i = 0; i < g_atom.nlevel; i++) {
            /* K limit */
            J2 = g_atom.J2[i];
            if (J2 < g_Kcut) {
                g_atom.maxK[i] = J2;
            } else {
                g_atom.maxK[i] = g_Kcut;
            }

            if (g_atom.maxK[i] > g_atom.MaxK) {
                g_atom.MaxK = g_atom.maxK[i];
            }

            g_atom.i_rho[i] =
                (int **)malloc((g_atom.maxK[i] + 1) * sizeof(int *));

            for (K = 0; K <= g_atom.maxK[i]; K++) {
                Qb                 = 2 * K + 1;
                g_atom.i_rho[i][K] = (int *)malloc(Qb * sizeof(int));
                for (Q = 0; Q < Qb; Q++) {
                    j++;
                    g_atom.i_rho[i][K][Q] = j;
                }
            }

/* Magnetic quantum numbers */
#if defined(HANLE_ZEEMAN_FS)
            g_atom.nM[i]    = J2 + 1;
            g_atom.M2[i]    = (int *)malloc(g_atom.nM[i] * sizeof(int));
            g_atom.M2[i][0] = -J2;
            for (K = 1; K < g_atom.nM[i]; K++) {
                g_atom.M2[i][K] = g_atom.M2[i][K - 1] + 2;
            }
#endif
        }

        /* SEE dimension */
        g_atom.rdim = j + 1;

        /* Allocate and define Qind */
        g_Qind    = (int *)malloc((2 * g_atom.MaxK + 1) * sizeof(int));
        g_Qind[0] = 0;
        i         = 0;
        for (K = 1; K <= g_atom.MaxK; K++) {
            i++;
            g_Qind[i] = K;
            i++;
            g_Qind[i] = -K;
        }
    }

    /* Indicate size of node */
    g_nodesize =
        sizeof(double) * (9 + g_atom.rdim + g_atom.jdim + g_atom.rtran * 3 +
                          g_atom.ntran + g_atom.nlevel);

    /*               */
    /* Sanity checks */
    /*               */

    /* Atom mass must be positive */
    if (g_atom.mass <= 0) {
        Error(E_ERROR, POR_AT,
              "atomic mass must be positive: "
              "%e <= 0",
              g_atom.mass);
    }

    /* Level quantities */
    for (int i = 0; i < g_atom.nlevel; i++) {
        if (i < (g_atom.nlevel - 1)) {
            /* Energy must be ordered */
            if (g_atom.E[i] >= g_atom.E[i + 1]) {
                Error(E_ERROR, POR_AT,
                      "energy must be in increasing "
                      "order: E[%d]=%e >= E[%d]=%e",
                      i, g_atom.E[i], i + 1, g_atom.E[i + 1]);
                return 0;
            }
        }

        /* J2 must be positive */
        if (g_atom.J2[i] < 0) {
            Error(E_ERROR, POR_AT,
                  "angular momentum cannot be "
                  "negative: J2[%d]=%d < 0",
                  i, g_atom.J2[i]);
            return 0;
        }
    }

    /* Transition quantities */
    for (int i = 0; i < g_atom.ntran; i++) {
        /* Indexes must be in nlevel range */
        if (g_atom.il[i] < 0 || g_atom.il[i] >= g_atom.nlevel) {
            Error(E_ERROR, POR_AT,
                  "lower level index for transition "
                  "%d must be in the range of the "
                  "levels: %d < 0 || %d > %d",
                  i, g_atom.il[i], g_atom.il[i], g_atom.nlevel);
            return 0;
        }

        /* Indexes must be in nlevel range */
        if (g_atom.iu[i] < 0 || g_atom.iu[i] >= g_atom.nlevel) {
            Error(E_ERROR, POR_AT,
                  "upper level index for transition "
                  "%d must be in the range of the "
                  "levels: %d < 0 || %d > %d",
                  i, g_atom.iu[i], g_atom.iu[i], g_atom.nlevel);
            return 0;
        }

        /* Indexes must be iu->il */
        if (g_atom.il[i] >= g_atom.iu[i]) {
            Error(E_ERROR, POR_AT,
                  "level indexes for transition "
                  "%d must be upper->lower: "
                  "%d >= %d",
                  i, g_atom.il[i], g_atom.iu[i]);
            return 0;
        }

        /* Radiative quantities */
        if (i >= g_atom.rtran) {
            continue;
        }

        /* Einstein coefficient must be positive */
        if (g_atom.Aul[i] <= 0.0) {
            Error(E_ERROR, POR_AT,
                  "Einstein coefficient for "
                  "transition %d must be "
                  "positive: %d <= 0",
                  i, g_atom.Aul[i]);
            return 0;
        }

        /* Core frequencies must be odd */
        if (g_atom.nfreqc[i] % 2 == 0) {
            Error(E_WARNING, POR_AT,
                  "number of frequencies for the "
                  "core of transition number %d "
                  "is even, must be odd, changed "
                  "%d -> %d",
                  i, g_atom.nfreqc[i], g_atom.nfreqc[i] + 1);
            g_atom.nfreqc[i] = g_atom.nfreqc[i] + 1;
        }

        /* Frequencies must be odd */
        if (g_atom.nfreq[i] % 2 == 0) {
            Error(E_WARNING, POR_AT,
                  "number of frequencies for "
                  "transition number %d "
                  "is even, must be odd, changed "
                  "%d -> %d",
                  i, g_atom.nfreq[i], g_atom.nfreq[i] + 1);
            g_atom.nfreq[i] = g_atom.nfreq[i] + 1;
        }

        /* At least 3 frequency points */
        if (g_atom.nfreqc[i] <= 2) {
            Error(E_ERROR, POR_AT,
                  "number of frequencies for the "
                  "core of transition number %d "
                  "is too small: %d <= 2",
                  i, g_atom.nfreqc[i]);
            return 0;
        }

        /* At least 3 frequency points */
        if (g_atom.nfreq[i] <= 2) {
            Error(E_ERROR, POR_AT,
                  "number of frequencies for "
                  "transition number %d "
                  "is too small: %d <= 2",
                  i, g_atom.nfreq[i]);
            return 0;
        }

        /* Core must be less than total */
        if (g_atom.nfreqc[i] > g_atom.nfreq[i]) {
            Error(E_ERROR, POR_AT,
                  "number of frequencies for the "
                  "core of transition number %d is "
                  "larger than total: %d > %d",
                  i, g_atom.nfreqc[i], g_atom.nfreq[i]);
            return 0;
        }

        /* Core must be less than total */
        if (g_atom.Dwc[i] > g_atom.Dw[i]) {
            Error(E_ERROR, POR_AT,
                  "Doppler width for the core "
                  "of transition number %d is "
                  "larger than total: %e > %e",
                  i, g_atom.Dwc[i], g_atom.Dw[i]);
            return 0;
        }

        /* Core Doppler width must be positive */
        if (g_atom.Dwc[i] <= 0.0) {
            Error(E_ERROR, POR_AT,
                  "Doppler width for the core "
                  "of transition number %d is "
                  "non-positive: %e <= 0",
                  i, g_atom.Dwc[i]);
            return 0;
        }

        /* Doppler width must be positive */
        if (g_atom.Dw[i] <= 0.0) {
            Error(E_ERROR, POR_AT,
                  "Doppler width for "
                  "of transition number %d is "
                  "non-positive: %e <= 0",
                  i, g_atom.Dw[i]);
            return 0;
        }
    }

    /* global lab frequencies */
    MakeFrequencies();

    /* Get node data size */
    *node_pmd_size = F_GetNodeDataSize();

    /* Return success */
    return 1;
}

#endif /* SMODE_FULL */
