/* Copyright (C) 2013 by Jiří Štěpán, stepan@asu.cas.cz */

/* console of the slave nodes */

#ifndef CSLAVE_H_
#define CSLAVE_H_

/* an infinite loop on a slave process: receives signals from master-node's
 * console */
extern void RunConsoleSlave(void);

#endif /* CSLAVE_H_ */
