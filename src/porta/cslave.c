/* Copyright (C) 2013 by Jiří Štěpán, stepan@asu.cas.cz */

#include "cslave.h"
#include "portal/def.h"
#include "portal/error.h"
#include "portal/fs.h"
#include "portal/global.h"
#include "portal/io.h"
#include "portal/modules.h"
#include "portal/process.h"
#include "portal/slv.h"
#include "portal/stack.h"
#include "portal/thread_io.h"

/*******************************************************************************/
void RunConsoleSlave(void)
{
    int    signal = SIG_NONE;
    int    i1, i2, i3;
    double d1, d2, d3;

    if (!G_MY_MPI_RANK)
        IERR;

    do {
        signal = pro_ReceiveSignalFromMaster();
        stk_Add(0, POR_AT,
                "process [%d] received signal %d from the master process",
                G_MY_MPI_RANK, signal);

        /* set fuction pointer according to binary or HDF5 I/O */
        switch (signal) {
        case SIG_SAVEGRID:
            io_SaveGridSlave_fp = &io_SaveGridSlave;
            break;
        case SIG_SAVEGRID_H5:
            io_SaveGridSlave_fp = &io_SaveGridSlave_h5;
            break;
        case SIG_LOADGRID:
            io_LoadGridSlave_fp = &io_LoadGridSlave;
            break;
        case SIG_LOADGRID_H5:
            io_LoadGridSlave_fp = &io_LoadGridSlave_h5;
            break;
        }

        switch (signal) {
        case SIG_EXIT:
            ExitPorta(0);
            /* Free-up communicators */
            MPI_Comm_free(&zband_comm);
            MPI_Comm_free(&freqband_comm);

#ifdef THREAD_IO
            end_io_comm(&io_comm);
#endif

            MPI_Finalize();
            exit(EXIT_SUCCESS);
            break;

        case SIG_SAVEGRID_H5: /*fall through*/
        case SIG_SAVEGRID:
            if (!PorMeshInitialized() || !mod_IsModuleLoaded())
                IERR;
            if (!(*io_SaveGridSlave_fp)()) {
                Error(E_ERROR, POR_AT, "the grid could not be stored");
            }
            break;

        case SIG_LOADGRID_H5: /*fall through*/
        case SIG_LOADGRID:
            if (!(*io_LoadGridSlave_fp)()) {
                Error(E_ERROR, POR_AT, "the grid could not be loaded");
            }
            break;

        case SIG_DIRECTIONS:
            if (!pro_S_RecvDirs()) {
                Error(E_ERROR, POR_AT, "cannot set direction quadrature");
            }
            break;

        case SIG_JACOBI:
            /* get #of iterations & max. relative change from master */
            if (MPI_SUCCESS != MPI_Bcast(&i1, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            d1 = slv_Jacobi(&g_global->grid[0], i1, d1);
            if (G_MY_MPI_RANK == 1 &&
                MPI_SUCCESS !=
                    MPI_Send(&d1, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD))
                CERR; /* process #1 reports the max. rel. change to master */
            break;

        case SIG_FSRAY:
            if (MPI_SUCCESS != MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&d2, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i1, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i2, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            {
                t_vec_3d dir;
                double   in = M_PI * d1 / 180.0, az = M_PI * d2 / 180.0;
                dir.x = sin(in) * cos(az);
                dir.y = sin(in) * sin(az);
                dir.z = cos(in);
                fs_FSSurfaceXY(
                    dir, i1,
                    i2); /* this solves the RT problem and sends (if at the top
                            of the atmosphere) the spectral data to master
                            (catched by too_SurfaceMap / too_SurfaceMap_h5) */
            }
            break;

        case SIG_LC_FSRAY:
            if (MPI_SUCCESS != MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&d2, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i1, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i2, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            {
                t_vec_3d dir;
                double   in = M_PI * d1 / 180.0, az = M_PI * d2 / 180.0;
                dir.x = sin(in) * cos(az);
                dir.y = sin(in) * sin(az);
                dir.z = cos(in);
                fs_FSSurface_LC_XY(
                    dir, i1,
                    i2); /* this solves the RT problem and sends (if at the top
                            of the atmosphere) the spectral data to master
                            (catched by too_SurfaceMap / too_SurfaceMap_h5) */
            }
            break;

        case SIG_FSCLV:
            if (MPI_SUCCESS != MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&d2, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&d3, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i1, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i2, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            fs_FSCLVFreq(
                d1, d2, d3, i1,
                i2); /* solves RT and sends to too_CLVFreq run by the master */
            break;

        case SIG_FSCLVAV_LC: /* fall through */
        case SIG_FSCLVAV:
            if (MPI_SUCCESS != MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&d2, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i1, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i2, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i3, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            if (signal == SIG_FSCLVAV) {
                fs_FSCLVFreqAV(d1, d2, i1, i2,
                               i3); /* solves RT and sends to too_CLVFreqAV run
                                       by the master */
            } else if (signal == SIG_FSCLVAV_LC) {
                fs_FSCLVFreqAV_LC(d1, d2, i1, i2,
                                  i3); /* solves RT and sends to too_CLVFreqAV
                                          run by the master */
            }
            break;

        case SIG_FSAZAV:
            if (MPI_SUCCESS != MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i1, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i2, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i3, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            fs_FSAZAV(d1, i1, i2, i3); /* solves RT and sends the data to
                                          too_FSAZAV run by the master */
            break;

        case SIG_GETTAU:
            if (MPI_SUCCESS != MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&d2, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&d3, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i1, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            if (MPI_SUCCESS != MPI_Bcast(&i2, 1, MPI_INT, 0, MPI_COMM_WORLD))
                CERR;
            {
                t_vec_3d dir;
                double   in = M_PI * d2 / 180.0, az = M_PI * d3 / 180.0;
                // Reversing the ray direction
                dir.x = -sin(in) * cos(az);
                dir.y = -sin(in) * sin(az);
                dir.z = -cos(in);
                fs_GetTau(
                    d1, dir, i1,
                    i2); /* catched by too_SurfaceTau / too_SurfaceTau_h5 */
            }
            break;

        default:
            break;
        }
    } while (1);
}
