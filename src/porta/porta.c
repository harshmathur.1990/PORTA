/* Copyright (C) 2013 by Jiří Štěpán, stepan@asu.cas.cz */

#include "portal/def.h"
#include "portal/directions.h"
#include "portal/error.h"
#include "portal/fs.h"
#include "portal/global.h"
#include "portal/grid.h"
#include "portal/matika.h"
#include "portal/mem.h"
#include "portal/plot.h"
#include "portal/portal.h"
#include "portal/process.h"
#include "portal/stack.h"
#include "portal/thread_io.h"
#include "portal/tools.h"
#include "portal/topology.h"
#include "portal/voigt.h"
#include <float.h>
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "cmaster.h"
#include "cslave.h"

#define SYNTAX_HELP                                                            \
    "\nSyntax:\n\tmpirun -np #processes ./porta #z-subdomains "                \
    "model-script.por\n"                                                       \
    "Total number of processes: 1+#processes (1 for the master node).\n"       \
    "Each subdomain can be parallelized in #freq-bands radiation "             \
    "frequencies\n"                                                            \
    "so that #processes = 1 + #z-subdomains * #freq-bands.\n"                  \
    "If no frequency parallelization is used, then #freq-bands=1.\n"           \
    "At least #processes=2 must be used.\n"

/*******************************************************************************/
int main(int argc, char *argv[])
{
    char stmp[2048];

#ifdef THREAD_IO
    int thread_support;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &thread_support);
    if (thread_support != MPI_THREAD_MULTIPLE)
        Error(
            E_ERROR, POR_AT,
            "MPI library does not support MPI_THREAD_MULTIPLE. Compile without "
            "THREAD_IO\n");
#else
    MPI_Init(&argc, &argv);
#endif
    MPI_Comm_rank(MPI_COMM_WORLD, &G_MY_MPI_RANK);
    MPI_Comm_size(MPI_COMM_WORLD, &G_MPI_NPROCS);

    if (!G_MY_MPI_RANK && G_MPI_NPROCS < 2) {
        Error(E_ERROR, POR_AT,
              "invalid number of MPI processes (#processes=%d)\n%s",
              G_MPI_NPROCS, SYNTAX_HELP);
    }

    /* MPI does not guarantee that the command line arguments will be passed to
     * all processes: broadcast them from the root */
    if (!G_MY_MPI_RANK) {
        if (argc != 3)
            Error(E_ERROR, POR_AT,
                  "invalid number of command-line parameters\n%s", SYNTAX_HELP);
        if (1 != sscanf(argv[1], "%d", &G_MPI_L))
            Error(E_ERROR, POR_AT, "invalid number of subdomains");
        if (G_MPI_L < 1 || G_MPI_L > G_MPI_NPROCS - 1) {
            Error(E_ERROR, POR_AT,
                  "invalid number of subdomains (%d) using %d slave processes",
                  G_MPI_L, G_MPI_NPROCS - 1);
        }
        if ((G_MPI_NPROCS - 1) % G_MPI_L) {
            Error(E_ERROR, POR_AT,
                  "number of slave processes (%d) has to be an integer "
                  "multiple of "
                  "the number of subdomains (%d)",
                  G_MPI_NPROCS - 1, G_MPI_L);
        }
        if (MPI_SUCCESS != MPI_Bcast(&G_MPI_L, 1, MPI_INT, 0, MPI_COMM_WORLD))
            Error(E_ERROR, POR_AT, "cannot broadcast number of domains");

        if (1 != sscanf(argv[2], "%s", stmp))
            Error(E_ERROR, POR_AT,
                  "cannot read the name of the POR script file");
        if (!(G_POR = (FILE *)fopen(argv[2], "rt")))
            Error(E_ERROR, POR_AT, "cannot open the POR script file");
    } else {
        if (MPI_SUCCESS != MPI_Bcast(&G_MPI_L, 1, MPI_INT, 0, MPI_COMM_WORLD))
            Error(E_ERROR, POR_AT,
                  "cannot receive the broadcasted number of domains");
    }

    /* number of frequency processes per domain */
    G_MPI_M = (G_MPI_NPROCS - 1) / G_MPI_L;

    /* Create communicators for Z-bands and Frequency-bands */
    G_MY_ZBAND_ID = pro_Domain_Z_ID(G_MY_MPI_RANK);
    G_MY_FREQBAND_ID =
        pro_MyFreqBand_ID() + 1; // color for MPI_Comm_split cannot be negative
    if (MPI_SUCCESS != MPI_Comm_split(MPI_COMM_WORLD, G_MY_ZBAND_ID,
                                      G_MY_MPI_RANK, &zband_comm))
        Error(E_ERROR, POR_AT, "Cannot create zband_comm communicator");
    if (MPI_SUCCESS != MPI_Comm_split(MPI_COMM_WORLD, G_MY_FREQBAND_ID,
                                      G_MY_MPI_RANK, &freqband_comm))
        Error(E_ERROR, POR_AT, "Cannot create freqband_comm communicator");

    /* Create slaves communicator */
    if (MPI_SUCCESS !=
        MPI_Comm_split(MPI_COMM_WORLD, IAM_MASTER, G_MY_MPI_RANK, &slaves_comm))
        Error(E_ERROR, POR_AT, "Cannot create slaves_comm communicator");

#ifdef THREAD_IO
    // Initialize io_comm
    init_io_comm(MPI_COMM_WORLD, &io_comm);
#endif

    /* run Porta */
    InitPorta();
    if (!G_MY_MPI_RANK) {
        RunConsoleMaster();
        fclose(G_POR);
    } else {
        RunConsoleSlave();
    }

    /*********************************************************
     * you can put any commands over here and execute them   *
     * by using the CONSOLE_CHEAT command in the console     *
     *********************************************************/

    /* the following code is only executed if the cheating command has been used
     * to escape the console loop */
    ExitPorta(0);

    MPI_Finalize(); /* this is to be called by each process */
    return EXIT_SUCCESS;
}
