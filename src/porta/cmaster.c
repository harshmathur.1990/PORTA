/* Copyright (C) 2013 by Jiří Štěpán, stepan@asu.cas.cz */

#include "portal/def.h"
#include "portal/directions.h"
#include "portal/error.h"
#include "portal/ese.h"
#include "portal/fs.h"
#include "portal/global.h"
#include "portal/grid.h"
#include "portal/io.h"
#include "portal/matika.h"
#include "portal/mem.h"
#include "portal/modules.h"
#include "portal/plot.h"
#include "portal/portal.h"
#include "portal/process.h"
#include "portal/slv.h"
#include "portal/stack.h"
#include "portal/thread_io.h"
#include "portal/tools.h"
#include "portal/topology.h"
#include "portal/voigt.h"
#include <stdio.h>
#include <string.h>

#include "cmaster.h"

/* skip rest of the line in G_POR */
#define SKIPLINE                                                               \
    {                                                                          \
        int i;                                                                 \
        do                                                                     \
            i = fgetc(G_POR);                                                  \
        while (i != '\n' && i != EOF);                                         \
    }
/* whitespace test */
#define WHITE(c) ((c) == ' ' || (c) == '\t' || (c) == '\n')

#define MSG_READONLY(par)                                                      \
    {                                                                          \
        fprintf(stderr, "variable \"%s\" is read-only\n", cParam[par]);        \
        SKIPLINE;                                                              \
    }

#define MAX_IDEN_LEN 127

#define CONSOLE_CHEAT "q"

/*
 * user commands
 */

#define N_COMMANDS 21

static const char cCommand[N_COMMANDS][MAX_IDEN_LEN + 1] = {
    "help",
    "echo",
    "set",
    "get",
    "version",
    "exit",
    "fs_surface",    /* performs FS along a general ray direction for all
                        frequencies and stores the emergent radiation into a
                        PSP disk file */
    "fs_surface_h5", /* As fs_surface_h5, but PSP file stored in HDF5 format */
    "fs_surface_lc", /* Calculation of emergent radiation. As fs_surface, but
                        using long characteristics */
    "fs_surface_lc_h5", /* As fs_surface_lc, but PSP file stored in HDF5
                           format */
    "fs_clv_freq",      /* FS for calculation of the center-to-limb variation at
                           a given discrete frequency of radiation, spatially
                           averaged WARNING: ONLY FOR HORZIONTALLY EQUIDISTANT
                           GRIDS! */
    "fs_clv_freq_av",   /* FS for calculation of the center-to-limb variation at
                           a given discrete frequency of radiation, spatially
                           averaged, azimuth averaged WARNING: ONLY FOR
                           HORZIONTALLY EQUIDISTANT GRIDS! */
    "fs_clv_freq_av_lc", /* As fs_clv_freq_av, but using long characteristics
                            method for the calculation of the emergent
                            radiation */
    "fs_azaver",         /* FS averaged over all grid points and all azimuths;
                            results in a single stokes profile */
    "get_tau_pos",       /* for a general ray direction calculates geometrical
                            coordinates of points with a given tau distance from
                            a surface (izmax) nodes; stores the data in a *.TAU
                            file */
    "get_tau_pos_h5",    /* TAU file store in HDF5 format */
    "solve_jacobi",      /* solve the non-LTE problem in the finest grid using
                            Jacobi iteration */
    "savemodel",         /* save the curent model into a PMD file */
    "savemodel_h5",      /* save the curent model into a PMD file (in HDF5
                            format) */
    "loadmodel",         /* load a PMD file */
    "loadmodel_h5"       /* load a PMD file (in HDF5 format) */
};
/* abreviations of the commands (must be consistent with cCommand!) */
enum cCommandID {
    CC_HELP = 0,
    CC_ECHO,
    CC_SET,
    CC_GET,
    CC_VERSION,
    CC_EXIT,
    CC_FSSURFACE,
    CC_FSSURFACE_H5,
    CC_FSSURFACE_LC,
    CC_FSSURFACE_LC_H5,
    CC_FSCLVFREQ,
    CC_FSCLVFREQAV,
    CC_FSCLVFREQAV_LC,
    CC_FSAZAVER,
    CC_GETTAU,
    CC_GETTAU_H5,
    CC_SOLVE_JACOBI,
    CC_SAVEMODEL,
    CC_SAVEMODEL_H5,
    CC_LOADMODEL,
    CC_LOADMODEL_H5
};

/*
 * variables
 */

#define N_PARAMS 9

static const char cParam[N_PARAMS][MAX_IDEN_LEN + 1] = {
    "nincl",       /* number of inclination angles per octant */
    "nazim",       /* number of azimuthal angles per octant */
    "stack_out",   /* stack output */
    "nfreq",       /* number of radiation frequencies */
    "nnodes",      /* total number of grid nodes in the finest grid */
    "domain",      /* domain origin and dimensions */
    "period",      /* horizontal periodicity of the domain on/off */
    "wavelengths", /* print all the radiation wavelengths to the standard output
                    */
    "quad"         /* near optimal angular quadrature set */
};
/* abreviations of the parameters (must be consistent with cParam!) */
enum cParamID {
    PA_NDIRS_INCL = 0,
    PA_NDIRS_AZIM,
    PA_STACKOUT,
    PA_NFREQ,
    PA_NNODES,
    PA_DOMAIN,
    PA_PERIOD,
    PA_WAVELENGTHS,
    PA_QUAD
};

/* Implementation for selecting different special quadratures
 * like the published in Stepan etal 2020 */
#define N_QUADS 13

static const char quad_sets[N_QUADS][MAX_IDEN_LEN + 1] = {
    "S1_i_L3n1",     "S1_i_L7n3",    "S1_i_L9n6",      "S1_i_L11n7",
    "S1_i_L13n10",   "S1_i_L15n11",  "S1_iquv_L3n3",   "S1_iquv_L5n4",
    "S1_iquv_L7n7",  "S1_iquv_L9n8", "S1_iquv_L11n11", "S1_iquv_L13n14",
    "S1_iquv_L15n18"};

/* global variables (global.c) */
extern t_global_vars *g_global;
extern FILE *         G_POR; /* the model script file */

/*******************************************************************************/
/* reads command from G_POR
 * return: -1 if unknown command, otherwise index number of command
 *        in cCommand, and N_COMMANDS if the line is commented-out
 */
static int ReadCommand(void)
{
    char cmd[MAX_IDEN_LEN + 1];
    int  i;

    if (feof(G_POR))
        return CC_EXIT;

    if (fscanf(G_POR, "%" POR_TOSTRING(MAX_IDEN_LEN) "s", cmd) <= 0)
        return CC_EXIT;

    if (cmd[0] == COMMENT_CHAR) {
        SKIPLINE;
        return N_COMMANDS;
    }
    /* "the secret command" closing the console and allowing to execute the
     * remaining code in main() */
    if (!strcmp(cmd, CONSOLE_CHEAT)) {
        fprintf(stderr, "*** entering wild mode ***\n");
        return -2;
    }
    /* browse available commands */
    for (i = 0; i < N_COMMANDS; i++) {
        if (!strcmp(cmd, cCommand[i]))
            return i;
    }
    fprintf(stderr, "unknown command: %s\n", cmd);
    SKIPLINE;
    return -1;
}

/*******************************************************************************/
/* reads parameter name from G_POR
 * return: -1 if unknown command, otherwise index number of parameter in cParam
 * and N_PARAMS if the line is comemnted-out
 */
static int ReadParam(void)
{
    char cmd[MAX_IDEN_LEN + 1];
    int  i;
    if (fscanf(G_POR, "%" POR_TOSTRING(MAX_IDEN_LEN) "s", (char *)&cmd))
        ;
    if (cmd[0] == COMMENT_CHAR) {
        SKIPLINE;
        return N_PARAMS;
    }
    for (i = 0; i < N_PARAMS; i++) {
        if (!strcmp(cmd, cParam[i]))
            return i;
    }
    fprintf(stderr, "UNKNOWN PARAMETER: %s\n", cmd);
    SKIPLINE;
    return -1;
}

/*******************************************************************************/
/* reads integer quantity
 * sets value of variable (only if reading succeeded)
 * return: 0 if error, 1 otherwise
 */
static int ReadInt(int *variable)
{
    int i;
    if (!fscanf(G_POR, "%d", &i)) {
        fprintf(stderr, "invalid integer value\n");
        return 0;
    }
    *variable = i;
    return 1;
}

/*******************************************************************************/
/* reads floating point (double) quantity
 * sets value of variable (only if reading succeeded)
 * return: 0 if error, 1 otherwise
 */
static int ReadDouble(double *variable)
{
    double d;
    if (!fscanf(G_POR, "%lf", &d)) {
        fprintf(stderr, "invalid floating point value\n");
        return 0;
    }
    *variable = d;
    return 1;
}

/*******************************************************************************/
/* reads string until end of line/eof, of maximum length MAX_STRING
 * sets str array
 * return: 0 if error, 1 otherwise
 * note: leading and final whitespaces are skipped
 */
static int ReadString(char str[])
{ /* WARNING: reads also the final \n : don't use SKIPLINE at the end of the
     line reading otherwise you skip the next command line */
    int st;
    if (!fgets(str, MAX_STRING, G_POR)) {
        fprintf(stderr, "can't read string\n");
        return 0;
    }
    /* skip leading whitespaces: */
    st               = 0;
    str[strlen(str)] = 0;
    while (st < strlen(str) && WHITE(str[st]))
        ++st;
    if (st > 0) {
        memmove(&str[0], &str[0] + st, strlen(str) - st + 1);
    }
    /* remove the final whitespaces */
    while (strlen(str) > 0 && WHITE(str[strlen(str) - 1]))
        str[strlen(str) - 1] = 0;
    return 1;
}

/*******************************************************************************/
#ifdef UNDEF
/* removes whitespace from the end of the string */
static void CutRestWhite(char str[])
{
    int i = strlen(str) - 1;
    while (i >= 0 && WHITE(str[i]))
        str[i--] = 0;
}
#endif

/*******************************************************************************/
/* start the console */
void RunConsoleMaster(void)
{
    int        cmd, par, tmp, tmp2, tmp3, i1, i2, i3;
    char       tmps[MAX_STRING + 1];
    double     d1, d2, d3;
    t_vec_3d   v1, v2;
    MPI_Status status;

    /* to branch in hdf5/binary */
    int signal;

    if (G_MY_MPI_RANK)
        IERR; /* only master node can run this loop */

    do {
        /* if invalid command-> skip the rest of the command line: */
        if ((cmd = ReadCommand()) < 0 && (cmd != -2))
            continue;
        /* exit console */
        if (cmd == -2)
            return;
        /* comment */
        if (cmd == N_COMMANDS)
            continue;
        /* process arguments of the command: */

        /* set signal and fuction pointer according to binary or HDF5 I/O */
        switch (cmd) {
        case CC_GETTAU_H5:
            too_SurfaceTau_fp = &too_SurfaceTau_h5;
            break;
        case CC_GETTAU:
            too_SurfaceTau_fp = &too_SurfaceTau;
            break;
        case CC_FSSURFACE_LC_H5:
            too_SurfaceMap_fp = &too_SurfaceMap_h5;
            break;
        case CC_FSSURFACE_LC:
            too_SurfaceMap_fp = &too_SurfaceMap;
            break;
        case CC_FSSURFACE_H5:
            too_SurfaceMap_fp = &too_SurfaceMap_h5;
            break;
        case CC_FSSURFACE:
            too_SurfaceMap_fp = &too_SurfaceMap;
            break;
        case CC_LOADMODEL_H5:
            signal               = SIG_LOADGRID_H5;
            io_LoadGridMaster_fp = &io_LoadGridMaster_h5;
            break;
        case CC_LOADMODEL:
            signal               = SIG_LOADGRID;
            io_LoadGridMaster_fp = &io_LoadGridMaster;
            break;
        case CC_SAVEMODEL_H5:
            signal               = SIG_SAVEGRID_H5;
            io_SaveGridMaster_fp = &io_SaveGridMaster_h5;
            break;
        case CC_SAVEMODEL:
            signal               = SIG_SAVEGRID;
            io_SaveGridMaster_fp = &io_SaveGridMaster;
            break;
        }

        switch (cmd) {
        case CC_HELP:
            SKIPLINE;
            fprintf(stderr, "list of commands:\n");
            for (tmp = 0; tmp < N_COMMANDS; tmp++) {
                fprintf(stderr, "\t%s\n", cCommand[tmp]);
            }
            fprintf(stderr, "list of variables:\n");
            for (tmp = 0; tmp < N_PARAMS; tmp++) {
                fprintf(stderr, "\t%s\n", cParam[tmp]);
            }
            break;
        case CC_ECHO:
            if (!ReadString(tmps))
                continue;
            fprintf(stderr, "%s\n", tmps);
            fflush(stderr);
            break;
            /*****************************************/
        case CC_SET:
            if ((par = ReadParam()) < 0)
                continue;
            /* comment instead of parameter? */
            if (par == N_PARAMS) {
                fprintf(stderr, "variable expected\n");
                continue;
            }
            switch (par) {
            case PA_NDIRS_INCL:
                tmp = g_global->dirs.n_incl_oct;
                if (!ReadInt(&g_global->dirs.n_incl_oct)) {
                    SKIPLINE;
                    continue;
                }
                if (g_global->dirs.n_incl_oct * g_global->dirs.n_azim_oct * 8 >
                        MAXNDIRS ||
                    g_global->dirs.n_incl_oct < 1) {
                    g_global->dirs.n_incl_oct = tmp;
                    fprintf(stderr, "invalid number of rays\n");
                } else {
                    if (g_global->dirs.n_azim_oct == -1) {
                        dir_MakeDirections(g_global->dirs.n_incl_oct,
                                           DEF_NDI_AZIM, &g_global->dirs);
                    } else {
                        dir_MakeDirections(g_global->dirs.n_incl_oct,
                                           g_global->dirs.n_azim_oct,
                                           &g_global->dirs);
                    }
                    if (!pro_SignalToAllSlaves(SIG_DIRECTIONS) ||
                        !pro_M2S_SendDirs()) {
                        Error(E_ERROR, POR_AT,
                              "cannot synchronize direction quadrature");
                    }
                }
                SKIPLINE;
                break;
            case PA_NDIRS_AZIM:
                tmp = g_global->dirs.n_azim_oct;
                if (!ReadInt(&g_global->dirs.n_azim_oct)) {
                    SKIPLINE;
                    continue;
                }
                if (g_global->dirs.n_incl_oct * g_global->dirs.n_azim_oct * 8 >
                        MAXNDIRS ||
                    g_global->dirs.n_azim_oct < 1) {
                    g_global->dirs.n_azim_oct = tmp;
                    fprintf(stderr, "invalid number of rays\n");
                } else {
                    dir_MakeDirections(g_global->dirs.n_incl_oct,
                                       g_global->dirs.n_azim_oct,
                                       &g_global->dirs);
                    if (!pro_SignalToAllSlaves(SIG_DIRECTIONS) ||
                        !pro_M2S_SendDirs()) {
                        Error(E_ERROR, POR_AT,
                              "cannot synchronize direction quadrature");
                    }
                }
                SKIPLINE;
                break;
            case PA_STACKOUT:
                tmp = g_global->stack_output;
                if (!ReadInt(&g_global->stack_output)) {
                    SKIPLINE;
                    continue;
                }
                if (g_global->stack_output < SOUT_OFF ||
                    g_global->stack_output > SOUT_MAX) {
                    g_global->stack_output = tmp;
                    fprintf(stderr, "invalid stack output\n");
                }
                SKIPLINE;
                break;
            case PA_NFREQ:
                MSG_READONLY(PA_NFREQ);
                continue;
                break;
            case PA_NNODES:
                MSG_READONLY(PA_NNODES);
                continue;
                break;
            case PA_DOMAIN:
                if (!ReadDouble(&v1.x) || !ReadDouble(&v1.y) ||
                    !ReadDouble(&v1.z) || /* origin */
                    !ReadDouble(&v2.x) || !ReadDouble(&v2.y) ||
                    !ReadDouble(&v2.z)) /* size   */
                {
                    SKIPLINE;
                    fprintf(stderr,
                            "invalid input; use \"set domain orig_x orig_y "
                            "orig_z  dim_x dim_y dim_z\"\n");
                    continue;
                }
                grd_SetDomainGeometry(v1, v2);
                fprintf(stderr,
                        "domain geometry: origin=[%3.3e,%3.3e,%3.3e] cm, "
                        "dimensions=[%3.3e,%3.3e,%3.3e] cm\n",
                        g_global->domain_origin.x, g_global->domain_origin.y,
                        g_global->domain_origin.z, g_global->domain_size.x,
                        g_global->domain_size.y, g_global->domain_size.z);
                SKIPLINE;
                continue;
                break;
            case PA_PERIOD:
                if (ReadInt(&tmp)) {
                    if (tmp) {
                        /* TODO: this has to be generalized for partially
                         * periodic grids */
                        g_global->period[0] = g_global->period[1] = 1;
                    } else {
                        /* Non-periodic boundaries not thoroughly tested.
                           Limiting PORTA to periodic boundaries. Jan'20 [AdV]
                         */
                        Error(E_ERROR, POR_AT,
                              "This version of PORTA needs periodic boundaries "
                              "along X and "
                              "Y");
                    }
                } else {
                    fprintf(stderr, "invalid input; use \"period 1 (true) or 0 "
                                    "(false)\"\n");
                }
                SKIPLINE;
                break;

            case PA_WAVELENGTHS:
                Error(E_WARNING, POR_AT,
                      "wavelengths cannot be changed from the command line");
                SKIPLINE;
                break;

            case PA_QUAD:
                if (!ReadString(tmps))
                    continue;

                for (i1 = 0; i1 < N_QUADS; i1++) {
                    if (!strcmp(tmps, quad_sets[i1]))
                        break;
                }
                if (i1 == N_QUADS) {
                    Error(E_ERROR, POR_AT, "the input set is not defined");
                } else {
                    g_global->dirs.n_incl_oct = i1;
                    g_global->dirs.n_azim_oct = -1;
                    dir_MakeDirectionsQuadSet(i1, &g_global->dirs);
                    if (!pro_SignalToAllSlaves(SIG_DIRECTIONS) ||
                        !pro_M2S_SendDirs()) {
                        Error(E_ERROR, POR_AT,
                              "cannot synchronize direction quadrature");
                    }
                }
                break;

            default:
                IERR;
                break;
            };
            break;
            /*****************************************/
        case CC_GET:
            if ((par = ReadParam()) < 0)
                continue;
            /* comment instead of parameter? */
            if (par == N_PARAMS) {
                fprintf(stderr, "variable expected\n");
                continue;
            }
            switch (par) {
            case PA_NDIRS_INCL:
                if (g_global->dirs.n_azim_oct == -1)
                    Error(E_ERROR, POR_AT, "no number of inclinations set");
                fprintf(stderr, "%d\n", g_global->dirs.n_incl_oct);
                break;
            case PA_NDIRS_AZIM:
                if (g_global->dirs.n_azim_oct == -1)
                    Error(E_ERROR, POR_AT, "no number of azimuths set");
                fprintf(stderr, "%d\n", g_global->dirs.n_azim_oct);
                break;
            case PA_STACKOUT:
                fprintf(stderr, "%d\n", g_global->stack_output);
                break;
            case PA_NFREQ:
                fprintf(stderr, "%d\n", PorNFreqs());
                break;
            case PA_NNODES:
                fprintf(stderr, "%d\n", PorNNodes());
                break;
            case PA_DOMAIN:
                fprintf(stderr,
                        "%17.17e %17.17e %17.17e    %17.17e %17.17e %17.17e\n",
                        g_global->domain_origin.x, g_global->domain_origin.y,
                        g_global->domain_origin.z, g_global->domain_size.x,
                        g_global->domain_size.y, g_global->domain_size.z);
                break;
            case PA_PERIOD:
                fprintf(stderr, "X:%d Y:%d\n", g_global->period[0],
                        g_global->period[1]);
                break;
            case PA_WAVELENGTHS:
                if (PorNFreqs() <= 0) {
                    Error(E_ERROR, POR_AT, "no radiation wavelengths set");
                } else {
                    const double *freq  = PorFreqs();
                    int           nfreq = PorNFreqs(), i;
                    for (i = nfreq - 1; i >= 0; i--) {
                        fprintf(stderr, "%d\t%10.10e\n", i,
                                C_C / freq[i] * 1.0e8);
                    }
                    fflush(stderr);
                }
                SKIPLINE;
                break;
            case PA_QUAD:
                if (g_global->dirs.n_azim_oct != -1) {
                    Error(E_ERROR, POR_AT, "no quadrature set");
                }
                fprintf(stderr, "%s\n", quad_sets[g_global->dirs.n_incl_oct]);
                break;
            default:
                Error(E_WARNING, POR_AT, "unknown parameter");
                SKIPLINE;
                break;
            };
            break;

            /*****************************************/
        case CC_VERSION:
            /* print version of Porta */
            fprintf(stderr, "%s %d.%d.%d %s, Copyright (C) %s\n", POR_LABEL,
                    POR_VER, POR_SUBVER, POR_PATCH, POR_EXTRAVER, POR_AUTHOR);
            break;
        case CC_EXIT:
            pro_SignalToAllSlaves(SIG_EXIT);
            ExitPorta(0);
            /* Free-up communicators */
            MPI_Comm_free(&zband_comm);
            MPI_Comm_free(&freqband_comm);

#ifdef THREAD_IO
            end_io_comm(&io_comm);
#endif

            MPI_Finalize(); /* this is to be called by each process */
            exit(EXIT_SUCCESS);
            break;

        case CC_FSSURFACE_LC_H5: /* fall through */
        case CC_FSSURFACE_LC:
            if (!PorMeshInitialized()) {
                Error(E_ERROR, POR_AT, "uninitialized grid");
            } else if (!mod_IsModuleLoaded()) {
                Error(E_ERROR, POR_AT, "no module loaded");
            } else {
                /* theta, chi [in degrees], min & max index of frequencies,
                 * filename */
                if (!ReadDouble(&d1) || !ReadDouble(&d2) || !ReadInt(&i1) ||
                    !ReadInt(&i2) || !ReadString(tmps)) {
                    Error(E_ERROR, POR_AT,
                          "invalid input; use \"%s inclination(deg)   "
                          "azimuth(deg)   "
                          "id_wl_min  id_wl_max   filename\"",
                          cCommand[cmd]);
                } else {
                    char cmt[IO_PSP_COMMENT_LEN + 1] = "";

                    if (d1 < 1.0e-6)
                        d1 = 1.0e-6; /* TODO: this trick is used in order to
                                              avoid exactly vertical rays,
                                              which can give problems for
                                              rotations */

                    if (i1 < 0 || i2 >= PorNFreqs() || i2 < i1) {
                        Error(E_ERROR, POR_AT, "invalid wavelength interval");
                        goto L_FSSURFACE_LC_END;
                    }

                    /* Can we get rid of this? */
                    if ((g_global->period[0] || g_global->period[1]) &&
                        fabs(90.0 - d1) < 3.0) {
                        Error(E_WARNING, POR_AT,
                              "too inclined rays in the periodic medium lead "
                              "to very long "
                              "solution times (inclination=%e deg)",
                              d1);
                    }

                    stk_Add(1, POR_AT,
                            "calculating formal solution (long "
                            "characteristics) at the "
                            "model surface...");

                    if (!pro_SignalToAllSlaves(SIG_LC_FSRAY))
                        IERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&d2, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i1, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i2, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (!(*too_SurfaceMap_fp)(d1, d2, i1, i2, tmps, cmt)) {
                        Error(E_ERROR, POR_AT,
                              "formal solution was unsuccessful");
                    }
                }
            }
        L_FSSURFACE_LC_END:
            break;

        case CC_FSSURFACE_H5: /*fall through*/
        case CC_FSSURFACE:
            if (!PorMeshInitialized()) {
                Error(E_ERROR, POR_AT, "uninitialized grid");
            } else if (!mod_IsModuleLoaded()) {
                Error(E_ERROR, POR_AT, "no module loaded");
            } else {
                /* theta, chi [in degrees], min & max index of frequencies,
                 * filename */
                if (!ReadDouble(&d1) || !ReadDouble(&d2) || !ReadInt(&i1) ||
                    !ReadInt(&i2) || !ReadString(tmps)) {
                    Error(E_ERROR, POR_AT,
                          "invalid input; use \"%s inclination(deg)   "
                          "azimuth(deg)   "
                          "id_wl_min  id_wl_max   filename\"",
                          cCommand[cmd]);
                } else {
                    char cmt[IO_PSP_COMMENT_LEN + 1] = "";

                    if (d1 < 1.0e-6)
                        d1 = 1.0e-6; /* TODO: this trick is used in order to
                                            avoid exactly vertical rays,
                                            which can give problems for
                                            rotations */

                    if (i1 < 0 || i2 >= PorNFreqs() || i2 < i1) {
                        Error(E_ERROR, POR_AT, "invalid wavelength interval");
                        goto L_FSSURFACE_END;
                    }
                    if ((g_global->period[0] || g_global->period[1]) &&
                        fabs(90.0 - d1) < 3.0) {
                        Error(E_WARNING, POR_AT,
                              "too inclined rays in the periodic medium lead "
                              "to very long "
                              "solution times (inclination=%e deg)",
                              d1);
                    }
                    stk_Add(
                        1, POR_AT,
                        "calculating formal solution at the model surface...");

                    if (!pro_SignalToAllSlaves(SIG_FSRAY))
                        IERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&d2, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i1, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i2, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (!(*too_SurfaceMap_fp)(d1, d2, i1, i2, tmps, cmt)) {
                        Error(E_ERROR, POR_AT,
                              "formal solution was unsuccessful");
                    }
                }
            }
        L_FSSURFACE_END:
            break;

            /**************************************************/

        case CC_FSCLVFREQ:
            if (!PorMeshInitialized()) {
                Error(E_ERROR, POR_AT, "uninitialized grid");
            } else if (!mod_IsModuleLoaded()) {
                Error(E_ERROR, POR_AT, "no module loaded");
            } else if (!g_global->period[0] || !g_global->period[1]) {
                Error(E_ERROR, POR_AT,
                      "this function can only be used in horizontally periodic "
                      "models");
            } else {
                /* azimuth-chi [in degrees], mu_min, mu_max, N_directions,
                 * frequency_index, filename */
                if (!ReadDouble(&d1)      /* chi */
                    || !ReadDouble(&d2)   /* mu_min */
                    || !ReadDouble(&d3)   /* mu_max */
                    || !ReadInt(&i1)      /* N_directions */
                    || !ReadInt(&i2)      /* frequency index */
                    || !ReadString(tmps)) /* file name */
                {
                    Error(E_ERROR, POR_AT,
                          "invalid input; use \"%s azimuth(deg)  mu_min  "
                          "mu_max  N_mu  "
                          "id_wl  filename\"",
                          cCommand[CC_FSCLVFREQ]);
                } else {
                    if (i2 < 0 || i2 >= PorNFreqs()) {
                        Error(E_ERROR, POR_AT, "invalid wavelength index");
                        goto L_FSCLV_END;
                    }
                    if (d2 <= 0.0 || d3 > 1.0 || d2 >= d3) {
                        Error(E_ERROR, POR_AT, "invalid inclination interval");
                        goto L_FSCLV_END;
                    }
                    if (i1 < 1) {
                        Error(E_ERROR, POR_AT,
                              "invalid number of CLV directions");
                        goto L_FSCLV_END;
                    }
                    if ((g_global->period[0] || g_global->period[1]) &&
                        fabs(90.0 - acos(d2) / M_PI * 180.0) < 3.0) {
                        Error(E_WARNING, POR_AT,
                              "too inclined rays in the periodic medium lead "
                              "to very long "
                              "solution times (inclination=%e deg)",
                              acos(d2) / M_PI * 180.0);
                    }
                    stk_Add(
                        1, POR_AT,
                        "calculating CLV of the spatially averaged signal...");

                    /* TODO: this trick is used in order to avoid exactly
                     * vertical rays:
                     */
                    if (d3 > 0.999999) {
                        d3 = 0.999999;
                        if (d2 >= d3)
                            d2 = d3 * 0.999999;
                    }

                    if (!pro_SignalToAllSlaves(SIG_FSCLV))
                        IERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&d2, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&d3, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i1, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i2, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (!too_CLVFreq(d1, d2, d3, i1, i2, tmps)) {
                        Error(E_ERROR, POR_AT,
                              "formal solution was unsuccessful");
                    }
                }
            }

        L_FSCLV_END:
            break;

            /**************************************************/

        case CC_FSCLVFREQAV_LC: /*fall through*/
        case CC_FSCLVFREQAV:
            if (!PorMeshInitialized()) {
                Error(E_ERROR, POR_AT, "uninitialized grid");
            } else if (!mod_IsModuleLoaded()) {
                Error(E_ERROR, POR_AT, "no module loaded");
            } else if (!g_global->period[0] || !g_global->period[1]) {
                Error(E_ERROR, POR_AT,
                      "this function can only be used in horizontally periodic "
                      "models");
            } else {
                /* mu_min, mu_max, N_directions, N_azimuths-to-average-over,
                 * frequency_index, filename */
                if (!ReadDouble(&d1)      /* mu_min */
                    || !ReadDouble(&d2)   /* mu_max */
                    || !ReadInt(&i1)      /* N_directions */
                    || !ReadInt(&i2)      /* N_azimuths-to-average-over */
                    || !ReadInt(&i3)      /* frequency index */
                    || !ReadString(tmps)) /* file name */
                {
                    Error(E_ERROR, POR_AT,
                          "invalid input; use \"%s mu_min  mu_max  N_mu "
                          "N_azims id_wl  "
                          "filename\"",
                          cCommand[cmd]);
                } else {
                    if (i3 < 0 || i3 >= PorNFreqs()) {
                        Error(E_ERROR, POR_AT, "invalid wavelength index");
                        goto L_FSCLVAV_END;
                    }
                    if (d1 <= 0.0 || d2 > 1.0 || d1 >= d2) {
                        Error(E_ERROR, POR_AT, "invalid inclination interval");
                        goto L_FSCLVAV_END;
                    }
                    if (i1 < 1) {
                        Error(E_ERROR, POR_AT,
                              "invalid number of CLV directions");
                        goto L_FSCLVAV_END;
                    }
                    if (i2 < 1) {
                        Error(E_ERROR, POR_AT,
                              "number of azimuths should be significantly "
                              "larger than 1");
                        goto L_FSCLVAV_END;
                    }
                    if ((g_global->period[0] || g_global->period[1]) &&
                        fabs(90.0 - acos(d1) / M_PI * 180.0) < 3.0) {
                        Error(E_WARNING, POR_AT,
                              "too inclined rays in the periodic medium lead "
                              "to very long "
                              "solution times (inclination=%e deg)",
                              acos(d1) / M_PI * 180.0);
                    }
                    if (cmd == CC_FSCLVFREQAV) {
                        stk_Add(1, POR_AT,
                                "calculating CLV of the spatially- and "
                                "azimuth-averaged "
                                "signal...");
                    } else if (cmd == CC_FSCLVFREQAV_LC) {
                        stk_Add(1, POR_AT,
                                "calculating CLV (long characteristics) of the "
                                "spatially- "
                                "and azimuth-averaged signal...");
                    }

                    /* TODO: this trick is used in order to avoid exactly
                     * vertical rays:
                     */
                    if (d2 > 0.999999) {
                        d2 = 0.999999;
                        if (d1 >= d2)
                            d1 = d2 * 0.999999;
                    }

                    if (cmd == CC_FSCLVFREQAV) {
                        if (!pro_SignalToAllSlaves(SIG_FSCLVAV))
                            IERR;
                    } else if (cmd == CC_FSCLVFREQAV_LC) {
                        if (!pro_SignalToAllSlaves(SIG_FSCLVAV_LC))
                            IERR;
                    }
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&d2, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i1, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i2, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i3, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (!too_CLVFreqAV(d1, d2, i1, i2, i3, tmps)) {
                        Error(E_ERROR, POR_AT,
                              "formal solution was unsuccessful");
                    }
                }
            }
        L_FSCLVAV_END:
            break;

            /**************************************************/

        case CC_FSAZAVER:
            if (!PorMeshInitialized()) {
                Error(E_ERROR, POR_AT, "uninitialized grid");
            } else if (!mod_IsModuleLoaded()) {
                Error(E_ERROR, POR_AT, "no module loaded");
            } else if (!g_global->period[0] || !g_global->period[1]) {
                Error(E_ERROR, POR_AT,
                      "this function can only be used in horizontally periodic "
                      "models");
            } else {
                /* inclination[deg], N_azimuths_to_aver, min_ifreq, max_ifreq,
                 * filename
                 */
                if (!ReadDouble(&d1)      /* LOS inclination [degrees] */
                    || !ReadInt(&i1)      /* N_directions */
                    || !ReadInt(&i2)      /* ifreq_min */
                    || !ReadInt(&i3)      /* ifreq_max */
                    || !ReadString(tmps)) /* file name */
                {
                    Error(E_ERROR, POR_AT,
                          "invalid input; use \"%s inclination[deg]  N_azims  "
                          "ifreq_min  "
                          "ifreq_max  filename\"",
                          cCommand[CC_FSAZAVER]);
                } else {
                    if (i2 < 0 || i3 >= PorNFreqs() || i2 > i3) {
                        Error(E_ERROR, POR_AT, "invalid wavelength indices");
                        goto L_FSAZAV_END;
                    }
                    if (d1 < 0.0 || d1 >= 90.0) {
                        Error(E_ERROR, POR_AT, "invalid inclination");
                        goto L_FSAZAV_END;
                    }
                    if (i1 < 1) {
                        Error(E_ERROR, POR_AT,
                              "number of azimuths should be significantly "
                              "larger than 1");
                        goto L_FSAZAV_END;
                    }
                    if ((g_global->period[0] || g_global->period[1]) &&
                        fabs(90.0 - d1) < 3.0) {
                        Error(E_WARNING, POR_AT,
                              "too inclined rays in the periodic medium lead "
                              "to very long "
                              "solution times (inclination=%e deg)",
                              d1);
                    }
                    stk_Add(
                        1, POR_AT,
                        "calculating formal solution for the spatially- and "
                        "azimuth-averaged signal...");

                    if (d1 < 1.0e-6)
                        d1 = 1.0e-6; /* TODO: this trick is used in order to
                                        avoid exactly vertical rays: */

                    if (!pro_SignalToAllSlaves(SIG_FSAZAV))
                        IERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i1, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i2, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i3, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (!too_FSAZAV(d1, i1, i2, i3, tmps)) {
                        Error(E_ERROR, POR_AT,
                              "formal solution was unsuccessful");
                    }
                }
            }
        L_FSAZAV_END:
            break;

            /**************************************************/

        case CC_GETTAU_H5: /*fall through*/
        case CC_GETTAU:
            if (!PorMeshInitialized()) {
                Error(E_ERROR, POR_AT, "unitialized grid");
            } else if (!mod_IsModuleLoaded()) {
                Error(E_ERROR, POR_AT, "no module loaded");
            } else {
                /* tau, theta, chi [in degrees], ifreq_min, ifreq_max, filename
                 */
                if (!ReadDouble(&d1) || !ReadDouble(&d2) || !ReadDouble(&d3) ||
                    !ReadInt(&i1) || !ReadInt(&i2) || !ReadString(tmps)) {
                    Error(E_ERROR, POR_AT,
                          "invalid input; use \"%s tau inclination(deg) "
                          "azimuth(deg) "
                          "id_wl_min id_wl_max filename\"",
                          cCommand[cmd]);
                } else {
                    char cmt[IO_TAU_COMMENT_LEN + 1] = "";
                    // TODO: this trick is used in order to avoid exactly
                    // vertical rays
                    if (d2 < 1.0e-6)
                        d2 = 1.0e-6;

                    /* // TODO: this trick is used in order to avoid very small
                     * taus */
                    /* if (d1<1.0e-6) d1 = 1.0e-6; */

                    if (i1 < 0 || i2 >= PorNFreqs() || i2 < i1) {
                        Error(E_ERROR, POR_AT, "invalid wavelength interval");
                        goto L_GETTAU_END;
                    }
                    if ((g_global->period[0] || g_global->period[1]) &&
                        fabs(90.0 - d2) < 3.0) {
                        Error(E_WARNING, POR_AT,
                              "too inclined rays in the periodic medium lead "
                              "to very long "
                              "solution times (inclination=%e deg)",
                              d2);
                    }
                    stk_Add(1, POR_AT, "calculating positions where tau=%e...",
                            d1);

                    if (!pro_SignalToAllSlaves(SIG_GETTAU))
                        IERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&d2, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&d3, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i1, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (MPI_SUCCESS !=
                        MPI_Bcast(&i2, 1, MPI_INT, 0, MPI_COMM_WORLD))
                        CERR;
                    if (!(*too_SurfaceTau_fp)(d1, d2, d3, i1, i2, tmps, cmt)) {
                        Error(E_ERROR, POR_AT, "get tau was unsuccessful");
                    }
                }
            }
        L_GETTAU_END:
            break;

        case CC_SOLVE_JACOBI:
            if (!PorMeshInitialized()) {
                Error(E_ERROR, POR_AT, "uninitialized grid");
            } else if (!mod_IsModuleLoaded()) {
                Error(E_ERROR, POR_AT, "no module loaded");
            } else if (!ReadInt(&tmp) || !ReadDouble(&d1)) {
                Error(E_ERROR, POR_AT,
                      "use \"%s max_iterations max_relative_change\"",
                      cCommand[CC_SOLVE_JACOBI]);
                SKIPLINE;
                continue;
            } else if (!pro_SignalToAllSlaves(SIG_JACOBI)) {
                CERR;
                break;
            }
            /* send info on # of iterations and ,aximum relative change */
            MPI_Bcast(&tmp, 1, MPI_INT, 0, MPI_COMM_WORLD);
            MPI_Bcast(&d1, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
            /* --== here the non-LTE problem is solved by the slaves! ==-- */
            if (MPI_SUCCESS !=
                MPI_Recv(&d1, 1, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, &status))
                CERR; /* get max. relative change from process #1 */
            PrintfErr("Jacobi iteration finished. Maximum relative change of "
                      "population: %6.6e.\n",
                      d1);
            SKIPLINE;
            break;

        case CC_SAVEMODEL_H5: /*fall through*/
        case CC_SAVEMODEL:
            if (!ReadString(tmps)) {
                Error(E_ERROR, POR_AT, "invalid file name");
            } else if (!PorMeshInitialized() || !mod_IsModuleLoaded()) {
                Error(
                    E_ERROR, POR_AT,
                    "the grid mesh/atomic module is not properly initialized");
            } else {
                if (!pro_SignalToAllSlaves(signal)) {
                    CERR;
                }
                if (!(*io_SaveGridMaster_fp)(tmps)) {
                    Error(E_ERROR, POR_AT, "the grid could not be saved");
                }
            }
            break;

        case CC_LOADMODEL_H5: /*fall through*/
        case CC_LOADMODEL:
            if (!ReadString(tmps)) {
                Error(E_ERROR, POR_AT, "invalid file name");
            }
            if (!pro_SignalToAllSlaves(signal)) {
                CERR;
                break;
            }
            if (!(*io_LoadGridMaster_fp)(tmps)) {
                Error(E_ERROR, POR_AT, "the grid could not be loaded from %s",
                      tmps);
            } else {
                stk_Add(1, "the grid has been loaded from %s", tmps);
            }
            break;

        default:
            IERR;
            break;
        }
    } while (1);
}
