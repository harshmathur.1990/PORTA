#!/usr/bin/env python
# -*- coding: utf-8 -*-

#===============================================================================
# Python3 code to read PORTA PMD files (in binary and HDF5 format)
#-------------------------------------------------------------------------------
#
# Defines functions:
#     dump_pmd
#
# Ángel de Vicente (angel.de.vicente@iac.es)
#
# Got help from
# https://www.devdungeon.com/content/working-binary-data-python
#===============================================================================

import argparse
import numpy as np
import struct

def dump_pmd_h5(f):
    import h5py
    
    ######################
    #READING PORTA HEADER#
    ######################
    
    print(f"Magic String: {f.attrs['PMD_MAGIC'].decode('utf-8')}")
    creation = f.attrs['CREATION_DATE']
    print(f"Creation date: {creation[2]}/{creation[1]+1}/{creation[0]+1900} {creation[3]}:{creation[4]}:{creation[5]}")
    
    [periodx,periody] = f.attrs['PERIODICITY']
    print(f"Periodicity: X: {periodx} Y: {periody}")

    [s_X,s_Y,s_Z] = f.attrs['DOMAIN_SIZE']
    print(f"Domain size: X: {s_X} Y: {s_Y} Z: {s_Z}")

    (o_X,o_Y,o_Z) = f.attrs['DOMAIN_ORIGIN']
    print(f"Domain origin: X: {o_X} Y: {o_Y} Z: {o_Z}")

    (g_NX,g_NY,g_NZ) = f.attrs['GRID_DIMENSIONS']
    print(f"Number of nodes: X: {g_NX} Y: {g_NY} Z: {g_NZ}")


    x_array = f.attrs['X_AXIS']
    print(f"X array: \n{x_array}\n")

    y_array = f.attrs['Y_AXIS']
    print(f"Y array: \n{y_array}\n")

    z_array = f.attrs['Z_AXIS']
    print(f"Z array: \n{z_array}\n")


    inc_a,azi_a = f.attrs['POLAR_NODES'][0],f.attrs['AZIMUTH_NODES'][0]
    print(f"Number of angles: Inclination: {inc_a} Azimuthal: {azi_a}")

    print(f"Name of Atomic Module: {f.attrs['MODULE_NAME'].decode('utf-8')}")
    print(f"Model Comment: {f.attrs['MODULE_COMMENT'].decode('utf-8')}")

    #############################
    #READING ATOMIC MODEL HEADER#
    #############################

    mod_v = f['Module'].attrs['MOD_VERSION'][0]
    freq = f['Module'].attrs['FREQUENCY'][0]
    
    print(f"""

#############################
ATOMIC MODEL HEADER
#############################

MOD_VERSION: {mod_v}
FREQUENCY: {freq}""")

    temp_bc = np.asarray(f['Module']['temp_bc'])
    print(f"BC Temp:: \n{temp_bc}\n")


    ####################
    #READING DATA NODES#
    ####################
    NJKQ = 9

    sizefloat = 4
    d_f = 'f8'
    d_fl = 'f4'

    node_data = np.zeros((g_NZ,g_NY,g_NX), dtype=[('nh',d_fl),('ne',d_fl),('temp',d_fl),('jkq',d_f,NJKQ)])

    print(f"""

#############################
NODES DATA
#############################
    """)

    
    for ii in range(g_NZ):
        for jj in range(g_NY):
            for kk in range(g_NX):
                node_data[ii,jj,kk]['nh'] = f['Module/g_data']['Nh'][ii,jj,kk]
                node_data[ii,jj,kk]['ne'] = f['Module/g_data']['Ne'][ii,jj,kk]
                node_data[ii,jj,kk]['temp'] = f['Module/g_data']['T'][ii,jj,kk]
                node_data[ii,jj,kk]['jkq'] = f['Module/g_data']['jkq'][ii,jj,kk]

                print(f"""
NODE {ii},{jj},{kk}:
--------------------
nh:      {node_data[ii,jj,kk]['nh']}
ne:      {node_data[ii,jj,kk]['ne']}
temp:    {node_data[ii,jj,kk]['temp']}
jkq:     {node_data[ii,jj,kk]['jkq']}""")

    f.close()

    
def dump_pmd(filename):
    file = open(filename, 'rb')

    ######################
    #READING PORTA HEADER#
    ######################
    
    m_s = file.read(8).decode('utf-8') ; print("Magic String: ",m_s)
    endian = int.from_bytes(file.read(1),byteorder='little') ; print("Endianness: ", endian)
    if endian == 0:
        endian_s = "little"
        e_str    = '<'
    else:
        endian_s = "big"
        e_str    = '>'
        
    sizeint = int.from_bytes(file.read(1),byteorder=endian_s)      ; print("Size of integer: ", sizeint)
    sizedble = int.from_bytes(file.read(1),byteorder=endian_s)     ; print("Size of double: ", sizedble)
    vpmd = int.from_bytes(file.read(sizeint),byteorder=endian_s)   ; print("Version of PMD file: ", vpmd)

    creation = struct.unpack(e_str+'iiiiii', file.read(sizeint*6))
    print(f"Creation date: {creation[2]}/{creation[1]+1}/{creation[0]+1900} {creation[3]}:{creation[4]}:{creation[5]}")
    
    period = struct.unpack(e_str+'ss', file.read(2)) ;
    periodx = int.from_bytes(period[0],byteorder=endian_s)
    periody = int.from_bytes(period[1],byteorder=endian_s)
    print(f"Periodicity: X: {periodx} Y: {periody}")

    (s_X,s_Y,s_Z) = struct.unpack(e_str+'ddd', file.read(sizedble*3))
    print(f"Domain size: X: {s_X} Y: {s_Y} Z: {s_Z}")

    (o_X,o_Y,o_Z) = struct.unpack(e_str+'ddd', file.read(sizedble*3))
    print(f"Domain origin: X: {o_X} Y: {o_Y} Z: {o_Z}")

    (g_NX,g_NY,g_NZ) = struct.unpack(e_str+'iii', file.read(sizeint*3))
    print(f"Number of nodes: X: {g_NX} Y: {g_NY} Z: {g_NZ}")


    MAXDIM = 8192
    x_array = np.asarray(struct.unpack(e_str+'d'*g_NX, file.read(sizedble*g_NX)))
    print(f"X array: \n{x_array}\n")
    _ = file.read((MAXDIM-g_NX)*sizedble)

    y_array = np.asarray(struct.unpack(e_str+'d'*g_NY, file.read(sizedble*g_NY)))
    print(f"Y array: \n{y_array}\n")
    _ = file.read((MAXDIM-g_NY)*sizedble)

    z_array = np.asarray(struct.unpack(e_str+'d'*g_NZ, file.read(sizedble*g_NZ)))
    print(f"Z array: \n{z_array}\n")
    _ = file.read((MAXDIM-g_NZ)*sizedble)

    (inc_a,azi_a) = struct.unpack(e_str+'ii', file.read(sizeint*2))
    print(f"Number of angles: Inclination: {inc_a} Azimuthal: {azi_a}")

    name_module = file.read(1023).decode('utf-8') 
    name = name_module.split('\0',1)[0]
    print(f"Name of Atomic Module: {name}")

    comment_model = file.read(4096).decode('utf-8') 
    comment = comment_model.split('\0',1)[0]
    print(f"Model Comment: {comment}")

    (size_PMD_header,size_node) = struct.unpack(e_str+'ii', file.read(sizeint*2))
    print(f"Size of the module header in bytes: {size_PMD_header}")
    print(f"Size of the node data in bytes: {size_node}")

    #############################
    #READING ATOMIC MODEL HEADER#
    #############################

    (mod_v,freq) = struct.unpack(e_str+'id', file.read(sizeint+sizedble))

    print(f"""

#############################
ATOMIC MODEL HEADER
#############################

MOD_VERSION: {mod_v}
FREQUENCY: {freq}""")

    temp_bc = np.zeros((g_NY,g_NX), dtype=np.float)
    for ii in range(g_NY):
        for jj in range(g_NX):
            temp_bc[ii, jj] = struct.unpack('<d', file.read(sizedble))[0]
    print(f"BC Temp:: \n{temp_bc}\n")


    ####################
    #READING DATA NODES#
    ####################
    NJKQ = 9

    sizefloat = 4
    d_f = 'f'+str(sizedble)
    d_fl = 'f4'

    node_data = np.zeros((g_NZ,g_NY,g_NX), dtype=[('nh',d_fl),('ne',d_fl),('temp',d_fl),('jkq',d_f,NJKQ)])

    print(f"""

#############################
NODES DATA
#############################
    """)

    
    for ii in range(g_NZ):
        for jj in range(g_NY):
            for kk in range(g_NX):
                node_data[ii,jj,kk]['nh'] = struct.unpack('<f', file.read(sizefloat))[0]
                node_data[ii,jj,kk]['ne'] = struct.unpack('<f', file.read(sizefloat))[0]
                node_data[ii,jj,kk]['temp'] = struct.unpack('<f', file.read(sizefloat))[0]

                for jkqi in range(NJKQ):
                    node_data[ii,jj,kk]['jkq'][jkqi] = struct.unpack('<d', file.read(sizedble))[0]

                    
                print(f"""
NODE {ii},{jj},{kk}:
--------------------
nh:      {node_data[ii,jj,kk]['nh']}
ne:      {node_data[ii,jj,kk]['ne']}
temp:    {node_data[ii,jj,kk]['temp']}
jkq:     {node_data[ii,jj,kk]['jkq']}""")
            
    file.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="PORTA pmd  file")
    args = parser.parse_args()

    try:
        import h5py
        f = h5py.File(args.file,'r')
        dump_pmd_h5(f)
    except:
        # I can't open it as HDF5 file. Assume is a binary PMD file
        dump_pmd(args.file)

if __name__ == "__main__":
    main()

    
    

