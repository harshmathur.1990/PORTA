#!/usr/bin/env python
# -*- coding: utf-8 -*-

#####################
# twolevel-pmd.py
#
# Tanausú del Pino Alemán
# Ángel de Vicente
#
# Writes a simple pmd file to test the twolevel module
#
#####################
#
# 02/11/2020 - Modified option names and added options for
#              temperature perturbation as a fraction of
#              model temperature (AdV)
#
# 02/04/2020 - Generalized the code to include perturbations in the
#              temperature (TdPA)
#
# 01/13/2020 - Added the extention modifier to the filename check
#              in main (TdPA)
#
# 12/04/2019 - Added routine to create the file in HDF5 format,
#              so if the package h5py is found both binary and
#              HDF5 versions are created (AdV)
#
# 11/28/2019 - Copied multilevel-pmd.py from 11/26/2019 as starting
#              point and implemented Sr I 460.7nm (TdPA)
#
#####################

import sys, math, os, struct, copy
import numpy as np
import datetime

######################################################################

def get_atom(atomname):
    ''' Configures the atomic model
    '''

    atom = {'check': False}

    if atomname == 'Sr':

        # Atomic mass in AMU
        atom['mass'] = 87.62
        # Number of transitions (radiative)
        atom['NR'] = 2
        # Energy of atomic levels in cm^-1
        atom['E'] = [0.0,21698.452]
        # Land\'e factor of atomic levels
        atom['gL'] = [0.0,1.0]
        # Total angular momentum of atomic levels multiplied by two
        atom['J2'] = [0,2]
        # Einstein spontaneous emission coefficient for each radiative
        # transition
        atom['Aul'] = 2.01e8
        # Reference temperature that determines the Doppler width to
        # build the frequency axis
        atom['Tref'] = 5e3
        # Success flag
        atom['check'] = True

    return atom

######################################################################

def get_atmo(atmoname, atomname):
    ''' Configures the atmospheric model
    '''

    # Initialize control flags
    atmo = {'check_atmo': False, \
            'check_atom': False}

    # Get thermal part
    if atmoname == 'FALC':

        # Number of nodes
        atmo['nz'] = 70
        # Heights in km
        atmo['z'] = [2219.430,2217.880,2216.430,2215.080,2212.920, \
                     2210.750,2209.650,2208.530,2207.400,2206.270, \
                     2205.700,2205.180,2204.660,2204.130,2203.630, \
                     2203.160,2202.700,2202.220,2201.830,2201.560, \
                     2201.160,2200.840,2200.100,2199.000,2190.000, \
                     2168.000,2140.000,2110.000,2087.000,2075.000, \
                     2062.000,2043.000,2017.000,1980.000,1915.000, \
                     1860.000,1775.000,1670.000,1580.000,1475.000, \
                     1378.000,1278.000,1180.000,1065.000,980.000, \
                     905.000,855.000,805.000,755.000,705.000, \
                     650.000,600.000,560.000,525.000,490.000, \
                     450.000,400.000,350.000,300.000,250.000, \
                     200.000,150.000,100.000,50.000,0.000,-20.000, \
                     -40.000,-60.000,-80.000,-100.000]
        # Temperature in K
        atmo['T'] = [102770.00,98790.00,94800.00,90816.00,83891.00, \
                     75934.00,71336.00,66145.00,60170.00,53284.00, \
                     49385.00,45416.00,41178.00,36594.00,32145.00, \
                     27972.00,24056.00,20416.00,17925.00,16500.00, \
                     15000.00,14250.00,13500.00,13000.00,12000.00, \
                     11150.00,10550.00,9900.00,9450.00,9200.00, \
                     8950.00,8700.00,8400.00,8050.00,7650.00, \
                     7450.00,7250.00,7050.00,6900.00,6720.00, \
                     6560.00,6390.00,6230.00,6040.00,5900.00, \
                     5755.00,5650.00,5490.00,5280.00,5030.00, \
                     4750.00,4550.00,4430.00,4400.00,4410.00, \
                     4460.00,4560.00,4660.00,4770.00,4880.00, \
                     4990.00,5150.00,5410.00,5790.00,6520.00, \
                     6980.00,7590.00,8220.00,8860.00,9400.00]
        # Electron density in cm^-3
        atmo['Ne'] = [6.560E+09,6.810E+09,7.070E+09,7.360E+09, \
                      7.910E+09,8.660E+09,9.170E+09,9.820E+09, \
                      1.070E+10,1.190E+10,1.280E+10,1.380E+10, \
                      1.510E+10,1.670E+10,1.880E+10,2.120E+10, \
                      2.410E+10,2.770E+10,3.080E+10,3.290E+10, \
                      3.530E+10,3.650E+10,3.730E+10,3.720E+10, \
                      3.560E+10,3.810E+10,4.060E+10,4.240E+10, \
                      4.310E+10,4.340E+10,4.360E+10,4.370E+10, \
                      4.390E+10,4.420E+10,4.540E+10,4.700E+10, \
                      5.080E+10,5.740E+10,6.320E+10,7.040E+10, \
                      7.860E+10,8.760E+10,9.800E+10,1.100E+11, \
                      1.180E+11,1.230E+11,1.240E+11,1.080E+11, \
                      9.410E+10,8.280E+10,9.030E+10,1.260E+11, \
                      1.770E+11,2.410E+11,3.280E+11,4.660E+11, \
                      7.240E+11,1.120E+12,1.710E+12,2.600E+12, \
                      3.930E+12,6.040E+12,9.890E+12,1.970E+13, \
                      7.680E+13,1.730E+14,4.480E+14,1.050E+15, \
                      2.210E+15,3.870E+15]
        # Microturbulence in km/s
        atmo['vmi'] = [11.900,11.800,11.700,11.600,11.500, \
                       11.300,11.200,11.000,10.800,10.600, \
                       10.500,10.300,10.100,9.930,9.700,9.470, \
                       9.220,8.950,8.750,8.620,8.470,8.380,8.280, \
                       8.190,7.960,7.780,7.600,7.380,7.220,7.130, \
                       7.040,6.920,6.760,6.550,6.220,5.970,5.610, \
                       5.210,4.880,4.520,4.200,3.860,3.500,3.030, \
                       2.640,2.300,2.060,1.820,1.570,1.330,1.070, \
                       0.865,0.725,0.628,0.555,0.501,0.477,0.501, \
                       0.627,0.867,1.170,1.480,1.750,1.970,2.000, \
                       2.000,2.000,2.000,2.000,2.000]
        # Reverse z axis and convert to cm
        atmo['z'] = atmo['z'][::-1]
        atmo['vmi'] = atmo['vmi'][::-1]
        for i in range(len(atmo['z'])):
            atmo['z'][i] = atmo['z'][i]*1e5
            atmo['vmi'][i] = atmo['vmi'][i]*1e5
        atmo['T'] = atmo['T'][::-1]
        atmo['Ne'] = atmo['Ne'][::-1]
        atmo['check_atmo'] = True

        # Atomic part
        if atomname == 'Sr':

            atmo['a_voigt'] = [5.80755658771e-04,5.86471720831e-04, \
                               5.92325336610e-04,5.98316136977e-04, \
                               6.05589117249e-04,6.18388098641e-04, \
                               6.25296481345e-04,6.37870267423e-04, \
                               6.51409269790e-04,6.66101100107e-04, \
                               6.73989074575e-04,6.88271858350e-04, \
                               7.03410188332e-04,7.17513674516e-04, \
                               7.36453630816e-04,7.56346728826e-04, \
                               7.78875662841e-04,8.04411285265e-04, \
                               8.24347536503e-04,8.37698262383e-04, \
                               8.53542438662e-04,8.63195231868e-04, \
                               8.74100500382e-04,8.83957227275e-04, \
                               9.09868147507e-04,9.31361434732e-04, \
                               9.53556956646e-04,9.82063495558e-04, \
                               1.00387278635e-03,1.01657431281e-03, \
                               1.02960782801e-03,1.04735834940e-03, \
                               1.07193995040e-03,1.10592403304e-03, \
                               1.16353026931e-03,1.21101284233e-03, \
                               1.28624352301e-03,1.38135761650e-03, \
                               1.47085088459e-03,1.58258254993e-03, \
                               1.69693223189e-03,1.83771284809e-03, \
                               2.01388605902e-03,2.30011540617e-03, \
                               2.60457289269e-03,2.94258332014e-03, \
                               3.23642120924e-03,3.59658594702e-03, \
                               4.06733283039e-03,4.65506210658e-03, \
                               5.51326828284e-03,6.46156607341e-03, \
                               7.36045132549e-03,8.20595254795e-03, \
                               9.14117756387e-03,1.03737422859e-02, \
                               1.23127186071e-02,1.49533275013e-02, \
                               1.80235664601e-02,2.11964951659e-02, \
                               2.46574667158e-02,2.90838386747e-02, \
                               3.50807021937e-02,4.26032186914e-02, \
                               5.27134240681e-02,5.60973467109e-02, \
                               5.88884304733e-02,6.07891178363e-02, \
                               6.17389342896e-02,6.29337600906e-02]
            atmo['Cul'] = [2.23758038147e+03,2.27044184445e+03, \
                           2.28597981159e+03,2.30720928733e+03, \
                           2.35072250365e+03,2.42815423195e+03, \
                           2.49293594226e+03,2.58641208406e+03, \
                           2.73050683100e+03,2.94797595627e+03, \
                           3.12743399017e+03,3.33059777545e+03, \
                           3.60145190153e+03,3.93321892059e+03, \
                           4.36680094230e+03,4.84423886866e+03, \
                           5.44534026569e+03,6.28083310421e+03, \
                           7.05235309833e+03,7.55140323611e+03, \
                           8.06352502437e+03,8.28477386304e+03, \
                           8.38540435102e+03,8.29267532933e+03, \
                           7.76036570941e+03,8.09583826950e+03, \
                           8.43832938451e+03,8.56734876945e+03, \
                           8.52266361652e+03,8.48030127782e+03, \
                           8.42312164268e+03,8.35490154384e+03, \
                           8.30343989836e+03,8.28242754705e+03, \
                           8.46124190718e+03,8.75685599745e+03, \
                           9.47867812037e+03,1.07454514252e+04, \
                           1.18748891769e+04,1.33007378894e+04, \
                           1.49314175434e+04,1.67409473059e+04, \
                           1.88303771177e+04,2.12578005373e+04, \
                           2.28829260957e+04,2.39164450907e+04, \
                           2.41409715305e+04,2.10386816609e+04, \
                           1.82950323428e+04,1.59840500895e+04, \
                           1.71733696150e+04,2.36062157060e+04, \
                           3.28116674157e+04,4.45496294253e+04, \
                           6.06895087860e+04,8.66239855201e+04, \
                           1.35754699781e+05,2.11660361753e+05, \
                           3.25635727404e+05,4.98370459801e+05, \
                           7.57392935029e+05,1.17079022111e+06, \
                           1.92591664470e+06,3.82841267031e+06, \
                           1.46100109252e+07,3.24377828314e+07, \
                           8.34719070312e+07,1.97554497789e+08, \
                           4.25298302356e+08,7.63418252562e+08]
            atmo['pops'] = [[1.38445059326e-07,8.99391064574e-10], \
                            [1.56294254285e-07,1.01388001337e-09], \
                            [1.79487301234e-07,1.16252991673e-09], \
                            [2.07403190034e-07,1.34122329111e-09], \
                            [2.68184196000e-07,1.73091372011e-09], \
                            [3.63183987435e-07,2.33634073834e-09], \
                            [4.33207806957e-07,2.78193745422e-09], \
                            [5.27453999656e-07,3.37654173931e-09], \
                            [6.62146881998e-07,4.22472672198e-09], \
                            [8.63489785807e-07,5.48987769526e-09], \
                            [1.01294168596e-06,6.42824330932e-09], \
                            [1.19884994229e-06,7.58180337738e-09], \
                            [1.46999129780e-06,9.26273172295e-09], \
                            [1.87676286220e-06,1.17853487420e-08], \
                            [2.50575142706e-06,1.56615785080e-08], \
                            [3.45992488180e-06,2.15156179184e-08], \
                            [5.03614118075e-06,3.11326992274e-08], \
                            [7.80582127219e-06,4.79255084163e-08], \
                            [1.12906762790e-05,6.89431839669e-08], \
                            [1.42941176746e-05,8.69605623467e-08], \
                            [1.87599072806e-05,1.13624372690e-07], \
                            [2.17663724050e-05,1.31479162197e-07], \
                            [2.55078516112e-05,1.53607171828e-07], \
                            [2.86615476428e-05,1.72123225461e-07], \
                            [3.70244719561e-05,2.20759072696e-07], \
                            [4.89155906681e-05,2.90002689026e-07], \
                            [6.29706214590e-05,3.71177557307e-07], \
                            [8.42057635818e-05,4.92665095558e-07], \
                            [1.04557422960e-04,6.08263235747e-07], \
                            [1.18174792193e-04,6.85211097713e-07], \
                            [1.34142623180e-04,7.75164336800e-07], \
                            [1.56141499310e-04,8.98172675521e-07], \
                            [1.90717840578e-04,1.09018173293e-06], \
                            [2.49510850949e-04,1.41397093305e-06], \
                            [3.81769987124e-04,2.13231529078e-06], \
                            [5.31427251907e-04,2.93325913216e-06], \
                            [8.82746251619e-04,4.78268141410e-06], \
                            [1.70379194696e-03,9.01919106576e-06], \
                            [3.02184378995e-03,1.56551852424e-05], \
                            [6.09359897636e-03,3.07436959838e-05], \
                            [1.20611861626e-02,5.92584128354e-05], \
                            [2.54315734588e-02,1.21039816312e-04], \
                            [5.52056926719e-02,2.52885381256e-04], \
                            [1.42136586653e-01,6.14240685100e-04], \
                            [2.92680271750e-01,1.19543256999e-03], \
                            [5.61855596197e-01,2.17047527929e-03], \
                            [8.69341094215e-01,3.21757033736e-03], \
                            [1.23531406832e+00,4.36726812387e-03], \
                            [1.83364770072e+00,6.16623621480e-03], \
                            [2.83185016504e+00,9.06669849310e-03], \
                            [5.87299855785e+00,1.78602209515e-02], \
                            [1.48169516039e+01,4.34639627568e-02], \
                            [3.37166137401e+01,9.69798005796e-02], \
                            [6.71686999595e+01,1.91778817577e-01], \
                            [1.29742149065e+02,3.71437463459e-01], \
                            [2.63316786307e+02,7.67534235782e-01], \
                            [6.00788070932e+02,1.84193395982e+00], \
                            [1.31266624454e+03,4.40760485557e+00], \
                            [2.68303956480e+03,1.04208054238e+01], \
                            [5.22299990566e+03,2.47709586900e+01], \
                            [9.49054952684e+03,5.73549033043e+01], \
                            [1.53670228914e+04,1.22055608613e+02], \
                            [2.14940601292e+04,2.30430854079e+02], \
                            [2.76899351724e+04,4.20030103742e+02], \
                            [3.48402828354e+04,8.99840475562e+02], \
                            [3.85827433212e+04,1.34606270955e+03], \
                            [4.20955008619e+04,2.11932788626e+03], \
                            [4.53087165942e+04,3.16918584915e+03], \
                            [4.77553931093e+04,4.41979066193e+03], \
                            [5.12946341037e+04,5.68041131699e+03]]
            atmo['etac'] = [4.36407359513e-15,4.53039319552e-15, \
                            4.70336660234e-15,4.89629939947e-15, \
                            5.26221016003e-15,5.76118826776e-15, \
                            6.10050105770e-15,6.53296967171e-15, \
                            7.11848976556e-15,7.91698538090e-15, \
                            8.51589151747e-15,9.18139419919e-15, \
                            1.00466165672e-14,1.11116492702e-14, \
                            1.25096587376e-14,1.41077117637e-14, \
                            1.60393564610e-14,1.84385907469e-14, \
                            2.05057090241e-14,2.19069193566e-14, \
                            2.35099691816e-14,2.43128653611e-14, \
                            2.48517218848e-14,2.47919093688e-14, \
                            2.37478678343e-14,2.54263857947e-14, \
                            2.71017724380e-14,2.83190486368e-14, \
                            2.88072440790e-14,2.90219151820e-14, \
                            2.91725923054e-14,2.92644486652e-14, \
                            2.94363522879e-14,2.97003229199e-14, \
                            3.06391782417e-14,3.18589560336e-14, \
                            3.47364780044e-14,3.98897413996e-14, \
                            4.49133482829e-14,5.22756308375e-14, \
                            6.24487117656e-14,7.85118661765e-14, \
                            1.06830731868e-13,1.74569228955e-13, \
                            2.79807169831e-13,4.57329439124e-13, \
                            6.54416231784e-13,8.93854147825e-13, \
                            1.30559992848e-12,2.03432146471e-12, \
                            4.12799418520e-12,9.77045215223e-12, \
                            2.09892344630e-11,3.95099988780e-11, \
                            7.23556423754e-11,1.39795291480e-10, \
                            3.08579897755e-10,6.76243157103e-10, \
                            1.44367357866e-09,3.06141568565e-09, \
                            6.38062906616e-09,1.28669993562e-08, \
                            2.53827664682e-08,5.55077818838e-08, \
                            1.92039852375e-07,3.80447655162e-07, \
                            8.27335085368e-07,1.64367305343e-06, \
                            2.97970751971e-06,4.76851400443e-06]
            atmo['epsc'] = [7.53933743798e-22,8.22128409245e-22, \
                            8.98149869861e-22,9.86951147796e-22, \
                            1.16881440808e-21,1.44406236456e-21, \
                            1.64847949362e-21,1.92877940537e-21, \
                            2.34771850231e-21,2.99894782937e-21, \
                            3.50932758448e-21,4.12977527679e-21, \
                            5.01983341240e-21,6.24543466484e-21, \
                            8.00784495878e-21,1.02994527611e-20, \
                            1.37367326745e-20,1.89635059258e-20, \
                            2.37284034437e-20,2.71761978127e-20, \
                            3.14557896592e-20,3.37915593798e-20, \
                            3.55026284154e-20,3.56952466255e-20, \
                            3.31362566710e-20,3.66758184868e-20, \
                            3.92160929855e-20,4.05336433973e-20, \
                            4.11566086364e-20,4.13646502339e-20, \
                            4.14017660203e-20,4.13632952301e-20, \
                            4.14373345052e-20,4.19031443653e-20, \
                            4.52012563360e-20,5.02576233860e-20, \
                            6.29638712334e-20,8.88079709351e-20, \
                            1.20583757141e-19,1.79274908888e-19, \
                            2.75470561766e-19,4.54816873039e-19, \
                            8.00919099301e-19,1.64592950520e-18, \
                            2.89956462419e-18,4.77897388467e-18, \
                            6.61840019608e-18,7.85033491894e-18, \
                            9.32127706856e-18,1.10791345922e-17, \
                            1.70460675922e-17,3.37565965618e-17, \
                            6.44659747419e-17,1.20079567364e-16, \
                            2.29257728931e-16,4.89271081237e-16, \
                            1.28105214242e-15,3.29142357576e-15, \
                            8.26897692657e-15,2.04446581496e-14, \
                            4.92782975456e-14,1.21094962349e-13, \
                            3.20715408828e-13,1.02930627529e-12, \
                            6.61142281685e-12,1.82678450538e-11, \
                            5.91727074335e-11,1.72364812849e-10, \
                            4.56122988748e-10,9.98938583414e-10]
            atmo['delta2'] = [1.22275496606e-11,1.95940831421e-11, \
                              2.67511389423e-11,3.71520050036e-11, \
                              7.46466514545e-11,2.15150070889e-10, \
                              4.56154272664e-10,1.00708553217e-09, \
                              2.33577286977e-09,5.65259904722e-09, \
                              9.15039675802e-09,1.42098208893e-08, \
                              2.20776215374e-08,3.44579806164e-08, \
                              5.21351824935e-08,7.60348335758e-08, \
                              1.08276786528e-07,1.51402571333e-07, \
                              1.95766908235e-07,2.31827258269e-07, \
                              2.91470811149e-07,3.43694134953e-07, \
                              4.68906969049e-07,6.52042882284e-07, \
                              1.33592862797e-06,1.63688460825e-06, \
                              2.01277081022e-06,2.68177893010e-06, \
                              3.34616313548e-06,3.76610144803e-06, \
                              4.24897805514e-06,4.96440394948e-06, \
                              6.03609228092e-06,7.76874837432e-06, \
                              1.14130914277e-05,1.53461224635e-05, \
                              2.38561220807e-05,4.11850791339e-05, \
                              6.63562093599e-05,1.18196575582e-04, \
                              2.04984096528e-04,3.74346438595e-04, \
                              6.97826475262e-04,1.50494980078e-03, \
                              2.73363146084e-03,4.71622440807e-03, \
                              6.86999895105e-03,1.02025825658e-02, \
                              1.55511697294e-02,2.41999083757e-02, \
                              4.06154310065e-02,6.61540654361e-02, \
                              9.85653509600e-02,1.38360526441e-01, \
                              1.92957318039e-01,2.79265045071e-01, \
                              4.37397860137e-01,6.77756310179e-01, \
                              1.03714049383e+00,1.57466566128e+00, \
                              2.35305271247e+00,3.44156157618e+00, \
                              4.87766178381e+00,6.63440362340e+00, \
                              8.35825137429e+00,8.88079054394e+00, \
                              9.29190808277e+00,9.55878877704e+00, \
                              9.67448429779e+00,9.84226174814e+00]

            # Reverse z axis
            atmo['Cul'] = atmo['Cul'][::-1]
            atmo['a_voigt'] = atmo['a_voigt'][::-1]
            atmo['etac'] = atmo['etac'][::-1]
            atmo['epsc'] = atmo['epsc'][::-1]
            atmo['pops'] = atmo['pops'][::-1]
            atmo['delta2'] = atmo['delta2'][::-1]
            atmo['check_atom'] = True

    dz = np.array(atmo['z'])
    dz = np.absolute(dz[1:] - dz[:-1])
    atmo['mindz'] = np.amin(dz)

    return atmo
######################################################################
######################################################################

def main():
    ''' Writes a simple pmd file (binary and HDF5 versions) for the twolevel module
    '''

    #################################
    # Internal function: write_hdf5 #
    #################################
    def write_hdf5(outname):

        try:
            import h5py
        except:
            msg = 'HDF5 package required for HDF5 file creation: skipping'
            sys.exit(msg)
        
        f = h5py.File(outname, 'w')

        # PORTA Head #
        # ------------
        f.attrs['PMD_MAGIC'] = np.string_('portapmd')
        f.attrs['PMD_VERSION'] = np.int32([2])

        now = datetime.datetime.now()
        date = np.int32([int(now.year - 1900.), int(now.month-1.), \
                int(now.day), int(now.hour), int(now.minute), \
                int(now.second)])
        f.attrs['CREATION_DATE'] = date
        f.attrs['PERIODICITY'] = np.int32([1,1])
        f.attrs['DOMAIN_SIZE'] = [Dx,Dy,Dz]
        f.attrs['DOMAIN_ORIGIN'] = [x0,y0,z0]
        f.attrs['GRID_DIMENSIONS'] = np.int32([nx,ny,nz])
        f.attrs['X_AXIS'] = x
        f.attrs['Y_AXIS'] = y
        f.attrs['Z_AXIS'] = z
        f.attrs['POLAR_NODES'] = np.int32([nth])
        f.attrs['AZIMUTH_NODES'] = np.int32([nph])
        f.attrs['MODULE_NAME'] = np.string_(m_name)

        comment = 'Model for ' + atomname + ' atom in ' + \
                  atmoname + ' atmospheric model replicated in ' + \
                  '{0}x{1}'.format(nx,ny) + ' columns for the ' + \
                  m_name + ' module'
        f.attrs['MODULE_COMMENT'] = np.string_(comment)

        # Sizes of header and node
        Head_size = int(60 + nx*ny*8)
        Node_size = int(22*8 + \
                    16*(atom['J2'][0] + 1)*(atom['J2'][0] + 1) + \
                    16*(atom['J2'][1] + 1)*(atom['J2'][1] + 1))

        rdim=0    # Size (in elements) of density matrix
        for ii in range(2):
            for K in range(atom['J2'][ii] + 1):
                for Q in range(K*2+1):
                    rdim += 2

        f.attrs['MODULE_HEADER_SIZE'] = np.int32([Head_size])

        # Not used in PORTA, really
        # f.attrs['Size Node'] = np.int32([Node_size])


        # Module head #
        # -------------
        f.create_group('Module') 

        f['Module'].attrs['TL_VERSION'] = np.int32([m_version])
        f['Module'].attrs['ATOM_MASS'] = [atom['mass']]
        f['Module'].attrs['A_UL'] = [atom['Aul']]
        f['Module'].attrs['E_UL'] = [atom['E'][1]-atom['E'][0]]
        f['Module'].attrs['JL2'] = np.int32([atom['J2'][0]])
        f['Module'].attrs['JU2'] = np.int32([atom['J2'][1]])
        f['Module'].attrs['GL'] = [atom['gL'][0]]
        f['Module'].attrs['GU'] = [atom['gL'][1]]
        f['Module'].attrs['TEMP'] = [atom['Tref']]

        T0 = atmo['T'][0]
        f['Module/temp_bc'] = np.full((ny,nx),T0)

        # Grid Data #
        # -----------

        # Grid node structured array
        node_dt = np.dtype([ \
                             ('eps',np.float), \
                             ('temp',np.float), \
                             ('density',np.float), \
                             ('B',np.float,3), \
                             ('V',np.float,3), \
                             ('dm',np.float,rdim), \
                             ('jkq',np.float,9), \
                             ('a_voigt',np.float), \
                             ('delta2',np.float), \
                             ('c_opac',np.float), \
                             ('c_emis',np.float), \
        ])

        rho_n = np.zeros((rdim,))   
        grid = f['Module'].create_dataset("g_data",(nz,ny,nx), dtype=node_dt)

        # For each height
        for iz in range(nz):

            # Populations per level in cm^-3
            n_pops = atmo['pops'][iz]
            # Total population in cm^-3
            n_N = sum(n_pops)
            # Temperature in K
            n_T = atmo['T'][iz]
            # Magnetic field (no magnetic field)
            n_B = [Bx,By,Bz]
            # Velocity field (no velocity field)
            n_v = [0.,0.,0.]
            # Damping parameter
            n_a = atmo['a_voigt'][iz]
            # Collisional rate for deexcitation
            n_Cul = atmo['Cul'][iz]
            # Opacity of the continuum in cm^-1
            n_eta_c = atmo['etac'][iz]
            # Thermal emissivity of the continuum in erg cm^-3 Hz^-1 s^-1
            # str^-1
            n_eps_c = atmo['epsc'][iz]
            # Destruction probability
            n_eps = n_Cul/(n_Cul + atom['Aul'])
            # Depolarization rate
            n_delta2 = atmo['delta2'][iz]

            # For each column to replicate
            grid[iz,:,:,'eps'] = n_eps
            grid[iz,:,:,'temp'] = n_T
            grid[iz,:,:,'density'] = n_N
            grid[iz,:,:,'B'] = n_B
            grid[iz,:,:,'V'] = n_v
            grid[iz,:,:,'jkq'] = [0.0]*9
            grid[iz,:,:,'a_voigt'] = n_a
            grid[iz,:,:,'delta2'] = n_delta2
            grid[iz,:,:,'c_opac'] = n_eta_c
            grid[iz,:,:,'c_emis'] = n_eps_c

            # If there are perturbations, apply them
            if bPerx:
                if sys.argv.count('-Afx') > 0:
                    for ix in range(nx):
                        grid[iz,:,ix,'temp'] += \
                            n_T*fVPerx*np.cos(fTPerx*x[ix])
                else:
                    for ix in range(nx):
                        grid[iz,:,ix,'temp'] += \
                            fVPerx*np.cos(fTPerx*x[ix])
                    
            if bPery:
                if sys.argv.count('-Afy') > 0:
                    for iy in range(ny):
                        grid[iz,iy,:,'temp'] += \
                            n_T*fVPery*np.cos(fTPery*y[iy])
                else:
                    for iy in range(ny):
                        grid[iz,iy,:,'temp'] += \
                            fVPery*np.cos(fTPery*y[iy])

            ridx = 0
            # For each level
            for jj in range(2):
                J2 = atom['J2'][jj]               # Get angular momentum
                for K in range(J2+1):             # For each K multipole
                    # If 0, initilize rho00 with input populations
                    if K == 0:
                        rho00 = n_pops[jj]/n_N/ \
                            np.sqrt(float(J2+1))
                        rho_n[ridx]   = rho00
                        rho_n[ridx+1] = 0.0
                        ridx += 2

                        # If non-zero, initialize to no atomic
                        # polarization
                    else:

                        # For each Q possible for the current K
                        for Q in range(K*2+1):
                            rho_n[ridx]   = 0.0
                            rho_n[ridx+1] = 0.0
                            ridx += 2

            grid[iz,:,:,'dm'] = rho_n
        f.close()

        # Closing message
        msg = '\nYour pmd file (HDF5 format) for the atom {0} and \n' + \
              '  atmospheric model {1} has been created and stored \n' + \
              '  in the file: {2}'
        print(msg.format(atomname, atmoname, outname))

    #######################################
    # Internal function: write_hdf5 (end) #
    #######################################


    ################################
    # Internal function: write_pmd #
    ################################
    def write_pmd(outname):

        # Open pmd file pmd
        f = open(outname, 'wb')

        ##############
        # PORTA Head #
        ##############

        # Identifier string
        if oldpy:
            f.write(struct.pack('c'*8, *list('portapmd')))
        else:
            f.write(struct.pack('8s', b'portapmd'))
        # Endianess (0 = little)
        f.write(struct.pack('b', 0))
        # Integer size
        f.write(struct.pack('b', 4))
        # Double size
        f.write(struct.pack('b', 8))
        # PMD version
        f.write(struct.pack('i', 2))
        # Date
        now = datetime.datetime.now()
        date = [int(now.year - 1900.), int(now.month-1.), \
                int(now.day), int(now.hour), int(now.minute), \
                int(now.second)]
        f.write(struct.pack('i'*6, *date))
        # X periodicity
        f.write(struct.pack('b', 1))
        # Y periodicity
        f.write(struct.pack('b', 1))
        # Domain size in cm
        f.write(struct.pack('ddd', *[Dx,Dy,Dz]))
        # Domain origin in cm
        f.write(struct.pack('ddd', *[x0,y0,z0]))
        # Grid dimensions
        f.write(struct.pack('iii', *[nx,ny,nz]))
        # X axis
        f.write(struct.pack('d'*nx, *x))
        f.write(struct.pack('d'*(8192-nx), *np.zeros((8192-nx))))
        # Y axis
        f.write(struct.pack('d'*ny, *y))
        f.write(struct.pack('d'*(8192-ny), *np.zeros((8192-ny))))
        # Z axis
        f.write(struct.pack('d'*nz, *z))
        f.write(struct.pack('d'*(8192-nz), *np.zeros((8192-nz))))
        # Polar nodes per octant
        f.write(struct.pack('i', nth))
        # Azimuth nodes per octant
        f.write(struct.pack('i', nph))
        # Module name
        if oldpy:
            f.write(struct.pack('c'*m_nsize, *list(m_name)))
            f.write(struct.pack('c'*(1023-m_nsize), \
                                *(['\x00']*(1023-m_nsize))))
        else:
            f.write(struct.pack(str(m_nsize)+'s', \
                                bytes(m_name,encoding="utf-8")))
            f.write(struct.pack(str(1023-m_nsize)+'s', \
                    bytes('\x00'*(1023-m_nsize),encoding="utf-8")))
        # Comment
        comment = 'Model for ' + atomname + ' atom in ' + \
                  atmoname + ' atmospheric model replicated in ' + \
                  '{0}x{1}'.format(nx,ny) + ' columns for the ' + \
                  m_name + ' module'
        c_size = len(list(comment))
        if oldpy:
            f.write(struct.pack('c'*c_size, *list(comment)))
            f.write(struct.pack('c'*(4096-c_size), \
                                *(['\0']*(4096-c_size))))
        else:
            f.write(struct.pack(str(c_size)+'s', \
                                bytes(comment,encoding="utf-8")))
            f.write(struct.pack(str(4096-c_size)+'s', \
                    bytes('\0'*(4096-c_size),encoding="utf-8")))
        # Sizes of header and node
        Head_size = int(60 + nx*ny*8)
        Node_size = int(22*8 + \
                        16*(atom['J2'][0] + 1)*(atom['J2'][0] + 1) + \
                        16*(atom['J2'][1] + 1)*(atom['J2'][1] + 1))
        f.write(struct.pack('i', Head_size))
        f.write(struct.pack('i', Node_size))

        ###############
        # Module head #
        ###############

        # Module version
        f.write(struct.pack('i', m_version))
        # Atomic mass in AMU
        f.write(struct.pack('d', atom['mass']))
        # Einstein coefficient for spontaneous emission
        f.write(struct.pack('d', atom['Aul']))
        # Energy of transition in erg
        f.write(struct.pack('d', atom['E'][1]-atom['E'][0]))
        # Angular momentum of atomic levels multiplied by two
        f.write(struct.pack('ii', *atom['J2']))
        # Lande factors
        f.write(struct.pack('dd', *atom['gL']))
        # Reference temperature that determines the Doppler width to
        # build the frequency axis
        f.write(struct.pack('d', atom['Tref']))
        # Temperature at the bottom layer
        T0 = atmo['T'][0]
        f.write(struct.pack('d'*nx*ny, *([T0]*nx*ny)))

        ###############
        # Module Grid #
        ###############

        # For each height
        for iz in range(nz):

            # Populations per level in cm^-3
            n_pops = atmo['pops'][iz]
            # Total population in cm^-3
            n_N = sum(n_pops)
            # Temperature in K
            n_T = atmo['T'][iz]
            # Magnetic field (no magnetic field)
            n_B = [Bx,By,Bz]
            # Velocity field (no velocity field)
            n_v = [0.,0.,0.]
            # Damping parameter
            n_a = atmo['a_voigt'][iz]
            # Collisional rate for deexcitation
            n_Cul = atmo['Cul'][iz]
            # Opacity of the continuum in cm^-1
            n_eta_c = atmo['etac'][iz]
            # Thermal emissivity of the continuum in
            # erg cm^-3 Hz^-1 s^-1 sr^-1
            n_eps_c = atmo['epsc'][iz]
            # Destruction probability
            n_eps = n_Cul/(n_Cul + atom['Aul'])
            # Depolarization rate
            n_delta2 = atmo['delta2'][iz]

            # For each column to replicate
            for iy in range(ny):

                # If perturbation in Y
                if bPery:
                    if sys.argv.count('-Afy') > 0:
                        dTy = n_T*fVPery*np.cos(fTPery*y[iy])
                    else:
                        dTy = fVPery*np.cos(fTPery*y[iy])
                else:
                    dTy = 0.0

                for ix in range(nx):

                    # If perturbation in X
                    if bPerx:
                        if sys.argv.count('-Afx') > 0:
                            dTx = n_T*fVPerx*np.cos(fTPerx*x[ix])
                        else:
                            dTx = fVPerx*np.cos(fTPerx*x[ix])
                    else:
                        dTx = 0.0

                    # Initialize counter
                    kk = 0

                    # Write photon destruction probability
                    f.write(struct.pack('d', n_eps))
                    kk += 1
                    # Write temperature
                    f.write(struct.pack('d', n_T+dTy+dTx))
                    kk += 1
                    # Write density
                    f.write(struct.pack('d', n_N))
                    kk += 1
                    # Write magnetic field
                    f.write(struct.pack('ddd', *n_B))
                    kk += 3
                    # Write velocity field
                    f.write(struct.pack('ddd', *n_v))
                    kk += 3

                    # Density matrix elements
                    mm = 0

                    # For each level
                    for jj in range(2):

                        # Get angular momentum
                        J2 = atom['J2'][jj]

                        # For each K multipole
                        for K in range(J2 + 1):

                            # If 0, initilize rho00 with input
                            # populations
                            if K == 0:
                                rho00 = n_pops[jj]/n_N/ \
                                        np.sqrt(float(J2+1))
                                f.write(struct.pack('d', rho00))
                                f.write(struct.pack('d', 0.0))
                                kk += 2
                                mm += 2

                            # If non-zero, initialize to no atomic
                            # polarization
                            else:

                                # For each Q possible for the
                                # current K
                                for Q in range(K*2+1):
                                    f.write(struct.pack('d',0.0))
                                    f.write(struct.pack('d',0.0))
                                    kk += 2
                                    mm += 2

                    # Write radiation field tensors (initialize to 0)
                    f.write(struct.pack('d'*9, *([0.0]*9)))
                    kk += 9

                    # Write damping parameter
                    f.write(struct.pack('d', n_a))
                    kk += 1

                    # Write depolarization rate for elastic collisions
                    # with neutral hydrogen
                    f.write(struct.pack('d', n_delta2))
                    kk += 1

                    # Write continuum opacity
                    f.write(struct.pack('d', n_eta_c))
                    kk += 1

                    # Write continuum emissivity
                    f.write(struct.pack('d', n_eps_c))
                    kk += 1

                    # If first grid node, notify how much was written
                    if iz==0 and iy==0 and ix==0:
                        msg = 'Node size {0} elements and {1} ' + \
                              'bytes. Expected {2} bytes'
                        print(msg.format(kk,kk*8,Node_size))
                        msg = 'Size of density matrix is {0} ' + \
                              'elements and {1} bytes'
                        print(msg.format(mm,mm*8))

        # Close pmd
        f.close()

        # Closing message
        msg = '\nYour pmd file (binary format) for the atom {0} and \n' + \
            '  atmospheric model {1} has been created and stored \n' + \
            '  in the file: {2}'
        print(msg.format(atomname, atmoname, outname))

    ######################################
    # Internal function: write_pmd (end) #
    ######################################


    #######################
    # Start main function #
    #######################

    # Set defaults
    outname = 'test'
    atomname = 'Sr'
    atmoname = 'FALC'
    Bm = '0'
    Bt = '0'
    Ba = '0'
    fTPerx = -1.0
    fVPerx = 1.0
    bPerx = False
    fTPery = -1.0
    fVPery = 1.0
    bPery = False
    inx = -1
    iny = -1

    # Check if old version
    if sys.version_info[0] < 3:
        oldpy = True
    else:
        oldpy = False

    # Manage command line inputs
    if len(sys.argv) > 1:
        options = ['-h','-f','-a','-A','-B','-t','-z', \
                   '-nx','-ny','-Px','-Py','-Ax','-Ay','-Afx','-Afy']
        if '-h' in sys.argv:
            msg = 'use: python twolevel-pmd.py -h -f file -a ' + \
                  'atom -A atmosphere'
            print(msg)
            msg = '-h == shows this help'
            print(msg)
            msg = '-f == name of output file'
            print(msg)
            msg = '-a == model atom (Sr)'
            print(msg)
            msg = '-A == model atmosphere (FALC)'
            print(msg)
            msg = '-B == homogeneous magnetic field module in gauss'
            print(msg)
            msg = '-t == homogeneous magnetic field ' + \
                  'inclination in deg'
            print(msg)
            msg = '-z == homogeneous magnetic field ' + \
                  'azimuth in deg'
            print(msg)
            msg = '-nx == number of nodes in X axis'
            print(msg)
            msg = '-ny == number of nodes in Y axis'
            print(msg)
            msg = '-Px == period for temperature perturbation ' + \
                  'or domain size in X axis in km'
            print(msg)
            msg = '-Py == period for temperature perturbation ' + \
                  'or domain size in Y axis in km'
            print(msg)
            msg = '-Ax == amplitude for temperature perturbation ' + \
                  'in X axis (absolute)'
            print(msg)
            msg = '-Ay == amplitude for temperature perturbation ' + \
                  'in Y axis (absolute)'
            print(msg)
            msg = '-Afx == amplitude for temperature perturbation ' + \
                'in X axis \n    (as a fraction of model temperature)'
            print(msg)
            msg = '-Afy == amplitude for temperature perturbation ' + \
                'in Y axis \n    (as a fraction of model temperature)'
            print(msg)
            sys.exit()
        if '-f' in sys.argv:
            if sys.argv.count('-f') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -f'
                sys.exit(msg)
            ii = sys.argv.index('-f') + 1
            if ii >= len(sys.argv):
                msg = 'No file name after -f'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No file name after -f'
                sys.exit(msg)
            outname = sys.argv[ii]
            try:
                f = open(outname+'.pmd', 'w')
                f.close()
            except:
                msg = 'Cannot open {0} for writing'
                sys.exit(msg.format(outname))
        if '-a' in sys.argv:
            if sys.argv.count('-a') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -a'
                sys.exit(msg)
            ii = sys.argv.index('-a') + 1
            if ii >= len(sys.argv):
                msg = 'No atom name after -a'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No atom name after -a'
                sys.exit(msg)
            atomname = sys.argv[ii]
            if atomname != 'Sr':
                msg = 'Atom name must be Sr'
                sys.exit(msg)
        if '-A' in sys.argv:
            if sys.argv.count('-A') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -A'
                sys.exit(msg)
            ii = sys.argv.index('-A') + 1
            if ii >= len(sys.argv):
                msg = 'No atmospheric model name after -A'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No atmospheric model name after -A'
                sys.exit(msg)
            atmoname = sys.argv[ii]
            if atmoname != 'FALC':
                msg = 'Atmo name must be FALC'
                sys.exit(msg)
        if '-B' in sys.argv:
            if sys.argv.count('-B') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -B'
                sys.exit(msg)
            ii = sys.argv.index('-B') + 1
            if ii >= len(sys.argv):
                msg = 'No magnetic field after -B'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No magnetic field after -B'
                sys.exit(msg)
            Bm = sys.argv[ii]
        if '-t' in sys.argv:
            if sys.argv.count('-t') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -t'
                sys.exit(msg)
            ii = sys.argv.index('-t') + 1
            if ii >= len(sys.argv):
                msg = 'No magnetic field inclination after -t'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No magnetic field inclination after -t'
                sys.exit(msg)
            Bt = sys.argv[ii]
        if '-z' in sys.argv:
            if sys.argv.count('-z') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -z'
                sys.exit(msg)
            ii = sys.argv.index('-z') + 1
            if ii >= len(sys.argv):
                msg = 'No magnetic field azimuth after -z'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No magnetic field azimuth after -z'
                sys.exit(msg)
            Ba = sys.argv[ii]
        if '-Afx' in sys.argv:
            if sys.argv.count('-Afx') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -Afx'
                sys.exit(msg)
            if sys.argv.count('-Ax') > 0:
                msg = 'Options -Afx and -Ax are mutually ' + \
                    'exclusive. Pick only one of them'
                sys.exit(msg)
            ii = sys.argv.index('-Afx') + 1
            if ii >= len(sys.argv):
                msg = 'No amplitude fraction after -Afx'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No amplitude fraction after -Afx'
                sys.exit(msg)
            fVPerx = float(sys.argv[ii])
            bPerx = True
            if np.absolute(fVPerx) <= 0.0:
                msg = 'X amplitude must be larger than 0'
                sys.exit(msg)
        if '-Ax' in sys.argv:
            if sys.argv.count('-Ax') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -Ax'
                sys.exit(msg)
            if sys.argv.count('-Afx') > 0:
                msg = 'Options -Afx and -Ax are mutually ' + \
                    'exclusive. Pick only one of them'
                sys.exit(msg)
            ii = sys.argv.index('-Ax') + 1
            if ii >= len(sys.argv):
                msg = 'No amplitude after -Ax'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No amplitude after -Ax'
                sys.exit(msg)
            fVPerx = float(sys.argv[ii])
            bPerx = True
            if np.absolute(fVPerx) <= 0.0:
                msg = 'X amplitude must be larger than 0'
                sys.exit(msg)
        if '-Px' in sys.argv:
            if sys.argv.count('-Px') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -Px'
                sys.exit(msg)
            ii = sys.argv.index('-Px') + 1
            if ii >= len(sys.argv):
                msg = 'No period after -Px'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No period after -Px'
                sys.exit(msg)
            fTPerx = float(sys.argv[ii])
            if fTPerx < 1e-6:
                msg = 'X period must be larger than 0'
                sys.exit(msg)
        if bPerx and fTPerx < 1e-6:
            msg = 'You must specify a period in X if you ' + \
                  'define the amplitude of the perturbation'
            sys.exit(msg)
        if '-nx' in sys.argv:
            if sys.argv.count('-nx') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -nx'
                sys.exit(msg)
            ii = sys.argv.index('-nx') + 1
            if ii >= len(sys.argv):
                msg = 'No number of nodes after -nx'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No number of nodes after -nx'
                sys.exit(msg)
            inx = int(sys.argv[ii])
            if inx < 2:
                msg = 'Number of X nodes must be at least 2'
                sys.exit(msg)
            if bPerx and inx < 5:
                msg = 'For the pertubed model, nodes must be at ' + \
                      'least 5 for one period in X'
                sys.exit(msg)
        if '-Afy' in sys.argv:
            if sys.argv.count('-Afy') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -Afy'
                sys.exit(msg)
            if sys.argv.count('-Ay') > 0:
                msg = 'Options -Afy and -Ay are mutually ' + \
                    'exclusive. Pick only one of them'
                sys.exit(msg)
            ii = sys.argv.index('-Afy') + 1
            if ii >= len(sys.argv):
                msg = 'No amplitude fraction after -Afy'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No amplitude fraction after -Afy'
                sys.exit(msg)
            fVPery = float(sys.argv[ii])
            bPery = True
            if np.absolute(fVPery) <= 0.0:
                msg = 'Y amplitude must be larger than 0'
                sys.exit(msg)
        if '-Ay' in sys.argv:
            if sys.argv.count('-Ay') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -Ay'
                sys.exit(msg)
            if sys.argv.count('-Afy') > 0:
                msg = 'Options -Afy and -Ay are mutually ' + \
                    'exclusive. Pick only one of them'
                sys.exit(msg)
            ii = sys.argv.index('-Ay') + 1
            if ii >= len(sys.argv):
                msg = 'No amplitude after -Ay'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No amplitude after -Ay'
                sys.exit(msg)
            fVPery = float(sys.argv[ii])
            bPery = True
            if np.absolute(fVPery) <= 0.0:
                msg = 'Y amplitude must be larger than 0'
                sys.exit(msg)
        if '-Py' in sys.argv:
            if sys.argv.count('-Py') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -Py'
                sys.exit(msg)
            ii = sys.argv.index('-Py') + 1
            if ii >= len(sys.argv):
                msg = 'No period after -Py'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No period after -Py'
                sys.exit(msg)
            fTPery = float(sys.argv[ii])
            bPery = True
            if fTPery < 1e-6:
                msg = 'Y period must be larger than 0'
                sys.exit(msg)
        if bPery and fTPery < 1e-6:
            msg = 'You must specify a period in Y if you ' + \
                  'define the amplitude of the perturbation'
            sys.exit(msg)
        if '-ny' in sys.argv:
            if sys.argv.count('-y') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -ny'
                sys.exit(msg)
            ii = sys.argv.index('-ny') + 1
            if ii >= len(sys.argv):
                msg = 'No number of nodes after -ny'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No number of nodes after -ny'
                sys.exit(msg)
            iny = int(sys.argv[ii])
            if iny < 2:
                msg = 'Number of Y nodes must be at least 2'
                sys.exit(msg)
            if bPery and iny < 5:
                msg = 'For the pertubed model, nodes must be at ' + \
                      'least 5 for one period in Y'
                sys.exit(msg)
    else:
        msg = 'You can run with the -h option to get the list of ' + \
              'possible parameters'
        print(msg)

######################################################################

    # Check magnetic fields are numbers
    try:
        Bm = float(Bm)
    except ValueError:
        msg = 'Magnetic field strength must be a number'
        print(msg)
        raise
    except:
        raise
    try:
        Bt = float(Bt)
        Bt *= np.pi/180.0
        ctB = np.cos(Bt)
        stB = np.sin(Bt)
    except ValueError:
        msg = 'Magnetic field inclination must be a number'
        print(msg)
        raise
    except:
        raise
    try:
        Ba = float(Ba)
        Ba *= np.pi/180.0
        caB = np.cos(Ba)
        saB = np.sin(Ba)
    except ValueError:
        msg = 'Magnetic field strength azimuth be a number'
        print(msg)
        raise
    except:
        raise

    # Get magnetic field cartesian components
    Bx = Bm*stB*caB
    By = Bm*stB*saB
    Bz = Bm*ctB

    # Angular quadrature, nodes per octant
    nth = 4
    nph = 2
    # Minimum value of mu
    mu = np.polynomial.legendre.leggauss(4)[0]
    mu = 0.5*mu + 0.5
    minmu = np.amin(np.absolute(mu))

    # Module version and name
    m_version = 2
    m_name = 'twolevel'

    # Atom
    atom = get_atom(atomname)
    if not atom['check']:
        msg = 'There was a problem setting the atomic model'
        sys.exit(msg)

    # Atmosphere
    atmo = get_atmo(atmoname, atomname)
    if not atmo['check_atmo']:
        msg = 'There was a problem setting the thermal part of ' + \
              'the atmospheric model'
        sys.exit(msg)
    if not atmo['check_atom']:
        msg = 'There was a problem setting the atomic part of ' + \
              'the atmospheric model'
        sys.exit(msg)

    # If FALC and Strontium, cut the model
    if atomname == 'Sr' and atmoname == 'FALC':
        z1 = 33
        for key in ['z','vmi','T','Ne','Cul','a_voigt','etac', \
                    'epsc','pops','delta2']:
            atmo[key] = atmo[key][:z1]
        dz = np.array(atmo['z'])
        dz = np.absolute(dz[1:] - dz[:-1])
        atmo['mindz'] = np.amin(dz)
        atmo['nz'] = len(atmo['z'])

    # Horizontal nodes, a column will be replicated
    if inx < 0:
        if bPerx:
            nx = 5
        else:
            nx = 3
    else:
        nx = inx
    if iny < 0:
        if bPery:
            ny = 5
        else:
            ny = 3
    else:
        ny = iny

    # Configure horizontal geometry.

    # If no perturbed and domain not specified
    if not bPerx and fTPerx < 1e-6:
        # It is extended enough so every ray crosses the
        # bottom boundary for 1.5D
        x0 = 0.0
        x1a = float(nx)*atmo['mindz']*minmu*1.01 + x0
        x1b = np.amax(atmo['z']) - np.min(atmo['z']) + x0
        x1 = np.max([x1a,x1b])
    else:
        # The period is the domain, not the difference
        # between extremes
        fTPerx *= 1e5
        x0 = 0.0
        x1 = fTPerx*(1.0 - 1.0/float(nx))

    # If no perturbed and domain not specified
    if not bPery and fTPery < 1e-6:
        # It is extended enough so every ray crosses the
        # bottom boundary for 1.5D
        y0 = 0.0
        y1a = float(ny)*atmo['mindz']*minmu*1.01 + y0
        y1b = np.amax(atmo['z']) - np.min(atmo['z']) + y0
        y1 = np.max([y1a,y1b])
    else:
        # The period is the domain, not the difference
        # between extremes
        fTPery *= 1e5
        y0 = 0.0
        y1 = fTPery*(1.0 - 1.0/float(ny))
    
    # Build geometry
    x = np.linspace(x0,x1,nx,endpoint=True)
    Dx = x[-1] + x[1] - 2.*x[0]
    y = np.linspace(y0,y1,ny,endpoint=True)
    Dy = y[-1] + y[1] - 2.*y[0]
    z = atmo['z']
    nz = atmo['nz']
    z0 = z[0]
    Dz = z[-1] - z[0]

    # Check correct size if perturbed
    if bPerx:
        if np.absolute(Dx-fTPerx) > 1e-6:
            msg = 'Something went wrong defining the domain'
            msg += '{0} != {1}'.format(Dx,fTPerx)
            sys.exit(msg)
        fTPerx = 2.0*np.pi/fTPerx
    if bPery:
        if np.absolute(Dy-fTPery) > 1e-6:
            msg = 'Something went wrong defining the domain'
            msg += '{0} != {1}'.format(Dy,fTPery)
            sys.exit(msg)
        fTPery = 2.0*np.pi/fTPery

    # Get size module name
    m_nsize = len(list(m_name))

    # Transform energies into erg
    for i in range(len(atom['E'])):
        atom['E'][i] = atom['E'][i]*6.62606896e-27*299792458e2

    write_pmd(outname+'.pmd')
    write_hdf5(outname+'.h5')
        

######################################################################

if __name__ == "__main__":
    main()
