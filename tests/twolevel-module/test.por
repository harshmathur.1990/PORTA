loadmodel_h5 test.h5
solve_jacobi   10   1.0e-4
savemodel_h5 test_out.h5

loadmodel test.pmd
solve_jacobi   10   1.0e-4
savemodel test_out.pmd

loadmodel_h5 test_out.h5
fs_surface 0 0 0 100 test_out.psp
fs_surface_h5 0 0 0 100 test_out_psp.h5
fs_surface_lc 0 0 0 100 test_out_lc.psp
fs_surface_lc_h5 0 0 0 100 test_out_lc_psp.h5

loadmodel test_out.pmd
get_tau_pos 1.0 0.0 0.0 0 100 test_out.tau
get_tau_pos_h5 1.0 0.0 0.0 0 100 test_out_tau.h5



