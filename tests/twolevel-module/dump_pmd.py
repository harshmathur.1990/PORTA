#!/usr/bin/env python
# -*- coding: utf-8 -*-

#===============================================================================
# Python3 code to read PORTA PMD files (in binary and HDF5 format)
#-------------------------------------------------------------------------------
#
# Defines functions:
#     dump_pmd[_h5]
#
# Ángel de Vicente (angel.de.vicente@iac.es)
#
# Got help from
# https://www.devdungeon.com/content/working-binary-data-python
#===============================================================================

import argparse
import numpy as np
import struct

def dump_pmd_h5(f):
    import h5py
    
    ######################
    #READING PORTA HEADER#
    ######################
    
    print(f"Magic String: {f.attrs['PMD_MAGIC'].decode('utf-8')}")
    creation = f.attrs['CREATION_DATE']
    print(f"Creation date: {creation[2]}/{creation[1]+1}/{creation[0]+1900} {creation[3]}:{creation[4]}:{creation[5]}")
    
    [periodx,periody] = f.attrs['PERIODICITY']
    print(f"Periodicity: X: {periodx} Y: {periody}")

    [s_X,s_Y,s_Z] = f.attrs['DOMAIN_SIZE']
    print(f"Domain size: X: {s_X} Y: {s_Y} Z: {s_Z}")

    (o_X,o_Y,o_Z) = f.attrs['DOMAIN_ORIGIN']
    print(f"Domain origin: X: {o_X} Y: {o_Y} Z: {o_Z}")

    (g_NX,g_NY,g_NZ) = f.attrs['GRID_DIMENSIONS']
    print(f"Number of nodes: X: {g_NX} Y: {g_NY} Z: {g_NZ}")


    x_array = f.attrs['X_AXIS']
    print(f"X array: \n{x_array}\n")

    y_array = f.attrs['Y_AXIS']
    print(f"Y array: \n{y_array}\n")

    z_array = f.attrs['Z_AXIS']
    print(f"Z array: \n{z_array}\n")


    inc_a,azi_a = f.attrs['POLAR_NODES'][0],f.attrs['AZIMUTH_NODES'][0]
    print(f"Number of angles: Inclination: {inc_a} Azimuthal: {azi_a}")

    print(f"Name of Atomic Module: {f.attrs['MODULE_NAME'].decode('utf-8')}")
    print(f"Model Comment: {f.attrs['MODULE_COMMENT'].decode('utf-8')}")

    #############################
    #READING ATOMIC MODEL HEADER#
    #############################

    tl_v      = f['Module'].attrs['TL_VERSION'][0]
    atom_mass = f['Module'].attrs['ATOM_MASS'][0]
    a_ul      = f['Module'].attrs['A_UL'][0]
    e_ul      = f['Module'].attrs['E_UL'][0]
    jl2       = f['Module'].attrs['JL2'][0]
    ju2       = f['Module'].attrs['JU2'][0]
    gl        = f['Module'].attrs['GL'][0]
    gu        = f['Module'].attrs['GU'][0]
    temp      = f['Module'].attrs['TEMP'][0]
    
    print(f"""

#############################
ATOMIC MODEL HEADER
#############################

TL_VERSION: {tl_v}
ATOM_MASS: {atom_mass}
A_UL: {a_ul}
E_UL: {e_ul}
JL2: {jl2}
JU2: {ju2}
GL: {gl}
GU: {gu}
TEMP: {temp}""")

    temp_bc = np.asarray(f['Module']['temp_bc'])
    print(f"BC Temp:: \n{temp_bc}\n")


    ####################
    #READING DATA NODES#
    ####################

    l_limit = (jl2+1)*(jl2+1)
    u_limit = (ju2+1)*(ju2+1)
    dm_size = 2*(l_limit+u_limit)
    NJKQ = 9

    d_f = 'f'+str(8)

    node_data = np.zeros((g_NZ,g_NY,g_NX), dtype=[('eps',d_f),('tmp',d_f),('density',d_f),('bx',d_f),('by',d_f),('bz',d_f),('vx',d_f),('vy',d_f),('vz',d_f),('dm',d_f,dm_size),('jkq',d_f,NJKQ),('voigt_a',d_f),('delta2',d_f),('c_opac',d_f),('c_emis',d_f)])

    print(f"""

#############################
NODES DATA
#############################
    """)

    
    for ii in range(g_NZ):
        for jj in range(g_NY):
            for kk in range(g_NX):
                node_data[ii,jj,kk]['eps'] = f['Module/g_data']['eps'][ii,jj,kk]
                node_data[ii,jj,kk]['tmp'] = f['Module/g_data']['temp'][ii,jj,kk]
                node_data[ii,jj,kk]['density'] = f['Module/g_data']['density'][ii,jj,kk]
                node_data[ii,jj,kk]['bx'] = f['Module/g_data']['B'][ii,jj,kk][0]
                node_data[ii,jj,kk]['by'] = f['Module/g_data']['B'][ii,jj,kk][1]
                node_data[ii,jj,kk]['bz'] = f['Module/g_data']['B'][ii,jj,kk][2]
                node_data[ii,jj,kk]['vx'] = f['Module/g_data']['V'][ii,jj,kk][0]
                node_data[ii,jj,kk]['vy'] = f['Module/g_data']['V'][ii,jj,kk][1]
                node_data[ii,jj,kk]['vz'] = f['Module/g_data']['V'][ii,jj,kk][2]
                node_data[ii,jj,kk]['dm']= f['Module/g_data']['dm'][ii,jj,kk]
                node_data[ii,jj,kk]['jkq'] = f['Module/g_data']['jkq'][ii,jj,kk]
                
                node_data[ii,jj,kk]['voigt_a'] = f['Module/g_data']['a_voigt'][ii,jj,kk]
                node_data[ii,jj,kk]['delta2'] = f['Module/g_data']['delta2'][ii,jj,kk]
                node_data[ii,jj,kk]['c_opac'] = f['Module/g_data']['c_opac'][ii,jj,kk]
                node_data[ii,jj,kk]['c_emis'] = f['Module/g_data']['c_emis'][ii,jj,kk]

                print(f"""
NODE {ii},{jj},{kk}:
--------------------
eps:     {node_data[ii,jj,kk]['eps']}
temp:    {node_data[ii,jj,kk]['tmp']}
density: {node_data[ii,jj,kk]['density']}
B:       {node_data[ii,jj,kk]['bx']} {node_data[ii,jj,kk]['by']} {node_data[ii,jj,kk]['bz']}
V:       {node_data[ii,jj,kk]['vx']} {node_data[ii,jj,kk]['vy']} {node_data[ii,jj,kk]['vz']}
dm:      {node_data[ii,jj,kk]['dm']}
jkq:     {node_data[ii,jj,kk]['jkq']}
a_voigt: {node_data[ii,jj,kk]['voigt_a']}
delta2:  {node_data[ii,jj,kk]['delta2']}
c_opac:  {node_data[ii,jj,kk]['c_opac']}
c_emis:  {node_data[ii,jj,kk]['c_emis']}""")

                            
    f.close()

    
def dump_pmd(filename):
    file = open(filename, 'rb')

    ######################
    #READING PORTA HEADER#
    ######################
    
    m_s = file.read(8).decode('utf-8') ; print("Magic String: ",m_s)
    endian = int.from_bytes(file.read(1),byteorder='little') ; print("Endianness: ", endian)
    if endian == 0:
        endian_s = "little"
        e_str    = '<'
    else:
        endian_s = "big"
        e_str    = '>'
        
    sizeint = int.from_bytes(file.read(1),byteorder=endian_s)      ; print("Size of integer: ", sizeint)
    sizedble = int.from_bytes(file.read(1),byteorder=endian_s)     ; print("Size of double: ", sizedble)
    vpmd = int.from_bytes(file.read(sizeint),byteorder=endian_s)   ; print("Version of PMD file: ", vpmd)

    creation = struct.unpack(e_str+'iiiiii', file.read(sizeint*6))
    print(f"Creation date: {creation[2]}/{creation[1]+1}/{creation[0]+1900} {creation[3]}:{creation[4]}:{creation[5]}")
    
    period = struct.unpack(e_str+'ss', file.read(2)) ;
    periodx = int.from_bytes(period[0],byteorder=endian_s)
    periody = int.from_bytes(period[1],byteorder=endian_s)
    print(f"Periodicity: X: {periodx} Y: {periody}")

    (s_X,s_Y,s_Z) = struct.unpack(e_str+'ddd', file.read(sizedble*3))
    print(f"Domain size: X: {s_X} Y: {s_Y} Z: {s_Z}")

    (o_X,o_Y,o_Z) = struct.unpack(e_str+'ddd', file.read(sizedble*3))
    print(f"Domain origin: X: {o_X} Y: {o_Y} Z: {o_Z}")

    (g_NX,g_NY,g_NZ) = struct.unpack(e_str+'iii', file.read(sizeint*3))
    print(f"Number of nodes: X: {g_NX} Y: {g_NY} Z: {g_NZ}")


    MAXDIM = 8192
    x_array = np.asarray(struct.unpack(e_str+'d'*g_NX, file.read(sizedble*g_NX)))
    print(f"X array: \n{x_array}\n")
    _ = file.read((MAXDIM-g_NX)*sizedble)

    y_array = np.asarray(struct.unpack(e_str+'d'*g_NY, file.read(sizedble*g_NY)))
    print(f"Y array: \n{y_array}\n")
    _ = file.read((MAXDIM-g_NY)*sizedble)

    z_array = np.asarray(struct.unpack(e_str+'d'*g_NZ, file.read(sizedble*g_NZ)))
    print(f"Z array: \n{z_array}\n")
    _ = file.read((MAXDIM-g_NZ)*sizedble)

    (inc_a,azi_a) = struct.unpack(e_str+'ii', file.read(sizeint*2))
    print(f"Number of angles: Inclination: {inc_a} Azimuthal: {azi_a}")

    name_module = file.read(1023).decode('utf-8') 
    name = name_module.split('\0',1)[0]
    print(f"Name of Atomic Module: {name}")

    comment_model = file.read(4096).decode('utf-8') 
    comment = comment_model.split('\0',1)[0]
    print(f"Model Comment: {comment}")

    (size_PMD_header,size_node) = struct.unpack(e_str+'ii', file.read(sizeint*2))
    print(f"Size of the module header in bytes: {size_PMD_header}")
    print(f"Size of the node data in bytes: {size_node}")

    #############################
    #READING ATOMIC MODEL HEADER#
    #############################

    (tl_v,atom_mass,a_ul,e_ul,jl2,ju2,gl,gu,temp) = struct.unpack(e_str+'idddiiddd', file.read(sizeint*3+sizedble*6))

    print(f"""

#############################
ATOMIC MODEL HEADER
#############################

TL_VERSION: {tl_v}
ATOM_MASS: {atom_mass}
A_UL: {a_ul}
E_UL: {e_ul}
JL2: {jl2}
JU2: {ju2}
GL: {gl}
GU: {gu}
TEMP: {temp}""")

    # mod_x_array = np.asarray(struct.unpack(e_str+'d'*g_NX, file.read(sizedble*g_NX)))
    # print(f"X array: \n{mod_x_array}\n")

    # mod_y_array = np.asarray(struct.unpack(e_str+'d'*g_NY, file.read(sizedble*g_NY)))
    # print(f"Y array: \n{mod_y_array}\n")

    temp_bc = np.zeros((g_NY,g_NX), dtype=np.float)
    for ii in range(g_NY):
        for jj in range(g_NX):
            temp_bc[ii, jj] = struct.unpack('<d', file.read(sizedble))[0]
    print(f"BC Temp:: \n{temp_bc}\n")


    ####################
    #READING DATA NODES#
    ####################
    fields_per_node = size_node // 8
    l_limit = (jl2+1)*(jl2+1)
    u_limit = (ju2+1)*(ju2+1)
    dm_size = 2*(l_limit+u_limit)
    NJKQ = fields_per_node - 13 - dm_size 

    d_f = 'f'+str(sizedble)

    node_data = np.zeros((g_NZ,g_NY,g_NX), dtype=[('eps',d_f),('tmp',d_f),('density',d_f),('bx',d_f),('by',d_f),('bz',d_f),('vx',d_f),('vy',d_f),('vz',d_f),('dm',d_f,dm_size),('jkq',d_f,NJKQ),('voigt_a',d_f),('delta2',d_f),('c_opac',d_f),('c_emis',d_f)])

    print(f"""

#############################
NODES DATA
#############################
    """)

    
    for ii in range(g_NZ):
        for jj in range(g_NY):
            for kk in range(g_NX):
                node_data[ii,jj,kk]['eps'] = struct.unpack('<d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['tmp'] = struct.unpack('<d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['density'] = struct.unpack('<d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['bx'] = struct.unpack('<d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['by'] = struct.unpack('<d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['bz'] = struct.unpack('<d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['vx'] = struct.unpack('<d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['vy'] = struct.unpack('<d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['vz'] = struct.unpack('<d', file.read(sizedble))[0]

                for dmi in range(dm_size):
                    node_data[ii,jj,kk]['dm'][dmi] = struct.unpack('<d', file.read(sizedble))[0]

                for jkqi in range(NJKQ):
                    node_data[ii,jj,kk]['jkq'][jkqi] = struct.unpack('<d', file.read(sizedble))[0]
                
                node_data[ii,jj,kk]['voigt_a'] = struct.unpack('<d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['delta2'] = struct.unpack('<d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['c_opac'] = struct.unpack('<d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['c_emis'] = struct.unpack('<d', file.read(sizedble))[0]

                print(f"""
NODE {ii},{jj},{kk}:
--------------------
eps:     {node_data[ii,jj,kk]['eps']}
temp:    {node_data[ii,jj,kk]['tmp']}
density: {node_data[ii,jj,kk]['density']}
B:       {node_data[ii,jj,kk]['bx']} {node_data[ii,jj,kk]['by']} {node_data[ii,jj,kk]['bz']}
V:       {node_data[ii,jj,kk]['vx']} {node_data[ii,jj,kk]['vy']} {node_data[ii,jj,kk]['vz']}
dm:      {node_data[ii,jj,kk]['dm']}
jkq:     {node_data[ii,jj,kk]['jkq']}
a_voigt: {node_data[ii,jj,kk]['voigt_a']}
delta2:  {node_data[ii,jj,kk]['delta2']}
c_opac:  {node_data[ii,jj,kk]['c_opac']}
c_emis:  {node_data[ii,jj,kk]['c_emis']}""")
            
    file.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="PORTA pmd file")
    args = parser.parse_args()

    try:
        import h5py
        f = h5py.File(args.file,'r')
        dump_pmd_h5(f)
    except:
        # I can't open it as HDF5 file. Assume is a binary PMD file
        dump_pmd(args.file)

if __name__ == "__main__":
    main()

    
    

