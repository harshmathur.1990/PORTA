#!/usr/bin/env python
# -*- coding: utf-8 -*-

#####################
# multilevel-pmd.py
#
# Tanausú del Pino Alemán
# Ángel de Vicente
#
# Writes a simple pmd file to test the multilevel module
#
#####################
#
# 01/21/2020 - Added the extention modifier to the filename check
#              in main (AdV)

# 12/03/2019 - Added routine to create the file in HDF5 format,
#              so if the package h5py is found both binary and
#              HDF5 versions are created (AdV)
#
# 11/26/2019 - Added homogeneous magnetic field, with strength,
#              inclination and azimuth (TdPA)
#            - Bugfix: when interpreting the atmosphere option
#              the code was working with the atomic variables (TdPA)
#
# 06/21/2019 - Added microturbulence, removed alternative calcium,
#              changed the horizontal axis definition to make it as
#              extended as needed given the model and the angular
#              quadrature or makes the model a cube (TdPA)
#
# 06/19/2019 - Added an alternative Calcium initialization, it was for
#              local testing (TdPA)
#
#####################

import sys, math, os, struct, copy
import numpy as np
import datetime
from pathlib import Path
from multiprocessing import Pool
from tqdm import tqdm


######################################################################


def generate_radiative_transitions():
    transitions = [3, 0, 1, 0, 5, 1, 5, 3, 7, 0, 4, 0, 7, 2, 4, 2, 6, 1, 8, 3, 6, 3]

    # transitions = [5, 1, 5, 3, 7, 2, 4, 2, 6, 1, 8, 3, 6, 3]

    return transitions


def collisional_transitions():
    collisional_transitions = list()

    radiative_transitions = generate_radiative_transitions()
    for i in range(9):
        for j in range(9):
            if i <= j:
                continue

            rad_p = False

            k = 0
            while k < len(radiative_transitions):
                if radiative_transitions[k] == i and radiative_transitions[k + 1] == j:
                    rad_p = True
                    break
                k += 2

            if rad_p is False:
                collisional_transitions.append(i)
                collisional_transitions.append(j)

    return collisional_transitions


def get_atom(atomname):
    ''' Configures the atomic model
    '''

    atom = {'check': False}

    if atomname == 'H':

        # Atomic mass in AMU
        atom['mass'] = 1.00784
        # Number of levels
        atom['NL'] = 9
        # Number of transitions (radiative + forbidden collisional)
        atom['NT'] = 36
        # Number of transitions (radiative)
        atom['NR'] = 11
        # Energy of atomic levels in cm^-1
        atom['E'] = [0, 82258.918314503884176, 82258.953600449603982, 82259.284202523806016, 97492.210253263619961, 97492.220754027264775, 97492.318486079660943, 97492.318663854093757, 97492.354619285237277]
        # Land\'e factor of atomic levels
        atom['gL'] = [2.0, 0.667, 2.0, 1.333, 0.667, 2.0, 0.8, 1.333, 1.2]
        # Total angular momentum of atomic levels multiplied by two
        atom['J2'] = [1, 1, 1, 3, 1, 1, 3, 3, 5]
        # Pair of indexes of upper-lower levels for each transition
        atom['iul'] = generate_radiative_transitions() + collisional_transitions()
        # Einstein spontaneous emission coefficient for each radiative
        # transition
        atom['Aul'] = [6.2648e8, 6.2649e8, 2.1046e6, 4.2097e6, 1.6725e8, 1.6725e8, 2.2448e7, 2.2449e7, 5.3877e7, 6.4651e7, 1.0775e7]

        # atom['Aul'] = [2.1046e6, 4.2097e6, 2.2448e7, 2.2449e7, 5.3877e7,
        #                6.4651e7, 1.0775e7]
        # Number of frequencies for each transition (must be odd and
        # larger than nfreqc)
        atom['nfreq'] = [51, 51, 151, 151, 51, 51, 151, 151, 151, 151, 151]
        # Number of frequencies for the core of each transition (must
        # be odd)
        atom['nfreqc'] = [11, 11, 71, 71, 11, 11, 71, 71, 71, 71, 71]
        # Doppler widths to cover with each transition (must be larger
        # or equal to Dwc)
        atom['Dw'] = [100, 100, 3000, 3000, 100, 100, 3000, 3000, 3000, 3000, 3000]
        # Doppler widths to cover with the core of each transition
        atom['Dwc'] = [10, 10, 50, 50, 10, 10, 50, 50, 50, 50, 50]
        # Reference temperature that determines the Doppler width to
        # build the frequency axis
        atom['Tref'] = [5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3]
        # Maximum K multipole order to take into account. negative
        # means unlimited
        atom['Kcut'] = -1
        # Success flag
        atom['check'] = True

    return atom


######################################################################

def get_atmo(atmoname, atomname):
    ''' Configures the atmospheric model
    '''

    # Initialize control flags
    atmo = {'check_atmo': False, \
            'check_atom': False}

    atmo['nz'] = 177
    # Heights in km
    atmo['z'] = [
        2980.54, 2960.541, 2940.541, 2920.542, 2900.542, 2880.543,
        2860.543, 2840.543, 2820.544, 2800.544, 2780.544,
        2760.544, 2740.544, 2720.545, 2700.54475, 2680.545, 2660.545,
        2640.546, 2620.546, 2600.546, 2580.547, 2560.547, 2540.547, 2520.547,
        2500.547, 2480.547, 2460.547, 2440.548, 2420.548, 2400.548,
        2380.549, 2360.549, 2340.549, 2320.55, 2300.55, 2280.55,
        2260.55, 2240.55, 2220.551, 2200.551, 2180.551, 2160.551,
        2140.552, 2120.552, 2100.552, 2080.553, 2060.553125,
        2040.553125, 2020.553125, 2000.552875, 1980.552875, 1960.554125, 1940.553,
        1920.554, 1900.554, 1880.554, 1860.555, 1840.556, 1820.556, 1800.556,
        1780.556, 1760.556, 1740.556, 1720.557, 1700.557, 1680.556875,
        1660.556875, 1640.557, 1620.557, 1600.558, 1580.558, 1560.558,
        1540.559, 1520.557875, 1500.558, 1480.559, 1460.559, 1440.559, 1420.56,
        1400.558875, 1380.56, 1360.56, 1340.56, 1320.56, 1300.561,
        1280.56, 1260.561, 1240.561, 1220.561, 1200.562,
        1180.562, 1160.561, 1140.562, 1120.562, 1100.562, 1080.563,
        1060.563, 1040.563, 1020.563, 1000.563, 980.5633125, 960.5636875,
        940.564, 920.5641875, 900.564625, 880.5649375, 860.565125, 840.5655,
        820.5658125, 800.5660625, 780.5664375, 760.5666875, 740.5721875, 720.594, 700.6424375, 680.7274375,
        660.8595625, 641.0424375, 621.27, 601.438125, 581.629875, 561.8346875,
        542.041, 522.242, 502.4383125, 482.71178125, 463.05159375, 443.44409375, 423.8785,
        404.348, 384.84121875, 365.34728125, 345.86178125, 326.384, 306.90690625,
        287.4251875, 267.9375, 248.4405, 228.930703125, 209.407796875,
        189.882703125, 170.3681875, 150.8986875, 131.5726875, 112.5046015625, 93.6721796875,
        74.98321875, 56.37054296875, 37.72887890625, 18.937681640625, -0.0, -19.009400390625,
        -38.0523671875, -57.11212109375, -76.181015625, -95.2545078125, -114.3265, -133.389296875,
        -152.43990625, -171.476796875, -190.49990625, -209.515390625, -228.521703125, -247.52040625,
        -266.51, -285.48740625, -304.4525, -323.407, -342.35234375,
        -361.29146875, -380.22921875, -399.1745, -418.13809375, -437.13378125,
        -456.1721875, -475.26246875, -494.41065625
    ]

    # Reverse z axis and convert to cm
    atmo['z'] = atmo['z'][::-1]
    for i in range(len(atmo['z'])):
        atmo['z'][i] = atmo['z'][i] * 1e5
    atmo['check_atmo'] = True
    atmo['check_atom'] = True

    dz = np.array(atmo['z'])
    dz = np.absolute(dz[1:] - dz[:-1])
    atmo['mindz'] = np.amin(dz)

    return atmo

######################################################################


def make_density_matrix(zz, xx, yy, populations, rdim, NL, atom, maxK):
    rho_n = np.zeros((rdim,))
    ridx = 0
    # For each level
    for jj in range(NL):
        J2 = atom['J2'][jj]  # Get angular momentum
        for K in range(maxK[jj]):  # For each K multipole
            # If 0, initilize rho00 with input populations
            if K == 0:
                rho00 = populations[jj] / np.sum(populations) / \
                        np.sqrt(float(J2 + 1))
                rho_n[ridx] = rho00
                ridx += 1

                # If non-zero, initialize to no atomic
                # polarization
            else:

                # For each Q possible for the current K
                for Q in range(K * 2 + 1):
                    rho_n[ridx] = 0.0
                    ridx += 1

    return zz, xx, yy, rho_n


def main():
    ''' Writes a simple pmd file (binary and HDF5 versions) for the multilevel module
    '''

    ###################################
    # Internal function: write_hdf5
    ###################################
    def write_hdf5(outname):
        try:
            import h5py
        except:
            msg = 'HDF5 package required for HDF5 file creation: skipping'
            sys.exit(msg)

        # write_path = Path('/data/harsh/merge_bifrost_output')
        write_path = Path('/home/harsh/BifrostRun/bifrost_supplementary_outputs_using_RH')

        f = h5py.File(write_path / outname, 'w')
        # f = h5py.File(write_path / outname, 'a')

        # PORTA Head #
        # ------------
        f.attrs['PMD_MAGIC'] = np.string_('portapmd')
        f.attrs['PMD_VERSION'] = np.int32([2])

        now = datetime.datetime.now()
        date = np.int32([int(now.year - 1900.), int(now.month - 1.), \
                         int(now.day), int(now.hour), int(now.minute), \
                         int(now.second)])
        f.attrs['CREATION_DATE'] = date
        f.attrs['PERIODICITY'] = np.int32([1, 1])
        f.attrs['DOMAIN_SIZE'] = [Dx, Dy, Dz]
        f.attrs['DOMAIN_ORIGIN'] = [x0, y0, z0]
        f.attrs['GRID_DIMENSIONS'] = np.int32([nx, ny, nz])
        f.attrs['X_AXIS'] = x
        f.attrs['Y_AXIS'] = y
        f.attrs['Z_AXIS'] = z
        f.attrs['POLAR_NODES'] = np.int32([nth])
        f.attrs['AZIMUTH_NODES'] = np.int32([nph])
        f.attrs['MODULE_NAME'] = np.string_(m_name)

        comment = 'Model for ' + atomname + ' atom in ' + \
                  atmoname + ' atmospheric model replicated in ' + \
                  '{0}x{1}'.format(nx, ny) + ' columns for the ' + \
                  m_name + ' module'
        f.attrs['MODULE_COMMENT'] = np.string_(comment)

        Head_size = int(28 + 20 * NL + 8 * NT + 40 * NR + \
                        8 * nx * ny)
        Node_size = int(9 * 8 + NR * 12 * 8 + NL * 8 + NT * 8)
        rdim = 0  # Size (in elements) of density matrix
        for ii in range(NL):
            for K in range(maxK[ii]):
                for Q in range(K * 2 + 1):
                    rdim += 1
        Node_size += rdim * 8

        f.attrs['MODULE_HEADER_SIZE'] = np.int32([Head_size])

        # Not used in PORTA, really
        f.attrs['Size Node'] = np.int32([Node_size])

        # Module head #
        # -------------
        f.create_group('Module')
        #
        f['Module'].attrs['ML_VERSION'] = np.int32([m_version])
        f['Module'].attrs['ATOM_MASS'] = [atom['mass']]
        f['Module'].attrs['NL'] = np.int32([atom['NL']])
        f['Module'].attrs['NT'] = np.int32([atom['NT']])
        f['Module'].attrs['NR'] = np.int32([atom['NR']])
        f['Module'].attrs['E'] = atom['E']
        f['Module'].attrs['GL'] = atom['gL']
        f['Module'].attrs['J2'] = np.int32(atom['J2'])
        f['Module'].attrs['IUL'] = np.int32(atom['iul'])
        f['Module'].attrs['AUL'] = atom['Aul']
        f['Module'].attrs['NFREQ'] = np.int32(atom['nfreq'])
        f['Module'].attrs['NFREQ_C'] = np.int32(atom['nfreqc'])
        f['Module'].attrs['DW'] = atom['Dw']
        f['Module'].attrs['DW_C'] = atom['Dwc']
        f['Module'].attrs['TEMP_REF'] = atom['Tref']
        f['Module'].attrs['K_CUT'] = np.int32([atom['Kcut']])

        # Grid Data #
        # -----------

        # Grid node structured array
        node_dt = np.dtype([ \
            ('T', np.float), \
            ('N', np.float), \
            ('B', np.float, 3), \
            ('v', np.float, 3), \
            ('vmi', np.float), \
            ('rho', np.float, rdim), \
            ('J', np.float, 9 * NR), \
            ('a_voigt', np.float, NR), \
            ('Cul', np.float, NT), \
            ('DK', np.float, NL), \
            ('eta_c', np.float, NR), \
            ('eps_c', np.float, NR) \
            ])

        grid = f['Module'].create_dataset("g_data", (nz, ny, nx), dtype=node_dt)
        # grid = f['Module']['g_data']
        # # For each height
        # for iz in range(nz):
        #
        #     # Populations per level in cm^-3
        #     n_pops = atmo['pops'][iz]
        #     # Total population in cm^-3
        #     n_N = sum(n_pops)
        #     # Temperature in K
        #     n_T = atmo['T'][iz]
        #     # Magnetic field (no magnetic field)
        #     n_B = [Bx, By, Bz]
        #     # Velocity field (no velocity field)
        #     n_v = [0., 0., 0.]
        #     # Microturbulence
        #     n_vmi = atmo['vmi'][iz]
        #     # Damping parameter
        #     n_a = atmo['a_voigt'][iz]
        #     # Collisional rate for deexcitation
        #     n_Cul = atmo['Cul'][iz]
        #     # Depolarizing collisional rate (no elastic collisions)
        #     n_delta2 = [0.0] * NL
        #     # Opacity of the continuum in cm^-1
        #     n_eta_c = atmo['etac'][iz]
        #     # Thermal emissivity of the continuum in erg cm^-3 Hz^-1 s^-1
        #     # str^-1
        #     n_eps_c = atmo['epsc'][iz]

            # For each column to replicate

        atmos_path = Path('/home/harsh/BifrostRun/BIFROST_en024048_hion_snap_385_0_504_0_504_-500000.0_3000000.0.nc')
        # atmos_path = Path('/data/harsh/run_bifrost/Atmos/bifrost_en024048_hion_0_504_0_504_-500000.0_3000000.0.nc')
        suppl_out_path = Path('/home/harsh/BifrostRun_fast_Access/BIFROST_en024048_hion_snap_385_0_504_0_504_-500000.0_3000000.0_supplementary_outputs.nc')
        # suppl_out_path = Path('/data/harsh/merge_bifrost_output/bifrost_output.h5')

        fatmos = h5py.File(atmos_path, 'r')
        fsuppl = h5py.File(suppl_out_path, 'r')

        f['Module/temp_bc'] = fatmos['temperature'][0][nxs+sample][:, nys+sample][:, :, -1]

        grid[:, :, :, 'T'] = np.transpose(fatmos['temperature'][0][nxs+sample][:, nys+sample], axes=(2, 0, 1))[::-1]
        # import ipdb;ipdb.set_trace()
        grid[:, :, :, 'N'] = np.transpose(np.sum(fsuppl['populations'][0, 0:NL][:, nxs+sample][:, :, nys+sample], 0), axes=(2, 0, 1))[::-1] * 1e-6
        B = np.zeros((nz, ny, nx, 3), dtype=np.float64)
        B[:, :, :, 0] = np.transpose(fatmos['B_x'][0][nxs+sample][:, nys+sample], axes=(2, 0, 1))[:: -1] * 1e4
        B[:, :, :, 1] = np.transpose(fatmos['B_y'][0][nxs+sample][:, nys+sample], axes=(2, 0, 1))[:: -1] * 1e4
        B[:, :, :, 2] = np.transpose(fatmos['B_z'][0][nxs+sample][:, nys+sample], axes=(2, 0, 1))[:: -1] * 1e4
        V = np.zeros((nz, ny, nx, 3), dtype=np.float64)
        V[:, :, :, 0] = np.transpose(fatmos['velocity_x'][0][nxs+sample][:, nys+sample], axes=(2, 0, 1))[::-1] * 1e2
        V[:, :, :, 1] = np.transpose(fatmos['velocity_y'][0][nxs+sample][:, nys+sample], axes=(2, 0, 1))[::-1] * 1e2
        V[:, :, :, 2] = np.transpose(fatmos['velocity_z'][0][nxs+sample][:, nys+sample], axes=(2, 0, 1))[::-1] * 1e2
        grid[:, :, :, 'B'] = B
        grid[:, :, :, 'v'] = V

        zzzz, xxxx, yyyy = np.where(np.ones((nz, nx, ny)) > 0)

        print('Just before Rho')

        populations = fsuppl['populations'][()][0, 0:NL][:, nxs+sample][:, :, nys+sample][:, :, :, ::-1]

        k = 0

        with Pool(processes=31) as pool:
            results = list()
            t = tqdm(zip(zzzz, xxxx, yyyy), total=nz * nx * ny)
            for zzz, xxx, yyy in t:
                results.append(
                    pool.apply_async(
                        make_density_matrix,
                        args=(
                            zzz,
                            xxx,
                            yyy,
                            populations[:, xxx, yyy, zzz],
                            rdim, NL, atom, maxK
                        )
                    )
                )
                k += 1
                if k == 20000:
                    for r in results:
                        zz, xx, yy, rho_n = r.get()
                        grid[zz, xx, yy, 'rho'] = rho_n
                    results = list()
                    k = 0
            for r in results:
                zz, xx, yy, rho_n = r.get()
                grid[zz, xx, yy, 'rho'] = rho_n

        print('After Rho')
        # nt_array = np.array(list(set(np.arange(36)) - set([0, 1, 4, 5]))).astype(np.int64)
        # nr_array = np.array([2, 3, 6, 7, 8, 9, 10])
        grid[:, :, :, 'J'] = [0.0] * 9 * NR  # Radiation field tensors (initialize to 0)
        grid[:, :, :, 'a_voigt'] = np.transpose(fsuppl['a_voigt'][0, :][:, nxs+sample][:, :, nys+sample], axes=(3, 1, 2, 0))[::-1]  # Damping parameter
        grid[:, :, :, 'Cul'] = np.transpose(fsuppl['Cul'][0, :][:, nxs+sample][:, :, nys+sample], axes=(3, 1, 2, 0))[::-1]  # Deexcitation rate for inelastic collisions with electrons
        grid[:, :, :, 'DK'] = np.zeros((nz, nx, ny, NL))  # Depolarization rate for elastic collisions with neutral hydrogen
        grid[:, :, :, 'eta_c'] = np.transpose(fsuppl['eta_c'][0, :][:, nxs+sample][:, :, nys+sample], axes=(3, 1, 2, 0))[::-1] * 1e-2  # Continuum opacity at each radiative transition
        grid[:, :, :, 'eps_c'] = np.transpose(fsuppl['eps_c'][0, :][:, nxs+sample][:, :, nys+sample], axes=(3, 1, 2, 0))[::-1] * 10  # Continuum emissivity at each radiative transition

        f.close()
        fatmos.close()
        fsuppl.close()
        # Closing message
        msg = '\nYour pmd file (HDF5 format) for the atom {0} and \n' + \
              '  atmospheric model {1} has been created and stored \n' + \
              '  in the file: {2}'
        print(msg.format(atomname, atmoname, outname))

    # Internal function: write_hdf5 (end)
    ###################################
    # Main function start

    # Set defaults
    outname = 'H_Bifrost_s_385'
    atomname = 'H'
    atmoname = 'Bifrost'
    # Bm = '0'
    # Bt = '0'
    # Ba = '0'
    # fTPerx = -1.0
    # fVPerx = 1.0
    # bPerx = False
    # fTPery = -1.0
    # fVPery = 1.0
    # bPery = False
    # inx = -1
    # iny = -1

    # # Check if old version
    # if sys.version_info[0] < 3:
    #     oldpy = True
    # else:
    #     oldpy = False



    ######################################################################

    # Angular quadrature, nodes per octant
    nth = 4
    nph = 2
    # Minimum value of mu
    mu = np.polynomial.legendre.leggauss(4)[0]
    mu = 0.5 * mu + 0.5
    minmu = np.amin(np.absolute(mu))

    # Module version and name
    m_version = 1
    m_name = 'multilevel'

    # Atom
    atom = get_atom(atomname)
    if not atom['check']:
        msg = 'There was a problem setting the atomic model'
        sys.exit(msg)

    # Atmosphere
    atmo = get_atmo(atmoname, atomname)
    if not atmo['check_atmo']:
        msg = 'There was a problem setting the thermal part of ' + \
              'the atmospheric model'
        sys.exit(msg)
    if not atmo['check_atom']:
        msg = 'There was a problem setting the atomic part of ' + \
              'the atmospheric model'
        sys.exit(msg)

    offset_x = 2
    offset_y = 2
    nxs = 180 + offset_x
    # nxe = 261
    nys = 60 + offset_y
    # nye = 261

    nn = 61
    ns = 3

    sample = np.arange(0, nn * ns, ns).astype(np.int64)

    nx = nn  #nxe - nxs
    ny = nn  #nye - nys

    # If no perturbed and domain not specified
    # if not bPerx and fTPerx < 1e-6:
    #     # It is extended enough so every ray crosses the
    #     # bottom boundary for 1.5D
    #     x0 = 0.0
    #     x1a = float(nx) * atmo['mindz'] * minmu * 1.01 + x0
    #     x1b = np.amax(atmo['z']) - np.min(atmo['z']) + x0
    #     x1 = np.max([x1a, x1b])
    # else:
    #     # The period is the domain, not the difference
    #     # between extremes
    #     fTPerx *= 1e5
    #     x0 = 0.0
    #     x1 = fTPerx * (1.0 - 1.0 / float(nx))
    #
    # # If no perturbed and domain not specified
    # if not bPery and fTPery < 1e-6:
    #     # It is extended enough so every ray crosses the
    #     # bottom boundary for 1.5D
    #     y0 = 0.0
    #     y1a = float(ny) * atmo['mindz'] * minmu * 1.01 + y0
    #     y1b = np.amax(atmo['z']) - np.min(atmo['z']) + y0
    #     y1 = np.max([y1a, y1b])
    # else:
    #     # The period is the domain, not the difference
    #     # between extremes
    #     fTPery *= 1e5
    #     y0 = 0.0
    #     y1 = fTPery * (1.0 - 1.0 / float(ny))

    # Build geometry
    x0 = -((nx-1)//2) * 48e5 * ns
    x1 = ((nx - 1)//2) * 48e5 * ns
    x = np.linspace(x0, x1, nx, endpoint=True)
    Dx = x[-1] + x[1] - 2. * x[0]
    y0 = -((ny-1)//2) * 48e5 * ns
    y1 = ((ny - 1)//2) * 48e5 * ns
    y = np.linspace(y0, y1, ny, endpoint=True)
    Dy = y[-1] + y[1] - 2. * y[0]
    z = atmo['z']
    nz = atmo['nz']
    z0 = z[0]
    Dz = z[-1] - z[0]
    print (x)
    print (Dx)
    print (y)
    print (Dy)

    # # Check correct size if perturbed
    # if bPerx:
    #     if np.absolute(Dx - fTPerx) > 1e-6:
    #         msg = 'Something went wrong defining the domain'
    #         msg += '{0} != {1}'.format(Dx, fTPerx)
    #         sys.exit(msg)
    #     fTPerx = 2.0 * np.pi / fTPerx
    # if bPery:
    #     if np.absolute(Dy - fTPery) > 1e-6:
    #         msg = 'Something went wrong defining the domain'
    #         msg += '{0} != {1}'.format(Dy, fTPery)
    #         sys.exit(msg)
    #     fTPery = 2.0 * np.pi / fTPery

    # Get size module name
    m_nsize = len(list(m_name))

    # Short for some dictionary quantities
    NL = atom['NL']
    NT = atom['NT']
    NR = atom['NR']

    # Compute maximum K for each level
    Kcut = atom['Kcut']
    if Kcut < 0:
        lKcut = 2000
    else:
        lKcut = Kcut
    maxK = []
    for ii in range(NL):
        J2 = atom['J2'][ii]
        maxK.append(np.min([lKcut, J2]) + 1)

    # Transform energies into erg
    for i in range(len(atom['E'])):
        atom['E'][i] = atom['E'][i] * 6.62606896e-27 * 299792458e2

    # # Transform indexes into C/python
    # for i in range(len(atom['iul'])):
    #     atom['iul'][i] = atom['iul'][i] - 1

    # write_hdf5('{}_{}_{}_{}_{}.h5'.format(outname, nxs, nxe, nys, nye))
    write_hdf5('{}_{}_{}_{}_{}.h5'.format(outname, nxs, nys, nn, ns))

######################################################################

if __name__ == "__main__":
    main()
