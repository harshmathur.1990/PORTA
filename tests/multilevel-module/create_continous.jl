using HDF5
using Dates
using ProgressBars

bifrost_indice = range(start=1, stop=467, step=1)
# bifrost_indice = range(start=1, stop=2, step=1)


function generate_radiative_transitions()
    transitions = [3, 0, 1, 0, 5, 1, 5, 3, 7, 0, 4, 0, 7, 2, 4, 2, 6, 1, 8, 3, 6, 3]

    return transitions
end


function collisional_transitions()
    collisional_transitions = Vector{Int32}()

    radiative_transitions = generate_radiative_transitions()

    for i in range(start=0, stop=8, step=1)
        for j in range(start=0, stop=8, step=1)
            if i <= j
                continue
            end

            rad_p = false

            k = 1

            while k <= length(radiative_transitions)
                if radiative_transitions[k] == i && radiative_transitions[k + 1] == j
                    rad_p = true
                    break
                end
                k += 2
            end

            if rad_p == false
                push!(collisional_transitions, i)
                push!(collisional_transitions, j)
            end
        
        end

    end


    return collisional_transitions

end


function non_zero_col_transitions()
    iul_to_n_mapper = [1, 2, 2, 2, 3, 3, 3, 3, 3]

    non_zero_col_transitions_r = Vector{Int32}()

    total_transitions = reduce(vcat, [generate_radiative_transitions(), collisional_transitions()])

    total_transitions = transpose(reshape(total_transitions, (2, div(length(total_transitions), 2))))

    for transition in eachrow(total_transitions)
        if iul_to_n_mapper[transition[1] + 1] != iul_to_n_mapper[transition[2] + 1]
            push!(non_zero_col_transitions_r, transition[1])
            push!(non_zero_col_transitions_r, transition[2])
        end

    end

    return non_zero_col_transitions_r

end


function non_zero_col_transitions_indices()
    iul_to_n_mapper = [1, 2, 2, 2, 3, 3, 3, 3, 3]

    non_zero_col_transitions_r = Vector{Int32}()

    total_transitions = reduce(vcat, [generate_radiative_transitions(), collisional_transitions()])

    total_transitions = transpose(reshape(total_transitions, (2, div(length(total_transitions), 2))))

    index = 1
    for transition in eachrow(total_transitions)
        if iul_to_n_mapper[transition[1] + 1] != iul_to_n_mapper[transition[2] + 1]
            push!(non_zero_col_transitions_r, index)
        end
        index += 1

    end

    return non_zero_col_transitions_r

end


function get_atom()

    atom = Dict{String,Any}(
        "mass" => 1.00784,
        "NL" => 9,
        "NT" => 23,
        "NR" => 11,
        "E" => [0, 82258.918314503884176, 82258.953600449603982, 82259.284202523806016, 97492.210253263619961, 97492.220754027264775, 97492.318486079660943, 97492.318663854093757, 97492.354619285237277],
        "gL" =>  [2.0, 0.667, 2.0, 1.333, 0.667, 2.0, 0.8, 1.333, 1.2],
        "J2" => [1, 1, 1, 3, 1, 1, 3, 3, 5],
        "iul" => non_zero_col_transitions(),
        "Aul" => [6.2648e8, 6.2649e8, 2.1046e6, 4.2097e6, 1.6725e8, 1.6725e8, 2.2448e7, 2.2449e7, 5.3877e7, 6.4651e7, 1.0775e7],
        "nfreq" => [21, 21, 151, 151, 21, 21, 151, 151, 151, 151, 151],
        "nfreqc" => [11, 11, 71, 71, 11, 11, 71, 71, 71, 71, 71],
        "Dw" => [100, 100, 3000, 3000, 100, 100, 3000, 3000, 3000, 3000, 3000],
        "Dwc" => [10, 10, 50, 50, 10, 10, 50, 50, 50, 50, 50],
        "Tref" => [5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3],
        "Kcut" => -1,
        "check" => true
    )

    return atom

end


function get_atmo()

    total_z = reverse([14367.05      , 14269.9       , 14173.14      , 14076.769     ,
       13980.78      , 13885.17      , 13789.941     , 13695.1       ,
       13600.63      , 13506.529     , 13412.821     , 13319.47      ,
       13226.5       , 13133.9       , 13041.66      , 12949.8       ,
       12858.3       , 12767.16      , 12676.389     , 12585.98      ,
       12495.92      , 12406.23      , 12316.899     , 12227.92      ,
       12139.299     , 12051.02      , 11963.11      , 11875.539     ,
       11788.32      , 11701.44      , 11614.92      , 11528.73      ,
       11442.899     , 11357.4       , 11272.24      , 11187.43      ,
       11102.95      , 11018.811     , 10935.        , 10851.53      ,
       10768.389     , 10685.58      , 10603.1       , 10520.95      ,
       10439.12      , 10357.63      , 10276.45      , 10195.6       ,
       10115.08      , 10034.87      ,  9955.207     ,  9876.491     ,
        9798.705     ,  9721.841     ,  9645.89      ,  9570.84      ,
        9496.678     ,  9423.398     ,  9350.985     ,  9279.431     ,
        9208.725     ,  9138.858     ,  9069.819     ,  9001.599     ,
        8934.189     ,  8867.578     ,  8801.756     ,  8736.714     ,
        8672.444     ,  8608.935     ,  8546.181     ,  8484.171     ,
        8422.895     ,  8362.3455    ,  8302.516     ,  8243.395     ,
        8184.973     ,  8127.246     ,  8070.203     ,  8013.836     ,
        7958.138     ,  7903.099     ,  7848.7135    ,  7794.974     ,
        7741.872     ,  7689.397     ,  7637.546     ,  7586.311     ,
        7535.681     ,  7485.653     ,  7436.218     ,  7387.3685    ,
        7339.1       ,  7291.401     ,  7244.269     ,  7197.6955    ,
        7151.6755    ,  7106.2       ,  7061.264     ,  7016.86      ,
        6972.985     ,  6929.627     ,  6886.786     ,  6844.452     ,
        6802.62      ,  6761.283     ,  6720.4375    ,  6680.0765    ,
        6640.194     ,  6600.784     ,  6561.841     ,  6523.36      ,
        6485.335     ,  6447.761     ,  6410.633     ,  6373.945     ,
        6337.691     ,  6301.868     ,  6266.47      ,  6231.491     ,
        6196.9275    ,  6162.773     ,  6129.0255    ,  6095.676     ,
        6062.7215    ,  6030.1605    ,  5997.9825    ,  5966.188     ,
        5934.77      ,  5903.726     ,  5873.048     ,  5842.734     ,
        5812.781     ,  5783.182     ,  5753.935     ,  5725.034     ,
        5696.475     ,  5668.256     ,  5640.372     ,  5612.816     ,
        5585.589     ,  5558.6845    ,  5532.0965    ,  5505.827     ,
        5479.868     ,  5454.2165    ,  5428.8685    ,  5403.823     ,
        5379.073     ,  5354.617     ,  5330.451     ,  5306.571     ,
        5282.976     ,  5259.659     ,  5236.618     ,  5213.851     ,
        5191.355     ,  5169.124     ,  5147.158     ,  5125.452     ,
        5104.003     ,  5082.809     ,  5061.866     ,  5041.171     ,
        5020.721     ,  5000.515     ,  4980.514     ,  4960.514     ,
        4940.515     ,  4920.515     ,  4900.515     ,  4880.515     ,
        4860.515     ,  4840.516     ,  4820.516     ,  4800.517     ,
        4780.517     ,  4760.517     ,  4740.518     ,  4720.518     ,
        4700.518     ,  4680.518     ,  4660.519     ,  4640.519     ,
        4620.519     ,  4600.52      ,  4580.52      ,  4560.52      ,
        4540.52      ,  4520.521     ,  4500.521     ,  4480.52      ,
        4460.521     ,  4440.521     ,  4420.521     ,  4400.522     ,
        4380.522     ,  4360.522     ,  4340.523     ,  4320.523     ,
        4300.523     ,  4280.524     ,  4260.524     ,  4240.524     ,
        4220.525     ,  4200.526     ,  4180.52575   ,  4160.52575   ,
        4140.52675   ,  4120.52675   ,  4100.52675   ,  4080.52675   ,
        4060.52775   ,  4040.52775   ,  4020.52775   ,  4000.52875   ,
        3980.527     ,  3960.52875   ,  3940.52875   ,  3920.529     ,
        3900.529     ,  3880.53      ,  3860.53      ,  3840.53      ,
        3820.53      ,  3800.53      ,  3780.53      ,  3760.531     ,
        3740.531     ,  3720.53075   ,  3700.53175   ,  3680.53175   ,
        3660.53175   ,  3640.53325   ,  3620.533     ,  3600.533     ,
        3580.534     ,  3560.533     ,  3540.534     ,  3520.534     ,
        3500.534     ,  3480.534     ,  3460.535     ,  3440.535     ,
        3420.535     ,  3400.536     ,  3380.536     ,  3360.536     ,
        3340.53675   ,  3320.536     ,  3300.536     ,  3280.537     ,
        3260.537     ,  3240.537     ,  3220.537     ,  3200.538     ,
        3180.538     ,  3160.538     ,  3140.539     ,  3120.539     ,
        3100.539     ,  3080.53975   ,  3060.53875   ,  3040.53875   ,
        3020.53975   ,  3000.54025   ,  2980.54      ,  2960.541     ,
        2940.541     ,  2920.542     ,  2900.542     ,  2880.543     ,
        2860.543     ,  2840.543     ,  2820.544     ,  2800.544     ,
        2780.544     ,  2760.544     ,  2740.544     ,  2720.545     ,
        2700.54475   ,  2680.545     ,  2660.545     ,  2640.546     ,
        2620.546     ,  2600.546     ,  2580.547     ,  2560.547     ,
        2540.547     ,  2520.547     ,  2500.547     ,  2480.547     ,
        2460.547     ,  2440.548     ,  2420.548     ,  2400.548     ,
        2380.549     ,  2360.549     ,  2340.549     ,  2320.55      ,
        2300.55      ,  2280.55      ,  2260.55      ,  2240.55      ,
        2220.551     ,  2200.551     ,  2180.551     ,  2160.551     ,
        2140.552     ,  2120.552     ,  2100.552     ,  2080.553     ,
        2060.553125  ,  2040.553125  ,  2020.553125  ,  2000.552875  ,
        1980.552875  ,  1960.554125  ,  1940.553     ,  1920.554     ,
        1900.554     ,  1880.554     ,  1860.555     ,  1840.556     ,
        1820.556     ,  1800.556     ,  1780.556     ,  1760.556     ,
        1740.556     ,  1720.557     ,  1700.557     ,  1680.556875  ,
        1660.556875  ,  1640.557     ,  1620.557     ,  1600.558     ,
        1580.558     ,  1560.558     ,  1540.559     ,  1520.557875  ,
        1500.558     ,  1480.559     ,  1460.559     ,  1440.559     ,
        1420.56      ,  1400.558875  ,  1380.56      ,  1360.56      ,
        1340.56      ,  1320.56      ,  1300.561     ,  1280.56      ,
        1260.561     ,  1240.561     ,  1220.561     ,  1200.562     ,
        1180.562     ,  1160.561     ,  1140.562     ,  1120.562     ,
        1100.562     ,  1080.563     ,  1060.563     ,  1040.563     ,
        1020.563     ,  1000.563     ,   980.5633125 ,   960.5636875 ,
         940.564     ,   920.5641875 ,   900.564625  ,   880.5649375 ,
         860.565125  ,   840.5655    ,   820.5658125 ,   800.5660625 ,
         780.5664375 ,   760.5666875 ,   740.5721875 ,   720.594     ,
         700.6424375 ,   680.7274375 ,   660.8595625 ,   641.0424375 ,
         621.27      ,   601.438125  ,   581.629875  ,   561.8346875 ,
         542.041     ,   522.242     ,   502.4383125 ,   482.71178125,
         463.05159375,   443.44409375,   423.8785    ,   404.348     ,
         384.84121875,   365.34728125,   345.86178125,   326.384     ,
         306.90690625,   287.4251875 ,   267.9375    ,   248.4405    ,
         228.93070313,   209.40779688,   189.88270313,   170.3681875 ,
         150.8986875 ,   131.5726875 ,   112.50460156,    93.67217969,
          74.98321875,    56.37054297,    37.72887891,    18.93768164,
          -0.        ,   -19.00940039,   -38.05236719,   -57.11212109,
         -76.18101563,   -95.25450781,  -114.3265    ,  -133.38929688,
        -152.43990625,  -171.47679687,  -190.49990625,  -209.51539063,
        -228.52170312,  -247.52040625,  -266.51      ,  -285.48740625,
        -304.4525    ,  -323.407     ,  -342.35234375,  -361.29146875,
        -380.22921875,  -399.1745    ,  -418.13809375,  -437.13378125,
        -456.1721875 ,  -475.26246875,  -494.41065625,  -513.62609375,
        -532.9193125 ,  -552.3025    ,  -571.792625  ,  -591.4046875 ,
        -611.1571875 ,  -631.07      ,  -651.1641875 ,  -671.4645    ,
        -691.9981875 ,  -712.792875  ,  -733.877625  ,  -755.278     ,
        -777.0218125 ,  -799.137875  ,  -821.6568125 ,  -844.613625  ,
        -868.0465625 ,  -891.9935625 ,  -916.4961875 ,  -941.5985    ,
        -967.3476875 ,  -993.7958125 , -1020.996     ][bifrost_indice]) * 1e5

    mindz = minimum(broadcast(abs, total_z[2:length(bifrost_indice)] - total_z[1:length(bifrost_indice)-1]))
    
    atmo = Dict{String, Any}(
        "nz" => length(bifrost_indice),
        "z" => total_z,
        "mindz" => mindz
    )

end



function display_time(seconds)


    intervals = [
        ("weeks", 604800),  # 60 * 60 * 24 * 7
        ("days", 86400),    # 60 * 60 * 24
        ("hours", 3600),    # 60 * 60
        ("minutes", 60),
        ("seconds", 1),
    ]

    result = []

    for nc in intervals
        name, count = nc
        value = div(seconds, count)
        if value > 0
            seconds -= value * count
            if value == 1
                name = rstrip(name, ['s'])
            end
            push!(result, "$value $name")
        end
    end
    return join(result, ' ')
end


# function prepare_write_bulk(f, batch_size=1000, num_elements=333)
#     buffer = Vector{Float64}(undef, batch_size * num_elements)

#     filled = 0

#     function flush_to_disk()
#         write(f, Array{Float64}(buffer[1:filled * num_elements]))
#     end

#     function write_to_buffer(an_array)
#         buffer[(filled * num_elements) + 1: (filled + 1) * num_elements] .= an_array
#         filled += 1
#         if filled == batch_size
#             flush_to_disk()
#             filled = 0
#         end
#     end

#     return write_to_buffer, flush_to_disk
# end


function main()

    function write_pmd(outname)

        fast_path = "/home/harsh/BifrostRun_fast_Access"
        write_path = "/run/media/harsh/5de85c60-8e85-4cc8-89e6-c74a83454760"
        f = open(write_path * "/" * outname, "w", lock=false)

        atmos_path = fast_path * "/BIFROST_en024048_hion_snap_385_0_504_0_504_-1020996.0_15000000.0.nc"
        suppl_out_path = fast_path * "/MULTI3D_BIFROST_en024048_hion_snap_385_0_504_0_504_-1020996.0_15000000.0_supplementary_outputs.nc"
        pb_rate_path = fast_path * "/MULTI3D_BIFROST_en024048_hion_snap_385_0_504_0_504_-1020996.0_15000000.0_pb_rates.nc"

        fatmos = h5open(atmos_path, "r")
        fsuppl = h5open(suppl_out_path, "r")
        pb_rates = h5open(pb_rate_path, "r")

        written = 0

        written += write(f, String("portapmd"))
        println("portapmd $written")

        written += write(f, Char(0))
        println("Endianness $written")
        
        written += write(f, Char(4))
        println("Integer size $written")
        
        written += write(f, Char(8))
        println("Double size $written")
        
        written += write(f, Int32(2))
        println("PMD version $written")

        # date_now = now()

        # date = [
        #     Int32(year(date_now) - 1900.),
        #     Int32(month(date_now) - 1.),
        #     Int32(day(date_now)),
        #     Int32(hour(date_now)),
        #     Int32(minute(date_now)),
        #     Int32(second(date_now))
        # ]

        date = [
            Int32(2022 - 1900.),
            Int32(12 - 1.),
            Int32(25),
            Int32(19),
            Int32(20),
            Int32(25)
        ]

        written += write(f, Array{Int32}(date))
        println("Date $written")

        written += write(f, Char(1))
        println("X Periodicity $written")

        written += write(f, Char(1))
        println("Y periodicity $written")

        written += write(f, Array{Float64}([Dx, Dy, Dz]))
        println("Domain size $written")

        written += write(f, Array{Float64}([x0, y0, z0]))
        println("Domain origin $written")

        written += write(f, Array{Int32}([nx,ny,nz]))
        println("Grid dimension $written")

        written += write(f, Array{Float64}(x))
        println("X-axis $written")

        written += write(f, Array{Float64}(fill(0, 8192-nx)))
        println("X-axis zero padding $written")

        written += write(f, Array{Float64}(y))
        println("Y-axis $written")

        written += write(f, Array{Float64}(fill(0, 8192-ny)))
        println("Y-axis zero padding $written")

        written += write(f, Array{Float64}(z))
        println("Z-axis $written")

        written += write(f, Array{Float64}(fill(0, 8192-nz)))
        println("Z-axis zero padding $written")

        written += write(f, Int32(nth))
        println("Polar nodes $written")

        written += write(f, Int32(nph))
        println("Azimuth nodes $written")

        written += write(f, String(m_name))
        println("Module name $written")

        written += write(f, String(repeat("\x00", 1023 - length(m_name))))
        println("Module name zero padding $written")

        comment = "Model for " * "$atomname" * " atom in " * "$atmoname" * " atmospheric model replicated in " * "$nx" * "x" * "$ny" * " columns for the " * "$m_name" * " module"

        written += write(f, String(comment))
        println("Comment $written")

        written += write(f, String(repeat("\0", 4096 - length(comment))))
        println("Comment zero padding $written")

        Head_size = Int32(28 + 20 * NL + 8 * NT + 40 * NR + 8 * nx * ny)
        Node_size = Int32(9 * 8 + NR * 12 * 8 + NL * 8 + NT * 8)

        println("Head size: $Head_size , Node size: $Node_size")

        for ii in range(start=0, stop=NL-1, step=1)
            maxkii = maxK[ii + 1]
            println("$ii - $maxkii")
            for K in range(start=0, stop=maxK[ii + 1]-1, step=1)
                for Q in range(start=0, stop=K * 2, step=1)
                    Node_size += 8
                end
            end
        end

        println("Head size: $Head_size , Node size: $Node_size")

        written += write(f, Int32(Head_size))
        println("Head size $written")

        written += write(f, Int32(Node_size))
        println("Node size $written")

        written += write(f, Int32(m_version))
        println("Module version $written")

        # Atomic mass in AMU
        written += write(f, Float64(atom["mass"]))
        println("Atom mass $written")

        # Number of atomic levels
        written += write(f, Int32(atom["NL"]))
        println("NL $written")

        # Number of transitions (radiative and forbidden collisions)
        written += write(f, Int32(atom["NT"]))
        println("NT $written")

        # Number of transitions (radiative)
        written += write(f, Int32(atom["NR"]))
        println("NR $written")

        # Energy of atomic levels in erg
        written += write(f, Array{Float64}(atom["E"]))
        println("E $written")

        # Land\"e factors of atomic levels
        written += write(f, Array{Float64}(atom["gL"]))
        println("gL $written")

        # Angular momentum of atomic levels multiplied by two
        written += write(f, Array{Int32}(atom["J2"]))
        println("J2 $written")

        # Pairs of upper-lower indexes for each transition. C notation
        written += write(f, Array{Int32}(atom["iul"]))
        println("iul $written")

        # Einstein coefficient for spontaneous emission for each
        # radiative transition
        written += write(f, Array{Float64}(atom["Aul"]))
        println("Aul $written")

        # Number of frequencies for each transition (must be odd and
        # larger than nfreqc)
        written += write(f, Array{Int32}(atom["nfreq"]))
        println("nfreq $written")

        # Number of frequencies for the core of each transition (must
        # be odd)
        written += write(f, Array{Int32}(atom["nfreqc"]))
        println("nfreqc $written")

        # Doppler widths to cover with each transition (must be larger
        # or equal to Dwc)
        written += write(f, Array{Float64}(atom["Dw"]))
        println("Dw $written")

        # Doppler widths to cover with the core of each transition
        written += write(f, Array{Float64}(atom["Dwc"]))
        println("Dwc $written")

        # Reference temperature that determines the Doppler width to
        # build the frequency axis
        written += write(f, Array{Float64}(atom["Tref"]))
        println("Tref $written")

        # Maximum K multipole order to take into account. negative
        # means unlimited
        written += write(f, Int32(atom["Kcut"]))
        println("Kcut $written")

        # Temperature at the bottom layer

        for iy in range(start=1, stop=ny, step=1)
            for ix in range(start=1, stop=nx, step=1)
                written += write(f, Float64(fatmos["temperature"][last(bifrost_indice), ix, iy, 1]))
            end
        end

        println("Temp bc $written")

        # println("Printed $written bytes in header")

        total_progress = 0

        total = 0

        start_time = time()

        previous_time = time()

        NPIX = nx * ny * nz

        nzi = non_zero_col_transitions_indices()

        # write_to_buffer, flush_to_disk = prepare_write_bulk(f, 50000, 333)
        # kindex = 0
        for iz in range(start=bifrost_indice[1], stop=last(bifrost_indice), step=1)

            for iy in range(start=nys, stop=nye, step=sampling)

                for ix in range(start=nxs, stop=nxe, step=sampling)

                    written = 0

                    written += write(f, Float64(fatmos["temperature"][last(bifrost_indice) - iz + 1, ix, iy, 1]))
                    # buffer[index + 1] = temp
                    # index += 1


                    # # Write density
                    written += write(f, Float64(sum(fsuppl["populations"][last(bifrost_indice) - iz + 1, ix, iy, 1:9, 1]) * 1e-6))
                    # buffer[index + 1] = density
                    # index += 1

                    # Write magnetic field
                    n_B = [
                        fatmos["B_x"][last(bifrost_indice) - iz + 1, ix, iy, 1] * 1e4,
                        fatmos["B_y"][last(bifrost_indice) - iz + 1, ix, iy, 1] * 1e4,
                        fatmos["B_z"][last(bifrost_indice) - iz + 1, ix, iy, 1] * 1e4
                    ]
                    written += write(f, Array{Float64}(n_B))
                    # buffer[index+1:index+1+2] .= n_B
                    # index += 3

                    # Write velocity field
                    n_v = [
                        fatmos["velocity_x"][last(bifrost_indice) - iz + 1, ix, iy, 1] * 1e2,
                        fatmos["velocity_y"][last(bifrost_indice) - iz + 1, ix, iy, 1] * 1e2,
                        fatmos["velocity_z"][last(bifrost_indice) - iz + 1, ix, iy, 1] * 1e2
                    ]
                    written += write(f, Array{Float64}(n_v))
                    # buffer[index+1:index+1+2] .= n_v
                    # index += 3

                    # Write microturbulence
                    written += write(f, Float64(0))
                    # buffer[index + 1] = 0.0
                    # index += 1

                    # Initialize number of written doubles
                    kk = 9

                    # For each level
                    for jj in range(start=0, stop=NL - 1, step=1)

                        # Get angular momentum
                        J2 = atom["J2"][jj + 1]

                        # For each K multipole
                        for K in range(start=0, stop=maxK[jj + 1] - 1, step=1)

                            # If 0, initilize rho00 with input populations
                            if K == 0
                                rho00 = fsuppl["populations"][last(bifrost_indice) - iz + 1, ix, iy, jj + 1, 1] / (sum(fsuppl["populations"][last(bifrost_indice) - iz + 1, ix, iy, 1:9, 1])) / sqrt(Float64(J2 + 1))
                                written += write(f, Float64(rho00))
                                # buffer[index + 1] = rho00
                                # index += 1
                                kk += 1

                            # If non-zero, initialize to no atomic
                            # polarization
                            else

                                # For each Q possible for the current K
                                for Q in range(start=0, stop=K * 2, step=1)
                                    written += write(f, Float64(0.0))
                                    # buffer[index + 1] = 0.0
                                    # index += 1
                                    kk += 1
                                end
                            end
                        end
                    end

                    # Number of written doubles not counting the first 9
                    mm = kk - 9

                    # Write radiation field tensors (initialize to 0)
                    written += write(f, Array{Float64}(fill(0.0, 9 * NR)))
                    # buffer[index + 1:index + 1 + (9 * NR) - 1] .= 0.0
                    # index += 9 * NR
                    kk += 9 * NR

                    # Write damping parameter
                    written += write(f, Array{Float64}(fsuppl["a_voigt"][last(bifrost_indice) - iz + 1, ix, iy, :, 1]))
                    # buffer[index + 1:index + 1 + NR - 1] .= reverse(fsuppl["a_voigt"][bifrost_indice, iy, ix, :, 1], dims=1)[iz, :]
                    # index += NR
                    kk += NR

                    # Write deexcitation rate for inelastic collisions
                    # with electrons
                    written += write(f, Array{Float64}(pb_rates["Cul_pb_rates"][:, last(bifrost_indice) - iz + 1, ix, iy][nzi, :]))
                    # buffer[index + 1: index + 1 + NT - 1] .= reverse(pb_rates["Cul_pb_rates"][:, bifrost_indice, iy, ix][nzi, :], dims=2)[:, iz]
                    # index += NT
                    kk += NT

                    # Write depolarization rate for elastic collisions
                    # with neutral hydrogen
                    written += write(f, Array{Float64}(fill(0.0, NL)))
                    # buffer[index + 1: index + 1 + NL - 1] .= 0
                    # index += NL
                    kk += NL

                    # Write continuum opacity at each radiative transition
                    written += write(f, Array{Float64}(fsuppl["eta_c"][last(bifrost_indice) - iz + 1, ix, iy, :, 1] * 1e-2))
                    # buffer[index + 1:index + 1 + NR - 1] .= reverse(fsuppl["eta_c"][bifrost_indice, iy, ix, :, 1], dims=1)[iz, :] * 1e-2
                    # index += NR
                    kk += NR

                    # Write continuum emissivity at each radiative transition
                    written += write(f, Array{Float64}(fsuppl["eps_c"][last(bifrost_indice) - iz + 1, ix, iy, :, 1] * 10))
                    # buffer[index + 1: index + 1 + NR - 1] .= reverse(fsuppl["eps_c"][bifrost_indice, iy, ix, :, 1], dims=1)[iz, :] * 10
                    # index += NR
                    kk += NR

                    tb = kk * 8
                    mmb = mm * 8
                    # If first grid node, notify how much was written
                    if iz == 1 && iy == 1 && ix == 1
                        msg = "Node size $kk elements and $tb bytes. Written $written bytes. Expected $Node_size bytes"
                        println(msg)
                        msg = "Size of density matrix is $mm elements and $mmb bytes"
                        println(msg)
                    end

                    # write_to_buffer(buffer)

                    # kindex += 1
                    # println("Kindex $kindex")

                    total += 1
                    if total % 1000 == 0
                        cur_time = time()
                        elapsed = cur_time - start_time
                        current_progress = round(total * 100 / NPIX, sigdigits = 2)
                        speed = round(total / (cur_time - start_time), sigdigits = 2)
                        estimsted_time = (cur_time - start_time) * (NPIX - total) / total
                        previous_time = cur_time
                        est = display_time(estimsted_time)
                        print("Iterations: $total / $NPIX Speed: $speed it/s Progress: $current_progress Estimated Time: $est \r")
                        flush(stdout)
                    end
                end
            end
        end
        # flush_to_disk()
        close(f)
        close(fatmos)
        close(fsuppl)
        close(pb_rates)


    end

    outname = "MULTI3D_H_Bifrost_s_385"
    atomname = "H"
    atmoname = "Bifrost"

    m_version = 1
    m_name = "multilevel"

    atom = get_atom()

    atmo = get_atmo()

    nth = 4
    nph = 2

    sampling = 3
    offset_x = 1
    offset_y = 1
    npix_x = 3
    npix_y = 3

    nxs = 1
    nxe = nxs + (npix_x - 1) * sampling + offset_x - 1
    nys = 1
    nye = nys + (npix_y - 1) * sampling + offset_y - 1

    nx = npix_x
    ny = npix_y

    x0 = -(div(nx - 1, 2)) * sampling * 48e5
    x1 = (div(nx - 1, 2)) * sampling * 48e5
    x = LinRange(x0, x1, nx)
    Dx = last(x) + x[2] - 2 * x[1]
    y0 = -(div(ny - 1, 2)) * sampling * 48e5
    y1 = (div(ny - 1, 2)) * sampling * 48e5
    y = LinRange(y0, y1, ny)
    Dy = last(y) + y[2] - 2 * y[1]
    z = atmo["z"]
    nz = atmo["nz"]
    z0 = z[1]
    Dz = last(z) - z[1]
    # println(x)
    # println(Dx)
    # println(y)
    # println(Dy)

    m_nsize = length(m_name)

    NL = atom["NL"]
    NT = atom["NT"]
    NR = atom["NR"]

    Kcut = atom["Kcut"]

    if Kcut < 0
        lKcut = 2000
    else
        lKcut = Kcut
    end

    maxK = Vector{Int32}()
    for ii in range(start=0, stop=NL - 1, step=1)
        J2 = atom["J2"][ii + 1]
        push!(maxK, min(lKcut, J2) + 1)
    end

    atom["E"] = atom["E"] * 6.62606896e-27 * 299792458e2

    write_pmd("$outname" * "_" * "$nxs" * "_" * "$nxe" * "_" * "$nys" * "_" * "$nye" * ".pmd")
end




main()
