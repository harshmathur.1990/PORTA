#!/usr/bin/env python
# -*- coding: utf-8 -*-

#####################
# multilevel-pmd.py
#
# Tanausú del Pino Alemán
# Ángel de Vicente
#
# Writes a simple pmd file to test the multilevel module
#
#####################
#
# 01/21/2020 - Added the extention modifier to the filename check
#              in main (AdV)

# 12/03/2019 - Added routine to create the file in HDF5 format,
#              so if the package h5py is found both binary and
#              HDF5 versions are created (AdV)
#
# 11/26/2019 - Added homogeneous magnetic field, with strength,
#              inclination and azimuth (TdPA)
#            - Bugfix: when interpreting the atmosphere option
#              the code was working with the atomic variables (TdPA)
#
# 06/21/2019 - Added microturbulence, removed alternative calcium,
#              changed the horizontal axis definition to make it as
#              extended as needed given the model and the angular
#              quadrature or makes the model a cube (TdPA)
#
# 06/19/2019 - Added an alternative Calcium initialization, it was for
#              local testing (TdPA)
#
#####################

import h5py
import sys, math, os, struct, copy
import numpy as np
import datetime
from pathlib import Path
from multiprocessing import Pool
from tqdm import tqdm
from scipy.interpolate import CubicSpline


######################################################################


# bifrost_indice = np.array([  0,   3,   5,   8,  11,  13,  16,  18,  21,  24,  26,  29,  32,
#         34,  37,  39,  42,  45,  47,  50,  53,  55,  58,  60,  63,  66,
#         68,  71,  74,  76,  79,  82,  84,  87,  89,  92,  95,  97, 100,
#        103, 105, 108, 110, 113, 116, 118, 121, 124, 126, 129, 132, 134,
#        137, 139, 142, 145, 147, 150, 153, 155, 158, 160, 163, 166, 168,
#        171, 174, 176, 179, 181, 184, 187, 189, 192, 195, 197, 200, 203,
#        205, 208, 210, 213, 216, 218, 221, 224, 226, 229, 231, 234, 237,
#        239, 242, 245, 247, 250, 252, 255, 258, 260, 263, 266, 268, 271,
#        274, 276, 279, 281, 284, 287, 289, 292, 295, 297, 300, 302, 305,
#        308, 310, 313, 316, 318, 321, 323, 326, 329, 331, 334, 337, 339,
#        342, 345, 347, 350, 352, 355, 358, 360, 363, 366, 368, 371, 373,
#        376, 379, 381, 384, 387, 389, 392, 394, 397, 400, 402, 405, 408,
#        410, 413, 418, 421, 423, 426, 429, 431, 434, 436, 439, 442, 444,
#        447, 449, 452, 455, 457, 460, 462, 465])


# bifrost_indice = np.arange(76, 434)
bifrost_indice = np.arange(0, 467)
# bifrost_indice = np.arange(0, 2)

def generate_radiative_transitions():
    transitions = [3, 0, 1, 0, 5, 1, 5, 3, 7, 0, 4, 0, 7, 2, 4, 2, 6, 1, 8, 3, 6, 3]

    # transitions = [5, 1, 5, 3, 7, 2, 4, 2, 6, 1, 8, 3, 6, 3]

    return transitions


def collisional_transitions():
    collisional_transitions = list()

    radiative_transitions = generate_radiative_transitions()
    for i in range(9):
        for j in range(9):
            if i <= j:
                continue

            rad_p = False

            k = 0
            while k < len(radiative_transitions):
                if radiative_transitions[k] == i and radiative_transitions[k + 1] == j:
                    rad_p = True
                    break
                k += 2

            if rad_p is False:
                collisional_transitions.append(i)
                collisional_transitions.append(j)

    return collisional_transitions


def non_zero_col_index():
    iul_to_n_mapper = np.array([1, 2, 2, 2, 3, 3, 3, 3, 3])

    non_zero_col_transitions = list()

    total_transitions = np.array(generate_radiative_transitions() + collisional_transitions())

    total_transitions = total_transitions.reshape(total_transitions.shape[0]//2, 2)

    for index, transition in enumerate(total_transitions):
        if iul_to_n_mapper[transition[0]] != iul_to_n_mapper[transition[1]]:
            non_zero_col_transitions.append(index)

    return np.array(non_zero_col_transitions)


def get_atom(atomname):
    ''' Configures the atomic model
    '''

    atom = {'check': False}

    if atomname == 'H':

        # Atomic mass in AMU
        atom['mass'] = 1.00784
        # Number of levels
        atom['NL'] = 9
        # Number of transitions (radiative + forbidden collisional)
        atom['NT'] = 23
        # Number of transitions (radiative)
        atom['NR'] = 11
        # Energy of atomic levels in cm^-1
        atom['E'] = [0, 82258.918314503884176, 82258.953600449603982, 82259.284202523806016, 97492.210253263619961, 97492.220754027264775, 97492.318486079660943, 97492.318663854093757, 97492.354619285237277]
        # Land\'e factor of atomic levels
        atom['gL'] = [2.0, 0.667, 2.0, 1.333, 0.667, 2.0, 0.8, 1.333, 1.2]
        # Total angular momentum of atomic levels multiplied by two
        atom['J2'] = [1, 1, 1, 3, 1, 1, 3, 3, 5]
        # Pair of indexes of upper-lower levels for each transition
        total_transitions = np.array(generate_radiative_transitions() + collisional_transitions())
        total_transitions = total_transitions.reshape(total_transitions.shape[0] // 2, 2)
        nzi = non_zero_col_index()
        rt = total_transitions[nzi]
        atom['iul'] = list(rt.reshape(rt.size))
        # Einstein spontaneous emission coefficient for each radiative
        # transition
        atom['Aul'] = [6.2648e8, 6.2649e8, 2.1046e6, 4.2097e6, 1.6725e8, 1.6725e8, 2.2448e7, 2.2449e7, 5.3877e7, 6.4651e7, 1.0775e7]

        # atom['Aul'] = [2.1046e6, 4.2097e6, 2.2448e7, 2.2449e7, 5.3877e7,
        #                6.4651e7, 1.0775e7]
        # Number of frequencies for each transition (must be odd and
        # larger than nfreqc)
        atom['nfreq'] = [21, 21, 151, 151, 21, 21, 151, 151, 151, 151, 151]
        # Number of frequencies for the core of each transition (must
        # be odd)
        atom['nfreqc'] = [11, 11, 71, 71, 11, 11, 71, 71, 71, 71, 71]
        # Doppler widths to cover with each transition (must be larger
        # or equal to Dwc)
        atom['Dw'] = [100, 100, 3000, 3000, 100, 100, 3000, 3000, 3000, 3000, 3000]
        # Doppler widths to cover with the core of each transition
        atom['Dwc'] = [10, 10, 50, 50, 10, 10, 50, 50, 50, 50, 50]
        # Reference temperature that determines the Doppler width to
        # build the frequency axis
        atom['Tref'] = [5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3, 5e3]
        # Maximum K multipole order to take into account. negative
        # means unlimited
        atom['Kcut'] = -1
        # Success flag
        atom['check'] = True

    return atom


######################################################################

def get_atmo(atmoname, atomname):
    ''' Configures the atmospheric model
    '''

    # Initialize control flags
    atmo = {'check_atmo': False,
            'check_atom': False}

    atmo['nz'] = bifrost_indice.shape[0]  #177
    # Heights in km
    # atmo['z'] = [
    #     14367.05      , 14076.769     , 13885.17      , 13600.63      ,
    #     13319.47      , 13133.9       , 12858.3       , 12676.389     ,
    #     12406.23      , 12139.299     , 11963.11      , 11701.44      ,
    #     11442.899     , 11272.24      , 11018.811     , 10851.53      ,
    #     10603.1       , 10357.63      , 10195.6       ,  9955.207     ,
    #     9721.841     ,  9570.84      ,  9350.985     ,  9208.725     ,
    #     9001.599     ,  8801.756     ,  8672.444     ,  8484.171     ,
    #     8302.516     ,  8184.973     ,  8013.836     ,  7848.7135    ,
    #     7741.872     ,  7586.311     ,  7485.653     ,  7339.1       ,
    #     7197.6955    ,  7106.2       ,  6972.985     ,  6844.452     ,
    #     6761.283     ,  6640.194     ,  6561.841     ,  6447.761     ,
    #     6337.691     ,  6266.47      ,  6162.773     ,  6062.7215    ,
    #     5997.9825    ,  5903.726     ,  5812.781     ,  5753.935     ,
    #     5668.256     ,  5612.816     ,  5532.0965    ,  5454.2165    ,
    #     5403.823     ,  5330.451     ,  5259.659     ,  5213.851     ,
    #     5147.158     ,  5104.003     ,  5041.171     ,  4980.514     ,
    #     4940.515     ,  4880.515     ,  4820.516     ,  4780.517     ,
    #     4720.518     ,  4680.518     ,  4620.519     ,  4560.52      ,
    #     4520.521     ,  4460.521     ,  4400.522     ,  4360.522     ,
    #     4300.523     ,  4240.524     ,  4200.526     ,  4140.52675   ,
    #     4100.52675   ,  4040.52775   ,  3980.527     ,  3940.52875   ,
    #     3880.53      ,  3820.53      ,  3780.53      ,  3720.53075   ,
    #     3680.53175   ,  3620.533     ,  3560.533     ,  3520.534     ,
    #     3460.535     ,  3400.536     ,  3360.536     ,  3300.536     ,
    #     3260.537     ,  3200.538     ,  3140.539     ,  3100.539     ,
    #     3040.53875   ,  2980.54      ,  2940.541     ,  2880.543     ,
    #     2820.544     ,  2780.544     ,  2720.545     ,  2680.545     ,
    #     2620.546     ,  2560.547     ,  2520.547     ,  2460.547     ,
    #     2400.548     ,  2360.549     ,  2300.55      ,  2260.55      ,
    #     2200.551     ,  2140.552     ,  2100.552     ,  2040.553125  ,
    #     1980.552875  ,  1940.553     ,  1880.554     ,  1840.556     ,
    #     1780.556     ,  1720.557     ,  1680.556875  ,  1620.557     ,
    #     1560.558     ,  1520.557875  ,  1460.559     ,  1400.558875  ,
    #     1360.56      ,  1300.561     ,  1260.561     ,  1200.562     ,
    #     1140.562     ,  1100.562     ,  1040.563     ,   980.5633125 ,
    #      940.564     ,   880.5649375 ,   840.5655    ,   780.5664375 ,
    #      720.594     ,   680.7274375 ,   621.27      ,   561.8346875 ,
    #      522.242     ,   463.05159375,   423.8785    ,   365.34728125,
    #      306.90690625,   267.9375    ,   209.40779688,   150.8986875 ,
    #      112.50460156,    56.37054297,   -38.05236719,   -95.25450781,
    #     -133.38929688,  -190.49990625,  -247.52040625,  -285.48740625,
    #     -342.35234375,  -380.22921875,  -437.13378125,  -494.41065625,
    #     -532.9193125 ,  -591.4046875 ,  -631.07      ,  -691.9981875 ,
    #     -755.278     ,  -799.137875  ,  -868.0465625 ,  -916.4961875 ,
    #     -993.7958125
    # ]

    atmo['z'] = list(np.array([14367.05      , 14269.9       , 14173.14      , 14076.769     ,
       13980.78      , 13885.17      , 13789.941     , 13695.1       ,
       13600.63      , 13506.529     , 13412.821     , 13319.47      ,
       13226.5       , 13133.9       , 13041.66      , 12949.8       ,
       12858.3       , 12767.16      , 12676.389     , 12585.98      ,
       12495.92      , 12406.23      , 12316.899     , 12227.92      ,
       12139.299     , 12051.02      , 11963.11      , 11875.539     ,
       11788.32      , 11701.44      , 11614.92      , 11528.73      ,
       11442.899     , 11357.4       , 11272.24      , 11187.43      ,
       11102.95      , 11018.811     , 10935.        , 10851.53      ,
       10768.389     , 10685.58      , 10603.1       , 10520.95      ,
       10439.12      , 10357.63      , 10276.45      , 10195.6       ,
       10115.08      , 10034.87      ,  9955.207     ,  9876.491     ,
        9798.705     ,  9721.841     ,  9645.89      ,  9570.84      ,
        9496.678     ,  9423.398     ,  9350.985     ,  9279.431     ,
        9208.725     ,  9138.858     ,  9069.819     ,  9001.599     ,
        8934.189     ,  8867.578     ,  8801.756     ,  8736.714     ,
        8672.444     ,  8608.935     ,  8546.181     ,  8484.171     ,
        8422.895     ,  8362.3455    ,  8302.516     ,  8243.395     ,
        8184.973     ,  8127.246     ,  8070.203     ,  8013.836     ,
        7958.138     ,  7903.099     ,  7848.7135    ,  7794.974     ,
        7741.872     ,  7689.397     ,  7637.546     ,  7586.311     ,
        7535.681     ,  7485.653     ,  7436.218     ,  7387.3685    ,
        7339.1       ,  7291.401     ,  7244.269     ,  7197.6955    ,
        7151.6755    ,  7106.2       ,  7061.264     ,  7016.86      ,
        6972.985     ,  6929.627     ,  6886.786     ,  6844.452     ,
        6802.62      ,  6761.283     ,  6720.4375    ,  6680.0765    ,
        6640.194     ,  6600.784     ,  6561.841     ,  6523.36      ,
        6485.335     ,  6447.761     ,  6410.633     ,  6373.945     ,
        6337.691     ,  6301.868     ,  6266.47      ,  6231.491     ,
        6196.9275    ,  6162.773     ,  6129.0255    ,  6095.676     ,
        6062.7215    ,  6030.1605    ,  5997.9825    ,  5966.188     ,
        5934.77      ,  5903.726     ,  5873.048     ,  5842.734     ,
        5812.781     ,  5783.182     ,  5753.935     ,  5725.034     ,
        5696.475     ,  5668.256     ,  5640.372     ,  5612.816     ,
        5585.589     ,  5558.6845    ,  5532.0965    ,  5505.827     ,
        5479.868     ,  5454.2165    ,  5428.8685    ,  5403.823     ,
        5379.073     ,  5354.617     ,  5330.451     ,  5306.571     ,
        5282.976     ,  5259.659     ,  5236.618     ,  5213.851     ,
        5191.355     ,  5169.124     ,  5147.158     ,  5125.452     ,
        5104.003     ,  5082.809     ,  5061.866     ,  5041.171     ,
        5020.721     ,  5000.515     ,  4980.514     ,  4960.514     ,
        4940.515     ,  4920.515     ,  4900.515     ,  4880.515     ,
        4860.515     ,  4840.516     ,  4820.516     ,  4800.517     ,
        4780.517     ,  4760.517     ,  4740.518     ,  4720.518     ,
        4700.518     ,  4680.518     ,  4660.519     ,  4640.519     ,
        4620.519     ,  4600.52      ,  4580.52      ,  4560.52      ,
        4540.52      ,  4520.521     ,  4500.521     ,  4480.52      ,
        4460.521     ,  4440.521     ,  4420.521     ,  4400.522     ,
        4380.522     ,  4360.522     ,  4340.523     ,  4320.523     ,
        4300.523     ,  4280.524     ,  4260.524     ,  4240.524     ,
        4220.525     ,  4200.526     ,  4180.52575   ,  4160.52575   ,
        4140.52675   ,  4120.52675   ,  4100.52675   ,  4080.52675   ,
        4060.52775   ,  4040.52775   ,  4020.52775   ,  4000.52875   ,
        3980.527     ,  3960.52875   ,  3940.52875   ,  3920.529     ,
        3900.529     ,  3880.53      ,  3860.53      ,  3840.53      ,
        3820.53      ,  3800.53      ,  3780.53      ,  3760.531     ,
        3740.531     ,  3720.53075   ,  3700.53175   ,  3680.53175   ,
        3660.53175   ,  3640.53325   ,  3620.533     ,  3600.533     ,
        3580.534     ,  3560.533     ,  3540.534     ,  3520.534     ,
        3500.534     ,  3480.534     ,  3460.535     ,  3440.535     ,
        3420.535     ,  3400.536     ,  3380.536     ,  3360.536     ,
        3340.53675   ,  3320.536     ,  3300.536     ,  3280.537     ,
        3260.537     ,  3240.537     ,  3220.537     ,  3200.538     ,
        3180.538     ,  3160.538     ,  3140.539     ,  3120.539     ,
        3100.539     ,  3080.53975   ,  3060.53875   ,  3040.53875   ,
        3020.53975   ,  3000.54025   ,  2980.54      ,  2960.541     ,
        2940.541     ,  2920.542     ,  2900.542     ,  2880.543     ,
        2860.543     ,  2840.543     ,  2820.544     ,  2800.544     ,
        2780.544     ,  2760.544     ,  2740.544     ,  2720.545     ,
        2700.54475   ,  2680.545     ,  2660.545     ,  2640.546     ,
        2620.546     ,  2600.546     ,  2580.547     ,  2560.547     ,
        2540.547     ,  2520.547     ,  2500.547     ,  2480.547     ,
        2460.547     ,  2440.548     ,  2420.548     ,  2400.548     ,
        2380.549     ,  2360.549     ,  2340.549     ,  2320.55      ,
        2300.55      ,  2280.55      ,  2260.55      ,  2240.55      ,
        2220.551     ,  2200.551     ,  2180.551     ,  2160.551     ,
        2140.552     ,  2120.552     ,  2100.552     ,  2080.553     ,
        2060.553125  ,  2040.553125  ,  2020.553125  ,  2000.552875  ,
        1980.552875  ,  1960.554125  ,  1940.553     ,  1920.554     ,
        1900.554     ,  1880.554     ,  1860.555     ,  1840.556     ,
        1820.556     ,  1800.556     ,  1780.556     ,  1760.556     ,
        1740.556     ,  1720.557     ,  1700.557     ,  1680.556875  ,
        1660.556875  ,  1640.557     ,  1620.557     ,  1600.558     ,
        1580.558     ,  1560.558     ,  1540.559     ,  1520.557875  ,
        1500.558     ,  1480.559     ,  1460.559     ,  1440.559     ,
        1420.56      ,  1400.558875  ,  1380.56      ,  1360.56      ,
        1340.56      ,  1320.56      ,  1300.561     ,  1280.56      ,
        1260.561     ,  1240.561     ,  1220.561     ,  1200.562     ,
        1180.562     ,  1160.561     ,  1140.562     ,  1120.562     ,
        1100.562     ,  1080.563     ,  1060.563     ,  1040.563     ,
        1020.563     ,  1000.563     ,   980.5633125 ,   960.5636875 ,
         940.564     ,   920.5641875 ,   900.564625  ,   880.5649375 ,
         860.565125  ,   840.5655    ,   820.5658125 ,   800.5660625 ,
         780.5664375 ,   760.5666875 ,   740.5721875 ,   720.594     ,
         700.6424375 ,   680.7274375 ,   660.8595625 ,   641.0424375 ,
         621.27      ,   601.438125  ,   581.629875  ,   561.8346875 ,
         542.041     ,   522.242     ,   502.4383125 ,   482.71178125,
         463.05159375,   443.44409375,   423.8785    ,   404.348     ,
         384.84121875,   365.34728125,   345.86178125,   326.384     ,
         306.90690625,   287.4251875 ,   267.9375    ,   248.4405    ,
         228.93070313,   209.40779688,   189.88270313,   170.3681875 ,
         150.8986875 ,   131.5726875 ,   112.50460156,    93.67217969,
          74.98321875,    56.37054297,    37.72887891,    18.93768164,
          -0.        ,   -19.00940039,   -38.05236719,   -57.11212109,
         -76.18101563,   -95.25450781,  -114.3265    ,  -133.38929688,
        -152.43990625,  -171.47679687,  -190.49990625,  -209.51539063,
        -228.52170312,  -247.52040625,  -266.51      ,  -285.48740625,
        -304.4525    ,  -323.407     ,  -342.35234375,  -361.29146875,
        -380.22921875,  -399.1745    ,  -418.13809375,  -437.13378125,
        -456.1721875 ,  -475.26246875,  -494.41065625,  -513.62609375,
        -532.9193125 ,  -552.3025    ,  -571.792625  ,  -591.4046875 ,
        -611.1571875 ,  -631.07      ,  -651.1641875 ,  -671.4645    ,
        -691.9981875 ,  -712.792875  ,  -733.877625  ,  -755.278     ,
        -777.0218125 ,  -799.137875  ,  -821.6568125 ,  -844.613625  ,
        -868.0465625 ,  -891.9935625 ,  -916.4961875 ,  -941.5985    ,
        -967.3476875 ,  -993.7958125 , -1020.996     ])[bifrost_indice])
    # Reverse z axis and convert to cm
    atmo['z'] = atmo['z'][::-1]
    for i in range(len(atmo['z'])):
        atmo['z'][i] = atmo['z'][i] * 1e5
    atmo['check_atmo'] = True
    atmo['check_atom'] = True

    dz = np.array(atmo['z'])
    dz = np.absolute(dz[1:] - dz[:-1])
    atmo['mindz'] = np.amin(dz)

    return atmo

######################################################################


def make_density_matrix(zz, xx, yy, populations, rdim, NL, atom, maxK):
    rho_n = np.zeros((rdim,))
    ridx = 0
    # For each level
    for jj in range(NL):
        J2 = atom['J2'][jj]  # Get angular momentum
        for K in range(maxK[jj]):  # For each K multipole
            # If 0, initilize rho00 with input populations
            if K == 0:
                rho00 = populations[jj] / np.sum(populations) / \
                        np.sqrt(float(J2 + 1))
                rho_n[ridx] = rho00
                ridx += 1

                # If non-zero, initialize to no atomic
                # polarization
            else:

                # For each Q possible for the current K
                for Q in range(K * 2 + 1):
                    rho_n[ridx] = 0.0
                    ridx += 1

    return zz, xx, yy, rho_n


def prepare_reinterpolate_to_177_grid(f467, var_name, f177):

    def reinterpolate_to_177_grid(i, j):
        i = int(i)
        j = int(j)
        var = f467[var_name][:, :, i, j]

        cs = CubicSpline(f467['ltau500'][0, i, j], var, axis=2)

        return cs(f177['ltau500'][0, i, j])

    return reinterpolate_to_177_grid


def main():
    ''' Writes a simple pmd file (binary and HDF5 versions) for the multilevel module
    '''

    ###################################
    # Internal function: write_hdf5
    ###################################
    def write_hdf5(outname):

        fast_path = Path('/home/harsh/BifrostRun_fast_Access/')
        write_path = Path('/run/media/harsh/5de85c60-8e85-4cc8-89e6-c74a83454760/')
        f = h5py.File(write_path / outname, 'w')

        # PORTA Head #
        # ------------
        f.attrs['PMD_MAGIC'] = np.string_('portapmd')
        f.attrs['PMD_VERSION'] = np.int32([2])

        now = datetime.datetime.now()
        date = np.int32([int(now.year - 1900.), int(now.month - 1.), \
                         int(now.day), int(now.hour), int(now.minute), \
                         int(now.second)])
        f.attrs['CREATION_DATE'] = date
        f.attrs['PERIODICITY'] = np.int32([1, 1])
        f.attrs['DOMAIN_SIZE'] = [Dx, Dy, Dz]
        f.attrs['DOMAIN_ORIGIN'] = [x0, y0, z0]
        f.attrs['GRID_DIMENSIONS'] = np.int32([nx, ny, nz])
        f.attrs['X_AXIS'] = x
        f.attrs['Y_AXIS'] = y
        f.attrs['Z_AXIS'] = z
        f.attrs['POLAR_NODES'] = np.int32([nth])
        f.attrs['AZIMUTH_NODES'] = np.int32([nph])
        f.attrs['MODULE_NAME'] = np.string_(m_name)

        comment = 'Model for ' + atomname + ' atom in ' + \
                  atmoname + ' atmospheric model replicated in ' + \
                  '{0}x{1}'.format(nx, ny) + ' columns for the ' + \
                  m_name + ' module'
        f.attrs['MODULE_COMMENT'] = np.string_(comment)

        Head_size = int(28 + 20 * NL + 8 * NT + 40 * NR + \
                        8 * nx * ny)
        Node_size = int(9 * 8 + NR * 12 * 8 + NL * 8 + NT * 8)
        rdim = 0  # Size (in elements) of density matrix
        for ii in range(NL):
            for K in range(maxK[ii]):
                for Q in range(K * 2 + 1):
                    rdim += 1
        Node_size += rdim * 8

        f.attrs['MODULE_HEADER_SIZE'] = np.int32([Head_size])

        # Not used in PORTA, really
        f.attrs['Size Node'] = np.int32([Node_size])

        # Module head #
        # -------------
        f.create_group('Module')
        #
        f['Module'].attrs['ML_VERSION'] = np.int32([m_version])
        f['Module'].attrs['ATOM_MASS'] = [atom['mass']]
        f['Module'].attrs['NL'] = np.int32([atom['NL']])
        f['Module'].attrs['NT'] = np.int32([atom['NT']])
        f['Module'].attrs['NR'] = np.int32([atom['NR']])
        f['Module'].attrs['E'] = atom['E']
        f['Module'].attrs['GL'] = atom['gL']
        f['Module'].attrs['J2'] = np.int32(atom['J2'])
        f['Module'].attrs['IUL'] = np.int32(atom['iul'])
        f['Module'].attrs['AUL'] = atom['Aul']
        f['Module'].attrs['NFREQ'] = np.int32(atom['nfreq'])
        f['Module'].attrs['NFREQ_C'] = np.int32(atom['nfreqc'])
        f['Module'].attrs['DW'] = atom['Dw']
        f['Module'].attrs['DW_C'] = atom['Dwc']
        f['Module'].attrs['TEMP_REF'] = atom['Tref']
        f['Module'].attrs['K_CUT'] = np.int32([atom['Kcut']])

        # Grid Data #
        # -----------

        # Grid node structured array
        node_dt = np.dtype([ \
            ('T', np.float), \
            ('N', np.float), \
            ('B', np.float, 3), \
            ('v', np.float, 3), \
            ('vmi', np.float), \
            ('rho', np.float, rdim), \
            ('J', np.float, 9 * NR), \
            ('a_voigt', np.float, NR), \
            ('Cul', np.float, NT), \
            ('DK', np.float, NL), \
            ('eta_c', np.float, NR), \
            ('eps_c', np.float, NR) \
            ])

        grid = f['Module'].create_dataset("g_data", (nz, ny, nx), dtype=node_dt)

        atmos_path = fast_path / 'BIFROST_en024048_hion_snap_385_0_504_0_504_-1020996.0_15000000.0.nc'
        suppl_out_path = fast_path / 'MULTI3D_BIFROST_en024048_hion_snap_385_0_504_0_504_-1020996.0_15000000.0_supplementary_outputs.nc'
        pb_rate_path = fast_path / 'MULTI3D_BIFROST_en024048_hion_snap_385_0_504_0_504_-1020996.0_15000000.0_pb_rates.nc'

        fatmos = h5py.File(atmos_path, 'r')
        fsuppl = h5py.File(suppl_out_path, 'r')
        pb_rates = h5py.File(pb_rate_path, 'r')

        f['Module/temp_bc'] = fatmos['temperature'][0][nxs:nxe][:, nys:nye][:, :, bifrost_indice[-1]]

        grid[:, :, :, 'T'] = np.transpose(fatmos['temperature'][0][nxs:nxe][:, nys:nye][:, :, bifrost_indice], axes=(2, 0, 1))[::-1]
        # import ipdb;ipdb.set_trace()
        grid[:, :, :, 'N'] = np.transpose(np.sum(fsuppl['populations'][:, :, :, :, bifrost_indice][0, 0:NL][:, nxs:nxe][:, :, nys:nye], 0), axes=(2, 0, 1))[::-1] * 1e-6
        B = np.zeros((nz, ny, nx, 3), dtype=np.float64)
        B[:, :, :, 0] = np.transpose(fatmos['B_x'][0][nxs:nxe][:, nys:nye][:, :, bifrost_indice], axes=(2, 0, 1))[:: -1] * 1e4
        B[:, :, :, 1] = np.transpose(fatmos['B_y'][0][nxs:nxe][:, nys:nye][:, :, bifrost_indice], axes=(2, 0, 1))[:: -1] * 1e4
        B[:, :, :, 2] = np.transpose(fatmos['B_z'][0][nxs:nxe][:, nys:nye][:, :, bifrost_indice], axes=(2, 0, 1))[:: -1] * 1e4
        V = np.zeros((nz, ny, nx, 3), dtype=np.float64)
        V[:, :, :, 0] = np.transpose(fatmos['velocity_x'][0][nxs:nxe][:, nys:nye][:, :, bifrost_indice], axes=(2, 0, 1))[::-1] * 1e2
        V[:, :, :, 1] = np.transpose(fatmos['velocity_y'][0][nxs:nxe][:, nys:nye][:, :, bifrost_indice], axes=(2, 0, 1))[::-1] * 1e2
        V[:, :, :, 2] = np.transpose(fatmos['velocity_z'][0][nxs:nxe][:, nys:nye][:, :, bifrost_indice], axes=(2, 0, 1))[::-1] * 1e2
        grid[:, :, :, 'B'] = B
        grid[:, :, :, 'v'] = V

        zzzz, xxxx, yyyy = np.where(np.ones((nz, nx, ny)) > 0)

        print('Just before Rho')

        populations = fsuppl['populations'][:, :, :, :, bifrost_indice][0, 0:NL][:, nxs:nxe][:, :, nys:nye][:, :, :, ::-1]

        k = 0

        with Pool(processes=31) as pool:
            results = list()
            t = tqdm(zip(zzzz, xxxx, yyyy), total=nz * nx * ny)
            for zzz, xxx, yyy in t:
                results.append(
                    pool.apply_async(
                        make_density_matrix,
                        args=(
                            zzz,
                            xxx,
                            yyy,
                            populations[:, xxx, yyy, zzz],
                            rdim, NL, atom, maxK
                        )
                    )
                )
                k += 1
                if k == 20000:
                    for r in results:
                        zz, xx, yy, rho_n = r.get()
                        grid[zz, xx, yy, 'rho'] = rho_n
                    results = list()
                    k = 0
            for r in results:
                zz, xx, yy, rho_n = r.get()
                grid[zz, xx, yy, 'rho'] = rho_n

        print('After Rho')

        # nt_array = np.array(list(set(np.arange(36)) - set([0, 1, 4, 5]))).astype(np.int64)
        # nr_array = np.array([2, 3, 6, 7, 8, 9, 10])
        nzi = non_zero_col_index()
        grid[:, :, :, 'J'] = [0.0] * 9 * NR  # Radiation field tensors (initialize to 0)
        grid[:, :, :, 'a_voigt'] = np.transpose(fsuppl['a_voigt'][:, :, :, :, bifrost_indice][0, :][:, nxs:nxe][:, :, nys:nye], axes=(3, 1, 2, 0))[::-1]  # Damping parameter
        grid[:, :, :, 'Cul'] = np.transpose(pb_rates['Cul_pb_rates'][nxs:nxe][:, nys:nye][:, :, bifrost_indice][:, :, :, nzi], axes=(2, 0, 1, 3))[::-1]# Deexcitation rate for inelastic collisions with electrons
        grid[:, :, :, 'DK'] = np.zeros((nz, nx, ny, NL))  # Depolarization rate for elastic collisions with neutral hydrogen
        grid[:, :, :, 'eta_c'] = np.transpose(fsuppl['eta_c'][:, :, :, :, bifrost_indice][0, :][:, nxs:nxe][:, :, nys:nye], axes=(3, 1, 2, 0))[::-1] * 1e-2  # Continuum opacity at each radiative transition
        grid[:, :, :, 'eps_c'] = np.transpose(fsuppl['eps_c'][:, :, :, :, bifrost_indice][0, :][:, nxs:nxe][:, :, nys:nye], axes=(3, 1, 2, 0))[::-1] * 10  # Continuum emissivity at each radiative transition

        f.close()
        fatmos.close()
        fsuppl.close()
        pb_rates.close()
        # Closing message
        msg = '\nYour pmd file (HDF5 format) for the atom {0} and \n' + \
              '  atmospheric model {1} has been created and stored \n' + \
              '  in the file: {2}'
        print(msg.format(atomname, atmoname, outname))

    def write_pmd(outname):
        # Open pmd file pmd

        fast_path = Path('/home/harsh/BifrostRun_fast_Access/')
        write_path = Path('/run/media/harsh/5de85c60-8e85-4cc8-89e6-c74a83454760/')

        f = open(write_path / outname, 'wb')

        atmos_path = fast_path / 'BIFROST_en024048_hion_snap_385_0_504_0_504_-1020996.0_15000000.0.nc'
        suppl_out_path = fast_path / 'MULTI3D_BIFROST_en024048_hion_snap_385_0_504_0_504_-1020996.0_15000000.0_supplementary_outputs.nc'
        pb_rate_path = fast_path / 'MULTI3D_BIFROST_en024048_hion_snap_385_0_504_0_504_-1020996.0_15000000.0_pb_rates.nc'

        fatmos = h5py.File(atmos_path, 'r')
        fsuppl = h5py.File(suppl_out_path, 'r')
        pb_rates = h5py.File(pb_rate_path, 'r')
        ##############
        # PORTA Head #
        ##############

        written = 0
        # Identifier string
        if oldpy:
            written += f.write(struct.pack('c' * 8, *list('portapmd')))
        else:
            written += f.write(struct.pack('8s', b'portapmd'))

        print("portapmd: {}".format(written))

        # Endianess (0 = little)
        written += f.write(struct.pack('b', 0))
        print("Endianness: {}".format(written))

        # Integer size
        written += f.write(struct.pack('b', 4))
        print("Integer size: {}".format(written))

        # Double size
        written += f.write(struct.pack('b', 8))
        print("Double size: {}".format(written))

        # PMD version
        written += f.write(struct.pack('i', 2))
        print("PMD version: {}".format(written))

        # Date
        # now = datetime.datetime.now()
        # dates = [int(now.year - 1900.), int(now.month - 1.), \
        #         int(now.day), int(now.hour), int(now.minute), \
        #         int(now.second)]
        dates = [int(2022 - 1900.), int(12 - 1.), \
                int(25), int(19), int(20), \
                int(25)]
        written += f.write(struct.pack('i' * 6, *dates))
        print("Date: {}".format(written))

        # X periodicity
        written += f.write(struct.pack('b', 1))
        print("X Periodicity: {}".format(written))

        # Y periodicity
        written += f.write(struct.pack('b', 1))
        print("Y Periodicity: {}".format(written))

        # Domain size in cm
        written += f.write(struct.pack('ddd', *[Dx, Dy, Dz]))
        print("Domain size: {}".format(written))

        # Domain origin in cm
        written += f.write(struct.pack('ddd', *[x0, y0, z0]))
        print("Domain origin: {}".format(written))

        # Grid dimensions
        written += f.write(struct.pack('iii', *[nx, ny, nz]))
        print("Grid dimension: {}".format(written))

        # X axis
        written += f.write(struct.pack('d' * nx, *x))
        print("X-axis: {}".format(written))

        written += f.write(struct.pack('d' * (8192 - nx), *np.zeros((8192 - nx))))
        print("X-axis zero padding: {}".format(written))

        # Y axis
        written += f.write(struct.pack('d' * ny, *y))
        print("Y-axis: {}".format(written))

        written += f.write(struct.pack('d' * (8192 - ny), *np.zeros((8192 - ny))))
        print("Y-axis zero padding: {}".format(written))

        # Z axis
        written += f.write(struct.pack('d' * nz, *z))
        print("Z-axis: {}".format(written))

        written += f.write(struct.pack('d' * (8192 - nz), *np.zeros((8192 - nz))))
        print("Z-axis zero padding: {}".format(written))

        # Polar nodes per octant
        written += f.write(struct.pack('i', nth))
        print("Polar nodes: {}".format(written))

        # Azimuth nodes per octant
        written += f.write(struct.pack('i', nph))
        print("Azimuth nodes: {}".format(written))

        # Module name
        if oldpy:
            written += f.write(struct.pack('c' * m_nsize, *list(m_name)))
            print("Module name: {}".format(written))
            written += f.write(struct.pack('c' * (1023 - m_nsize), \
                                *(['\x00'] * (1023 - m_nsize))))
            print("Module name zero padding: {}".format(written))
        else:
            written += f.write(struct.pack(str(m_nsize) + 's', \
                                bytes(m_name, encoding="utf-8")))
            print("Module name: {}".format(written))
            written += f.write(struct.pack(str(1023 - m_nsize) + 's', \
                                bytes('\x00' * (1023 - m_nsize), encoding="utf-8")))
            print("Module name zero padding: {}".format(written))

        # Comment
        comment = 'Model for ' + atomname + ' atom in ' + \
                  atmoname + ' atmospheric model replicated in ' + \
                  '{0}x{1}'.format(nx, ny) + ' columns for the ' + \
                  m_name + ' module'
        c_size = len(list(comment))
        if oldpy:
            written += f.write(struct.pack('c' * c_size, *list(comment)))
            print("Comment: {}".format(written))
            written += f.write(struct.pack('c' * (4096 - c_size), \
                                *(['\0'] * (4096 - c_size))))
            print("Comment zero padding: {}".format(written))
        else:
            written += f.write(struct.pack(str(c_size) + 's', \
                                bytes(comment, encoding="utf-8")))
            print("Comment: {}".format(written))
            written += f.write(struct.pack(str(4096 - c_size) + 's', \
                                bytes('\0' * (4096 - c_size), encoding="utf-8")))
            print("Comment zero padding: {}".format(written))

        # Sizes of header and node
        Head_size = int(28 + 20 * NL + 8 * NT + 40 * NR + \
                        8 * nx * ny)
        Node_size = int(9 * 8 + NR * 12 * 8 + NL * 8 + NT * 8)

        print("Head size: {}, Node size: {}".format(Head_size, Node_size))

        for ii in range(NL):
            print("{}-{}".format(ii, maxK[ii]))
            for K in range(maxK[ii]):
                for Q in range(K * 2 + 1):
                    Node_size += 8

        print("Head size: {}, Node size: {}".format(Head_size, Node_size))

        written += f.write(struct.pack('i', Head_size))
        print("Head size: {}".format(written))

        written += f.write(struct.pack('i', Node_size))
        print("Node size: {}".format(written))
        #
        # ###############
        # # Module head #
        # ###############
        #
        # Module version
        written += f.write(struct.pack('i', m_version))
        print("Module version: {}".format(written))

        # Atomic mass in AMU
        written += f.write(struct.pack('d', atom['mass']))
        print("Atom mass: {}".format(written))

        # Number of atomic levels
        written += f.write(struct.pack('i', atom['NL']))
        print("NL: {}".format(written))

        # Number of transitions (radiative and forbidden collisions)
        written += f.write(struct.pack('i', atom['NT']))
        print("NT: {}".format(written))

        # Number of transitions (radiative)
        written += f.write(struct.pack('i', atom['NR']))
        print("NR: {}".format(written))

        # Energy of atomic levels in erg
        written += f.write(struct.pack('d' * NL, *atom['E']))
        print("E: {}".format(written))

        # Land\'e factors of atomic levels
        written += f.write(struct.pack('d' * NL, *atom['gL']))
        print("gL: {}".format(written))

        # Angular momentum of atomic levels multiplied by two
        written += f.write(struct.pack('i' * NL, *atom['J2']))
        print("J2: {}".format(written))

        # Pairs of upper-lower indexes for each transition. C notation
        written += f.write(struct.pack('i' * 2 * NT, *atom['iul']))
        print("iul: {}".format(written))

        # Einstein coefficient for spontaneous emission for each
        # radiative transition
        written += f.write(struct.pack('d' * NR, *atom['Aul']))
        print("Aul: {}".format(written))

        # Number of frequencies for each transition (must be odd and
        # larger than nfreqc)
        written += f.write(struct.pack('i' * NR, *atom['nfreq']))
        print("nfreq: {}".format(written))

        # Number of frequencies for the core of each transition (must
        # be odd)
        written += f.write(struct.pack('i' * NR, *atom['nfreqc']))
        print("nfreqc: {}".format(written))

        # Doppler widths to cover with each transition (must be larger
        # or equal to Dwc)
        written += f.write(struct.pack('d' * NR, *atom['Dw']))
        print("Dw: {}".format(written))

        # Doppler widths to cover with the core of each transition
        written += f.write(struct.pack('d' * NR, *atom['Dwc']))
        print("Dwc: {}".format(written))

        # Reference temperature that determines the Doppler width to
        # build the frequency axis
        written += f.write(struct.pack('d' * NR, *atom['Tref']))
        print("Tref: {}".format(written))

        # Maximum K multipole order to take into account. negative
        # means unlimited
        written += f.write(struct.pack('i', atom['Kcut']))
        print("Kcut: {}".format(written))

        # Temperature at the bottom layer
        for iy in range(ny):
            for ix in range(nx):
                written += f.write(struct.pack('d', fatmos['temperature'][0, ix, iy, bifrost_indice[-1]]))

        print("Temp bc: {}".format(written))

        ###############
        # Module Grid #
        ###############

        # For each height

        # sys.stdout.write("\nWritten {} bytes in header\n".format(written))
        nzi = non_zero_col_index()
        #
        t = tqdm(total=nx * ny * nz)
        #
        for iz in range(nz):

            # For each column to replicate
            for iy in range(ny):

                for ix in range(nx):

                    # Write temperature
                    # print("Temp {}".format(fatmos['temperature'][0, ix, iy, bifrost_indice][::-1][iz]))
                    f.write(struct.pack('d', fatmos['temperature'][0, ix, iy, bifrost_indice][::-1][iz]))

                    # # Write density
                    # print("Density {}".format(np.sum(fsuppl['populations'][0, 0:NL, ix, iy, bifrost_indice][:, ::-1][:, iz], 0) * 1e-6))
                    f.write(struct.pack('d', np.sum(fsuppl['populations'][0, 0:NL, ix, iy, bifrost_indice][:, ::-1][:, iz], 0) * 1e-6))

                    # Write magnetic field
                    n_B = [
                        fatmos['B_x'][0, ix, iy, bifrost_indice][::-1][iz] * 1e4,
                        fatmos['B_y'][0, ix, iy, bifrost_indice][::-1][iz] * 1e4,
                        fatmos['B_z'][0, ix, iy, bifrost_indice][::-1][iz] * 1e4
                    ]
                    f.write(struct.pack('ddd', *n_B))
                    # Write velocity field
                    n_v = [
                        fatmos['velocity_x'][0, ix, iy, bifrost_indice][::-1][iz] * 1e2,
                        fatmos['velocity_y'][0, ix, iy, bifrost_indice][::-1][iz] * 1e2,
                        fatmos['velocity_z'][0, ix, iy, bifrost_indice][::-1][iz] * 1e2
                    ]
                    f.write(struct.pack('ddd', *n_v))
                    # Write microturbulence
                    f.write(struct.pack('d', 0))

                    # Initialize number of written doubles
                    kk = 9

                    # For each level
                    for jj in range(NL):

                        # Get angular momentum
                        J2 = atom['J2'][jj]

                        # For each K multipole
                        for K in range(maxK[jj]):

                            # If 0, initilize rho00 with input populations
                            if K == 0:
                                rho00 = fsuppl['populations'][0, jj, ix, iy, bifrost_indice][::-1][iz] / np.sum(fsuppl['populations'][0, 0:NL, ix, iy, bifrost_indice][:, ::-1][:, iz], 0) / \
                                        np.sqrt(float(J2 + 1))
                                f.write(struct.pack('d', rho00))
                                kk += 1

                            # If non-zero, initialize to no atomic
                            # polarization
                            else:

                                # For each Q possible for the current K
                                for Q in range(K * 2 + 1):
                                    f.write(struct.pack('d', 0.0))
                                    kk += 1

                    # Number of written doubles not counting the first 9
                    mm = kk - 9

                    # Write radiation field tensors (initialize to 0)
                    f.write(struct.pack('d' * 9 * NR, *([0.0] * 9 * NR)))
                    kk += 9 * NR

                    # Write damping parameter
                    f.write(struct.pack('d' * NR, *list(fsuppl['a_voigt'][0, :, ix, iy][:, bifrost_indice][:, ::-1][:, iz])))
                    kk += NR

                    # Write deexcitation rate for inelastic collisions
                    # with electrons
                    f.write(struct.pack('d' * NT, *list(pb_rates['Cul_pb_rates'][ix, iy, bifrost_indice][::-1, nzi][iz])))
                    kk += NT

                    # Write depolarization rate for elastic collisions
                    # with neutral hydrogen
                    f.write(struct.pack('d' * NL, *list(np.zeros([NL]))))
                    kk += NL

                    # Write continuum opacity at each radiative transition
                    f.write(struct.pack('d' * NR, *list(fsuppl['eta_c'][0, :, ix, iy][:, bifrost_indice][:, ::-1][:, iz] * 1e-2)))
                    kk += NR

                    # Write continuum emissivity at each radiative transition
                    f.write(struct.pack('d' * NR, *list(fsuppl['eps_c'][0, :, ix, iy][:, bifrost_indice][:, ::-1][:, iz] * 10)))
                    kk += NR

                    # If first grid node, notify how much was written
                    if iz == 0 and iy == 0 and ix == 0:
                        msg = 'Node size {0} elements and {1} bytes. ' + \
                              'Expected {2} bytes'
                        print(msg.format(kk, kk * 8, Node_size))
                        msg = 'Size of density matrix is {0} elements ' + \
                              'and {1} bytes'
                        print(msg.format(mm, mm * 8))

                    t.update(1)

        # Close pmd
        f.close()
        fatmos.close()
        fsuppl.close()
        pb_rates.close()

        # Closing message
        msg = '\nYour pmd file (binary format) for the atom {0} and \n' + \
              '  atmospheric model {1} has been created and stored \n' + \
              '  in the file: {2}'
        print(msg.format(atomname, atmoname, outname))

    # Internal function: write_hdf5 (end)
    ###################################
    # Main function start

    # Set defaults
    outname = 'MULTI3D_H_Bifrost_s_385'
    atomname = 'H'
    atmoname = 'Bifrost'
    # Bm = '0'
    # Bt = '0'
    # Ba = '0'
    # fTPerx = -1.0
    # fVPerx = 1.0
    # bPerx = False
    # fTPery = -1.0
    # fVPery = 1.0
    # bPery = False
    # inx = -1
    # iny = -1

    # Check if old version
    if sys.version_info[0] < 3:
        oldpy = True
    else:
        oldpy = False



    ######################################################################

    # Angular quadrature, nodes per octant
    nth = 4
    nph = 2
    # Minimum value of mu
    # mu = np.polynomial.legendre.leggauss(4)[0]
    # mu = 0.5 * mu + 0.5
    # minmu = np.amin(np.absolute(mu))

    # Module version and name
    m_version = 1
    m_name = 'multilevel'

    # Atom
    atom = get_atom(atomname)
    if not atom['check']:
        msg = 'There was a problem setting the atomic model'
        sys.exit(msg)

    # Atmosphere
    atmo = get_atmo(atmoname, atomname)
    if not atmo['check_atmo']:
        msg = 'There was a problem setting the thermal part of ' + \
              'the atmospheric model'
        sys.exit(msg)
    if not atmo['check_atom']:
        msg = 'There was a problem setting the atomic part of ' + \
              'the atmospheric model'
        sys.exit(msg)

    nxs = 0
    nxe = 3
    nys = 0
    nye = 3

    nx = nxe - nxs
    ny = nye - nys

    # If no perturbed and domain not specified
    # if not bPerx and fTPerx < 1e-6:
    #     # It is extended enough so every ray crosses the
    #     # bottom boundary for 1.5D
    #     x0 = 0.0
    #     x1a = float(nx) * atmo['mindz'] * minmu * 1.01 + x0
    #     x1b = np.amax(atmo['z']) - np.min(atmo['z']) + x0
    #     x1 = np.max([x1a, x1b])
    # else:
    #     # The period is the domain, not the difference
    #     # between extremes
    #     fTPerx *= 1e5
    #     x0 = 0.0
    #     x1 = fTPerx * (1.0 - 1.0 / float(nx))
    #
    # # If no perturbed and domain not specified
    # if not bPery and fTPery < 1e-6:
    #     # It is extended enough so every ray crosses the
    #     # bottom boundary for 1.5D
    #     y0 = 0.0
    #     y1a = float(ny) * atmo['mindz'] * minmu * 1.01 + y0
    #     y1b = np.amax(atmo['z']) - np.min(atmo['z']) + y0
    #     y1 = np.max([y1a, y1b])
    # else:
    #     # The period is the domain, not the difference
    #     # between extremes
    #     fTPery *= 1e5
    #     y0 = 0.0
    #     y1 = fTPery * (1.0 - 1.0 / float(ny))

    # Build geometry
    x0 = -((nx-1)//2) * 48e5
    x1 = ((nx - 1)//2) * 48e5
    x = np.linspace(x0, x1, nx, endpoint=True)
    Dx = x[-1] + x[1] - 2. * x[0]
    y0 = -((ny-1)//2) * 48e5
    y1 = ((ny - 1)//2) * 48e5
    y = np.linspace(y0, y1, ny, endpoint=True)
    Dy = y[-1] + y[1] - 2. * y[0]
    z = atmo['z']
    nz = atmo['nz']
    z0 = z[0]
    Dz = z[-1] - z[0]
    print (x)
    print (Dx)
    print (y)
    print (Dy)

    # # Check correct size if perturbed
    # if bPerx:
    #     if np.absolute(Dx - fTPerx) > 1e-6:
    #         msg = 'Something went wrong defining the domain'
    #         msg += '{0} != {1}'.format(Dx, fTPerx)
    #         sys.exit(msg)
    #     fTPerx = 2.0 * np.pi / fTPerx
    # if bPery:
    #     if np.absolute(Dy - fTPery) > 1e-6:
    #         msg = 'Something went wrong defining the domain'
    #         msg += '{0} != {1}'.format(Dy, fTPery)
    #         sys.exit(msg)
    #     fTPery = 2.0 * np.pi / fTPery

    # Get size module name
    m_nsize = len(list(m_name))

    # Short for some dictionary quantities
    NL = atom['NL']
    NT = atom['NT']
    NR = atom['NR']

    # Compute maximum K for each level
    Kcut = atom['Kcut']
    if Kcut < 0:
        lKcut = 2000
    else:
        lKcut = Kcut
    maxK = []
    for ii in range(NL):
        J2 = atom['J2'][ii]
        maxK.append(np.min([lKcut, J2]) + 1)

    # Transform energies into erg
    for i in range(len(atom['E'])):
        atom['E'][i] = atom['E'][i] * 6.62606896e-27 * 299792458e2

    # # Transform indexes into C/python
    # for i in range(len(atom['iul'])):
    #     atom['iul'][i] = atom['iul'][i] - 1

    # write_hdf5('{}_{}_{}_{}_{}.h5'.format(outname, nxs, nxe, nys, nye))
    write_hdf5('{}_{}_{}_{}_{}.h5'.format(outname, nxs, nxe, nys, nye))
    # write_pmd('{}_{}_{}_{}_{}.pmd'.format(outname, nxs, nxe, nys, nye))

######################################################################

if __name__ == "__main__":
    main()
