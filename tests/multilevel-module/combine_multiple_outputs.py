import h5py
import numpy as np
from pathlib import Path

ref_x = 180
ref_y = 60

indices = np.arange(0, 183, 3)


def get_element_position(arr, val):
    return np.where(arr == val)

vec_get_element_position = np.vectorize(get_element_position, signature='(m,n),()->(),()')

def combine_stokes_params():
    base_path = Path('/home/harsh/BifrostRun_fast_Access/MULTI3D_s_385_x_180_y_60_dimx_180_dim_y_180_step_x_3_step_y_3/')

    look_up_arr = np.arange(183 * 183, dtype=np.float64).reshape(183, 183)
    si = np.zeros((1261, 183, 183), dtype=np.float64)
    sq = np.zeros((1261, 183, 183), dtype=np.float64)
    su = np.zeros((1261, 183, 183), dtype=np.float64)
    sv = np.zeros((1261, 183, 183), dtype=np.float64)

    for offset_x in range(3):
        for offset_y in range(3):
            f = h5py.File(base_path / 'MULTI3D_H_Bifrost_s_385_{}_{}_61_3_out_1_profs.h5'.format(ref_x+offset_x, ref_y+offset_y), 'r')
            x, y= vec_get_element_position(look_up_arr, look_up_arr[indices + offset_x][:, indices + offset_y].reshape(61 * 61))
            si[:, x, y] = f['stokes_I'][()].reshape(1261, 61 * 61)
            sq[:, x, y] = f['stokes_Q'][()].reshape(1261, 61 * 61)
            su[:, x, y] = f['stokes_U'][()].reshape(1261, 61 * 61)
            sv[:, x, y] = f['stokes_V'][()].reshape(1261, 61 * 61)
            f.close()

    f = h5py.File(base_path / 'combined_output_profs.h5', 'w')
    f['stokes_I'] = si
    f['stokes_Q'] = sq
    f['stokes_U'] = su
    f['stokes_V'] = sv
    f.close()


if __name__ == '__main__':
    combine_stokes_params()
