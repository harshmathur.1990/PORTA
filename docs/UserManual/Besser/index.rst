.. _besser_formal_solver:

The BESSER formal solver
========================

To solve the radiation transfer equations, PORTA applies the generalization of
the short-characteristics (SC) method of Kunasz, P. and Auer, L.H. (1988) [1]_ to the
polarized case. For a given frequency and direction, we consider three
consecutive spatial points :math:`M`, :math:`O`, and :math:`P` along the
propagation direction, with :math:`M` the upwind points, :math:`P` the downwind
points, and :math:`O` the point where the Stokes parameters need to be computed.

.. _sc_3d:
.. figure:: images/sc.png
   :align: center	    

   Short-characteristics in a three-dimensional Cartesian rectilinear grid.

The aim of the original SC method is to solve the integral form of the
radiation transfer equation:

.. math:: I_O = I_M e^{-\tau_{MO}} + \int_{0}^{\tau_{MO}} dt S\left(t\right) e^{-t}, 
	  
where :math:`I_O` is the intensity we want to compute, :math:`I_M` the intensity in the
upwind point, :math:`S\left(t\right)` is the source function at :math:`\tau = t`, and
:math:`\tau_{MO}` is the optical distance between points :math:`M` and :math:`O`. In our case,
the equation to solve is:

.. math:: \vec{S}_O = \vec{S}_M e^{-\tau_{MO}} + \int_{0}^{\tau_{MO}} dt S_{\rm eff}\left(t\right) e^{-t},

where :math:`\vec{S}_O` is the vector of Stokes parameters we want to compute,
:math:`\vec{S}_M` is the Stokes vector in the upwind points, and :math:`S_{\rm eff}\left(t\right)`
is the effective source function given by:

.. math:: S_{\rm eff}\left(t\right) = S\left(t\right) - \vec{K}'\left(t\right)\vec{S}\left(t\right),

	  
with :math:`\vec{K}'\left(t\right) = \vec{K}\left(t\right)/\eta_I - \vec{1}.`

The original SC method is based on the approximation of parabolic interpolation
of the source function between the upwind point :math:`M`, the grid
point :math:`O`, and the downwind point :math:`P` (see :numref:`sc_3d`). In 2D and 3D grids, the
upwind and downwind points of the SC do not generally coincide with any
spatial grid node and the radiation transfer quantities (i.e., the emission
and absorption coefficients) have to be interpolated from the nearby 9-point
(biquadratic case) or 4-point (bilinear case) stencils of the discrete grid
points. Bilinear interpolation is sufficient in the fine grids of today’s
MHD models and this option is implemented in PORTA. The biquadratic interpolation
can lead to more accurate solution but it is significantly more time consuming.
The upwind Stokes vector :math:`\vec{S}_M` needs to be interpolated
from the same grid nodes. Proper topological ordering of the grid points is
therefore necessary for every direction of the short characteristics. The
intersection points :math:`M` and :math:`P` may be located on a vertical plane of
the grid instead of a horizontal one.

To avoid the overshooting problems of the parabolic interpolation, Štěpán, J.,
and Trujillo Bueno, J. (2013) [2]_ developed
an accurate formal solver, dubbed BESSER (BEzier Spline SolvER).  The algorithm
implements an interpolation scheme based on the use of monotonic Bézier splines
(see Auer, L. (2003) [3]_ for an
alternative implementation with splines), imposing a continuous first derivative
of the interpolant.

The control points of the splines are used to preserve monotonicity of the
interpolant (it is contained in an envelope defined by the tangents of the
spline in its endpoints and of the control point which is located in the
intersection of these tangents, :numref:`besser_interp`). By imposing a continuous first
derivative of the interpolant, we achieve a smooth connection of the Bézier
splines in the central point :math:`O`. This implementation leads to a symmetrical
interpolation independently of the choice of the interpolation direction 
(:math:`MOP` for one direction of the ray propagation or :math:`POM` for the opposite direction
of the ray). In addition, our BESSER method always provides reliable values
for the diagonal of the :math:`\Lambda`-operator, i.e., in the interval [0,1),
used in methods based on the Jacobi iteration. See :numref:`besser_interp` for a comparison
between the parabolic and BESSER implementations.

.. _besser_interp:
.. figure:: images/fs_besvspar.png
   :width: 80%
   :align: center	    	    

   Parabolic and BESSER interpolation using the three successive points
   M, O, and P. Dotted line: parabolic interpolation. Solid line: interpolation
   using our BESSER method with continuous derivative at point O.

Given a quantity :math:`y` (e.g., the source function) defined at three successive
points :math:`x_M`, :math:`x_O`, andi :math:`x_P`, we use two quadratic Bézier splines to interpolate
:math:`y` between points :math:`M` and :math:`O` and between points :math:`O` and :math:`P` (see :numref:`besser_interp`).
First, we look for an optimal interpolation in the interval :math:`MO`. For the sake
of simplicity, we parametrize the :math:`x`-coordinate in this interval by a dimensionless
parameter :math:`u=(x-x_M)/h_M`, where :math:`h_M=x_O-x_M`. The Bézier spline in the interval :math:`MO`
is a parabola passing through points :math:`M` and :math:`O`. The derivatives at such points are
defined by the position of the control point whose :math:`y`-coordinate is
:math:`c_M` (see :numref:`besser_interp`). The equation for such a spline reads (Auer, L. 2003 [3]_):

.. math:: y(u)=(1-u)i^2y_M+2u(1-u)c_M+u^2y_O, u \in [0,1].

Similarly, one can define a Bézier spline between points :math:`O` and :math:`P` by doing the
formal changes :math:`y_M\rightarrow y_O`, :math:`y_O\rightarrow y_P`, and :math:`u=(x-x_O)/h_P`,
where :math:`h_P=x_P-x_O`; the :math:`y`-coordinate of the ensuing control point is denoted
by :math:`c_P` (see :numref:`besser_interp`). We look for the values of :math:`c_M` and :math:`c_P` that satisfy the
following conditions: (1) if the sequence :math:`y_M`, :math:`y_O`, :math:`y_P` is monotonic, then
the interpolation is monotonic in the whole interval :math:`[x_M,x_P]`; (2) if the
sequence of :math:`y_i` values is not monotonic, then the interpolant has the only local
extremum at :math:`O`; and (3) the first derivative of the interpolant at point :math:`O`
should be continuous. The ensuing algorithm proceeds as follows:



1. Calculate the quantities :math:`d_M=(y_O-y_M)/h_M`, :math:`d_P=(y_P-y_O)/h_P`.

2. If the sequence :math:`y_M`, :math:`y_O`, :math:`y_P` is not monotonic (i.e.,
   if :math:`d_Md_P\le 0`), then set :math:`c_M=c_P=y_O` and exit the
   algorithm. The derivative of the splines at point :math:`O` is equal to zero,
   leading to a local extremum at the central point. 

3. Estimate the derivative at point :math:`O`,

   :math:`{y'}_O = \frac{h_Md_P + h_Pd_M}{h_m+h_p}`

   (see Eq. (7) of Auer, L. (2003) [3]_, and
   references therein).  This derivative is equal to that provided at the same
   point by parabolic interpolation among points :math:`M`, :math:`O`, and
   :math:`P`. Moreover, in contrast with Eq. (12) of Auer, L. (2003) [3]_, it is an
   expression that relates :math:`y'` (e.g., the source function derivative)
   linearly with the :math:`y`-values (e.g., with the source function values).

4. Calculate the initial positions of the control points,
   :math:`c_M=y_O-\frac{h_M}{2}{y'}_O`, and :math:`c_P=y_O+\frac{h_P}{2}{y'}_O`.
   The control points calculated this way lead to a unique parabolic
   interpolation among points :math:`MOP`. If the algorithm is stopped here,
   the resulting formal solver would be equivalent to the standard parabolic interpolation.  

5. Check that :math:`min(y_M,y_O)\le c_M\le max(y_M,y_O)`. If the condition is
   satisfied, then go to step (7), otherwise continue with step (6).

6. If the condition in step (5) is not satisfied, then there is an overshoot of
   the interpolant in the interval :math:`MO`. Set :math:`c_M=y_M`, so that the
   first derivative at :math:`M` is equal to zero and the overshoot is
   corrected. Since the value of :math:`c_P` is not of interest for the formal
   solution between :math:`M` and :math:`O`, exit the algorithm. 

7. Check if :math:`min(y_O,y_P)\le c_P\le max(y_O,y_P)`. If this conditionis not
   satisfied, then set :math:`c_P=y_P` so that the overshoot in the interval
   :math:`OP` is corrected. 

8. Calculate the new derivative at :math:`O`, :math:`{y'}_O=(c_P-y_O)/(h_P/2)`,
   using the corrected value of :math:`c_P` calculated in step (7).

9. Calculate a new :math:`c_M` value to keep the derivative at :math:`O`
   smooth. It is easy to realize that this change cannot produce an overshoot in
   the :math:`MO` interval, hence the solution remains monotonic with a
   continuous derivative. 

Steps (8) and (9) of the above-mentioned algorithm deal with the correction of
the overshoots in the downwind interval followed by modification of the
:math:`c_M` upwind control point value. We have found that it is suitable to
guarantee the smoothness of the derivative, and this can be done with only a
small increase in the computing time with respect to the parabolic method. Our
BESSER interpolation is stable; that is, the interpolant varies smoothly with
smooth changes of the :math:`M`, :math:`O`, and :math:`P` points. No abrupt
changes of the splines occur that could negatively affect the stability of the
iterative method.

In contrast to some other formal solvers based on the idea of quadratic Bézier
splines, our BESSER algorithm guarantees that a monotonic sequence of the
:math:`MOP` points leads to a monotonic interpolant in all situations. This fact
is of critical importance in 2D and 3D grids in which :math:`\tau_{MO}` and
:math:`\tau_{OP}` may differ significantly because of unpredictable
intersections of the grid planes, especially if periodic boundary conditions are
considered. Such large differences often lead to overshoots, unless treated by
BESSER or a similarly suitable strategy.


.. [1] `Kunasz, P. and Auer, L.H. 1988, JQSRT, 39, 67
       <https://ui.adsabs.harvard.edu/abs/1988JQSRT..39...67K/abstract>`_ 

.. [2] `Štěpán, J., and Trujillo Bueno, J. 2013, A&A, 557, A143
       <https://ui.adsabs.harvard.edu/abs/2013A%26A...557A.143S/abstract>`_    

.. [3] `Auer, L. 2003, ASPC 288, 3
       <https://ui.adsabs.harvard.edu/abs/2003ASPC..288....3A/abstract>`_ 
