*************
Introduction
*************

General information
===================

.. _porta_intro:

What is PORTA
---------------

PORTA is a modular computer program for solving three-dimensional (3D),
non-equilibrium radiative transfer problems with massively parallel computers
(see Štěpán, J., and Trujillo Bueno, J. 2013 [1]_). The
present version can be applied for modeling the spectral line polarization
produced by the scattering of anisotropic radiation and the Hanle and Zeeman
effects assuming complete frequency redistribution, either using two-level or
multilevel atomic models. The numerical method of solution used to find the
self-consistent values of the atomic density matrix at each point of the model's
Cartesian grid is based on Jacobi iterative scheme and on a
short-characteristics formal solver of the Stokes-vector transfer equation that
uses monotonic Bézier interpolation. The code can also be applied for computing
the linear polarization of the continuum radiation caused by Rayleigh and
Thomson scattering in 3D models of stellar atmospheres. It can also be applied
to solve the simpler 3D radiative transfer problem of unpolarized radiation in
multilevel systems.  The present public version of PORTA comes with the HDF5
input/output, an advanced graphical user interface, and is released within the
framework of the POLMAG project funded by the European Research Council (see the
`POLMAG webpage <http://research.iac.es/proyecto/polmag/>`_).


Purpose of the code
--------------------

In addition to a 3D radiative transfer code itself suitable for doing applications, 
PORTA provides a flexible framework for further developments. This is because PORTA
has been originally developed in a modular way, with the aim of facilitating the
preparation of new 
modules for solving a broader set of radiative transfer problems in the future
(e.g., the PRD problem in 3D models). The developer manual will be included
in one of the future releases of the code, but 
the users are already encouraged to share their own modules with the community
via our repository.

PORTA is provided with several tools written in C and Python for creating the models,
visualization of the results, and producing publication-level quality
figures.

.. _structure_of_the_code:

Structure of the code
----------------------

The code consists of three main parts:

#. The PORTA wrapper: It runs the main loop of the code and executes the user
   scripts.

#. PORTAL: The library which contains all the functionalities needed for solving
   the radiative transfer problem, most of the I/O operations, parallelization,
   and the Application Programming Interface (API) necessary for most of the
   polarized radiative transfer applications (including the common mathematical
   and physical functions).

#. Modules: The problem-specific libraries implementing the physics of the
   matter-radiation interaction in different physical regimes. Learn
   more about the modules in section :ref:`modules_porta`.


Note on implementation and parallelism
---------------------------------------

The code has been written in the C programming language and it is parallelized
via MPI libraries. In the current version, the parallelization is done using
domain decomposition in the Z coordinate and in the radiation frequency
domain. This design leads to two main advanatages: firstly, in typical problems
the computing time scales almost linearly with the number of CPU cores, i.e.,
twice the number of CPU cores means 50% less wall-clock time. The
parallelization strategy does not affect the convergence rate. Secondly, the
accuracy of the solution is identical for all possible parallelizations. In the
future, we plan to extend the parallelization in the spatial domain in order to
avoid possible memory issues when using extremely large 3D models.



Terms of use
==============

You can use PORTA freely in your scientific research.

In case you publicly present your results obtained with the help of the code, we
ask you to quote the paper `Štěpán, J., and Trujillo Bueno, J. 2013, A&A, 557, A143 <https://ui.adsabs.harvard.edu/abs/2013A%26A...557A.143S/abstract>`_, and to
include a link to the code repository `https://gitlab.com/polmag/PORTA
<https://gitlab.com/polmag/PORTA>`_.

.. todo::
   Include here new paper when done

Authors (in alphabetical order)
===============================

| Tanausú del Pino Alemán 
| Ángel de Vicente
| Jiří Štěpán 
| Javier Trujillo Bueno

The present version of PORTA is released within the framework of the `POLMAG
<http://research.iac.es/proyecto/polmag/>`_ research project funded by the
European Research Council (see http://research.iac.es/proyecto/polmag/)  

.. [1] `Štěpán, J., and Trujillo Bueno, J. 2013, A&A, 557, A143 <https://ui.adsabs.harvard.edu/abs/2013A%26A...557A.143S/abstract>`_

