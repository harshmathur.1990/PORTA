.. _PORTA_files_formats:

*******************************
Appendix A: PORTA files formats
*******************************

.. note::
   For the *binary* formats, the arrays are always stored with *Z* coordinate
   (if present) moving slower, then *Y* coordinate moving faster and then *X*
   moving the fastest. That is, for two-dimensional arrays, for a given *Y*
   value, all *X* elements will be consecutive in the file. Similarly, for a
   three-dimensional array, for a given *Z* value, all its plane elements will
   be consecutive in the file (and within the plane itself, as above, for a given
   *Y* value, all *X* elements will be consecutive in the file).

.. _pmd_format:

PMD files
============

*Binary* format
----------------

The PMD files consist of three parts: *PORTA HEADER*, *MODULE HEADER*, and
*MODULE GRID DATA*. The *PORTA HEADER* has the same format for all PMD files,
but the *MODULE HEADER* and the *MODULE GRID DATA* are module dependent.

The format for the common part, the *PORTA HEADER* is as follows:

.. include:: pmd_porta_header.txt

The description of the format for the module specific parts, *MODULE HEADER* and
*MODULE_GRID_DATA* is given below for each module.

Twolevel module
^^^^^^^^^^^^^^^^

Description for this module can be found in section :ref:`twolevel-module`

.. include:: pmd_twolevel.txt

	     
Multilevel module
^^^^^^^^^^^^^^^^^^

Description for this module can be found in section :ref:`multilevel-module`

.. include:: pmd_multilevel.txt


Contpol module
^^^^^^^^^^^^^^^^^^
Description for this module can be found in section :ref:`contpol-module`

.. include:: pmd_contpol.txt


HDF5 format
-----------

The PMD files with the HDF5 format consist of three parts:

  1. attributes for the "/" group (equivalent to *PORTA HEADER* in the *binary* format)
  2. attributes for the "/Module" group, (equivalent to the *MODULE HEADER* in
     the *binary* format)
  3. datasets for the "/Module" group, (equivalente to the *MODULE GRID DATA* in
     the *binary* format)


.. note::
   HDF5 is a self-describable file format, so given an HDF5 file, its format
   details can be obtained, for example, with the *h5dump* command, as seen
   below:

.. code-block:: bash
		
   $ h5dump -H test.h5
   HDF5 "test.h5" {
   GROUP "/" {
      ATTRIBUTE "AZIMUTH_NODES" {
	 DATATYPE  H5T_STD_I32LE
	 DATASPACE  SIMPLE { ( 1 ) / ( 1 ) }
      }
   [...]

The common part to all PMDs, the "/" group attributes, are the following (type
of data in parenthesis, number of elements in square brackets):

.. include:: pmd_h5_porta_header.txt

The description of the attributes and datasets specific to each module is given
below for each module. 

Twolevel module
^^^^^^^^^^^^^^^^

Description for this module can be found in section :ref:`twolevel-module`

.. include:: pmd_h5_twolevel.txt


Multilevel module
^^^^^^^^^^^^^^^^^^

Description for this module can be found in section :ref:`multilevel-module`

.. include:: pmd_h5_multilevel.txt


Contpol module
^^^^^^^^^^^^^^^^^^

Description for this module can be found in section :ref:`contpol-module`

.. include:: pmd_h5_contpol.txt


.. _psp_format:

PSP files
=============

"Binary" format
------------------

.. include:: psp.txt

HDF5 format
------------------

.. include:: psp_h5.txt

.. _tau_format:

TAU files
==============

"Binary" format
------------------

.. include:: tau.txt
	     
HDF5 format
------------------

.. include:: tau_h5.txt
