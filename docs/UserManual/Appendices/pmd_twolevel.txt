.. code-block:: none

   |-------------------------------------------------------------------------------|
   | ==============================================================================|
   | PORTA HEADER                                                                  |
   | ==============================================================================|
   |--------------------------+----------------------------------------------------|
   | [...]                                                                         |
   | ==============================================================================|
   | TWOLEVEL MODULE HEADER                                                        |
   | ==============================================================================|
   |--------------------------+----------------------------------------------------|
   | (int)                    | Module version                                     |   
   | (double)                 | Atomic mass in AMU                                 |
   | (double)                 | Einstein coefficient for spontaneous emission      |
   | (double)                 | Energy of transition in erg                        |
   | 2 x (int)                | Angular momentum of atomic levels                  |
   |                          |    (multiplied by two): jl2, ju2                   |
   | 2 x (double)             | Lande factors                                      |
   | (double)                 | Reference temperature that determines the          |
   |                          |    Doppler width to                                |
   |                          |    build the frequency axis                        |
   | ny x nx x (double)       | Temperature at the bottom layer                    |
   | ==============================================================================|
   | TWOLEVEL MODULE GRID DATA                                                     |
   | ==============================================================================|
   |--------------------------+----------------------------------------------------|
   | nz x ny x nx x node_data | Data structure (node_data) for each node           |
   |                          |    specified below.                                |
   |                          |    DM_size = 2 * ( (jl2+1)^2 + (ju2+1)^2 )         |
   |                          |                                                    |
   |  node_data:              |                                                    |
   |   (double)               | photon destruction probability                     |
   |   (double)               | temperature                                        |
   |   (double)               | density                                            |
   |   3 x (double)           | magnetic field (Bx, By, Bz)                        |
   |   3 x (double)           | velocity field (Vx, Vy, Vz)                        |
   |   DM_size x (double)     | Density matrix elements                            |
   |   9 x (double)           | Radiation field tensors                            |
   |   (double)               | Damping parameter                                  |
   |   (double)               | Depolarization rate for elastic collisions with    |
   |                          |    neutral hydrogen                                |
   |   (double)               | Continuum opacity                                  |
   |   (double)               | Continuum emissivity                               |
   |-------------------------------------------------------------------------------|

