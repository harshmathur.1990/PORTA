.. _running_porta:

Running the code
================

Running PORTA (after a successful :ref:`installation_porta`) is very simple: we
simply run a command like the following one in the directory where the *porta*
executable is located:

.. code-block:: bash

   mpirun -n (1+L*M) ./porta (L) test.por

+ where *L* represents the number of vertical blocks we want to divide the
  atmosphere in, and *M* is the number of frequency bands we would like to divide
  the frequencies into. This is discussed in more detail in section
  `Parallelization in PORTA`_

+ *test.por* is a text file where we specify the different commands we want
  PORTA to run, such as loading an atmospheric model, applying a number of Jacobi
  iterations, etc. A description of all the possible PORTA commands and their
  syntax is given in section `PORTA commands`_

.. _parallelization_porta:
  
Parallelization in PORTA
-------------------------

As already explained in section :ref:`porta_intro`, PORTA solves the non-LTE
radiative transfer equations in a rectilinear 3D Cartesian grid, and it can be
parallelized by:

+ dividing the spatial domain in a number of blocks (L) in the vertical dimension,
+ dividing the number of radiation frequencies in a number of frequency bands
  (M).

Given these divisions, the number of *slave* processes that PORTA will use is
:math:`L*M`. PORTA also runs a *master* process and, therefore, the total number
of processes that PORTA will require is :math:`1 + L*M`. (*For details about the
subdivision of work amongst the processes, see* Štěpán, J., and Trujillo
Bueno, J. 2013 [1]_).

The parallelization in the spatial domain can be seen in the following image,
where a 3D model with a grid of 10 points in the vertical dimension (*Z*) has
been divided in three blocks. 
  
.. figure:: ../_static/img/paral.png
   :width: 40%

   PORTA spatial domain parallelization
	   
At the same time, the radiation frequencies can be divided in a number of
frequency bands. Thus, the computations required for a spatial domain block can
be further subdivided and performed by a number of different processes, each
taking care of a disjoint set of frequencies.

Sample executions
.................

For example, in the provided test for the *twolevel* module (see section
:ref:`twoleveltest`), the atmosphere is given by a 3x3x70 grid and the default
number of frequencies is 101. Thus, several example possibilities for running
this test with PORTA could be:

+ Run the test with only 1 *slave* process (i.e., no parallelization in the
  spatial domain nor the radiation frequencies)
  
.. code-block:: bash

   mpirun -n 2 ./porta 1 test.por

+ Parallelize only in the spatial dimension (i.e., each process will be in
  charge of the computations for all frequencies in its given domain block). In
  the following example, we divide the domain in 7 blocks:

.. code-block:: bash

   mpirun -n 8 ./porta 7 test.por

+ Parallelize in both the spatial dimension and the frequencies. If keeping the
  7 vertical blocks as above, but also subdividing the frequencies in 10 bands,
  then we would run PORTA with 70 *slave* processes:

.. code-block:: bash

   mpirun -n 71 ./porta 7 test.por

.. todo::
   Run some basic benchmarks on possible divisions and give here? some hints on
   the best way to parallelize PORTA

.. _porta_commands:   

PORTA commands
--------------

In the examples above, the file *test.por* is a text file where we specify the
different commands that we want PORTA to run. A sample commands file could be
something like:

.. code-block:: none

   # Sample PORTA commands file
   #
   loadmodel_h5 pmd_files/twolevel.h5
   solve_jacobi   10   1.0e-4
   savemodel_h5 pmd_files/twolevel_iter10.h5

   loadmodel_h5 pmd_files/twolevel_iter10.h5
   fs_surface_h5 0 0 0 100 psp_files/twolevel_iter10.h5
   

.. note::
   Lines starting with the character *#* are treated as comments and ignored by
   PORTA.

   
The complete list of available PORTA commands is given in the table below and
further details for each command are given in the ensuing sections.

Some of the commands that perform read/write operations to binary files have two
versions, shown in the command names with the suffix *[_h5]*, to indicate the
optional part. One of the commands reads/generates the files in the original
binary formats of PORTA, and the other one uses the HDF5 file format. For
example, the command to load an atmosphere model from a file can be either
*loadmodel* or *loadmodel_h5*. Further details about the PORTA file formats are
available in section :ref:`PORTA_file_formats`.

.. tabularcolumns:: |l|p{13cm}|

+-----------------------+-------------------------------------------------------------+
| Name                  | Description                                                 |
+=======================+=============================================================+
| `help`_               | Prints the list of the available commands and variables     | 
+-----------------------+-------------------------------------------------------------+
| `version`_            | Prints the PORTA version information                        |
+-----------------------+-------------------------------------------------------------+
| `echo`_               | Prints ('echoes') some arbitrary text                       | 
+-----------------------+-------------------------------------------------------------+
| `exit`_               | Stops processing the commands script                        |
+-----------------------+-------------------------------------------------------------+
| `get`_                | Prints the value of some variables (number of angles,       |
|                       | wavelengths, etc.)                                          | 
+-----------------------+-------------------------------------------------------------+
| `set`_                | Sets the value of some variables (number of angles, etc,    | 
+-----------------------+-------------------------------------------------------------+
| `loadmodel[_h5]`_     | Loads a model from a file                                   |
+-----------------------+-------------------------------------------------------------+
| `savemodel[_h5]`_     | Saves the curent model into a file                          |
+-----------------------+-------------------------------------------------------------+
| `solve_jacobi`_       | Solves the non-LTE problem using Jacobi iteration           |
+-----------------------+-------------------------------------------------------------+
| `fs_surface[_h5]`_    | Performs the formal solution along a general ray            |
|                       | direction for all/some frequencies and stores the emergent  |
|                       | radiation in a file.                                        |
|                       | It uses the *short characteristics* method.                 |
+-----------------------+-------------------------------------------------------------+
| `fs_surface_lc[_h5]`_ | As `fs_surface[_h5]`_, but using the *long characteristics* |
|                       | method.                                                     |
+-----------------------+-------------------------------------------------------------+
| `fs_clv_freq`_        | Performs the formal solution for the calculation            |
|                       | of the center-to-limb variation at a given discrete         |
|                       | frequency of radiation, spatially averaged.                 |
+-----------------------+-------------------------------------------------------------+
| `fs_clv_freq_av`_     | Performs the formal solution for the calculation            |
|                       | of the center-to-limb variation at a given discrete         |
|                       | frequency of radiation, spatially and azimuth averaged.     | 
+-----------------------+-------------------------------------------------------------+
| `fs_clv_freq_av_lc`_  | As `fs_clv_freq_av`_, but using the *long characteristics*  |
|                       | method                                                      |
+-----------------------+-------------------------------------------------------------+
| `fs_azaver`_          | Performs the formal solution, averaged over all grid        |
|                       | points and all azimuths. It results in a single Stokes      |
|                       | profile                                                     |
+-----------------------+-------------------------------------------------------------+
| `get_tau_pos[_h5]`_   | For a general ray direction calculates geometrical          |
|                       | coordinates of points with a given tau distance from the    | 
|                       | surface nodes.                                              |
+-----------------------+-------------------------------------------------------------+


help
.............

+ **Syntax**: help

+ **Description**: Prints:
  
  + the list of the available PORTA commands, and
  + the list of variables that can be queried or set with the `get`_ and `set`_ commands.

  
version
.............

+ **Syntax**: version

+ **Description**: Prints the PORTA version and copyright information

  
echo
.............

+ **Syntax**: echo *text*

+ **Description**: Prints ('echoes') some arbitrary *text*


exit
.............

+ **Syntax**: exit

+ **Description**: Stops processing the commands script


get
.............

+ **Syntax**: get *var*

+ **Description**: Prints the value of variable *var*. The names of the
  variables that can be queried with this command can be obtained with the
  command `help`_ and are the following:

  + nincl:  number of inclination angles per octant 
  + nazim: number of azimuthal angles per octant 
  + stack_out: stack output  
  + nfreq: number of radiation frequencies 
  + nnodes: total number of grid nodes in the model
  + domain: domain origin and dimensions
  + period: horizontal periodicity of the domain
  + wavelengths: radiation wavelengths
    
.. todo::
   does stack_out work?
    
set
.............

+ **Syntax**: set *var* *value*

+ **Description**: Set variable *var* to *value*. *value* is a scalar, except
  for the variable *domain*, explained below. The names of the
  variables that can be set with this command is a subset of those obtained with the
  command `help`_ and are the following:

  + nincl: set the number of inclination angles per octant
  + nazim: set the number of azimuthal angles per octant
  + stack_out: set stack output
  + domain: set the domain geometry, by providing the domain origins and
    dimensions. The syntax for setting the domain is:

    + *set domain Ox Oy Oz Dx Dy Dz*,
      where Ox, Oy, Oz represent the domain origin in X,Y and Z coordinates
      respectively, and Dx, Dy, Dz the domain dimensions.  

.. todo::
   set period 0|1 is accepted, but for the moment we restrict PORTA to work with
   boundary conditions, so we do not include set period description here, since
   it doesn't make much sense. Add later on when checked with non-periodic
   boundaries.

.. todo::
   what does setting stack_out do?

.. todo::
   does setting the domain geometry work?
   
loadmodel[_h5]
...............

+ **Syntax**: loadmodel[_h5] *file*

+ **Description**: Loads a PORTA MODEL DATA (PMD) *file*:

  + loadmodel: Reads a PMD file in the original PORTA binary format
  + loadmodel_h5: Reads a PMD file in the newer HDF5 file format

  Further details about the PORTA file formats are available in section
  :ref:`PORTA_file_formats`. 

    
savemodel[_h5]
................

+ **Syntax**: savemodel[_h5] *file*

+ **Description**: Saves the curent model into a PMD *file*:
  
  + savemodel: Saves a PMD file in the original PORTA binary format
  + savemodel_h5: Saves a PMD file in the newer HDF5 file format

  Further details about the PORTA file formats are available in section
  :ref:`PORTA_file_formats`. 

.. note::
   Conversion between the original PORTA binary format and the newer HDF5 file
   format can be performed with PORTA by running commands like the following:

.. code-block:: none

   loadmodel test.pmd
   savemodel_h5 test.h5
       
solve_jacobi
.............

+ **Syntax**: solve_jacobi *max_iterations* *max_rc*

+ **Description**: Solves the non-LTE problem using Jacobi iteration.

  + *max_iterations* is the maximum number of Jacobi iterations to perform.
  + *max_rc* is the threshold for the relative change of atomic populations
    between consecutive iterations. 
    
PORTA will continue performing Jacobi iterations until one of the above criteria
(*max_iterations* or *max_rc*) is reached, at which point PORTA will stop. 
  

fs_surface[_h5]
................

+ **Syntax**: fs_surface[_h5] *incl* *azim* *fr_i_min* *fr_i_max* *file*

+ **Description**: Performs the formal solution along a general ray direction
  for all/some frequencies and stores in a file the emergent Stokes profiles for
  each point of the model's surface. It uses the *short characteristics* method
  with monotonic Bézier interpolation.

  + *fr_i_min* and *fr_i_max* are the starting and ending frequency **indices**,
    respectively, for which the emergent radiation is computed. (*The indices
    for all frequencies can be obtained, for example, with the "get wavelengths"
    command*) 
  + *incl* and *azim* are the inclination and azimuth angles (in degrees),
    respectively, of the line of sight for which the emergent radiation will be
    computed.
  + The emergent radiation will be stored in a PORTA SPECTRUM PROFILE (PSP)
    *file*

    + fs_surface will save the PSP file in the original PORTA binary format
    + fs_surface_h5 will save the PSP file in the newer HDF5 file format

      Further details about the PORTA file formats are available in section
      :ref:`PORTA_file_formats`.  

     

fs_surface_lc[_h5]
...................

+ **Syntax**: fs_surface_lc[_h5] *incl* *azim* *fr_i_min* *fr_i_max* *file*

+ **Description**: As `fs_surface[_h5]`_, but using the *long characteristics* 
  method.    

.. note::
   Details about the *short characteristics* and the *long characteristics*
   methods are given in section :ref:`sc_vs_lc`.
  
fs_clv_freq
.............

+ **Syntax**: fs_clv_freq *azim* *mu_min* *mu_max* *n_dirs* *freq_i* *file*

+ **Description**: Performs the formal solution for the calculation of the
  center-to-limb variation of the emergent Stokes profiles at a given discrete
  frequency of radiation and azimuth, spatially averaged.

  + *n_dirs* is the number of directions to take, equally spaced, in the range
    of *mu_min* :math:`\le \mu \le` *mu_max* (:math:`\mu = 0` being the limb and
    :math:`\mu = 1` the disk center).
  + *azim* is the azimuth angle (in degrees)
  + *freq_i*  is the frequency **index** for which the center-to-limb variation
    will be computed (*The indices
    for all frequencies can be obtained, for example, with the "get wavelengths"
    command*).
  + The center-to-limb variation results will be stored in plain text in *file*,
    with *n_dirs* lines, where each one will have five values: the :math:`\mu`
    value and the four Stokes values, where Q,U and V are given as a percentage
    over I. That, is, each line in *file* will be given by:
    
    + :math:`\mu` *I Q/I[%] U/I[%] V/I[%]*
  
.. warning::
   Only for horizontally equidistant grids.

    
fs_clv_freq_av
................

+ **Syntax**: fs_clv_freq_av *mu_min* *mu_max* *n_dirs* *n_azim* *freq_i* *file*

+ **Description**: Performs the formal solution for the calculation of the
  center-to-limb variation of the emergent Stokes profiles at a given discrete
  frequency of radiation, spatially and azimuth averaged. Similar to
  `fs_clv_freq`, but instead of specifying a particular azimuth angle, equally
  spaced *n_azim* azimuth angles will be taken by PORTA for doing the average.

  Output in *file* is as per the description of `fs_clv_freq`.
  
.. warning::
   Only for horizontally equidistant grids.

fs_clv_freq_av_lc
...................

+ **Syntax**: fs_clv_freq_av_lc *mu_min* *mu_max* *n_dirs* *n_azim* *freq_i* *file*

+ **Description**: As `fs_clv_freq_av`_, but using the *long characteristics*
  method 

.. warning::
   Only for horizontally equidistant grids.

.. note::
   Details about the *short characteristics* and the *long characteristics*
   methods are given in section :ref:`sc_vs_lc`.
   
fs_azaver
.............

+ **Syntax**: fs_azaver *incl* *n_azim* *fr_i_min* *fr_i_max* *file*

+ **Description**: Performs the formal solution, and provides the emergent
  Stokes profiles averaged over all grid points and all azimuths.

  + *incl* is the inclination angle (in degrees), of the line of sight.
  + *n_azim* is the number of equally spaced azimuth angles to be considered for
    the calculation.
  + *fr_i_min* and *fr_i_max* are the starting and ending frequency **indices**,
    respectively.
  + The output will be stored in plain text in *file*, where each line will have
    five values: the :math:`\lambda` frequency value and the four Stokes
    values. That, is, each line in *file* will be given by: 
    
    + :math:`\lambda` *I Q U V*
  
.. todo::
   Verify description OK?
 
get_tau_pos[_h5]
.................

+ **Syntax**: get_tau_pos *tau* *incl* *azim* *fr_i_min* *fr_i_max* *file*

+ **Description**: For a general ray direction calculates geometrical
  coordinates of points with a given :math:`\tau` distance from the surface
  nodes. It stores the data in a *.TAU file.  

  + *incl* and *azim* are the inclination and azimuth angles (in degrees),
    respectively, of the line of sight for which the coordinates of :math:`\tau
    =` *tau* are to be calculated.
  + *fr_i_min* and *fr_i_max* are the starting and ending frequency **indices**,
    respectively, for which the :math:`\tau` coordinates are to be calculated.
  + The results are stored in *file*, with PORTA TAU format, which basically
    will store for each wavelength, the actual :math:`\tau` value and the 3D
    coordinates with the given :math:`\tau =` *tau*.

    + get_tau_pos will save the TAU file in the original PORTA binary format
    + get_tau_pos_h5 will save the TAU file in the newer HDF5 file format

  Further details about the PORTA file formats are available in section
  :ref:`PORTA_file_formats`.  

.. todo::
   Should the GUI be able to display these TAU files somehow?
       
.. _PORTA_file_formats:

PORTA files
-------------------

PORTA uses three different types of files:

+ PORTA MODEL DATA (PMD) files. Used with commands `loadmodel[_h5]`_ and
  `savemodel[_h5]`_ to read or write an atmosphere model.
+ PORTA SPECTRUM PROFILE (PSP) files. Used with commands `fs_surface[_h5]`_ and
  `fs_surface_lc[_h5]`_ to store the emergent radiation.
+ PORTA TAU (TAU) files. Used with commands `get_tau_pos[_h5]`_ to store the
  geometrical coordinates of points with a given :math:`\tau` distance from the
  surface.

Each of these files can be formatted using the original PORTA
binary formats or the newer HDF5 file format.

The technical details of each format for each file type is given in Appendix
:ref:`PORTA_files_formats`. The directories for the tests for the three different
modules include *Python* scripts that generate PMD files (both in "binary" and
in HDF5 formats), which can help as starting point for the generation of new PMD
files by PORTA users. Details of the mentioned tests and how to run them are in
section :ref:`public-tests`.

PSP and TAU files are generated by PORTA as output of the above-mentioned
commands.  

The included PORTA GUI visualization tool (see section
:ref:`visualization_PORTA`) can help with the visualization of PMD files (for
the three modules in this version of PORTA) and PSP files.

.. todo::
   Fill in the appendix with the technical details of each file type

Sample run
-----------

Once PORTA has been compiled and we have a PMD atmosphere file, we can run PORTA
with a commands file. In section :ref:`public-tests` we will show the
steps in more detail, but below we can see a possible commands script to test
the multilevel module:

.. code-block:: none

   loadmodel_h5 test.h5
   solve_jacobi   10   1.0e-4
   savemodel_h5 test_j10.h5

   loadmodel_h5 test_j10.h5
   fs_surface_h5 0 0 0 100 test_psp.h5

When PORTA runs with this script, we should get two new files in the same directory where we
run the code: *test_j10.h5* and *test_psp.h5*, and we should get output
information similar to the following:

.. code-block:: none

   $ mpirun -np 5 ./porta 4 testh5.por
   [0] ## Porta 0.1.0  (build Dec 10 2019, 16:52:43)
   [0] ## Running 4 slave processes (4 subdomains, 1 frequency bands)
   [0] ## initializing global variables
   [1] ## initializing global variables
   [0] ## generating directions quadrature [1 x 1] per octant
   [1] ## generating directions quadrature [1 x 1] per octant
   [0] ## loading a PMD (HDF5) model from test.h5
   [1] ## generating directions quadrature [4 x 2] per octant
   [0] ## generating directions quadrature [4 x 2] per octant
   [0] ## creating a new grid with 3 x 3 x 70 nodes
   [1] ## Subdomain [1] z-indices: 0--18 (19 in total)
   [2] ## Subdomain [2] z-indices: 18--35 (18 in total)
   [0] ## loading the module multilevel
   [4] ## Subdomain [4] z-indices: 52--69 (18 in total)
   [3] ## Subdomain [3] z-indices: 35--52 (18 in total)
   [1] ## domain dimensions: 3.479145e+03 x 3.479145e+03 x 2.319430e+03 km^3
   [0] ## Loaded: libmultilevel.so
   [0] ## setting 502 frequencies
   [1] ## setting 502 frequencies
   [0] ## loading nodes data for 3x3x70 nodes
   [0] ## test.h5
   [1] STARTING JACOBI ITERATION WITH 10 ITERS. AND MAX. REL. CHANGE 1.000000e-04, \
       T = 0.000000e+00 sec
   [1]    1	7.000000e+00	2.684213e-02
   [1]    2	1.500000e+01	1.035172e-02
   [1]    3	2.200000e+01	1.483056e-03
   [1]    4	3.000000e+01	6.878365e-04
   [1]    5	3.700000e+01	4.897980e-04
   [1]    6	4.400000e+01	3.713853e-04
   [1]    7	5.200000e+01	3.515242e-04
   [1]    8	5.900000e+01	3.337930e-04
   [1]    9	6.700000e+01	3.149104e-04
   [1]   10	7.400000e+01	2.949745e-04
   [0] Jacobi iteration finished. Maximum relative change of population: 2.949745e-04.
   [0] ## saving a PMD model to the file test_j10.h5
   [0] ## loading a PMD (HDF5) model from test_j10.h5
   [0] ## releasing grid 0
   [0] ## unlinking the module multilevel
   [0] ## generating directions quadrature [4 x 2] per octant
   [1] ## generating directions quadrature [4 x 2] per octant
   [0] ## creating a new grid with 3 x 3 x 70 nodes
   [1] ## Subdomain [1] z-indices: 0--18 (19 in total)
   [2] ## Subdomain [2] z-indices: 18--35 (18 in total)
   [4] ## Subdomain [4] z-indices: 52--69 (18 in total)
   [0] ## loading the module multilevel
   [3] ## Subdomain [3] z-indices: 35--52 (18 in total)
   [1] ## domain dimensions: 3.479145e+03 x 3.479145e+03 x 2.319430e+03 km^3
   [0] ## Loaded: libmultilevel.so
   [0] ## setting 502 frequencies
   [1] ## setting 502 frequencies
   [0] ## loading nodes data for 3x3x70 nodes
   [0] ## test_j10.h5
   [0] ## calculating formal solution at the model surface...
   [0] ## exiting Porta
   [0] ## releasing grid 0
   [0] ## unlinking the module multilevel
   [0] ## bye
   $

		
For each run, PORTA also generates a more detailed *porta.log* file, which can be
useful for debugging purposes. The first few lines of this file for this
run were:

.. code-block:: none

   [1] @ Tue Dec 10 16:54:25 2019: initializing global variables
   [0] @ Tue Dec 10 16:54:25 2019: initializing global variables
   [0] @ Tue Dec 10 16:54:25 2019: generating directions quadrature [1 x 1] per octant
   [1] @ Tue Dec 10 16:54:25 2019: generating directions quadrature [1 x 1] per octant
   [1] @ Tue Dec 10 16:54:25 2019: process [1] received signal 4001 from the \
       master process
   [4] @ Tue Dec 10 16:54:25 2019: process [4] received signal 4001 from the \
       master process
   [2] @ Tue Dec 10 16:54:25 2019: process [2] received signal 4001 from the \
       master process
   [3] @ Tue Dec 10 16:54:25 2019: process [3] received signal 4001 from the \
       master process
   [0] @ Tue Dec 10 16:54:25 2019: loading a PMD (HDF5) model from test.h5
   [1] @ Tue Dec 10 16:54:25 2019: loading a PMD model
   [2] @ Tue Dec 10 16:54:25 2019: loading a PMD model
   [3] @ Tue Dec 10 16:54:25 2019: loading a PMD model
   [4] @ Tue Dec 10 16:54:25 2019: loading a PMD model
   [0] @ Tue Dec 10 16:54:25 2019: domain size: (3.479145000000e+03, \
       3.479145000000e+03, 2.319430000000e+03) km
   [0] @ Tue Dec 10 16:54:25 2019: domain origin: (0.000000000000e+00, \
       0.000000000000e+00, -1.000000000000e+02) km
   [0] @ Tue Dec 10 16:54:25 2019: (nx,ny,nz): (3,3,70)
   [0] @ Tue Dec 10 16:54:25 2019: mix(x), max(x): 0.000000000000e+00, \
       2.319430000000e+03 km
   [0] @ Tue Dec 10 16:54:25 2019: mix(y), max(y): 0.000000000000e+00, \
       2.319430000000e+03 km
   [0] @ Tue Dec 10 16:54:25 2019: mix(z), max(z): -1.000000000000e+02, \
       2.219430000000e+03 km


.. note::
   The numbers in square brackets at the beginning of each line for the output
   and log files generated by PORTA represent the MPI rank of the process
   generating that output information, which can also be useful for debugging.

.. [1] `Štěpán, J., and Trujillo Bueno, J. 2013, A&A, 557, A143 <https://ui.adsabs.harvard.edu/abs/2013A%26A...557A.143S/abstract>`_






  

   
