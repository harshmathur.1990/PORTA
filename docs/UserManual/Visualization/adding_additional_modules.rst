Adding Additional Modules
.........................

Users can add their own widgets to manage and visualize *pmd* data for their
own modules. In order for the *pmd* GUI to recognize a module, there must be
a file, called ``*module_name.py*'' in the source folder, with ``module_name''
the name of the module as it is in the header of the corresponding *pmd* file,
with the following dependences and class structure:

.. code-block:: python


    import source.general
    from source.loader import *
    from source.tooltip import *

    class MODULE_class(Toplevel):
	''' Class that defines the main body of the module class
	'''

	def __init__(self, parent, title = None):
	    ''' Initializes the widget single tab
	    '''

	    global CONF
	    CONF = source.general.CONF

	    # Initialized a support window.
	    Toplevel.__init__(self, parent)
	    self.transient(parent)

	    # Creates a font to use in the widget
	    self.cFont = tkFont.Font(family="Helvetica", \
				     size=CONF['wfont_size'])

	    # Set the title if any
	    if title:
		self.title(title)


	    self.parent = parent
	    self.result = None
	    self.plt_action = None

	    head = Frame(self)
	    body = Frame(head)

	    --- User widgets in a frame in body ---

	    head.pack(padx=5, pady=5)
	    body.pack(fill=BOTH,expand=1)

	    self.grab_set()

	    if not self.initial_focus:
		self.initial_focus = self

	    self.buttons(self.do)

	    self.display(self.mid)

	    self.optionbox(self.up)

	    self.progressbar(self.bot)

	    self.up.pack(fill=BOTH,expand=1,side=TOP)
	    self.mid.pack(fill=BOTH,expand=1,side=TOP)
	    self.do.pack(fill=BOTH,expand=1,side=BOTTOM)
	    self.bot.pack(fill=BOTH,expand=1,side=BOTTOM)
	    body.pack(fill=BOTH,expand=1)
	    head.pack(fill=BOTH,expand=1)

	    self.protocol("WM_DELETE_WINDOW", self.close)
	    self.bind("<Escape>", self.close)

	    self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
				      parent.winfo_rooty()+50))

	    self.initial_focus.focus_set()

	    self.wait_window(self)

	def body(self, master):
	    ''' Creates the dialog body and set initial focus.
	    '''

	    return master

	def close(self, event=None):
	    ''' Method that handles the window closing.
	    '''

	    # put focus back to the parent window
	    self.parent.focus_set()
	    self.destroy()


