.. _modules_porta:

**************
Modules
**************

Modular structure of the code
==============================

From the very beginning, PORTA has been designed as a versatile radiative
transfer code; i.e., keeping in mind that users may wish to apply it to
different astrophysical problems for which the use of Cartesian grids is
suitable.

Building a monolithic code that would implement all the physical and geometrical
combinations is clearly an unsuitable strategy. Instead, PORTA has been designed
such that the main code library (PORTAL) implements the functionality for
solving the radiative transfer equation and all the related issues of
parallelization, input/output, etc. The modules implement the microscopic
physics of the atom-photon interaction. In particular, the modules are required
to provide to the main library the vector of emission coefficients and the
propagation matrix at each spatial grid point, direction and radiation
frequency. Subsequently, they are provided with information from the main
library about the Stokes parameters for all the directions and
frequencies. These data are subsequently processed by the module according to
the used physical model of light-matter interaction.

This way, the developers of the modules are free to implement any possible
physical model of the radiation-matter interaction without worrying about the
radiation transfer itself. The only necessity is to know the boundary
illumination of the model and to calculate the emission and absorption
coefficients provided the local radiation field.

PORTA comes already with modules that implement some of the most commonly
considered physical scenarios, such as scattering polarization and the Hanle and
Zeeman effects in multilevel atoms; hence, it is ready to be used right away for
many interesting applications. In addition to that, other modules with more
physical effects are to be expected in the near future (e.g., PRD). Even though
we are still preparing a detailed developer manual, we already invite the
members of the astrophysical community to contribute with their own modules.


.. _public-modules:

List of publicly available modules
===================================

In the following sections, we provide the list and a brief description of the
modules that come with PORTA.

For each of these modules, there is a corresponding test directory where we
provide Python scripts to create the corresponding models, and a basic commands
file to help verify the correctness of each of the modules (for a description of
how to make use of these tests, see section :ref:`public-tests`).

The list of available modules is:

.. tabularcolumns:: |p{.2\textwidth}|p{.8\textwidth}|

+----------------------------+----------------------------------------------------------+
| Name                       | Description                                              |
+============================+==========================================================+
| :ref:`multilevel-module`   | Multilevel atom in CRD: scattering polarization and the  |
|                            | Hanle effect. Optional: Zeeman effect contribution on the|
|                            | emergent Stokes profiles.                                |        
+----------------------------+----------------------------------------------------------+
| :ref:`twolevel-module`     | Twolevel atom in CRD: scattering polarization and the    |
|                            | Hanle effect. Optional: Zeeman effect contribution on the|
|                            | emergent Stokes profiles.                                |        
+----------------------------+----------------------------------------------------------+
| :ref:`contpol-module`      | Continuum polarization                                   |
+----------------------------+----------------------------------------------------------+


.. _multilevel-module:

``multilevel``
--------------------

The multilevel module solves the problem of the generation and transfer of
polarized radiation in spectral lines for a multilevel model atom. The number of
levels that can be included is limited by the available memory and the
computational time, and it is highly dependent on the size of the model
atmosphere.

The transfer problem is solved iteratively in the Hanle regime (see section 7.2
of Landi Degl'Innocenti, E. and Landolfi, M., 2004), taking into account
scattering polarization, with the Jacobi method and making use of the diagonal
Lambda operator.

The formal solution, that is, the computation of the emergent Stokes parameters,
takes into account the joint action of the Hanle and Zeeman effects, the later
under the approximation that the Zeeman splittings do not have a significant
impact on the atomic density matrix elements (a suitable approximation when the
Zeeman splitting is much smaller than the line's thermal width). The module can
be compiled to ignore the Zeeman effect in the formal solver, for which one
should configure PORTA with the ``--no_hz`` option (see
:ref:`porta_compilation`).

The module does not include bound-free transitions. Therefore, the ionization
balance needs to be computed previously to the execution in order to obtain the
number density of the ion we are interested in. Damping parameters, collisional
excitation rates, depolarizing collisional rates, and continuum opacity and
emissivity must be also included for every node in the model atmosphere.

The boundary conditions are periodic in X and Y, black-body illumination for the
radiation coming into the lower boundary, and no illumination for the radiation
coming into the top boundary.

.. comments

   .. note::
      Doxygen-generated API for the ``multilevel`` module can be found `here
      <../multilevel-module-api/index.html>`_

.. _twolevel-module:

``twolevel``
--------------------

The twolevel module solves the problem of the generation and transfer of
spectral line polarization using the two-level model atom.

The transfer problem is solved iteratively in the Hanle regime (see section 7.2
of Landi Degl'Innocenti, E. and Landolfi, M., 2004), taking into account
scattering polarization, with the Jacobi method and making use of the the diagonal
Lambda operator.

The formal solution, that is, the computation of the emergent Stokes parameters,
takes into account the joint action of the Hanle and Zeeman effects, the later
under the approximation that the Zeeman splittings do not have a significant
impact on the atomic density matrix elements (a suitable approximation when the
Zeeman splitting is much smaller than the line's thermal width). The module can
be compiled to ignore the Zeeman effect in the formal solver, for
which one should configure PORTA with the ``--no_hz`` option (see
:ref:`porta_compilation`).

The module does not include bound-free transitions. Therefore, the ionization
balance needs to be computed previously to the execution in order to obtain the
number density of the ion we are interested in. Damping parameters,
collisional excitation rates, depolarizing collisional rates, and continuum
opacity and emissivity must be also included for every node in the model
atmosphere.

The boundary conditions are periodic in X and Y, black-body illumination for the
radiation coming into the lower boundary, and no illumination for the radiation
coming into the top boundary.

.. comments
   
   .. note::
      Doxygen-generated APIs for the ``twolevel`` module can be found `here
      <../twolevel-module-api/index.html>`_

  
.. _contpol-module:

``contpol``
--------------------

The contpol module solves the radiative transfer problem of the linear
polarization produced by scattering processes in the continuum radiation. The
opacity contributions included are:

* Thomson scattering coefficient
* H- bound-free absorption and emission coefficients
* H- free-free absorption coefficient
* H bound-free absorption and emission coefficients
* H free-free absorption coefficient
* H Rayleigh scattering coefficient
* H + proton free-free absorption coefficient 

The ionization and excitation equilibrium of hydrogen to calculate the opacity
is computed in LTE. The hydrogen and electron number densities must be provided.

The transfer problem is solved iteratively with Lambda iteration, taking into
account scattering polarization.

The boundary conditions are perdiodic in X and Y, black-body illumination for the
radiation coming into the lower boundary, and no illumination for the radiation
coming into the top boundary.

.. comments

   .. note::
      Doxygen-generated API for the ``continuum`` module can be found `here
      <../contpol-module-api/index.html>`_


