**********************
Miscellaneous issues
**********************

Support
==========================

For any problem not in the :ref:`faq`, contact:

| Ángel de Vicente:
.. image:: ../_static/img/mail_angel.png
   :width: 22 %

.. note::
   An announcement list (with very low traffic) has been created to send
   information about new versions, updates, etc. If you want to be added to this
   mailing list, please send an e-mail to us.
	   
.. _faq:

Frequently asked questions
==========================

Before contacting us asking a question about PORTA, please check the Q&A list below.

* **What kind of boundary conditions PORTA uses?**

In the current version, the code assumes periodic boundary conditions in the horizontal (X and Y) dimensions. This is due to the fact that the main application domain of PORTA has been a synthesis of spectra in snapshots of the 3D MHD simulations that use the same conditions. However, we are planning to generalize the code for more general scenarios.

* **What system of physical units is used?**

The code consistently works in the CGS system.

The wavelength in the emergent Stokes files are expressed in vacuum Angstrom units.

* **Should I use HDF or the proprietary data format for the PMD files?**

The HDF format is easy to use and platform independent. It the prefered one for many applications. The proprietary data format has been historically the first one implemented in PORTA. We keep it in the code for the cases in which the 3D models are very large and difficulties could arise from using HDF.

Known bugs
==========================

There are no known bugs at the time of writing.
   
