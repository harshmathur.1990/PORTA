.. rst format CheatSheet:
   http://openalea.gforge.inria.fr/doc/openalea/doc/_build/html/source/sphinx/rest_syntax.html#comments-and-aliases 

PORTA
=====

PORTA is a radiative transfer code for the simulation of the spectral line
intensity and polarization caused by scattering processes and the Hanle and
Zeeman effects in 3D models of stellar atmospheres. The numerical method of
solution is based on Jacobi's iterative method and on a short-characteristics
formal solver of the Stokes-vector transfer equation which uses monotonic Bézier
interpolation. A crucial feature of PORTA is its parallelization strategy, which
allows to speed up the numerical solution of complicated 3D problems by several
orders of magnitude with respect to sequential radiative transfer approaches,
given its excellent linear scaling with the number of available
processors. PORTA can also be conveniently applied to solve the 3D radiative
transfer problem of the intensity and polarization of the Sun's continuous
radiation.


.. toctree::
   :maxdepth: 1
   :numbered:
      
   Introduction/index
   Installation/index
   Running/index
   Modules/index
   Tests/index
   Visualization/index
   Besser/index
   Notes_Implementation/index
   Miscellaneous/index
   Publications/index
   Appendices/index

