.. _public-tests:

*******
Tests
*******

As explained in section :ref:`public-modules`, PORTA comes with three modules
to solve some of the most commonly considered physical problems. It also
comes with three sample tests for these three modules.

In the following sections we give details of how to run these tests in order to
become familiar with the operation mode of PORTA, exercise the available
commands and help verify that the modules work correctly.


.. _twoleveltest:

Twolevel module test
=====================

This test concerns the :ref:`twolevel-module` module, and the files
corresponding to this test are located in the directory
``tests/twolevel-module``.

.. _generate-pmd:

Generate the model atmosphere (PMD file)
------------------------------------------

The script ``twolevel-pmd.py`` can be used to generate a test PMD file. You can
see its options by passing the -h option:

.. code-block:: none

   $ ./twolevel-pmd.py -h
   use: python twolevel-pmd.py -h -f file -a atom -A atmosphere
   -h == shows this help
   -f == name of output file
   -a == model atom (Sr)
   -A == model atmosphere (FALC)
   -B == homogeneous magnetic field module in gauss
   -t == homogeneous magnetic field inclination in deg
   -z == homogeneous magnetic field azimuth in deg
   -nx == number of nodes in X axis
   -ny == number of nodes in Y axis
   -Px == period for temperature perturbation or domain size in X axis in km
   -Py == period for temperature perturbation or domain size in Y axis in km
   -Ax == amplitude for temperature perturbation in X axis (absolute)
   -Ay == amplitude for temperature perturbation in Y axis (absolute)
   -Afx == amplitude for temperature perturbation in X axis 
       (as a fraction of model temperature)
   -Afy == amplitude for temperature perturbation in Y axis 
       (as a fraction of model temperature)
   
.. note::
  The PMD file generators for the three module tests work both for Python 2 and
  Python 3.

  
Without any options, the script will create an atmosphere file with 3x3x33
(*nx,ny,nz*) grid points. If the HDF5 library is present, the script will create
two files: ``test.pmd`` and ``test.h5``, having the same data, but in *binary*
and HDF5 formats respectively (see Appendix :ref:`PORTA_files_formats` for
details of both formats).

The variation with height of the temperature, electron density and
microturbulence is given by the FAL-C model (Fontenla et. al. 1993 [1]_). The other
parameters are: Voigt parameter, collisional rate, atomic density, elastic
collisions, background opacity and emissivity. In this test, the spectral line
considered is the Sr I line at 460.7 nm, which can be modeled with the two level
atom approximation. This spectral line is formed in the solar photosphere of the
model atmosphere. With this script we can generate atmospheric models with an
homogeneous magnetic field, with different configurations, defined by the
inclination, azimuth and strength.

As an example, if we consider a 1D model atmosphere without magnetic field or
with a magnetic field parallel to the Z axis and we iterate until getting the
self-consistent solution, we would obtain zero linear polarization in a
disk-center observation.



.. code-block:: none

   $ ./twolevel-pmd.py 
   You can run with the -h option to get the list of possible parameters
   Node size 42 elements and 336 bytes. Expected 336 bytes
   Size of density matrix is 20 elements and 160 bytes

   Your pmd file (binary format) for the atom Sr and 
     atmospheric model FALC has been created and stored 
     in the file: test.pmd

   Your pmd file (HDF5 format) for the atom Sr and 
     atmospheric model FALC has been created and stored 
     in the file: test.h5


By default, the model created by *twolevel-pmd.py* has the same temperature
value for all points of a given plane in *Z* (effectively turning the model into
a 1.5D model), but we can introduce a temperature sinusoidal perturbation at
each height in the model. With options *-nx* and *-ny* we can change the number
of nodes for each plane. With options *-Px* and *-Py* we can specify the period
for the perturbation in each dimension (equal to the domain size). And lastly,
the amplitude of the perturbation can be provided as an absolute value (options
*-Ax* and *-Ay*) or as a fraction of the corresponding FAL-C temperature value
at each height (with options *-Afx* and *-Afy*).

The resulting temperature structure is:

.. math:: T(x,y,z) = \overline{T}(z) + \Delta T_x {\rm cos}\frac{2\pi x}{P_x} + \Delta
	  T_y {\rm cos}\frac{2\pi y}{P_y},

where :math:`\overline{T}` is the temperature of the FAL-C model, and
:math:`\Delta T_x` (and similarly for :math:`\Delta T_y`) is :math:`A_x` (if
option -Ax is used) or :math:`Af_x \overline{T}(z)` (if option -Afx is
used).

For the examples given below, we created a 3D model atmosphere as:

.. code-block:: none

   $ ./twolevel-pmd.py -B 20 -t 45 -z 0 -nx 50 -ny 50 -Px 2000 -Py 2000 -Afx 0.1 -Afy 0.1


.. _inspect-pmd:
   
Inspect the model atmosphere files
------------------------------------------

For a quick view of the data in these *PMD* files, you can use the script
*dump_pmd.py* to print the contents of a *PMD* file, either in "binary" or HDF5
format. For example:

.. code-block:: none

   $ ./dump_pmd.py test.pmd
   Magic String:  portapmd
   Endianness:  0
   Size of integer:  4
   Size of double:  8
   Version of PMD file:  2
   Creation date: 4/3/2020 11:46:6
   Periodicity: X: 1 Y: 1
   Domain size: X: 200000000.0 Y: 200000000.0 Z: 177000000.0
   Domain origin: X: 0.0 Y: 0.0 Z: -10000000.0
   Number of nodes: X: 50 Y: 50 Z: 33
   X array: 
   [0.00e+00 4.00e+06 8.00e+06 1.20e+07 1.60e+07 2.00e+07 2.40e+07 2.80e+07
    [...]
    1.92e+08 1.96e+08]

   Y array: 
   [0.00e+00 4.00e+06 8.00e+06 1.20e+07 1.60e+07 2.00e+07 2.40e+07 2.80e+07
    [...]
    1.92e+08 1.96e+08]

   Z array: 
   [-1.000e+07 -8.000e+06 -6.000e+06 -4.000e+06 -2.000e+06  0.000e+00
     [...]
     1.475e+08  1.580e+08  1.670e+08]

   Number of angles: Inclination: 4 Azimuthal: 2
   Name of Atomic Module: twolevel
   Model Comment: Model for Sr atom in FALC atmospheric model replicated \
       in 50x50 columns for the twolevel module
   Size of the module header in bytes: 20060
   Size of the node data in bytes: 336


   #############################
   ATOMIC MODEL HEADER
   #############################

   TL_VERSION: 2
   ATOM_MASS: 87.62
   A_UL: 201000000.0
   E_UL: 4.310279234095649e-12
   JL2: 0
   JU2: 2
   GL: 0.0
   GU: 1.0
   TEMP: 5000.0
   BC Temp:: 
   [[9400. 9400. 9400. ... 9400. 9400. 9400.]
    [9400. 9400. 9400. ... 9400. 9400. 9400.]
    [9400. 9400. 9400. ... 9400. 9400. 9400.]
    ...
    [9400. 9400. 9400. ... 9400. 9400. 9400.]
    [9400. 9400. 9400. ... 9400. 9400. 9400.]
    [9400. 9400. 9400. ... 9400. 9400. 9400.]]



   #############################
   NODES DATA
   #############################


   NODE 0,0,0:
   --------------------
   eps:     0.7915842017029036
   temp:    11280.0
   density: 56975.04542069
   B:       14.14213562373095 0.0 14.142135623730951
   V:       0.0 0.0 0.0
   dm:      [0.90030001 0.         0.05756181 0.         0.         0.
    0.         0.         0.         0.         0.         0.
    0.         0.         0.         0.         0.         0.
    0.         0.        ]
   jkq:     [0. 0. 0. 0. 0. 0. 0. 0. 0.]
   a_voigt: 0.0629337600906
   delta2:  9.84226174814
   c_opac:  4.76851400443e-06
   c_emis:  9.98938583414e-10

   NODE 0,0,1:
   --------------------
   eps:     0.7915842017029036
   temp:    11272.587819235609
   density: 56975.04542069
   B:       14.14213562373095 0.0 14.142135623730951
   V:       0.0 0.0 0.0
   dm:      [0.90030001 0.         0.05756181 0.         0.         0.
    0.         0.         0.         0.         0.         0.
    0.         0.         0.         0.         0.         0.
    0.         0.        ]
   jkq:     [0. 0. 0. 0. 0. 0. 0. 0. 0.]
   a_voigt: 0.0629337600906
   delta2:  9.84226174814
   c_opac:  4.76851400443e-06
   c_emis:  9.98938583414e-10

       

Though the contents of the HDF5 format files can also be printed with
*dump_pmd.py*, you should better use the `h5dump
<https://portal.hdfgroup.org/display/HDF5/HDF5+Command-line+Tools>`_
command-line tool, or better still the `HDFView
<https://portal.hdfgroup.org/display/HDFVIEW/HDFView>`_ tool, of which a sample
screenshot is given below.

.. figure:: images/hdfview.png
   :width: 100%

   Sample screenshot with the HDFView tool

For more advanced visualization of the *PMD* files, you can use the PORTA
GUI (see section :ref:`visualization_PORTA`).


Compile PORTA and the twolevel library
---------------------------------------------------

After generating the model atmosphere files we need to create the PORTA
executable, together with the corresponding twolevel module (located in
``src/modules/twolevel``), which we would do by issuing the following commands
in the PORTA main directory. These will compile and place the PORTA executable
and libraries in the *tests/twolevel-module* directory:

.. code-block:: bash

    $ ./configure --module=twolevel --prefix=$PWD/tests/twolevel-module/
    generating makefile ...
    configuration complete, type make to build.

    $ make clean ; make

.. note::
   The PORTA wrapper and library are independent of the module library, so they
   could be compiled only once, instead of compiling it for each module test,
   but here for simplicity we show how to compile the three main parts of PORTA
   for each test.

   Although having all the files in the same directory is probably the easiest,
   the dynamic libraries do not really need to be in the same directory as the
   PORTA executable, as far as the system can find them. If you need to have
   them in separate directories, you can use the *LD_LIBRARY_PATH* environment
   variable).

    
.. warning::
   For more details about dependencies and the compilation process and options,
   see section :ref:`installation_porta`


Execute PORTA
---------------

Prepare a commands file
........................

As explained in section :ref:`running_porta`, you run PORTA by specifying a file
with the commands you want PORTA to execute. In the *twolevel-module* test
directory you have the file *test.por*, which executes many of the PORTA
commands (see section :ref:`porta_commands` for a detailed description of each
command). The *test.por* file contents are:

.. code-block:: none

   loadmodel_h5 test.h5
   solve_jacobi   10   1.0e-4
   savemodel_h5 test_out.h5

   loadmodel test.pmd
   solve_jacobi   10   1.0e-4
   savemodel test_out.pmd

   loadmodel_h5 test_out.h5
   fs_surface 0 0 0 100 test_out.psp
   fs_surface_h5 0 0 0 100 test_out_psp.h5
   fs_surface_lc 0 0 0 100 test_out_lc.psp
   fs_surface_lc_h5 0 0 0 100 test_out_lc_psp.h5

   loadmodel test_out.pmd
   get_tau_pos 1.0 0.0 0.0 0 100 test_out.tau
   get_tau_pos_h5 1.0 0.0 0.0 0 100 test_out_tau.h5
   

Launch PORTA in parallel
.........................

PORTA can be parallelized both in the spatial domain and in the frequency
domain (for details and hints on the best way to execute PORTA in parallel, see
section :ref:`parallelization_porta`). For example, to run this test with 28
slaves, 7 vertical blocks and 4 frequency bands, we would issue the command:

.. code-block:: none

   $ mpirun -np 29 ./porta 7 test.por


Executing the simple 1.5D test should take only a few seconds, and a few minutes for
the 3D test (see :ref:`generate-pmd`), and it should have generated:

   + two *PMD* files ("binary" and HDF5) representing atmosphere files after
     10 jacobi iterations: *test_out.h5* and *test_out.pmd*
   + four *PSP* files (two "binary", two HDF5) representing the emergent
     radiation after the 10 jacobi iterations, two calculated with the short
     characteristics method (*test_out.psp* and *test_out_psp.h5*) and two with
     the long characteristics one (*test_out_lc.psp* and *test_out_lc_psp.h5*)
   + two *TAU* files ("binary" and HDF5) representing the surface for which
     :math:`\tau=1.0`: *test_out.tau* and *test_out_tau.h5*.


.. note::
   This sample test is run only with 29 processes, but usually PORTA runs
   will require hundreds of processes, for which we will use a
   supercomputer and we will need to write a submission script to run the code
   via a *Resource Manager*, such as *Slurm*, *LoadLeveler*, *PBS*, etc. You
   will need to read the documentation for your software, but as an example, a
   submission of *PORTA* with *Slurm* could look like the following, in which we
   have to make sure, amongst other things, that the right software is loaded
   before execution so as to match the software used for compilation:

.. code-block:: bash
   
   #!/bin/bash
   #
   #SBATCH -J porta
   #SBATCH -n 197
   #SBATCH -t 01:00:00
   #SBATCH -o porta-test%j.out
   #SBATCH -e porta-test%j.err
   #SBATCH -D .

   module purge
   module load gnu/7.2.0
   module load szip/gnu/2.1.1                   
   module load openmpi/gnu/3.0.1                
   module load hdf5/gnu/openmpi/1.10.1          

   srun ./porta 14 test.por
   
	   
Inspect the PORTA output files
-------------------------------

As a result of the previous run, PORTA would have created the three types of
PORTA files (see section :ref:`PORTA_file_formats`):

   + As we have seen above in section :ref:`inspect-pmd`, *PMD* files can be
     quickly inspected with the *dump_pmd.py* script, with *HDFView* (if in
     HDF5 format), and more specifically with the PORTA GUI. 

   + *PSP* files (which are not module dependent) can similarly be quickly
     inspected with the *dump_psp.py* script (in the main *tests* directory),
     with *h5dump* or *HDFView* (if in HDF5 format), or more specifically with
     the PORTA GUI.

   + *TAU* files (which are not module dependent) can similarly be quickly
     inspected with the  *dump_tau.py* script (in the main *tests* directory),
     or with *h5dump* or *HDFView* (if in HDF5 format).

.. note::
   At this time PORTA GUI is not able to help with the visualization of *TAU*
   files, but we intend to add this functionality in the near future.
   


Below we can see two screenshots of the PORTA GUI being used to visualize the
output *PMD* file *test_out.h5* and the output *PSP* file *test_out_lc_psp.h5*.

.. figure:: images/twolevel_module_test_gui_pmd_temp.png
   :width: 100%
 
   Screenshot of PORTA GUI with visualization of *PMD* output file

.. figure:: images/twolevel_module_test_gui_psp.png
   :width: 100%

   Screenshot of PORTA GUI with visualization of *PSP* output file

.. note::   
   Besides the output files, PORTA will also generate a ``porta.log`` log file in
   the execution directory, which will give details of the files/modules loaded,
   timings, etc.

.. todo::
   In the case of HDF5 input/output files we can also use other methods to
   quickly inspect the structure and data in them. Add at some point reference
   to the h5tovts script or similar.


   
.. _multileveltest:

Multilevel module test
=======================

This test concerns the :ref:`multilevel-module` module, and the files
corresponding to this test are located in the directory
``tests/multilevel-module``. Since the details to run this test are basically
the same as for the :ref:`twoleveltest`, we only mention here the few aspects
that are different.

The script ``multilevel-pmd.py`` can be used to generate a test PMD file. You
can see its options by passing the -h option: 

.. code-block:: none

   $ ./multilevel-pmd.py -h
   use: python multilevel-pmd.py -h -f file -a atom -A atmosphere
   -h == shows this help
   -f == name of output file
   -a == model atom (Ca or Mg [default])
   -A == model atmosphere (FALC)
   -B == homogeneous magnetic field module in gauss
   -t == homogeneous magnetic field inclination in deg
   -z == homogeneous magnetic field azimuth in deg
   -nx == number of nodes in X axis
   -ny == number of nodes in Y axis
   -Px == period for temperature perturbation or domain size in X axis in km
   -Py == period for temperature perturbation or domain size in Y axis in km
   -Ax == amplitude for temperature perturbation in X axis (absolute)
   -Ay == amplitude for temperature perturbation in Y axis (absolute)
   -Afx == amplitude for temperature perturbation in X axis 
       (as a fraction of model temperature)
    
Without any options, the script will create an atmosphere file (1D, FAL-C model)
with 3x3x70 (*nx,ny,nz*) grid points (if the HDF5 libary is present, two files
will be created: on in *binary* format and another one in HDF5 format). Similar
to the ``twolevel-pmd.py`` script, we can also generate a 3D model by
introducing a temperature sinusoidal perturbation (see :ref:`generate-pmd`).

The variation with height of the temperature, electron density and
microturbulence is given by the FAL-C model (Fontenla et. al. 1993 [1]_). The other
parameters are: Voigt parameter, collisional rate, atomic
density, background opacity and emissivity. Either the Mg k line at 280 nm or
the Ca II IR triplet can be selected in this script to perform several tests. A
given magnetic field can be inserted in the atmospheric model through the
corresponding flags of the script. Several results from Manso Sainz, R. and
Trujillo Bueno, J. (2010) [2]_ can be obtained using the atmospheric models generated
with this script.

To compile the module library (and here, for simplicity, also the PORTA wrapper
and library) we would issue the following commands in the PORTA main
directory. These will compile and place the PORTA executable and libraries in
the *tests/multilevel-module* directory:

.. code-block:: bash

    $ ./configure --module=multilevel --prefix=$PWD/tests/multilevel-module/
    generating makefile ...
    configuration complete, type make to build.

    $ make clean ; make


.. _contpoltest:

Contpol module test
=====================

This test concerns the :ref:`contpol-module` module, and the files
corresponding to this test are located in the directory
``tests/contpol-module``. Since the details to run this test are basically
the same as for the :ref:`twoleveltest`, we only mention here the few aspects
that are different.

The script ``contpol-pmd.py`` can be used to generate a test PMD file. You can
see its options by passing the -h option:

.. code-block:: none

   $ ./contpol-pmd.py -h
   use: python contpol-pmd.py -h -f file -l freq
   -h == shows this help
   -f == name of output file
   -l == frequency (in Angstroms) [default: 3100 A]
   -nx == number of nodes in X axis
   -ny == number of nodes in Y axis
   -Px == period for temperature perturbation or domain size in X axis in km
   -Py == period for temperature perturbation or domain size in Y axis in km
   -Ax == amplitude for temperature perturbation in X axis (absolute)
   -Ay == amplitude for temperature perturbation in Y axis (absolute)
   -Afx == amplitude for temperature perturbation in X axis 
       (as a fraction of model temperature)
   -Afy == amplitude for temperature perturbation in Y axis 
       (as a fraction of model temperature)
    
Without any options, the script will create an atmosphere file (1D, FAL-C model)
with 3x3x70 (*nx,ny,nz*) grid points (if the HDF5 libary is present, two files will be
created: on in *binary* format and another one in HDF5 format). Similar to the
``twolevel-pmd.py`` script, we can also generate a 3D model by introducing a
temperature sinusoidal perturbation (see :ref:`generate-pmd`).

The variation with height of the temperature, electron density
and hydrogen density is given by the FAL-C model (Fontenla et. al. 1993 [1]_). This
module is one-frequency based, and the default frequency for this script is 310
nm. This frequency can be changed via the *l* argument to the script (see the
usage information with the flag ‘-h’). With this model, one can check several
results obtained in Fluri, D. M. and Stenflo, J. O. (1999) [3]_.

To compile the module library (and here, for simplicity, also the PORTA wrapper
and library) we would issue the following commands in the PORTA main
directory. These will compile and place the PORTA executable and libraries in
the *tests/contpol-module* directory:

.. code-block:: bash

    $ ./configure --module=contpol --prefix=$PWD/tests/contpol-module/
    generating makefile ...
    configuration complete, type make to build.

    $ make clean ; make


   
.. [1] `Fontenla, J. M., Avrett, E. H., and Loeser, R. 1993, ApJ, 406, 319 <https://ui.adsabs.harvard.edu/abs/1993ApJ...406..319F/abstract>`_ 
.. [2] `Manso Sainz, R. and Trujillo Bueno, J. 2010, ApJ, 722, 1416 <https://ui.adsabs.harvard.edu/abs/2010ApJ...722.1416M/abstract>`_       
.. [3] `Fluri, D. M. and Stenflo, J. O. 1999 A&A, 341, 902 <https://ui.adsabs.harvard.edu/abs/1999A%26A...341..902F/abstract>`_. 
