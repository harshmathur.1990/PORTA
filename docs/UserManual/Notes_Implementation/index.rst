.. _notes_implementation

Notes on other implementation issues
====================================

Angular quadratures
--------------------

PORTA uses a tensor-product quadrature, namely Gaussian in the cosine of the
inclinations with respect to the Z axis and the equidistant trapezoidal rule for
integration in azimuths. The number of directions in inclinations and azimuths
can be set in the POR script by ``set nincl N`` and ``set nazim M``,
respectively, for the quadrature with ``N*M`` directions per octant. For most
applications, we recommend to use ``M=N``.

Coordinate system and boundary conditions
----------------------------------------

PORTA uses a right-handed coordinate system and a Cartesian grid for the discrete
representation of the spatial distribution of the physical quantities. The grid does
not need to be equidistant in any axis. The current version of PORTA assumes
periodic boundary conditions in the X and Y axes. In future releases, this
constraint will be removed. Given domain size Dx and three grid points x1, x2,
x3, the representation of the axis in the code would be as in the following
diagram:

.. figure:: images/xperiod.png
   :width: 30%
   :align: center

   Representation of axis points in PORTA	   
	   
Due to the grid periodicity, the last white grid point corresponds to x1 but it
is never actually stored in the memory.

The currently available modules assume black-body illumination at the bottom of
the grid (minimum Z coordinate) and zero illumination from the top of the grid
(maximum Z coordinate).

.. _sc_vs_lc:

Long characteristics formal solution
---------------------------------------

PORTA provides two different commands to calculate the emergent radiation:
*fs_surface[_h5]* and *fs_surface_lc[_h5]*. The former uses the *short
characteristics* method and the latter the *long characteristics* method.

The *short characteristics* method is computationally less expensive than the
*long characteristics* one, but in coarse grids it introduces a large numerical
diffusion due to the high number of upwind interpolations needed. This is even
more acute when the ray of interest has a large angle from the vertical, in
which case it cuts vertical planes, for which further interpolations are needed,
which in turn increase the diffusion.

The following figure illustrates these differences between the *short* and *long
characteristics* methods in 2D. In the *short characteristics* panel (left), we
need to interpolate the intensity values for each *short* ray (for clarity, here
the ray is not highly inclined from the vertical, so we don't have to
interpolate in the vertical planes, but in the case of highly inclined rays this
would lead to interpolation in the vertical planes, and thus to the diffusion
increase). In the *long characteristics* panel (right), we only need to
interpolate the intensity values in the bottom plane which is then propagated
through the domain (the dashed lines represent the continuation of rays that
leave the left boundary and which we have to follow starting from the right
boundary, since we are assuming periodic horizontal boundaries).

.. figure:: images/sc_vs_lc.png
   :width: 100%
   :align: center

   Short characteristics vs. long characteristics methods. Interpolated values
   are marked in the figures as *Int*).
	   
Depending on the particular model and the inclination of the rays of interest,
the difference between the solution calculated with the *short* and the *long*
characteristics can be quite substantial. As an example, we show below the
emergent maps obtained for a 60º ray, calculated with the *short
characteristics* method (left) and the *long characteristics* method (right).

.. figure:: images/sc_vs_lc_P.png
   :width: 100%
   :align: center

   Example emergent maps calculated with *short characteristics* (left) and
   *long characteristics* (right). 
	   




